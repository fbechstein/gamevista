---
title: 'P Lost Horizon (PC)'
author: gamevista
type: post
date: 2009-08-29T20:32:36+00:00
excerpt: '<p>[caption id="attachment_518" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/08/fenton.jpg" rel="lightbox[lh1]"><img class="caption alignright size-full wp-image-518" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-fenton.jpg" border="0" alt="Darf ich vorstellen? Fenton Paddock" title="Darf ich vorstellen? Fenton Paddock" align="right" width="140" height="100" /></a>Darf ich vorstellen? Fenton Paddock[/caption]Kennt ihr noch die guten alten Abenteuerfilme? Ein verwegener Typ ist mit einer hübschen Frau irgendwo im Ausland, sucht irgendein Artefakt und irgendwelche bösen Menschen, meist Nazis, wollen ihnen an den Kragen. Die erfolgreichste Filmserie heißt „Indiana Jones“, die wohl jeder von euch kennt.</p> '
featured_image: /wp-content/uploads/2009/08/small-fenton.jpg

---
<a href="http://www.deepsilver.com/" target="_top">Deep Silver</a> hat auf der diesjährigen Gamescom ein wenig über ein neues Point&Click-Adventure geplaudert. Es heißt **Lost Horizon** und soll ähnlich wie die alten Abenteuerfilme eine Mischung aus Rätsel, Drama und Action bieten. Wie sie das mit dem eher langatmigen Gameplay eines Point&Click-Adventures verbinden wollen, erklärten zwei der Entwickler von „Animation Arts“ (Entwickler der erfolgreichen Adventures „Geheimakte“) auf der Gamescom.

Zuerst aber zur Story: 1936 versuchen die Nazis ihre Welteroberungspläne durch den Fund von okkulten Waffen zu realisieren. Dazu zählt natürlich auch ein lang verschollener Schlüssel zum mythischen Shambala, einem mythologischen Königreich. Fenton Paddock, ein ehemaliger britischer Soldat und glückloser Schmuggler, wird gebeten, eine britische Expedition zu suchen, zu welcher man den Kontakt verloren hat. Also bricht er nach Tibet auf und gerät ins Visier der Nazis. Der Titel „Lost Horizon“ stammt vom gleichnamigen Buch mit dem das Spiel aber geschichtlich nichts gemein hat, außer der Zeit und dass ein Abenteurer etwas erlebt.

Natürlich ist Fenton Jones … Pardon, ich meine Fenton Paddock nicht allein. Kim steht ihm zur Seite und muss in einigen Situationen kooperativ gesteuert werden. Das Ganze klingt bedeutend komplizierter als es eigentlich ist.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/verfolgung.jpg" rel="lightbox[lh3]"><img class="caption alignright size-full wp-image-521" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-verfolgungsjagd.jpg" border="0" alt="Verfolgungsjadg wie aus einem Abenteuerfilm." title="Verfolgungsjadg wie aus einem Abenteuerfilm." align="right" width="140" height="100" /></a>Bei einer Autoverfolgung muss man beispielsweise ihr zuerst sagen, dass sie schneller und dann langsamer fahren soll, anschließend auf den LKW aufspringen, sich danach bei ihr erkundigen, was eigentlich in den Kisten ist und diese dann mit ihrer Hilfe öffnen, die Raketen (siehe Screenshot) in den Auspuff des Motors stecken, Kim ein mit einem Tuch umwickelnden Stock geben, den sie dann (von uns gesteuert) mit den Zigarettenanzünder entzündet und zurückreicht, daraufhin wieder mit ihm die Raketen abschießen. Puh … geschafft!

Dadurch, dass der Spieler sehr schnell in vielen Situationen agieren muss, entsteht wirklich eine Art Action. Nicht zu vergessen ist dabei, dass der Spieler manchmal nur eine gewisse Zeit zum Reagieren hat.



Aber auch der Fan des klassischen Adventures soll nicht zu kurz kommen. So gibt es wie immer viele, teilweise auch anspruchsvolle Rätsel. Die Spiellänge soll ungefähr 20 Stunden betragen, dabei hängt es natürlich stark davon ab, wie viele der Dialoge ihr euch anhört. Die Entwickler haben dabei versucht einen möglichst filmähnlichen Stil zu erzeugen.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/kims-wohnung.jpg" rel="lightbox[lh4]"><img class="caption alignright size-full wp-image-523" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-kims-wohnung.jpg" border="0" alt="Die handgezeichneten Hintergründe sind sehr detailliert und wirken stimmig." title="Die handgezeichneten Hintergründe sind sehr detailliert und wirken stimmig." align="right" width="140" height="100" /></a>Marco Zeugner (Projektleiter, Besitzer und Gamedesigner) von Deep Silver hat uns verraten, dass die Erstellung von solchen actionsgeladenen Szenen sehr aufwendig ist und sie daher vermehrt nur am Anfang eingesetzt wurden um den Einstieg zu erleichtern.

Der grafische Stil ist anders als in den vorherigen Werken. Man hat sich für einen illustrativen Stil entschieden, das heißt die Hintergründe sind komplett handgezeichnet. Die Spielwelt wirkt sehr stimmig und es entsteht sofort eine Stimmung wie damals im Kino als Harrison Ford (alias Indiana Jones) noch sein Lasso schwang. Leider ist die musikalische Untermalung zwar gut, aber nicht zu vergleichen mit der genialen Filmmusik von Indiana Jones. Marco Zeugner erklärte dazu, dass man die etwas sanftere Musik gewählt hat um den Spieler langfristig nicht abzuschrecken. Nach 30 Minuten „Indiana Jones“ Titelmelodie hört selbst der größte Fan auf zu spielen.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/club.jpg" rel="lightbox[lh5]"><img class="caption alignright size-full wp-image-525" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-club.jpg" border="0" alt="Natürlich darf ein klassisches Inventar bei einem Adventure nicht fehlen." title="Natürlich darf ein klassisches Inventar bei einem Adventure nicht fehlen." align="right" width="140" height="100" /></a>Technische Neuerungen sind zum einem, dass die Hintergründe nun eine höhere Auflösung haben – dies hat aber die die Hardwareanforderungen nicht spürbar erhöht. Auf der anderen Seite, soll das Spiel nun mit vielen verschiedenen Auflösungen zu Recht kommen.

Das klassische Adventure ist für manche etwas zu langweilig. Das neue Adventure „Lost Horizon“ könnte dieses Manko ausgleichen. Die Entwickler versuchen einen interaktiven Abenteurfilm zu entwickeln und das scheint ihnen auch zu gelingen. Jeder Adventurefan, der Indiana Jones cool findet, darf sich auf das dritte Quartal 2010 freuen, denn dann erscheint „Lost Horizon“.

* * *



* * *