---
title: 'P Cursed Mountain (Wii)'
author: gamevista
type: post
date: 2009-08-20T16:02:15+00:00
excerpt: '<p>[caption id="attachment_566" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/08/gottheit.jpg" rel="lightbox[cm1]"><img class="caption alignright size-full wp-image-566" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-gottheit.jpg" border="0" alt="Hier sehen wir eine Gottheit des Buddhismus, die wohl ein Bossgegner sein wird." title="Hier sehen wir eine Gottheit des Buddhismus, die wohl ein Bossgegner sein wird." align="right" width="140" height="100" /></a>Hier sehen wir eine Gottheit des Buddhismus, die wohl ein Bossgegner sein wird.[/caption]Bei einem Horrorspiel sitzt man normalerweise verkrampft von der Konsole und benutzt den Kontroller, um sich dahinter zu verstecken. Nun aber, soll man den Kontroller dazu verwenden zu kämpfen. Das funktioniert natürlich nur mit der Wii und so ist „Cursed Mountain“ ein Wii-exklusiver Titel.</p> '
featured_image: /wp-content/uploads/2009/08/small-gottheit.jpg

---
Für „richtige“ Gamer war es bis jetzt kein Verlust keine Wii zu haben. Die meisten Spiele sind eher für die Familie ausgelegt. Nun endlich wagt sich ein Entwickler das, was bis dato sich noch keiner so gewagt hat. Einige Hersteller haben bereits bekannte, auch nicht unbedingt familienfreundliche Marken wie Call of Duty und Brothers in Arms auf die Wii gebracht, doch erstmals wird nun ein Survival-Horror-Game exklusiv für die Wii entwickelt.

Es sind die 80er Jahre und wir gehen ausgerüstet ohne GPS-Gerät, Satellitentelefon oder Schneemobil das Himalayagebirge hinauf. Die Frage stellt sich natürlich jedem: Warum? Unser älterer Bruder war auf der Suche nach einem Artefakt und wir gehen ihn nun suchen, da der Kontakt zu ihm abgebrochen ist und wir nicht wissen, ob er überhaupt noch lebt. Das Ganze wäre natürlich kein gutes Horrorspiel geworden, wenn es nicht auch einen Fluch geben würde. Man bekommt es immer wieder mit Geistern zu tun, die wir aber nicht einfach abschießen sollen, sondern erlösen. Der Fluch verhindert nämlich, dass sie aufsteigen können und wir helfen ihnen irgendwie dabei.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/geist.jpg" rel="lightbox[cm2]"><img class="caption alignright size-full wp-image-569" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-geist.jpg" border="0" alt="Wir können uns durch den Eispickel etwas Distanz verschaffen." title="Wir können uns durch den Eispickel etwas Distanz verschaffen." align="right" width="140" height="100" /></a>Dabei gibt es keine Waffen. Wir haben lediglich einen Eispickel und können im Spielverlauf verschiedene Artefakte „drauf stecken“, wodurch wir über verschiedene Fernkampfattacken verfügen. Allgemein wirkt das Kampfsystem sehr durchdacht. Zuerst müssen wir die Geister schwächen und können sie dann durch Gebetsgesten erlösen. Diese Gesten basieren auf Gebetsgesten von tibetanischen Mönchen. Diese Finishing Moves sehen durchaus ganz nett aus.



<a href="http://www.gamevista.de/wp-content/uploads/2009/08/boss.jpg" rel="lightbox[cm3]"><img class="caption alignright size-full wp-image-571" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-boss.jpg" border="0" alt="Unser Charakter und im Hintergrund ein Bossgegner auf den wir im Spiel mehrfach treffen werden." title="Unser Charakter und im Hintergrund ein Bossgegner auf den wir im Spiel mehrfach treffen werden." align="right" width="140" height="100" /></a>Das Spiel ist levelbasiert, das heißt es gibt verschiedene aufeinander folgende Level. Typisch für ein solches Spiel gibt es am Ende jedes Levels ein Bossfight. Ein Gegner, den wir mehrfach im Spiel begegnen werden als Endgegner ist diese Vogel-Mensch-Mischung rechts neben unserem Charakter (siehe Screenshot). Es gibt einen Lebensbalken bzw. –kreis, den man entweder durch das Erlösen von Geistern oder durch das Entzünden von Räucherstäbchen in bestimmten Gefäßen wieder auffüllen kann.

Grafisch ist das Spiel, das wohl aufwendigste Wii-Spiel, was jemals produziert wurde. Bis jetzt war wohl niemanden klar, das die Wii so eine Grafik überhaupt bieten kann. Man erkennt  sogar vom Berg herab, die einzelnen Level, wie z.B. das Dorf und das Kloster.

Auf meine Rückfrage hin, stellt Martin Filipp (Developer Relations Manager bei Deep Silver) klar, dass das Risiko der Entwicklung zwar wirklich recht hoch sei, man jedoch fest an den Titel glaube. Man möchte mit dem Titel vor allem die Erwachsenen ansprechen, die die Konsole bis jetzt nur selten nutzen und zeigen, dass die Wii nicht nur eine Partykonsole ist.

Das Spiel erscheint am 21. August 2009 und wir sind sehr gespannt, ob das Spiel einschlagen wird. Das Spiel hat viel Potenzial, vermittelt sehr viel Spannung und Horror-Feeling. 

* * *



* * *

 