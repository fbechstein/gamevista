---
title: 'P Die Siedler 7'
author: gamevista
type: post
date: 2010-02-09T19:10:22+00:00
excerpt: '<p>[caption id="attachment_1357" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/11/1.jpg" rel="lightbox[ds71]"><img class="caption alignright size-full wp-image-1357" src="http://www.gamevista.de/wp-content/uploads/2010/02/small1.jpg" border="0" alt="Die Siedler siedeln wieder!" title="Die Siedler siedeln wieder!" align="right" width="140" height="100" /></a>Die Siedler siedeln wieder![/caption]Momentan laufen einige Beta-Tests von Spielen. Wir haben für euch die Beta-Version von <strong>Die Siedler 7 </strong>ausführlich getestet. Die Entwickler <a href="http://www.bluebyte.de/">BlueByte</a> setzen bei ihrem neusten Ableger auf einige Änderungen. Die letzten beiden Siedler Spiele konnten leider nicht wirklich überzeugen. Ob es der siebte Teil besser macht, lest ihr in unserer Preview.</p> '
featured_image: /wp-content/uploads/2010/02/small1.jpg

---
**Die Siedler** war schon immer ein Spiel, das sich in Deutschland besser verkauft hat als in Nordamerika. Das lag vor allem an der Fixierung des Spiels auf Bauen und Wirtschaften. Bereits mit dem fünften Teil der Serie versuchte [BlueByte][1] ein neues Kampfsystem einzuführen. Das stieß damals auf wenig Zuspruch und deshalb lag der Fokus des sechsten Teils wieder auf dem Bauen. Mit dem siebten Teil versuchen die Entwickler nun ein Spiel zu verkaufen, das gleich mehrere neue Ansätze mit sich bringt.

Die Wirtschaftskreisläufe sind Siedler-typische komplex. In alle bisherigen Spielen der Serie endeten die Kreisläufe im Militär. Das haben die Entwickler geändert. Nun gibt es drei Möglichkeiten diese Kreisläufe, wie zum Beispiel den zur Herstellung von Brot, zu nutzen Nämlich im Handel, in der Forschung und im Militär. Wir können Handelsmänner, die für uns Handel treiben, Kleriker, die für uns forschen und Soldaten, die für uns Gebiete erobern, verteidigen und den Feind bekämpfen.

Früher hat es gereicht den Feind einfach zu töten. Dazu hat man eine möglichst große Armee aufgebaut und damit dann die feindliche Burg gestürmt. Natürlich ist das nicht genug und die zwei neuen Kategorien hätte keine Daseinsberechtigung. Also haben die Entwickler ein sogenanntes Sieg-Punkte-System eingeführt. Dahinter versteckt sich, dass die Spieler für verschiedene Sachen, wie zum Beispiel dem Erobern (und Halten) von vielen Gebieten der Karte oder dem Erforschen eines Siegespunktes, einen solchen Punkt erhält. Dabei reicht es nicht nur auf den Handel, die Forschung oder das Militär zu setzen – die Mischung macht’s!

<p style="text-align: center;">
  <img class=" size-full wp-image-1358" src="http://www.gamevista.de/wp-content/uploads/2010/02/4.jpg" border="0" width="600" height="340" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/4.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/4-300x170.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Die Kleriker können in der Kirche ausgebildet werden und forschen dann für den Spieler.



<a href="http://www.gamevista.de/wp-content/uploads/2009/11/3.jpg" rel="lightbox[ds72]"><img class="caption alignright size-full wp-image-1359" src="http://www.gamevista.de/wp-content/uploads/2010/02/small3.jpg" border="0" alt="Viele Maps haben einzigartige Gebäude." title="Viele Maps haben einzigartige Gebäude." align="right" width="140" height="100" /></a>Auch drei Missionen der Kampagne kann man in der Beta anspielen. Leider gibt es noch viele Bugs, doch das was man sieht und spielen kann gefällt durchaus. Man erkennt schnell, dass die Leveldesigner sich wie immer große Mühe gegeben haben interessante und vielseitige Maps zu erstellen. Die drei ersten Missionen der Kampagne wirken stimmig, sind aber weder sonderlich fesselend noch actionreich. Es ist bis jetzt nur wenig über die Geschichte bekannt und jeder Siedler-Fan muss sich wohl überraschen lassen.

Die Geräusche des Waldes, der Tiere und des Wassers wissen zu gefallen. Ganz anders ist es da mit der Musik. Während im letzten Teil noch ein Orchester die Musik beisteuerte, setzen die Entwickler im neuen Teil auf neuer wirkende Musik. Diese passt aber leider überhaupt nicht zu **Die Siedler**. Zumindest symbolisiert die Musik, dass sich die Serie verändert hat, aber vor allem der Titelsong ist eher eine Zumutung.

Der Multiplayermodus ist zugegeben spannender und actionreicher als jemals zuvor. Dabei kommt ihm vor allem das neue Kampfsystem zu Gute. Dieses basiert auf Generälen, die der Spieler befehligen kann. Dabei hat jeder General seine Armee, die wir natürlich ausbilden müssen. Diese Truppen könnten dann zum Beispiel Gebiete erobern, verteidigen und feindliche Truppen vernichtend schlagen.

**Fazit**

**Die Siedler 7** könnte das beste Siedler-Spiel aller Zeit sein. Doch leider ist der neue Siedler-Teil nicht so familienfreundlichen wie das im Trailer wirkt. Der siebte Teil der Serie kann vor allem durch viele interessante und sinnvolle Neuerungen punkten, doch die Entwickler haben noch viele Löcher auszubessern bevor das Spiel erscheinen kann. Dennoch ist es ein Anwärter für den deutschen Entwicklerpreis 2010 – das liegt aber nicht an der überragend Qualität des Spiels, sondern vielmehr daran, dass ernsthafte Konkurrenz, außer Crysis 2, fehlt.

<p style="text-align: center;">
  <img class=" size-full wp-image-1110" src="http://www.gamevista.de/wp-content/uploads/2009/11/2.jpg" border="0" width="600" height="338" />
</p>

Die Animationen der Siedler sind hübsch und es macht Spaß ihnen einfach beim Siedeln zuzuschauen.

* * *



* * *

 [1]: http://www.bluebyte.de/