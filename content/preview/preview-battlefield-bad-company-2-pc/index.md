---
title: 'P Battlefield Bad Company 2 (PC)'
author: gamevista
type: post
date: 2010-02-22T09:04:49+00:00
excerpt: '<p>[caption id="attachment_1384" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2010/02/logo.gif" rel="lightbox[bfbc21]"><img class="caption alignright size-full wp-image-1384" src="http://www.gamevista.de/wp-content/uploads/2010/02/smalllogo.gif" border="0" alt="Auf in die Schlacht!" title="Auf in die Schlacht!" align="right" width="140" height="100" /></a>Auf in die Schlacht![/caption] Bis heute spielen begeisterte Fans <strong>Battlefield 2</strong> und die unzähligen Mods für dieses Spiel beweisen, dass es eines der beliebtesten Spiele überhaupt ist. Nun endlich nach jahrelangem Fasten steht der Release eines neuen <strong>Battlefield</strong>-Ablegers für den PC bevor. Der erste Teil von <strong>Bad Company</strong> konnte damals auf der Xbox360 und PS3 vor allem durch schrägen Humor im Singleplayermodus und großflächig zerstörbare Maps im Multiplayermodus überzeugen. Genau da setzt der zweite Teil, auch entwickelt von <a href="http://www.dice.se/">DICE</a>, an. Ob es wieder gelungen ist und ob das erste PC-Spiel der <strong>Battlefield</strong>-Serie seit Langem überzeugen kann, haben wir für euch herausgefunden.</p> '
featured_image: /wp-content/uploads/2010/02/smalllogo.gif

---
Die Beta-Version von **Battlefield Bad Company 2** bot leider nur Einblick in eine Multiplayermap mit einem Spielmodus. Dennoch konnte man sich einen guten Eindruck vom Game- und Leveldesign und von der Grafik verschaffen.

<a href="http://www.gamevista.de/wp-content/uploads/2009/11/1.jpg" rel="lightbox[bfbc22]"><img class="caption alignright size-full wp-image-1386" src="http://www.gamevista.de/wp-content/uploads/2010/02/small1.jpg" border="0" alt="Das Klassensystem wirkt stimmig." title="Das Klassensystem wirkt stimmig!" align="right" width="140" height="100" /></a> Das Gamedesign unterscheidet sich nicht wesentlich von den Vorgängern, sowohl dem direkten Vorgänger als auch dem letzten **Battlefield**-PC-Spiel. Es gibt unterschiedliche Soldatenklassen, wobei es dieses mal nur vier Typen gibt. Dabei gibt es den Standard-Soldaten, der mit einem Maschinengewehr ausgerüstet ist und Munition an andere Spieler verteilen kann. Dazu kommt der Sanitäter, der sowohl heilen als auch wiederbeleben kann und ebenfalls mit einem Maschinengewehr ausgerüstet ist. Gegen feindliche und als Verstärkung für verbündete Fahrzeuge gibt es den Techniker, der sowohl einen Raketenwerfer als auch ein Gerät zum Reparieren hat. Abgerundet wird das Team vom Scharfschützen, der in manchen Phasen eines Matches entscheidend seien kann, aber vor allem beim Stürmen unterlegen ist.

 ****

Zusätzlich haben die Entwickler ein Erfahrungspunktesystem eingebaut, das ganz ähnlich funktioniert, wie das in **Rainbow Six Vegas 2**. Für einen einfachen Kill erhält man Punkte, für einen Headshot gibt es Extrapunkte. Aber auch für das Heilen, Wiederbeleben, Reparieren und ähnliche Aktionen gibt es Punkte. Diese Punkte werden je nach verwendeter Klasse gut geschrieben. Je nach Klasse kann man so zum Beispiel einen neuen Raketenwerfer oder ein besseres Scharfschützengewehr frei schalten.

<p style="text-align: center;">
  <img class=" size-full wp-image-1110" src="http://www.gamevista.de/wp-content/uploads/2009/11/2.jpg" border="0" width="600" height="338" />
</p>



<a href="http://www.gamevista.de/wp-content/uploads/2010/02/4.jpg" rel="lightbox[bfbc23]"><img class="caption alignright size-full wp-image-1387" src="http://www.gamevista.de/wp-content/uploads/2010/02/small4.jpg" border="0" alt="Wow! Genial!" title="Wow! Genial!" align="right" width="140" height="100" /></a>Diese neuen Waffen und Spezialisierungen kann man dann im Ingame-Menü einsetzen und damit seine Klasse jederzeit verändern. Das Spiel spielt in der nahen Zukunft. Die Auswahl an Waffen ist beeindruckend: Von Schrotflinten über Maschinengewehrprototypen bis hin zu älteren Waffen aus dem zweiten Weltkrieg. Zusätzlich zu den Verbesserungen je Klasse, steigt man auch noch Level auf und erlangten einen höheren Rang. Auch dafür erhält man neue Gegenstände und Waffen. Das alles führt dazu, dass das Spiel eine sehr große Langzeitmotivation bietet. Der Spieler spielt darauf hin mit einer neuen Waffe zu spielen – diese entstehende Motivation ist beeindruckend.

Grafisch ist das Spiel das wohl schönste **Battlefield**-PC-Spiel aller Zeiten, wobei die stärkste Konkurrenz **Battlefield 2** ist, das bereits deutlich in die Tage gekommen ist. Die Level sind zu etwa 90% zerstörbar. Von Häuserwänden, über ganze Häuser bis hin zu Deckungen – alles kann verschossen oder gesprengt werden. Leider stimmt aber die Aussage, dass sich immer wieder neue taktische Möglichkeiten ergeben, nicht. Das anspielte Level &#8222;Port Valdez&#8220; war relativ linear und ließ für taktische Spielereien nur bedingt Raum. Dadurch spielte sich die Map zwar hin und wieder etwas anders, aber eigentlich immer recht ähnlichen.

<a href="http://www.gamevista.de/wp-content/uploads/2009/11/3.jpg" rel="lightbox[bfbc24]"><img class="caption alignright size-full wp-image-1388" src="http://www.gamevista.de/wp-content/uploads/2010/02/small3.jpg" border="0" alt="Die Sicht aus einem Fahrzeug kann anfangs irritierend sein." title="Die Sicht aus einem Fahrzeug kann anfangs irritierend sein." align="right" width="140" height="100" /></a>Die Beta-Version enthielt leider auch nur einen Spielmodus. Es gibt ein angreifendes und ein verteidigendes Team im Spielmodus „Rush“. Dabei müssen die Angreifer zwei Boxen zerstören, bevor ihre Verstärkungspunkte aufgebraucht sind. Die Verteidiger müssen diese beiden Boxen verteidigen. Schaffen sie es nicht, was öfters der Fall ist, müssen sie sich zurückziehen und die Frontlinie verschiebt sich nach hinten. Wenn die Angreifer dies drei Mal schaffen, haben sie die Runde gewonnen.



Wir konnten die Beta-Version von **Battlefield Bad Company 2** ausführlich testen und es gibt nur 6 Worte dafür: Nettes Konsolen-Spiel, aber kein PC-Spiel. Ein PC-Spieler merkt bereits im ersten Match, dass die Steuerung irgendwie komisch ist. Vergeblich sucht man die Taste zum Hinlegen – auch Scharfschützen können nur hocken. Man braucht teilweise ein ganzes Magazin bevor ein gegnerischer Soldat zu Boden geht.

Leider ist es eine riesengroße Enttäuschung für alle PC-Spieler. **Battlefield Bad Company 2** setzt da an, wo **Bad Company 1** aufhörte. Wer glaubt, dass es ein Nachfolger von **Battlefield 2** ist, irrt deutlich, und es bleibt nur zu hoffen, dass sich [EA][1] und [DICE][2] irgendwann entschließen, doch mal ein **Battlefield 3** zu entwickeln.

Die PC-Beta-Version verheißt also nichts gutes, für die Zukunft der **Battlefield**-Serie auf dem PC. Ob man nun lachen sollte, weil es endlich wieder ein PC-Spiel gibt, oder weinen sollte, weil es nur eine mittelmäßige Portierung einer Konsolenversion ist, weiß ich leider auch nicht. Eines ist klar: Der PC-Spieler ist symbolisch gesprochen in diesem Bild der Soldat, der auf dem Boden liegt und zu dem keiner eilt um ihn wieder zu beleben.

<p style="text-align: center;">
  <img class=" size-full wp-image-1389" src="http://www.gamevista.de/wp-content/uploads/2010/02/5.jpg" border="0" width="600" height="337" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/5.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/5-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *



* * *

 [1]: http://www.electronic-arts.de/
 [2]: http://www.dice.se/