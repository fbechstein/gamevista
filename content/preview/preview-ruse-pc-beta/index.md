---
title: 'P R.U.S.E. (PC-Beta)'
author: gamevista
type: post
date: 2009-12-10T14:26:04+00:00
excerpt: '<p>[caption id="attachment_1166" align="alignright" width=""]<img class="caption alignright size-full wp-image-1166" src="http://www.gamevista.de/wp-content/uploads/2009/12/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Laut dem Publisher <a href="http://www.ruse.de.ubi.com" target="_blank">Ubisoft</a> sowie dem Entwickler Eugen Systems startete vor einigen Tagen die Closed Beta zum Echtzeitstrategietitel <strong>R.U.S.E.</strong> Der neuste Spross von <a href="http://www.ruse.de.ubi.com" target="_blank">Ubisoft</a> soll das Strategie-Genre kräftig umkrempeln und will mit einer stufenlos-zoombaren Kamera, sowie innovativem Gameplay, überzeugen. Im Rahmen der Closed Beta hatten wir die Möglichkeit, den etwas anderen Strategietitel, anzutesten. Ob <strong>R.U.S.E.</strong> demnach den etwas angestaubten Strategiesektor neu belebt und ob man wirklich seine Gegner so gut täuschen kann, wie es <a href="http://www.ruse.de.ubi.com" target="_blank">Ubisoft</a> beschreibt, erzählen wir euch in unserer Preview aus der Beta.</p> '
featured_image: /wp-content/uploads/2009/12/ruse_small.jpg

---
Typisch für ein Strategiespiel, ob für Konsole oder PC dürfte eine Vielzahl von Einheiten, Gebäuden und Taktiken sein, um dem Gegner ordentlich einzuheizen. So forscht und baut man, sammelt Ressourcen und versucht im Stein-Schere-Papier Prinzip dem Gegner die richtigen Einheiten in den Weg zu stellen. Leider wird das mit der Zeit doch recht langweilig und so haben sich schon diverse Entwicklerstudios an der Innovationsforschung im Echtzeitstrategiebereich versucht, meist erfolglos.

Während Command & Conquer immer bunter wird und etliche Fans den alten Tagen nachtrauern, an denen das Entwicklerstudio Westwood noch verantwortlich war. Versuchte <a href="http://www.ruse.de.ubi.com" target="_blank">Ubisoft</a> am Anfang des Jahres mit Tom Clancy\`s Endwar, mit einer Sprachsteuerung der Einheiten, Akzente zu setzen und dem Spieler vom altgedienten Schlachtfeld ins digitale Zeitalter einzustimmen. Leider haperte es dann doch am Mission, Mapdesign bzw. an der Präsentation des Spiels. Nun versucht <a href="http://www.ruse.de.ubi.com" target="_blank">Ubisoft</a> ein weiteres Mal, zusammen mit dem Entwickler Eugen Systems, das Strategiespiel-Genre neu zu beleben.

<p style="text-align: center;">
  <img class=" size-full wp-image-1167" src="http://www.gamevista.de/wp-content/uploads/2009/12/ruse6.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/ruse6.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/ruse6-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Mit dem Konzept dem Spieler eine Auswahl an Täuschungsmanövern, in Verbindung mit einer stufenlos zoombaren Karte zur Verfügung zu stellen, ziehen wir also auf das Schlachtfeld. Zuvor wählen wir uns noch eine der vier in der Beta verfügbaren Fraktionen aus. Insgesamt soll es aber sechs Fraktionen geben. Zum einen wären da die Hauptkriegsteilnehmer des 2. Weltkriegs USA, England, Deutschland und Russland. Bisher noch nicht zu testen waren Frankreich und Italien.   
Wobei sich hier sicherlich die spannende Frage stellt, wie sich das Balancing gestallten wird. Wir haben die Möglichkeit zwischen einem Ranking-Spiel im 1vs1- oder 2vs2-Modus zu wählen und bekommen prompt einen Gegner zugewiesen der so wie wir Level 1 ist. Denn am Ende der Runde bekommt man Erfahrungspunkte auf sein Konto gutgeschrieben was das Level erhöht und so für ausgeglichene Teams sorgen soll.



Der erste Blick fällt auf die riesige Übersichtskarte die mehrere Sektoren einer Schneelandschaft beherbergt. Das Interface ist Übersichtlich und lässt viel Platz für das Spielgeschehen. Sofort fällt uns die brandneue IRISZOOM-Engine auf, in der man stufenlos zwischen verschiedenen taktischen Ansichten, nahtlos hin und her wechseln kann. So könnt ihr in Sekundenbruchteilen wichtige Befehle geben und euch den Kampf nah am Geschehen ansehen. Dies ist wichtig da man sehr oft an vielen verschiedenen Teilen der Karte kämpft und taktiert.

Eine wichtige Rolle dabei spielen die so genannten RUSE\`s. Das sind Fähigkeiten die jeweils auf einen Sektor festgelegt werden können um den Gegner entweder mit Scheinangriffen zu verwirren, Einheiten oder Gebäude zu verstecken oder die Spielzüge des Gegenspielers aufzudecken. So gibt es insgesamt zehn dieser Fähigkeiten die man nach und nach im Spiel verwenden kann, vorausgesetzt man hat die nötigen Punkte dazu. Damit eröffnen sich, vor allem im Teamspiel, ganz neue Möglichkeiten für ein Strategiespiel. Denn so kann man z.B. den Luftangriff des Mitspielers, mit Attrappen die Vorrausfliegen, unterstützen.

Auffallend ist der Aspekt das man für **R.U.S.E.** sehr wenig Mikromanagement, das möglichst effektive Steuern der einzelnen Einheit, benötigt. Vielmehr ist es wichtig die komplette Karte im Blick zu haben damit auch kein Angriff vom Kartenrand, in der Hitze des Gefechts, verborgen bleibt. Dabei bleibt **R.U.S.E.** sehr einsteigerfreundlich und wir fühlen uns sofort vertraut mit der Steuerung und Interface.

<p style="text-align: center;">
  <img class=" size-full wp-image-1168" src="http://www.gamevista.de/wp-content/uploads/2009/12/ruse2.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/ruse2.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/ruse2-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

So baut man zu Beginn der Runde zwei Rohstofflager, die uns das nötige Kleingeld per Lastwagen zur Basis liefern, und erschafft danach die erste Produktionsstätte für Infanterie, Flugabwehr, Artillerie, Panzer, Panzerabwehr, Flugzeuge oder Prototypen. Wir entscheiden uns zuerst für eine Baracke um ein paar Soldaten zu produzieren. Die Infanterie ist ein wichtiger Bestandteil von **R.U.S.E.** denn sie sind die einzige Einheit die Gebäude einnehmen kann. Dabei ist sie sehr billig und kann sich in Wäldern oder Dörfern verstecken, um aus dem Hinterhalt heraus zu feuern. Für einige Dollar mehr könnten wir uns auch Elite Infanterie leisten. Diese muss man aber zuerst erforschen, was zu Spielanfang unser Untergang wäre.

Die Forschung stellt einen zentralen Punkt im Spiel dar, denn können wir am Anfang noch mit billigen Panzer III der deutschen Wehrmacht gegen die Panzer unseres Gegners konkurrieren. Wird dieser zwangsläufig Panzerjäger oder Bomber erforschen. Dabei hat jede Fraktion ihre Vor- und Nachteile. Die USA werden demnach billige Einheiten in Massen produzieren können, während Deutschland auf starke und teure Einheiten setzt.



Zu Anfang stehen uns zwei RUSE-Punkte zu Verfügung. Damit wir unserem Gegner nicht preisgeben was wir für Gebäude bauen, setzen wir in unserem Startsektor die Ruse-Fähigkeit Camouflage Net ein. Diese tarnt unsere Gebäude für vier Minuten und lässt so unsere Strategie für den Gegner unsichtbar werden. Außerdem nutzen wir die Blitz-Fähigkeit um unsere Ressourcen-Trucks und Einheiten im Startgebiet schneller zu bewegen.   
So täuschen und bluffen wir uns durch das Spiel, wie in einer Partie Poker, um schlussendlich auf Prototypen wir dem deutschen Maus-Panzer oder dem russischen IS-3 zurückzugreifen.

Natürlich gehört zu einem Strategiespiel auch die Nutzung der Karteneigenschaften. So fährt man auf Straßen schneller, kann in Wäldern oder Städten Hinterhalte legen. Vermisst haben wir die Vor- und Nachteile von Bergen und Erhöhungen. Diese gibt es auf den momentan vorhandenen Karten quasi nicht. Gut gelöst hat der Entwickler die Luftkämpfe, schließlich spielte dieser eine große Rolle im 2. Weltkrieg.   
So errichtet man ein Flugfeld und hat je nach Fraktion Auswahl über Jäger, Bomber oder Aufklärungsflugzeugen. Natürlich können wir auch hier die Flugzeuge auf eine stärkere Variante erforschen. Insgesamt wird **R.U.S.E.** mehr als 200 verschieden Einheiten und Gebäude bieten, darunter werden auch diverse Bunker und Verteidigungsanlagen zur Verfügung stehen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1169" src="http://www.gamevista.de/wp-content/uploads/2009/12/ruseallscreenshotartilleryshoot_12156476783_5136.jpg" border="0" width="600" height="343" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/ruseallscreenshotartilleryshoot_12156476783_5136.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/ruseallscreenshotartilleryshoot_12156476783_5136-300x172.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Grafisch ist **R.U.S.E.** durchaus auf der Höhe der Zeit, Einheiten und Landschaft sehen prächtig aus und bieten jede Menge Details. Dabei geht die Performance selbst bei größeren Scharmützeln nicht in die Knie. Bei Gefechten von Jagdflugzeugen kann man sogar die spannenden Dogfight\`s beobachten, während die großen Bomber ganz Landstriche mit einem Bombenteppich belegen. Für eine Beta ist das Spiel doch schon sehr ausgereift. So kamen uns kaum Grafikfehler oder Abstürze in die Quere. Das Matchmaking-System verschaffte uns innerhalb von Sekunden die ersten Multiplayer-Kämpfe.

**Fazit**

Der Titel **R.U.S.E.** bietet endlich frischen Wind im Strategie-Genre. Auch wenn man sich schon ein paar Videos und Screenshots zu **R.U.S.E.** angeschaut hat, spielt es sich doch um einiges anders als man denkt, im positiven Sinne. Umso erfreulicher ist es, das ein großer Publisher wie <a href="http://www.ruse.de.ubi.com" target="_blank">Ubisoft</a> darauf setzt, ein Spiel auf den Markt zu bringen das bisher so noch nie da gewesen ist. Vorausgesetzt der Singleplayer hat auch einiges zu bieten, dürfte **R.U.S.E.** einer der ganz großen Strategietitel des nächsten Jahres werden. Wem **R.U.S.E.** bisher nichts sagt der sollte sich unbedingt das Beta-Video mit kommentaren vom Entwickler anschauen. R.U.S.E. erscheint voraussichtlich im ersten Frühjahr 2010.

[> Zum R.U.S.E. &#8211; Beta Trailer][1]







 [1]: videos/item/root/ruse-beta-trailer