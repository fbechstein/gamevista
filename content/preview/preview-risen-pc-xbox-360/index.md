---
title: 'P Risen (PC, Xbox 360)'
author: gamevista
type: post
date: 2009-07-07T09:28:14+00:00
excerpt: '<p>[caption id="attachment_17" align="alignright" width="140"]<a href="http://www.piranha-bytes.com/" target="_blank" rel="lightbox"><img class="caption alignright size-full wp-image-17" src="http://www.gamevista.de/wp-content/uploads/2009/07/risentier.png" border="0" alt="Risen - nette Zeitgenossen erwarten den Spieler" title="Risen - nette Zeitgenossen erwarten den Spieler" width="140" height="100" align="right" />Piranha Bytes</a>Risen - nette Zeitgenossen erwarten den Spieler[/caption] sind zurück! Nach dem Bug-Debakel bei Gothic3 und der Trennung vom Publisher <a href="http://www.jowood.com/" target="_blank">Jowood</a> präsentieren die Essener zusammen mit dem Publisher <a href="http://www.deepsilver.de/" target="_blank">Deepsilver</a> den inoffiziellen Gothic Nachfolger – Risen. <br />Das Spiel soll am 2. Oktober 2009 seinen Streetday feiern. Wir haben einige Informationen über das Inselabenteuer für Euch zusammengetragen. </p>'
featured_image: /wp-content/uploads/2009/07/risentier.png

---
**Die Story  
**   
Die Story von Risen liest sich sehr spektakulär. Die Welt wurde von einer Katastrophe genannt „Die dunkle Welle“ heimgesucht. Dieses magische Ereignis hat die komplette Welt verwüstet und viele Menschen verloren ihr Leben. Damit nicht genug.  
Nun scheint noch ein neues Übel aufzukommen, bei dem Menschen verschwinden, dunkle Kreaturen die Bevölkerung bedrohen und uralte Ruinen aus dem Boden emporstechen.  
Ein vermeintlich sicherer Ort ist die Insel, auf welche Ihr angespült werdet. Und dort beginnt auch Euer Abenteuer bei Risen.   
Auf der Insel herrscht &#8222;Die Inquisition&#8220;, eine Organisation des Königs, welche die Aufgabe hat zum einen die merkwürdigen Ereignisse zu untersuchen. Die Bevölkerung wird von eben dieser Organisation kontrolliert, es darf sich nämlich keiner frei auf der Insel bewegen. Wer erwischt wird, muss mit Konsequenzen rechnen. Die Gegner der Inquisition sind die Banditen. Diese versuchen Ihre Hafenstadt wieder zurückzuerobern, die von der Inquisition besetzt wurde.

**Das Gameplay**

<figure id="attachment_18" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-18" src="http://www.gamevista.de/wp-content/uploads/2009/07/risenisland.png" border="0" alt="Risen - am Strand beginnt das Abenteuer" title="Risen - am Strand beginnt das Abenteuer" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Risen &#8211; am Strand beginnt das Abenteuer</figcaption></figure>Bei Risen versucht Piranha Bytes „Back to the Roots“ zu gehen. Das Spiel soll storylastiger und düsterer als in Gothic 3 werden.  Man beginnt das Spiel ohne Vorkenntnisse und darf sich erst langsam in die Story und deren Zusammenhänge vortasten. Das Spiel wird in vier Kapitel unterteilt und einige Gebiete werden erst nach und nach freigeschaltet.   
Das Gameplay erinnert stark an die alten Gothic Teile, d.h. Ihr könnt im Spielverlauf entscheiden welchem Lager Euer Charakter sich anschließt. Dabei stehen Euch drei Fraktionen zur Verfügung: die Inquisition, die Banditen und die Magier.



  
**Kämpfe & Magie**

<figure id="attachment_19" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-19" src="http://www.gamevista.de/wp-content/uploads/2009/07/risencombos.png" border="0" alt="Risen - Combos statt totklicken" title="Risen - Combos statt totklicken" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Risen &#8211; Combos statt totklicken</figcaption></figure>Im Gegensatz zu Gothic3, wo man á la Hack´n´Slay die Gegner ins Jenseits beförderte, setzen die Piranhas auf altbewährtes, nämlich Combos. Ihr bekommt die Möglichkeit mit der Kombination aus den Tastenkombinationen W-A-S-D und der linken Maustaste verschiedene Combos auszuprobieren. Mit der rechten Maustaste könnt Ihr Euer virtuelles Ich verteidigen. Ferner spielt die Dauer des Drückens der Kampf-Taste eine sehr große Rolle. Drückt Ihr kurz, wird ein leichter Schlag ausgeführt – drückt Ihr die Taste länger, wird der Schlag umso intensiver. In Verbindung mit den Combos entstehen so nicht nur optisch schöne Kämpfe, sondern auch kritische Treffer mit denen Ihr Euren Gegner mehr Schaden zufügt.   
Die Magie wurde gegenüber der Gothic-Reihe ebenfalls geändert. Es gibt ab sofort ein Levelsystem, wo Ihr erlernte Zauber je nach Level ausbauen könnt. Ebenfalls lassen sich Zauber selbst zusammenstellen.   
Bereits heiß diskutiert und mit Spannung erwartet ist der Verwandlungszauber. Mit dem Zauber habt Ihr die Möglichkeit Euch z.B. in eine Maus zu verwandeln und in ein Loch zu kriechen um unüberwindbare Wege zu meistern.

**Die Technik**

<figure id="attachment_20" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-20" src="http://www.gamevista.de/wp-content/uploads/2009/07/risenruinen.png" border="0" alt="Risen - detailreiche Spielgrafik rundet das Spielgeschehen ab" title="Risen - detailreiche Spielgrafik rundet das Spielgeschehen ab" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Risen &#8211; detailreiche Spielgrafik rundet das Spielgeschehen ab</figcaption></figure>Risen basiert auf einer von Piranha Bytes selbstentwickelten Grafikengine. Die Grafik erinnert stark an Gothic 3, was jedoch nicht weiter verwundert, denn letztendlich entwickelte dasselbe Team alle drei Gothic-Teile. Systemvoraussetzungen sind leider noch nicht bekannt, das Abenteuer soll jedoch auch auf älteren Rechnern flüssig laufen.  
Das Spiel wird sowohl für den PC, als auch für die Xbox 360 erscheinen. Eine Demo soll höchstwahrscheinlich noch vor dem Release erscheinen.



  
**Fazit**

<figure id="attachment_21" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-21" src="http://www.gamevista.de/wp-content/uploads/2009/07/risenhafen.png" border="0" alt="Risen gehört zu den RPG Hoffnungen 2009" title="Risen gehört zu den RPG Hoffnungen 2009" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Risen gehört zu den RPG Hoffnungen 2009</figcaption></figure>Man kann wirklich gespannt sein, was die Piranhas aus Essen uns da servieren werden. Wenn das Spiel nicht das gleiche Schicksal wie Gothic3 erleidet, dann können wir ein Rollenspiel des Jahres „Made in Germany“ erwarten. 

<font color="#999999"><a href="index.php?Itemid=111&#038;option=com_zoo&#038;view=category&#038;alpha_char=t">> Screenshoots zu Risen</a></font>  
[> Videos zu Risen][1] 

&nbsp;







 [1]: index.php?Itemid=102&option=com_zoo&view=category&alpha_char=r