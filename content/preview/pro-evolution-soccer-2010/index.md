---
title: Pro Evolution Soccer 2010 – Meisterliga Upgrade
author: gamevista
type: post
date: 2009-07-22T19:50:32+00:00
excerpt: '[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]<a href="http://de.games.konami-europe.com/" target="_blank" title="Konami"><strong><font color="#000080"><span style="font-style: normal"><span style="font-weight: normal">Konami</span></span></font></strong></a><strong><font color="#000000"><span style="font-style: normal"><span style="font-weight: normal"> enthüllt neue Details zum Fussballspiel </span></span></font></strong><strong><font color="#000000"><span style="font-style: normal"><strong>Pro Evolution Soccer 2010</strong></span></font></strong>.<strong><font color="#000000"><span style="font-style: normal"><span style="font-weight: normal"> Die Meister Liga, einer der sehr beliebten Spielmodi, wird einige zusätzliche Optionen enthalten.</span></span></font></strong> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
<p style="margin-bottom: 0cm; background: none transparent scroll repeat 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
  <strong><font color="#000000"><span style="font-style: normal"><span style="font-weight: normal">So wird es 3 Untermenüs, Club House, Stadium Walk und Office, geben. Jeder dieser Bereiche soll für eine bestimme Managerfunktion stehen, welche der Spieler steuern kann. Im Modus Clubhouse wird es möglich sein taktische Einstellungen und die Optionen der kommenden Spieltage festzulegen. Damit bietet Clubhouse völlig neue Möglichkeiten. Sie können eine Jugendmannschaft ausbilden um dann fähige Jungstars in ihr Hauptteam zu rekrutieren. Stadium Walk ermöglicht dem Spieler Änderungen am Hauptspiel.<br />Im Office Modus werden Spielerscouting sowie das Vertragsmanagement möglich sein, außerdem wird Sponsoring, echte Währungen und Fanclubs eine sehr große Rolle spielen.<br /></span></span></font></strong><strong><font color="#000000"><span style="font-style: normal"><span style="font-weight: normal">Bleibt zu hoffen das die versprochenen Änderungen im Multiplayer- und Interfacebereich besser werden als bei PES 2009.</span></span></font></strong>
</p>

**<font color="#000000"><span style="font-style: normal"><strong>PES 2010</strong><span style="font-weight: normal"> erscheint für Wii, PLAYSTATION 3, Xbox 360, PC-DVD, PSP (PlayStation Portable), PlayStation 2 und Mobiltelefone im Herbst. </p> 

<p>
  </span></span></font></strong>
</p>



<p>
  
</p>

