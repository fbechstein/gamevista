---
title: 'Preview und Interview: Stronghold Kingdoms (PC)'
author: gamevista
type: post
date: 2010-04-01T05:37:57+00:00
excerpt: '<p>[caption id="attachment_1627" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/10/logo.jpg" rel="lightbox[shk1]"><img class="caption alignright size-full wp-image-1627" src="http://www.gamevista.de/wp-content/uploads/2010/04/smalllogo.jpg" border="0" alt="Das erste Stronghold-MMO" title="Das erste Stronghold-MMO" align="right" width="140" height="100" /></a>Das erste Stronghold-MMO[/caption]Nach langer Zeit melden sich die Stronghold-Entwickler der <a href="http://www.fireflyworlds.com/">Firefly Studios</a> zurück. Nachdem man von ihrem letzten, noch nicht erschienen Werk <strong>Dungeon Hero</strong> schon über ein Jahr nichts Neues mehr gehört hatte, kündigten sie <strong>Stronghold Kingdoms </strong>an und widmen sich anscheinend voll und ganz diesem Titel. Wir haben uns die Alphaversion des Strategie-MMOs näher angeschaut. Ob der neuste Teil der Stronghold-Serie überzeugen kann, lest ihr in unserer Preview. Zusätzlich haben wir noch Simon Bradbuy, Lead Designer bei Firefly, zu den Spielen <strong>Stronghold Kingdoms</strong>, <strong>Dungeon Hero</strong> und der Stronghold-Serie befragt.</p> '
featured_image: /wp-content/uploads/2010/04/smalllogo.jpg

---
<a href="http://www.gamevista.de/wp-content/uploads/2010/04/combat.jpg" rel="lightbox[shk2]"><img class="caption alignright size-full wp-image-1629" src="http://www.gamevista.de/wp-content/uploads/2010/04/smallcombat.jpg" border="0" alt="Bis zu einer solchen Burg, ist es ein langer Weg" title="Bis zu einer solchen Burg, ist es ein langer Weg" align="right" width="140" height="100" /></a>Das Leben als Ritter ist anfangs immer etwas öde und so dauert es fast einen Monat, bis das Spiel endlich ohne gravierende Grafikfehler bei mir lief. Das ist aber für eine Alphaversion nun mal völlig normal und das Spiel läuft inzwischen wirklich gut. Wenn ich nicht wüsste, dass es noch keine Betaversion ist, würde ich es wahrscheinlich nicht erkennen. Nun endlich kann ich das Menü sehen und problemlos dem Ritterleben frönen. Dachte ich zumindest …

Man bemerkt sehr schnell, dass **Stronghold Kingdoms** sehr viel tiefgehender in seiner Spielmechanik ist als alle anderen Stronghold-Spiele bis jetzt. Einfach nur mal schnell ein Stündchen spielen und dann steht eine hübsche Burg – das war bis jetzt Standard in der Stronghold-Serie. Doch diese einfache Regel brechen die Entwickler jetzt. In ihrem kostenlosen MMO stehen vor allem der langsame Aufbau, die zeitaufwendige Forschung und die Interaktion mit anderen Spielern im Vordergrund. Trotzt der Geringbeschäftigung wird einem nie langweilig, wenn man sich währenddessen noch mit etwas anderem beschäftigt – Zeit dazu hat man. Es spielt sich wie ein gutes Browsergame und ist entsprechend nicht besonders actiongeladen, bietet aber eine hübsche Grafik und eine viel bessere Spielmechanik.

Grafisch ist es etwas besser als **Stronghold Crusader**. Es wird schnell deutlich, dass einige Animationen und Texturen verbessert und kopiert wurden, andere sind hingegen neu. Dennoch wirkt die Grafik in sich stimmig und ist für mich inzwischen typisch **Stronghold**. Einzig die etwas unschöne Übersichtskarte von England stößt auf. Im Zeitalter von Google Earth ist da sehr viel mehr machbar, auch bei einer interaktiven MMO-Übersichtskarte.

<p style="text-align: center;">
  <img class="caption size-full wp-image-1630" src="http://www.gamevista.de/wp-content/uploads/2010/04/world.jpg" border="0" alt="Nicht hübsch, aber zweckdienlich: Die Übersichtskarte" title="Nicht hübsch, aber zweckdienlich: Die Übersichtskarte" width="600" height="435" srcset="http://www.gamevista.de/wp-content/uploads/2010/04/world.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/04/world-300x218.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Musik beinhaltet das Spiel (noch) nicht, wobei das bei einem solchen Spiel auch gar nicht notwendig ist. Wenn man Musik will, muss man sie eben abspielen. **Stronghold Kingdoms** läuft gut mal nebenbei, während man zum Beispiel eine Preview schreibt.

Wie finanziert sich ein kostenloses MMO? Genau, durch natürlich nicht kostenlose Spielkarten. Diese Karten können in Paketen à fünf Karten gekauft werden und bringen dann Vorteile, wie zum Beispiel eine Warteschlange für den Gebäudebau, ohne die man jeweils nur ein Gebäude in Auftrag geben kann.

Interessant ist die Idee mit den Karten, aber nicht wirklich neu. Soweit man das beurteilen kann, funktioniert das bei **Stronghold Kingdoms** aber sehr gut und die Karten werden durchaus positiv angenommen. Selbst für Gelegenheitsspieler wird kein Weg an der einen oder anderen Spielkarte vorbei führen, aber wieso auch?

Wir hatten die Chance **Simon Bradbury**, Lead Designer von **Stronghold Kingdoms** und Mitgründer der [Firefly Studios][1] zu interviewen.

**1. Was sind die Schlüsselelemente bzw. Mainfeatures von** **Stronghold Kingdoms? Was unterscheidet das neue Spiel von allen bisher dagewesenen Stronghold-Spielen und von anderen MMOs?**

<a href="http://www.gamevista.de/wp-content/uploads/2010/04/research.jpg" rel="lightbox[shk3]"><img class="caption alignright size-full wp-image-1632" src="http://www.gamevista.de/wp-content/uploads/2010/04/smallresearch.jpg" border="0" alt="Die Forschungsmöglichkeiten sind wirklich umfassend" title="Die Forschungsnmöglichkeiten sind wirklich umfassend" align="right" width="140" height="100" /></a>Was **Stronghold Kingdoms** so vollkommen einzigartig macht ist, dass es das erste echte Strategie-MMO ist (meiner Meinung nach!). Die meisten MMOs sind RPG (Rollenspiele) oder nur solche, die ich als light Version von Strategie bezeichnen würde. Kingdoms hat richtige Tiefe und bringt einiges von den bekannten Stronghold-Spielen mit sich – wie zum Beispiel das Burgbauen, das Erobern, das Wirtschaftaufbauen und kombiniert das mit einem tiefgehenden Forschungsbaum, der ähnlich ist wie der in Spielen wie Civilisation.

Was es anders macht als bisherige Stronghold-Spiele ist die völlige Interaktivität. Du levelst up, erhälst einen neuen Rang, ziehst in den Krieg. Ich gehe immer noch mit einem guten Gefühl ins Bett, wenn ich weiß, dass meine Burg während ich schlafe weiter gebaut wird.



**2.** **Stronghold Kingdoms wird komplett kostenlos zum Downloaden und Spielen sein. Aber wie groß und entscheidend wird die Rolle der Spielkarten letztlich sein? Oder anders gefragt: Wird es möglich sein, dass Spiel auch ohne diese Karten zu spielen?**

Kingdoms wird kostenlos sein, aber natürlich kostet es Geld das Spiel zu entwickeln und zu betreiben. Es wird vielleicht einen Premiumservice geben, der es Spielern erlaubt, die nicht 24 Stunden 7 Tage die Woche spielen, im Spiel zu bleiben und sich immer noch mit anderen Spielern messen zu können. Aber wir hoffen, dass Spiel durch den Verkauf von Spielkarten zu finanzieren. Diese werden durchaus einen entscheidenden Einfluss auf das Spiel haben und es wird wichtig sein die Karten entsprechend passend anzuwenden. Wir können uns gut vorstellen kostenlose Spielkarten an Spieler zu verteilen.

<a href="http://www.gamevista.de/wp-content/uploads/2010/04/cards.jpg" rel="lightbox[shk4]"><img class="caption alignright size-full wp-image-1634" src="http://www.gamevista.de/wp-content/uploads/2010/04/smallcards.jpg" border="0" alt="Die Spielkarten sind gewöhnungsbedürftig, aber hilfreich" title="Die Spielkarten sind gewöhnungsbedürftig, aber hilfreich" align="right" width="140" height="100" /></a>**3. Es wird auch in der finalen Version Fünfer-Päckchen geben? Wie wird entschieden, welche Karten diese beinhalten? Und was werden diese koste?**

Ja, Karten werden in Paketen verkauft und diese werden zufällig ausgewählt sein. Die Spieler werden aber Kartensets verkaufen können um genau die Karten zu kriegen, die sie wollen. Momentan haben wir noch keinen Preis festgelegt.

**4. In der News zu** **Dungeon Hero vom 30. März 2009 steht, dass ihr am einem next-gen Spiel der Stronghold-Serie arbeiten. Ist** **Stronghold Kingdoms dieses Spiel oder können wir auf ein** **Stronghold 3 oder** **Stronghold Crusader 2 hoffen?**

Wir werden sehr bald dazu etwas ankündigen. Was ich sagen kann ist das: Kingdoms meinten wir nicht, als wir das sagten!!

**5. Die letzte News über** **Dungeon Hero ist von Ende März 2009. Ist das Spiel gecancelt oder nur langfristig pausiert?**

<a href="http://www.gamevista.de/wp-content/uploads/2010/04/dungeonhero.jpg" rel="lightbox[shk5]"><img class="caption alignright size-full wp-image-1636" src="http://www.gamevista.de/wp-content/uploads/2010/04/smalldungeonhero.jpg" border="0" alt="Schade, Dungeon Hero sah sehr interessant aus." title="Schade, Dungeon Hero sah sehr interessant aus." align="right" width="140" height="100" /></a>Es ist nicht tot, es ruht nur. Es tut uns sehr leid, dass wir den vielen Leuten die uns fragen wie es um das Spiel steht nicht mehr sagen können, aber dafür gibt es verschiedene gute Gründe. **Dungeon Hero** hat einen besonderen Platz in unseren Herzen, das ist alles was ich momentan sagen kann.

**Vielen Dank, dass du dir Zeit genommen hast, unsere Fragen zu beantworten. Wir sind sehr gespannt auf** **Stronghold Kingdoms und die weitere Entwicklung der Stronghold-Serie und** **Dungeon Hero.**

Kein Problem. Bitte besucht doch unsere Website [www.strongholdkingdoms.com][2] und tragt euch für die Openbeta ein, die in ein paar Monaten starten wird.



**Fazit**

**Stronghold Kingdoms** sieht nach einem sehr guten und kostenlosen MMO aus. Das Spiel wird gut angenommen und die inzwischen ausgehungerte Stronghold-Gemeinde nimmt das Spiel dankend an. Für jeden Stronghold-Fan ist also zumindest das intensive Testen eine Pflicht. Für alle von euch, die keine Stronghold-Fans sind ist es aber auch ein Blick wert. Es lässt sich gut nebenbei spielen, aber auch mit nur ein paar Klicks am Tag. **Stronghold Kingdoms** erweitert die Serie würdig, wenn auch in eine für die Serie eher untypische Richtung.

<p style="text-align: center;">
  <img class="caption size-full wp-image-1637" src="http://www.gamevista.de/wp-content/uploads/2010/04/village.jpg" border="0" alt="Stronghold hat seinen Weg ins Jahr 2010 gefunden" title="Stronghold hat seinen Weg ins Jahr 2010 gefunden" width="600" height="418" srcset="http://www.gamevista.de/wp-content/uploads/2010/04/village.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/04/village-300x209.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

 

* * *



* * *

 [1]: http://www.fireflyworlds.com/
 [2]: http://www.strongholdkingdoms.com/