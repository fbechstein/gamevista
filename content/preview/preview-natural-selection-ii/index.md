---
title: 'P Natural Selection II'
author: gamevista
type: post
date: 2010-01-17T10:07:40+00:00
excerpt: '<p>[caption id="attachment_1319" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/10/logo.jpg" rel="lightbox[ns21]"><img class="caption alignright size-full wp-image-1319" src="http://www.gamevista.de/wp-content/uploads/2010/01/smalllogo.jpg" border="0" alt="Überraschungshit?" title="Überraschungshit?" align="right" width="140" height="100" /></a>Überraschungshit?[/caption]Vielen von euch wird der Name <strong>Natural Selection</strong> wahrscheinlich an eine Mod erinnern. In der Tat war es eine Modifikation für <strong>Half-Life</strong>, die Halloween 2002 erschien und zu einer der populärsten überhaupt wurde. Nun hat sich ein unabhängiges Entwicklerstudio gewagt und will, basierend auf der Mod, mit komplett neuer Engine, ein Spiel entwickeln. <strong>Natural Selection II </strong>sollte Herbst 2009 erscheinen – soll inzwischen im Frühjahr 2010 erscheinen. In unserer Preview lest ihr alles über den eventuellen Überraschungshit 2010.</p> '
featured_image: /wp-content/uploads/2010/01/smalllogo.jpg

---
**Natural Selection II** erfindet **Natural Selection** nicht neu. Es geht einfach gesagt darum, warum es nun mal immer geht in einem First-Person-Shooter: Spaß haben und die andere abballern! Dabei geht es in **Natural Selection** um den Kampf Aliens vs. Marines, mit einigen Veränderungen des typischen Gameplays: So wird das Spiel auch Real-Time-Strategy-Elemente enthalten. Es wird für den Commander der Marines, einer der beiden spielbaren Seiten, möglich sein Verteidigungsgeschütze, Ressourcenpunkte, Infanterieportale, eine Art Waffenschrank und mehr in der Überkopfperspektive zu bauen. Dann können die Bodentruppen, die das Spiel natürlich in der ersten Perspektive spielen, an den entsprechenden Punkten sich zum Beispiel Ausrüstung kaufen.

<a href="http://www.gamevista.de/wp-content/uploads/2010/01/onos.jpg" rel="lightbox[ns22]"><img class="caption alignright size-full wp-image-1321" src="http://www.gamevista.de/wp-content/uploads/2010/01/smallonos.jpg" border="0" alt="Eine der drei spielbaren Typen der Aliens" title="Eine der drei spielbaren Typen der Aliens" align="right" width="140" height="100" /></a>Weil ein Spiel mit nur einer Fraktion keinen Sinn macht und es langweilig wäre, wenn man gegen die gleichen Leute nur mit anderen Farben spielt, schießen die Marines in **Natural Selection II** natürlich auf Aliens, die damit auch die zweite spielbare Fraktion bilden. Der Entwickler <a href="http://www.unknownworlds.com" target="_blank">Unknown Worlds</a> mit Sitz in San Francisco hat bis jetzt drei Alientypen vorgestellt. Da haben wir zum einen den Skulk, der sich wohl durch seine Schnelligkeit und seine scharfen Zähne hervor tun wird, den Gorge, der für mich ein wenig wie ein außerirdisches Schwein aussieht und als größter Gegner den Onos, dem man nicht unbedingt gegenüber stehen will.



Zusätzlich zu den komplett unterschiedlichen Fraktionen und den RTS-Elementen gibt es aber noch weitere Features. Der Entwickler wirbt mit „dynamischer Umgebung“. Das System klingt äußerst interessant. So können die Aliens Teile der Karte, zum Beispiel einer Raumbasis, infizieren und mit ihrem Schleim überziehen. Das führt dann unter Umständen zu einem Energieausfall, sodass die Lichter ausgehen und Energie benötigende Gebäude der Marines nicht mehr funktionieren. Dagegen können diese natürlich mit Flammenwerfen vorgehen. Die eigene Engine soll eine breite Möglichkeit an Variationen bieten und jedes Match dadurch einzigartig machen.

<a href="http://www.gamevista.de/wp-content/uploads/2010/01/marine.jpg" rel="lightbox[ns23]"><img class="caption alignright size-full wp-image-1323" src="http://www.gamevista.de/wp-content/uploads/2010/01/smallmarine.jpg" border="0" alt="Hübsch und alientötend!" title="Hübsch und alientötend!" align="right" width="140" height="100" /></a>Zuletzt wird **Natural Selection II** mit allen Tools, mit denen die Entwickler das Spiel momentan entwickeln, ausgeliefert. Dazu gehören der Leveleditor, der Model-Viewer, der Cinematiceditor und der Scriptdebugger. Damit wollen sie, ganz wie es sich eigentlich gehört, die Modder unterstützen und es bleibt zu hoffen, dass sich viele fleißige Modder finden, die das Spiel weiter ausbauen.

<p style="text-align: left;">
  Vermarktet wird das Spiel nur online, einmal über Steam und einmal über die Seite der Entwickler. Nach einem Sudoko-Lernspiel, arbeiten die Entwickler nun an einem Spiel, was ich als „Eventueller Überraschungshit 2010“ bezeichnen will. <strong>Natural Selection II</strong> hat das Potenzial ein gutes Spiel zu werden. Jetzt bleiben uns nur noch zwei Fragen: Wann genau erscheint es und für welche Seite soll ich mich nur entscheiden?
</p>

<p style="text-align: center;">
  <img class=" size-full wp-image-1324" src="http://www.gamevista.de/wp-content/uploads/2010/01/labor.jpg" border="0" width="600" height="418" srcset="http://www.gamevista.de/wp-content/uploads/2010/01/labor.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/01/labor-300x209.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *



* * *