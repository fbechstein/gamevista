---
title: 'P Alan Wake (PC, Xbox 360)'
author: gamevista
type: post
date: 2009-06-24T08:00:00+00:00
excerpt: '[caption id="attachment_10" align="alignright" width="140"]<img class="caption alignright size-full wp-image-10" src="http://www.gamevista.de/wp-content/uploads/2009/06/alan_small1.png" border="0" alt="Alan Wake" title="Alan Wake - A Psychological Action Thriller" width="140" height="100" align="right" />Alan Wake - A Psychological Action Thriller[/caption] <p>Im Frühjahr 2010 soll das Meisterwerk der Max Payne Schmiede <a href="http://www.remedygames.com/" target="_blank">Remedy</a> erscheinen. Die technische Umsetzung und Grafik sollen bereits jetzt schon zu den Highlights der kommenden Games zählen. Wird die Story dabei untergehen? Wir versuchen dies etwas zu beleuchten. </p>'
featured_image: /wp-content/uploads/2009/06/alan_small1.png

---
**Wer ist eigentlich Alan Wake?**

Alan Wake ist ein Horror-Schriftsteller, der unter einer Schreibblockade leidet. Um sich ein wenig zu erholen und zu inspirieren beschließt er mit seiner Freundin Alice die Zeit im verträumten Städtchen Bright Falls zu verbringen. Dies allerdings entpuppt sich als ein wahrer Albtraum. Seine Freundin, welche im Übrigen auch seine Muse ist, verschwindet spurlos und es scheint, als wären seine Horrorgeschichten Wirklichkeit. Alan Wake macht sich auf den Weg um seine Freundin zu suchen. ** ** 

**GTA meets Silent Hill**

Das Spiel soll in verschiedenen Episoden aufgeteilt sein, die jeweils mit einem Cliffhanger enden. Silent Hill- und GTA-Spieler werden sich im Gameplay von Alan Wake sofort zurechtfinden. Remedy erschafft hier eine ziemlich authentische Umgebung mit vielen Freiheiten, in der man einen Tag- und Nacht Zyklus erhält. In der Nacht entpuppt sich das Grauen in Form von üblen Gestalten, die nicht einfach mit einem Schuss aus der Pistole erledigt werden können. Die Taschenlampe ist hier genauso wichtig wie der Colt – erst nach dem Beleuchten der finsteren Kreaturen werden diese geschwächt  und können mit einem goldenen Schuss ins Jenseits befördert werden. Alan Wake hetzt hier durch Wälder und Gebäude und die Dramaturgie wird vom Soundtrack sehr stark in den Vordergrund gestellt. 



**  
Grafikpracht Ahoi</p> 

</strong>Kleine technische Finessen wie die besonders ausgearbeiteten Lichteffekte oder die vom Wind verursachten Wellenbewegungen des Wassers machen das Spiel zu ei<figure id="attachment_11" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-11" src="http://www.gamevista.de/wp-content/uploads/2009/06/alan_small2.png" border="0" alt="Alan Wake" title="Die Lichteffekte machen das Spiel zum wahren Augenschmaus" vspace="20" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Die Lichteffekte machen das Spiel zum wahren Augenschmaus</figcaption></figure>nem wahren Hingucker. Die Grafikengine wird hierzu komplett neuentwickelt, so dass man keine Befürchtung haben muss, dass die Max Payne Engine zum Einsatz kommt. Die Nachteile bei der Max Payne Engine lagen beim Aussenleveldesign, welcher bei Alan Wake sehr stark im Vordergrund tritt.  
Leider geht die Grafikpracht zu Lasten der Hardware. Dual- bzw. Quadcore Prozessoren werden supportet. Ob bis dahin DirectX 11 unterstützt wird, steht leider noch  in den Sternen. Erscheinen soll das Spiel auf der Xbox 360 und für den PC.

**A Psychological Action Thriller 2010?**

Wir können gespannt sein, ob Alan Wake zu den Top Games 2010 dazugehören wird.  Die diesjährige E3 Präsentation sah bereits sehr gut aus. Wenn die Story von der Grafikpracht nicht erschlagen wird, so können wir auf einen Mystery Thriller der Extraklasse hoffen. ** ** 

[> Alan Wake Screenshots anschauen][1]   
[> Alan Wake Videos anschauen][2]







 [1]: screenshots/item/screenshots/alan-wake
 [2]: videos/alphaindex/a