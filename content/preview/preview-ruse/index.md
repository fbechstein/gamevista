---
title: 'P R.U.S.E. (PC, Xbox 360, PS3)'
author: gamevista
type: post
date: 2009-08-30T18:54:26+00:00
excerpt: '<p>[caption id="attachment_509" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/08/jger-details.jpg" rel="lightbox[ruse1]"><img class="caption alignright size-full wp-image-509" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-jger-details.png" border="0" alt="Man kann sehr schön die Detailverliebtheit an diesem Jäger erkennen." title="Man kann sehr schön die Detailverliebtheit an diesem Jäger erkennen." align="right" width="140" height="100" /></a>Man kann sehr schön die Detailverliebtheit an diesem Jäger erkennen.[/caption] Schon länger kursiert ein erster Trailer des neuen Strategiespiels aus dem Hause <a href="http://www.ubi.com/de/" target="_top">Ubisoft</a> im Internet. <strong>R.U.S.E.</strong> erscheint im ersten Quartal 2010 für Windows PC, PS3 und Xbox 360. Das Spiel wird die sogenannte „Iriszoom Engine“ verwenden, welche ausgezeichnete Zoommöglichkeiten bietet.</p> '
featured_image: /wp-content/uploads/2009/08/small-jger-details.png

---
Auf der diesjährigen Gamescom konnte man das Spiel anspielen. Die Zoommöglichkeiten sind wirklich unglaublich: Man kann in die Übersichtskarte reinzoomen und dann sogar kleinste Details an Panzer oder Infanterie erkennen. Zusätzlich dazu bietet das Spiel einen Aufbau- und Wirtschaftsteil. Es können Nachschublager und verschiedene Gebäude zur Erstellung von Truppen gebaut werden. Es gibt nur einen Rohstoff im Spiel: Geld. Für dieses kann man Truppen kaufen, dazu zählen verschiedene Panzer, leichte Fahrzeuge, Artillerie-Geschütze, verschiedene Arten von Infanterie, Flugzeuge und Anti-Panzer-Fahrzeuge. Darüber hinaus kann man auch spezielle Spionage-Aktionen durchführen, wie z.B. das Sehen der feindlichen Befehle oder das Verdecken aller eigenen Truppen oder Gebäude in einem Sektor.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/wasserdarstellung.jpg" rel="lightbox[ruse2]"><img class="caption alignright size-full wp-image-512" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-wasserdarstellung.png" border="0" alt="Die Effekte sind noch nicht ganz ausgereift." title="Die Effekte sind noch nicht ganz ausgereift." align="right" width="140" height="100" /></a>Bis jetzt ist noch nicht viel über die Geschichte bekannt. Lediglich erkennt man, dass das Spiel anscheint in der Zeit des zweiten Weltkriegs angesiedelt ist und man weiß bereits, dass man sich in der Kampagne vom Hauptmann zum Oberbefehlshaber hocharbeitet. Es soll 6 verschiedene Nationen geben, die Achsenmächte und die Alliierten. Die Story wird getragen von kleinen Videos, die während den Missionen am Rand eingeblendet werden und somit den Spieler nicht völlig aus dem Spielfluss reißen. Es ist davon auszugehen, dass man den ganzen oder Teile des zweiten Weltkrieges in der Kampagne nachspielen können wird.

Grafisch ist das Spiel eine Offenbarung. Noch nie hat ein EZS einen solchen Zoom geboten, und konnte eine sehr gute Übersicht mit einer extremen Detailverliebtheit verbinden. Auch die Effekte sind bereits jetzt wirklich schön. Leider gibt es hier und da noch ein paar grafische Fehler, aber es ist ja auch noch ein wenig Zeit bis zum Release. Ein Beispiel sind die Feuerwerferpanzer, welche besonders effektiv gegen Infanterie sind. Diese schießen wie ein normaler Panzer, nur eben nicht mit Panzergranaten sondern mit Feuerstößen – das müssen die Entwickler noch verbessern.



<a href="http://www.gamevista.de/wp-content/uploads/2009/08/zange.jpg" rel="lightbox[ruse3]"><img class="caption alignright size-full wp-image-514" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-zange.png" border="0" alt="Auch die Steuerungen größere Gruppen von Einheiten gestaltet sich einfach." title="Auch die Steuerungen größere Gruppen von Einheiten gestaltet sich einfach." align="right" width="140" height="100" /></a>Die Entwickler wollten zeigen, dass man auch mit einem Konsolenkontroller ein Echtzeitstrategiespiel gut spielen kann. Dies ist ihnen gut gelungen und die Steuerung mit dem Xbox360-Kontroller funktioniert in der Tat sehr gut und ist schnell erlernt. Da das Spiel auch für den PC erscheint, kann man es natürlich auch klassisch mit Tastatur und Maus spielen. Die Steuerung ist präzise und funktioniert einwandfrei.

Spielerisch ist es ein guter Vertreter des Genres. Wie üblich bietet es eine Mischung aus Aufbau, Spionage und Truppenführung. An einigen Stellen muss der Entwickler „Eugen Systems“ aber noch ein wenig nachrüsten. Im Moment fehlt noch eine Gruppierungsfunktion, die natürlich nicht fehlen darf bei einem Spiel dieser Art.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/bersichtskarte.jpg" rel="lightbox[ruse4]"><img class="caption alignright size-full wp-image-516" src="http://www.gamevista.de/wp-content/uploads/2009/08/small-bersichtskarte.png" border="0" alt="Stufenlos kann man in diese Übersichtskarte hineinzoomen und sich mitten ins Gefecht bringen." title="Stufenlos kann man in diese Übersichtskarte hineinzoomen und sich mitten ins Gefecht bringen." align="right" width="140" height="100" /></a>Noch ein weiteres Detail konnten wir den Leuten am Ubisoftstand entlocken. Es wird entgegen des Trends einen LAN-Modus geben, indem man mit bis zu sieben Mitspielern mit- und gegeneinander spielen können wird. Das gleiche gilt auch für den Online-Modus.

Die Entwickler haben bis jetzt gute Arbeit geleistet. Wenn das Gezeigte die Qualität des gesamten Spiels widerspiegelt und vor oder in der Beta noch einige Fehler korrigiert werden, dann wird R.U.S.E. wohl der Strategie-Hit 2010.

* * *



* * *

 