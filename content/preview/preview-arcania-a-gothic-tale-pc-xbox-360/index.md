---
title: 'P Arcania – A Gothic Tale (PC, Xbox 360)'
author: gamevista
type: post
date: 2009-08-25T13:31:57+00:00
excerpt: '<p>[caption id="attachment_490" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/08/held_big.png" rel="lightbox[arcania1]"><img class="caption alignright size-full wp-image-490" src="http://www.gamevista.de/wp-content/uploads/2009/08/held_small.png" border="0" alt="Erst bei der gamescom wurde das Gesicht des Arcania-Helden enthüllt" title="Erst bei der gamescom wurde das Gesicht des Arcania-Helden enthüllt" align="right" width="140" height="100" /></a>Erst bei der gamescom wurde das Gesicht des Arcania-Helden enthüllt[/caption]Im Rahmen der gamescom in Köln hatten wir die Gelegenheit den offiziellen Nachfolger der Gothic Reihe - <strong>Arcania - A Gothic Tale</strong> in einem frühen Entwicklungsstadium sehen zu dürfen.</p> '
featured_image: /wp-content/uploads/2009/08/held_small.png

---
Der Spieler schlüpft in die Rolle eines namenlosen Helden. Der Held aus Gothic 1-3 nennt sich nun König Robar III, der über die Welt von Arcania herrscht. Das Spiel beginnt mit Diego, einem alten Bekannten aus den letzten Gothic-Teilen. Unser namenloser Held ist mit Diego zu seinem Dorf unterwegs, welches komplett zerstört wurde. Allem Anschein nach wurde das Dorf von den Truppen Königs Robar III zerstört, denn Schiffe mit der Flagge von Robar III ziehen weg.  Hier schwört der namenlose Held Rache, denn er glaubt, dass Robar III dahinter steckt.  
So in etwa beginnt das Abenteuer rund um Arcania, welches einen Neuanfang der Gothic Reihe darstellt. Den Entwicklern und dem Publisher geht es dabei allen Gothic Fans und neuen Spielern den bestmöglichen Start ins Abenteurer zu ermöglichen, denn wer will schon wie z.B. in einer Serie mit der 3. Staffel anfangen.  
Unser namenloser Held hat erst seit der gamescom ein sehr markantes Gesicht bekommen. Die Entwickler haben sich hier sehr nach Schauspielern und Figuren aus Superman und Lost gerichtet. 

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/berg_big.png" rel="lightbox[arcania1]"><img class="caption alignright size-full wp-image-493" src="http://www.gamevista.de/wp-content/uploads/2009/08/berg_small.png" border="0" alt="Verschiedene Gebiete runden das Spiel ab" title="Verschiedene Gebiete runden das Spiel ab" align="right" width="140" height="100" /></a>Das Spiel wird insgesamt fünf Gebiete beinhalten, welche eine Insel bilden. Die Gebiete werden erst nach der Erfüllung bestimmter Aufgaben für den Spieler zugänglich sein. Die Entwickler verzichten hier auf irgendwelche unsichtbaren Barrieren bzw. Zugang ab einem bestimmten Level. Die Gebiete werden aus Bergen, Tälern, Seen, einer Gebirgskette mit Schnee und eisigen Temperaturen und einer Vulkanlandschaft bestehen. Das Gesamtgebiet ist kleiner als bei Gothic 3, jedoch komplexer. Die Entwickler wollen damit eine viel dichtere Story erreichen, die bei einer zu großen Map nicht umsetzbar wäre. Der Spieler würde sich dann eher im Spiel verlaufen. Laut Jowood wird man kaum mehr als 100 Meter laufen müssen, um neue Quests zu erhalten.  
Das erste Gebiet von Arcania heißt Cleaved Maiden und besteht aus einem Küstengebiet mit viel Vegetation. In der Präsentation konnten wir bereits einige Feinde entdecken und einen kleinen Dungeon. Ansonsten waren NPCs noch nicht zu sehen.



<a href="http://www.gamevista.de/wp-content/uploads/2009/08/dungeon_big.png" rel="lightbox[arcania1]"><img class="caption alignright size-full wp-image-495" src="http://www.gamevista.de/wp-content/uploads/2009/08/dungeon_small.png" border="0" alt="Einer der zahlreichen Dungeons in Arcania" title="Einer der zahlreichen Dungeons in Arcania" align="right" width="140" height="100" /></a>Für Arcania wurde eine komplett neue Engine entwickelt, die u.a. dafür sorgen wird, dass der Spieler ohne jegliches Nachladen und Ladescreens sich frei bewegen kann &#8211; und dies ohne Lags. Eine weitere Neuheit wird sein, dass z.B. beim Lauf durch einen sehr dichten Wald die einzelnen Busch- und Sträucherpartien komplett ausgeblendet werden, so dass der namenlose Held nicht den Überblick verliert. Kurzes Beispiel: der Held läuft am Waldrand entlang und sieht sehr viel Wildwuchs. Sobald er sich dem nähert wird dieser transparent. Nach einigen Schritten ist dieser wieder sichtbar.   
Der Tag- und Nachtwechsel liefert spektakuläre Lichtfarben. Insgesamt sieht die Grafik bereits jetzt sehr gut aus und die Engine erlaubt eine sehr gute Weitsicht. Beispiel: es gibt einen Punkt auf der Insel, von dem man fast die komplette Insel überblicken kann. Man kann sogar das Schloß von König Robar III sehen, frei nach dem Motto &#8222;so nah und doch so fern&#8220;.

Es wurde ebenfalls ein komplett neues Kampf- und Magiesystem entwickelt. Hier kann man sich z.B. mit einem Bogen zwei Modi aussuchen, man kann auf das automatische Zielsystem zurückgreifen oder das Zielen selbst übernehmen. Im manuellen Modus fügt man dem Gegner dadurch auch mehr Schaden zu. Generell werden die Kampf- und Magieskills je nach Level ausbaubar sein.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/burg_big.jpg" rel="lightbox[arcania1]"><img class="caption alignright size-full wp-image-497" src="http://www.gamevista.de/wp-content/uploads/2009/08/burg_small.png" border="0" alt="Die neuentwickelte Engine bietet spektakuläre Lichtfarben beim Tag- / Nachtwechsel" title="Die neuentwickelte Engine bietet spektakuläre Lichtfarben beim Tag- / Nachtwechsel" align="right" width="140" height="100" /></a>**Fazit:** Bei Arcania &#8211; A Gothic Tale ist zur Zeit nur ein Gebiet (von fünf) fertig. Hier folgen nun Tests, um zu sehen wie lange die Spieler benötigen um das Gebiet zu spielen, wie plausibel die Quests miteiander funktionieren. Erst dann wird man anfangen in die anderen Gebiete die Quests zu implementieren. Die Präsentation sah grafisch bereits sehr gut aus. Leider haben wir noch kaum NPCs gesehen, so dass man hier leider keine Aussage über das Kampfverhalten bzw. Quests machen kann.  
Das Spiel wurde während der gamescom auf 2010 geschoben, so dass der geplante Release für 2009 nun komplett ausgeschlossen werden kann.

[> weitere Screenshots und Concept Arts zu Arcania &#8211; A Gothic Tale][1] 

* * *



* * *

 

 [1]: index.php?Itemid=111&option=com_zoo&view=item&category_id=0&item_id=228