---
title: Battlefield 1943 – Erscheint im ersten Quartal 2010
author: gamevista
type: news
date: 2009-08-21T18:58:55+00:00
excerpt: '<p>[caption id="attachment_341" align="alignright" width=""]<img class="caption alignright size-full wp-image-341" src="http://www.gamevista.de/wp-content/uploads/2009/08/battle1943.png" border="0" alt="Battlefield 1943" title="Battlefield 1943" align="right" width="140" height="100" />Battlefield 1943[/caption]Es ist noch nicht so lang her da gab <a href="http://www.electronic-arts.de">Electronic Arts</a> bekannt das die PC Version des Shooters <strong>Battlefield 1943</strong> auf unbestimmte Zeit verschoben wird. Nun hat sich Produzent Gordon van Dyke an die Presse gewand um Stellung zu beziehen. Demnach soll eine Portierung der Frostbyte Engine auf den PC ein Grund für die Verschiebung sein.</p> '
featured_image: /wp-content/uploads/2009/08/battle1943.png

---
Weiterhin soll es wohl weitere Probleme geben die die Verschiebung von **Battlefield 1943** auf das erste Quartal 2010 rechtfertigen.

* * *



* * *

 

 

 

 