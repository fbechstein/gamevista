---
title: 'Star Wars: The Force Unleashed 2 – Erscheint im Oktober'
author: gamevista
type: news
date: 2010-05-05T06:22:33+00:00
excerpt: '<p>[caption id="attachment_1192" align="alignright" width=""]<img class="caption alignright size-full wp-image-1192" src="http://www.gamevista.de/wp-content/uploads/2009/12/tfu2.jpg" border="0" alt="Star Wars: The Force Unleashed 2" title="Star Wars: The Force Unleashed 2" align="right" width="140" height="100" />Star Wars: The Force Unleashed 2[/caption]Der Publisher <strong>LucasArts </strong>hat bestätigt das <a href="http://www.lucasarts.com/games/theforceunleashed2/" target="_blank">Star Wars: The Force Unleashed 2</a> am 27. Oktober 2010 in den Handel kommen wird. Dies geht aus einer Facebook Meldung auf der offiziellen Facebookseite des Spiels hervor. Da nun das Veröffentlichungsdatum bekannt ist dürften in kürze weitere Details zum zweiten Teil des Actionspiels bekannt werden.</p> '
featured_image: /wp-content/uploads/2009/12/tfu2.jpg

---
Wir sind gespannt.

 

   

* * *

   

