---
title: Metro 2033 – Erscheint ungeschnitten und ab 18 Jahren
author: gamevista
type: news
date: 2010-01-22T14:43:00+00:00
excerpt: '<p>[caption id="attachment_1346" align="alignright" width=""]<img class="caption alignright size-full wp-image-1346" src="http://www.gamevista.de/wp-content/uploads/2010/01/metro_2033.jpg" border="0" alt="Metro 2033" title="Metro 2033" align="right" width="140" height="100" />Metro 2033[/caption]Der russische Entwickler <strong>4A Games</strong> veröffentlichte vor kurzem einen neuen Trailer zum Endzeit-Shooter <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a>. Der Publisher THQ, zuständig für den Vertrieb in Deutschland, gab nun bekannt das <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a> gänzlich ungeschnitten und ab 18 Jahren bei uns im Handel erscheinen wird.</p> '
featured_image: /wp-content/uploads/2010/01/metro_2033.jpg

---
Ursprünglich war der Veröffentlichungstermin auf Ende 2009 gelegt worden. Doch nun wurde der Titel auf das Jahr 2010 verschoben.

Das Spiel <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a> entsprang aus der Buchvorlage des russischen Schriftstellers Dmitry Glukhovsky. Als Überlebender eines atomaren Krieges, kämpft sich der Protagonist durch die U-Bahntunnel der russischen Metropole Moskau. Diese ist der einzige Zufluchtsort für den Rest der Menschheit.

[> Zum Metro 2033 &#8211; Trailer][1]

<cite></cite>

* * *



* * *

 

 [1]: videos/item/root/metro-2033-trailer