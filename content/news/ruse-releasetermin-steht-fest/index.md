---
title: R.U.S.E. – Releasetermin steht fest
author: gamevista
type: news
date: 2010-07-27 18:26:02 +0000
excerpt: Ubisoft hat im Rahmen einer Twitter-Meldung den finalen Veröffentlichungstermin
  des Strategiespiels R.U.S.E. bekannt gegeben. Der neue Termin soll nach mehreren
  Verschiebungen nun der Letzte sein und eingehalten werden. Demnach wird am 7. September
  der Titel in den USA veröffentlicht.
featured_image: "/wp-content/uploads/2010/07/ruse_small.jpg"

---
Ubisoft hat im Rahmen einer Twitter-Meldung den finalen Veröffentlichungstermin des Strategiespiels R.U.S.E. bekannt gegeben. Der neue Termin soll nach mehreren Verschiebungen nun der Letzte sein und eingehalten werden. Demnach wird am 7. September der Titel in den USA veröffentlicht.

In Europa wird das Spiel am 9. September für PC, PlayStation 3 und Xbox 360 in den Handel kommen.