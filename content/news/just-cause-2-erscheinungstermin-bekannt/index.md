---
title: Just Cause 2 – Erscheinungstermin bekannt
author: gamevista
type: news
date: 2009-11-25T08:25:45+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Der Publisher <a href="http://www.justcause.com" target="_blank">Eidos</a> hat im Rahmen einer offiziellen Pressemitteilung bekannt gegeben das <strong>Just Cause 2</strong> im Frühjahr 2010 erscheinen wird. Demnach wird der Titel am 23. März 2010 in Europa im Handel für PlayStation 3, Xbox 360 und PC erscheinen.<strong> Just Cause 2</strong> bietet ein über 1000 km² großes Inselparadies in Südostasien, was dem Spieler völlige Bewegungsfreiheit bieten soll.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
Es gibt schneebedeckte Berge, sonnige Strände, dichte Dschungelgebiete und ausgedörrte Wüsten zu erkunden.    
&#8222;_Just Cause 2_ lässt euch die Welt nicht nur frei entdecken, sondern gibt euch auch die Freiheit, auf jede beliebige Art mit ihr zu interagieren&#8220;, sagte Lee Singleton, General Manager von Square Enix London Studios. &#8222;Die Kombination aus Enterhaken und Fallschirm verändert die Art, wie ihr in einer offenen Welt spielt. Durch sie könnt ihr innerhalb von Sekunden abheben und diesen taktischen Vorteil nutzen, wenn ihr eure Missionen von oben in Angriff nehmt. Wer lieber festen Boden unter den Füßen behält, dem bietet der Enterhaken unzählige Möglichkeiten, die nur durch die eigene Fantasie eingeschränkt werden,&#8220; berichtet der Publisher.

 







 