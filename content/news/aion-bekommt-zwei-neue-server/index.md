---
title: Aion – Bekommt zwei neue Server
author: gamevista
type: news
date: 2009-09-29T08:38:39+00:00
excerpt: '<p>[caption id="attachment_224" align="alignright" width=""]<img class="caption alignright size-full wp-image-224" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion" title="Aion" align="right" width="140" height="100" />Aion[/caption]Wie Entwickler <a href="http://www.eu.ncsoft.com/de/">NCSoft </a>nun offiziell bestätigte wird das vor kurzem erschienene Online Rollenspiel <strong>Aion </strong>zwei neue Server in Europa bekommen. Französischsprachige Spieler finden ihr neues Heim auf dem Server Deltras. Ein weiterer Server mit dem Namen Kahrun wird für die Englisch sprechende Community installiert.</p> '
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
Grund hierfür sind die massiven Wartezeiten beim Versuch sich in das Spiel einzuloggen. [NCSoft][1] will weitere Server nachlegen falls das sich das Problem durch die neuen Server nicht beheben lässt.

 

* * *



* * *

 

 [1]: http://www.eu.ncsoft.com/de/