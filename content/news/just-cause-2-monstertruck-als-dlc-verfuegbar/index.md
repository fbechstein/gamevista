---
title: Just Cause 2 – Monstertruck als DLC verfügbar
author: gamevista
type: news
date: 2010-04-15T16:11:07+00:00
excerpt: '<p>[caption id="attachment_1689" align="alignright" width=""]<img class="caption alignright size-full wp-image-1689" src="http://www.gamevista.de/wp-content/uploads/2010/04/justcause2_small.png" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Nachdem der Entwickler <strong>Avalanche Software</strong> am gestrigen Tage einen schicken rosa Eiswagen als Zusatzinhalt veröffentlicht hat, kommt heute ein Fahrzeug für die etwas härtere Fraktion. Demnach kann Rico Rodriguez die Insel Panau ab heute mit einem Monstertruck unsicher machen. Doch damit nicht genug.</p> '
featured_image: /wp-content/uploads/2010/04/justcause2_small.png

---
Die Entwickler spendieren dem Ding auch noch einen Granatwerfer auf dem Dach. Der Monstertruck für das Actionspiel <a href="http://www.justcause.com/" target="_blank">Just Cause 2</a> ist ab sofort für PlayStation 3, Xbox 360 und PC verfügbar.

 

   

* * *

   



<span style="font-size: 11pt; line-height: 115%; font-family: "></p> 

<p>
  </span>
</p>