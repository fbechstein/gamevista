---
title: Fallout Online – Anmeldung zur Beta gestartet
author: gamevista
type: news
date: 2010-06-22T16:46:10+00:00
excerpt: '<p>[caption id="attachment_1939" align="alignright" width=""]<img class="caption alignright size-full wp-image-1939" src="http://www.gamevista.de/wp-content/uploads/2010/06/falloutonline.jpg" border="0" alt="Fallout Online" title="Fallout Online" align="right" width="140" height="100" />Fallout Online[/caption]Publisher <strong>Interplay </strong>hat seine Pforten für die geschlossene Beta zum Online-Rollenspiel <a href="http://www.fallout-on-line.com/" target="_blank">Fallout Online</a> geöffnet. Fans der Fallout-Serie bekommen endlich die Möglichkeit sich für die Testphase anzumelden und das Spiel auf Herz und Nieren zu testen. Auf der <a href="http://www.fallout-on-line.com/" target="_blank">offiziellen Website</a> des Entwicklers kann man sich ab sofort registrieren und auf einen der begehrten Keys hoffen.</p> '
featured_image: /wp-content/uploads/2010/06/falloutonline.jpg

---
Wann genau die Beta startet wird ist bisher nicht bekannt. Auch wird über die Inhalte und Schauplätze des Endzeit-MMO\`s geschwiegen. **Interplay** hat die überaus erfolgreichen letzten drei Fallout-Titel entwickelt.

<a href="http://www.fallout-on-line.com/" target="_blank">> Zur Fallout Online &#8211; Beta Anmeldung</a>

 

   

* * *

   

