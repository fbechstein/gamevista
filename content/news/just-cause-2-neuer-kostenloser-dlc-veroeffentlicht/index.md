---
title: Just Cause 2 – Neuer kostenloser DLC veröffentlicht
author: gamevista
type: news
date: 2010-06-23T16:22:09+00:00
excerpt: '<p><strong>[caption id="attachment_1947" align="alignright" width=""]<img class="caption alignright size-full wp-image-1947" src="http://www.gamevista.de/wp-content/uploads/2010/06/justcause2_small.png" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Eidos </strong>hat heute sechs neue Mini-Zusatzinhalt für das Actionspiel <a href="http://www.justcause.com" target="_blank">Just Cause 2</a> veröffentlicht. Eines dieser DLC`s wird komplett kostenlos angeboten und trägt den Namen Tuk Tuk Boom Boom. Das Paket mit dem ungewöhnlichen Namen bringt eine wahre Massenvernichtungswaffe ins Spiel.</p> '
featured_image: /wp-content/uploads/2010/06/justcause2_small.png

---
Diese wurde auf das untermotorisierte Gefährt mit dem Namen Tuk Tuk verbaut. Außerdem gibt es noch zwei neue Waffen, Rico\`s Signature Gun und Bull\`s Eye Sniper Rifle, und zwei neue Fahrzeuge zu entdecken. Darunter ein Hovercraft von der Agency und ein Chevalier Classic. Jeweils zum Preis von 0,99 Euro zu kaufen bei <a href="http://store.steampowered.com/news/3974/" target="_blank">Steam</a>.

 

   

* * *

   

