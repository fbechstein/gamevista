---
title: Borderlands – Erster Zusatzinhalt angekündigt
author: gamevista
type: news
date: 2009-10-17T17:00:51+00:00
excerpt: '<p>[caption id="attachment_121" align="alignright" width=""]<img class="caption alignright size-full wp-image-121" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderlands_small.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Wie der Publisher <a href="http://www.2kgamesinternational.com" target="_blank">2K Games</a> per Pressemitteilung berichtet hat wird Ende des Jahres ein Download-Inhalt zum Actionspiel <strong>Borderlands </strong>erscheinen. Das Update mit dem Namen „<strong>Die Zombie Insel von Dr. Ned</strong>“ wird es für 10 Dollar zu erweben geben.</p> '
featured_image: /wp-content/uploads/2009/07/borderlands_small.jpg

---
Im ersten DLC müsst ihr mit Dr. Ned ein Heilmittel für eine Zombieseuche finden. Diese wurde von Dr. Ned versehentlich ausgelöst weil er damit beauftragt wurde die Mitarbeiter des Geschäftsmanns Jakob Cove am Leben zu halten. So werden die Arbeiter durch die Medizin in Zombies verwandelt. Weiterhin berichtet der Entwickler Gearbox Software das jede Menge neuer Quests, Gegenstände und Gegner dazu kommen werden. Der Ego-Shooter **Borderlands** erscheint am 23. Oktober 2009 für Konsole und am 30. Oktober für PC.

 

* * *



* * *

 