---
title: Risen – Hotfix für Geforcekarten Besitzer
author: gamevista
type: news
date: 2009-10-08T18:37:48+00:00
excerpt: '<p>[caption id="attachment_844" align="alignright" width=""]<img class="caption alignright size-full wp-image-844" src="http://www.gamevista.de/wp-content/uploads/2009/10/risenisland.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Der Entwickler <a href="http://www.piranha-bytes.com" target="_blank">Piranha Bytes</a> hat einen Hotfix für das Rollenspiel <strong>Risen </strong>veröffentlicht. Grund dafür sind die Grafikprobleme mit bestimmten Geforce Grafikkarten. Dabei handelt es sich um Karten wie die Geforce 6800 Ultra, 6600 GT und Geforce 7100 GS bis Geforce 7950 GX.</p> '
featured_image: /wp-content/uploads/2009/10/risenisland.png

---
Benutzer der Karten berichten über einen Grafikfehler der einen grauen Schleier auf den Bildschirm wirft. Das Update soll dies beseitigen. Wie immer gibt es den Patch bei uns im Downloadbereich.

[> Zum Risen &#8211; Hotfix Download][1]

 

* * *



* * *

 

 [1]: downloads/patches/item/patches/risen-hotfix