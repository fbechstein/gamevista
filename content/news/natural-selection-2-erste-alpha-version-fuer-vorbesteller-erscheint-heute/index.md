---
title: Natural Selection 2 – Erste Alpha-Version für Vorbesteller erscheint heute
author: gamevista
type: news
date: 2010-07-26T16:22:02+00:00
excerpt: '<p>[caption id="attachment_348" align="alignright" width=""]<img class="caption alignright size-full wp-image-348" src="http://www.gamevista.de/wp-content/uploads/2009/08/ns2_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Das Team von den Entwicklern von Unknown Worlds präsentiert heute im Verlauf des Tages die erste lauffähige Alpha-Version des zweiten Teils von Natural Selection. Der Ego-Shooter-Strategiemix soll noch dieses Jahr erscheinen. Derzeit bereiten die Entwickler die Veröffentlichung der Alpha auf der Onlineplattform Steam vor.</p> '
featured_image: /wp-content/uploads/2009/08/ns2_small.jpg

---
Die Vorbesteller der Special Edition dürfen sich heute auf folgenden spielbaren Inhalt freuen:

&#8211; Eine der spielbaren Karten ns2_tram, plus einer kleinen Testkarte    
&#8211; Marine mit Gewehr, Pistole, SwitchAx, Granatwerfer, Shotgun   
&#8211; Skulks, Gorges, Lerks mit ihren Basisfähigkeiten   
&#8211; Marine Armory zum kaufen neuer Waffen   
&#8211; Alien Evolutionsmenü zur Auswahl eurer Lebensform und Upgrades    
&#8211; Basic Marine- und Alien-Commander Interface  
&#8211; Sounds, Artworks, Animationen

Als kleinen Vorgeschmack wurde auch ein neues Video veröffentlicht. 

[> Zum Natural Selection 2 &#8211; Alpha Gameply Footage Video][1]

   

* * *

   



 [1]: videos/item/root/natural-selection-2-alpha-gameplay-footage-2