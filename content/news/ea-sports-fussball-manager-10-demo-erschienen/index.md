---
title: EA Sports Fussball Manager 10 Demo erschienen
author: gamevista
type: news
date: 2009-10-16T16:48:38+00:00
excerpt: '<p>[caption id="attachment_877" align="alignright" width=""]<img class="caption alignright size-full wp-image-877" src="http://www.gamevista.de/wp-content/uploads/2009/10/fm10_small.png" border="0" alt="EA Sports Fussball Manager 10" title="EA Sports Fussball Manager 10" align="right" width="140" height="100" />EA Sports Fussball Manager 10[/caption]Die neue Ausgabe von Deutschlands populärster Fußballmanager-Simulation erscheint am 29. Oktober 2009, doch bereits jetzt können Spieler den <strong>EA SPORTS FUSSBALL MANAGER 10</strong> testen. In der heute erschienenen Demo können virtuelle Teammanager eine halbe Saison in einer der ersten Ligen aus Deutschland, England,  Frankreich, Italien, Polen und Spanien spielen. Die entsprechenden Spielstände können später mit der Vollversion fortgesetzt werden.</p> '
featured_image: /wp-content/uploads/2009/10/fm10_small.png

---
Die Demo zum FUSSBALL MANAGER 10 enthält sämtliche verfügbare Spielberechnungsmodi, wie das 3D-Spiel, den Textmodus, den Videotext und die Sofortberechnung. So können die Spieler bereits in der Demo unter anderem das verbesserte 3D-Spiel und den Manager an der Seitenlinie testen. Darüber hinaus kann das neue User Interface mit Auflösungen von bis zu 1920*1200 und frei konfigurierbarem Schreibtisch ausprobiert werden.

Um die Downloadgröße zu minimieren, verzichtet die FUSSBALL MANAGER 10 Demo auf 3D-Spielergesichter, 3D-Kommentare und Soundeffekte, so wie auf den umfangreichen Stadioneditor.  Ebenfalls nicht enthalten sind Nationaltrainer-Modus, Create a Club, das Privatleben, so wie der erstmals im FUSSBALL MANAGER enthaltene Online-Modus.

Zu den Stärken des FUSSBALL MANAGER 10 gehören der lokale Inhalt, die Aktualität und die große Anzahl internationaler Wettbewerbe. Er enthält die lizenzierten Ligen-, Vereins- und Spielerdaten aus 44 Ländern, spielbar sind sogar 61 Länder. Die umfangreiche Spielerdatenbank beinhaltet über 3.900 Vereine und mehr als 37.000 Spieler, von denen über 9.000 mit Original-Bildern im Spiel enthalten sind. Weitere Spieler können durch den Editor hinzugefügt werden.

Der FUSSBALL MANAGER 10 wird vom Entwicklungsstudio <a href="http://www.brightfuture.de/" target="_top">Bright Future</a> in Köln für <a href="http://www.electronic-arts.de/games/easports/" target="_top">EA SPORTS </a>entwickelt. Das Spiel erscheint am 29. Oktober in sechs verschiedenen Sprachen für den PC.

[> zur EA Sports Fussball Manager 10 &#8211; Demo][1]

* * *



* * *

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=347