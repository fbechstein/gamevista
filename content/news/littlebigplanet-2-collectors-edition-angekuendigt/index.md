---
title: LittleBigPlanet 2 – Collector`s Edition angekündigt
author: gamevista
type: news
date: 2010-11-09T16:31:07+00:00
excerpt: '<p><strong>[caption id="attachment_2267" align="alignright" width=""]<img class="caption alignright size-full wp-image-2267" src="http://www.gamevista.de/wp-content/uploads/2010/09/lbp2.jpg" border="0" alt="LittleBigPlanet 2" title="LittleBigPlanet 2" align="right" width="140" height="100" />LittleBigPlanet 2[/caption]Sonys </strong>Produktmanager Alex Pavey hat bestätigt, dass der zweite Teil von <strong>LittleBigPlanet </strong>auch als Collector`s Edition erhältlich sein wird. Bisher war nur bekannt, dass die US-Fassung als Sammleredition erscheinen wird. Als Inhalt zählen eine Steelbook-Verpackung, fünf PSN-Avatare und sieben DLC-Kostüme (TRON, Toy Story, Muppets, und weitere).</p> '
featured_image: /wp-content/uploads/2010/09/lbp2.jpg

---
Jak & Daxter und Ratchet & Clank Kostüme werden auch als Pre-Order Gegenstände verfügbar sein. <a href="http://www.littlebigplanet.com/de-de/2/" target="_blank">LittleBigPlanet 2</a> wird im Januar 2011 für PlayStation 3 im Handel erscheinen.

 

 

   

* * *

   

