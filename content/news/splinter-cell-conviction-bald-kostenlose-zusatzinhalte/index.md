---
title: 'Splinter Cell: Conviction – Bald kostenlose Zusatzinhalte'
author: gamevista
type: news
date: 2010-04-15T15:57:51+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Gute Nachrichten für alle Fans von Sam Fisher aus <a href="http://www.splintercell.us.ubi.com/" target="_blank">Splinter Cell: Conviction</a>. Der Publisher <strong>Ubisoft </strong>kündigt passend zum Veröffentlichungstag der Xbox 360-Version von <a href="http://www.splintercell.us.ubi.com/" target="_blank">Splinter Cell: Conviction</a>, kostenlose Zusatzinhalte für den Taktikshooter für die nächsten Wochen an. Somit erhalten sie unter anderem bald neue Waffen und allerlei Agentenequipment.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
Außerdem sollen verschiedene Skins und Bonuskarten für den Deniable-Ops-Modus erscheinen. Laut **Ubisoft** wird die MP7A1 Maschinenpistole den Anfang bilden. <a href="http://www.splintercell.us.ubi.com/" target="_blank">Splinter Cell: Conviction</a> ist für Xbox 360 ab sofort erhältlich. Die PC-Version kommt am 29. April 2010 in den Handel.

 

   

* * *

   



<span style="font-size: 11pt; line-height: 115%; font-family: "></p> 

<p>
  </span>
</p>