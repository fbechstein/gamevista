---
title: DiRT 3 – Erstes Entwicklertagebuch erschienen
author: gamevista
type: news
date: 2010-11-15T19:11:06+00:00
excerpt: '<p>[caption id="attachment_2429" align="alignright" width=""]<img class="caption alignright size-full wp-image-2429" src="http://www.gamevista.de/wp-content/uploads/2010/11/dirt3.jpg" border="0" alt="DiRT 3" title="DiRT 3" align="right" width="140" height="100" />DiRT 3[/caption]Publisher <strong>Codemasters </strong>hat den ersten Teil der Entwicklertagebücher für den dritten Teil der Rennspielserie DiRT veröffentlicht. Zu sehen gibt es zahlreiche Spielszenen und diverse Mitarbeiter von <strong>Codemasters </strong>erklären die neuen Features des Spiels.</p> '
featured_image: /wp-content/uploads/2010/11/dirt3.jpg

---
Dabei wird unter anderem eine Rally Schule besucht, um den neuen Teil noch realistischer Entwickeln zu können.

[> Zum DiRT 3 – Entwicklertagebuch #1][1]

   

* * *

   



 [1]: videos/item/root/dirt-3--entwicklertagebuch-1