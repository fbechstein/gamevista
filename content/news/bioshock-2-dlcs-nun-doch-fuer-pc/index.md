---
title: Bioshock 2 – DLC`s nun doch für PC
author: gamevista
type: news
date: 2010-10-28T20:12:24+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Publisher <strong>2K Games </strong>hat die bisherigen Berichte revidiert, dass Zusatzinhalte für den Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> ausschließlich für Konsolen entwickelt werden würden. Laut einem <a href="http://forums.2kgames.com/forums/showthread.php?p=1254568">offiziellen Posting </a>wurde nun die Entwicklung eines neuen Patches und beider DLC`s für PC bestätigt.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Demnach wird **Protectors Trails** im Dezember und **Minerva\`s Den** im nächsten Jahr veröffentlicht. Ein genauer Termin folgt demnächst.

 

 

   

* * *

   

