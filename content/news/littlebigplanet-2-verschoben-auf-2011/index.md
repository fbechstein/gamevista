---
title: LittleBigPlanet 2 – Verschoben auf 2011
author: gamevista
type: news
date: 2010-09-23T18:48:20+00:00
excerpt: '<p>[caption id="attachment_2267" align="alignright" width=""]<img class="caption alignright size-full wp-image-2267" src="http://www.gamevista.de/wp-content/uploads/2010/09/lbp2.jpg" border="0" alt="LittleBigPlanet 2" title="LittleBigPlanet 2" align="right" width="140" height="100" />LittleBigPlanet 2[/caption]Entwickler <strong>Media Molecule</strong> hat den Titel <a href="http://www.littlebigplanet.com/de-de/2/" target="_blank">LittleBigPlanet 2</a> auf das nächste Jahr verschoben, dies wurde im Rahmen einer Pressemitteilung bestätigt. Der ursprüngliche Veröffentlichungstermin im November kann daher nicht eingehalten werden.</p> '
featured_image: /wp-content/uploads/2010/09/lbp2.jpg

---
Als neues Datum gaben die Entwickel lediglich den Januar 2011 als Zeitraum an an. Bis dahin möchte das Studio die Zeit nutzen um dem Spiel den letzten Feinschliff zu verpassen.

 

   

* * *

   

