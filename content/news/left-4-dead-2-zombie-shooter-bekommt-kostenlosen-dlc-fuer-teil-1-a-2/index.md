---
title: 'Left 4 Dead 2 – Zombie-Shooter bekommt kostenlosen DLC für Teil 1 & 2'
author: gamevista
type: news
date: 2010-08-23T17:32:10+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Entwickler <strong>Valve </strong>verkündete vor kurzem offiziell eine weitere Mini-Episode für den Zombie-Shooter <a href="http://www.l4d.com" target="_blank">Left 4 Dead 2</a> zu veröffentlichen. Das besondere daran ist, dass auch der Vorgänger Teil 1 von diesem Zusatzinhalt profitieren wird.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
**The Sacrifice** wird am 5. Oktober 2010 als Download Content für beide Titel **Left 4 Dead** und <a href="http://www.l4d.com" target="_blank">Left 4 Dead 2</a> veröffentlicht. Wie immer kommen PC-Spieler kostenlos in den Genuss, hingegen Xbox 360 Besitzer 560 Microsoft-Punkte berappen müssen. Die Geschichte der The Sacrifice-Kampagne ist in drei Karten gegliedert und soll deutlich größer ausfallen als die Crash Course oder The Passing-Kampangen.

 

   

* * *

   



 