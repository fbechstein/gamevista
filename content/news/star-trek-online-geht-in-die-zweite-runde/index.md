---
title: Star Trek Online – Geht in die zweite Runde
author: gamevista
type: news
date: 2010-07-28T18:52:54+00:00
excerpt: '<p>[caption id="attachment_915" align="alignright" width=""]<img class="caption alignright size-full wp-image-915" src="http://www.gamevista.de/wp-content/uploads/2009/10/startrek_online.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Publisher <strong>Atari </strong>und das Entwicklerteam von <strong>Cryptic </strong>haben das zweite große Update für das Online-Rollenspiel <a href="http://www.startrekonline.com" target="_blank">Star Trek Online</a> veröffentlicht. Das Season Two-Paket kann somit ab sofort kostenlos heruntergeladen werden. Inhaltlich wird unter anderem die Levelgrenze von 45 auf 51 angehoben.</p> '
featured_image: /wp-content/uploads/2009/10/startrek_online.jpg

---
Daneben gibt es neue Ränge und neue Tier 5-Schiffe zu erspielen. Außerdem könnt ihr euch über eine Highlevel-Version der Special-Task-Missionen freuen. Eine neue Spieloption mit dem Namen Federation Diplomatic Corps und fünf neue Episoden runden das Addon ab.

 

   

* * *

   

