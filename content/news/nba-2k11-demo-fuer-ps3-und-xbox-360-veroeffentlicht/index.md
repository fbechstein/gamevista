---
title: NBA 2K11 – Demo für PS3 und Xbox 360 veröffentlicht
author: gamevista
type: news
date: 2010-09-15T17:40:54+00:00
excerpt: '<p>[caption id="attachment_2225" align="alignright" width=""]<img class="caption alignright size-full wp-image-2225" src="http://www.gamevista.de/wp-content/uploads/2010/09/nba2k11.jpg" border="0" alt="NBA 2K11" title="NBA 2K11" align="right" width="140" height="100" />NBA 2K11[/caption]Basketball-Fans dürften heute ein Freudentänzchen hinlegen. Publisher <strong>2K Games</strong> hat die Demoversion von <a href="http://2ksports.com/games/nba2k11" target="_blank">NBA 2K11</a> für PlayStation 3 und Xbox 360 veröffentlicht. Die beiden Testversionen sind ab sofort über das PlayStation Network bzw. auf dem Xbox Live-Marktplatz verfügbar.</p> '
featured_image: /wp-content/uploads/2010/09/nba2k11.jpg

---
Der diesjährige Basketballtitel setzt mit Michael Jordan auf einen der wohl bekanntesten Spieler der Sportgeschichte. Zum ersten Mal dürft ihr den Superstar in zehn einzigartigen Spielen seiner Karriere selbst steuern. <a href="http://2ksports.com/games/nba2k11" target="_blank">NBA 2K11</a> wird am 8. Oktober 2010 für Xbox 360, PlayStation 3 , PC, PSP und Wii veröffentlicht. Die Demo umfasst die Finalrunde der letzten NBA Saison. Dabei treffen die Boston Celtics auf die L.A. Lakers.

 

   

* * *

   

