---
title: End of Nations – Entwickler präsentieren die Einheiten
author: gamevista
type: news
date: 2010-07-19T20:36:18+00:00
excerpt: '<p>[caption id="attachment_2028" align="alignright" width=""]<img class="caption alignright size-full wp-image-2028" src="http://www.gamevista.de/wp-content/uploads/2010/07/endofnations.jpg" border="0" alt="End of Nations" title="End of Nations" align="right" width="140" height="100" />End of Nations[/caption]<strong>Petroglyph Games</strong> zeigt heute ein weiteres Entwicklervideo zum Online-Strategiespiel <a href="http://www.endofnations.com" target="_blank">End of Nations</a>. Der Inhalt der zweiten Episode sind diesmal die verschiedenen Einheiten und Fähigkeiten.</p> '
featured_image: /wp-content/uploads/2010/07/endofnations.jpg

---
<a href="http://www.endofnations.com" target="_blank"><br />End of Nations</a> legt dabei sehr viel Wert auf das Zusammenspiel der einzelnen Kommandeure. Das Strategiespiel erscheint voraussichtlich 2011. Neue Eindrücke dürft ihr auf der diesjährigen gamescom in Köln sammeln. Dort wird <a href="http://www.endofnations.com" target="_blank">End of Nations</a> erstmals von Petroglyph Games vorgestellt.

 

[> Zum End of Nations &#8211; Unit Trailer][1]

   

* * *

   



 [1]: videos/item/root/end-of-nations-unit-trailer