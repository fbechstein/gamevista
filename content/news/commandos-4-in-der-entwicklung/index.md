---
title: Commandos 4 – In der Entwicklung
author: gamevista
type: news
date: 2009-08-24T10:09:28+00:00
excerpt: '<p>[caption id="attachment_478" align="alignright" width=""]<img class="caption alignright size-full wp-image-478" src="http://www.gamevista.de/wp-content/uploads/2009/08/commandos2_bi.jpg" border="0" alt="Commandos" title="Commandos" align="right" width="140" height="100" />Commandos[/caption]Die gamescom ist noch gar nicht solange her, man denkt was kann an einem Montag morgen schon noch neues veröffentlicht werden. Da schwappt eine Mitteilung von den Entwicklern von <a href="http://www.pyrostudios.com">Pyro Studios</a> in die Medienlandschaft die einige Glücksgefühle auslösen dürften. Denn dieser teilten nun mit das sie momentan am nunmehr vierten Teil von <strong>Commandos </strong>arbeiten.</p> '
featured_image: /wp-content/uploads/2009/08/commandos2_bi.jpg

---
Wie das neue Commandos aussehen wird ist bisher noch nicht bekannt, aber [Pyro Studios][1] sagt nur soviel das sie sich an den Perspektiven vom zweiten Teil halten wollen. Das heißt wenig 3D und viel isometrische Ansicht. Wann ein Release in Sicht ist war bisher nicht rauszufinden.

* * *



* * *

 

 [1]: http://www.pyrostudios.com