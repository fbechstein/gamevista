---
title: 'Die Kunst des Mordens: Karten des Schicksals – Demo veröffentlicht'
author: gamevista
type: news
date: 2010-01-27T17:43:35+00:00
excerpt: '<p>[caption id="attachment_1371" align="alignright" width=""]<img class="caption alignright size-full wp-image-1371" src="http://www.gamevista.de/wp-content/uploads/2010/01/kdm3_small.png" border="0" alt="Die Kunst des Mordens 3" title="Die Kunst des Mordens 3" align="right" width="140" height="100" />Die Kunst des Mordens 3[/caption]Der Publisher <strong>City Interactive</strong> hat kurz vor der Veröffentlichung des Adventures <strong>Die Kunst des Mordens 3: Karten des Schicksals</strong>, eine Demo veröffentlicht. Die Anspielversion mit einer Größe von 1 Gigabyte lässt sie die FBI Agentin Nicole Bonnet erste Fälle lösen. Die Demo findet ihr wie immer im Downloadbereich auf <strong>Gamevista.de</strong>.</p> '
featured_image: /wp-content/uploads/2010/01/kdm3_small.png

---
Die **Kunst des** **Mordens 3: Karten des Schicksals** erscheint am Donnerstag den 28. Januar 2010 im deutschen Handel.

**<span style="text-decoration: underline;">Features:</span>**

-Durchlebe eine düstere Handlung voller mysteriöser Wendungen  
-Löse herausfordernde Rätsel, die logisches Denken erfordern  
-Finde und analysiere Beweisstücke  
-Entdecke detaillierte Schauplätze aus verschiedenen Orten in den USA

[> Zum Die Kunst des Mordens 3: Karten des Schicksals &#8211; Demo Download][1]

* * *



* * *

 [1]: downloads/demos/item/demos/die-kunst-des-mordens-3-demo