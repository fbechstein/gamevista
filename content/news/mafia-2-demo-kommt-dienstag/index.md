---
title: Mafia 2 – Demo erscheint am Dienstag
author: gamevista
type: news
date: 2010-08-09T16:32:04+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]<strong>2K Games</strong> gab heute bekannt, dass die Demo zum Actionspiel <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> bereits morgen, dem 10. August 2010, erscheint. Über die Online-Plattform Steam wird die PC-Version des lang erwarteten zweiten Teils am Dienstag zum Download angeboten.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Für alle Konsolenbesitzer wird zugleich auf dem Xbox LIVE Marktplatz bzw. dem PlayStation Network die Demo veröffentlicht. Die finale Version von <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> wird am 27. August 2010 im Handel erscheinen.

 

   

* * *

   

