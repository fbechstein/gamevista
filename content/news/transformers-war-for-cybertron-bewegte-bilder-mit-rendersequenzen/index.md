---
title: 'Transformers: War for Cybertron – Neuer Trailer erschienen'
author: gamevista
type: news
date: 2010-01-15T14:00:00+00:00
excerpt: '<p style="text-align: justify;"> </p> <p>[caption id="attachment_1181" align="alignright" width=""]<img class="caption alignright size-full wp-image-1181" src="http://www.gamevista.de/wp-content/uploads/2009/12/transformers_warforcybertron.jpg" border="0" alt="Transformers: WAr for Cybertron" title="Transformers: WAr for Cybertron" align="right" width="140" height="100" />Transformers: WAr for Cybertron[/caption]Der Publisher <strong>Activision Blizzard</strong> hat ein neues Video zum kommenden Actionspiel <a href="http://www.transformersgame.com" target="_blank">Transformers: War for Cybertron</a> veröffentlicht. Der zweiminütige Trailer zeigt cineastische Rendersequenzen um den Kampf auf dem Planten Cybertron. Hier bekriegen sich Autobots und Decepticons.</p> '
featured_image: /wp-content/uploads/2009/12/transformers_warforcybertron.jpg

---
<a href="http://www.transformersgame.com" target="_blank">Transformers: War for Cybertron</a> wird zeitlich vor den Filmen spielen und die Kriege auf dem Heimatplaneten der Autobots erzählen. Bisher ist nicht genau klar wann das Actionspiel erscheinen soll. Bisher gibt es nur die Aussage vom zweiten Quartal 2010.

 

<p style="text-align: justify;">
  <a href="videos/item/root/transformers-war-for-cybertron-render-trailer">> Zum Transformers: War for Cybertron &#8211; Render Trailer</a>
</p>

<cite></cite>

* * *



* * *

 