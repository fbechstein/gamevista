---
title: Diablo 3 – Auf der Gamescom 2010 testen
author: gamevista
type: news
date: 2010-08-02T16:40:36+00:00
excerpt: '<p>[caption id="attachment_456" align="alignright" width=""]<img class="caption alignright size-full wp-image-456" src="http://www.gamevista.de/wp-content/uploads/2009/08/diablo3_small.jpg" border="0" alt="Diablo 3" title="Diablo 3" align="right" width="140" height="100" />Diablo 3[/caption]Der Entwickler <strong>Blizzard </strong>wird auf der Gamescom 2010 in Köln, die vom 19.08. bis 22.08.2010 stattfindet, den dritten Teil der Diablo-Reihe präsentieren. Laut einer Meldung aus dem offiziellen Forum wurde einem Fan des Spiels nahegelegt, auf der Gamescom den Diablo 3-Stand zu besuchen, auf die Frage hin ob man Diablo 3 dort antesten könne.</p> '
featured_image: /wp-content/uploads/2009/08/diablo3_small.jpg

---
Dies ist zwar keine definitive Zusage das <a href="http://us.blizzard.com/diablo3/" target="_blank">Diablo 3</a> auch anspielbar sein wird. Trotzdem wird **Blizzard** sicher mehr als nur Trailer und Bilder für ihr kommendes Action-Rollenspiel zeigen.

 

   

* * *

   

