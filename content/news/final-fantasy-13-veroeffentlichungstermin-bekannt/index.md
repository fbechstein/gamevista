---
title: Final Fantasy 13 – Veröffentlichungstermin und Video
author: gamevista
type: news
date: 2009-11-19T20:15:44+00:00
excerpt: '<p>[caption id="attachment_1073" align="alignright" width=""]<img class="caption alignright size-full wp-image-1073" src="http://www.gamevista.de/wp-content/uploads/2009/11/ff13.jpg" border="0" alt="Final Fantasy XIII" title="Final Fantasy XIII" align="right" width="140" height="100" />Final Fantasy XIII[/caption]Am Freitag den 13. November 2009 wurde bereits bekannt gegeben das <strong>Final Fantasy XIII </strong>am 9. März 2010 für PlayStation 3 und Xbox 360 erscheint. Nun hat der Entwickler <a href="http://www.finalfantasy13game.com/" target="_blank">Square Enix</a> ein umfassendes Video zum Rollenspiel veröffentlicht. Der knapp fünf Minuten lange Trailer zeigt viele Spielszenen und Zwischensequenzen. Das Ganze wird kommentiert vom Entwicklerteam rund um <a href="http://www.finalfantasy13game.com/" target="_blank">Square Enix</a>.</p> '
featured_image: /wp-content/uploads/2009/11/ff13.jpg

---
[> Zum Final Fantasy XIII &#8211; Announcement Trailer][1]

 







 

 [1]: videos/item/root/final-fantasy-xiii-announcement-trailer