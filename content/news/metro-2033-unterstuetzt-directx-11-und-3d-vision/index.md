---
title: Metro 2033 – Unterstützt DirectX 11 und 3D Vision
author: gamevista
type: news
date: 2010-02-17T20:18:43+00:00
excerpt: '<p>[caption id="attachment_1346" align="alignright" width=""]<img class="caption alignright size-full wp-image-1346" src="http://www.gamevista.de/wp-content/uploads/2010/01/metro_2033.jpg" border="0" alt="Metro 2033" title="Metro 2033" align="right" width="140" height="100" />Metro 2033[/caption]Der Publisher THQ hat nun offiziell bekannt gegeben das der russische Grusel-Shooter Metro 2033, die neuste DirectX 11 Funktionen nutzt und Nvidias 3D Visionen unterstützt.<em> „Unsere Techniker haben sehr eng mit dem Entwicklerstudio 4A Games zusammenarbeitet, um zu garantieren, dass Metro 2033 diese spektakulären Effekte der neuen DX11-Hardwaregeneration ausnutzt", </em>erklärt Tony Tamasi, Senior Vice President of Content and Technology bei NVIDIA.</p> <p /><em /><br />  '
featured_image: /wp-content/uploads/2010/01/metro_2033.jpg

---
„Die 4A-Grafikengine ist eine der fortschrittlichsten Grafikroutinen, mit denen wir je gearbeitet haben. Mit aktivierten DX11-Effekten ist Metro 2033 zweifelsfrei eines der optisch beeindruckendsten PC-Spiele im Jahr 2010. Zusammen mit NVIDIA 3D Vision und PhysX erleben Sie eine atemberaubende Optik.&#8220;  
</em>

<p style="text-align: center;">
  <img class="caption size-full wp-image-1458" src="http://www.gamevista.de/wp-content/uploads/2010/02/metro2033d11.jpg" border="0" alt="Metro 2033 Unterstützt DirectX 11 und Nvidias 3D Vision" title="Metro 2033 Unterstützt DirectX 11 und Nvidias 3D Vision" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/metro2033d11.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/metro2033d11-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

