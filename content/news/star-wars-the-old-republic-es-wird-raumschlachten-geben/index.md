---
title: 'Star Wars: The Old Republic – Es wird Raumschlachten geben!'
author: gamevista
type: news
date: 2010-07-25 19:09:34 +0000
excerpt: " Das Entwicklerteam von BioWare hat im Rahmen der Comi-Con-Messe angekündigt,
  dass in dem Online-Rollenspielk Star Wars: The Old Republic auch Raumschlachten
  möglich sein werden. Bisher ging man davon aus, dass es nur Missionen auf Planten
  geben wird. Nun wird man die Möglichkeiten haben auch im Weltall sein Unwesen zu
  treiben."
featured_image: "/wp-content/uploads/2009/07/wp_20090626_deceived01_1280x960_small.jpg"

---
 Das Entwicklerteam von BioWare hat im Rahmen der Comi-Con-Messe angekündigt, dass in dem Online-Rollenspielk Star Wars: The Old Republic auch Raumschlachten möglich sein werden. Bisher ging man davon aus, dass es nur Missionen auf Planten geben wird. Nun wird man die Möglichkeiten haben auch im Weltall sein Unwesen zu treiben.

Laut einem offiziellen Foren-Posting beschreibt Bioware-Mitarbeiter SeanDahlberg die Weltraummissionen so:

„Der Weltraumkampf soll eine alternative Spielerfahrung darstellen. Das Hauptaugenmerk bleibt aber bei den Bodenquests auf den Planeten. Im All könnt ihr euer persönliches Raumschiff zu verschiedenen Hotspots, die auf der Galaxiekarte zu finden sind, fliegen. Von dort aus schießt ihr euren Weg durch Asteroidenfelder, feindliche Jäger, Fregatten, Zerstörer. Die typischen Star Wars-Orte dürfen da natürlich nicht fehlen.