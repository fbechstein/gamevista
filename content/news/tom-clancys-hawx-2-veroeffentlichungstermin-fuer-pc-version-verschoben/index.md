---
title: Tom Clancy’s H.A.W.X. 2 – Veröffentlichungstermin für PC-Version verschoben
author: gamevista
type: news
date: 2010-08-20T16:43:39+00:00
excerpt: |
  |
    <p>[caption id="attachment_2000" align="alignright" width=""]<img class="caption alignright size-full wp-image-2000" src="http://www.gamevista.de/wp-content/uploads/2010/07/hawx2_s_003.jpg" border="0" alt="Tom Clancy's H.A.W.X. 2" title="Tom Clancy's H.A.W.X. 2" align="right" width="140" height="100" />Tom Clancy's H.A.W.X. 2[/caption]Publisher <strong>Ubisoft </strong>hat den Release-Termin für die PC-Version des Actionspiels <a href="http://hawxgame.uk.ubi.com/" target="_blank">Tom Clancy's H.A.W.X. 2</a> um einen Monat verschoben. Bisher war eine Veröffentlichung am 3. September 2010 geplant. Nun erscheint der Titel erst am 1. Oktober 2010. Einen Grund für die Verschiebung nannte der Publisher nicht.</p>
featured_image: /wp-content/uploads/2010/07/hawx2_s_003.jpg

---
Die Xbox 360-Version wird wie geplant Anfang September in den Handel kommen.

   

* * *

   



 