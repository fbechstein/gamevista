---
title: FIFA 11 – Demo erscheint gleichzeitig mit PES 2011
author: gamevista
type: news
date: 2010-09-05T09:31:55+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="FIFA 11" title="FIFA 11" align="right" width="140" height="100" />FIFA 11[/caption]Publisher <strong>Electronic Arts</strong> wird die Demo der diesjährige FIFA-Version <a href="http://fifa.easports.com/dede/home.action" target="_blank">FIFA 11</a> am 15. September 2010 veröffentlichen. Am selben Tag erscheint auch die Demo des Fussballspiels <strong>Pro Evolution Soccer  2011</strong> von Konami. Damit können Fans dieses Jahr den direkten Vergleich beider Spiele ziehen.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Wer also noch nicht in den Genuss der Testversionen auf der Spielemesse gamescom 2010 gekommen ist, hat ab nächster Woche die Chance dazu. Die spielbaren Mannschaften der <a href="http://fifa.easports.com/dede/home.action" target="_blank">FIFA 11</a> Demo sind FC Chelsea, **FC** Barcelona, Real Madrid, Juventus Turin, Bayer Leverkusen und Olympique Lyon.

 

   

* * *

   

