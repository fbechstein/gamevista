---
title: 'Call of Juarez: Bound in Blood – Old West Map-Pack'
author: gamevista
type: news
date: 2009-08-04T09:31:42+00:00
excerpt: '<p>[caption id="attachment_270" align="alignright" width=""]<a href="http://www.ubi.com" target="_blank" title="ubisoft"><img class="caption alignright size-full wp-image-270" src="http://www.gamevista.de/wp-content/uploads/2009/08/call_of_juarez_small.jpg" border="0" alt="Call of Juarez: Bound in Blood" title="Call of Juarez: Bound in Blood" align="right" width="140" height="100" />Ubisoft </a>ubisoft[/caption]kündigte vor ein paar Wochen ganze drei Download Pakete für ihren Wild-West-Shooter Call of <strong>Juarez: Bound in Blood</strong> an. Nun wurde der Name der ersten Erweiterung bekannt. Das "<strong>Old West Mappack</strong>" wird unter anderem vier neue Mehrspieler Karten mit sich bringen. Außerdem werdet ihr die Möglichkeit bekommen an mehreren historischen Ereignissen der amerikanischen Geschichte teilzunehmen. </p>'
featured_image: /wp-content/uploads/2009/08/call_of_juarez_small.jpg

---
Hier eine kurze Erklärung für die vier neuen Karten:

<p class="MsoNormal">
  &#8211; <strong>Little Bighorn</strong>: Ein offenes Gelände, das die berühmte Schlacht von Little Bighorn (25 &#8211; 26. Juni 1876) rekonstruiert. Damals wurden Generals Custer&#8217;s Truppen von US-amerikanischen Indianern  neutralisiert.<br /> &#8211; <strong>Vulture Mine</strong>: Basierend auf einen Diebstahl von Gold aus einer Mine, müssen die Räuber das Depot, in dem das Gold versteckt ist, aufbrechen und zerstören, während die anderen den Aufzug in die Luft jagen, um entkommen zu können.<br /> &#8211; <strong>Fort Smith</strong>: Um aus diesem Gefängnis zu fliehen, müssen die Räuber versuchen, die Gefängnismauer durchzubrechen, den Alarm des Sheriff&#8217;s auszulösen und dann versuchen zu entkommen.<br /> &#8211; <strong>Elmira Prison Camp</strong>: Auf dieser Map wird die spektakuläre Flucht aus dem Unions Bürgerkriegs Gefängnis vom 7. Oktober 1864 rekonstruiert. Häftlinge müssen sich im Camp freikämpfen, um einen Weg zur Waffenkammer zu finden um an Transportmittel zu kommen.
</p>

Erscheinen wird das ganze bereits am 06. August 2009 auf dem Playstation Network oder dem Xbox Live Marktplatz[  
][1] 







<p class="MsoNormal">
   
</p>

 [1]: videos/alphaindex/a