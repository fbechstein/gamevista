---
title: Risen – Inhalt der Collectors Edition veröffentlicht
author: gamevista
type: news
date: 2009-08-20T11:44:58+00:00
excerpt: '<p>[caption id="attachment_420" align="alignright" width=""]<img class="caption alignright size-full wp-image-420" src="http://www.gamevista.de/wp-content/uploads/2009/08/risentier.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Entwickler <a href="http://www.piranha-bytes.com">Piranha Bytes</a> veröffentlichte nun den Inhalt der Collectors Edition für ihr Rollenspiel <strong>Risen</strong>. Diese streng limitierte Sonderedition bietet euch einige Extras wie zum Beispiel eine Making-of-DVD die viel Material über die Entwicklungzeit von <strong>Risen </strong>enthält.</p> '
featured_image: /wp-content/uploads/2009/08/risentier.png

---
Weiterhin wird die Collectors Edition unter anderem auch einen Soundtrack und ein A1 Poster, das von den Entwicklern unterzeichnet wurde, enthalten. Dann wird noch eine Karte der Insel Faranga und ein Ghul Stickerset beiliegen. Als Highlight dürfte aber der Zugangscode zu einer bisher noch nicht bekannten Beta gelten. Wer sich die Collectors Edition holen möchte muss mit 10 Euro Aufpreis rechnen.

 

 