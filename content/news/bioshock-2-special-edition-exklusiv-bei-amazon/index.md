---
title: Bioshock 2 – Special Edition exklusiv bei Amazon
author: gamevista
type: news
date: 2010-01-15T20:51:40+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Ab sofort wird es möglich sein eine Special Edition des Ego-Shooters <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> beim Internethandel <a href="http://www.amazon.de/BioShock-Special-exklusiv-bei-Amazon/dp/B002Z7EEJ4/ref=sr_1_1?ie=UTF8&s=videogames&qid=1263588830&sr=1-1" target="_blank">Amazon.de</a> vorzubestellen. Der Publisher<strong> 2K Games </strong>und <a href="http://www.amazon.de/BioShock-Special-exklusiv-bei-Amazon/dp/B002Z7EEJ4/ref=sr_1_1?ie=UTF8&s=videogames&qid=1263588830&sr=1-1" target="_blank">Amazon.de</a> haben dabei einen Exklusivvertrag für den Vertrieb der <a href="http://www.amazon.de/BioShock-Special-exklusiv-bei-Amazon/dp/B002Z7EEJ4/ref=sr_1_1?ie=UTF8&s=videogames&qid=1263588830&sr=1-1" target="_blank">Special Edition</a> geschlossen. Diese kann nun für 80,99 EUR vorbestellt werden.</p> <p> </p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Neben dem Spiel selbst wird die <a href="http://www.amazon.de/BioShock-Special-exklusiv-bei-Amazon/dp/B002Z7EEJ4/ref=sr_1_1?ie=UTF8&#038;s=videogames&#038;qid=1263588830&#038;sr=1-1" target="_blank">Special Edition</a> ein 164-seitiges Artbook, drei Deco-Poster von Rapture, den Soundtrack auf CD und als besonderes Extra eine Schallplatte mit der Musik des ersten Teils enthalten.

 

 

<cite></cite>

* * *



* * *

 