---
title: Assassin´s Creed 2 – Neue Limited Edition?
author: gamevista
type: news
date: 2009-07-28T12:41:04+00:00
excerpt: '<p>[caption id="attachment_112" align="alignright" width=""]<a href="http://www.ubi.com" target="_blank" title="ubisoft"><img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassins Creed 2" title="Assassins Creed 2" align="right" width="140" height="100" />Ubisoft </a>ubisoft[/caption]bot für das kommende <strong>Assassin´s Creed 2</strong> eine Limited Edtion namens „Black Box“ <span> </span>an. Diese Sammler Edition war anscheinend so begehrt das sie nach kurzer Zeit ausverkauft war. Die Black Box enthält neben einem Artwork Buch, auch einen Soundtrack und 3 Bonus Missionen. <a href="http://www.ubi.com" target="_blank" title="ubisoft">Ubisoft </a>dachte sich aber das reiche nicht für eine Limited Edition und packte eine Sammler Figur von Enzio, dem neuen Helden des Spiels hinzu. </p>'
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Außerdem findet ihr noch eine Video DVD in dem Paket.Nun wird von diversen Quellen über eine neue Limited Edition berichtet. Diese soll wie nicht anders zu erwarten war, den Namen White Box haben. Auch hier gilt, wer schnell genug zugreift kann eine dieser edlen Sammlerstücke für sich ergattern. Die White Box wird nicht den gleichen Inhalt wie ihr Protegé die Black Box haben. Leider wird nur 1 Bonus Mission, Santa Maria Die Frari in Venedig, enthalten sein und eine andere Enzio Figur wird dem Paket beiliegen. Enzio hat hier eine weiße anstatt schwarze Kopfbedeckung. Ob es noch weitere Änderungen gibt? Wir halten euch auf dem laufenden.







 