---
title: Borderlands – GOTY-Edition bald auch für Mac
author: gamevista
type: news
date: 2010-11-18T19:26:21+00:00
excerpt: '<p><strong>[caption id="attachment_2441" align="alignright" width=""]<img class="caption alignright size-full wp-image-2441" src="http://www.gamevista.de/wp-content/uploads/2010/11/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Gearbox </strong>hat angekündigt, dass die <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> Game of the Year Edition bald auch für Mac veröffentlicht wird. Am 3. Dezember 2010 wird damit der Shooter auch auf den Apple-Rechnern verfügbar sein.</p> '
featured_image: /wp-content/uploads/2010/11/smalllogo.jpg

---
Wie auch die PC-Variante wird die Game of the Year Edition sämtliche veröffentlichte DLC\`s beinhalten sowie auch die Karte von Pandora.

 

 

   

* * *

   

