---
title: 'Scorpion: Disfigured – Demo veröffentlicht'
author: gamevista
type: news
date: 2009-07-28T14:04:36+00:00
excerpt: '<p>[caption id="attachment_188" align="alignright" width=""]<img class="caption alignright size-full wp-image-188" src="http://www.gamevista.de/wp-content/uploads/2009/07/scorpion_disfigured_small.jpg" border="0" alt="Sscorpion: Disfigured" title="Sscorpion: Disfigured" align="right" width="140" height="100" />Sscorpion: Disfigured[/caption]Entwickler <a href="http://www.scorpionthegame.com" target="_blank" title="b-cool">B-Cool</a> Interactive veröffentlichte Heute die Demo zu seinem seit mehr als 3 Monaten erhältlichen Ego Shooters <strong>Scorpion: Disfigured</strong>. Die Demo umfasst 640 MB und lässt sie in die Rolle eines Agenten, der im Auftrag einer Geheimorganisation arbeitet, in einen Industriekomplex eindringen. </p>'
featured_image: /wp-content/uploads/2009/07/scorpion_disfigured_small.jpg

---
Dort sollen sie fiese Machenschaften aufklären, alles dreht sich um einen Virus der per Genexperiment aus wehrlosen Menschen, willenlose Kampfmaschinen machen soll.Ihr Auftrag ist das ganze schnell zu beenden, mit Hilfe eines Prototyp Kampfanzugs und einer Wissenschaftlerin die von dort fliehen konnte. Wer Lust hat sich dies anzuschauen, hier könnt ihr die Demo downloaden.

[  
> zur Scorpion: Disfigured Demo][1]







 

 [1]: downloads/demos/item/demos/scorpion-disfigured-demo "Scorpion: Disfigured – Demo"