---
title: 'Call of Duty: Modern Warfare 2 – Erscheint ungeschnitten!'
author: gamevista
type: news
date: 2009-09-25T17:44:40+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Wie <a href="http://www.infinityward.com">Infinity Ward</a> und <a href="http://www.activisionblizzard.com">Activision Blizzard</a> heute per offizieller Pressemitteilung bekannt gaben wird <strong>Call of Duty: Modern Warfare 2</strong> in Deutschland ungeschnitten auf den Markt kommen. Der lang erwartete Egoshooter wird damit am 10. November für Xbox 360, PlayStation 3 und PC veröffentlicht und das ganz ohne eingriffe der USK.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Denn die Unterhaltungssoftwareselbstkonrolle gab **Modern Warfare 2** „Keine Jugendfreigabe“.   
Weiterhin wird die deutsche Version wohl 1 zu 1 mit der US Version kompatibel sein, d.h. das jeder einzelne Level der US Version ungekürzt auch in der deutschen Version vorhanden sein wird. Auch wurde sehr viel wert auf die Lokalisierung gelegt.  „Dank der aufwändigen Übersetzung werden die deutschen Spieler sämtliche militärischen Dialoge und Anweisungen auch in der Hitze des Gefechts problemlos verstehen. So bekommen die deutschen Spieler 100 % Spannung – mit einer perfekten deutschen Sprachausgabe“, so Robert Bowling, Creative Strategist von [Infinity Ward][1].

 

* * *



* * *

 

 [1]: http://www.infinityward.com