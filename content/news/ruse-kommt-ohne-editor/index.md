---
title: R.U.S.E. – Wird ohne Editor veröffentlicht
author: gamevista
type: news
date: 2010-01-25T16:18:47+00:00
excerpt: '<p>[caption id="attachment_1362" align="alignright" width=""]<img class="caption alignright size-full wp-image-1362" src="http://www.gamevista.de/wp-content/uploads/2010/01/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E. erscheint ohne Editor" align="right" width="140" height="100" />R.U.S.E. erscheint ohne Editor[/caption]Der Publisher <strong>Ubisoft </strong>hat im Rahmen einer Frage- und Antwortrunde, im <a href="http://forums.ubi.com/eve/forums/a/tpc/f/4821071447/m/9551095508/p/1" target="_blank">offiziellen Forum</a>, neue Details zum Strategiespiel <a href="http://www.ruse.ubi.com/" target="_blank">R.U.S.E.</a> verkündet. So wird der Titel leider ohne einen Karten Editor auskommen müssen. Auch diverse Modding-Tools sind nicht geplant.</p> '
featured_image: /wp-content/uploads/2010/01/ruse_small.jpg

---
Den Anlass dafür liefert der Community Manager von **Ubisoft**. Aufgrund der Kartengröße stellt dies eine unüberwindbare Herausforderung für das Entwicklerteam dar. 

“The gaming community has talent, that’s an undeniable fact and we all love mods. However when it comes to the vastness of the maps we are talking about in R.U.S.E., things tend to be a little bit tougher and it makes the development process of a mapping tool a real challenge. So sadly there is no plan to include a mapping tool when the game will hit shelves. But you may not need it considering the thousands square kilometers waiting for you,” berichtet Ubisoft. 

Bisher ist nicht genau klar wann <a href="http://www.ruse.ubi.com/" target="_blank">R.U.S.E.</a> erscheinen wird. Vor kurzem wurde der Titel auf das nächste Geschäftsjahr, das am 1. April 2010 beginnt, verschoben.

* * *



* * *