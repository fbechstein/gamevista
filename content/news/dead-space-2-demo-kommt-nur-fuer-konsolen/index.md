---
title: Dead Space 2 – Demo kommt nur für Konsolen
author: gamevista
type: news
date: 2010-12-09T14:50:30+00:00
excerpt: '<p>[caption id="attachment_1151" align="alignright" width=""]<img class="caption alignright size-full wp-image-1151" src="http://www.gamevista.de/wp-content/uploads/2009/12/deadspace2.jpg" border="0" alt="Dead Space 2" title="Dead Space 2" align="right" width="140" height="100" />Dead Space 2[/caption]Publisher <strong>Electronic Arts</strong> hat nun offiziell bestätigt, dass es für den Grusel-Shooter <a href="http://www.deadspace.ea.com/" target="_blank">Dead Space 2</a> keine PC-Demo geben wird. Denn noch vor den Feiertagen werden Konsolenbesitzer in den Genuss einer Testversion des  Spiels kommen. Am 21. Dezember 2010 dürfen Xbox 360-Besitzer den neusten Teil anspielen.</p> '
featured_image: /wp-content/uploads/2009/12/deadspace2.jpg

---
PlayStation 3-Nutzer dürfen sich einen Tag später, am 22. Dezember 2010, über ihre Demo freuen. Bisher gibt es keinen Termin für eine PC-Demo.

 

   

* * *

   

