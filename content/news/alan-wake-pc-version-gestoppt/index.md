---
title: Alan Wake PC-Version gestoppt
author: gamevista
type: news
date: 2009-07-15T12:52:41+00:00
excerpt: '<p>[caption id="attachment_53" align="alignright" width=""]<img class="caption alignright size-full wp-image-53" src="http://www.gamevista.de/wp-content/uploads/2009/07/alan_small1.png" border="0" alt="Alan Wake - ob eine PC Version jemals erscheinen wird ist fraglich" title="Alan Wake - ob eine PC Version jemals erscheinen wird ist fraglich" align="right" width="140" height="100" />Alan Wake - ob eine PC Version jemals erscheinen wird ist fraglich[/caption]Anfang Juni wurde der Release Termin für <strong>Alan Wake</strong> genannt. Es wird im Frühjahr 2010 erscheinen. Auf Nachfrage des finnischen Magazins Pelaaja, verriet der Entwickler (Remedy), dass dies nur für die Xbox 360-Version gilt. </p>'
featured_image: /wp-content/uploads/2009/07/alan_small1.png

---
Remedy veröffentlichte nun weitere Informationen: So ist ein zeitgleicher Release ausgeschlossen, man hätte durchaus Interesse daran die PC-Version weiter zu entwickeln, arbeite aber momentan exklusiv an der Xbox 360-Version und die Entscheidung über die PC-Version liege einzig beim Publisher, welcher Microsoft ist.

Dieser scheint das Ziel der erst im Februar gegründeten „PC Gaming Alliance“ bereits wieder vergessen zu haben. Eigentlich wollte man den PC als Spieleplattform wieder stärken.  
Nach der Schließung mehrere Mircosoft Games Studios, die an PC-Spielen gearbeitet haben, scheint nun auch das Spiel Alan Wake, diesem Einschnitt zum Opfer gefallen zu sein. 

[> Alan Wake Screenshots anschauen][1]   
[> Alan Wake Videos anschauen][2]







 [1]: screenshots/item/screenshots/alan-wake
 [2]: videos/alphaindex/a