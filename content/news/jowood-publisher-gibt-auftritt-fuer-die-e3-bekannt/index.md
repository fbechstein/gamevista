---
title: JoWood – Publisher gibt Auftritt für die E3 bekannt
author: gamevista
type: news
date: 2010-05-11T17:59:54+00:00
excerpt: '<p>[caption id="attachment_1789" align="alignright" width=""]<img class="caption alignright size-full wp-image-1789" src="http://www.gamevista.de/wp-content/uploads/2010/05/burg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Die in Los Angeles stattfindende Spielemesse E3 ist eines der ganz großen Events im Spiele-Genre. Grund genug das die ersten Publisher ihre Spiele die sie präsentieren wollen bekannt geben. So auch <a href="http://www.jowood.com" target="_blank">JoWood</a>, denn dieser hat am heutigen Tag das offizielle Lineup veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/05/burg_small.png

---
Neben dem eindrucksvollen Rollenspiel **Arcania: Gothic 4**, werden auch die Erweiterungspakete **Die Gilde 2: Renaissance** und **Spellforce 2: Faith in Destiny** vorgestellt.   
Die diesjährige E3 findet vom 15. bis 17. Juni 2010 in Los Angeles statt.

 

 

   

* * *

   

