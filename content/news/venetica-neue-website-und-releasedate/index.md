---
title: Venetica – neue Website und Releasedate
author: gamevista
type: news
date: 2009-07-21T21:49:28+00:00
excerpt: '<p>[caption id="attachment_96" align="alignright" width=""]<img class="caption alignright size-full wp-image-96" src="http://www.gamevista.de/wp-content/uploads/2009/07/venetica_small.png" border="0" alt="Venetica kommt mit einer neuen Website" title="Venetica kommt mit einer neuen Website" align="right" width="140" height="100" />Venetica kommt mit einer neuen Website[/caption]Der Publisher <a href="http://www.dtp-entertainment.com/" target="_blank">DTP-Entertainment</a> hat heute neben dem Relaunch der neuen <a href="http://www.venetica-game.com/" target="_blank">Venetica-Internetseite,</a> ebenfalls die Veröffentlichungstermine der <strong>Venetica</strong> PC- und Xbox 360-Versionen bekanntgegeben.</p>'
featured_image: /wp-content/uploads/2009/07/venetica_small.png

---
Demnach erscheint das Spiel für den PC am 4. September 2009, die Xbox 360 &#8211; Version ist dann einen Monat später dran, nämlich am 2. Oktober 2009. Wer genau aufgepasst hat wird sofort merken, dass ebenfalls am 2. Oktober 2009 Piranha Bytes **Risen** erscheinen wird. Wir können uns somit auf einen wahrhaftigen RPG-Spätsommer freuen. 





