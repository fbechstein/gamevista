---
title: Capcom – Vier Neuankündigungen auf der TGS 2010
author: gamevista
type: news
date: 2010-09-13T18:12:52+00:00
excerpt: '<p>[caption id="attachment_343" align="alignright" width=""]<img class="caption alignright size-full wp-image-343" src="http://www.gamevista.de/wp-content/uploads/2009/08/capcom.jpg" border="0" alt="Capcom" title="Capcom" align="right" width="140" height="100" />Capcom[/caption]Publisher <a href="http://www.capcom.com" target="_blank">Capcom</a> wird auf der bald startenden Spielemesse <strong>Tokyo Game Show 2010</strong> weitere Titel ausstellen als bisher bekannt. So mancher Fan war von dem bisherigen Lineup des Herstellers ein wenig enttäuscht, dürfte nun aber äußerst gespannt sein. Bisher wurden lediglich fünf Spiele auf die Messe in Tokyo geschickt, darunter u.a.  <strong>Bionic Commando Rearmed 2</strong> und <strong>Marvel vs. Capcom 3.</strong></p> '
featured_image: /wp-content/uploads/2009/08/capcom.jpg

---
 

Der Publisher möchte nun seine Liste der ausgestellten Titel um vier weitere Spiele erweitern. Damit werden natürlich zahlreiche Spekulationen über neue Spieletitel losgetreten. Denkbar wären ein sechster Resident Evil-Teil oder auch ein Devil May Cry 5. Die TGS 2010 findet vom 16. bis 19. September 2010 in Tokyo statt.

   

* * *

   

