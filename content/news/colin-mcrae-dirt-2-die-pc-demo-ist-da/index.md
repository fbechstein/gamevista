---
title: 'Colin McRae: DiRT 2 – Die PC Demo ist da'
author: gamevista
type: news
date: 2009-12-01T17:08:13+00:00
excerpt: '<p>[caption id="attachment_386" align="alignright" width=""]<img class="caption alignright size-full wp-image-386" src="http://www.gamevista.de/wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg" border="0" alt="Colin McRae: DiRT 2" title="Colin McRae: DiRT 2" align="right" width="140" height="100" />Colin McRae: DiRT 2[/caption]Wie der Publisher <a href="http://www.dirt2game.com" target="_blank">Codemasters</a> bereits angekündigt hatte, wurde heute die PC Demo für das Rennspiel <strong>Colin McRae: DiRT 2</strong> veröffentlicht. Die Testversion bringt 1,3 Gigabyte auf die Wage und lässt euch in zwei Spielmodi, Landrush und Trailblazer, über die Piste heizen. Fahren könnt ihr dabei den Eclipse GT und den West Coast Chopper Stuka TT.</p> '
featured_image: /wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg

---
Die PC-Vollversion von **Colin McRae: DiRT 2** ist ab Freitag den 04. Dezember 2009 im Handel erhältlich.

[> Zum Colin McRae: DiRT 2 &#8211; PC Demo Download][1]







 [1]: downloads/demos/item/demos/colin-mcrae-dirt-2-demo