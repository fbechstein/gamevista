---
title: Battlefield Heroes – Neue kostenlose Inhalte verfügbar
author: gamevista
type: news
date: 2010-04-21T18:49:24+00:00
excerpt: '<p>[caption id="attachment_774" align="alignright" width=""]<img class="caption alignright size-full wp-image-774" src="http://www.gamevista.de/wp-content/uploads/2009/09/battlefieldheroes.jpg" border="0" alt="Battlefield Heroes" title="Battlefield Heroes" align="right" width="140" height="100" />Battlefield Heroes[/caption]Der Publisher <strong>Electronic Arts</strong> hat für das kostenlose Actionspiel <a href="http://www.battlefieldheroes.com" target="_blank">Battlefield Heroes</a> neue Inhalte zur Verfügung gestellt. Ab sofort stehen so die neue Karte Midnight Mayhem und ein Jetpack, mit dem ihr kleine Erkundungsflüge unternehmen könnt, zur Auswahl. Des Weiteren wurde ein neuer Spielmodus mit dem Namen V2 Vengeance eingeführt.</p> '
featured_image: /wp-content/uploads/2009/09/battlefieldheroes.jpg

---
In diesem müsst ihr eine Abschussanlage für V2 Raketen unter eure Kontrolle bringen. Passend dazu spendiert EA einen neuen Trailer der das Ganze vorstellt.

<a href="videos/item/root/battlefield-heroes-v2-vengeance-trailer" target="_blank">> Zum Battlefield Heroes &#8211; V2 Vengeance Trailer</a>

 

   

* * *

* * *

 __

 