---
title: 'Kane & Lynch 2: Dog Days – Demo für Xbox 360 und PC ab sofort erhältlich'
author: gamevista
type: news
date: 2010-07-26T17:33:47+00:00
excerpt: '<p>[caption id="attachment_1576" align="alignright" width=""]<img class="caption alignright size-full wp-image-1576" src="http://www.gamevista.de/wp-content/uploads/2010/03/kane-and-lynch2.jpg" border="0" alt="Kane & Lynch 2: Dog Days" title="Kane & Lynch 2: Dog Days" align="right" width="140" height="100" />Kane & Lynch 2: Dog Days[/caption]Publisher <strong>Square Enix</strong> hat heute offiziell die Demo von <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> veröffentlicht. Ab sofort ist es möglich die Demo über die Onlineplattform <a href="http://store.steampowered.com/app/28000/" target="_blank">Steam</a> bzw. dem Xbox Live Marktplatz als Goldmitglied herunterzuladen. Der einzigartige Grafikstil soll dabei neue Akzente setzen.</p> '
featured_image: /wp-content/uploads/2010/03/kane-and-lynch2.jpg

---
Dabei wird der zweite Teil noch düsterer und sorgt mit neuen Mehrspielermodi für mehr Abwechslung. Zum Inhalt der Demo hat der Publisher leider kein Wort verloren. Da hilft nur ausprobieren!   
<a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> erscheint am 20. August 2010 im Handel.

 

   

* * *

   

