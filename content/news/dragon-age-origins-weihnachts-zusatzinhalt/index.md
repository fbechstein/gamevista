---
title: 'Dragon Age: Origins – Neuer DLC zu Weihnachten?'
author: gamevista
type: news
date: 2009-11-20T18:25:12+00:00
excerpt: '<p>[caption id="attachment_1076" align="alignright" width=""]<img class="caption alignright size-full wp-image-1076" src="http://www.gamevista.de/wp-content/uploads/2009/11/return_to_ostagar002.jpg" border="0" alt="Neues Addon " title="Neues Addon " align="right" width="140" height="100" />Neues Addon [/caption]<a href="http://eu.dragonage.com/extra/?id=14355&lang=en" target="_blank">BioWare</a> hat heute auf der<a href="http://eu.dragonage.com/extra/?id=14355&lang=en" target="_blank"> offiziellen Website</a> des Rollenspiels<strong> Dragon Age: Origins</strong> den nächsten Downloadinhalt angekündigt. Der Titel wird „<strong>Rückkehr nach Ostagar</strong>“ sein und führt sie, wie nicht anders zu erwarten war, an den Ort Ostagar und gibt den Spielern einen Einblick in ein düsteres Kapitel in der Geschichte der Grauen Wächter.</p> '
featured_image: /wp-content/uploads/2009/11/return_to_ostagar002.jpg

---
Der Zusatzinhalt erscheint noch in diesem Winter für Xbox 360, PlayStation 3 sowie für den PC. Der neue Zusatzinhalt ****gibt den Spielern die Möglichkeit, Rache zu üben und sich auf die Suche nach den mächtigen Waffen und der Rüstung des einst so gewaltigen Königs Cailan zu begeben. „Rückkehr nach Ostagar“ wird neben „The Stone Prisoner“ und The Warden\`s Keep“ der dritte Zusatzinhalt sein.

 







 