---
title: The Secret of Monkey Island Special Edition – ab sofort auf Xbox Live
author: gamevista
type: news
date: 2009-07-15T11:30:45+00:00
excerpt: '<p>[caption id="attachment_49" align="alignright" width=""]<img class="caption alignright size-full wp-image-49" src="http://www.gamevista.de/wp-content/uploads/2009/07/monkeyse_small.png" border="0" alt="The Secret of Monkey Island SE ab sofort auf XBLA" title="The Secret of Monkey Island SE ab sofort auf XBLA" align="right" width="140" height="100" />The Secret of Monkey Island SE ab sofort auf XBLA[/caption]<strong>The Secret of Monkey Island Special Edition</strong> steht ab sofort für die Xbox Live Arcade für 800 MS Points zum Download zur Verfügung. Die Steam PC Version sollte heute Abend zur Verfügung stehen. Ob das Remake die Erwartungen halten bzw. sogar übetreffen kann, berichten wir Euch schnellstmöglich. </p><p>&nbsp;</p>'
featured_image: /wp-content/uploads/2009/07/monkeyse_small.png

---
&nbsp;

&nbsp;





