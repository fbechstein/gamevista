---
title: Blur – Multiplayer Beta bietet nun mehr Features
author: gamevista
type: news
date: 2010-03-24T20:45:36+00:00
excerpt: '<p>[caption id="attachment_769" align="alignright" width=""]<img class="caption alignright size-full wp-image-769" src="http://www.gamevista.de/wp-content/uploads/2009/09/blur.jpg" border="0" alt="Blur" title="Blur" align="right" width="140" height="100" />Blur[/caption]Wie der Publisher <strong>Activision Blizzard</strong> per Pressemitteilung verlauten lässt, wird ab Donnerstag, den 25. März, die Online Multiplayer Beta zum Rennspiel <a href="http://www.blurgame.com/home" target="_blank">Blur</a> mehr Inhalte anbieten. Somit wird das Level-Limit von Stufe 10 auf 15 erhöht. Außerdem wird der Fuhrpark erweitert. Damit gesellen sich der Koenigsegg CCXR und die Corvette ZR1 in die Garagen der Spieler. Das Demolition Challenge enthält nun 16 neue zerstörerische Herausforderungen.</p> '
featured_image: /wp-content/uploads/2009/09/blur.jpg

---
Der Mod-Shop bietet nun zwei neuen Mods, Iron Fist für mehr Schaden und Last Gap lässt Spieler einen Abräumer abfeuern, wenn euer Wagen einen Totalschaden erlitten hat.

 

<cite></cite>

   

* * *

   



 