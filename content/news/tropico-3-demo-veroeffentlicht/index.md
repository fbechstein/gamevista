---
title: Tropico 3 – Demo veröffentlicht
author: gamevista
type: news
date: 2009-09-11T18:58:37+00:00
excerpt: '<p> </p> <p><img class=" alignright size-full wp-image-120" src="http://www.gamevista.de/wp-content/uploads/2009/07/tropico3_small.jpg" border="0" alt="Tropico 3" title="Tropico 3" align="right" width="140" height="100" />Publisher <a href="http://www.tropico3.com/de/">Kalypso Media</a> hat heute eine Demo zum Aufbau Strategiespiel <strong>Tropico 3</strong> veröffentlicht. Unter anderem könnt ihr das Tutorial und zwei Missionen antesten. In <strong>Tropico 3</strong> versucht ihr als El Presidente in der Zeit des Kalten Krieges, den kleinen karibischen Inselstaat Tropico an euch zu reißen.</p> '
featured_image: /wp-content/uploads/2009/07/tropico3_small.jpg

---
Dabei könnt ihr wählen ob ihr mit Hilfe eures Militärs als skrupelloser und korrupter Tyrann eure Macht sichert oder als großzügiger Vater der Nation für Wohlstand sorgt.

Die Vollversion von **Tropico 3** wird am 24. September 2009 für PC und Xbox 360 erscheinen.

[> Zum Tropico 3 Demo Download][1]

* * *



* * *

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=275