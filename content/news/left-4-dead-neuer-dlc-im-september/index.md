---
title: Left 4 Dead – Neuer DLC im September
author: gamevista
type: news
date: 2009-08-04T22:23:24+00:00
excerpt: '<p>[caption id="attachment_278" align="alignright" width=""]<img class="caption alignright size-full wp-image-278" src="http://www.gamevista.de/wp-content/uploads/2009/08/left4dead2_small.jpg" border="0" alt="Left 4 Dead" title="Left 4 Dead" align="right" width="140" height="100" />Left 4 Dead[/caption]Da freut sich der Fan. Wie <a href="http://www.valvesoftware.com" target="_blank" title="valve">Valve </a>nun berichtete wird für den Zombie-Shooter <strong>Left 4 Dead</strong> im September eine neue Kampagne eingereicht. <strong>Crash Course</strong> wird sie heißen und sich zwischen der Originalkampagne „No Mercy“ und „Death Toll“ Storytechnisch ansiedeln. </p>'
featured_image: /wp-content/uploads/2009/08/left4dead2_small.jpg

---
**Crash Course** wird für PC Nutzer völlig kostenlos sein, hingegen Xbox 360 Besitzer 560 Microsoft Punkte bezahlen dürfen. Der Versus-Modus, in dem man als Spieler auch Boss Zombies steuern, soll laut Valve gradliniger und schneller werden. Laut offizieller Pressemitteilung wird eine Runde im Versus Modus nur 30 Minuten dauern.  

<div class="textblock">
  
</div>

<div class="textblock">
  
</div>

<div class="textblock">
  
</div>