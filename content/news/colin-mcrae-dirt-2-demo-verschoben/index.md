---
title: 'Colin McRae: DiRT 2 – Demo verschoben'
author: gamevista
type: news
date: 2009-11-28T16:09:58+00:00
excerpt: '<p>[caption id="attachment_386" align="alignright" width=""]<img class="caption alignright size-full wp-image-386" src="http://www.gamevista.de/wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg" border="0" alt="Colin McRae: DiRT 2" title="Colin McRae: DiRT 2" align="right" width="140" height="100" />Colin McRae: DiRT 2[/caption]Der Entwickler <a href="http://www.dirt2game.com" target="_blank">Codemasters</a> hat ein weiteres Mal die PC-Demo zum Rennspiel <strong>Colin</strong> <strong>McRae DiRT 2 </strong>verschoben. Diese sollte bereits am Montag den 30. November 2009 erscheinen. Der neue Termin ist nun auf den 4. Dezember 2009 gelegt worden und hoffen wir nun das <a href="http://www.dirt2game.com" target="_blank">Codemasters</a> endlich den PC-Freunden auch die Chance gibt das neue DiRT antesten zu können.</p> '
featured_image: /wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg

---
Gründe für die Verschiebung gab der Entwickler nicht an. Die Vollversion für PC steht ab dem 03. Dezember 2009 im Handel.

 





