---
title: Natural Selection 2 – Läuft mit Steam + Systemanforderungen bekannt
author: gamevista
type: news
date: 2010-03-03T18:51:13+00:00
excerpt: '<p>[caption id="attachment_1515" align="alignright" width=""]<img class="caption alignright size-full wp-image-1515" src="http://www.gamevista.de/wp-content/uploads/2010/03/gorge_render_01_800x597_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Wie der Entwickler <strong>Valve </strong>auf der offiziellen Webseite des Ego-Shooters <a href="http://www.unknownworlds.com/ns2/" target="_blank">Natural Selection 2</a> bekannt gegeben hat, wird der Nachfolger des ersten Teils auf der Onlineplattform Steam vertrieben. Wer sein Spiel bereits vorbestellt hat, soll ohne Probleme zu Steam transferiert werden. Weiterhin teilen die Entwickler mit, dass sie bereits mit einer kleineren Gruppe von Spielern die Leistungsfähigkeit der neuen Grafik-Engine testen.</p> '
featured_image: /wp-content/uploads/2010/03/gorge_render_01_800x597_small.jpg

---
Dieser Test wird bald auf alle Spieler ausgeweitet die die Special Edition vorbestellt haben, so Valve.

 

Zu guter Letzt, gaben die Entwickler noch die Systemanforderungen des Shooters bekannt.

<div>
  <ul>
    <li>
      Windows Vista/XP/Windows 7
    </li>
    <li>
      CPU mit 1,2 GHz
    </li>
    <li>
      256 MByte Arbeitsspeicher
    </li>
    <li>
      DirectX-kompatible Grafikkarte
    </li>
    <li>
      Internetverbindung
    </li>
  </ul>
  
  <hr />
  
  <p>
    
  </p>
  
  <hr />
  
  <ul id="nointelliTXT">
  </ul></p>
</div>