---
title: 'Ghost Recon: Future Soldier – Ubisoft verschiebt Titel'
author: gamevista
type: news
date: 2010-05-18T19:44:26+00:00
excerpt: '<p>[caption id="attachment_1622" align="alignright" width=""]<img class="caption alignright size-full wp-image-1622" src="http://www.gamevista.de/wp-content/uploads/2010/03/ghostrecon_futuresoldier.jpg" border="0" alt="Ghost Recon: Future Soldier" title="Ghost Recon: Future Soldier" align="right" width="140" height="100" />Ghost Recon: Future Soldier[/caption]Pubisher <strong>Ubisoft </strong>hat offiziell bestätigt das der neuste Ghost Recon Teil, <strong>Future Soldier</strong>, definitiv nicht mehr dieses Jahr erscheint. Zwar wurde kein offizieller Veröffentlichungstermin benannt, laut <strong>Ubisoft </strong>wird aber mit dem 1. Quartal 2011 gerechnet.</p> '
featured_image: /wp-content/uploads/2010/03/ghostrecon_futuresoldier.jpg

---
Der Grund dürfte der nicht gewollte Wettkampf mit dem im November erscheinenden **Call of Duty: Black Ops** und dem im Oktober erscheinenden **Medal of Honor** sein.

 

 

   

* * *

   

