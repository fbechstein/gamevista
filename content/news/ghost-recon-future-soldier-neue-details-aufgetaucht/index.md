---
title: 'Ghost Recon: Future Soldier – Neue Details aufgetaucht'
author: gamevista
type: news
date: 2010-04-13T19:03:52+00:00
excerpt: |
  |
    <p>[caption id="attachment_1622" align="alignright" width=""]<img class="caption alignright size-full wp-image-1622" src="http://www.gamevista.de/wp-content/uploads/2010/03/ghostrecon_futuresoldier.jpg" border="0" alt="Ghost Recon: Future Soldier " title="Ghost Recon: Future Soldier " align="right" width="140" height="100" />Ghost Recon: Future Soldier [/caption]Der Publisher <strong>Ubisoft </strong>hat weitere Informationen zu <a href="http://www.ghostrecon.uk.ubi.com/" target="_blank">Tom Clancy's Ghost Recon: Future Soldier</a> verkündet. Der Taktik-Shooter der im Herbst 2010 für PC, Xbox 360 und PlayStation 3 erscheinen soll, wird euch in die Rolle der Elite Einheit der Ghost`s versetzen. Demnach sind die Ghost`s mit den neusten Technologien in Form von Waffen, Drohnen und Tarntechnologie ausgestattet.</p> <p />
featured_image: /wp-content/uploads/2010/03/ghostrecon_futuresoldier.jpg

---
So wird es möglich sein, sich mit einer Art Tarnanzug unsichtbar zu machen oder alla Crysis mit einem verstärkten Exoskelett mehr Stärke bzw. eine höhere Geschwindigkeit zu erreichen. Diverse Prototypen in Form von fliegenden Drohnen oder ferngelenkten Waffensystemen dürfen da natürlich nicht fehlen. Weiterhin teil **Ubisoft** mit, dass <a href="http://www.ghostrecon.uk.ubi.com/" target="_blank">Ghost Recon: Future Soldier</a> einen umfangreichen Koop-Modus bereitstellen wird. So wird es auf der Xbox 360 möglich sein per Splitscreen oder Xbox Live mit seinen Freunden zusammen zu spielen. Ähnlich wird das natürlich auf der PlayStation 3 und PC auch der Fall sein.

   

* * *

   



<span style="font-size: 11pt; line-height: 115%; font-family: "></p> 

<p>
  </span>
</p>

<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow: hidden;">
  Normal 0 21 false false false DE X-NONE X-NONE <span style="font-size: 11pt; line-height: 115%; font-family: ">Der Publisher Ubisoft hat weitere Informationen zu <strong><span style="font-family: ">Tom Clancy&#8217;s Ghost Recon: Future Soldier verkündet. Der Taktik-Shooter der im Herbst 2010 für PC, Xbox 360 und PlayStation 3 erscheinen soll, wird euch in die Rolle der Elite Einheit der Ghost`s versetzen. Demnach sind die Ghost`s mit den neusten Technologien in Form von Waffen, Drohnen und Tarntechnologie ausgestattet. So wird es möglich sein, sich mit einer Art Tarnanzug unsichtbar zu machen oder alla Crysis mit einem verstärkten Exoskelett mehr Stärke bzw. eine höhere Geschwindigkeit zu erreichen. Diverse Prototypen in Form von fliegenden Drohnen oder ferngelenkten Waffensystemen dürfen da natürlich nicht fehlen. Weiterhin teil Ubisoft mit, dass </span></strong>Ghost Recon: Future Soldier einen umfangreichen Koop-Modus bereitstellen wird. So wird es auf der Xbox 360 möglich sein per Splitscreen oder Xbox Live mit seinen Freunden zusammen zu spielen. Ähnlich wird das natürlich auf der PlayStation 3 und PC auch der Fall sein.</span>
</div>