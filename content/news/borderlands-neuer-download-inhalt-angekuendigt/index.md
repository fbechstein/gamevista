---
title: Borderlands – Neuer Download-Inhalt angekündigt
author: gamevista
type: news
date: 2010-01-29T17:01:58+00:00
excerpt: '<p>[caption id="attachment_1392" align="alignright" width=""]<img class="caption alignright size-full wp-image-1392" src="http://www.gamevista.de/wp-content/uploads/2010/01/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Der Entwickler <strong>Gearbox Software</strong> hat offiziell einen dritten Download-Inhalt für den Rollenspiel-Shooter <a href="http://www.borderlandsthegame.com/" target="_blank">Borderlands</a> angekündigt. Mit dem Namen <strong>The Secret Armory of General Knoxx</strong>, soll der bisher umfangreichste Zusatzinhalt demnächst veröffentlicht werden.</p> '
featured_image: /wp-content/uploads/2010/01/smalllogo.jpg

---
Unterstützt werden die Konsolen Xbox 360 und PlayStation 3, sowie der PC. <a href="http://www.borderlandsthegame.com/" target="_blank">Borderlands: The Secret Armory of General Knoxx</a> wird unter anderem neue Gegner, Gebiete und Mission beinhalten. Außerdem soll die Maximalstufe angehoben werden.

 

* * *



* * *