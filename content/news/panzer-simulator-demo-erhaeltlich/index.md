---
title: Panzer Simulator Demo erhältlich
author: gamevista
type: news
date: 2009-09-09T08:52:51+00:00
excerpt: '<p>[caption id="attachment_655" align="alignright" width=""]<img class="caption alignright size-full wp-image-655" src="http://www.gamevista.de/wp-content/uploads/2009/09/panzersimulator.png" border="0" alt="Panzer Simulator" title="Panzer Simulator" align="right" width="140" height="100" />Panzer Simulator[/caption]Wer wollte nicht schon immer einmal einen Panzer fahren? Bisher waren Simulationen hierzu hochkomplex und ultrakompliziert. Im <strong>Panzer Simulator</strong> ist das anders. Der Spieler kann sich nahezu frei in der Landschaft bewegen. Je nach Aufgabenstellung kann es bei Ihren Aufträgen um deren präzise Ausübung gehen oder auch um die Demonstration der höchstmöglichen Zerstörungskraft. Die Steuerung ist hierbei eher simpel gehalten. Auch ein Schadensmodell wurde in das Spiel integriert. Als verfügbare Panzer stehen der deutsche "Tiger" und der amerikanische "Sherman" zur Wahl.</p> '
featured_image: /wp-content/uploads/2009/09/panzersimulator.png

---
[> zur Panzer Simulator Demo][1]

* * *



* * *

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=263