---
title: Jagged Alliance Online – Klassiker wird als Browser-Game neu aufgelegt
author: gamevista
type: news
date: 2010-08-19T13:58:43+00:00
excerpt: '<p>[caption id="attachment_2141" align="alignright" width=""]<img class="caption alignright size-full wp-image-2141" src="http://www.gamevista.de/wp-content/uploads/2010/08/jagged-alliance-2.jpg" border="0" alt="Jagged Alliance" title="Jagged Alliance" align="right" width="140" height="100" />Jagged Alliance[/caption]Der Klassiker <strong>Jagged Alliance</strong> dürfte wohl jedem Strategiefan ein Begriff sein. Das äußerst bekannte Rundenstrategiespiel, was bereits einige Jahre auf dem Buckel hat, bekommt nun auch seine Browserspiel-Version. <strong>Bitcomposer Games</strong> und <strong>Gamigo </strong>kündigten den Titel für das nächste Jahr an.</p> '
featured_image: /wp-content/uploads/2010/08/jagged-alliance-2.jpg

---
**Jagged Alliance Online** wird als reines Browser-Online-Rollenspiel daher kommen und soll vom Spielablauf den Vorgängern sehr ähneln. Wie immer gibt es Rundenstrategie gemixt mit Echtzeit-Spielelementen. Wer sich für die Neuauflage interessiert sollte auf der derzeit stattfindenden Spielemesse gamescom in Köln in Halle 9.2 Stand C036 vorbeischauen. Dort gibt es erste Bilder zum Browser-MMO.

   

* * *

   



 