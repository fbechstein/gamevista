---
title: 'Disciples 3: Renaissance – Demo veröffentlicht'
author: gamevista
type: news
date: 2010-07-10T18:57:18+00:00
excerpt: '<p><strong>[caption id="attachment_2003" align="alignright" width=""]<img class="caption alignright size-full wp-image-2003" src="http://www.gamevista.de/wp-content/uploads/2010/07/disciples3.jpg" border="0" alt="Disciples 3: Renaissance" title="Disciples 3: Renaissance" align="right" width="140" height="100" />Disciples 3: Renaissance[/caption]Kalypso Media</strong> hat die Demo zum Strategiespiel <a href="http://www.disciples3.com/" target="_blank">Disciples 3: Renaissance</a> veröffentlicht. Der rundenbasierte dritte Teil der Serie wurde vom russischen Entwicklerstudio <strong>Akella </strong>produziert.<br /> Die Testversion zeigt die drei Hauptfraktionen von Nevendaar und beinhaltet außerdem ein Tutorial.</p> '
featured_image: /wp-content/uploads/2010/07/disciples3.jpg

---
Ihr könnt die ersten Missionen der Rassen The Empire, The Legions of the Damned und The Elven Alliance anspielen. <a href="http://www.disciples3.com/" target="_blank">Disciples 3: Renaissance</a> wurde am 24. Juni 2010 für PC veröffentlicht.

 

[> Zum Disciples 3: Renaissance – Demo Download][1]

* * *

  



 

 [1]: downloads/demos/item/demos/disciples-3-renaissance-demo