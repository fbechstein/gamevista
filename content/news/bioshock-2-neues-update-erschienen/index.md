---
title: Bioshock 2 – Neues Update erschienen
author: gamevista
type: news
date: 2010-02-13T15:47:22+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2 " title="Bioshock 2 " align="right" width="140" height="100" />Bioshock 2 [/caption]Zum kürzlich erschienen Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a>, hat der Publisher <strong>2K Games</strong> den ersten Patch veröffentlicht. Das Update mit der Versionsnummer 1.001 behebt unter anderem einen Fehler in Verbindung mit Windows 7. Sobald das Spiel minimiert wurde, stürzte das Spiel ab. Dies sollte nun der Vergangenheit angehören.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Außerdem wird ein Fehler der zum Absturz auf den Desktop führte behoben. 

[> Zum Bioshock 2 &#8211; Patch 1.001 Download][1]

**<span style="text-decoration: underline;">Die komplette Liste der Änderungen:</span>**

<div>
  <ul>
    <li>
      Fixed accepting a game invite on the Multiplayer Menu via Friend Invite causing a crash. &#8211; Fixed a black screen that would occur minimizing and restoring the game in Windows 7.
    </li>
    <li>
      Fixed the ability to adopt or harvest Little Sisters after rebinding the &#8218;F&#8216; or &#8218;B&#8216; keys to Zoom, Fire or Fire Plasmid.
    </li>
    <li>
      Fixed Big Daddy HUD elements remaining on screen when not appropriate.
    </li>
    <li>
      Fixed an issue where the player&#8217;s plasmid hand would become unresponsive if they pressed the Next Plasmid key immediately after firing any chargeable plasmid (such as ElectroBolt 2).
    </li>
    <li>
      Fixed a potential save file issue.
    </li>
    <li>
      Fixed an issue where the resolution would change on its own if entering gameplay in a 16:10 resolution on a native 16:10 monitor.
    </li>
    <li>
      Fixed a problem where if you were running the game in a resolution higher than your desktop resolution the mouse cursor wouldn&#8217;t be able to go across the whole screen.
    </li>
    <li>
      Fixed an issue in Windows 7 where certain Radio messages could become scrambled or broken after defeating a Big Sister if Voice audio had been turned down or muted previously.
    </li>
    <li>
      Fixed a crash while playing Dionysus Park on a 32-bit machine.
    </li>
    <li>
      Fixed a crash that would occur sometimes loading a previous manual save made before any present QuickSave.
    </li>
    <li>
      Fixed a crash while signing in to GFWL when running compatibility mode for XP SP2 while running Vista.
    </li>
    <li>
      Fixed an issue where you couldn&#8217;t play audio diaries in certain areas.
    </li>
    <li>
      Fixed a rare crash that could occur any time before a specific Big Sister sequence.
    </li>
  </ul>
  
  <hr />
  
  <p>
    
  </p>
  
  <hr />
  
  <ul id="nointelliTXT">
  </ul></p>
</div>

 [1]: downloads/patches/item/patches/bioshock-2-patch-1001