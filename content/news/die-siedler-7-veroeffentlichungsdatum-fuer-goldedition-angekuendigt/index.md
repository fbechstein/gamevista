---
title: Die Siedler 7 – Veröffentlichungsdatum für Goldedition angekündigt
author: gamevista
type: news
date: 2011-01-18T17:15:42+00:00
excerpt: '<p>[caption id="attachment_2522" align="alignright" width=""]<img class="caption alignright size-full wp-image-2522" src="http://www.gamevista.de/wp-content/uploads/2011/01/small1.jpg" border="0" alt="Die Siedler 7" title="Die Siedler 7" align="right" width="140" height="100" />Die Siedler 7[/caption]Publisher <strong>Ubisoft </strong>hat den Release-Zeitpunkt der Goldedition von <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Die Siedler 7</a> im Rahmen einer Pressemitteilung angekündigt. Demnach erscheint die Edition bestehend aus allen drei bisher veröffentlichten Erweiterungspaketen (DLC`s) am 24. Februar 2011 im Handel.</p> '
featured_image: /wp-content/uploads/2011/01/small1.jpg

---
Als Bonus packen die Publisher noch den dritten Teil der Die Siedler-Reihe mit dazu. Die Goldedition von <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Die Siedler 7</a> wird ca. 40 Euro kosten.

 

 

* * *



* * *