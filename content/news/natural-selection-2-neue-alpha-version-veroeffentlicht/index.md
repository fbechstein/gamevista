---
title: Natural Selection 2 – Neue Alpha-Version veröffentlicht
author: gamevista
type: news
date: 2010-11-17T18:20:45+00:00
excerpt: '<p>[caption id="attachment_348" align="alignright" width=""]<img class="caption alignright size-full wp-image-348" src="http://www.gamevista.de/wp-content/uploads/2009/08/ns2_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Das Entwicklerteam von <strong>Unknown Worlds</strong> hat eine weitere Version des Alpha-Clients von <a href="http://www.unknownworlds.com/ns2/" target="_blank">Natural Selection 2</a> veröffentlicht. Vorbesteller der Special-Edition können somit die Entwicklung des zweiten Teils Live miterleben. Die aktuelle Version wird automatisch beim Start von Steam heruntergeladen und installiert.</p> '
featured_image: /wp-content/uploads/2009/08/ns2_small.jpg

---
Dabei werden Neuerungen wie die Physikengine Havok und ein neues Benutzerinterface getestet. Unknown Worlds will mit der neuen Version unter anderem auch eine bessere Performance auf den Bildschirm zaubern. Die kompletten Änderungen im Überblick:

 

**Technical**

<div>
  <li>
    <strong>Changed collision detection to use the Havok engine</strong>
  </li>
  <li>
    <strong>Optimized trace ray and sweep capsule tests</strong>
  </li>
  <li>
    Fixed bug where keys could get stuck down after a frame hitch
  </li>
  <li>
    Fixed bug where players could not be removed from the scoreboard after they left the game
  </li>
  <li>
    Fixed bug where bots would not be properly removed from the server
  </li>
  <li>
    Fixed crash on exit when trying to run the game without Steam running
  </li>
  <li>
    Fixed bug where particle systems weren&#8217;t properly occlusion culled
  </li>
  <li>
    Fixed hardware cursors not being properly released on shutdown
  </li>
  <li>
    Fixed bugs with the occlusion culling system not properly determining if something is visible
  </li>
  <li>
    Changed the &#8222;Couldn&#8217;t Connect&#8220; error message to be more descriptive
  </li>
  <li>
    Added net_messages console command to log network messages received
  </li>
  <li>
    Added r_gui console command to enable/disable drawing the GUI
  </li>
  <li>
    Added the ability to include PROFILE markers in script code
  </li>
  <li>
    Added support for Lua GUIs on models (i.e. ammo display on the Rifle)
  </li>
  <p>
    <strong>Gameplay</strong>
  </p>
  
  <li>
    <strong>Completely re-did marine, alien and &#8222;shared&#8220; (waypoints, chat, death messages, etc.) interfaces in Lua (was in flash). You should see noticeable frame-rate improvements.</strong>
  </li>
  <li>
    Added very basic bots for load testing (&#8222;dev 1&#8220; at server, &#8222;addbots #&#8220; or &#8222;removebots #&#8220;
  </li>
  <li>
    Armory &#8222;buy&#8220; menu fully client-side now so it&#8217;s much more responsive
  </li>
  <li>
    Hydras now build smoothly
  </li>
  <li>
    MACs and Drifters now cost energy to produce (further eliminating spam and overly simplistic defense)
  </li>
  <li>
    Sentries turn off completely without power
  </li>
  <li>
    Power nodes have almost-dead and dead electricity/sparking effects
  </li>
  <li>
    Reduced number of network messages and footstep sounds dramatically
  </li>
  <li>
    Networking optimizations
  </li>
  <li>
    Power nodes work on level change
  </li>
  <li>
    Structure flinch animations fixed
  </li>
  <li>
    Changing weapons as marine is now faster and more reponsive
  </li>
  <li>
    Added basic pathing for bots (when commanding)
  </li>
  <li>
    Fixed MAC welding effects
  </li>
  <li>
    Skulk leap now requires research (on the Whip, coming soon)
  </li>
  <li>
    Fixed &#8222;doubling up&#8220; of some sounds
  </li>
  <p>
    Also, here&#8217;s the changelog from the previous version which we released previously (Build 153):
  </p>
  
  <p>
    <strong>Technical</strong>
  </p>
  
  <li>
    Fixed the net_lag console command
  </li>
  <li>
    Fixed the player limit on servers
  </li>
  <li>
    Fixed bug where the server could send two updates with the same time
  </li>
  <li>
    Fixed bug where attempting to connect to a nonexistent server would not properly fail
  </li>
  <li>
    Fixed issue where time would not advance uniformly on the client causing motion to appear jerky
  </li>
  <li>
    Fixed bug where disconnecting and then reconnecting to a server could put the game in an incorrect state
  </li>
  <li>
    Fixed bug where clients sometimes couldn&#8217;t move when when connected to a server
  </li>
  <li>
    Added support for extrapolation past the last update from the server
  </li>
  <li>
    Added a timeout for authenticating with the Steam servers
  </li>
  <li>
    Added displaying the average server tick rate with the net_stats console command
  </li>
  <p>
    <strong>Gameplay</strong>
  </p>
  
  <li>
    Fixed problem where infinite MACs and Drifters could be built and cause problems (now limited to 25 units in an area)
  </li>
  <li>
    Fixed power node in tram marine expansion
  </li>
  <li>
    Power nodes can be repaired properly again
  </li>
  <li>
    Fixed extractor multiple upgrade bugs
  </li>
  <li>
    Fixed hive sight drawing incorrectly
  </li>
  <li>
    Fixed sentry accuracy problems
  </li>
  <li>
    Fixed lowered server CPU performance when using sentries
  </li>
  <li>
    Fixed problem with power nodes on map reset
  </li>
  <li>
    Fixed missing text tooltips for commander UIs
  </li>
  <li>
    Fixed build harvester problem from gestate menu
  </li>
  <li>
    Fixed death message weirdness
  </li>
  <li>
    Fixed ns2_junction minimap
  </li>
  <li>
    Changed power nodes to pulse red when out of power (makes it more obvious)
  </li>
</div>

   

* * *

   

