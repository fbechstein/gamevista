---
title: 'CSI: Tödliche Absichten – Demo für Hobbydetektive'
author: gamevista
type: news
date: 2010-01-03T18:25:30+00:00
excerpt: '<p>[caption id="attachment_1257" align="alignright" width=""]<img class="caption alignright size-full wp-image-1257" src="http://www.gamevista.de/wp-content/uploads/2010/01/csi_2.jpg" border="0" alt="CSI: Tödliche Absichten" title="CSI: Tödliche Absichten" align="right" width="140" height="100" />CSI: Tödliche Absichten[/caption]Zum Adventure <strong>CSI: Tödliche Absichten</strong> wurde vor kurzem eine Demoversion für den PC veröffentlicht. So kann der Spieler brutale Verbrechen im Spielerparadies Las Vegas aufklären und arbeitet dabei mit den Charakteren aus der neunten Staffel von CSI: Crime Scene Investigation zusammen.</p> '
featured_image: /wp-content/uploads/2010/01/csi_2.jpg

---
Die Demo ist knapp 350 Megabyte klein und bietet einige spannende Schauplätze aus der TV-Serie.

[> Zum CSI: Tödliche Absichten &#8211; Demo Download][1]

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/csi-deadly-intent-demo