---
title: Fussball Manager 11 – Entwicklung offiziell bestätigt
author: gamevista
type: news
date: 2010-06-08T17:28:05+00:00
excerpt: '<p>[caption id="attachment_1891" align="alignright" width=""]<img class="caption alignright size-full wp-image-1891" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifam11pcscrnofflinejustfootballger.jpg" border="0" alt="Fussball Manager 11" title="Fussball Manager 11" align="right" width="140" height="100" />Fussball Manager 11[/caption]<strong>Electronic Arts</strong> kündigte heute im Rahmen einer Pressemitteilung die Entwicklung des <a href="http://www.fm10.de/publish/de/fussball_manager/fm11/spielbeschreibung/spielbeschreibung.html" target="_blank">Fussball Manager 11</a> an. Auch in diesem Jahr wird die Fussballmanager-Simulation zahlreiche Neuerungen erfahren.  Dabei wurde viel Wert auf die Vorstellungen der Community gelegt. So wurde der Taktikbereich komplett neu gestaltet und verbessert.</p> '
featured_image: /wp-content/uploads/2010/06/fifam11pcscrnofflinejustfootballger.jpg

---
Hinzu gekommen sind Anweisungen für die Spieleröffnung des Torwarts, für Ecken und für die Zielpositionen jedes einzelnen Spielers in der Offensive und in der Defensive. Ein neuer Aufstellungsassistent ermöglicht optimale automatische Anpassungen der Aufstellung bei Sperren oder Verletzungen.   
Außerdem wurde mehr Wert auf die Transferzeit gelegt. So trifft der Manager nun auf alle Probleme, die auch die Manager im realen Leben haben, wie etwa der Druck der Medien, die regelmäßig über Gerüchte, Angebote, Vertragsverlängerungen und unzufriedene Spieler berichten.

Auch der im vergangenen Jahr sehr erfolgreich eingeführte Onlinemodus wurde erheblich erweitert. Integriert wurde u.a. ein Aktionskarten-System, mit dem man noch direkteren Einfluss auf das Spielgeschehen hat. Ferner gibt es einen neuen Draft für Jugendspieler, neue Auktionsformen und in regelmäßigen Abständen spezielle Sponsorenmissionen. Wer beispielsweise die längste Siegesserie schafft, wird mit zusätzlichem Budget belohnt.

<a href="http://www.fm10.de/publish/de/fussball_manager/fm11/spielbeschreibung/spielbeschreibung.html" target="_blank">Fussball Manager 11</a> erscheint vorrausichtlich im Herbst diesen Jahres.

<p style="text-align: center;">
  <img class=" size-full wp-image-1892" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifam11pcscrnonlinesponsormissionger.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/06/fifam11pcscrnonlinesponsormissionger.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/06/fifam11pcscrnonlinesponsormissionger-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

