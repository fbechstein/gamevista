---
title: 'Assassin’s Creed: Brotherhood – Update 2.0 DLC ab sofort verfügbar'
author: gamevista
type: news
date: 2011-01-18T17:30:09+00:00
excerpt: '<p>[caption id="attachment_2524" align="alignright" width=""]<img class="caption alignright size-full wp-image-2524" src="http://www.gamevista.de/wp-content/uploads/2011/01/ac2_small.jpg" border="0" alt="Assassin’s Creed: Brotherhood" title="Assassin’s Creed: Brotherhood" align="right" width="140" height="100" />Assassin’s Creed: Brotherhood[/caption]Publisher <strong>Ubisoft </strong>hat angekündigt,  das der kostenlose Download-Content für das Actionspiel <a href="http://assassinscreed.de.ubi.com/brotherhood/" target="_blank">Assassin’s Creed: Brotherhood</a>, Animus Project Update 2.0, ab sofort verfügbar ist. Ihr bekommt den Patch jeweils für Xbox 360 und PlayStation 3 im PSN bzw. über XBL.</p> '
featured_image: /wp-content/uploads/2011/01/ac2_small.jpg

---
Das Update beinhaltet einen brandneuen Spielmodus, sowie eine neue Karte mit dem Namen Pienza. Im neuen Truhenjagd-Mehrspieler-Modus kämpfen zwei Teams von je drei Spielern mit verbündeten Templern, um Truhen einzusammeln bzw. zu verteidigen. Außerdem ist das neue Templar-Punkte-Feature enthalten, dass die fleißige Arbeit der Abstergo Rekruten belohnt.

 

* * *



* * *