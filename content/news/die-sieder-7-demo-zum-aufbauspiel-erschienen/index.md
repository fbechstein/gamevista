---
title: Die Sieder 7 – Demo zum Aufbauspiel erschienen
author: gamevista
type: news
date: 2010-03-19T18:25:18+00:00
excerpt: '<p>[caption id="attachment_1587" align="alignright" width=""]<img class="caption alignright size-full wp-image-1587" src="http://www.gamevista.de/wp-content/uploads/2010/03/small1.jpg" border="0" alt="Die Siedler 7" title="Die Siedler 7" align="right" width="140" height="100" />Die Siedler 7[/caption]Bereits in einer Woche steht die Veröffentlichung des siebten Teils der Die Siedler-Reihe an. Passend dazu hat der Publisher <strong>Ubisoft </strong>die Demo zum Aufbaustrategietitel bereitgestellt. Die 2,6 Gigabyte große Testversion bietet dem Spieler eine Map „Stadt am Fluss“ zum ausprobieren. Dabei können sie <a href="http://www.siedler.de.ubi.com" target="_blank">Die Siedler 7</a> in drei verschiedenen Modi testen. Der Singleplayer-Modus bietet eine ganz normale Runde gegen den Computergegner.</p> '
featured_image: /wp-content/uploads/2010/03/small1.jpg

---
Daneben gibt es noch den Freien Siedel-Modus der euch ohne Zwang von Computergegnern siedeln lässt. Zu guter Letzt bietet die Demo von <a href="http://www.siedler.de.ubi.com" target="_blank">Die Siedler 7</a> noch einen Mehrspieler-Modus. <a href="http://www.siedler.de.ubi.com" target="_blank">Die Siedler 7</a> erscheint am 25. März 2010 im Handel. Die passende Demo steht ab sofort bei uns zum download bereit.

 

[> Zum Die Siedler 7 &#8211; Demo download][1] 

<cite></cite>

   

* * *

   



 

 [1]: downloads/demos/item/demos/die-siedler-7-demo