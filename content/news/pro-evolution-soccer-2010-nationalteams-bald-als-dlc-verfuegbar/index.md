---
title: Pro Evolution Soccer 2010 – Nationalteams bald als DLC verfügbar
author: gamevista
type: news
date: 2010-05-25T15:01:11+00:00
excerpt: '<p><strong>[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]Konami </strong>wird für das Fußballspiel <a href="http://www.konami.com/pes2010" target="_blank">Pro Evolution Soccer 2010</a> einen Zusatzinhalt veröffentlichen, der die Nationalteams der Fifa Fußballweltmeisterschaft beinhaltet. Das Update berücksichtigt dabei die aktuellen Trikots und Aufstellungen der Teams.</p> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
Der DLC wird am 8. Juni kostenlos auf PlayStation 3, Xbox 360 und PC erscheinen. Folgende Teams sind mit dabei:

  * Japan
  * Ghana
  * Elfenbeinküste
  * Argentinien
  * Brasilien
  * Irland
  * Italien
  * England
  * Griechenland
  * Kroatien
  * Schweden
  * Spanien
  * Deutschland
  * Frankreich
  * Niederlande
  * Portugal
  * Australien
  * Korea

   

* * *

   

