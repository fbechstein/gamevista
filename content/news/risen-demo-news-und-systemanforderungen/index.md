---
title: Risen – Demo News und Systemanforderungen
author: gamevista
type: news
date: 2009-07-31T11:03:18+00:00
excerpt: '<p>[caption id="attachment_228" align="alignright" width=""]<img class="caption alignright size-full wp-image-228" src="http://www.gamevista.de/wp-content/uploads/2009/07/risenhafen.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Die Anzeichen für eine Demo zu <a href="http://www.piranha-bytes.com/" target="_blank">Piranha Bytes</a> <strong>Risen</strong> vor dem geplanten Release am 02. Oktober 2009 sehen sehr gut aus. Laut Daniel Oberlerncher von Publisher <a href="http://www.deepsilver.com/" target="_blank">Deep Silver</a> will man mit der Demo zeigen, dass das Spiel bugfrei auf dem Markt erscheint. Wann jedoch die Demo released werden soll, steht zur Zeit noch nicht fest.</p>'
featured_image: /wp-content/uploads/2009/07/risenhafen.png

---
Was jedoch feststeht sind die Systemanforderungen zu Risen. Diese sehen wie folgt aus: 

Minimum:  
CPU: 2.0 GHz   
RAM: 1 GB  
Grafik: Pixel Shader 3.0 und 256 MB (z.B. GeForce 7900 oder ATI 1800)

Empfohlen:  
CPU:3.0 GHz Dual Core  
RAM: 2 GB  
Grafik: Pixel Shader 3.0 und 512 MB (z.B. GeForce 8800 oder ATI Radeon HD 2900) 

Wie man sieht, wird das Spiel auch etwas schwächeren Rechnern gut laufen.





