---
title: Patrizier 4 – Update 1.1.1 kommt mit neuen Inhalten
author: gamevista
type: news
date: 2010-09-10T16:51:02+00:00
excerpt: '<p><img class="caption alignright size-full wp-image-1615" src="http://www.gamevista.de/wp-content/uploads/2010/03/patrizier4.jpg" border="0" alt="Patrizier 4" title="Patrizier 4" align="right" width="140" height="100" />Patrizier 4[/caption]Die seit letzter Woche erhältliche Wirtschaftssimulation <a href="http://www.patrizier4.de/de/index.php" target="_blank">Patrizier 4</a> erhielt vor kurzem  den ersten Patch. Publisher <strong>Kalypso Media</strong> veröffentlichte heute das Update mit der Versionsnummer 1.1.1,  das gleichzeitig neue Inhalte in das Spiel einbringt. Neben zahlreichen Verbesserungen und Beseitigungen von Fehlern, wird das Spiel auch neue Animationen, Missionen und eine überarbeitete Seekarte erhalten.</p> '
featured_image: /wp-content/uploads/2010/03/patrizier4.jpg

---
Der Patch wird automatisch über den im Spiel befindlichen Kalypso-Launcher geladen.

 

**Die kompletten Änderungen des Updates 1.1.1 für Patrizier 4:**

  * Zwei seltene Abstürze beseitigt, die in unterschiedlichen Situationen auftreten konnten
  * Erledigte Forderungen des Landesfürsten können im Logbuch nun auch gelöscht werden
  * Das GeldKlimper-Geräusch wurde leiser gestellt und in die options.ini aufgenommen
  * Rivalen melden sich beim Bau vieler Gebäude nicht mehr pro Gebäude
  * Probleme im Zusammenhang mit der Konvoiliste beseitigt
  * Ein Problem im Zusammenhang mit dem Durchschalten eigener Kontore (mittels TAB-Taste im geöffneten Kontor) beseitigt
  * Mausscroll-Problem am Bildschirmrand bei bestimmten Auflösung beseitigt
  * Probleme mit der Preisanzeige im Handelsroutendialog beseitigt. Zuvor verwendete Preise werden nur übernommen, wenn eine Stadt neu hinzugefügt wird
  * In der Seeschlacht wird nun ein Sound für jede einzelne Kanone abgespielt
  * Das Ereignis »Feuer« wurde implementiert. Es kommt aufgrund der möglichen heftigen Auswirkungen erst ab Schwierigkeitsgrad &#8222;Fortgeschritten&#8220; vor

**Neuerungen des Inhalts-Updates für Patrizier 4**

  * Detailliertere Seekarte mit animiertem Wasser.
  * Neues Hintergrundbild für das Hauptmenü.
  * Animierte Hafenkräne.
  * Unterschiedliche Bodentexturen in verschiedenen Regionen.
  * In einigen Regionen werden nun unterschiedliche Wohnhäuser verwendet.
  * Einige Bedienungsschwächen im Interface beseitigt.
  * Neue Missionen, um Zusätzliche Sabotage-Missionen und Heirat implementiert.
  * Asynchrone Sprachausgabe bei den Einführungsvideos beseitigt.
  * Die Spielzeit wird nun im Handelsfenster angehalten. Die Leertaste für den Zeitvorlauf funktioniert aber weiterhin.
  * Neue &#8222;dunkle&#8220; Missionen und Sabotagemöglichkeiten in der Taverne: Rufmord begehen lassen, Handel mit Piraten, Aufstand anzetteln, Feuer gezielt legen lassen.
  * Neue Warenlieferungs-Missionen im Rathaus.
  * Neue Missionen in der Gilde: Stimmung des Landesfürsten verbessern, Hungernot in einer Stadt auslösen.
  * Folgende neue Ereignisse wurden implementiert: Feuerausbruch in einer Stadt, Schiffsbrüchiger auf See, Landesfürst blockiert Häfen mit Hilfe von Piraten, Landesfürst lässt keine Bettler in die Stadt.
  * Die Heirat und die Familie wurde implementiert. Der User kann nun ab einem bestimmten Wohlstand in Form einer Mission im Rathaus die Forderungen eines Werbers erfüllen und dadurch heiraten.

   

* * *

   

