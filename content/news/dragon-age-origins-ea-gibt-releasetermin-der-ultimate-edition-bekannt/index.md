---
title: 'Dragon Age: Origins – EA gibt Releasetermin der Ultimate Edition bekannt'
author: gamevista
type: news
date: 2010-11-11T18:52:15+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Bereits vor einigen Wochen kündigte der Publisher <strong>Electronic Arts</strong> offiziell die Ultimate Edition für das sehr erfolgreiche Rollenspiel <a href="http://dragonage.bioware.com" target="_blank">Dragon Age: Origins</a> an. Nun hat der Publisher den genauen Veröffentlichungstermin der Ultimate Edition bekannt gegeben.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Pünktlich zum Weihnachtsgeschenk wird umfangreiche Fassung am 16. Dezember 2010 in den Handel kommen. Diese enthält sämtliche bereits erschienenen Download-Inhalte.

 

Die Inhalte im Überblick:

  * The Stone Prisoner
  * Warden&#8217;s Keep
  * Return to Ostagar
  * The Darkspawn Chronicles
  * Leliana&#8217;s Song
  * The Golems of Amgarrak
  * Witch Hunt

   

* * *

   

