---
title: Mass Effect 2 – BioWare kündigt Collectors Edition an
author: gamevista
type: news
date: 2009-11-05T15:59:16+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Der Entwickler <a href="http://www.masseffect.bioware.com" target="_blank">BioWare</a> hat heute im Rahmen einer offiziellen Pressemitteilung verkündet das der Action-Rollenspiel Mix <strong>Mass Effect 2</strong> auch als limitierte <strong>Collectors Edition</strong> erscheinen wird. Die <strong>Collectors Edition</strong> wird neben der Standardversion ab dem 28. Januar 2010 erhältlich sein.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Präsentiert wird das Ganze in einer schicken Metallverpackung und enthält neben der Vollversion des Spiels ein 48 Seiten starkes Hardcover Buch „Art of Mass Effect 2“, auch die Ausgabe 1 des Comics Mass Effect Redemption und eine Bonus DVD mit Videos die einen Blick hinter die Kulissen erlaubt gibt es in dieser limitierten Version. Außerdem werden zusätzlich einzigartige Waffen und Rüstungen für das Spiel enthalten sein.

 

* * *



* * *