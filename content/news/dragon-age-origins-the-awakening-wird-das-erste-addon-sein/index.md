---
title: 'Dragon Age: Origins – The Awakening wird das erste Addon sein'
author: gamevista
type: news
date: 2010-01-05T16:27:13+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Der Entwickler <a href="http://www.dragonage.bioware.com" target="_blank">BioWare</a> hat nun den Gerüchten um ein neues Addon zum Rollenspiel <strong>Dragon Age: Origins</strong> eine offizielle Ankündigung folgen lassen. Das erste Addon wird demnach ab dem 16. März unter dem Namen <strong>The Awakening</strong> erscheinen.  So wird, wie schon vermutet wurde, das erste Zusatzpaket an die Story des Hauptspiels anschließen.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
In den USA wird das Addon bereits mit 39,99 US Dollar gelistet, hierzulande wird es dann ca. 30 Euro kosten. Das Addon wird unter anderem neue Gebiete und mächtigere Gegner bieten. Weiterhin sind neue Gegenstände, Zaubersprüche, Fähigkeiten und Talente geplant. Weiterhin wird das Level Cap angehoben. Die Spielzeit wird mit ungefähr 15 Stunden angegeben.

 

 

* * *



* * *

 