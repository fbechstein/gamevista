---
title: Battlefield 1943 – Auf unbestimmte Zeit verschoben
author: gamevista
type: news
date: 2009-08-12T20:44:33+00:00
excerpt: '<p>[caption id="attachment_341" align="alignright" width=""]<img class="caption alignright size-full wp-image-341" src="http://www.gamevista.de/wp-content/uploads/2009/08/battle1943.png" border="0" alt="Battlefield 1943" title="Battlefield 1943" align="right" width="140" height="100" />Battlefield 1943[/caption]Wie Publisher <a href="http://www.electronic-arts.de/" target="_blank">Electronic Arts</a> nun bekannt gab wird <strong>Battlefield 1943</strong> auf unbestimmte Zeit verschoben werden. Erst Ende April bestätigte <a href="http://www.electronic-arts.de/" target="_blank">EA </a>das <strong>Battlefield 1943</strong> ganze 3 Monate später erscheinen wird als die Konsolen Fassung. Dort wurde noch der Termin September angepeilt.</p> '
featured_image: /wp-content/uploads/2009/08/battle1943.png

---
Nun nahm Gordon van Dyke, Producer von **Battlefield 1943**, zu den Vorwürfen vieler Fans die PC Besitzer benachteiligt sehen Stellung. <a href="http://www.electronic-arts.de/" target="_blank">Electronic Arts</a> möchte ein hochwertiges Erlebnis auch für Pc Besitzer gewährleisten und muss deshalb den Termin verschieben. Scheinbar deutet er damit er damit an das die Entwickler wohl Probleme haben mit der Portierung von Konsole zum Pc System. Ob **Battlefield 1943** dann noch dieses Jahr erscheinen wird ist sehr unwahrscheinlich.

 





