---
title: Xbox 360 Elite Edtion – In Deutschland jetzt günstiger
author: gamevista
type: news
date: 2009-08-27T18:35:59+00:00
excerpt: '<p />[caption id="attachment_530" align="alignright" width=""]<img class="caption alignright size-full wp-image-530" src="http://www.gamevista.de/wp-content/uploads/2009/08/xbox360.jpg" border="0" alt="Xbox 360" title="Xbox 360" align="right" width="140" height="95" />Xbox 360[/caption]<strong>Microsoft </strong>bestätigte heute per Pressemitteilung das der Preis für eine neue Sonderversion der <a href="http://www.amazon.de/exec/obidos/ASIN/B002MD3B4A/cynamite-21/">Xbox 360 Elite Edition</a> von 299 Euro auf 249 Euro gesenkt wird. Die Preissenkung hat laut <strong>Microsoft</strong> ab dem 28. August Gültigkeit. Der Grund für das Ganze dürfte die <strong>PlayStation 3</strong> Slim sein. Denn durch die  Preissenkung ihrerseits wurde Konkurrent <strong>Microsoft</strong> sicher heftig unter Druck gesetzt. <br /> '
featured_image: /wp-content/uploads/2009/08/xbox360.jpg

---
Die neue [Xbox Elite Edition][1] Version ist eine Antwort auf die neue PlayStation 3 Slim und bietet unter anderem eine 120 GB Festplatte, weiterhin liegen ein WLan Adapter bei und das Actionspiel Halo 3. Erscheinungstermin ist der 22. September und [hier][1] könnt ihr vorbestellen.

 

* * *



* * *

 

 [1]: http://www.amazon.de/exec/obidos/ASIN/B002MD3B4A/cynamite-21/