---
title: Grotesque Tactics – Demo zu Indie-Hit veröffentlicht
author: gamevista
type: news
date: 2010-06-24T15:13:56+00:00
excerpt: '<p>[caption id="attachment_1949" align="alignright" width=""]<img class="caption alignright size-full wp-image-1949" src="http://www.gamevista.de/wp-content/uploads/2010/06/grotesque.jpg" border="0" alt="Grotesque Tactics" title="Grotesque Tactics" align="right" width="140" height="100" />Grotesque Tactics[/caption]Vor kurzem hat der Entwickler <strong>Silent Dream</strong> eine neue Demo zum Rundenstrategiespiel <a href="http://www.grotesque-game.com" target="_blank">Grotesque Tactics</a> veröffentlicht. Mit einer Spielzeit von ca. 2 Stunden und 10 Quests könnt ihr euch einen ersten Überblick zum witzigen Strategie-Rollenspielmix verschaffen. Den Demo findet ihr ab sofort bei uns zum Download.</p> '
featured_image: /wp-content/uploads/2010/06/grotesque.jpg

---
[> Zur Grotesque Tactics &#8211; Premium Demo][1]

 

   

* * *

   



 [1]: downloads/demos/item/demos/grotesque-tactics-premium-demo