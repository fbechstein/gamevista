---
title: Pro Evolution Soccer 2011 – Erste Details bekannt
author: gamevista
type: news
date: 2010-05-04T19:15:35+00:00
excerpt: '<p>[caption id="attachment_1755" align="alignright" width=""]<img class="caption alignright size-full wp-image-1755" src="http://www.gamevista.de/wp-content/uploads/2010/05/pes2011.jpg" border="0" alt="Pro Evolution Soccer 2011" title="Pro Evolution Soccer 2011" align="right" width="140" height="49" />Pro Evolution Soccer 2011[/caption]Der Publisher <a href="http://konami.com" target="_blank">Konami</a> kündigte im Rahmen einer Pressemitteilung die 2011 Ausgabe von Pro Evolution Soccer an. Das Fußballspiel soll im kommenden Herbst auf PlayStation 3, Xbox 360, PC, Wii, PlayStation 2 und PSP erscheinen. Die diesjährige Ausgabe soll viele Neuerungen mit sich bringen.</p> '
featured_image: /wp-content/uploads/2010/05/pes2011.jpg

---
So wird viel Zeit in Sachen Passspiel und verbesserter KI der Verteidiger investiert. Weiterhin teilt der Publisher mit, dass die Meisterliga diesmal auch Online nutzbar sein wird. Größte Änderung ist die neue Power Anzeige, die nun jedem einzelnen Spieler zur Verfügung steht. Diese erlaubt euch die Stärke und Zielgenauigkeit vor jedem einzelnen Pass und Schuss präzise zu bestimmen. So kann man nun individuell in den Raum passen.

 

   

* * *

* * *