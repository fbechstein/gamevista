---
title: Xbox 360 – Nächstes Update sperrt inoffizielle Memory Units
author: gamevista
type: news
date: 2009-10-20T17:00:42+00:00
excerpt: '<p>[caption id="attachment_530" align="alignright" width=""]<img class="caption alignright size-full wp-image-530" src="http://www.gamevista.de/wp-content/uploads/2009/08/xbox360.jpg" border="0" alt="Xbox 360" title="Xbox 360" align="right" width="140" height="95" />Xbox 360[/caption]Wie der Entwickler <a href="http://www.xbox.com" target="_blank">Microsoft</a> berichtet wird im nächsten Dashboard Update für die <strong>Xbox 360</strong> diverse noName Speicherkarten gesperrt. Das Update was Anfang November erscheinen soll wird neben Twitter- und Facebook Support auch „unautorisierte Memory Units“ sperren.</p> '
featured_image: /wp-content/uploads/2009/08/xbox360.jpg

---
Wenn ihr also Spielstände und Profile darauf gespeichert habt solltet ihr diese möglichst schnell auf „autorisierte“ Speicherkarten transferieren. Weiterhin teilt <a href="http://www.xbox.com" target="_blank">Microsoft</a> mit das originales Microsoft-Zubehör und Hardware von dieser Sperre nicht betroffen sind. Wen wunderts.

 

* * *



* * *

 