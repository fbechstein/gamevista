---
title: 'Battlefield: Bad Company 2 – Neue Infos zum Hardcore-Modus'
author: gamevista
type: news
date: 2010-01-07T21:27:57+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Zum bald erscheinenden Ego-Shooter <strong>Battlefield: Bad Company 2</strong> werden nun immer mehr Details bekannt. So wird es allem Anschein nach einen Hardcore-Modus für den neusten Battlefield-Spross geben. Bekannt aus <strong>Modern Warfare 2</strong> werden auch hier die Waffen wesentlich durchschlagskräftiger daher kommen. Außerdem soll es keine Gegnermarkierungen oder Kill-Cams geben.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Um das Ganze noch ein wenig fordernder zu machen, wird es nur noch Möglich geben über Kimme und Korn zu zielen. Eine heikle Angelegenheit, da man nun ohne viele Anzeigen auf dem Bildschirm nicht mehr so gut von Freund oder Feind unterscheiden kann.

 

<cite></cite>

* * *



* * *

 