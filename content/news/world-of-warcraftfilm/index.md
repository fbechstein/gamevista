---
title: World of Warcraft – Film erst 2012
author: gamevista
type: news
date: 2009-07-26T14:24:45+00:00
excerpt: '[caption id="attachment_163" align="alignright" width=""]<img class="caption alignright size-full wp-image-163" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="139" height="100" />World of Warcraft[/caption]Nachdem am Mittwoch bekannt wurde das <strong>Sam Raimi</strong> die Regie für den <strong>World of Warcraft</strong> <strong>Film</strong> übernehmen würde war die Freude groß. Nun folgt die nächste Info, denn laut Warner Bros. ist der Kinostart erst für das Jahr 2012 geplant.<br />'
featured_image: /wp-content/uploads/2009/07/wow_small.png

---
<img class="caption alignright size-full wp-image-163" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="139" height="100" />World of Warcraft Nachdem am Mittwoch bekannt wurde das <strong>Sam Raimi</strong> die Regie für den <strong>World of Warcraft</strong> <strong>Film</strong> übernehmen würde war die Freude groß. Nun folgt die nächste Info, denn laut Warner Bros. ist der Kinostart erst für das Jahr 2012 geplant.

Grund für den späten Termin ist **Spider-Man 4**. Dieser muss noch vor dem Start von **World of Warcraft** abgedreht werden. Dieser wird 2011 in die Kinos kommen. Warner Bros. ist überzeugt das die **Warcraft** Lizenz sehr großes Potenzial für einen Kinohit hat. 







