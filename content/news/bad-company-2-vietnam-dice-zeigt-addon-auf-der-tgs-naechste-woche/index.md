---
title: 'Bad Company 2: Vietnam – DICE zeigt Addon auf der TGS nächste Woche'
author: gamevista
type: news
date: 2010-09-08T18:06:38+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Entwickler <strong>DICE </strong>hat angekündigt, das Addon <a href="http://www.battlefieldbadcompany2.com" target="_blank">Bad Company 2: Vietnam</a> auf der Spielemesse <a href="http://expo.nikkeibp.co.jp/tgs/2010/en/" target="_blank">TGS</a> in Tokyo, die ab dem 16. September 2010 startet, zum ersten Mal der Öffentlichkeit in vollem Umfang vorzustellen. Der schwedische Entwickler teilte dies auf ihrem <a href="http://blogs.battlefield.ea.com/battlefield_bad_company/archive/2010/09/08/getting-ready-for-tokyo-game-show.aspx" target="_blank">Blog</a> mit.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Vietnam wurde auf der E3 angekündigt und wird unter anderem vier neue Karten, neue Waffen sowie Fahrzeuge beinhalten. Mit dem Release wird zum Ende des Jahres für PlayStation 3, Xbox 360 und PC gerechnet.

 

   

* * *

   

