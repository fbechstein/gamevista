---
title: R.U.S.E. – Chimera Pack DLC Veröffentlichungstermin
author: gamevista
type: news
date: 2011-01-10T16:49:56+00:00
excerpt: '<p>[caption id="attachment_2497" align="alignright" width=""]<img class="caption alignright size-full wp-image-2497" src="http://www.gamevista.de/wp-content/uploads/2011/01/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Publisher <strong>Ubisoft </strong>hat den Veröffentlichungstermin für den Zusatzinhalt Chimera DLC für das Strategiespiel <a href="http://ruse.de.ubi.com/" target="_blank">R.U.S.E</a>. bekannt gegebene. Am 18. Januar 2011 wird der Downloadable-Content zur Verfügung stehen. Über den Inhalt sickerten bisher nur sehr wenige Informationen durch, laut einer Twitter-Meldung des Publishers sollen aber schon sehr bald erste Details folgen.</p> '
featured_image: /wp-content/uploads/2011/01/ruse_small.jpg

---
**The Chimera Pack** ist der zweite Zusatzinhalt für das Strategiespiel. **The Manhatten Project** wurde bereits kurz vor Weihnachten veröffentlicht und bringt neben neuen Mehrspielerkarten, auch zwei weitere Mehrspieler-Modi.

 

   

* * *

   

