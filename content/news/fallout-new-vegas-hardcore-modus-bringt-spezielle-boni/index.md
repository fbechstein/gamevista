---
title: 'Fallout: New Vegas – Hardcore-Modus bringt spezielle Boni'
author: gamevista
type: news
date: 2010-10-17T18:09:04+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]<strong>Bethesda`s</strong> <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> wird Spieler mit einer speziellen Belohnung honorieren die das Rollenspiel im Hardcore-Modus durchspielen. Laut der Beschreibung wird der Harcore-Modus den Schwierigkeitsgrad dahingehend steigern, dass Stimpacks nur leichte Wunden über eine gewisse Zeit heilen.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Schwere Verletzungen bleiben somit für die restliche Spielzeit bestehen. Rad-Away wird die Folgen von radioaktiver Strahlung ebenso nur über eine bestimmte Zeit beseitigen. Munition hat zusätzliches Gewicht und Essen sowie Trinken ist durchgängig nötig um in dem Endzeitspiel zu überleben.

 

Welchen Boni es am Ende des Spiels gibt verriet Bethesda nicht. <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> erscheint am 22. Oktober 2010 für PC, PlayStation 3 und Xbox 360.

   

* * *

   



 