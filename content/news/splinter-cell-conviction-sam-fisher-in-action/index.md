---
title: 'Splinter Cell: Conviction – Sam Fisher in Action'
author: gamevista
type: news
date: 2009-10-16T09:26:47+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Der Publisher <a href="http://www.ubi.com" target="_blank">Ubisoft</a> hat einen weiteren Gameplay Trailer zum kommenden Actiontitel <strong>Splinter Cell: Conviction</strong> veröffentlicht. Das vier Minuten lange Video ist auf der Tokyo Game Show 2009 entstanden und zeigt jede menge Action.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
Auf der Suche nach den Mördern seiner Tochter jagt Sam Fisher im Video Terroristen in der Stadt Washington D.C. **Splinter Cell** Fans dürfte sofort auffallen das im neuen Teil sich nicht mehr nur durch lineare Level bewegt wird. **Splinter Cell: Conviction** bietet hier eine recht offene Spielwelt. Außerdem scheint es wesentlich rauer zuzugehen. Wer mal keinen Lust auf Schleichaktionen hat schaltet auf rasante Actionsequenzen um. **Splinter Cell: Conviction** erscheint Ende Februar für Xbox 360 und PC.

 

[> Zum Splinter Cell: Conviction &#8211; TSG Walkthrough Trailer][1]

* * *



* * *

 

 [1]: videos/item/root/splinter-cell-conviction-tsg09-walkthrough