---
title: R.U.S.E. – Ab sofort im Handel erhältlich
author: gamevista
type: news
date: 2010-09-09T16:22:00+00:00
excerpt: '<p>[caption id="attachment_2217" align="alignright" width=""]<img class="caption alignright size-full wp-image-2217" src="http://www.gamevista.de/wp-content/uploads/2010/09/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Publisher <strong>Ubisoft </strong>kündigte heute die Veröffentlichung des Echtzeitstrategiespiels <a href="http://ruse.de.ubi.com" target="_blank">R.U.S.E.</a> an. Der Titel ist damit ab sofort im Handel für Xbox 360, PlayStation 3 und PC erhältlich. <a href="http://ruse.de.ubi.com/" target="_blank">R.U.S.E.</a> dreht sich um die Schlachten des 2. Weltkrieges und stellt dem Spieler mit Hilfe der IRISZOOM Engine eine virtuelle Landkarte zur Verfügung, bei der ihr bis auf den einzelnen Panzer heranzoomen könnt.</p> '
featured_image: /wp-content/uploads/2010/09/ruse_small.jpg

---
So könnt ihr einzelne Einheiten befehligen oder ganze Manöver kontrollieren. Hinzukommen taktische Finessen wie die R.U.S.E.-Fähigkeiten, mit denen ihr z.B. eure Feinde täuschen könnt oder eure Truppen stärkt.

 

   

* * *

   

