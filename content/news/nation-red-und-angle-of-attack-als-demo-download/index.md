---
title: Nation Red und Angle of Attack als Demo Download
author: gamevista
type: news
date: 2009-08-06T18:08:00+00:00
excerpt: '<p>[caption id="attachment_295" align="alignright" width=""]<img class="caption alignright size-full wp-image-295" src="http://www.gamevista.de/wp-content/uploads/2009/08/nation_red_small.jpg" border="0" alt="Nation Red" title="Nation Red" align="right" width="140" height="100" />Nation Red[/caption]Auch heute präsentieren wir euch wieder neue Demos als Download. Einmal hätte wir da den rundenbasierten Zombieshooter <strong>Nation Red</strong>. Entwickler <a href="http://www.kaoskontrol.com" target="_blank" title="diezelpower">DiezelPower </a>präsentiert euch diesen etwas anderen Shooter. </p>'
featured_image: /wp-content/uploads/2009/08/nation_red_small.jpg

---
Das ganze wird aus der Vogelperspektive gespielt und man kann verschiedene Power Ups einsammeln um damit mehr Zombies beim Ableben zu helfen. **Nation Red** könnt ihr direkt auf der <a href="http://www.nation-red.com/main.html" target="_blank" title="nation red">offiziellen Website</a> als Download für 10,99 Euro Kaufen oder ihr bekommt es in den nächsten Tagen auf GamersGate oder Direct2Drive. Wahrscheinlich wird es auch bald per Steam angeboten. 

[> Zur Nation Red Demo][1]   
<img class="caption alignright size-full wp-image-296" src="http://www.gamevista.de/wp-content/uploads/2009/08/angleofattack_small.jpg" border="0" alt="Angle of Attack" title="Angle of Attack" align="right" width="140" height="100" />

Außerdem gibt es heute bei uns die Demo von **Angle of Attack**, einem Science-Fiction Flugspiel des Entwicklers <a href="http://www.bc3000ad.com" target="_blank" title="3000ad">3000AD</a>. Die Demo enthält sowohl Flug- und Bodenangriffsmissionen. Die Vollversionen enthält 16 Kampagnenmissionen und einen Multiplayermodus mit bis zu 16 Spielern.

> Zur Angle of Attack Demo </p> 

</a>







 [1]: downloads/demos/item/demos/nation-red-demo "nation red demo"