---
title: Battlefield 3 – Unterstützt Dedicated Server
author: gamevista
type: news
date: 2011-02-10T14:16:16+00:00
excerpt: '<p>[caption id="attachment_2551" align="alignright" width=""]<img class="caption alignright size-full wp-image-2551" src="http://www.gamevista.de/wp-content/uploads/2011/02/battlefield3.jpg" border="0" alt="Battlefield 3" title="Battlefield 3" align="right" width="140" height="100" />Battlefield 3[/caption]Nachdem Publisher <strong>Electronic Arts</strong> eine Absage für Mod-Tools im kommenden <a href="http://www.ea.com/de/battlefield3" target="_blank">Battlefield 3</a> erteilt hat, wurde nun die Unterstützung für Dedicated Server für die PC-Version versprochen. Demnach wird es möglich sein im Mehrspielermodus Server zu betreiben die beliebige Spieleinstellungen unterstützen.</p> '
featured_image: /wp-content/uploads/2011/02/battlefield3.jpg

---
Passend zu dieser Meldung betonte Produzent Patrick Bach, dass <a href="http://www.ea.com/de/battlefield3" target="_blank">Battlefield 3</a> definitiv hauptsächlich für PC entwickelt werden würde. <a href="http://www.ea.com/de/battlefield3" target="_blank">Battlefield 3</a> erscheint im Herbst 2011 für PC, PlayStation 3 und Xbox 360 im Handel.

 

* * *



* * *