---
title: Just Cause 2 – Black Market Boom DLC erschienen
author: gamevista
type: news
date: 2010-05-26T16:21:05+00:00
excerpt: '<p>[caption id="attachment_1848" align="alignright" width=""]<img class="caption alignright size-full wp-image-1848" src="http://www.gamevista.de/wp-content/uploads/2010/05/justcause2_small.png" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Das Miniaddon <strong>Black Market Boom Pack</strong> für das Actionspiel <a href="http://www.justcause.com" target="_blank">Just Cause 2</a> ist ab sofort erhältlich. Der Download Content wird über die Onlineplattform Steam für 1,99 € angeboten und bringt drei neuen Waffen ins Spiel.</p> '
featured_image: /wp-content/uploads/2010/05/justcause2_small.png

---
Nach dem Download des Erweiterungspakets stehen euch ein Quad Rocket Launcher, eine Air Propulsion Gun und ein Cluster Bomb Launcher zur Verfügung.

 

<a href="http://store.steampowered.com/app/35102/" target="_blank">> Zum Just Cause 2 &#8211; Black Market Boom Pack</a>

   

* * *

   

