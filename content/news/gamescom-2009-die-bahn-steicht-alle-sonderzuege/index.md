---
title: Gamescom 2009 – die Bahn streicht alle Sonderzüge
author: gamevista
type: news
date: 2009-08-17T13:12:43+00:00
excerpt: '<p>[caption id="attachment_372" align="alignright" width=""]<img class="caption alignright size-full wp-image-372" src="http://www.gamevista.de/wp-content/uploads/2009/08/gamescom_small.png" border="0" alt="Gamescom feiert in Köln ihr Debut" title="Gamescom feiert in Köln ihr Debut" align="right" width="140" height="100" />Gamescom feiert in Köln ihr Debut[/caption]Wie auch letztes Jahr zu der GamesConvention in Leipzig, hat die Bahn dieses Jahr zig Sonderzüge zur <a href="http://www.gamescom.de/" target="_blank">gamescom</a> in Köln angekündigt und auch bereits Tickets verkauft. Für 70,- EUR konnten Gamer aus Hamburg oder Berlin nach Köln reisen, dort die Messe aufsuchen und Abends wieder die Heimreise auf sich nehmen. <br />Daraus wird nun nichts - aus bisher unerklärlichen Gründen hat die Bahn alle Sonderzüge gestrichen. Wer bereits ein Ticket gekauft hat, erhält selbstverständlich eine Gutschrift.</p> '
featured_image: /wp-content/uploads/2009/08/gamescom_small.png

---
 

* * *



* * *