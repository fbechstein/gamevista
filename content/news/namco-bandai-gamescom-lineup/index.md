---
title: Namco Bandai – gamescom Lineup
author: gamevista
type: news
date: 2009-08-10T20:43:54+00:00
excerpt: '<p class="MsoNormal">[caption id="attachment_324" align="alignright" width=""]<img class="caption alignright size-full wp-image-324" src="http://www.gamevista.de/wp-content/uploads/2009/08/namcobandai.jpg" border="0" alt="Namco Bandai" title="Namco Bandai" align="right" width="140" height="100" />Namco Bandai[/caption]Der Publisher <a href="http://www.namcobandaigames.com" target="_blank">Namco Bandai</a> veröffentlichte heute mit seinen Partnern das <strong>gamescom </strong>Lineup.<br />Zwischen 20. und 23. August wird in Köln auch unter dem Namen <a href="http://www.namcobandaigames.com">"Namco Bandai"</a> <strong>Atari </strong>Spiele präsentiert.</p>'
featured_image: /wp-content/uploads/2009/08/namcobandai.jpg

---
<p class="MsoNormal">
   
</p>

Hier eine kleine Liste:

  * <span><strong>Tekken 6: Bloodline Rebellion</strong> (PlayStation 3, Xbox 360)</span>
  * <span><strong>Dragon Ball Raging Blast </strong>(PlayStation 3, Xbox 360)</span>
  * <span><strong>Dragon Ball: Revenge of King Piccolo</strong> (Wii)</span>
  * <span><strong>One Piece: Unlimited Cruise 2: Awakening of a Hero</strong> (Wii)</span>
  * **<span>Tales of Symphonia: Dawn of the </span><span>New World</span>** <span>(Wii)</span>
  * <span><strong>Family Trainer Extreme Challenge</strong> (Wii)</span>
  * **Katamari Forever** (PlayStation 3)
  * **Champions Online** (Xbox 360)





