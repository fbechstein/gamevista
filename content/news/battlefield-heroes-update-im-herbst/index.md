---
title: Battlefield Heroes – Update im Herbst
author: gamevista
type: news
date: 2009-09-26T19:29:33+00:00
excerpt: '<p>[caption id="attachment_774" align="alignright" width=""]<img class="caption alignright size-full wp-image-774" src="http://www.gamevista.de/wp-content/uploads/2009/09/battlefieldheroes.jpg" border="0" alt="Battlefield Heroes" title="Battlefield Heroes" align="right" width="140" height="100" />Battlefield Heroes[/caption]Wie Publisher <a href="http://www.electronic-arts.de">Electronic Arts</a> zusammen mit dem Entwicklerteam von <a href="http://www.dice.se">Digital Illusions</a> bekannt gaben, erscheint am 30. September 2009 ein sehr großes Update für den Multiplayer Shooter <strong>Battlefield Heroes</strong>. Der Name fällt auf „<strong>Heroes of the Fal</strong>l“ und wird unter anderem eine neue Karte mit herbstlichem Aussehen mit sich bringen.</p> '
featured_image: /wp-content/uploads/2009/09/battlefieldheroes.jpg

---
Außerdem gibt es Änderungen am Balancing und natürlich ein paar Bugfixes. **Battlefield Heroes** hat bisher über zwei Millionen registrierte Mitglieder, damit ist es eines der erfolgreichsten kostenlosen Onlinespiele.

 

* * *



* * *

 