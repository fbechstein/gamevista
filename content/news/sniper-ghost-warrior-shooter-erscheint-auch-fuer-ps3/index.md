---
title: 'Sniper: Ghost Warrior – Shooter erscheint auch für PS3'
author: gamevista
type: news
date: 2010-11-04T18:35:09+00:00
excerpt: '<p>[caption id="attachment_1820" align="alignright" width=""]<img class="caption alignright size-full wp-image-1820" src="http://www.gamevista.de/wp-content/uploads/2010/05/sniper_ghost_warrior.jpg" border="0" alt="Sniper: Ghost Warrior" title="Sniper: Ghost Warrior" align="right" width="140" height="100" />Sniper: Ghost Warrior[/caption]Publisher <strong>City Interactive</strong> hat nun offiziell bestätigt, dass der Ego-Shooter <a href="http://www.sniperghostwarrior.com/" target="_blank">Sniper: Ghost Warrior</a> auch für die PlayStation 3 erscheinen wird. Der Titel ist bereits vor einiger Zeit für Xbox 360 und PC veröffentlicht worden. Als Release-Termin gibt der Publisher das erste Quartal 2011 an.</p> '
featured_image: /wp-content/uploads/2010/05/sniper_ghost_warrior.jpg

---
Die PlayStation 3-Fassung wird neue Features im Vergleich zu den Xbox 360- und PC-Versionen enthalten.

Die Features der PlayStation 3-Version im Überblick.

  * Gameplay und Grafik verbessert
  * Exklusive Einzelspieler-Missionen für PS3-Spieler
  * Zusätzliche Multiplayer-Karten
  * Exklusive neue Multiplayer-Modi, u.a. Capture the Flag und Hardcore-Schwierigkeitsgrad
  * Exklusive neue Scharfschützen-Gewehre wie das L96 und das M200 Intervention
  * Zusätzliche Einzelspieler-Herausforderung: Bonus-Mission im Hardcore-Modus als Test für die allerbesten Spieler

   

* * *

   

