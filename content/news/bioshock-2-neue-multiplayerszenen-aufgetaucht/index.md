---
title: Bioshock 2 – Neue Multiplayerszenen aufgetaucht
author: gamevista
type: news
date: 2009-10-29T19:12:06+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Entwickler <a href="http://www.2kmarin.com" target="_blank">2K Marin</a> liefert nun regelmäßig neue Bilder zum Shooter <strong>Bioshock 2</strong> um den Fans die Wartezeit etwas zu verkürzen. Diesmal wurde ein neuer Trailer zum Thema Multiplayer veröffentlicht. Demnach wurde heute ein Video vorgestellt das den <em>Capture the Little Sister</em> Multiplayer Modus vorstellt.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Hier ist es die Aufgabe der Spieler die _Little Sister_ einzufangen und zum Tunnelsystem in den Wänden der Unterwasserstadt zu bringen. Scheinbar eine neue Art des altbekannten _Capture the Flag_ Modus aus anderen Multiplayer-Spielen.

 

[> Zum Bioshock 2 &#8211; Capture the little sister Trailer][1]

* * *



* * *

 

 [1]: videos/item/root/bioshock-2-little-sister-game-mode