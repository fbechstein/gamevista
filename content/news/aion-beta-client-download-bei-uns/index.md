---
title: Aion – Beta Client Download bei uns
author: gamevista
type: news
date: 2009-07-30T19:02:35+00:00
excerpt: '<p>[caption id="attachment_224" align="alignright" width=""]<img class="caption alignright size-full wp-image-224" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion" title="Aion" align="right" width="140" height="100" />Aion[/caption]Wie ihr vielleicht schon gesehen habt ist bei uns ein neuer Download verfügbar. Gemeint ist der ziemlich schwangere <strong>Aion</strong> Beta Client, der satte 6 GB auf die Wage bringt. Damit ihr die morgen startende Beta pünktlich loszocken könnt solltet ihr jetzt schon anfangen zu ziehen. </p>'
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
Wir bieten natürlich ordentlich Speed, was von dem Patcher den ihr danach ausführen müsst nicht zu sagen ist. Dieser zieht einen 450 Mb Patch mit wahnsinniger Geschwindigkeit. Dauer ca. 2 Stunden, also bringt etwas Geduld mit. Vom 31. Juli bis zum 03. August könnt ihr euch dann einloggen und die schicke Crysis Grafik genießen. 

Geändert werden in Bezug auf die letzte Beta, neue Audio Dateien, Lokalisierungen und zwei neue Server pro Region. Ihr werdet euch spätestens am Freitag um 21.00 Uhr MESZ im Betaevent einloggen können.  
Eine bekanntgabe der Gewinner unserer Verlosung wird im Laufe des morgigen Tages stattfinden. 

[> Den Aion Beta Client gibt’s hier  
][1] <a href="http://community.dawnofwar2.com/viewtopic.php?f=17&#038;t=25492" target="_blank" title="patchnotes"></a>







 

 

[  
][1]

 [1]: downloads/demos/item/demos/aion-tower-of-eternity-game-client "aion beta"