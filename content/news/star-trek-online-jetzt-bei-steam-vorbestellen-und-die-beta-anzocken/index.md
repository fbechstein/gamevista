---
title: Star Trek Online – Jetzt bei Steam vorbestellen und die Beta anzocken
author: gamevista
type: news
date: 2010-01-05T20:21:17+00:00
excerpt: '<p>[caption id="attachment_1268" align="alignright" width=""]<img class="caption alignright size-full wp-image-1268" src="http://www.gamevista.de/wp-content/uploads/2010/01/logo.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Der Onlinedienst Steam bietet ab sofort die Möglichkeit an das Online-Rollenspiel<strong> Star Trek Online </strong>vorzubestellen. Zum einen gibt es die Standardversion für 49,99 EUR, zum anderen wird die Digital Deluxe Edition für 49,99 EUR angeboten. Bei beiden Versionen wird es einen Zugang zur nächsten Betaphase, die ab dem 12. Januar 2010 startet, geben.</p> <p> </p> '
featured_image: /wp-content/uploads/2010/01/logo.jpg

---
Weiterhin wird für jeden Vorbesteller, der bei Steam ordert eine Chromodynamik Rüstung bereit gestellt. Die Technik der Rüstung stammt aus dem Delta Quadranten und wurde von der U.S.S. Voyager zurückgebracht. Sie verbessert nicht nur eure Schadenswerte sondern erhöht auch eure Resistenz. Falls ihr in Erwägung zieht die Digital Deluxe Edition zu erwerben bietet diese eurer gewählten Rasse einen Bonus mit „Joined Trill“. Der Symboint steigert bis zu fünf verschieden Talente. **Star Trek Online** erscheint am 02. Februar 2010 im deutschen Handel.

 

Die komplette Liste der Gegenstände der Digital Deluxe Edition:

  * Original Star Trek Uniform Set: Drei Uniformen aus der Original Serie (blue, red, yellow).
  * Exclusive “KHAAAN!” Emote: An unforgettable moment from the second Star Trek Film. This exclusive emote allows players to relive Kirk’s unforgettable moment of fury, with the timeless cry… “KHAAAN!”
  * Exclusive Klingon Blood Wine Toast Emote: Raise a glass like a Klingon! Greet other players with an exclusive Klingon gesture –the blood wine toast.
  * Unique Registry Prefix: Give your ship the coveted NX prefix, seen only on a handful of elite Starfleet vessels like the Defiant, 22nd century Enterprise, and Prometheus.
  * Unique Ship Item: Automated Defense Battery. This Tactical Module grants any ship a passive 360 arc attack power with a short range.

* * *



* * *

 