---
title: Aliens vs. Predator – Swarm Mappack nächste Woche
author: gamevista
type: news
date: 2010-03-08T19:47:46+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Der Publisher <strong>SEGA </strong>hat angekündigt, dass bereits nächste Woche ein neuer Download-Inhalt für den Ego-Shooter <a href="http://www.sega.com/games/aliens-vs-predator" target="_blank">Aliens vs. Predator</a> erscheinen wird. Das <strong>Swarm Mappack</strong> wird somit am 18. März für ca. 5,99 EUR bzw. 560 Microsoft Points, per Steam, PSN und Xbox 360 Live Marktplatz, erhältlich sein. Inhalt des Packs sind vier neue Karten für den Mehrspielermodus.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Zwei dieser Karten werden für den Multiplayer-Modus (Deathmatch, Predator Hunt, Infestation, Species Deathmatch, Mixed Species Deathmatch und Domination) bereitgestellt und die anderen beiden neue Karten sind für den Survival-Modus vorgesehen.

 

* * *



* * *