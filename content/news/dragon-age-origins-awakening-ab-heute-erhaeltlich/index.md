---
title: 'Dragon Age: Origins – Awakening ab heute erhältlich'
author: gamevista
type: news
date: 2010-03-18T14:18:05+00:00
excerpt: '<p>[caption id="attachment_1579" align="alignright" width=""]<img class="caption alignright size-full wp-image-1579" src="http://www.gamevista.de/wp-content/uploads/2010/03/dragon_small.png" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Der Publisher <strong>Electronic Arts</strong> hat zusammen mit dem Entwickler <strong>BioWare </strong>das erste Addon zum Rollenspielhit <a href="http://www.dragonage.bioware.com" target="_blank">Dragon Age: Origins</a> veröffentlicht. <strong>Awakening </strong>ist ab sofort im Handel für Xbox 360 und PC, sowie als Downloadversion auf Xbox LIVE und dem PlayStation Network verfügbar.</p> '
featured_image: /wp-content/uploads/2010/03/dragon_small.png

---
Das Erweiterungspack <a href="http://www.dragonage.bioware.com" target="_blank">Dragon Age: Origins – Awakening</a> stammt von demselben Designer- und Autorenteam wie <a href="http://www.dragonage.bioware.com" target="_blank">Dragon Age: Origins</a> und führt mit Amaranthine eine neue Region der Welt ein, die es zu entdecken gilt. Die Spieler treffen dabei auf eine Vielzahl furchterregender Kreaturen, zu denen nicht nur intelligente Abkömmlinge der Dunklen Brut, sondern auch weitere bedrohliche Wesen wie ein Inferno-Golem und ein Spektraldrache zählen. Dragon Age: Origins – Awakening bietet den Spielern neue Möglichkeiten, ihre Helden und Gruppenmitglieder individuell zu entwickeln und die Charakterattribute neu festzulegen, wodurch die Wiederspielbarkeit noch weiter erhöht wird. Die Spieler können ihre Abenteuer aus <a href="http://www.dragonage.bioware.com" target="_blank">Dragon Age: Origins</a> mit höheren Erfahrungsstufen, neuen Zaubern, Fähigkeiten, Spezialisierungen und Gegenständen sowie fünf neuen Gruppenmitgliedern fortsetzen.

**Features:**   
**Eine atemberaubende Erweiterung der Welt:**    
BioWares bisher tiefgehendstes Universum wird durch das neue Gebiet Amaranthine noch größer.

  * Ergründe die Geheimnisse der Dunklen Brut und enthülle ihre wahren Motive 
  * Baue den Orden der Grauen Wächter wieder auf und richte ihnen in Vigils Wacht einen Stützpunkt ein 

**Neue, komplexe moralische Entscheidungen:**    
Erlebe eine epische Geschichte, die auf deine Spielweise reagiert und sich entsprechend entwickelt.

  * Präge mit deinen Entscheidungen und deinem Umgang mit komplexen Situationen dein gesamtes Spielerlebnis. 

**Neue Möglichkeiten, deinen Helden zu entwickeln:** **   
** Entdecke zusätzliche Zauber, Fähigkeiten, Spezialisierungen und Gegenstände, mit denen du deinen Helden und deine Gruppe noch individueller anpassen kannst.

  * Importiere deinen Charakter aus Dragon Age: Origins oder starte als neuer Grauer Wächter aus dem Nachbarland Orlais 
  * Begegne fünf brandneuen Gruppenmitgliedern und einem guten, alten Bekannten aus Dragon Age: Origins 

**Noch mehr intensivere Schlachten:** **   
** Kämpfe gegen zahlreiche neue, furchterregende Kreaturen

  * Beweise deine Fähigkeiten gegen eine höher entwickelte, intelligente Art der Dunklen Brut und weitere bedrohliche Wesen wie den Inferno-Golem und den Spektraldrachen!

<cite></cite>

   

* * *

   

