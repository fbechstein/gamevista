---
title: Borderlands – Gearbox gibt weitere Details zu Claptrap DLC bekannt
author: gamevista
type: news
date: 2010-09-09T19:11:28+00:00
excerpt: '<p>[caption id="attachment_2220" align="alignright" width=""]<img class="caption alignright size-full wp-image-2220" src="http://www.gamevista.de/wp-content/uploads/2010/09/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Entwickler <strong>Gearbox </strong>hat heute weitere Details zum nächsten Zusatzpaket mit dem Namen <strong>Claptrap’s New Robot Revolution</strong> bekannt gegeben. Wie schon angekündigt wurde wird der DLC die Levelbegrenzung nicht erhöhen, <strong>Gearbox </strong>wird aber mit einem kostenlosen Patch das Spielerlevel um acht Stufen steigern.</p> '
featured_image: /wp-content/uploads/2010/09/smalllogo.jpg

---
Spieler ohne den Gerneral Knoxx DLC werden damit bis zu Level 58 spielen können, wohin gegen Besitzer des Download-Content bis zur Stufe 69 aufsteigen. **Claptrap’s New Robot Revolution** wird die Umgebung umgestalten und die Bevölkerung von <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> neu ansiedeln. Im Laufe der Story wird der Spieler sechs neue Gebiete durchqueren die gespickt sind mit neuen und alten Gegnern. Der Zusatzinhalt wird sich eurem Level anpassen und falls ihr euch entscheiden solltet diesen ein zweites Mal durchzuspielen, erhaltet ihr zwei extra Skillpunkte und sechs SDU\`s. **Claptrap’s New Robot Revolution** wird am 28. September 2010 für PC, PlayStation 3und Xbox 360 als Download erscheinen.

 

   

* * *

   

