---
title: Stargate Resistance – Interview mit Kathryn Dutchin
author: gamevista
type: news
date: 2009-12-27T13:57:45+00:00
excerpt: '<p><img class=" alignright size-full wp-image-1211" src="http://www.gamevista.de/wp-content/uploads/2009/12/logo.jpg" border="0" alt="Stargate Resistance - Endlich ein Stargate-Shooter" title="Stargate Resistance - Endlich ein Stargate-Shooter" align="right" width="140" height="100" /></p> <p>Wir bekamen die Gelegenheit die Entwickler von <strong>Stargate Resistance</strong> zu interviewen. <strong>Stargate Resistance</strong> ist ein Third-person-shooter und erscheint Anfang 2010 als Einstimmung auf bzw. als Geldbeschaffungsmittel für das MMO <strong>Stargate Worlds</strong>, welches auch 2010 erscheinen soll.</p> '
featured_image: /wp-content/uploads/2009/12/logo.jpg

---
**1. Bitte stell dich unseren Lesern kurz vor.**

Hi! Ich bin Kat Dutchin und ich bin die Producer von **Stargate Resistance**.

<img class=" alignright size-full wp-image-1212" src="http://www.gamevista.de/wp-content/uploads/2009/12/kathryn_dutchin.jpg" border="0" alt="Kathryn Dutchin" title="Kathryn Dutchin" align="right" width="140" height="100" />

**2. Was ist **Stargate Resistance**?**

Stargate Resistance ist ein schnelllebiger, teambasierter Third-person-multiplayer-shooter, indem man den Kampf zwischen dem Stargate Commando (Menschen, SGC) und den Systems Lords (Aliens) um die Kontrolle über das Universum spielt.

**3. Was ist besonders an** **Stargate Resistance im Vergleich zu anderen Multiplayershootern?**

Ich möchte euch drei Besonderheiten vorstellen, die mir am wichtigsten sind:

  * <span style="text-decoration: underline;">Galactic Domination</span> (Galaktische Vorherrschaft) repräsentiert die ewige Schlacht zwischen System Lords und dem Stargate Commando (SGC). Jede gewonnene Schlacht trägt zur Vorherrschaft der Fraktion über den entsprechenden Planeten bei. Wenn man dann eine Schlacht auf einem eroberten Planeten führt, erhält man einen Bonus. Jede Eroberung eines Planeten trägt wiederum zur Vorherrschaft in der Galaxie bei und die Fraktion kann noch größere Rewards erhalten.
  * Die Fraktionen in **Stargate Resistance** sind <span style="text-decoration: underline;">asymmetrisch</span>, das heißt dass die beiden Fraktionen unterschiedliche Stärken und Schwächen haben und man entsprechend unterschiedliche Taktiken einsetzen muss um zu siegen. Zum Beispiel hat das SGC Vorteile beim Distanzkampf (mit MGs, Pistolen etc.) und verfügt über gute Defensivtaktiken. Sie haben ein ganzes Arsenal an militärischen Waffen. Die System Lords sind arrogant und nutzen Tarnfelder und Schutzschilder um nah an ihren Feind zu kommen und ihn ihm Nahkampf zu vernichten.
  * „<span style="text-decoration: underline;">We have a Stargate!</span>“ Spieler können durch das Stargate reisen. _(Kommentar des Autors: Ein Traum für Fans wird wahr: Endlich durch das Gate zu gehen!)_

<p style="text-align: center;">
  <img class=" size-full wp-image-1213" src="http://www.gamevista.de/wp-content/uploads/2009/12/gate.jpg" border="0" width="600" height="480" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/gate.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/gate-300x240.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**4. Warum entwickelt ihr dieses Spiel? Ich dachte ihr arbeitet an** **Stargate Worlds.**

Als die Wirtschaftskrise ausbrach, hatte Cheyenne ein sehr gutes Team, das **Stargate Worlds** entwickelt hat. Die Entwicklung war sehr teuer und aufwendig und so kam es, dass die Firma so wie so aufgebaut war, nicht länger funktionierte. Wir sind privat gegründet und unsere Investorengruppe wollte einen Release in relativ kurzer Zeit. Da wir **Stargate Worlds** in dieser kurzen Zeit nicht mit einer guten Qualität fertig stellen konnten, haben wir uns entschieden **Stargate Resistance** zu entwickeln. Das war einfach die beste Option um **Stargate Worlds** zu der Qualität zu bringen, die wir erreichen möchten. Wir sind gespannt wie **Stargate Resistance** den Fans gefallen wird und denken, dass es **Stargate Worlds** gut abrundet.

**5. Wie viele Maps wird es beim Release geben und wie groß sind diese sein?**

Am Anfang wird es drei Maps (sogenannte Worlds / Welten), jede mit einem anderen Gameplay, geben. Sie sind für 16 Spieler optimiert. In der Zukunft würden wir gerne mehr Maps für größere und kleinere Teams anbieten.

**6. In einigen Multiplayershootern, wie zum Beispiel** **Star Wars Battlefront II, kann man Helden spielen. Habt ihr über ein solches Feature auch nachgedacht? Werdet ihr es später noch einbauen?**

Das ist auf jeden Fall eine interessante Idee. Wenn wir an einer Erweiterung arbeiten werden, werden wir definitiv auch solche Elmente aus der TV-Serie einbauen.

**7. Was ist deine Lieblingsfraktion und – Klasse und warum?**

Ich liebe es als Ashrak zu spielen. Sie ist eine System Lord Assassine und nutzt ihre Tarnfähigkeiten um über die Map zu schleichen, systematisch ihre Opfer zu jagen und sie mit einem Hieb ihres Dolches zu killen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1214" src="http://www.gamevista.de/wp-content/uploads/2009/12/ashrak.jpg" border="0" width="600" height="445" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/ashrak.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/ashrak-300x223.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**8. Welche ist deine Lieblingswelt und wieso?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/12/amarna.jpg" rel="lightbox[sgr1]"><img class="caption alignright size-full wp-image-1216" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallamarna.jpg" border="0" alt="Ein antiker Tempel ist Ort einer Schlacht" title="Ein antiker Tempel ist Ort einer Schlacht" align="right" width="140" height="100" /></a>Das ist eine schwere Frage. Die Welten sind alle wirklich hübsch und so unterschiedlich voneinander. Als eine Ashrak bevorzuge ich Amarna (den antiken Tempel). Es ist eine Team-Death-Match-Karte mit vielen Tunneln und Seitenräumen, in denen man perfekt auf Gegner lauern kann. Dort gibt es so viele super Plätze um sich als Assassine zu verstecken.

**9. Warum habt ihr entschiedenen einen Third-person-multiplayer-shooter und nicht, wie die meisten, einen First-person-multiplayer-shooter zu entwickeln?**

Wir wollten nicht sein wie alle anderen. Ernsthaft: Wir denken, dass der Spieler sich so besser mit seinem Charakter identifizieren kann. Die Charakterentwicklung ist ein wichtiger Bestandteil des Spiels. Du bist ein Mitglied eines Teams und einer ganzen Community, die versucht das Schicksal des Universums zu retten. Zukünftig wird es möglich sein, seinen Charakter weiter zu entwickeln und das würde nur wenig bringen, wenn man ihn (oder sie!) dann nicht sieht.

**10. Wird das Spiel zum vollen Preis von 45 bis 50 € verkauft werden?**

Der Preis wird noch festgelegt werden.

**11. In der F.A.Q. schreibt ihr, dass seine Breitbandinternetverbindung zum Spielen notwendig ist. Das heißt ihr habt keinen Botsupport entwickelt?**

Wegen unserem Feature „Galactic Domination“ ist das Spiel so aufgebaut, dass immer Teams gegen- bzw. miteinander online spielen.

**12. Warum habt ihr euch entschieden das Spiel nur für den PC und nicht für Konsolen zu entwickeln?**

Während wir das Spiel für den PC veröffentlichen, sind wir natürlich offen für andere Plattformen wie Xbox 360 und PS3, falls Nachfrage besteht.**  
** 

**13. Habt ihr schon über ein mögliches Add-on oder DLC nachgedacht? Gibt es eine mögliche dritte Fraktion?**

<img class=" alignright size-full wp-image-1211" src="http://www.gamevista.de/wp-content/uploads/2009/12/logo.jpg" border="0" alt="Stargate Resistance - Endlich ein Stargate-Shooter" title="Stargate Resistance - Endlich ein Stargate-Shooter" align="right" width="140" height="100" />

Das Entwicklerteam arbeitet bereits hart daran kostenlose Bonusinhalte („free digital content bonus“) und eine erste Expansion zu entwicklen, die kurz nach dem Release veröffentlicht werden sollen. Wir denken wir fördern und fordern eine Community am besten, wenn wir immer weiter neue Maps, Achievments, Unlockables und Features hinzufügen. Momentan gibt es noch keinen Plan für eine dritte Fraktion, aber man weiß nie, was sich das Designteam als nächstes ausdenkt.

**Vielen Dank und einen guten Rutsch von uns.**

Wir werden **Stargate Resistance** Anfang 2010 auch testen. Es ist seit dem Spiel zum Stargatefilm das erste Spiel was auf der Stargatelizenz basiert und erscheint – zumindest bis jetzt. Bis 2003 entwickelte Perception einen Stargateshooter mit Single- und Multiplayerteil namens **Stargate The Alliance**. Damals beendete der Publisher JoWooD die Entwicklung aufgrund fehlender Qualität, soweit die Pressemeldungen.

Anscheinend haben aber die Entwickler die Wirtschaftskrise gemeistert, scheitern wohl nicht an der Lizenz und so können die Fans auf gleich zwei Spiele im Stargateuniversum in 2010 hoffen.

 

* * *



* * *