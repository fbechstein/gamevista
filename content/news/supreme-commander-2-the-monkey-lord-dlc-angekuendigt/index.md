---
title: Supreme Commander 2 – The Monkey Lord DLC angekündigt
author: gamevista
type: news
date: 2010-09-08T14:13:52+00:00
excerpt: '<p>[caption id="attachment_1330" align="alignright" width=""]<img class="caption alignright size-full wp-image-1330" src="http://www.gamevista.de/wp-content/uploads/2010/01/supreme-commander-2.jpg" border="0" alt="Supreme Commander 2" title="Supreme Commander 2" align="right" width="140" height="100" />Supreme Commander 2[/caption]Das Entwicklerteam von <strong>Gas Powered Games</strong> hat den ersten Zusatzinhalt für das Strategiespiel <a href="http://www.supremecommander2.com" target="_blank">Supreme Commander 2</a> offiziell angekündigt. Demnach wird der als Download erhältliche DLC mit dem Namen <strong>The Monkey Lord</strong> neben neuen Karten, auch weitere Einheiten und Upgrade-Möglichkeiten enthalten.</p> '
featured_image: /wp-content/uploads/2010/01/supreme-commander-2.jpg

---
Einen Termin für die Veröffentlichung gaben die Entwickler bisher nicht bekannt. Wie viel ihr für den Download-Content bezahlen müsst wurde noch nicht verraten.

 

   

* * *

   

