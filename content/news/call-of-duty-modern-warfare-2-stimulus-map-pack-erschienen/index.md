---
title: 'Call of Duty: Modern Warfare 2 – Stimulus Map Pack erschienen'
author: gamevista
type: news
date: 2010-03-30T19:07:57+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2 " title="Call of Duty: Modern Warfare 2 " align="right" width="140" height="100" />Call of Duty: Modern Warfare 2 [/caption]Der Publisher <strong>Activision Blizzard</strong> stellt ab heute das erste Multiplayer Mappack für den Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare 2</a> online. Vorerst aber nur für Xbox-360-Spieler, denn <strong>Microsoft </strong>hat einen einmonatigen Exklusivdeal mit den Entwicklern abgeschlossen. Somit erscheint das Stimulus Mappack für PlayStation 3 und PC erst in ca. einem Monat.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Inhaltlich bietet der DLC fünf neue Mehrspielerkarten. Die Kosten belaufen sich auf 1200 Microsoft Punkte.

 

<cite></cite>

   

* * *

   

