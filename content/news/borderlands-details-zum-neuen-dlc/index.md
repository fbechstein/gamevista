---
title: Borderlands – Details zum neuen DLC
author: gamevista
type: news
date: 2009-12-29T16:51:11+00:00
excerpt: '<p>[caption id="attachment_1250" align="alignright" width=""]<img class="caption alignright size-full wp-image-1250" src="http://www.gamevista.de/wp-content/uploads/2009/12/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Der Entwickler <a href="http://www.borderlandsthegame.com" target="_blank">Gearbox</a> hat neue Details zum ab sofort verfügbaren Zusatzinhalt für den Ego-Shooter <strong>Borderlands </strong>veröffentlicht. Bisher kommen aber nur Xbox 360 Besitzer in den Genuss von „<strong>Mad Moxxi`s Underdome Riot</strong>“. Die PlayStation 3 Version erscheint nächste Woche, die PC Variante später. Der zweite Downloadcontent wird für Spieler ab der Stufe 10 verfügbar sein.</p> '
featured_image: /wp-content/uploads/2009/12/smalllogo.jpg

---
Trotzdem wird es auch für Spieler der Stufe 50 an Herausforderung nicht mangeln. Denn die Gegner passen sich dem Spieler-Level an. Im neuen Zusatzinhalt wird es drei Kampfarenen geben, die sehr unterschiedlich ausfallen. Demnach geht es z.B. in **Hellburbia** in Straßenkämpfen heiß her. Die **Angelic Ruins** bieten eine verschneite Landschaft mit Alienruinen-Setting . Die letzte Arena bildet der **Gully**, hier sind große Canyons mit vielen guten Sniperpositionen vorhanden. Weiterhin wird es eine Bank geben wo ihr eure Gegenstände bunkern könnt und neue Shops für bessere Items. Die Arenen bieten einen neuen Spielmodus in dem in zwei Schwierigkeitsstufen Gegnerwellen auf euch zurollen. **Mad Moxxi&#8217;s Underdome Riot** erscheint heute auf Xbox Live für 800 MS-Points und ab dem 07. Januar 2010 auch über das PSN für 9,99 Euro.

 

* * *



* * *

 