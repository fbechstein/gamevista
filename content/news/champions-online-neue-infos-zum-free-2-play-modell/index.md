---
title: Champions Online – Neue Infos zum Free-2-Play-Modell
author: gamevista
type: news
date: 2011-01-13T17:04:27+00:00
excerpt: '<p>[caption id="attachment_122" align="alignright" width=""]<img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" />Champions Online[/caption]Entwickler <strong>Cryptic</strong> wird den Online-Rollenspieltitel Champions Online auch als Free-2-Play Variante anbieten. Der Termin ist auf den 25. Januar 2011 gesetzt und pünktlich vor der Umstellung auf das kostenlose Modell, veröffentlichten die Macher nun ein <a href="http://www.champions-online.com/f2p_faq" target="_blank">FAQ</a> zu den wichtigsten Fragen auf ihrer <a href="http://www.champions-online.com/f2p_faq" target="_blank">Website</a>.</p> '
featured_image: /wp-content/uploads/2009/07/champions_online_small.jpg

---
Es sollten alle Fragen die anstehen beantwortet werden zum Thema Ingame Shop, Cryptic Points, Free-2-Play-Modell gegen Abonnement-Variante und Gold- bzw. Silber-Member.

 

* * *



* * *