---
title: 'Mass Effect 2 – PC & Xbox 360 Version erscheinen gleichzeitig'
author: gamevista
type: news
date: 2009-08-23T13:03:50+00:00
excerpt: '<p />[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Jesse Houston, Associate Producer von <a href="http://masseffect.bioware.com">Bioware</a>, kündigte erfreulicherweise an das die PC Version des Rollenspiels <strong>Mass Effect 2</strong> keine längere Entwicklungszeit benötigt als sein Konsolenebenbild. Demnach können sich beide Spielerlager mit verstärktem Hang zum Rollenspiel auf das erste Quartal 2010 gleichermaßen freuen. <br /> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Denn der Nachfolger des beliebten Action RPGs wird gleichzeitig für die Microsoft Konsole und PC erscheinen. Eine längere Wartezeit ist somit ausgeschlossen. Wie es mit der PlayStation 3 Umsetzung aussieht wurde leider nicht näher benannt.

* * *



* * *

 