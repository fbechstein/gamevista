---
title: Bioshock 2 – Neuer DLC verschoben
author: gamevista
type: news
date: 2010-04-28T18:31:22+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Publisher <strong>2K Games</strong> kündigte vor kurzem den Veröffentlichungstermin für das nächste Download-Paket mit dem Namen<strong> Rapture Metro</strong> für den Ego-Shooter<a href="http://www.bioshock2game.com" target="_blank"> Bioshock 2</a> an. Nun wurde das Release des ursprünglich für den 29. April 2010 geplanten DLC auf unbestimmte Zeit verschoben. Gründe bzw. einen neuen Termin gab der Publisher nicht an.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Sicher wird es aber bald neue Details dazu geben. Wir halten euch auf dem Laufenden.

 

 

   

* * *

   

