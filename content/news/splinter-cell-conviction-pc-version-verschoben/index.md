---
title: 'Splinter Cell: Conviction – PC Version verschoben'
author: gamevista
type: news
date: 2010-03-20T12:59:43+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Eigentlich sollte die PC-Version des Taktik-Shooters <a href="http://www.splintercell.us.ubi.com" target="_blank">Splinter Cell: Conviction</a> zusammen mit der Xbox 360-Version am 15. April 2010 im Handel erscheinen. Nun hat der Publisher <strong>Ubisoft </strong>im Rahmen einer Pressemitteilung den PC-Release um zwei Wochen nach hinten verlegt. Demnach wird <a href="http://www.splintercell.us.ubi.com" target="_blank">Splinter Cell: Conviction</a> für PC Fans erst am 30. April im Laden stehen.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
Gründe für die Verschiebung nannte der Publisher nicht. Allerdings könnte der  neue Kopierschutz, der eine ständige Internetverbindung voraussetzt, damit zutun haben.

 

<cite></cite>

   

* * *

   



 