---
title: 'Armed Assault 2: Operation Arrowhead – Erreicht Goldstatus'
author: gamevista
type: news
date: 2010-06-07T15:40:33+00:00
excerpt: '<p>[caption id="attachment_293" align="alignright" width=""]<img class="caption alignright size-full wp-image-293" src="http://www.gamevista.de/wp-content/uploads/2009/08/arma2_small.png" border="0" alt="Armed Assault 2" title="Armed Assault 2" align="right" width="140" height="100" />Armed Assault 2[/caption]Das Entwicklerstudio <strong>Bohemia Interactive</strong> verkündeten heute den Goldstatus für ihr erstes Erweiterungspaket <a href="http://www.arma2.com" target="_blank">Armed Assault 2: Operation Arrowhead</a>. Demnach wird dem Release am 29. Juni 2010 nichts mehr im Wege stehen. Das Addon wird neben einer neuen Kampagne und zahlreichen neuen Fahrzeugen auch weitere Waffen in die Militärsimulation integrieren.</p> '
featured_image: /wp-content/uploads/2009/08/arma2_small.png

---
<a href="http://www.arma2.com" target="_blank">Armed Assault 2: Operation Arrowhead</a> wird ohne das Hauptprogramm auskommen und damit eigenständig laufen.

**Features:**

  * Neuer spielbarer Inhalt 
  * Neue Story-Kampagne für Einzel- und Mehrspielermodus. Zahlreiche neue Tutorials, 
  * Einzelszen arios und Multiplayer-Modi
  * Umfangreiche Spielwelt 
  * Drei brandneue, groβe, zentralasiatisch anmutende Karten, inklusive weitreichender Städte-, Wüsten- und Berglandschaften samt komplett zerstörbarer und interaktiver Umgebung 
  * Zusätzliche Einheiten und Fahrzeuge 
  * Zahlreiche Fraktionen, darunter US-Armee, Vereinte Nationen, takistanische Armee und Guerillas, die zusammen über 300 neue Einheiten, Waffen und Fahrzeuge darstellen 
  * Grenzt nahtlos an die unvergleichliche Atmosphäre von Arma 2 an. 
  * Einzigartige GAMEPLAY-ELEMENTE 
  * Abnehmbare Rucksäcke samt Ausrüstung, fortgeschrittene Waffentechnik, Darstellungen von Materialschäden, Echtzeit-Simulationen von Flugdrohnen, Entscheidungsfreiheiten und Vergeltungsmaßnahmen

   

* * *

   

