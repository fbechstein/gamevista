---
title: 'Battlefield: Bad Company 2 – Releasetermin bekannt'
author: gamevista
type: news
date: 2009-08-18T20:12:56+00:00
excerpt: '<p class="MsoNormal"> </p> <p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]<a href="http://www.electronic-arts.d" target="_blank">Electronic Arts</a> hat per Pressemitteilung auf der gamescom 2009 in Köln den Release Termin für ihr Ego Shooter <strong>Battlefield: Bad Company 2</strong> bekannt gegeben. Demnach wird <strong>Battlefield: Bad Company 2</strong> am 05. März 2010 in Europa in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
**Bad Company 2** wird anders als sein Vorgänger auch für den PC erscheinen und wird neben der gewohnten Kampagne auch Fahrzeuge im Multiplayer Modus enthalten.

* * *



* * *

 

<p class="MsoNormal">
   
</p>

 