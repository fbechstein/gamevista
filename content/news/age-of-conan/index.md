---
title: Age of Conan – Details zum neuen Patch
author: gamevista
type: news
date: 2009-07-25T12:18:47+00:00
excerpt: '<p><img class="caption alignright size-full wp-image-155" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderranges_1024x768_small.jpg" border="0" alt="Age of Conan" title="Age of Conan" align="right" width="140" height="100" />Wie im offiziellen Forum von <a href="http://www.ageofconan.com" target="_blank" title="funcom" rel="nofollow">Funcom </a>mitgeteilt wurde, arbeiten sie momentan akribisch an der Fertigstellung des 6. Updates für <strong>Age of Conan: Hyborian Adventures</strong>. Leider gibt es noch keinen genauen Veröffentlichungstermin.</p> '
featured_image: /wp-content/uploads/2009/07/borderranges_1024x768_small.jpg

---
<p><img class="caption alignright size-full wp-image-155" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderranges_1024x768_small.jpg" border="0" alt="Age of Conan" title="Age of Conan" align="right" width="140" height="100" />Wie im offiziellen Forum von <a href="http://www.ageofconan.com" target="_blank" title="funcom" rel="nofollow">Funcom </a>mitgeteilt wurde, arbeiten sie momentan akribisch an der Fertigstellung des 6. Updates für <strong>Age of Conan: Hyborian Adventures</strong>. Leider gibt es noch keinen genauen Veröffentlichungstermin.</p>

Mit Haus der Crom und Toth-Amons Festung werden 2 neue Gebiete in Hyboria Einzug halten. Ausserdem sind neue PvP Verbesserungen geplant. News hierzu gibt es in Kürze.   
Hauptziel des Patches ist aber die Gildenstrukturen zu stärken. Es wird ein neues System eingeführt mit dem einzelne Mitglieder den Ruf ihrer Gilde steigern können. In den Bereichen Handwerk, PvE bzw. PvP wird es Möglichkeiten geben Aufgaben für die Gilde zu erfüllen. <a href="http://www.ageofconan.com" target="_blank" title="funcom" rel="nofollow">Funcom </a>sagt zudem das **Age of Conan** Gilden sich in einer Rangliste messen können.







