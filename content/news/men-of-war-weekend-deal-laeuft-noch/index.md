---
title: Men of War – Weekend Deal läuft noch
author: gamevista
type: news
date: 2009-07-27T15:33:18+00:00
excerpt: '<p>[caption id="attachment_174" align="alignright" width=""]<img class="caption alignright size-full wp-image-174" src="http://www.gamevista.de/wp-content/uploads/2009/07/men_of_war.jpg" border="0" alt="Men of War" title="Men of War" align="right" width="140" height="100" />Men of War[/caption]Es ist Montag, das Wochenende schon wieder rum und ein Mancher ärgert sich vielleicht darüber das er schon wieder den <a href="http://store.steampowered.com" target="_blank" title="steam">Steam Weekend Deal</a> verpasst hat.  Nun da haben wir eine gute Nachricht für euch. <strong>Men of War</strong> ist weiterhin um 75% billiger zu erhalten.</p>'
featured_image: /wp-content/uploads/2009/07/men_of_war.jpg

---
Das im Rahmen des <a href="http://store.steampowered.com" target="_blank" title="steam">Steam Weekend Deals</a> sehr günstig zu ersteigernde Strategie Spiel **Men of War** gibt es immer noch für 7 Euro im <a href="http://store.steampowered.com" target="_blank" title="steam">Steam Shop</a>. Im WW2 Echtzeitstrategiehit von <a href="http://www.1cpublishing.eu" target="_blank" title="1c company">1C Company </a>steuert ihr eine von 4 Fraktionen (USA, Russland, Deutschland, England – bald auch Japan). Dabei wird sehr auf Realismus und genaue Schadensmodelle geachtet. Bei den vielen verschiedenen Fahrzeugen könnt ihr zb. Ketten oder Turm oder Motor eines Panzers einzeln anvisieren. Multiplayerschlachten für bis zu 8 gegen 8 Spieler sind auch möglich.









 