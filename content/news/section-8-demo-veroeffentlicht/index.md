---
title: Section 8 – Demo veröffentlicht
author: gamevista
type: news
date: 2009-08-23T12:40:37+00:00
excerpt: '<p>[caption id="attachment_450" align="alignright" width=""]<img class="caption alignright size-full wp-image-450" src="http://www.gamevista.de/wp-content/uploads/2009/08/section8.jpg" border="0" alt="Section 8" title="Section 8" align="right" width="140" height="100" />Section 8[/caption]Entwickler <a href="http://www.joinsection8.com/">Timegate </a>veröffentlichte nun die Xbox 360 Demo zum Science-Fiction Shooter <strong>Section 8</strong>. Die Demo wird eine Karte enthalten worauf man bis zu 16 Spielern um zwei Basen kämpfen kann. Natürlich könnt ihr Anfangs aus mehreren Klassen wählen, diese dürfen dann auch individuell Angepasst werden. So könnt ihr als Sniper auch einen Raketenwerfer anlegen.</p> '
featured_image: /wp-content/uploads/2009/08/section8.jpg

---
Besonders der Start der Runden wird interessant sein, denn ihr könnt per Landungskapsel selbst bestimmen wo ihr am Schlachtgeschehen teilnehmt. Die Demo gibts wie immer auf dem [Xbox Live Marktplatz][1] zu finden. **Section 8** erscheint am 4. September auf Xbox 360 und PC.

 

* * *



* * *

 

 [1]: http://marketplace.xbox.com/en-US/games/media/66acd000-77fe-1000-9115-d802475007d4/?of=1