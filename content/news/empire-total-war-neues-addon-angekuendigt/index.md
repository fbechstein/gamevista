---
title: 'Empire: Total War – Neues Addon angekündigt'
author: gamevista
type: news
date: 2009-09-21T20:41:51+00:00
excerpt: '<p>[caption id="attachment_736" align="alignright" width=""]<img class="caption alignright size-full wp-image-736" src="http://www.gamevista.de/wp-content/uploads/2009/09/empire_thewarpath.jpg" border="0" alt="Empire: Total War" title="Empire: Total War" align="right" width="140" height="100" />Empire: Total War[/caption]Wie Publisher <a href="http://www.sega.com">SEGA </a>während des heutigen Tages im Zuge einer Pressemeldung mitteilte, wird es ein Addon für das <strong>Strategiespiel Empire: Total War</strong> geben. Der Name wird <strong>The Warpath Campaign</strong> sein und bietet euch eine neue Kampagne für Nordamerika.</p> '
featured_image: /wp-content/uploads/2009/09/empire_thewarpath.jpg

---
Unter anderem werden insgesamt fünf neue Indianerstämme eingeführt. Diese könnt ihr auch im Multiplayerpart nutzen. Darunter sein werden die Irokesen, Pueblo Indianer, Prärie Indianer, Huronen und Cherokee sein. Jede diese Völker hat wieder unterschiedliche Fähigkeiten und Eigenschaften. **The Warpath Campaign** wird nach derzeitiger Planung im Oktober 2009 veröffentlicht. Es soll exklusiv über Steam für 6,99 Euro erhältlich sein.    
Bei uns findet ihr ab sofort den neuen Trailer zum Addon, wir wünschen viel Spaß beim anschauen.

[> Zum Empire: Total War &#8211; The Warpath Campaign Announcement Trailer][1]

 

* * *



* * *

 

 [1]: videos/item/root/empire-total-war-the-warpath-campaign-trailer