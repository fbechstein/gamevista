---
title: Left 4 Dead 2 – Valve verkündet Demo Termin
author: gamevista
type: news
date: 2009-10-15T16:59:46+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Der Entwickler <a href="http://www.valvesoftware.com" target="_blank">Valve</a> hat heute den Termin für die Demo des Zombieshooter <strong>Left 4 Dead 2</strong> verkündet. Auch gibt es nun Details zur Antestversion. So wird es möglich sein die <strong>Demo </strong>mit bis zu 4 Mitspielern gemeinsam zu bewältigen.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Als Kapitel wird die „**The Parish**“ Kampagne anspielbar sein. Die neuen Bossinfizierten werden auch dabei sein, sowie die Nahkampfwaffen die in der Vollversion enthalten sind. Vorbesteller dürfen schon am 27. Oktober 2009 loslegen. Xbox 360 Gold Member und PC Besitzer dürfen am 3. November 2009 die Leitung glühen lassen. 

Xbox 360 Silber Member dürfen **Left 4 Dead 2** erst am 10. November 2009 antesten.    
**Left 4 Dead 2** erscheint am 19. November 2009 im deutschen Handel.

 

* * *



* * *

 