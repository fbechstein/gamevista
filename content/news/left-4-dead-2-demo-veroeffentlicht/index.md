---
title: Left 4 Dead 2 – Demo veröffentlicht
author: gamevista
type: news
date: 2009-11-03T23:00:00+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Der Entwickler <a href="http://store.steampowered.com/app/590/" target="_blank">Valve</a> hat heute seine Demo zum lang erwarteten Nachfolger von <strong>Left 4 Dead</strong> veröffentlicht. Während Vorbesteller schon letzte Woche in den Genuss der Zombiejagd kamen, liefert <a href="http://store.steampowered.com/app/590/" target="_blank">Valve</a> heute nun endlich die Demo für den Rest der Fangemeinde. So können Xbox 360 Besitzer mit einem Xbox Live Gold Account und PC-Besitzer ab sofort die Demo downloaden.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Xbox 360 Kunden ohne Gold Account können erst ab dem 10. November die Testversion ausprobieren.

In der Demo schlüpft ihr in die Rolle der neuen Protagonisten Coach, Ellis, Nick oder Rochelle und müsst euch gegen eine wahre Flut Untoter beweisen. Ihr bekommt dabei zwei Level der Kampagne &#8222;The Parish&#8220; zu sehen die in New Orleans spielt. Es wird auch die neuen Bossinfizierten und Nahkampfwaffen zu bestaunen geben. Wenn euch die Demo gefallen sollte müsst ihr euch noch bis zum 17. November gedulden, denn dann soll der Vollversion im Handel erscheinen. Der Download der Demo wird über den Onlinedienst <a href="http://store.steampowered.com/app/590/" target="_blank">Steam</a> bzw. über den <a href="http://marketplace.xbox.com/de-DE/games/offers/0ddf0002-0000-4000-8000-0000454188d6?partner=RSS" target="_blank">Xbox Live Markplatz</a> laufen.

* * *



* * *