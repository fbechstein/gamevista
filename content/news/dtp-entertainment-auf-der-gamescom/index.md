---
title: DTP Entertainment auf der gamescom
author: gamevista
type: news
date: 2009-08-05T09:53:54+00:00
excerpt: '<p>[caption id="attachment_282" align="alignright" width=""]<img class="caption alignright size-full wp-image-282" src="http://www.gamevista.de/wp-content/uploads/2009/08/venetica_small.png" border="0" alt="Venetica wird auf der gamescom vertreten sein" title="Venetica wird auf der gamescom vertreten sein" align="right" width="140" height="100" />Venetica wird auf der gamescom vertreten sein[/caption]<a href="http://www.dtp-entertainment.com/" target="_blank">DTP Entertainment</a> wird mit insgesamt 16 Spielstationen auf der gamescom in Köln vertreten sein. Am Stand B030 in Halle 8.1 könnt Ihr dann Spiele wie <strong>Black Mirror 2</strong>, <strong>Venetica</strong> und<strong> Drakensang: Am Fluss der Zeit</strong> anspielen.</p>'
featured_image: /wp-content/uploads/2009/08/venetica_small.png

---




