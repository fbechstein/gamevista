---
title: 'Stalker: Call of Pripyat  – erreicht Goldstatus'
author: gamevista
type: news
date: 2009-09-20T16:29:57+00:00
excerpt: '<p>[caption id="attachment_728" align="alignright" width=""]<img class="caption alignright size-full wp-image-728" src="http://www.gamevista.de/wp-content/uploads/2009/09/stalker_cop.jpg" border="0" alt="S.T.A.L.K.E.R.: Call of Pripyat" title="S.T.A.L.K.E.R.: Call of Pripyat" align="right" width="140" height="100" />S.T.A.L.K.E.R.: Call of Pripyat[/caption]Wie das russische Entwicklerteam <a href="http://www.gsc-game.com">GSC Game World</a> nun offiziell bekannt gegeben hat, wurden die Arbeiten am Standalone Ego Shooter <strong>S.T.A.L.K.E.R. : Call of Pripyat</strong> abgeschlossen. Demnach wurde nun der Goldstatus verkündet und das Spiel befindet sich auf dem Weg in das Presswerk um fleißig Kopien herstellen zu können.</p> '
featured_image: /wp-content/uploads/2009/09/stalker_cop.jpg

---
Somit dürfte dem Erscheinungstermin am 5. November 2009 hierzulande nichts mehr im Wege stehen.

 

* * *



* * *

 