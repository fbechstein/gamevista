---
title: Demigod Multiplayer Demo ist online
author: gamevista
type: news
date: 2009-07-31T19:52:29+00:00
excerpt: '<p>[caption id="attachment_238" align="alignright" width=""]<img class="caption alignright size-full wp-image-238" src="http://www.gamevista.de/wp-content/uploads/2009/07/demigod_small.png" border="0" alt="Demigod" title="Demigod" align="right" width="140" height="100" />Demigod[/caption]Ab sofort findet Ihr im Downloadbereich die Multiplayer Demo zu <strong>Demigod</strong>. Bei der Demigod Multiplayer Demo habt Ihr die Möglichkeit alle Multiplayer Modi (Conquest, Dominate, Fortress und Slaughter) auszuprobieren. Es stehen Euch vier Demigods (Regulus, Rook, Sedna und Lord Erebus) auf der Cataract-Karte zur Verfügung.</p>'
featured_image: /wp-content/uploads/2009/07/demigod_small.png

---
Die Demo umfasst 600 MB und kann [>hier<][1] heruntergeladen werden.

&nbsp;







&nbsp;

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=131