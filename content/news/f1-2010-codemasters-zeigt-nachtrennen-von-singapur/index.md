---
title: F1 2010 – Codemasters zeigt Nachtrennen von Singapur
author: gamevista
type: news
date: 2010-09-14T17:45:12+00:00
excerpt: '<p>[caption id="attachment_1800" align="alignright" width=""]<img class="caption alignright size-full wp-image-1800" src="http://www.gamevista.de/wp-content/uploads/2010/05/f1_2010.jpg" border="0" alt="F1 2010" title="F1 2010" align="right" width="140" height="100" />F1 2010[/caption]Publisher <strong>Codemasters </strong>hat einen neuen Trailer zum bald erscheinenden Rennspiel <a href="http://formula1-game.com/" target="_blank">F1 2010</a> veröffentlicht. Diesmal wird die Grand Prix-Strecke in Singapur gezeigt, die typisch bei Nacht und im gleißenden Scheinwerferlicht befahren wird.</p> '
featured_image: /wp-content/uploads/2010/05/f1_2010.jpg

---
Außerdem gibt es erstmals Bilder der neuen Strecke in Abu Dhabi mit dem Namen Yas Marina Circuit zu sehen. <a href="http://formula1-game.com/" target="_blank">F1 2010</a> wird für PC, Xbox 360 und PlayStation 3 am 23. September 2010 in den Handel kommen.

[> Zum F1 2010 &#8211; Nightrace Trailer][1]

   

* * *

   



 [1]: videos/item/root/f1-2010-nightrace-trailer