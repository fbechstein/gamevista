---
title: Starcraft 2 – Neue Infos zur ersten Episode
author: gamevista
type: news
date: 2010-01-04T18:43:57+00:00
excerpt: '<p>[caption id="attachment_1261" align="alignright" width=""]<img class="caption alignright size-full wp-image-1261" src="http://www.gamevista.de/wp-content/uploads/2010/01/starcraft2_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="88" />Starcraft 2[/caption]Die erste Episode des Strategiespiels <strong>Starcraft 2: Wings of Liberty</strong> vom Entwicklerstudio Blizzard wird auch eine Art Prolog zur bald darauf erscheinende Protoss Kampagne bieten. Nun hat ein <a href="http://www.starcraft2.com" target="_blank">Blizzard</a> Mitarbeiter weitere <a href="http://forums.battle.net/thread.html?topicId=22048397260&postId=220474735026&sid=3000#2" target="_blank">Details</a> bekannt gegeben. So wird die Minikampagne wohl nur ein paar Missionen haben und als Teaser für den kommenden Protoss-Part agieren.</p> '
featured_image: /wp-content/uploads/2010/01/starcraft2_small.jpg

---
**Starcraft 2** wird in drei Teilen erscheinen und **Wings of Liberty** wird dabei den Start der Trilogie bilden. Hier wird es hauptsächlich um die Geschichte der Terraner gehen. **Starcraft 2: Wings of Liberty** soll 2010 erscheinen. Die beiden anderen Teile **Legacy of the Void** und **Heart of the Swarm** folgend später.

 

* * *



* * *

 