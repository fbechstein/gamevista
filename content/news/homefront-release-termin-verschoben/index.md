---
title: Homefront – Release-Termin verschoben
author: gamevista
type: news
date: 2010-09-17T17:50:38+00:00
excerpt: '<p>[caption id="attachment_1910" align="alignright" width=""]<img class="caption alignright size-full wp-image-1910" src="http://www.gamevista.de/wp-content/uploads/2010/06/homefront.jpg" border="0" alt="Homefront" title="Homefront" align="right" width="140" height="100" />Homefront[/caption]Publisher <strong>THQ </strong>hat den Veröffentlichungstermin für den Ego-Shooter <a href="http://www.homefront-game.com" target="_blank">Homefront</a> verschoben. Das Entwicklerteam von den <strong>Kaos Studios</strong> werden nicht wie geplant, den Titel im Februar 2011 in den Handel bringen, sondern erst im März nächsten Jahres veröffentlichen.</p> '
featured_image: /wp-content/uploads/2010/06/homefront.jpg

---
<a href="http://www.homefront-game.com/" target="_blank">Homefront</a> spiegelt den fiktiven Konflikt zwischen den wirtschaftlich angeschlagenen USA und dem wiedervereinigten Korea wieder. Mit einem Angriff von Korea erlebt der Nordamerikanische Kontinent erstmals eine Invasion in dem der Spieler sich dem zivilen Widerstand anschließt. Dabei können sie die Kontrolle über zahlreiche Luft- und Bodenfahrzeuge übernehmen. Neben der Einzelspielerkampagne liefert <a href="http://www.homefront-game.com/" target="_blank">Homefront</a> einen umfassenden Mehrspielerteil.

 

   

* * *

   

