---
title: 'The Elder Scrolls 5: Skyrim – Offiziell angekündigt'
author: gamevista
type: news
date: 2010-12-12T14:11:44+00:00
excerpt: '<p>[caption id="attachment_2485" align="alignright" width=""]<img class="caption alignright size-full wp-image-2485" src="http://www.gamevista.de/wp-content/uploads/2010/12/skyrim.gif" border="0" alt="The Elder Scrolls 5: Skyrim" title="The Elder Scrolls 5: Skyrim" align="right" width="140" height="100" />The Elder Scrolls 5: Skyrim[/caption]Der offizielle Nachfolger von <strong>Bethesdas Oblivion</strong> ist per Pressemitteilung heute angekündigt worden. Der fünfte Teil der Elder Scrolls-Reihe, <a href="http://www.bethsoft.com" target="_blank">The Elder Scrolls 5: Skyrim</a>, wird am 11. November 2011 in den Handel kommen. Der Nachfolger spielt nach den Ereignissen von Oblivion in dem Dörfchen Tamriel.</p> '
featured_image: /wp-content/uploads/2010/12/skyrim.gif

---
Erste Einblicke verschafft der kürzlich erschienene Debüt-Trailer.

[> Zum The Elder Scrolls 5: Skyrim &#8211; Debüt-Trailer][1]

   

* * *

   



 [1]: videos/item/root/the-elder-scrolls-5-skyrim-teaser