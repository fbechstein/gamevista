---
title: 'Star Wars: The Clone Wars: Republic Heroes – Release verschoben'
author: gamevista
type: news
date: 2009-07-26T12:22:58+00:00
excerpt: '[caption id="attachment_159" align="alignright" width=""]<img class="caption alignright size-full wp-image-159" src="http://www.gamevista.de/wp-content/uploads/2009/07/cwrh4_small.jpg" border="0" alt="Star Wars The Clone Wars Republic Heroes" title="Star Wars The Clone Wars Republic Heroes" align="right" width="140" height="100" />Star Wars The Clone Wars Republic Heroes[/caption] Wie <a href="http://www.kromestudios.com" target="_blank" title="krome studios">Krome Studios</a> nun offiziell im Rahmen der Comicon in San Diego verkündet haben, wird sich der Release von <strong>Star Wars: The Clone Wars: Republic Heroes</strong> leider verschieben. <br />'
featured_image: /wp-content/uploads/2009/07/cwrh4_small.jpg

---
Der ursprüngliche Termin vom 15. September kann nicht mehr eingehalten werden. Das Release wird damit auf den 06. Oktober verschoben. Hoffen wir auf eine spannende Story und heiße Co-op Gefechte.





