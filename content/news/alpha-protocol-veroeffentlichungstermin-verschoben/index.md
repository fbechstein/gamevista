---
title: Alpha Protocol – Veröffentlichungstermin verschoben
author: gamevista
type: news
date: 2009-10-07T11:38:30+00:00
excerpt: '<p>[caption id="attachment_119" align="alignright" width=""]<img class="caption alignright size-full wp-image-119" src="http://www.gamevista.de/wp-content/uploads/2009/07/ap_small.jpg" border="0" alt="Alpha Protocol" title="Alpha Protocol" align="right" width="140" height="100" />Alpha Protocol[/caption]Der Publisher <a href="http://www.sega.de" target="_blank">Sega</a> hat heute bekannt gegeben das das Spiel <strong>Alpha Protocol</strong> auf das Frühjahr 2010 verschoben wurde. Bisher war auf der <a href="http://www.sega.com/games/alpha-protocol/?t=EnglishUSA&gseoid=alpha-protocol" target="_blank">offiziellen Webseite</a> des Publishers ein Veröffentlichungstermin mit Oktober 2009 angegeben. Umso mehr verwundert jetzt der doch etwas in der Ferne liegende Termin.</p> '
featured_image: /wp-content/uploads/2009/07/ap_small.jpg

---
Auf Amazon.de wird **Alpha Protocol** jetzt am 16. April 2010 gelistet. Scheinbar braucht der Entwickler Obsidian noch etwas mehr Zeit für die Fertigstellung. Details für die Verschiebung wurden allerdings bisher nicht benannt. **Alpha Protocol** soll sowohl für Xbox 360, als auch für PlayStation 3 und PC erscheinen.

 

* * *



* * *

 