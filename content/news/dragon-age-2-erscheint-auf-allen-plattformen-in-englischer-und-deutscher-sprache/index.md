---
title: Dragon Age 2 – Erscheint auf allen Plattformen in englischer und deutscher Sprache
author: gamevista
type: news
date: 2011-01-11T18:29:10+00:00
excerpt: '<p>[caption id="attachment_1996" align="alignright" width=""]<img class="caption alignright size-full wp-image-1996" src="http://www.gamevista.de/wp-content/uploads/2010/07/dragonage21.jpg" border="0" alt="Dragon Age 2" title="Dragon Age 2" align="right" width="140" height="100" />Dragon Age 2[/caption]Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Pressemitteilung bekannt gegeben, dass der Rollenspiel Titel <a href="http://dragonage.bioware.com/da2/" target="_blank">Dragon Age 2</a> in Deutschland auf allen Plattformen in englischer und deutscher Sprachausgabe erscheinen wird. <a href="http://dragonage.bioware.com/da2/" target="_blank">Dragon Age 2</a> erscheint am 10. März 2011 für die Xbox 360, PlayStation 3 sowie PC.</p> '
featured_image: /wp-content/uploads/2010/07/dragonage21.jpg

---
Die limitierte Sonderedition von <a href="http://dragonage.bioware.com/da2/" target="_blank">Dragon Age 2</a> mit dem Namen BioWare Signature Edition, kann noch bis heute Abend vorbestellt werden. Diese enthält drei zusätzliche Missionen, den Soundtrack des Spiels zum Download und eine Ingame-Waffenkammer mit exklusiven Waffen. Zu erwerben ist diese per Amazon.de und Gamestop.

 

   

* * *

   

