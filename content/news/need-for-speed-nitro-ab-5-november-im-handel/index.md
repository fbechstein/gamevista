---
title: 'Need for Speed: NITRO – Ab 5. November im Handel'
author: gamevista
type: news
date: -001-11-30T00:00:00+00:00
excerpt: '<p>[caption id="attachment_909" align="alignright" width=""]<img class="caption alignright size-full wp-image-909" src="http://www.gamevista.de/wp-content/uploads/2009/10/small_nfs_nitro.jpg" border="0" alt="Need for Speed: NITRO" title="Need for Speed: NITRO" align="right" width="140" height="100" />Need for Speed: NITRO[/caption]Der Publisher <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a> hat heute per offizieller Pressemitteilung bekannt gegeben das das Rennspiel <strong>Need for Speed: NITRO</strong> ab dem 5. November im Handel erhältlich ist. <br /> Das speziell für die Wii und Nintendo DS entwickelte Rennspiel ist ein unterhaltsamer und zugleich aufregender Arcade Racer.</p> '
draft: true
featured_image: /wp-content/uploads/2009/10/small_nfs_nitro.jpg

---
Dazu kommt noch ein fesselnder Soundtrack der mit Songs einiger weltbekannter und innovativer Künstler gespickt ist. Darunter sind unter anderem k-os, Placebo und Crystal Method. **Need for Speed: NITRO** bietet Single- sowie Multiplayer Rennen. Unter der <a href="http://www.ea.com/eatrax/" target="_blank">offizellen Website</a> von EA könnt ihr in jeden Song reinhören.

* * *



* * *

 