---
title: 'Need for Speed: Shift – Die Demo ist da!'
author: gamevista
type: news
date: 2009-09-18T17:48:02+00:00
excerpt: '<p>[caption id="attachment_124" align="alignright" width=""]<img class="caption alignright size-full wp-image-124" src="http://www.gamevista.de/wp-content/uploads/2009/07/shift_small.jpg" border="0" alt="Need for Speed: Shift" title="Need for Speed: Shift" align="right" width="140" height="100" />Need for Speed: Shift[/caption]Publisher <a href="http://www.ea.com/games/need-for-speed-shift">Electronic Arts</a> hat die Demo zum Rennspiel <strong>Need for Speed: Shift</strong> veröffentlicht. Insgesamt sind fünf Rennboliden und zwei Strecken enthalten. Entweder geht’s auf den Circuit de Spa Francorchamps oder auf die fiktive Strecke in der Londoner City.</p> '
featured_image: /wp-content/uploads/2009/07/shift_small.jpg

---
Als Rennwagen stehen der Nissan GT-R, BMW M3, Lotus Elise oder ein Dodge Viper zur Verfügung. Einen Bonus wagen gilt es Freizuschalten. Hier wartet der Pagani Zonda F auf euch. Die Demo umfasst ca. 1 Gigabyte an Daten die ihr natürlich bei uns herunterladen könnt.

[> Zum Need for Speed: Shift &#8211; Demo Download][1]

 

 

* * *



* * *

 

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=290