---
title: Heavy Rain – PS3 Bundle enthüllt
author: gamevista
type: news
date: 2010-01-20T19:59:35+00:00
excerpt: '<p>[caption id="attachment_1338" align="alignright" width=""]<img class="caption alignright size-full wp-image-1338" src="http://www.gamevista.de/wp-content/uploads/2010/01/heavyrain.jpg" border="0" alt="Heavy Rain" title="Heavy Rain" align="right" width="140" height="100" />Heavy Rain[/caption]Das französische Versandhaus <a href="http://www.amazon.fr/Console-PS3-Slim-Heavy-rain/dp/B0033WSJXC/ref=sr_1_12?ie=UTF8&s=videogames&qid=1264018032&sr=8-12" target="_blank">Amazon.fr</a> listet aktuell ein P<a href="http://www.amazon.fr/Console-PS3-Slim-Heavy-rain/dp/B0033WSJXC/ref=sr_1_12?ie=UTF8&s=videogames&qid=1264018032&sr=8-12" target="_blank">layStation 3 Paket</a>, dass für 349.95 EUR zusammen mit dem neuen <strong>Heavy Rain</strong> auf den Markt kommt. Das Paket beinhaltet eine 250 Gb PlayStation 3 und eine Kopie des Spiels. Leider ist das Bundle bisher nur in Frankreich verfügbar, der Grund ist denkbar einfach.</p> '
featured_image: /wp-content/uploads/2010/01/heavyrain.jpg

---
Der Entwickler **Quantic Dream** hat seinen Firmensitz ebenfalls in dem Land. <a href="http://www.amazon.fr/Console-PS3-Slim-Heavy-rain/dp/B0033WSJXC/ref=sr_1_12?ie=UTF8&#038;s=videogames&#038;qid=1264018032&#038;sr=8-12" target="_blank">Die PlayStation 3 Version mit Heavy Rain</a> erscheint als Paket am 24. Februar 2010, an dem Tag erscheint das Spiel auch in ganz Europa. Ob es das Bundle auch bei uns geben wird ist fraglich.

<p style="text-align: center;">
  <img class=" size-full wp-image-1339" src="http://www.gamevista.de/wp-content/uploads/2010/01/heavyrainps3.jpg" border="0" width="456" height="418" srcset="http://www.gamevista.de/wp-content/uploads/2010/01/heavyrainps3.jpg 456w, http://www.gamevista.de/wp-content/uploads/2010/01/heavyrainps3-300x275.jpg 300w" sizes="(max-width: 456px) 100vw, 456px" />
</p>

* * *



* * *