---
title: 'Assassin’s Creed: Bloodlines – Interview mit Frederic Lefrancois'
author: gamevista
type: news
date: 2009-07-17T10:21:06+00:00
excerpt: '<p>[caption id="attachment_63" align="alignright" width=""]<img class="caption alignright size-full wp-image-63" src="http://www.gamevista.de/wp-content/uploads/2009/07/acb_small1.png" border="0" alt="Assassin‘s Creed: Bloodlines" title="Assassin‘s Creed: Bloodlines" align="right" width="140" height="100" />Assassin‘s Creed: Bloodlines[/caption]<a href="http://www.ubi.com/de/" target="_blank">Ubisoft</a> kündigte die Veröffentlichung von<strong> Assassin‘s Creed: Bloodlines</strong> an. Das Spiel wird von den Griptonite Studios unter Aufsicht des Assassin’s Creed-Kernteams von Ubisoft Montreal für die Playstation Portable entwickelt. Zeitlich setzt der Titel direkt nach dem Ende des Vorgängers an und erzählt Altairs Geschichte weiter. Diesmal führt es ihn in die Städte Limasol und Kyrenia auf Zypern, einer wunderschönen und historisch ungemein reizvollen Insel im Mittelmeer. Diese kann in typischer Assassin‘s Creed Manier erkundet werden und bieten 8 neue Missionstypen die das Spielerlebnis ergänzen. Dass diese offene Welt und das dynamische Spielprinzip auf der PSP passieren, stellt einen technischen Meilenstein für das System dar.</p><p>Frederic Lefrancois, 3rd Party Producer für Assassin’s Creed: Bloodlines, stand Rede und Antwort zum neuem PSP Abenteuer.</p>'
featured_image: /wp-content/uploads/2009/07/acb_small1.png

---
&nbsp;

**Bitte stelle dich kurz vor und erläutere deine Rolle in dem Projekt.** 

_Frederic Lefrancois:_ Mein Name ist Frederic Lefrancois und ich bin 3rd Party Producer für Assassin’s Creed: Bloodlines. Meine Aufgabe ist es, sicherzustellen, dass das Spiel pünktlich zum Weihnachtsgeschäft erscheint und das höchstmögliche Maß an Qualität aufweist. Ich arbeite Hand in Hand mit den Entwicklern von “Assassin’s Creed: Bloodlines” und dem Assassin’s Creed-Kernteam von Ubisoft Montreal zusammen, um ein großartiges Spiel zu schaffen, das den Maßstäben, die der Vorgänger gesetzt hat, gerecht wird und alle charakteristischen Eigenschaften der Marke „Assassin’s Creed“ beinhaltet.

**Wer zeichnet sich für die Entwicklung verantwortlich?**

_Frederic Lefrancois:_ Das Spiel wird von dem Griptonite Studio unter Aufsicht des Assassin’s Creed-Kernteams von Ubisoft Montreal entwickelt.

**Wie ist Assassin’s Creed PSP in das Assassin’s Creed-Universum eingebunden?  
**   
_Frederic Lefrancois:_ “Assassin’s Creed: Bloodlines” führt die Geschichte Altairs unmittelbar nach den Ereignissen von Assassin’s Creed fort und handelt von seinen Kampf gegen den neu erstarkten Templerorden auf Zypern. Es ist das einzige Spiel, das die Verbindung zwischen Assassins’s Creed und Assassin’s Creed 2 erklärt. Wer also verstehen will, wie Ezio geboren wurde und woher sein Kodex stammt, kommt um diesen Titel nicht herum. 



**Was ist die Handlung des Spiels? Kannst du uns eine kurze Zusammenfassung geben?**

<figure id="attachment_64" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-64" src="http://www.gamevista.de/wp-content/uploads/2009/07/acb_small2.png" border="0" alt="Assassin‘s Creed: Bloodlines" title="Assassin‘s Creed: Bloodlines" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Assassin‘s Creed: Bloodlines</figcaption></figure>_Frederic Lefrancois:_ Assassin’s Creed PSP beginnt dort, wo Assassin’s Creed endete. Nach dem Tod von Robert De Sable durch die Hand Altairs fliehen die Templer unter der Führung Armand Boucharts aus dem Heiligen Land, um sich auf der Insel Zypern niederzulassen. Altair ist ihnen dicht auf den Fersen, besessen davon, ihrem Streben nach Macht ein für allemal ein Ende zu setzen. Hierzu hilft Altair den Bewohnern der Insel, die Besatzung durch die Templer zu beenden und die Anführer des Ordens auszuschalten.    
In wie fern hat sich Altair als Charakter im PSP-Spiel im Vergleich zu Assassin’s Creed weiterentwickelt?  
Das ihm von seinem Orden indoktrinierte Weltbild liegt in Trümmern und wurde im Blut gemeuchelter Templer ertränkt, so dass Altair den Ereignissen in “Assassin’s Creed: Bloodlines” nun unvoreingenommen begegnen kann. Da er nicht mehr an seinen ehemaligen Meister Al Mualim gebunden ist, ist er nun Herr über seine eigenen Entscheidungen und sein Schicksal. Er ist neugieriger, wissbegieriger und längst nicht mehr so leichtgläubig.

**Welche spielerischen Elemente können wir in “Assassin’s Creed: Bloodlines” erwarten? Wird der Spieler vor jedem Attentat Nachforschungen anstellen können?**

<figure id="attachment_65" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-65" src="http://www.gamevista.de/wp-content/uploads/2009/07/acb_small3.png" border="0" alt="Assassin‘s Creed: Bloodlines" title="Assassin‘s Creed: Bloodlines" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Assassin‘s Creed: Bloodlines</figcaption></figure>_Frederic Lefrancois:_ Die spielerischen Grundlagen des ersten Assassin’s Creed haben wir beibehalten, darunter die für die Reihe charakteristischen Free-Running-Elemente und das intuitive Kampfsystem. Bevor Altair sein Ziel zur Strecke bringt, hat er die Möglichkeit, eine Reihe von Missionen zu erfüllen, die sich nicht auf Nachforschungen beschränken. Es gibt acht neue Missionstypen und wir wollten jeden dieser Aufträge für den Spieler so erfrischend und spannend gestalten wie das schlussendliche Attentat selbst.

**Wie wurden die offene Spielwelt und die völlige Bewegungsfreiheit, die als grundlegende Elemente der Assassin’s Creed-Marke gelten, in der PSP-Version umgesetzt?**

_Frederic Lefrancois:_ Wie auch schon bei Assassin’s Creed wird Altair auch in diesem Spiel verschiedene Großstädte erkunden können. Diesmal führt es ihn in die Städte Limasol und Kyrenia auf Zypern, eine wunderschöne und historisch ungemein reizvolle Insel im Mittelmeer. Selbstverständlich wird Altair durch seine charakteristischen Fähigkeiten auch in diesem Spiel seine Umgebung zu seinem Vorteil ausnutzen: er wird auf sein Können im Free-Running, Akrobatik-Einlagen in luftiger Höhe und seine Kletterkünste zurückgreifen können, um in jeder Situation die Oberhand zu erlangen. Dass wir dies alles auf der PSP möglich machen, stellt einen technischen Meilenstein für das System dar, denn solch eine Bewegungsfreiheit gab es auf der PSP noch nie. 



**  
Warum habt ihr euch für Zypern als Ort der Handlung des PSP-Spiels entschieden?**

<figure id="attachment_66" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-66" src="http://www.gamevista.de/wp-content/uploads/2009/07/acb_small4.png" border="0" alt="Assassin‘s Creed: Bloodlines" title="Assassin‘s Creed: Bloodlines" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Assassin‘s Creed: Bloodlines</figcaption></figure>_Frederic Lefrancois:_ Im Sommer 1191 zog sich der Templerorden aus dem Heiligen Land zurück und ließ sich auf Zypern nieder, das er zuvor von Richard Löwenherz erworben hatte. Sie besetzten die Insel und unterdrückten die Bevölkerung. Als sich diese durch eine Revolution gegen die neuen Herrscher auflehnen, wurde ihr Aufstand von den Templern blutig niedergeschlagen. Wir haben uns für dieses Setting entschieden, da es eine faszinierende historische Epoche repräsentiert, um die sich zudem noch Gerüchte über einen legendären Templerschatz auf Zypern ranken. Die Unterdrückung der Bewohner der Insel ist der ideale Hintergrund, um Altairs Geschichte fortzuführen und einige grundlegenden Elemente von Assassin’s Creed 2 zu erklären. 

**Wird die PS3/PSP-Verbindungsfunktion unterstützt?**

_Frederic Lefrancois:_ In Assassin’s Creed 2 für PS3 wird der Spieler zusätzliche Gesundheit und Aufwertungen für seine versteckten Klingen freischalten können. Diese Boni werden dann auch für Assassin’s Creed: Bloodlines (PSP) verfügbar sein. Dies funktioniert in beide Richtungen: Auf der PS3 wird der Spieler auf jede Templer-Münze und jede Spezialwaffe zugreifen können, die er auf der PSP gefunden hat.

**Vielen Dank für das Interview!**

[> Screenshots zu Assassin’s Creed: Bloodlines][1]  
[> Video zu Assassin’s Creed: Bloodlines][2]







 [1]: screenshots/item/root/assassins-creed-bloodlines
 [2]: videos/item/root/-assassins-creed-bloodlines-trailer