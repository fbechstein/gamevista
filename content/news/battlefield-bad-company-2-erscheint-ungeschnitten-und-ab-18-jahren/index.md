---
title: 'Battlefield: Bad Company 2 – Erscheint ungeschnitten und ab 18 Jahren'
author: gamevista
type: news
date: 2010-01-14T15:37:46+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Der Publisher <strong>Electronic Arts</strong> hat neue Details zum Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> bekannt gegeben. Die Unterhaltungssoftware Selbstkontrolle hat dem Actionspiel, nach intensiver Testphase, den „ab 18 Jahren“ Aufkleber verpasst. Demnach wird <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> keine Jugendfreigabe erhalten und nur an erwachsene Spieler verkauft werden dürfen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Erfreulich für alle Volljährigen da der Titel gänzlich ungeschnitten auf dem Markt erscheinen wird. Gordon van Dyke, Executive Producer des Entwicklers DICE, gab über <a href="http://twitter.com/GordonVanDyke/status/7714020389" target="_blank">Twitter</a> bekannt das es nur eine Verkaufsversion für alle Länder gibt.

 

<cite></cite>

* * *



* * *

 