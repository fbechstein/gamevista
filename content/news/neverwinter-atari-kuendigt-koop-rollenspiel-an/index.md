---
title: Neverwinter – Atari kündigt Koop-Rollenspiel an
author: gamevista
type: news
date: 2010-08-23T18:05:56+00:00
excerpt: '<p>[caption id="attachment_2152" align="alignright" width=""]<img class="caption alignright size-full wp-image-2152" src="http://www.gamevista.de/wp-content/uploads/2010/08/neverwinter.jpg" border="0" alt="Neverwinter" title="Neverwinter" align="right" width="140" height="100" />Neverwinter[/caption]Entwickler <strong>Cryptic </strong>und Publisher <strong>Atari </strong>kündigten heute im Rahmen einer Pressemitteilung, ihr nächstes großes Projekt an. Das Koop-Rollenspiel mit dem Namen <a href="http://www.playneverwinter.com/" target="_blank">Neverwinter</a> wird exklusiv für PC im Jahr 2011 veröffentlicht. Der Spieler wird die Möglichkeit haben aus fünf Dungeon & Dragon-Klassen zu wählen und im typischen Neverwinter Nights-Stil die Heldengruppe steuern.</p> '
featured_image: /wp-content/uploads/2010/08/neverwinter.jpg

---
Zusammen mit Freunden oder der KI könnt ihr so als eine Gruppe aus Helden die Geschichte erleben. Fans werden außerdem ihre eigene Story mit einer Art Storygenerator erstellen können.

 

   

* * *

   



 