---
title: Der Herr der Ringe Online – Termin für die Free-to-Play-Phase bekannt
author: gamevista
type: news
date: 2010-10-31T18:43:11+00:00
excerpt: '<p><strong>[caption id="attachment_2206" align="alignright" width=""]<img class="caption alignright size-full wp-image-2206" src="http://www.gamevista.de/wp-content/uploads/2010/09/lotro_small.jpg" border="0" alt="Der Herr der Ringe Online" title="Der Herr der Ringe Online" align="right" width="140" height="100" />Der Herr der Ringe Online[/caption]Codemasters </strong>hat den Termin für die Umstellung auf das kostenlose Spielmodell, des Online-Rollenspiels <a href="http://www.lotro.com/" target="_blank">Der Herr der Ringe Online</a>, bekannt gegeben. Der Publisher teilte im Rahmen einer Pressemitteilung mit, dass bereits am Dienstag, den 2. November 2010, der Startschuss für den kostenlosen Besuch von Mittelerde fallen wird.</p> '
featured_image: /wp-content/uploads/2010/09/lotro_small.jpg

---
 

Um die komplette Welt von Mittelerde erkunden zu dürfen, muss der Spieler allerdings im Item-Shop weitere Questgebiete dazukaufen. Auch zusätzliche Annehmlichkeiten wie mehr Taschen oder die Aufhebung der Goldgrenze kann gegen echtes Geld erworben werden.

   

* * *

   

