---
title: Steam – Viele Spiele über die Weihnachtstage günstiger
author: gamevista
type: news
date: 2009-12-23T18:51:50+00:00
excerpt: '<p>[caption id="attachment_1229" align="alignright" width=""]<img class="caption alignright size-full wp-image-1229" src="http://www.gamevista.de/wp-content/uploads/2009/12/steam.jpg" border="0" alt="Steam" title="Steam" align="right" width="140" height="100" />Steam[/caption]Die Onlineplattform <a href="http://store.steampowered.com/" target="_blank">Steam</a> hat für alle PC-Spieler eine große Weihnachtsaktion gestartet. So sind viele Spiele bis zu 80% günstiger. Darunter fallen ältere Spielehits sowie neue und damit aktuelle Games. Ob Batman: Arkham Asylum, Colin McRae: DiRT 2 oder Borderlands jeden Tag wird eine andere Auswahl angeboten.</p> '
featured_image: /wp-content/uploads/2009/12/steam.jpg

---
Was die nächsten 24 Stunden angeboten wird zeigen wir euch nun, die nächsten Special Deals werden Morgen, den 24. Dezember, um 18 Uhr bekannt gegeben. Weiterhin sind viele Spiele verschiedenster Entwicklerstudios und Publisher, darunter Atari, 2K Games, Eidos, THQ, Ubisoft, Codemasters und Indie-Studios, bis zum 3. Januar 2010 um bis zu 66% reduziert.

 

**<span style="text-decoration: underline;">Angebot vom 23. Dezember:</span>**  
<strong id="nointelliTXT"></strong>

<strong id="nointelliTXT"></strong><strong id="nointelliTXT">Defence Grid</strong><strong id="nointelliTXT"></strong>: **The Awakening** für 2,24 EUR  
<strong id="nointelliTXT">Mirror`s Edge</strong> für 3,74 EUR<strong id="nointelliTXT"><br />GTA 4</strong> für 7,49 EUR<strong id="nointelliTXT"><br />Operation Flashpoint: Dragon Rising</strong> für 24,49 EUR

<cite></cite>

* * *



* * *

 