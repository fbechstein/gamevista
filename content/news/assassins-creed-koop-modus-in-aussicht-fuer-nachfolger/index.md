---
title: 'Assassins Creed –  Koop-Modus in Aussicht für Nachfolger'
author: gamevista
type: news
date: 2010-11-01T19:36:05+00:00
excerpt: '<p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassins Creed 2 " title="Assassins Creed 2" align="right" width="140" height="100" />Assassins Creed 2[/caption]Bereits am 19. November 2010 erscheint der nächste Teil der Assassins Creed-Reihe für die Konsolen Xbox 360 und PlayStation 3 mit dem <a href="http://www.assassinscreed.com" target="_blank">Titel Assassins Creed: Brotherhood</a>. Im Rahmen eines Interviews mit der Presse hat der Games Director von <strong>Ubisoft</strong>, Patrick Plourde, das Thema Koop-Modus angesprochen.</p> '
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Laut seiner Aussage wird in einer bisher noch nicht angekündigten Fortsetzung, ein Koop-Modus fest mit eingeplant. Damit entsprechen die Entwickler den Wünschen vieler Fans des Actionspiels. Weitere Informationen sollen bald folgen.

 

   

* * *

   

