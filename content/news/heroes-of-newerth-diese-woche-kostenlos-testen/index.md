---
title: Heroes of Newerth – Diese Woche kostenlos testen
author: gamevista
type: news
date: 2010-08-03T16:17:06+00:00
excerpt: '<p>[caption id="attachment_1624" align="alignright" width=""]<img class="caption alignright size-full wp-image-1624" src="http://www.gamevista.de/wp-content/uploads/2010/03/heroesofnewerth.jpg" border="0" alt="Heroes of Newerth" title="Heroes of Newerth" align="right" width="140" height="100" />Heroes of Newerth[/caption]Der Entwickler <strong>S2 Games</strong> schickt die Fans des Dota-Spiels <a href="https://www.heroesofnewerth.com/free2play.php?cid=125" target="_blank">Heroes of Newerth</a> auf eine kostenfreie Testwoche. Ihr könnt ab sofort bis zum kommenden Montag das erst kürzlich erschienene Spiel testen. Den Client könnt ihr <a href="https://www.heroesofnewerth.com/free2play.php?cid=125" target="_blank">hier</a> herunterladen. Das Angebot beinhaltet den vollen Spielumfang und alle 68 Helden.</p> '
featured_image: /wp-content/uploads/2010/03/heroesofnewerth.jpg

---
Außerdem kann man auch die Replay-Funktion benutzen.

 

   

* * *

   

