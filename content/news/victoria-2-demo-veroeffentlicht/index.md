---
title: Victoria 2 – Demo veröffentlicht
author: gamevista
type: news
date: 2010-08-11T15:24:03+00:00
excerpt: '<p>[caption id="attachment_2114" align="alignright" width=""]<img class="caption alignright size-full wp-image-2114" src="http://www.gamevista.de/wp-content/uploads/2010/08/victoria2.jpg" border="0" alt="Victoria 2" title="Victoria 2" align="right" width="140" height="100" />Victoria 2[/caption]Publisher <strong>Paradox Interactive</strong> hat die Demo zum bald erscheinenden  Strategiespiel <a href="http://www.paradoxplaza.com/games/victoria-2" target="_blank">Victoria 2</a> veröffentlicht. Zum Inhalt zählen neben einem  Tutorial, auch weitere 15 Spielrunden der Vereinigten Staaten von  Amerika. Die Demo bietet damit umfangreiche Spielinhalte wie z.B.  Wirtschaft, Militär und Kolonisierung.</p> '
featured_image: /wp-content/uploads/2010/08/victoria2.jpg

---
<a href="http://www.paradoxplaza.com/games/victoria-2" target="_blank">Victoria 2</a> erscheint am 13. August 2010 für PC im Handel.

[> Zur Victoria 2 &#8211; Demo][1]

   

* * *

   



 [1]: downloads/demos/item/demos/victoria-2-demo