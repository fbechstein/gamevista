---
title: 'Napoleon: Total War – DLC bringt neue Kampagne ins Spiel'
author: gamevista
type: news
date: 2010-05-25T18:55:56+00:00
excerpt: '<p>[caption id="attachment_431" align="alignright" width=""]<img class="caption alignright size-full wp-image-431" src="http://www.gamevista.de/wp-content/uploads/2009/08/napoleon_totalwar.jpg" border="0" alt="Napoleon: Total War" title="Napoleon: Total War" align="right" width="140" height="100" />Napoleon: Total War[/caption]Der Publisher <strong>SEGA </strong>hat im Rahmen einer offiziellen Pressemitteilung neue Zusatzinhalte für das Strategiespiel <a href="http://www.totalwar.com" target="_blank">Napoleon: Total War</a> angekündigt. Mit einer neuen Kampagne und 32 Regionen, die zwischen vier Nationen aufgeteilt sind, dürft ihr nun erstmals das spanische Volk steuern.</p> '
featured_image: /wp-content/uploads/2009/08/napoleon_totalwar.jpg

---
Der DLC mit dem Namen **The Peninsular Campaign** bietet des weiteren drei neue Agenten, den Priester für Spanien und Portugal, den Provokateur für England und Frankreich und einen einzigartigen Agenten für die spanische Fraktion, den Guerilla. Der voraussichtliche Veröffentlichungstermin ist für den Sommer 2010 geplant.

 

   

* * *

   

