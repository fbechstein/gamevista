---
title: Fifa 09 – Deal of the Week bei Xbox Live Marketplace
author: gamevista
type: news
date: 2009-08-10T19:29:52+00:00
excerpt: '<p>[caption id="attachment_322" align="alignright" width=""]<img class="caption alignright size-full wp-image-322" src="http://www.gamevista.de/wp-content/uploads/2009/08/fifa09_small.png" border="0" alt="Fifa 09" title="Fifa 09" align="right" width="140" height="100" />Fifa 09[/caption]Diese Woche bietet Microsoft im Zuge des <a href="http://www.xbox.com/de-DE/live/deal-of-the-week.htm" target="_blank">Deal of the Week</a>, das von ElectronicArts entwickelte <strong>Fifa 09</strong> an. Das Ganze kostet statt den 800 MS Punkten nur 400 MS Punkte und nennt sich <strong>Fifa 09 FCC Premium Unlock</strong>.</p>'
featured_image: /wp-content/uploads/2009/08/fifa09_small.png

---
<strong class="autolinks"><span class="autolinks">Deal of the Week </span></strong><span class="autolinks"><span class="autolinks">bedeutet das Xbox</span></span> LIVE Goldmitglieder jede Woche die Möglichkeit haben einen ausgewählten Marktplatzinhalt zum attraktiven Angebotspreis zu kaufen. 

Den Link zum <a href="http://www.xbox.com/de-DE/live/deal-of-the-week.htm" target="_blank">Deal of the Week</a> gibt es <a href="http://www.xbox.com/de-DE/live/deal-of-the-week.htm" target="_blank">hier</a>. 





