---
title: 'Risen – Mehr Inhalt in der Standart- und Collectors Edition'
author: gamevista
type: news
date: 2009-09-09T15:34:13+00:00
excerpt: '<p>[caption id="attachment_658" align="alignright" width=""]<img class="caption alignright size-full wp-image-658" src="http://www.gamevista.de/wp-content/uploads/2009/09/risentier.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Publisher <a href="http://www.deepsilver.de">Deep Silver</a> hat per Pressemitteilung preisgegeben das die Standart- und Collector`s des Rollenspiels <strong>Risen </strong>mehr Inhalt haben wird als bisher bekannt war.<br /> Die normale Version wird demnach neben der üblichen Spiele DVD und einem Handbuch auch den Soundtrack enthalten.</p> '
featured_image: /wp-content/uploads/2009/09/risentier.png

---
Dieser wurde übrigens von Kai Rosenkranz komponiert der auch schon die Soundtracks für die anderen Gothic Teile schrieb.    
Die Collector\`s Edition von Risen wird neben den bereits bekannten Extras auch ein **Risen** Comic enthalten. Dieser erzählt die Vorgeschichte des Rollenspiels. Im Comic sind zwei Teile enthalten. Im ersten Teil wird die Geschichte „Der Inquisitor“ erzählt. Im zweiten Teil geht es um die Ereignisse auf der Insel Faranga die aus der Sicht des Banditenschefs Don Esteban geschildert werden.

Hier noch mal eine genaue Liste der Inhalte der Collector\`s Edition:

 

  * **Risen** Comic, der die Vorgeschichte erzählt
  * Eine Karte der Insel Faranga aus der Hand des königlichen Kartenzeichners
  * Der Soundtrack zum Spiel, komponiert von Kai Rosenkranz
  * Making-of-DVD von Risen mit exklusivem Piranha-Bytes-Interview
  * Spezieller A1-Posterdruck mit den Autogrammen von Piranha Bytes
  * Zugangscode zum Betatest des nächsten Piranha-Bytes-Spiels
  * Artbooklet über die Entstehung des Helden und anderer Charaktere
  * Gnom- und Ghul-Stickerset

 

* * *



* * *