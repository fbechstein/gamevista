---
title: 'Call of Duty: Modern Warfare 2 – DLC exklusiv für Xbox'
author: gamevista
type: news
date: 2010-01-07T21:40:08+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Der Entwickler <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Infinity Ward</a> wird für den Ego-Shooter <strong>Call of Duty: Modern Warfare 2 </strong>exklusiv einen Zusatzinhalt zum Downloaden für die Xbox 360 Konsole anbieten. Dies ging aus einer <a href="http://www.microsoft.com/presspass/exec/steve/2010/01-06CES.mspx" target="_blank">Presseerklärung</a> von Robbie Bach, Präsident der Entertainment Sparte bei Microsoft, hervor. Demnach wurde diese Abmachung zwischen dem Entwickler bzw. Publisher von <strong>Modern Warfare 2</strong> und Microsoft schon vor einiger Zeit geschlossen.</p> <p> </p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Kurz darauf korrigierte der Publisher Activision Blizzard die Aussage. So ist es zwar richtig das der DLC im 2. Quartal diesen Jahres exklusiv für die Xbox 360 erscheint, allerdings ist nicht ausgeschlossen das zu einem späteren Zeitpunkt auch PC und PlayStation 3-Freunde in den Genuss kommen werden. Genauere Details zum Zusatzinhalt wurden leider bisher nicht genannt.

 

<cite></cite>

* * *



* * *

 