---
title: 'Theatre of War 2: Kursk 1943 – Demo zum Addon erschienen'
author: gamevista
type: news
date: 2010-02-07T19:59:17+00:00
excerpt: '<p>[caption id="attachment_714" align="alignright" width=""]<img class="caption alignright size-full wp-image-714" src="http://www.gamevista.de/wp-content/uploads/2009/09/theatreofwar.jpg" border="0" alt="Theatre of War 2" title="Theatre of War 2" align="right" width="140" height="100" />Theatre of War 2[/caption]Zum Strategietitel <a href="http://www.battlefront.com/" target="_blank">Theatre of War 2</a> wird momentan ein Stand-Alone Addon mit dem Namen <strong>Kursk 1943</strong> entwickelt. Dabei liegt der Fokus auf den 49  Tagen dauernden Konflikt innerhalb des zweiten Weltkriegs, vom 05. Juli bis zum 23. August 1943, zwischen Deutschland und der Sowjetunion. Diese ging als die größte Panzerschlacht in die Geschichte ein.</p> '
featured_image: /wp-content/uploads/2009/09/theatreofwar.jpg

---
Für den Zusatzinhalt wird das Hauptspiel nicht benötigt. Heute wurde die Demo für <a href="http://www.battlefront.com/" target="_blank">Theatre of War 2: Kursk 1943</a> veröffentlicht. Die Testversion bietet neben einem Tutorial auch zwei Missionen, jeweils auf Seite der Sowjetunion bzw. der Deutschen.

[> Zur Theatre of War 2: Kursk 1943 Demo][1]

 

* * *



* * *

 [1]: downloads/demos/item/demos/theatre-of-war-2-kursk-1943-demo