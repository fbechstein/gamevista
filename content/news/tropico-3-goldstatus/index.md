---
title: Tropico 3 – Goldstatus
author: gamevista
type: news
date: 2009-09-08T08:56:42+00:00
excerpt: '<p>[caption id="attachment_120" align="alignright" width=""]<img class="caption alignright size-full wp-image-120" src="http://www.gamevista.de/wp-content/uploads/2009/07/tropico3_small.jpg" border="0" alt="Tropico 3" title="Tropico 3" align="right" width="140" height="100" />Tropico 3[/caption]Endlich ist es fertig. Entwickler <a href="http://www.haemimontgames.com">Haemimont Games</a> gab nun offiziell per Pressemitteilung bekannt das ihr Aufbau Strategiespiel<strong> Tropico 3</strong> Goldstatus erreicht hat. Demnach ist es nun auf dem Weg in die Presswerke und dem Veröffentlichungstermin am 24. September in Deutschland dürfte damit nichts im Wege stehen.</p> '
featured_image: /wp-content/uploads/2009/07/tropico3_small.jpg

---
Weiterhin teilte Kalypso Media, Publisher von **Tropico 3**, mit das sie vor dem Release noch eine Demo veröffentlichen wollen. Der Erscheinungstermin der Demo soll damit noch diese Woche sein.

 

 

* * *



* * *