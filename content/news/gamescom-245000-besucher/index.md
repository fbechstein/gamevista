---
title: Gamescom – 245.000 Besucher
author: gamevista
type: news
date: 2009-08-23T17:23:39+00:00
excerpt: '<p>[caption id="attachment_109" align="alignright" width=""]<img class="caption alignright size-full wp-image-109" src="http://www.gamevista.de/wp-content/uploads/2009/07/gamescom_small.png" border="0" alt="Gamescom 2009" title="Gamescom 2009" align="right" width="140" height="100" />Gamescom 2009[/caption]Was für ein Auftakt. Die erste Spielemesse <a href="http://www.gamescom.de/">gamescom </a>in Köln war ein voller Erfolg. Laut Veranstalter sind vom 19. bis zum 23. August ganze 245.000 Besucher zur Tagungsstätte für Gamer gepilgert. Insgesamt waren 17.000 Fachbesucher und 228.000 Hobbyspieler auf der <a href="http://www.gamescom.de/">gamescom </a>in Köln. Auf einer Fläche von 120.000 Quadratmetern waren 458 Unternehmen aus 31 Ländern ausstellend.</p> '
featured_image: /wp-content/uploads/2009/07/gamescom_small.png

---
Oliver P. Kuhrt, Geschäftsführer der Koelnmesse GmbH dazu: „Die Premiere der [gamescom][1] ist hervorragend verlaufen. Wir freuen uns über die begeisterten Reaktionen der Aussteller und Besucher. Damit hat sich die gamescom auf Anhieb als Leitmesse etabliert. &#8220; Olaf Wolters, seineszeichen Geschäftsführer des BIU resümiert: &#8222;Wir sind sehr stolz auf die Premiere der gamescom. Sie hat auf Anhieb alle Ziele erfüllt und ist die größte Spielemesse der Welt. Computer- und Videospiele sind so attraktiv, dass unsere Branche trotz Wirtschaftskrise alle Rekorde bricht.&#8220; Die nächste [gamescom][1] findet genau in einem Jahr statt, zwischen dem 18. und 22. August 2010  in Köln statt.

 

* * *



* * *

 

 [1]: http://www.gamescom.de/