---
title: 'Fallout: New Vegas – Viertes Entwicklertagebuch zeigt die Fraktionen'
author: gamevista
type: news
date: 2010-10-07T20:14:00+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Entwickler <strong>Bethesda </strong>haben ihr viertes Entwicklertagebuch für das Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> veröffentlicht. In diesem Teil erklären die Entwickler die unterschiedlichen Fraktionen im Spiel. Wie schon im vorigen Video angedeutet wurde, werden die Fraktionen nicht in gut und böse unterschieden, sondern behalten sich einer Mischung aus beidem vor.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
<a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> wird am 22. Oktober 2010 für PC, PlayStation 3 und Xbox 360 auf den Markt kommen.

[> Zum Fallout: New Vegas &#8211; Fraktionen Entwicklertagebuch][1]

   

* * *

   



 

 [1]: videos/item/root/fallout-new-vegas--viertes-entwicklertagbuch