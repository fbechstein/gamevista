---
title: Crysis 2 – Mehrspielermodus auf der Gamescom 2010
author: gamevista
type: news
date: 2010-07-21T17:56:00+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Publisher <strong>Electronic Arts</strong> wird auf der diesjährigen Spielemesse gamescom 2010 in Köln den Mehrspielermodus für den Ego-Shooter <a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a> vorstellen. Bisher gab es recht wenig Material zu sehen. Der Entwickler <strong>Crytek </strong>wird zusammen mit EA erstmals bewegte Bilder von den Mehrspieler-Karten und -Modis präsentieren und im Rahmen der EA-Pressekonferenz am 17. August 2010 zeigen.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
<a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a> wird voraussichtlich Ende 2010 für PlayStation 3, Xbox 360 und PC in den Handel kommen.

 

   

* * *

   

