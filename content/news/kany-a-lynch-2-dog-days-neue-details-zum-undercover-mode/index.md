---
title: 'Kane & Lynch 2: Dog Days – Neue Details zum Undercover Mode'
author: gamevista
type: news
date: 2010-05-19T16:26:05+00:00
excerpt: '<p>[caption id="attachment_1815" align="alignright" width=""]<img class="caption alignright size-full wp-image-1815" src="http://www.gamevista.de/wp-content/uploads/2010/05/kane-and-lynch2.jpg" border="0" alt="Kany & Lynch 2: Dog Days" title="Kany & Lynch 2: Dog Days" align="right" width="140" height="100" />Kany & Lynch 2: Dog Days[/caption]<strong>Square Enix</strong> hat ein neues Video zu <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> veröffentlicht. Der Trailer stellt den Fragile Alliance-Modus „Undercover Cop“ aus dem Mehrspielerteil von Kane & Lynch 2 vor. In diesem unterwandert ein Spieler in Form eines Undercover-Cops euer Team und versucht Überfälle zu vereiteln.</p> '
featured_image: /wp-content/uploads/2010/05/kane-and-lynch2.jpg

---
<a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> erscheint am 27. August 2010 für PlayStation 3, Xbox 360 und PC.

 

[> Zum Kane & Lynch 2: Dog Days &#8211; Undercover Cop Trailer][1]

   

* * *

   



 [1]: videos/item/root/kane-a-lynch-2-dog-days-undercover-mode-trailer