---
title: 'Colin McRae: DiRT 2 – Estadio Del Ray'
author: gamevista
type: news
date: 2009-09-08T17:47:38+00:00
excerpt: '<p>[caption id="attachment_386" align="alignright" width=""]<img class="caption alignright size-full wp-image-386" src="http://www.gamevista.de/wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg" border="0" alt="Colin McRae: DiRT 2" title="Colin McRae: DiRT 2" align="right" width="140" height="100" />Colin McRae: DiRT 2[/caption]Publisher <a href="http://www.codemasters.com/games/?gameid=299">Codemasters</a> hat heute einen neuen Trailer zum Rallyspiel <strong>Colin McRae: DiRT 2</strong> veröffentlicht. Im Video seht ihr das in Los Angeles stehende Estadio Del Ray Stadion. Wie immer gibt es jede menge Staub, laute Motoren und waghalsige Sprünge zu beobachten. Die Strecke bietet einmal sehr sandige Passagen wo das driften an der Tagesordnung steht.</p> '
featured_image: /wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg

---
Andererseits gibt es auch asphaltierte Streckenteile.  Hin und wieder kann man richtig gute Sprünge hinlegen. Schauts euch am besten einfach an, es lohnt sich.

 

[> Zum Colin McRae: DiRT 2 &#8211; Estadio Del Ray Trailer][1]

* * *



* * *

 [1]: videos/item/root/colin-mcrae-dirt-2-estadio-del-rey-trailer