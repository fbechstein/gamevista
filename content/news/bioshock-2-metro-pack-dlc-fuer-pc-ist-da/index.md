---
title: BioShock 2 – Metro Pack DLC für PC ist da
author: gamevista
type: news
date: 2010-05-26T17:51:33+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="BioShock 2" title="BioShock 2" align="right" width="140" height="100" />BioShock 2[/caption]<strong>2K Games</strong> hat die PC-Version des <strong>Metro Pack</strong> DLC für den Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">BioShock 2</a> veröffentlicht. Der Zusatzinhalt ging gestern im Zuge eines neuen Patches Live. Das <strong>Metro Pack</strong> fügt unter anderem neue Karten, Archievements und den Kill`em Kindly-Modus, der auf Nahkampf-Gefechte setzt, hinzu.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Bisher kamen nur Xbox 360- und PlayStation 3 Besitzer in den Genuss des Download Contents.    
Für knapp 10 Euro erhaltet ihr das Pack auf dem <a href="http://www.microsoft.com/games/en-us/Games/Pages/bioshock2.aspx" target="_blank">Games for Windows Live-Marktplatz</a>.

 

   

* * *

   

