---
title: 'Kane & Lynch 2: Dog Days – Bewegte Bilder vom Cops and Robbers-Modus'
author: gamevista
type: news
date: 2010-05-27T19:12:14+00:00
excerpt: '<p>[caption id="attachment_1576" align="alignright" width=""]<img class="caption alignright size-full wp-image-1576" src="http://www.gamevista.de/wp-content/uploads/2010/03/kane-and-lynch2.jpg" border="0" alt="Kane & Lynch 2: Dog Days" title="Kane & Lynch 2: Dog Days" align="right" width="140" height="100" />Kane & Lynch 2: Dog Days[/caption]<strong>IO Interactive`s</strong> Actionepos <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> nähert sich immer mehr dem Veröffentlichungstermin. Am 27. August 2010 erscheint der zweite Teil. Nun haben die Entwickler ein weiteres Häppchen in Form eines Trailers veröffentlicht, der den Fans die Wartezeit ein wenig verkürzen soll.</p> '
featured_image: /wp-content/uploads/2010/03/kane-and-lynch2.jpg

---
Mit zahlreichen Spielszenen und Zwischensequenzen wird heute der Mehrspieler-Modus Cops & Robbers vorgestellt. Dieser lässt zwei Teams auf einander losgehen, Cops und Verbrecher, im Streit um geklautes Geld. Dabei muss das Verbrecherteam das gestohlene Geld in ein sicheres Versteck bringen.

 

[> Zum Kane & Lynch 2: Dog Days &#8211; Cops and Robbers Trailer][1]

   

* * *

   



 [1]: videos/item/root/kane-a-lynch-2-dog-days-cops-and-robbers-trailer