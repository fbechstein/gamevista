---
title: Fifa 10 – Demo zum Download
author: gamevista
type: news
date: 2009-09-09T23:00:00+00:00
excerpt: '<p> </p> <p>[caption id="attachment_115" align="alignright" width=""]<img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" />Fifa 10[/caption]Publisher <a href="http://fifa.easports.com">Electronic Arts</a> veröffentlichte nun endlich die lang erwartete Demo zum diesjährigen<strong> Fifa</strong>. Die Anspielversion ist über Xbox Live für Xbox 360, dem PlayStation Network für die PS3 und bei uns für PC erhältlich.</p> '
featured_image: /wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg

---
Die Testversion ermöglicht euch die Teams FC Chelsea, FC Barcelona, Juventus Turin, Bayern München, Marseille und Chicago Fire anzuspielen. Weiterhin wird es ein Feature erlauben eure Ingame Videos per [EA][1] Sports Football World anderen Fans zum anschauen zur Verfügung zu stellen. Wir wünschen euch viel Spass und sind gespannt wie sich das neue **Fifa 10** spielt.

[> Zum Fifa 10 Demo Download][2]

* * *



* * *

 [1]: http://fifa.easports.com
 [2]: downloads/demos/item/demos/fifa-10-demo