---
title: Xbox 360 Dashboard – Update erscheint am 11.August
author: gamevista
type: news
date: 2009-07-22T16:17:29+00:00
excerpt: '<p>[caption id="attachment_105" align="alignright" width=""]<img class="caption alignright size-full wp-image-105" src="http://www.gamevista.de/wp-content/uploads/2009/07/xbox360.jpg" border="0" alt="Xbox 360 Dashboard Update kommt am 11. August 2009" title="Xbox 360 Dashboard Update kommt am 11. August 2009" align="right" width="140" height="95" />Xbox 360 Dashboard Update kommt am 11. August 2009[/caption]<font style="font-style: normal; font-weight: normal" color="#000000">Der Termin für das nächste <strong>Xbox 360 Dashboard</strong> Update wurde endlich von <strong><a href="http://www.microsoft.com/germany/" target="_blank">Microsoft</a></strong></font><strong><font style="font-style: normal; font-weight: normal" color="#000000"> </font></strong><font style="font-style: normal; font-weight: normal" color="#000000">bekannt gegeben und wurde auf den 11. August 2009 festgelegt. Auf der E3 wurden ja schon jede Menge neue Features vorgestellt nur der Termin fehlte.</font></p>'
featured_image: /wp-content/uploads/2009/07/xbox360.jpg

---
<font style="font-style: normal; font-weight: normal" color="#000000">Die wohl größte Neuerung wird der Games-On-Demand Service sein mit dem ihr über 30 Titeln ua. Mass Effect, Assassin&#8217;s Creed, BioShock, Mass Effect oder LEGO Star Wars kaufen und runterladen könnt. <br />Weiterhin werden Funktionen wie ein Avatar Marktplatz, indem ihr neue Outfits für eure Ebenbilder erstehen oder freispielen könnt, vorhanden sein. Außerdem gibt es ein 5 Sterne Bewertungssystem das es möglich machen soll, sämtliche Xbox Live Spiele zu bewerten und euch anzeigen zu lassen welche Spiele sehr beliebt bei der Community sind.</font> 

<font style="font-style: normal; font-weight: normal" color="#000000">Das Live Party System wird etwas benutzerfreundlicher durch eine automatische Reconnect Funktion.</font>

<p class="caption">
  <font style="font-style: normal; font-weight: normal" color="#000000">Leider werden</font><font style="font-style: normal" color="#000000"> </font><font style="font-style: normal; font-weight: normal" color="#000000">Last.fm, Facebook, Twitter, Zune Video und 1080p Instant On erst später nachgereicht.</font>
</p>





