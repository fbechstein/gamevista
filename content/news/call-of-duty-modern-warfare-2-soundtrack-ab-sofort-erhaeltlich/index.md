---
title: 'Call of Duty: Modern Warfare 2 – Soundtrack ab sofort erhältlich'
author: gamevista
type: news
date: 2010-06-02T20:09:04+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Wie der Publisher <strong>Activision </strong>im Rahmen einer Pressemitteilung bestätigte, ist nun der Soundtrack zum Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare 2</a> ab sofort erhältlich. Der Soundtrack umfasst  17 Titel die von Hans Zimmer, bekannt für seine Filmmusik aus The Dark Knight oder Pearl Harbor, komponiert wurden.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Kaufen könnt ihr einzelne Titel oder das ganze Album bei iTunes oder <a href="http://www.amazon.de/Call-Duty-Modern-Warfare-2/dp/B003NZUGXK/ref=sr_1_3?ie=UTF8&#038;s=dmusic&#038;qid=1275509452&#038;sr=8-3" target="_blank">Amazon</a> für ca. 9 Euro.

 

   

* * *

   

