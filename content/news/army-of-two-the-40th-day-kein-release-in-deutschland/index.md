---
title: 'Army of Two: The 40th Day – Kein Release in Deutschland'
author: gamevista
type: news
date: 2009-12-27T13:10:36+00:00
excerpt: '<p>[caption id="attachment_1234" align="alignright" width=""]<img class="caption alignright size-full wp-image-1234" src="http://www.gamevista.de/wp-content/uploads/2009/12/armyoftwo.jpg" border="0" alt="Army of Two: The 40th Day" title="Army of Two: The 40th Day" align="right" width="140" height="100" />Army of Two: The 40th Day[/caption]Nachdem <strong>Alien vs. Predator </strong>nicht in Deutschland erscheinen wird, Grund war das der Entwickler gegenüber der USK keine Lust hatte seinen Titel Prüfstellenkonform schneiden zu lassen, zieht der Entwickler von <strong>Army of Two: The 40th Day</strong> nun nach.</p> '
featured_image: /wp-content/uploads/2009/12/armyoftwo.jpg

---
Nachdem die USK dem Titel mit dem Siegel „Keine Altersfreigabe“ abgestempelt hat, entschied der Publisher EA Games das Spiel in Deutschland nicht zu veröffentlichen. Wie immer ist man nun auf die Importversion angewiesen. Demnach wird **Army of Two: The 40th Day** am 08.01.2010 in Europa für PlayStation 3, PSP und Xbox 360 erscheinen.

 

 

<cite></cite>

* * *



* * *

 