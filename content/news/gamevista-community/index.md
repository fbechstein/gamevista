---
title: Community
author: gamevista
type: news
date: 2009-06-09T07:26:35+00:00
featured_image: /wp-content/uploads/2009/06/community.png

---
<figure id="attachment_1" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-1" src="http://www.gamevista.de/wp-content/uploads/2009/06/community.png" border="0" title="Willkommen in der Community" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Willkommen in der Community</figcaption></figure>Willkommen im gamevista.de Community-Bereich 

Neben topaktuellen News, Hotspots, vielen Downloads und Videos findet ihr hier die Möglichkeit euch über Spiele, Lösungen und anderes auszutauschen. Ihr Habt die Möglichkeit in verschiedenen [Foren][1] nach Rat zu fragen, oder anderen mit eurem überlegenen Wissen zu helfen. 

Ihr könnt euer [Profil][2] pflegen, sowie eigene Signaturen erstellen, Avatare hochladen etc.

Du bist noch kein Mitglied? Dann [registriere Dich jetzt][3] und nutze die Chance sich mit anderen auszutauschen, an Verlosungen teilzunehmen und Deine Meinung loszuwerden.

 [1]: communitybuilder/forum
 [2]: index.php?option=com_comprofiler&Itemid=100
 [3]: index.php?option=com_comprofiler&task=registers