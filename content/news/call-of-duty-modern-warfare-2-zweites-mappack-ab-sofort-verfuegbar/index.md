---
title: 'Call of Duty: Modern Warfare 2 – Zweites Mappack ab sofort verfügbar'
author: gamevista
type: news
date: 2010-07-06T16:52:26+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Entwickler <strong>Infinity Ward</strong> schickt sein zweites Mappack ins Rennen. Das <strong>Resurgence Mappack</strong> für den Ego-Shooter <a href="http://www.modernwarfare2.com" target="_blank">Call of Duty: Modern Warfare 2</a> ist ab sofort für 12,59 Euro als herunterladbarer Inhalt erhältlich. Der DLC umfasst fünf neue Karten, darunter sind Carnival, Trailer Park, Fuel, Strike und Vacant.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
> Zum <a href="http://store.steampowered.com/app/10196/" target="_blank">Call of Duty: Modern Warfare 2 &#8211; Resurgency Mappack Steam Angebot</a>

 

Die Beschreibung der einzelnen Karten:

“**Carnival**,” a large abandoned theme park that offers lots of unique vantage points that work well for all game modes. The once vibrant rides now sit idle and neglected as perfect cover points in one of the most original multiplayer locations for Modern Warfare 2.

“**Fuel**,” a massive oil refinery with extremely long sightlines great for medium- to long-range firefights. The spacious surroundings offer a great sniper advantage; offices, warehouses, and buildings house objectives and provide opportunity for close-range firefights.

“**Trailer Park**,” a medium-sized mobile estate with a maze of tight flank routes and lots of cover. Junk covered yards, deserted trailers, and debris-riddled surroundings offer intense firefights with a wide variety of vantage points and routes available.

“**Strike**,” a large urban desert town, excellent for medium-to-large team games. The large monument, connecting streets, and surrounding buildings offer great firefights in Team Deathmatch, and great vantage points for objective team games.

“**Vacant**,” a deserted Russian office complex offering a great mixture of interior and exterior combat. Long hallways with minimal cover points offer a fast escape route for potential runners, but high accuracy and fast aiming is required when advancing through this large complex.

   

* * *

   

