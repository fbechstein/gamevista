---
title: Just Cause 2 – Läuft nicht auf älteren Betriebssystemen
author: gamevista
type: news
date: 2010-02-18T18:52:09+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Der Publisher <strong>Eidos </strong>hat knapp einen Monat vor der Veröffentlichung des Actionspiels <a href="http://www.justcause.com/" target="_blank">Just Cause 2</a> die offiziellen Systemanforderungen veröffentlicht. Diese Meldung birgt eine schlechte Nachricht für Besitzer älterer Betriebssysteme, denn <a href="http://www.justcause.com/" target="_blank">Just Cause 2</a> wird zwingend auf der DirectX 10 Schnittstelle laufen. Diese wird aber bisher nur von den Betriebssystemen Windows Vista bzw. Windows 7 unterstützt.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
So bleiben Fans des Open-World-Spiels, die noch Windows XP bzw. ältere Systemen installiert haben, ausgeschlossen. Eine doch sehr merkwürdige Art ein Produkt zu verkaufen, denn durch solch hohe Anforderungen werden potentielle Kunden vom Kauf absehen. 

**<span style="text-decoration: underline;">Die Systemanforderungen für Juse Cause 2:</span>**

**Minimal:**

&#8211; Operating System: Microsoft Windows Vista or Windows 7 **(Windows XP is unsupported)**   
&#8211; Processor: Dual-core CPU with SSE3 (Athlon 64 X2 4200 / Pentium D 3GHz)   
&#8211; Graphics Card: Nvidia Geforce 8800 Series / ATI Radeon HD 2600 Pro with 256MB memory or equivalent DX10 card with 256MB memory   
&#8211; Memory: 2GB RAM   
&#8211; DirectX: Microsoft DirectX 10   
&#8211; Hard Drive: 10GB of free drive space   
&#8211; Optical Drive: DVD-ROM drive   
&#8211; Sound Card: 100% DirectX 10 compatible sound card   
&#8211; Internet Connection: Internet connection required for product activation   
&#8211; Input: Keyboard and mouse (Microsoft Xbox 360 controller optional)

**Empfohlen:**

&#8211; Operating System: Microsoft Windows Vista or Windows 7 **(Windows XP is unsupported)**   
&#8211; Processor: Intel Core 2 Duo 2.6GHz or AMD Phenom X3 2.4GHz   
&#8211; Graphics Card: Nvidia GeForce GTS 250 Series with 512MB / ATI Radeon HD 5750 Series with 512MB or equivalent DX10 card with 512MB memory   
&#8211; Memory: 3GB   
&#8211; DirectX: Microsoft DirectX 10.1 with Vista SP1   
&#8211; Hard Drive: 10GB of free drive space   
&#8211; Optical Drive: DVD-ROM drive   
&#8211; Sound Card: 100% DirectX 10 compatible Dolby Digital 5.1 sound card   
&#8211; Internet Connection: Internet connection required for product activation   
&#8211; Input: Keyboard and mouse (Xbox 360 controller optional)

<p style="text-align: center;">
  <img class="caption size-full wp-image-1468" src="http://www.gamevista.de/wp-content/uploads/2010/02/justcause2big.jpg" border="0" alt="Läuft nur mit Windows Vista und Windows 7" title="Läuft nur mit Windows Vista und Windows 7" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/justcause2big.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/justcause2big-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *



* * *