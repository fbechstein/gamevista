---
title: 'Ghost Recon: Future Soldier – Neuer Teil offiziell bestätigt'
author: gamevista
type: news
date: 2010-02-09T18:46:01+00:00
excerpt: '<p>[caption id="attachment_307" align="alignright" width=""]<img class="caption alignright size-full wp-image-307" src="http://www.gamevista.de/wp-content/uploads/2009/08/ubisoftlogo_small.jpg" border="0" alt="Ubisoft" title="Ubisoft" align="right" width="140" height="100" />Ubisoft[/caption]Das ein neuer Ghost Recon Teil in Planung ist war schon hinlänglich bekannt. Lange hatte man gemunkelt in welchem Setting der neuste Teil spielen würde und natürlich welchen Namen das Spiel tragen wird. Nun hat der Publisher <strong>Ubisoft </strong>offiziell bestätigt das <strong>Tom Clancy`s Ghost Recon: Future Soldier</strong> in der Entwicklung sei und zum Weihnachtsgeschäft 2010 erscheint.</p> '
featured_image: /wp-content/uploads/2009/08/ubisoftlogo_small.jpg

---
Dabei kümmert sich das gleiche Studio wie von **Ghost Recon Advanced Warfighter 1 & 2** um die Entwicklung. Wieder wird es um Hightech-Waffen und den Einsatz neuester Technologien im modernen Krieg gehen. Bereits im Sommer 2010 wird es eine Mehrspieler-Beta, exklusiv für Xbox 360 Konsolen Besitzer, geben. Kleiner Haken an der Sache, man muss sich das am 15. April 2010 erscheinende **Splinter Cell: Conviction** kaufen, um sich für die Beta von **Ghost Recon: Future Soldier** zu qualifizieren. **Ghost Recon: Future Soldier** erscheint für PC, Xbox 360 und Playstation 3.

**\*UPDATE\*  
**   
Zusätzlich hat der Publisher **Ubisoft** den ersten Trailer in Form eines Teaser-Videos zu **Ghost Recon: Future Soldier** veröffentlicht.

[> Zum Ghost Recon: Future Soldier &#8211; Teaser][1]

* * *



* * *

 [1]: videos/item/root/ghost-recon-future-soldier-teaser