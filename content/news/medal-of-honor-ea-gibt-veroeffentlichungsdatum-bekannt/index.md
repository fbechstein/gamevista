---
title: Medal of Honor – EA gibt Veröffentlichungsdatum bekannt
author: gamevista
type: news
date: 2010-05-05T19:32:07+00:00
excerpt: '<p>[caption id="attachment_1764" align="alignright" width=""]<img class="caption alignright size-full wp-image-1764" src="http://www.gamevista.de/wp-content/uploads/2010/05/moh2010.jpg" border="0" alt="Medal of Honor" title="Medal of Honor" align="right" width="140" height="100" />Medal of Honor[/caption]Der Publisher <strong>Electronic Arts</strong> hat nun offiziell das Veröffentlichungsdatum zum neusten <a href="http://www.medalofhonor.com" target="_blank">Medal of Honor</a> Titel bestätigt. Demnach erscheint der Ego-Shooter am 14. Oktober 2010 im Handel. Im Rahmen der Pressemitteilung wurde gleichzeitig zum Veröffentlichungsdatum auch ein neuer Trailer angekündigt, der morgen der Öffentlichkeit präsentiert werden soll.</p> '
featured_image: /wp-content/uploads/2010/05/moh2010.jpg

---
Der Trailer zeigt Platoon Ranger der U.S. Army, die mit Hilfe eines Transporthubschraubers in das Kampfgebiet gebracht werden. <a href="http://www.medalofhonor.com" target="_blank">Medal of Honor</a> wird nicht wie seine Vorgänger im zweiten Weltkrieg spielen, sondern den Konflikt im heutigen Afghanistan austragen. <a href="http://www.medalofhonor.com" target="_blank">Medal of Honor</a> wird für PlayStation 3, Xbox 360 und PC erscheinen. 

„Medal of Honor bietet ein realistisches Bild eines aktuellen Konflikts. Inspiriert von realen Personen und Ereignissen lässt es die Spieler in die Haut eines modernen Kriegers schlüpfen – vom einfachen Infanteristen, bis zum Tier 1 Operator“, so Greg Goodrich, Executive Producer bei Medal of Honor. „Wir sind sehr stolz auf diesen Trailer, weil er einen Ausblick auf das Spiel bietet, das wir gerade erschaffen, und außerdem noch Einblicke in die Denkweise dieser Soldaten gewährt.“

 

   

* * *

   

