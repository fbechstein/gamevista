---
title: 'Arcania: A Gothic Tale – Verschoben auf 2010'
author: gamevista
type: news
date: 2009-08-19T17:45:58+00:00
excerpt: '<p>[caption id="attachment_117" align="alignright" width=""]<img class="caption alignright size-full wp-image-117" src="http://www.gamevista.de/wp-content/uploads/2009/07/arcania_32_small.jpg" border="0" alt="Arcania: A Gothic Tale" title="Arcania: A Gothic Tale" align="right" width="140" height="100" />Arcania: A Gothic Tale[/caption]Wie Publisher <a href="http://www.jowood.com" target="_blank">Jowood </a>per Pressemitteilung verkündet erscheint <strong>Arcania: A Gothic Tale</strong> wohl nicht mehr dieses Jahr. Demnach wird der Release auf Anfang 2010 verschoben. Wie aus der Pressemitteilung zu entnehmen war, will <a href="http://www.jowood.com" target="_blank">Jowood </a>dem Entwicklerteam von Spellbound mehr Zeit geben um Fehler und Ungereimtheiten aus dem Spiel entfernen zu können.</p> '
featured_image: /wp-content/uploads/2009/07/arcania_32_small.jpg

---
Der CEO von <a href="http://www.jowood.com" target="_blank">Jowood </a>gab auch einen Kommentar dazu ab den wir gerne an euch weitergeben.

 

<p class="MsoNormal">
  <em>&#8222;Für einen Publisher sind Investitionen in die Qualität und Innovation von Produkten der richtige Weg, um im Rollenspiel-Markt eine bedeutende Rolle einzunehmen. Mit <strong id="EL_12506920727134286419340">Arcania: A Gothic Tale</strong> wollen wir allen Fans der Gothic-Serie das bestmögliche Produkt anbieten. Wir haben es selbst in der Hand, gegen die besten Rollenspiele auf dem Markt zu bestehen.“</p> 
  
  <p>
    </em>Einen genauen Veröffentlichungstermin gibt es bisher nicht.
  </p>
  
  <hr />
  
  <p>
    
  </p>
  
  <hr />
  
  <p>
     
  </p>
  
  <p class="MsoNormal">
     
  </p>
  
  <p class="MsoNormal">
     
  </p>
  
  <p>
     
  </p>