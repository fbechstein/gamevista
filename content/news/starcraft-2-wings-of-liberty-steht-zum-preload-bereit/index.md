---
title: 'Starcraft 2: Wings of Liberty – Steht zum Preload bereit'
author: gamevista
type: news
date: 2010-07-16T20:08:14+00:00
excerpt: '<p><strong>[caption id="attachment_2022" align="alignright" width=""]<img class="caption alignright size-full wp-image-2022" src="http://www.gamevista.de/wp-content/uploads/2010/07/starcraft2_gameplay_small.jpg" border="0" alt="Starcraft 2: Wings of Liberty" title="Starcraft 2: Wings of Liberty" align="right" width="140" height="105" />Starcraft 2: Wings of Liberty[/caption]Blizzard </strong>hat vorsorglich vor dem Release am 27. Juli den kompletten Client von <a href="http://www.starcraft2.com" target="_blank">Starcraft 2: Wings of Liberty</a> zum PreLoad freigegeben. Wer sich den Stress im Geschäft nicht hingeben möchte oder denkt am 27. sei evtl. ein Ausverkauf des zweiten Teils möglich, der kann sich ab sofort auf der <a href="http://beta-eu.battle.net/de/info/digital-purchase" target="_blank">offiziellen Website</a> vom Battlenet den kompletten Client von <a href="http://www.starcraft2.com" target="_blank">Starcraft 2: Wings of Liberty</a> schonmal herunterladen.</p> '
featured_image: /wp-content/uploads/2010/07/starcraft2_gameplay_small.jpg

---
Preislich ist die digitale Kopie etwas teurer als im Laden. Sie liegt bei stolzen 59,99 Euro und setzt einen Battlenet-Account voraus. Wer sich trotzdem dazu entschließen möchte kann ab dem 27. Juli 2010 10:00 Uhr CET seine <a href="http://www.starcraft2.com" target="_blank">Starcraft 2: Wings of Liberty</a>-Version Bezahlen und loszocken.

 

 

   

* * *

   



 