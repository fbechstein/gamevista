---
title: Tomb Raider 9 Gerüchte
author: gamevista
type: news
date: 2009-07-13T10:06:15+00:00
excerpt: '<p>[caption id="attachment_37" align="alignright" width=""]<img class="caption alignright size-full wp-image-37" src="http://www.gamevista.de/wp-content/uploads/2009/07/tr9_small1.png" border="0" alt="Tomb Raider 9 soll düsterer werden" title="Tomb Raider 9 soll düsterer werden" align="right" width="140" height="100" />Tomb Raider 9 soll düsterer werden[/caption]Die Gerüchteküche rund um den neuen Tomb Raider Teil brodelt. Im Internet ist ein <a href="http://www.neogaf.com/forum/showpost.php?p=16625394&postcount=44" target="_blank">Dokument</a> aufgetaucht, welches kurz die Geschichte und die Features des neuen Tomb Raider Teils aufzählt. <br />Darin geht es, dass die junge Lara auf einer geheimnisvollen Insel landet und ums Überleben kämpfen muss. Die Featurteliste hört sich ebenfalls spannend an. <strong>Tomb Raider 9</strong> soll laut des Dokuments ein neues Konzept verfolgen. Open World ist das Stichwort. Lara kann sich überall auf der Insel frei bewegen, kann überall klettern, in Höhlen reinkriechen usw. </p>'
featured_image: /wp-content/uploads/2009/07/tr9_small1.png

---
Wenn sich die Gerüchte als Wahrheit entpuppen, so erwartet uns mit Sicherheit ein sehr spannendes Spiel. Wir halten Euch auf dem Laufenenden. 

&nbsp;





