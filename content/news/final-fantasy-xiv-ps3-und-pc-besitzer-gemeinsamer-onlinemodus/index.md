---
title: Final Fantasy XIV – PS3 und PC Besitzer gemeinsamer Onlinemodus
author: gamevista
type: news
date: 2009-08-27T17:46:47+00:00
excerpt: '<p />[caption id="attachment_528" align="alignright" width=""]<img class="caption alignright size-full wp-image-528" src="http://www.gamevista.de/wp-content/uploads/2009/08/final-fantasyxiv.png" border="0" alt="Final Fantasy XIV" title="Final Fantasy XIV" align="right" width="140" height="100" />Final Fantasy XIV[/caption]Laut Sage Sundi, seineszeichen Online Producer beim Entwickler <a href="http://www.square-enix.com/eu/de">Square Enix</a>, wird es möglich sein im kommenden Online Rollenspiel <strong>Final Fantasy XIV</strong> gemeinsam von zwei verschiedenen Plattformen spielen zu können. Demnach sind PlayStation 3 und PC Besitzer gemeinsam in der Welt von <strong>Final Fantasy XIV</strong> unterwegs. <br /> '
featured_image: /wp-content/uploads/2009/08/final-fantasyxiv.png

---
Dabei gibt es keinerlei Begrenzungen auf bestimmte Gebiete, es wird alles begehbar sein. Deswegen arbeiten [Square Enix][1] momentan an einer Server Lösung die von beiden Plattformen speichert und so doppelte Namen verhindert.</p> 

* * *



* * *

 

 [1]: http://www.square-enix.com/eu/de