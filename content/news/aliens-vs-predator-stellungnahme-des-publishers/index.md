---
title: Aliens vs. Predator – Stellungnahme des Publishers
author: gamevista
type: news
date: 2010-02-08T17:23:33+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Für Fans des Ego-Shooters <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> war dies ein unruhiges Wochenende. Erst wurde die Demo zum Alienshooter veröffentlicht. Dann wurde sie nachträglich aus der Steam-Spieleübersicht gelöscht. Natürlich nur für deutsche Kunden, denn <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> wird nach wie vor nicht in Deutschland veröffentlicht.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Dies führte zur Aussage eines Mitarbeiters des SEGA Support-Teams, dass Importversionen von <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> über den deutschen Steam-Dienst nicht aktivierbar seien.

Nun hat der Publisher offiziell dazu Stellung bezogen und die Aktivierung einer Importversion bestätigt.

_» Aliens vs Predator wird von SEGA in Deutschland nicht veröffentlicht. Direkt über Steam können in Deutschland die Demo und das Produkt ebenfalls nicht bezogen werden. Die Steam-Aktivierung einer regulären Verkaufsversion (Boxed Copy) wird in Deutschland jedoch uneingeschränkt möglich sein.«_

So wird es nun möglich sein das Spiel, über England oder Österreich zu importieren und in seinen Steam-Account einzubinden. Der Kauf direkt über Steam ist allerdings nicht möglich. _  
_ 

* * *



* * *