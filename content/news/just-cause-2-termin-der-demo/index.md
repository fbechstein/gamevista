---
title: Just Cause 2 – Termin der Demo
author: gamevista
type: news
date: 2010-02-26T21:59:12+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption] Der Publisher <strong>Square Enix</strong> hat im Rahmen einer Pressemitteilung offiziell den Termin der Demo für das Open-World Actionspiel bekannt gegeben. Demnach wird die Testversion von <a href="http://www.justcause.com/" target="_blank">Just Cause 2</a> am 4. März 2010 veröffentlicht werden. Für Xbox 360 wird die Demo über den Live Marktplatz veröffentlicht. PlayStation-Fans bekommen die Anspielversion wie immer über das PlayStation-Network, PC Spieler laden sich die Demo über den Onlinedienst Steam.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
Als Umfang gibt der Publisher die 90 Quadratkilometer große Lautan-Lama-Wüste an, eine riesige, weitreichende Fläche mit einer atemberaubenden, sonnenverbrannten Szenerie. Dies ist nur eine der vielen Gebiete auf der über 1000 Quadratkilometer großen Inselgruppe Panau. Vollgepackt mit Städten, Dörfern, militärischen Stützpunkten, Gebirgszügen, Missionen und vielem mehr. <a href="http://www.justcause.com/" target="_blank">Just Cause 2 </a>wird für PC. Xbox 360 und PlayStation 3 am 26. März in den Handel kommen.

 

* * *



* * *