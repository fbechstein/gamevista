---
title: Bioshock 2 – Zusatzinhalte schon in Arbeit
author: gamevista
type: news
date: 2010-01-18T18:21:47+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Wie nicht anders zu erwarten war, wird es auch für den kommenden Actionhit <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> Zusatzinhalte geben. So werden die so genannten Download-Content bereits programmiert. Dies bestätigte Senior System Designer Kent Hudson gegenüber der Presse. Details gab Hudson aber nicht bekannt, Preise sowie Veröffentlichungstermine fehlen auch.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
<a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> vom Entwickler **2K Marin** erscheint am 09. Februar 2010 in Deutschland.

 

* * *



* * *