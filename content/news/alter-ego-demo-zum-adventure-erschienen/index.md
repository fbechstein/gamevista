---
title: Alter Ego – Demo zum Adventure erschienen
author: gamevista
type: news
date: 2010-05-12T17:02:30+00:00
excerpt: '<p>[caption id="attachment_1543" align="alignright" width=""]<img class="caption alignright size-full wp-image-1543" src="http://www.gamevista.de/wp-content/uploads/2010/03/alterego.jpg" border="0" alt="Alter Ego" title="Alter Ego" align="right" width="140" height="100" />Alter Ego[/caption]Heute hat der Publisher <strong>bitComposer Games</strong> eine Testversion zum Adventure <a href="http://www.de.namcobandaigames.eu/product/pc/alter-ego/3776" target="_blank">Alter Ego</a> des Entwicklers <strong>Future Games</strong> veröffentlicht. Die Demo umfasst 240 Megabyte und lässt euch die ersten drei Kapitel des Spiels erleben.</p> '
featured_image: /wp-content/uploads/2010/03/alterego.jpg

---
[> Zum Alter Ego &#8211; Demo Download][1]

**Die Features des fertigen Spiels sind:**

  * Beliebtes Setting: England am Ende des 19. Jahrhunderts 
  * Spannende Detektivstory mit Horrorelementen 
  * Erstklassige Zwischensequenzen welche die Story vorantreiben 
  * 2 spielbare Charaktere 
  * Über 28 weitere Charaktere, die man treffen und mit denen man sprechen kann 
  * Über 80 Orte in der Stadt Plymouth und ihrer Umgebung 
  * 150 aufnehmbare Gegenstände, die miteinander kombiniert werden können 
  * Über 1000 Soundeffekte.

   

* * *

   



 [1]: downloads/demos/item/demos/alter-ego-demo