---
title: Brink – Drittes Entwicklertagebuch und Vorbestellerboni
author: gamevista
type: news
date: 2010-08-24T16:42:19+00:00
excerpt: '<p>[caption id="attachment_1143" align="alignright" width=""]<img class="caption alignright size-full wp-image-1143" src="http://www.gamevista.de/wp-content/uploads/2009/12/brink-game.jpg" border="0" alt="Brink" title="Brink" align="right" width="140" height="100" />Brink[/caption]Publisher <strong>Bethesda </strong>hat heute den dritten Teil ihrer Entwicklertagebuch-Reihe zum Actionspiel <a href="http://www.brinkthegame.com" target="_blank">Brink</a> veröffentlicht.  Hier erklären die Entwickler wie sie die Grenzen von Einzel- und Mehrspielermodus verschwinden lassen wollen. Außerdem wurden heute die Extras der Vorbestellerversion von <a href="http://www.brinkthegame.com" target="_blank">Brink</a> vorgestellt.</p> '
featured_image: /wp-content/uploads/2009/12/brink-game.jpg

---
<a href="http://www.brinkthegame.com" target="_blank">Brink</a> wird im Frühjahr 2011 für Xbox 360, PlayStation 3 und PC veröffentlicht.

[> Zum Brink &#8211; Entwicklertagebuch #3][1]

Die Extras der Vorbestellerversion im Überblick:

&#8211; SpecOps DLC    
&#8211; exklusive Waffe: Hockler-Maschinenpistole    
&#8211; exklusives GreenEye-Zielfernrohr   
&#8211; exklusive Sloani-Gefechtsmaske   
&#8211; einzigartiges Dogtag-Körpertattoo

   

* * *

   



 

 [1]: videos/item/root/brink-entwicklertagebuch-3