---
title: Supreme Commander 2 – Veröffentlichungstermin bekannt
author: gamevista
type: news
date: 2010-01-26T18:15:36+00:00
excerpt: '<p>[caption id="attachment_1330" align="alignright" width=""]<img class="caption alignright size-full wp-image-1330" src="http://www.gamevista.de/wp-content/uploads/2010/01/supreme-commander-2.jpg" border="0" alt="Supreme Commander 2" title="Supreme Commander 2" align="right" width="140" height="100" />Supreme Commander 2[/caption]Der Publisher <strong>Square Enix</strong> hat im Rahmen einer offiziellen Pressemitteilung, den Veröffentlichungstermin für das Strategiespiel <a href="http://www.supremecommander.com/" target="_blank">Supreme Commander 2</a> bekannt gegeben. Die PC Version des lang erwarteten zweiten Teils, erscheint demnach am 5. März 2010 im deutschen Handel.</p> '
featured_image: /wp-content/uploads/2010/01/supreme-commander-2.jpg

---
Die Xbox 360-Version wird zwei Wochen später, am 19. März 2010 veröffentlicht.

 

 

* * *



* * *