---
title: Crysis 2 – Crytek bestätigt PC-Demo Termin
author: gamevista
type: news
date: 2011-02-16T17:36:25+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Entwickler <strong>Crytek </strong>hat nun den offiziellen Termin für die PC-Demo des Ego-Shooters <a href="http://www.ea.com/crysis-2" target="_blank" rel="nofollow">Crysis 2</a> angekündigt. Demnach erscheint die Testversion für die heimischen Rechenmaschinen am 1. März 2011. Eine Xbox 360 Multiplayer-Demo erschien bereits im letzten Monat.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
Als Inhalt zählen zwei Spielmodi: Team Instant Action, in dem ihr so viele Gegner wie möglich ausschalten müsst, und Crash Site, hier kämpft der Spieler um die Oberhand gegen Alien-Drop-Pods.

 

 

* * *



* * *