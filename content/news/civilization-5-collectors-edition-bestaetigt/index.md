---
title: Civilization 5 – Collector`s Edition bestätigt
author: gamevista
type: news
date: 2010-07-12T16:44:39+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]<strong>Publisher 2K Games</strong> hat offiziell angekündigt, dass der fünfte Teil der Civilization-Reihe auch als Sammler Edition erscheinen wird. Die Collector`s Edition wird neben der Digital Deluxe Edition und normalen Verkaufsversion damit die dritte Fassung sein zwischen der sich die Fans entscheiden müssen.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Bisher ist nur der Preis von ungefähr 100 US Dollar bekannt. Als Inhalt zählen fünf Figuren aus Metall die bestimmten Einheiten nachempfunden wurden. Außerdem wird es noch den Spielesoundtrack auf CD geben und eine Making of DVD „Behind the Scenes at Firaxis on Civilization V“ Ein Artbook mit 176 Seiten darf da natürlich nicht fehlen. <a href="http://www.civilization5.com" target="_blank">Civilization 5</a> wird am 24. September 2010 im Handel erhältlich sein.

 

   

* * *

   

