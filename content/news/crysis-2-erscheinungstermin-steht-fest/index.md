---
title: Crysis 2 – Erscheinungstermin steht fest
author: gamevista
type: news
date: 2010-04-03T18:19:49+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Der Publisher <strong>Electronic Arts</strong> hat auf ihrer tschechischen Website den Release-Termin für die lang ersehnten Fortsetzung von <strong>Crysis </strong>bekannt gegeben. Demnach soll der zweite Teil des Ego-Shooters am 19.11.2010 für PC, PlayStation 3 und Xbox 360 im Handel erscheinen.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
<a href="http://www.ea.com/games/crysis-2" target="_blank"><br />Crysis 2</a> spielt nicht mehr auf einer Südseeinsel sondern verlagert das Geschehen auf die Halbinsel Manhattan New York. Die Story knüpft mit drei Jahren Abstand an die des Vorgängers an.

 

<cite></cite>

   

* * *

   

