---
title: R.U.S.E. – Beta verlängert
author: gamevista
type: news
date: 2010-04-07T17:58:51+00:00
excerpt: '<p>[caption id="attachment_1666" align="alignright" width=""]<img class="caption alignright size-full wp-image-1666" src="http://www.gamevista.de/wp-content/uploads/2010/04/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Der Publisher <strong>Ubisoft </strong>hat die offene Betaphase für das Strategiespiel <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a> verlängert. Demnach sollte die Open-Beta bereits am Montag den 5. April enden. Nun wird allen Fans die Möglichkeit gegeben noch eine Woche länger, bis zum 12. April 2010, das Strategiespiel auf Herz und Nieren zu testen.</p> '
featured_image: /wp-content/uploads/2010/04/ruse_small.jpg

---
Wer noch nicht in den Genuss der Testversion gekommen ist, kann den Client über den Online-Dienst Steam beziehen.

 

<a href="http://store.steampowered.com/app/33310/" target="_blank">> Zum R.U.S.E. &#8211; Beta-Client Download</a>

<cite></cite>

   

* * *

   

