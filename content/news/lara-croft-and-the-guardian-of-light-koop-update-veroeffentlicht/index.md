---
title: Lara Croft and the Guardian of Light – Koop-Update veröffentlicht
author: gamevista
type: news
date: 2010-11-23T18:15:38+00:00
excerpt: '<p>[caption id="attachment_2035" align="alignright" width=""]<img class="caption alignright size-full wp-image-2035" src="http://www.gamevista.de/wp-content/uploads/2010/07/laracroft_gol.jpg" border="0" alt="Lara Croft and the Guardian of Light " title="Lara Croft and the Guardian of Light " align="right" width="140" height="100" />Lara Croft and the Guardian of Light [/caption]Publisher <strong>Square Enix</strong> hat nach langem Warten das Koop-Update für das Actionspiel <a href="http://www.laracroftandtheguardianoflight.com" target="_blank">Lara Croft and the Guardian of Light</a> veröffentlicht. Das Update wird je nach System über PSN, Xbox Live oder Steam heruntergeladen und installiert.</p> '
featured_image: /wp-content/uploads/2010/07/laracroft_gol.jpg

---
Neben diversen Verbessrungen der Grafik, Sound, Steuerung und Stabilität des Spiels, ist es nun Möglich den Titel im Koop-Modus mit Freunden zu erleben.

 

 

   

* * *

   

