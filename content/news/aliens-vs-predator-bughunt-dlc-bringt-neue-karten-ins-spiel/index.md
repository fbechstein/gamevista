---
title: Aliens vs. Predator – Bughunt DLC bringt neue Karten ins Spiel
author: gamevista
type: news
date: 2010-07-02T16:50:47+00:00
excerpt: '<p><strong>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]SEGA </strong>kündigte heute neue Zusatzinhalte für den Ego-Shooter <a href="http://www.sega.com/games/aliens-vs-predator" target="_blank">Aliens vs. Predator</a> für Xbox 360, PlayStation 3 und PC an. Der DLC mit dem Namen <strong>Bughunt Pack</strong>, bringt vier neue Karten ins Spiel.<br /> Ab dem 7. Juni 2010 können die vier Karten zum Preis von 5,99 Euro über Steam erworben werden.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Leider nicht in Deutschland da die Alien-Hatz der USK zur Prüfung nicht vorgestellt wurde und somit keine Alterseinstufung erhalten hat. Wer trotzdem einen Blick in das Erweiterungspaket werfen möchte kann sich auf folgenden Trailer freuen.

[> Zum Aliens vs. Predator &#8211; Bughunt DLC Trailer][1]

   

* * *

   



 [1]: videos/item/root/aliens-vs-predator--bughunt-dlc-trailer