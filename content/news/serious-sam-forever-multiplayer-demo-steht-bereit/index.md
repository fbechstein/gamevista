---
title: Serious Sam Forever – Multiplayer Demo steht bereit
author: gamevista
type: news
date: 2010-01-26T18:29:07+00:00
excerpt: '<p>[caption id="attachment_1368" align="alignright" width=""]<img class="caption alignright size-full wp-image-1368" src="http://www.gamevista.de/wp-content/uploads/2010/01/serious_sam_hd_small.png" border="0" alt="Serious Sam" title="Serious Sam" align="right" width="140" height="100" />Serious Sam[/caption]Das Entwicklerteam rund um <strong>Prophets of Sam</strong>, hat eine Multiplayer-Demo zum Actionspiel Serious Sam 2 veröffentlicht. Dabei handelt es sich um eine Modifikation von Serious Sam. Enthalten sind vier unterschiedliche Karten, vier verschiedene Spielmodi und knapp 400 Bots die die heutige PC-Hardware ordentlich fordern soll.</p> '
featured_image: /wp-content/uploads/2010/01/serious_sam_hd_small.png

---
Besonderen Wert legen die Entwickler auf die anspruchsvolle künstliche Intelligenz der Bots. Um <a href="http://www.serioussamforever.com/" target="_blank">Serious Sam Forever</a> spielen zu können, benötigen sie das Hauptspiel Serious Sam 2. Die Demo bringt schlappe 138 Megabyte auf die Wage. 

[> Zum Serious Sam Forever &#8211; Multiplayer Demo Download][1]

 

* * *



* * *

 [1]: downloads/demos/item/demos/serious-sam-forever-multiplayer-demo