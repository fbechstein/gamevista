---
title: Natural Selection 2 – Alpha-Version wird bald veröffentlicht
author: gamevista
type: news
date: 2010-07-14T17:16:09+00:00
excerpt: '<p>[caption id="attachment_2016" align="alignright" width=""]<img class="caption alignright size-full wp-image-2016" src="http://www.gamevista.de/wp-content/uploads/2010/07/ns2_marine_hallway_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Die Entwickler von den <strong>Unknown World</strong> Studios lassen die Community im vollen Umfang am Entwicklungsprozess des Ego-Shooters <a href="http://www.unknownworlds.com/ns2/" target="_blank">Natural Selection 2</a> teil haben. Dies wurde wieder durch eine Newsmeldung auf der <a href="http://www.unknownworlds.com/ns2/" target="_blank">offiziellen Website</a> von NS2 bestätigt. Am 26. Juli 2010 wird nach langen Warten die erste spielbare Version den Fans und der Community präsentiert.</p> '
featured_image: /wp-content/uploads/2010/07/ns2_marine_hallway_small.jpg

---
Die Alpha-Version von <a href="http://www.unknownworlds.com/ns2/" target="_blank">Natural Selection 2</a> wird aber ausschließlich den Vorbestellern der Special Edition vorbehalten sein. Wer also den Release des Spiels nicht abwarten kann, der sollte auf der <a href="http://www.naturalselection2.com/buy" target="_blank">offiziellen Webseite</a> vorbeischauen und einen Kauf in betracht ziehen.

 

   

* * *

   

