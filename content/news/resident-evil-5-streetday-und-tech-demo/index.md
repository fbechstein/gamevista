---
title: Resident Evil 5 Streetday und Tech-Demo
author: gamevista
type: news
date: 2009-07-16T19:39:40+00:00
excerpt: '<p>[caption id="attachment_55" align="alignright" width=""]<img class="caption alignright size-full wp-image-55" src="http://www.gamevista.de/wp-content/uploads/2009/07/residentevil_small2.png" border="0" alt="Resident Evil 5" title="Resident Evil 5" align="right" width="140" height="100" />Resident Evil 5[/caption]Der Publisher <a href="http://www.capcom-europe.com/" target="_blank">Capcom </a>hat heute den Veröffentlichungstag der PC Version des Survival Horrors <strong>Resident Evil 5</strong> bekanntgegeben. Demnach erscheint das Actionspektakel am 18. September 2009. <br />Die PC Version soll inhaltlich keinen Unterschied zu der Xbox 360- und Playstation 3-Version darstellen und sogar mit Nvidias 3D Vision Brille zusammenarbeiten. </p>'
featured_image: /wp-content/uploads/2009/07/residentevil_small2.png

---
Damit Ihr Euch jetzt schon auf das Top Game hardwaretechnisch vorbereiten könnt, veröffentlicht Capcom eine [Tech-Demo][1], mit der Ihr testen könnt ob Euer PC den Hardwarehunger der Resident Evil Zombies stillen kann. Die Demo könnt Ihr hier herunterladen <[klick][1]> 







 [1]: downloads/demos/item/demos/resident-evil-5-tech-demo