---
title: Peter Molyneux – plant Ankündigung auf Microsoft Pressekonferenz
author: gamevista
type: news
date: 2009-07-28T19:37:20+00:00
excerpt: '<p>[caption id="attachment_195" align="alignright" width=""]<img class="caption alignright size-full wp-image-195" src="http://www.gamevista.de/wp-content/uploads/2009/07/blackandwhite2_small.jpg" border="0" alt="Black & White 2" title="Black & White 2" align="right" width="140" height="100" />Black & White 2[/caption]Laut Pressemitteilung wird <strong>Peter Molyneux</strong> auf der <a href="http://www.gamescom.de" target="_blank" title="gamescom 2009">gamescom</a> 2009 in Köln ein Wort an die Fangemeinschaft richten. Auf der Pressekonferenz von Microsoft soll er eine „besondere Ankündigung“ geplant haben. </p>'
featured_image: /wp-content/uploads/2009/07/blackandwhite2_small.jpg

---
Schon Hideo Kojima und Shingo Seabass Takatsuka von Konami kündigten ihren Besuch auf der <a href="http://www.gamescom.de" target="_blank" title="gamescom">gamescom </a>2009 an. Nun bestätigte Microsoft den Besuch von **Peter Molyneux**. **Molyneux** entwickelte unter anderem Hits wie Syndicate, Dungeon Keeper oder Black & White. Der Termin der Pressekonferenz wurde auf Mittwoch, den 19. August 2009 von 10 bis 11 Uhr gelegt.  
Wir werden euch berichten was **Herr Molyneux** neustes Ankündigung sein wird, wir sind gespannt. 







 