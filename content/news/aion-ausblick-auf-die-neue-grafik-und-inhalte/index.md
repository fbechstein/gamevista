---
title: Aion – Ausblick auf die neue Grafik und Inhalte
author: gamevista
type: news
date: 2009-11-24T17:29:11+00:00
excerpt: '<p>[caption id="attachment_224" align="alignright" width=""]<img class="caption alignright size-full wp-image-224" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion" title="Aion" align="right" width="140" height="100" />Aion[/caption]Der Publisher <a href="http://www.de.aiononline.com" target="_blank">NCSoft</a> hat für das Online-Rollenspiel <strong>Aion </strong>ein neues Video veröffentlicht. In dem acht Minuten langen Trailer werden die Pläne von <a href="http://www.de.aiononline.com" target="_blank">NCSoft</a> detailliert gezeigt. So wird das Online-Rollenspiel in naher Zukunft mit diversen Verbesserungen der Grafikengine aufwarten und mit neuen Inhalten versehen.</p> '
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
 

Wem Aion noch nichts sagt dem sei unsere [Review der Beta][1] ans Herz gelegt.

[> Zum Aion &#8211; Vision Trailer  
][2]    
**<span style="text-decoration: underline;">Hier die Verbesserungen im Detail:</span>**

Überarbeitung der Grafik: HDR-Effekte, verbesserter Schattenwurf, indirekte Beleuchtung   
Jahrzeiten-Wechsel mit Wettereffekten (Schnee, Regen, Sturm)   
Unterwassergebiete und Kämpfe (Schwimmen, Tauchen)   
Reittiere, auf denen man offenbar auch kämpfen kann oder sich Wettrennen in der Luft liefern kann   
Spieler-Häuser (Housing) und die individuelle Gestaltung der Innenräume; Entstehung von neuen Siedlungen   
Neue Gebiete (Zonen) und möglicherweise Belagerungsschlachten mit gigantischen Gegnern   
Neue Waffen, Kampftechniken und Animationen







 

 [1]: hot-spots/aion-beta-review
 [2]: videos/item/root/aion-vision-trailer