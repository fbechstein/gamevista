---
title: 'Command & Conquer 4 – Unterstützt kein LAN & dedizierte Server'
author: gamevista
type: news
date: 2010-02-22T18:40:43+00:00
excerpt: '<p>[caption id="attachment_157" align="alignright" width=""]<img class="caption alignright size-full wp-image-157" src="http://www.gamevista.de/wp-content/uploads/2009/07/shot_basg4y50_small.jpg" border="0" alt="Command & Conquer 4" title="Command & Conquer 4" align="right" width="140" height="100" />Command & Conquer 4[/caption]In der Vergangenheit hatten es PC-Fans nicht leicht. So wurden in den letzten Monaten diverse Spiele bekannt, die in Zukunft keinen LAN-Modus unterstützen oder dedizierte Server gänzlich fehlen. Nun hat der Entwickler von <strong>Electronic Arts</strong> im Rahmen eines Interviews bestätigt, dass der vierte Teil der Command & Conquer Reihe keine dedizierten Server unterstützen wird und auch kein LAN-Modus geplant sei.</p> '
featured_image: /wp-content/uploads/2009/07/shot_basg4y50_small.jpg

---
Grund dafür sei das **Global Player Progression System**. Dieses System registriert alle Spielarten, ob Multiplayer, Singleplayer oder Skirmish. Dabei werden ständig Daten an einen EA-Server übertragen und so Spielfortschritte gespeichert.

 

* * *



* * *