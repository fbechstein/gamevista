---
title: 'Arcania: Gothic 4 – Demo erscheint am bald'
author: gamevista
type: news
date: 2010-09-03T16:51:55+00:00
excerpt: '<p>[caption id="attachment_1648" align="alignright" width=""]<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Publisher <strong>JoWood </strong>gab heute den Veröffentlichungstermin der Demo zum Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> bekannt. Am 24. September 2010 wird die Testversion für PC und Xbox 360 bereit gestellt. Umfang der Demo werden einige Quests aus dem Hauptprogramm sein.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
<a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> erscheint am 12. Oktober 2010 für PC und Xbox 360, PlayStation-Besitzer müssen sich leider bis zum nächsten Jahr gedulden.

 

   

* * *

   

