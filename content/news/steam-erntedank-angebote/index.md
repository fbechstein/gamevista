---
title: Steam – Erntedank-Angebote
author: gamevista
type: news
date: 2010-11-28T18:54:21+00:00
excerpt: |
  |
    <p>[caption id="attachment_1229" align="alignright" width=""]<img class="caption alignright size-full wp-image-1229" src="http://www.gamevista.de/wp-content/uploads/2009/12/steam.jpg" border="0" alt="Steam" title="Steam" align="right" width="140" height="100" />Steam[/caption]Passend zum Erntdankfest bietet die Spieleplattform <a href="http://store.steampowered.com/" target="_blank">Steam</a> jeden Tag verschiedene Spieletitel vergünstigt an. Heute im Angebot stehen Highlights wie zum Beispiel <strong>Tom Clancy's Splinter Cell: Conviction</strong> für 20,09 Euro oder <strong>Patrizier IV - Steam Special Edition</strong> für 12,50 Euro.</p>
featured_image: /wp-content/uploads/2009/12/steam.jpg

---
Das komplette Angebot für den 29.11.2010 gilt bis Montag 19:00 Uhr. Die Spieletitel im Überblick:

 

Mount & Blade: Warband (5,- Euro)   
Tom Clancy&#8217;s Splinter Cell: Conviction (20,09 Euro)   
The Ball (8,99 Euro)   
Street Fighter IV (10,20 Euro)   
World of Goo (2,25 Euro)   
Patrizier IV &#8211; Steam Special Edition (12,50 Euro)   
The Orange Box (29,99 Euro)   
Bit.Trip Beat (3,99 Euro)

   

* * *

   

