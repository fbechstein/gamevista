---
title: Fifa 10 – Ronaldinho kann dribbeln!
author: gamevista
type: news
date: 2009-09-07T18:45:08+00:00
excerpt: '<p>[caption id="attachment_115" align="alignright" width=""]<img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" />Fifa 10[/caption]Publisher <a href="http://www.fifa.easports.com/deDE/home.action">Electronic Arts</a> veröffentlichte heute offiziell einen neuen Trailer zum Fussballspiel <strong>Fifa 10</strong>. Zu sehen gibt es den brasilianischen Starspieler Ronaldinho wie er im Videotutorial dem Zuschauer die grunlegenden Angriffsmethoden zeigt. <strong>Fifa 10</strong> erscheint am 1. Oktober 2009 in Deutschland.</p> '
featured_image: /wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg

---
[> Zum Fifa 10 &#8211; Basic Attacking Tutorial Trailer][1]

 

 

* * *



* * *

 [1]: videos/item/root/fifa-10-basic-attacking-tutorial