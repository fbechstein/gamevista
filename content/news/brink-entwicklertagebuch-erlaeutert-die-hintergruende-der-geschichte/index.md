---
title: Brink – Entwicklertagebuch erläutert die Hintergründe der Geschichte
author: gamevista
type: news
date: 2010-08-10T18:47:22+00:00
excerpt: '<p><strong>[caption id="attachment_1143" align="alignright" width=""]<img class="caption alignright size-full wp-image-1143" src="http://www.gamevista.de/wp-content/uploads/2009/12/brink-game.jpg" border="0" alt="Brink" title="Brink" align="right" width="140" height="100" />Brink[/caption]Bethesda </strong>veröffentlichte heute das erste Entwicklertagebuch für den Ego-Shooter <a href="http://www.brinkthegame.com" target="_blank">Brink</a>. Das Video klärt einige Fragen zur Hintergrundgeschichte des Spiels und zeigt dabei zahlreiche Spielszenen. Auch <a href="http://www.brinkthegame.com" target="_blank">Brink</a> wird auf der gamescom 2010 in Köln, die vom 18. – 22. August stattfindet, ausgestellt.</p> '
featured_image: /wp-content/uploads/2009/12/brink-game.jpg

---
Der Titel erscheint im Frühjahr 2011 für PC, PlayStation 3 und Xbox 360.

[> Zum Brink &#8211; Entwicklertagebuch #1][1]

   

* * *

   



 

 [1]: videos/item/root/brink-entwicklertagebuch-1