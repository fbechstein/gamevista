---
title: Assassin’s Creed 2 – Neues Entwicklertagebuch aufgetaucht
author: gamevista
type: news
date: 2009-10-23T19:19:53+00:00
excerpt: |
  |
    <p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin's Creed 2 " title="Assassin's Creed 2 " align="right" width="140" height="100" />Assassin's Creed 2 [/caption]Der Publisher <a href="http://www.ubi.com" target="_blank">Ubisoft</a> hat ein weiteres Entwicklertagebuch zum Actionspiel <strong>Assassin's Creed 2</strong> veröffentlicht. In dem über vier Minuten langen Video erklärt euch das Team rund um Ubisoft Montreal den Werdegang von Ezio in den fast 30 Jahren in den Assassin's Creed 2 spielt.</p>
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Der Trailer trägt den Namen „Kleider machen Leute“ und kann bei uns im Videobereich angeschaut werden.

[> Zum Assassin&#8217;s Creed 2 &#8211; Entwicklertagebuch #5][1]

* * *



* * *

 

 [1]: videos/item/root/assassins-creed-2-entwicklertagebuch-5