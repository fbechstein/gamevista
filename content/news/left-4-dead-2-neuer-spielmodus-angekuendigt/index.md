---
title: Left 4 Dead 2 – Neuer Spielmodus angekündigt
author: gamevista
type: news
date: 2009-10-08T18:56:16+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Laut Entwickler <a href="http://www.valvesoftware.com" target="_blank">Valve</a> wird es einen neuen Spielmodus für den zweiten Teil des Zombieshooters <strong>Left 4 Dead</strong> geben. Im <em>Scavenge Modus</em> ist es das Ziel der Überlebenden einen Generator mit möglichst viel Benzin zu befüllen. Die Infizierten müssen dies natürlich mit allen Mitteln verhindern.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Mit jedem Benzinkanister bekommen die Überlebenden 20 Sekunden mehr auf ihr Zeitkonto gutgeschrieben. **Left 4 Dead 2** kommt am 19. November in den deutschen Handel.

 

* * *



* * *

 

 

 