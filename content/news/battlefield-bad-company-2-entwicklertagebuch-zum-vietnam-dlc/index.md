---
title: 'Battlefield: Bad Company 2 – Entwicklertagebuch zum Vietnam-DLC'
author: gamevista
type: news
date: 2010-11-23T17:32:40+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Publisher <strong>Electronic Arts</strong> hat in Kooperation mit dem Entwickler <strong>DICE </strong>ein Entwicklertagebuch zum Zusatzinhalt <a href="http://www.battlefieldbadcompany2.com" target="_blank">Battlefield: Bad Company 2 Vietnam</a> veröffentlicht. <strong>DICE </strong>Community-Chef gibt neue Details zu den kommenden Änderungen des Mehrspielermodus bekannt. Zusätzlich gibt es auch einige Spielszenen zu sehen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Bisher gibt es keinen genauen Veröffentlichungstermin für den Download-Content, erscheinen wird dieser aber für PlayStation 3, Xbox 360 und PC.

[> Zum Battlefield: Bad Company 2 &#8211; Vietnam-Entwicklertagebuch][1]

   

* * *

   



 

 [1]: videos/item/root/battlefield-bad-company-2--vietnam-dlc-entwicklertagebuch