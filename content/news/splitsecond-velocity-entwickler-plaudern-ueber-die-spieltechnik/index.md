---
title: 'Split/Second: Velocity – Entwickler plaudern über die Spieltechnik'
author: gamevista
type: news
date: 2010-05-25T16:30:47+00:00
excerpt: '<p>[caption id="attachment_1691" align="alignright" width=""]<img class="caption alignright size-full wp-image-1691" src="http://www.gamevista.de/wp-content/uploads/2010/04/splitsecond_hero.jpg" border="0" alt="Split/Second: Velocity" title="Split/Second: Velocity" align="right" width="140" height="100" />Split/Second: Velocity[/caption]Das Entwicklerteam von <strong>Black Rock Studios</strong> präsentiert einen neuen Trailer für das Rennspiel <a href="www2.disney.co.uk/split-second-velocity/" target="_blank">Split/Second: Velocity</a>. In dem mittlerweile fünften Entwicklertagebuch plaudern die Mitarbeiter über die Technik und die Spezialeffekte de in <a href="www2.disney.co.uk/split-second-velocity/" target="_blank">Split/Second: Velocity</a> integriert wurden. <br /><br /></p> '
featured_image: /wp-content/uploads/2010/04/splitsecond_hero.jpg

---
So fliegen euch allerlei an der Rennstrecke befindlichen Kulissen um die Ohren während ihr vom Gegner abgedrängt  werdet. Natürlich kommt es wie bei jedem Rennspiel auch auf Geschick und Können an, aber auch spektakuläre Physikeffekte tragen ihren Teil zum Sieg bei.

[> Zum Split/Second: Velocity Entwicklertagebuch #5][1]

   

* * *

   



 [1]: videos/item/root/splitsecond-velocity-entwicklertagebuch-5