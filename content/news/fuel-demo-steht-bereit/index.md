---
title: Fuel – Demo steht bereit
author: gamevista
type: news
date: 2009-07-09T16:00:27+00:00
excerpt: '<p>[caption id="attachment_31" align="alignright" width=""]<img class="caption alignright size-full wp-image-31" src="http://www.gamevista.de/wp-content/uploads/2009/07/fuel_small.png" border="0" alt="Fuel - Demo zum Rennspiel steht bereit" title="Fuel - Demo zum Rennspiel steht bereit" align="right" width="140" height="100" />Fuel - Demo zum Rennspiel steht bereit[/caption]Ab sofort steht Euch die Demo zum <a href="http://www.codemasters.de/index.php?territory=German" target="_blank">Codemasters</a> Rennspiel <strong>Fuel</strong> im <a href="index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=43">Downloadbereich</a> zur Verfügung. In der Demo könnt Ihr Euch im Ocean Rush-Event gegen sieben Mitkonkurrenten beweisen. </p>'
featured_image: /wp-content/uploads/2009/07/fuel_small.png

---
Die Demo ist 1,03 GB gross und kann [hier][1] heruntergeladen werden. 

&nbsp;







 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=43