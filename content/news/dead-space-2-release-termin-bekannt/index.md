---
title: Dead Space 2 – Release Termin bekannt
author: gamevista
type: news
date: 2010-06-15T17:42:37+00:00
excerpt: '<p>[caption id="attachment_1151" align="alignright" width=""]<img class="caption alignright size-full wp-image-1151" src="http://www.gamevista.de/wp-content/uploads/2009/12/deadspace2.jpg" border="0" alt="Dead Space 2" title="Dead Space 2" align="right" width="140" height="100" />Dead Space 2[/caption]Der Publisher <strong>Electronic Arts</strong> veröffentlichte heute den Release-Termin für den Grusel-Shooter <a href="http://www.deadspace.ea.com/" target="_blank">Dead Space 2</a>. Am 28. Januar 2011 wird der zweite Teil in Europa in den Handel kommen. Zur Story sagt <strong>EA </strong>folgendes.</p> '
featured_image: /wp-content/uploads/2009/12/deadspace2.jpg

---
&#8222;Nach seinem Erwachen an einem dunklen und rätselhaften Ort findet sich der Held von Dead Space 2, Isaac Clarke, inmitten einer gewaltigen Invasion untoter Monster, den Nekromorphs, wieder. Umgeben von den schrillen Schreien der Monster und dem unheimlichen Echo ihrer Zerstörungswut, bahnt sich Isaac einen Weg von einem gespenstischen, verlassenen Gebäude zum nächsten. Dem Wahnsinn nahe, wird Isaac alles tun, um zu überleben und den unerbittlichen Ansturm des Feindes aufzuhalten. 

Schauplatz der Handlung ist die neue Raumstation Der Sprawl. Mit einem neuen Arsenal an Waffen für den Kampf gegen die Nekromorphs stellt sich Isaac der Herausforderung mit offenem Visier. Die Spieler werden Isaac in noch größeren und schrecklicheren Kämpfen erleben, die niemanden kalt lassen dürften. Dead Space 2 besticht durch ein erstklassiges Sounddesign, eine herausragende Grafik und überragende Actionsequenzen.&#8220;

<p style="text-align: center;">
  <img class=" size-full wp-image-1920" src="http://www.gamevista.de/wp-content/uploads/2010/06/ds2_e32010_05.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/06/ds2_e32010_05.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/06/ds2_e32010_05-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

