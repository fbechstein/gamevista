---
title: World of Warcraft – Hotfix 3.3.0a veröffentlicht
author: gamevista
type: news
date: 2009-12-15T19:43:03+00:00
excerpt: '<p>[caption id="attachment_163" align="alignright" width=""]<img class="caption alignright size-full wp-image-163" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="139" height="100" />World of Warcraft[/caption]Der Publisher <a href="http://www.wow-europe.com/de" target="_blank">Blizzard</a> hat heute einen Hotfix für den vor kurzem erschienen Patch 3.3.0 veröffentlicht. Das Update mit der Versionsnummer 3.3.0a hat gerade einmal die Größe von fünf Megabyte und verändert unter anderem ein paar Klassenfertigkeiten. Die komplette Liste der Änderungen zur Übersicht:</p> '
featured_image: /wp-content/uploads/2009/07/wow_small.png

---
[> Zum World of Warcraft &#8211; Patch 3.3.0a Download][1]

**<span style="text-decoration: underline;">Changelog:</span>**

  * In Ulduar, the Salvaged Demolisher’s Hurl Boulder ability will now properly ignite tar.
  * Vault of Archavon bosses are again able to be hit by area of effect abilities.
  * Chains of Ice is no longer limited to a single target. In addition, when Chains of Ice is used on a snare immune target with Endless Winter talented, it will once again apply Frost Fever.
  * Druids will now be able to shapeshift out of a spell-reflected Entangling Roots spell.
  * The trinket Ephemeral Snowflake will now be triggered by all healing spells.
  * The players’ Gunship will now appropriately dock after achieving victory.
  * Lord Marrowgar will now do significantly less melee damage in both the 10 player normal and 10 player heroic difficulty.
  * Everlasting Affliction can now refresh the duration of Corruption on a target when the Warlock casts Shadow Bolt. This talent also refreshes the duration of Corruption with Drain Life and Haunt.

* * *



* * *

 

 [1]: downloads/patches/item/patches/world-of-warcraft-patch-330a