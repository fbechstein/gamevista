---
title: Uncharted 2 – Neue Karte und Update
author: gamevista
type: news
date: 2009-11-26T21:49:28+00:00
excerpt: '<p />[caption id="attachment_39" align="alignright" width=""]<img class="caption alignright size-full wp-image-39" src="http://www.gamevista.de/wp-content/uploads/2009/07/uncharted2_small.png" border="0" alt="Uncharted 2" title="Uncharted 2" align="right" width="140" height="100" />Uncharted 2[/caption]Wie der Entwickler <a href="http://www.unchartedthegame.com" target="_blank">Naughty Dog</a> bereits berichtet hat wird es einen Nachfolger zu <strong>Uncharted 2</strong> geben, zumindest hat das Nathan Drake-Synchronsprecher Nolan North gegenüber des Official PlayStation Magazine gesagt.<em /> "Also ich weiß, dass wir bestimmt einen dritten Teil machen werden. <br />  '
featured_image: /wp-content/uploads/2009/07/uncharted2_small.png

---
Es wäre finanziell untragbar, würden wir dies nicht tun,“ </em>so North. __Diesen Freitag wird es außerdem eine neue Mehrspielerkarte für **Uncharted 2** geben. Mit dem Namen „**The Fort**“ kommt diese völlig kostenlos auf eure Konsole. Zusätzlich wird ein Update bereitgestellt das das Multiplayer-Menü verbessert und den Online Modus verbessern soll. Wie die neue Karte aussieht seht ihr in folgendem Video.

[> Zum Uncharted 2 &#8211; The Fort Map Trailer][1]







 

 [1]: videos/item/root/uncharted-2-the-fort-map-trailer