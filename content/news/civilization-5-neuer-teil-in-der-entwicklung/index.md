---
title: Civilization 5 – Neuer Teil in der Entwicklung
author: gamevista
type: news
date: 2010-02-19T08:12:59+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Der Publisher <strong>2K Games</strong> hat im Rahmen einer Pressemitteilung, offiziell die Entwicklung des nunmehr fünften Teils der Sid Meier`s Civilization-Reihe bestätigt. Laut dem Entwickler <strong>Firaxis Game</strong>s soll <a href="http://www.civilization5.com/" target="_blank">Civilization 5</a> bereits im Herbst dieses Jahres in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Erste Details sind eine neue Grafik-Engine und besserer Support im Mehrspielerbereich. So wird es neue Spielfunktionen geben die das Austauschen von Spielinhalten ermöglichen soll. Neben einer schickeren Grafik, wird auch das Modifizieren von <a href="http://www.civilization5.com/" target="_blank">Civilization 5</a> wieder möglich sein.

<p style="text-align: center;">
  <img class="caption size-full wp-image-1471" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_600.jpg" border="0" alt="Civilization 5 mit neuer Grafik-Engine" title="Civilization 5 mit neuer Grafik-Engine" width="600" height="386" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_600.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/civ5_600-300x193.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *



* * *