---
title: 'Arcania: A Gothic Tale – Gothic 4 ohne Bugs?'
author: gamevista
type: news
date: 2009-07-28T20:25:27+00:00
excerpt: '<p>[caption id="attachment_117" align="alignright" width=""]<img class="caption alignright size-full wp-image-117" src="http://www.gamevista.de/wp-content/uploads/2009/07/arcania_32_small.jpg" border="0" alt="Arcania: A Gothic Tale" title="Arcania: A Gothic Tale" align="right" width="140" height="100" />Arcania: A Gothic Tale[/caption]In einem Interview äußerte sich <a href="http://www.spellbound.de" target="_blank" title="spellbound">Spellbounds </a>Creative Producer Steffen Rühl zum neuen <strong>Gothic – Arcania: A Gothic Tale</strong>. Wie aus dem Interview herauszuhören war kümmern sich nun zwei Teams um die Qualitätssicherung. Beide Teams erhalten regelmäßig Entwicklerversionen des Rollenspiels so soll sichergestellt werden das Bugs frühzeitig erkannt und entfernt werden können. </p>'
featured_image: /wp-content/uploads/2009/07/arcania_32_small.jpg

---
Ein weiterer Schritt werden diverse Community Betatests sein, an denen 50 bis 100 Communitymitglieder teilnehmen können. Laut <a href="http://www.spellbound.de" target="_blank" title="spellbound">Spellbound</a> sollen keine schwerwiegenden Bugs im neuen **Gothic** Teil enthalten sein. Wir erinnern uns nur ungern an die unspielbare Releaseversion von **Gothic 3**. <a href="http://www.jowood.com" target="_blank" title="jowood">JoWood</a> Produzent Johann Ertl teilte weiterhin mit das sie Notfalls den Release verschieben werden, würde man feststellen das **Arcania: A Gothic Tale** größere Bugs enthalte. Der geplante Release soll bei Ende 2009 liegen. 

[  
][1] 







 [1]: videos/item/root/risen-nightwish-trailer