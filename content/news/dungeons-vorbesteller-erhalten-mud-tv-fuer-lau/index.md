---
title: Dungeons – Vorbesteller erhalten M.U.D. TV für lau
author: gamevista
type: news
date: 2011-01-18T18:42:24+00:00
excerpt: '<p>[caption id="attachment_2526" align="alignright" width=""]<img class="caption alignright size-full wp-image-2526" src="http://www.gamevista.de/wp-content/uploads/2011/01/dungeons.jpg" border="0" alt="Dungeons" title="Dungeons" align="right" width="140" height="100" />Dungeons[/caption]Interessenten des bald erscheinenden <strong>Dungeons </strong>dürfte der Kauf des Spiels nun noch leichter fallen. Auf der Onlineplattform <strong>Steam </strong>gibt es derzeit ein <a href="http://store.steampowered.com/app/57650" target="_blank">Angebot</a> für Vorbesteller des Dungeon Keeper-Nachfolgers. Demnach erhalten alle Käufer die das Spiel vor der Veröffentlichung bestellen die Wirtschaftssimulation <strong>M.U.D. TV</strong> gratis.</p> '
featured_image: /wp-content/uploads/2011/01/dungeons.jpg

---
Dungeons erscheint auf Steam offiziell am 11. Februar 2011, im Laden steht es bereits am 27. Januar 2011. Wer also die paar Tage verkraften kann und wartet bekommt beide Spiele für 49,99 Euro. In **M.U.D. TV** übernehmt ihr die Kontrolle über einen Fernsehsender, genau wie in der Vorlage des Vorgängers **Mad TV**.

 

* * *



* * *