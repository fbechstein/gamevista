---
title: Borderlands – Erster DLC auch für PC Spieler
author: gamevista
type: news
date: 2009-11-10T17:29:26+00:00
excerpt: '<p>[caption id="attachment_1021" align="alignright" width=""]<img class="caption alignright size-full wp-image-1021" src="http://www.gamevista.de/wp-content/uploads/2009/11/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Der Entwickler <a href="http://www.borderlandsthegame.com" target="_blank">Gearbox Software</a> hat heute mit einer offiziellen Pressemitteilung den ersten Zusatzinhalt für den Rollenspiel-Shooter <strong>Borderlands </strong>angekündigt. Der erste DLC trägt den Namen „<strong>The Zombie Island of Dr. Ned</strong>“ und ist von <a href="http://www.borderlandsthegame.com" target="_blank">Gearbox Software</a> mit einem Veröffentlichungstermin am 24. November 2009 angegeben.</p> '
featured_image: /wp-content/uploads/2009/11/smalllogo.jpg

---
Dieser Termin ist aber nur für Konsolenspieler relevant. PC Spieler müssen sich wie immer etwas länger gedulden. Dies wurde aus einem <a href="http://twitter.com/gearboxsoftware" target="_blank">Twitter Beitrag</a> bekannt. So planen die Entwickler den Download Content auch für PC anzubieten. Ein genaues Datum dafür wurde aber nicht genannt.

* * *



* * *