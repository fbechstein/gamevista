---
title: Risen Präsentation auf der gamescom
author: gamevista
type: news
date: 2009-08-04T12:57:50+00:00
excerpt: '<p>[caption id="attachment_275" align="alignright" width=""]<img class="caption alignright size-full wp-image-275" src="http://www.gamevista.de/wp-content/uploads/2009/08/risentier.png" border="0" alt="Auf der gamescom könnt Ihr Risen auf 30 Stationen antesten" title="Auf der gamescom könnt Ihr Risen auf 30 Stationen antesten" align="right" width="140" height="100" />Auf der gamescom könnt Ihr Risen auf 30 Stationen antesten[/caption]Auf der gamescom werdet Ihr die Möglichkeit erhalten <strong>Risen</strong> auf insgesamt 30 Spielstation anzutesten. 20 Stationen sind PC Stationen und 10 Stationen werden Xbox 360 Stationen sein. <a href="http://www.deepsilver.com/" target="_blank">Deep Silver</a> und <a href="http://www.piranha-bytes.com/" target="_blank">Piranha Bytes</a> präsentieren eine auf die gamescom zugeschnittene Version wo einige Quests nicht erhalten sein werden, jedoch die komplette Insel erforscht werden kann.</p>'
featured_image: /wp-content/uploads/2009/08/risentier.png

---
Zusätzlich habt Ihr die Möglichkeit die Entwickler zu befragen. 

Anbei die Termine 

&#8211; Mittwoch, 19. August: 11:30 Uhr und 15.00 Uhr  
&#8211; Donnerstag, 20. August: 12.30 Uhr und 17.30 Uhr  
&#8211; Freitag, 21. August: 12.30 Uhr und 17.30 Uhr  
&#8211; Samstag, 22. August: 10.30 Uhr, 12.45 Uhr, 16.45 Uhr und 18.30 Uhr  
&#8211; Sonntag, 23. August: 12.15 Uhr und 15.00 Uhr

<div id="intelliTXT" class="textblock">
  Ihr findet die Deep Silver-Bühne in der Halle 7.1 am Stand B041.
</div>

<div class="textblock">
  
</div>

<div class="textblock">
  
</div>

<div class="textblock">
  
</div>