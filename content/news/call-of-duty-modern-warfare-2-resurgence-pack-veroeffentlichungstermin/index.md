---
title: 'Call of Duty: Modern Warfare 2 – Resurgence Pack Veröffentlichungstermin'
author: gamevista
type: news
date: 2010-05-15T18:46:28+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Der Publisher <strong>Activision </strong>hat heute im Rahmen einer Pressemeldung den Veröffentlichungstermin für den zweiten Zusatzinhalt für den Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare 2</a> bekannt gegeben. Demnach wird der zweite DLC am 3. Juni offiziell für Xbox 360 verfügbar sein.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Das **Resurgency Map Pack** umfasst fünf Mehrspielerkarten,  darunter sind wieder zwei alte bekannte Maps aus dem ersten Teil dabei. Bei den beiden Karten handelt es sich um Vacant und Strike. Die drei komplett neuen Karten heißen Carnival (Vergnügungspark), Fuel (Öl-Raffinerie) und Trailer Park.  

Der Preis dürfte sich wieder bei ca. 15 Euro einpendeln. Einen genauen Release-Termin für PlayStation 3 und PC gibt es bisher nicht. Denkbar ist wie beim Stimulus Pack eine Veröffentlichung einen Monat nach dem Release der Xbox 360-Version.

 

   

* * *

   

