---
title: 'Sniper: Ghost Warrior – Erscheint ungekürzt & Veröffentlichungstermin'
author: gamevista
type: news
date: 2010-05-20T18:23:24+00:00
excerpt: '<p>[caption id="attachment_1820" align="alignright" width=""]<img class="caption alignright size-full wp-image-1820" src="http://www.gamevista.de/wp-content/uploads/2010/05/sniper_ghost_warrior.jpg" border="0" alt="Sniper Ghost Warrior" title="Sniper Ghost Warrior" align="right" width="140" height="100" />Sniper Ghost Warrior[/caption]Der <strong>Publisher City</strong> Interactive hat im Rahmen einer Pressemitteilung gute Nachrichten für alle deutschen Fans des Ego-Shooters <a href="http://www.sniperghostwarrior.com/" target="_blank">Sniper: Ghost Warrior</a> verkündet. Demnach wird das Actionspiel laut Einschätzung der USK „keine Jugendfreigabe“ erhalten und völlig ungekürzt erscheinen.</p> '
featured_image: /wp-content/uploads/2010/05/sniper_ghost_warrior.jpg

---
Einen genauen Releasetermin gibt’s auch noch. <a href="http://www.sniperghostwarrior.com/" target="_blank">Sniper: Ghost Warrior</a> wird am 24. Juni 2010 in den deutschen Handel kommen. 

**Features_:_**

  * Erlebe ein absolut **realistisches Ballistik-System**, das nicht nur die Distanz, sondern auch Umgebungsparameter, wie beispielsweise den Wind, mit einberechnet.
  * **Kontrolliere deine Atmung**, um die Präzision deiner Schüsse zu erhöhen.
  * **Arbeite mit deinem Beobachter zusammen**, um noch effizienter zu agieren.Verfolge im Bullet-Cam-Modus, wie deine Kugeln mit tödlicher Präzision ihr Ziel finden.

   

* * *

   

