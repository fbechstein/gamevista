---
title: R.U.S.E. – Erster DLC wird kostenlos erscheinen
author: gamevista
type: news
date: 2010-11-18T19:44:07+00:00
excerpt: '<p>[caption id="attachment_2444" align="alignright" width=""]<img class="caption alignright size-full wp-image-2444" src="http://www.gamevista.de/wp-content/uploads/2010/11/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Publisher <strong>Ubisoft </strong>hat angekündigt, dass im Dezember der erste Zusatzinhalt für das Strategiespiel <a href="http://ruse.de.ubi.com/" target="_blank">R.U.S.E.</a> erscheinen wird. Das The Manhattan Protject Packet wird neben drei neuen Karten für den Mehrspielermodus, auch zwei weitere Spielmodi enthalten. Im Nuclear War-Modus sind von Spielstart an alle Upgrades freigeschaltet.</p> '
featured_image: /wp-content/uploads/2010/11/ruse_small.jpg

---
Der zweite Modi mit dem Namen Total War setzt auf die Einheiten von Kriegsbeginn im Jahr 1939, nach und nach schalten sich dann die neuen Upgrades frei. Einen genauen Veröffentlichungstermin gibt es bisher nicht, allerdings wird der DLC völlig kostenlos für PC, Xbox 360 und PlayStation 3 erscheinen wird.

 

 

   

* * *

   

