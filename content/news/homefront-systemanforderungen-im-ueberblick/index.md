---
title: Homefront – Systemanforderungen im Überblick
author: gamevista
type: news
date: 2011-01-25T20:29:57+00:00
excerpt: '<p>[caption id="attachment_1910" align="alignright" width=""]<img class="caption alignright size-full wp-image-1910" src="http://www.gamevista.de/wp-content/uploads/2010/06/homefront.jpg" border="0" alt="Homefront " title="Homefront " align="right" width="140" height="100" />Homefront [/caption]Publisher <strong>THQ </strong>hat die Systemanforderungen für den bald erscheinenden Ego-Shooter <a href="http://www.homefront-game.com/" target="_blank">Homefront</a> veröffentlicht. Als minimale Hardware-Voraussetzungen sind ein Intel Core 2 Duo 2,4 GHZ oder AMD X2 Athlon X2 2,8 GHZ von Nöten. Zusätzlich sollten 2 GByte Ram Arbeitsspeicher sowie eine GeForce 7900 GS oder Radeon 1900XT verbaut sein.</p> '
featured_image: /wp-content/uploads/2010/06/homefront.jpg

---
Optimal wären eine Quadcore CPU und eine GeForce 260 bzw. Radeon 4850. <a href="http://www.homefront-game.com/" target="_blank">Homefront</a> erscheint am 8. März 2011 für PC, Xbox 360 und PlayStation 3 im Handel. Die Systemvorrausetzungen im Überblick:

**Minimale Voraussetzungen**

  * Windows XP, Windows Vista oder Windows 7
  * Intel Pentium Core 2 Duo 2,4 GHz oder AMD Athlon X2 2,8 GHz.
  * 2 GByte RAM
  * Shader Model 3,0 graphics card mit 256MByte Speicher
  * NVIDIA GeForce 7900GS oder ATI Radeon 1900XT
  * 10GByte freier Festplattenspeicher

**Empfohlene Anforderungen**

  * Windows Vista oder Windows 7
  * Quad Core 2 GHz+ CPU
  * 2 GByte RAM
  * NVidia GeForce 260 oder ATI Radeon 4850
  * 10 GByte freier Festplattenspeicher

* * *



* * *