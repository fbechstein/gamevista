---
title: Runes of Magic Game Client und Screenshots
author: gamevista
type: news
date: 2009-08-05T10:49:07+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Ab sofort findet Ihr in unserer Downloadsektion den kompletten Game Client zu <strong>Runes of Magic</strong>. Das MMORPG ist kostenlos und der Game Client kann in einer 3,6 GB Datei heruntergeladen werden. </p>'
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Die Voraussetzungen zum spielen sind: 

CPU: Intel Pentium 4 2,0 GHz oder ähnliches Äquivalent  
Speicher: 512MB RAM  
benötigter Festplattenspeicher: 3,8 GB  
Grafikkarte: DirectX 9.0c kompatibel, 128MB Grafikspeicher  
Breitband Internetverbindung  
Tastatur und Maus

Ferne haben wir knapp 250 Screenshots in der Gallery hinterlegt.

[> Runes of Magic Client downloaden][1]  
[> Runes of Magic Screenshots ansehen][2]







 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=147
 [2]: index.php?Itemid=111&option=com_zoo&view=item&category_id=0&item_id=146