---
title: Armed Assault 2 – Addon angekündigt
author: gamevista
type: news
date: 2009-08-14T10:27:01+00:00
excerpt: '<p>[caption id="attachment_293" align="alignright" width=""]<img class="caption alignright size-full wp-image-293" src="http://www.gamevista.de/wp-content/uploads/2009/08/arma2_small.png" border="0" alt="Armed Assault 2" title="Armed Assault 2" align="right" width="140" height="100" />Armed Assault 2[/caption]Entwickler<a href="http://www.bistudio.com" target="_self"> Bohemia Interactive</a> hat nun per Pressemitteilung verlauten lassen das sie aktuell an einem Addon zu ihrer Kriegssimulation <strong>Armed Assault 2</strong> arbeiten. Das Addon trägt den Namen <strong>Operation Arrowhead</strong> und wird 3 Jahre nach dem Konflikt in Cernarus angesiedelt sein.</p> '
featured_image: /wp-content/uploads/2009/08/arma2_small.png

---
<p style="margin-bottom: 0cm;">
  In diesem fiktiven Konflikt werdet ihr unter der Führung der US Army nach Takistan geschickt um dort Frieden zu verbreiten. Das Addon <strong>Armed Assault 2: Operation Arrowhead</strong> wird laut <a href="http://www.bistudio.com" target="_self">Bohemia Interactive</a> ein eigenständig laufendes Spiel werden. Demnach wird kein Hauptspiel benötigt. <strong>Operation Arrowhead</strong> könnt ihr übrigens auf der in der nächsten Woche stattfindenden gamescom antesten. Findet könnt ihr den Stand von <strong>Bohemia Interactive</strong> in Halle 4.2, Stand L034. Wann das Addon erscheinen wird ist bisher noch unbekannt.
</p>







 