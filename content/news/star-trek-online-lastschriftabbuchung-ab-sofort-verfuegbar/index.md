---
title: Star Trek Online – Lastschriftabbuchung ab sofort verfügbar
author: gamevista
type: news
date: 2010-02-11T21:32:57+00:00
excerpt: '<p>[caption id="attachment_1438" align="alignright" width=""]<img class="caption alignright size-full wp-image-1438" src="http://www.gamevista.de/wp-content/uploads/2010/02/smallspace.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Bisher konnte man für ein Abo des Online-Rollenspiels <a href="http://www.startrekonline.com/splash?redir=node/1084" target="_blank">Star Trek Online</a> nur per Kreditkarte, Paypal oder Gametime Karte bezahlen. Nun hat der Publisher <strong>Atari </strong>für deutsche Kunden eine wichtige Bezahlmethode hinzugefügt. Ab sofort wird es möglich sein, die Gebühren für <a href="http://www.startrekonline.com/splash?redir=node/1084" target="_blank">Star Trek Online</a>, auch per Lastschriftverfahren vom Konto abbuchen zu lassen.</p> '
featured_image: /wp-content/uploads/2010/02/smallspace.jpg

---
Alles weitere findet ihr auf der <a href="http://www.startrekonline.com/splash?redir=node/1084" target="_blank">offiziellen Website</a>.

 

* * *



* * *