---
title: 'Batman: Arkham Asylum – DLCs Details bekannt'
author: gamevista
type: news
date: 2009-09-14T18:38:25+00:00
excerpt: '<p>[caption id="attachment_694" align="alignright" width=""]<img class="caption alignright size-full wp-image-694" src="http://www.gamevista.de/wp-content/uploads/2009/09/batman_arkham_asylum_small.jpg" border="0" alt="Batman: Arkham Asylum " title="Batman: Arkham Asylum " align="right" width="140" height="100" />Batman: Arkham Asylum [/caption]Schon lange ist bekannt, dass <a href="http://eidos.com/" target="top">Eidos</a> DLCs für <strong>Batman: Arkham Asylum</strong> veröffentlichen will. Nun endlich hat man einen offiziellen Termin bekannt gegeben: Der erste DLC erscheint am 17. und der zweite am 24. September.</p> <p>Zusätzlich sind auch noch Details der DLCs bekannt geworden.</p> '
featured_image: /wp-content/uploads/2009/09/batman_arkham_asylum_small.jpg

---
Im ersten DLS „Totally Insane FreeFlow Combat map“ müssen sich die Spieler extrem hartnäckigen und schnellen Gegnern in den Weg stellen, die versuchen aus der Anstalt auszubrechen. Dabei wird auch den besten Spielern einiges abverlangt werden, so Eidos. Schnelle Reaktionen und punktgenaues Timing sind erforderlich um die vielen Wellen von Verrückten besiegen zu können.

Der zweite DLC ist komplett anders und heißt „Nocturnal Hunter Invisible Predator map“. In dieser ist der Spieler auf sein Talent als Räuber angewiesen. Um die Heerscharen des Jokers aufzuhalten, muss der Spieler sein ganzes Arsenal an Geräten einsetzen und den Schatten noch intensiver nutzen.

Die DLCs werden für die PS3, Xbox360 und die PC-Version des Spiels erscheinen.

* * *



* * *