---
title: R.U.S.E. – Systemvorrausetzungen zur bald startenden Beta
author: gamevista
type: news
date: 2010-03-07T15:46:12+00:00
excerpt: '<p>Zur bald startenden öffentlichen Beta-Phase des Strategiespiels <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a>, hat das Team rund um <strong>Ubisoft </strong>nun die Systemvorrausetzungen bekannt gegeben. Der Beta-C[caption id="attachment_1526" align="alignright" width=""]<img class="caption alignright size-full wp-image-1526" src="http://www.gamevista.de/wp-content/uploads/2010/03/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]lient steht ab sofort auf der Onlineplattform <strong>Steam </strong>zum Download bereit und ab Dienstag dem 9. März wird die Beta freigeschaltet. <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a> erscheint voraussichtlich im Mai 2010 im Handel.</p> '
featured_image: /wp-content/uploads/2010/03/ruse_small.jpg

---
 

**Minimale Systemvoraussetzungen**

  * Betriebssystem: Windows XP (mit Service Pack 3) oder Windows Vista (mit Service Pack 2) oder Windows 7
  * CPU: 2,8 GHz Intel Pentium 4 oder AMD Athlon 64 3000+ oder besser
  * RAM: 1 GB für XP; 2 GB für Vista und Win7
  * Grafikkarte: 128 MB DirectX 9.0c-kompatibel (ATI Radeon X1000 oder GeForce 6 Serie oder besser)
  * DVD-ROM: DVD-ROM Speed 4x, Dual-Layer-Laufwerk
  * Soundkarte: DirectX 9.0c-kompatibel
  * DirectX 9.0c

**Empfohlene Systemvoraussetzungen**

  * Betriebssystem: Windows XP (mit Service Pack 3) oder Windows Vista (mit Service Pack 2) oder Windows 7
  * CPU: 3,5 GHz Intel Pentium D oder AMD Athlon 64 3500+ oder besser
  * RAM: 2 GB (XP/Vista/Win7)
  * Grafikkarte: 256 MB DirectX 9.0c-kompatibel (ATI Radeon X1900 oder GeForce 7800 Serie oder besser)
  * DVD-ROM: DVD-ROM Speed 4x, Dual-Layer-Laufwerk
  * Soundkarte: DirectX 9.0c-kompatibel
  * DirectX 9.0c

   

* * *

   



 