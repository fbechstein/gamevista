---
title: Mad Skills Motorcross Demo erschienen
author: gamevista
type: news
date: 2009-08-26T08:55:26+00:00
excerpt: '<p>[caption id="attachment_501" align="alignright" width=""]<img class="caption alignright size-full wp-image-501" src="http://www.gamevista.de/wp-content/uploads/2009/08/madskill_small.png" border="0" alt="Mad Skills Motorcross" title="Mad Skills Motorcross" align="right" width="140" height="100" />Mad Skills Motorcross[/caption]Vom Entwickler <a href="http://turborilla.com/" target="_top">Turborilla</a> erschien die Demo zu <strong>Mad Skills Motorcross</strong>.</p> '
featured_image: /wp-content/uploads/2009/08/madskill_small.png

---
Mad Skills Motorcross ist ein Motorrad Rennspiel auf dem Ihr über 50 Strecken befahren könnt. Powerups und ein Editor runden das Spiel ab. In der Demo stehen Euch die ersten Tracks der Vollversion zum anspielen bereit.

[> zur Mad Skills Motorcross Demo][1]

* * *



* * *

 [1]: downloads/demos/item/demos/mad-skills-motocross-demo