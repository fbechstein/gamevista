---
title: 'Splinter Cell: Conviction – Der Aufruhr DLC wird bald erscheinen'
author: gamevista
type: news
date: 2010-05-18T19:27:31+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Der nächste Zusatzinhalt für den Taktik-Shooter <a href="http://www.splintercell.us.ubi.com/" target="_blank">Splinter Cell: Conviction</a> steht vor der Tür. Bereits nächste Woche Donnerstag, den 27. Mai, wird der Download Content <strong>Der Aufruhr</strong> für Xbox 360 und PC erscheinen. Inhaltlich gibt es einiges neues zu entdecken.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
Neben neuen Koop-Missionen werden auch neue Mehrspielerkarten angeboten. Kostenpunkt sind 9,99 Euro bzw. 800 Microsoftpunkte. 

Neben Schauplätzen an den Docks von San Francisco, einem Friedhof in New Orleans, einem Gefängnis in Portland bekommt ihr auch eine Forschungsstation in Salt Lake City zu sehen.

 

   

* * *

   

