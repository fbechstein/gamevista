---
title: 'Battlefield: Bad Company 2 – Mit Hardcore-Modus und 3D Vision Unterstützung'
author: gamevista
type: news
date: 2010-02-10T21:01:43+00:00
excerpt: '<p>[caption id="attachment_1433" align="alignright" width=""]<img class="caption alignright size-full wp-image-1433" src="http://www.gamevista.de/wp-content/uploads/2010/02/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2 mit 3D Vision Technik" title="Battlefield: Bad Company 2 mit 3D Vision Technik" align="right" width="140" height="100" />Battlefield: Bad Company 2 mit 3D Vision Technik[/caption]Der Entwickler <strong>DICE </strong>hat im Rahmen eines Interviews, die Unterstützung von Nvidias 3D Vision-Technik für den kommenden Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> bestätigt. Außerdem wird das im März erscheinende Spiel über einen Hardcore-Modus verfügen. In diesem ist der Waffenschaden doppelt so hoch wie im Gegensatz zum Standard-Modus von <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a>.</p> '
featured_image: /wp-content/uploads/2010/02/badcompany2logo.jpg

---
Laut Aussage der Entwickler, wird es möglich sein Gegner mit einem Schuss zu Boden zu bringen. Dabei werden die Hilfsanzeigen auf dem Bildschirm auf das Nötigste minimiert. Es wird kein Fadenkreuz und keine Munitionsanzeige geben.  
Battlefield: Bad Company 2 erscheint am 4. März 2010 für PlayStation 3, Xbox 360 und PC im Handel.

 

* * *



* * *