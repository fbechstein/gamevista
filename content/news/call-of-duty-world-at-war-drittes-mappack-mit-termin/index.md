---
title: 'Call of Duty: World at War – drittes Mappack mit Termin'
author: gamevista
type: news
date: 2009-07-31T12:07:40+00:00
excerpt: '<p>[caption id="attachment_231" align="alignright" width=""]<img class="caption alignright size-full wp-image-231" src="http://www.gamevista.de/wp-content/uploads/2009/07/1280x1024_small.jpg" border="0" alt="Call of Duty: World at War" title="Call of Duty: World at War" align="right" width="140" height="100" />Call of Duty: World at War[/caption]<a href="http://www.callofduty.com" target="_blank" title="cod waw">Activison</a> kündigte letzte Woche das dritte Mappack für ihren WW2 Ego-Shooter<strong> Call of Duty: World at War</strong> an. Nun wurde auch ein Termin der Öffentlichkeit präsentiert. Das Mappack rund um die drei neuen Maps Breach, Battery und Revolution soll am 06. August 2009 erscheinen. </p>'
featured_image: /wp-content/uploads/2009/07/1280x1024_small.jpg

---
Außerdem wird es für den Zombie Modus eine neue Map geben die den Name „Der Riese“ trägt. Die Kosten werden je nach Plattform entweder 800 Microsoft Punkte oder 9,99 Euro betragen.







 