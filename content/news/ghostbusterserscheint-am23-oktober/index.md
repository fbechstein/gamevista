---
title: Ghostbusters – Erscheint am 23. Oktober
author: gamevista
type: news
date: 2009-07-27T15:07:01+00:00
excerpt: '[caption id="attachment_172" align="alignright" width=""]<a href="http://www.de.atari.com" target="_blank" title="atari"><img class="caption alignright size-full wp-image-172" src="http://www.gamevista.de/wp-content/uploads/2009/07/ghostbusters_small.jpg" border="0" alt="Ghostbusters - The Video Game" title="Ghostbusters - The Video Game" align="right" width="140" height="100" />Publisher Atari </a>atari[/caption]gibt in seiner aktuellen Releaseliste den Veröffentlichungstermin für <strong>Ghostbusters - The Video Game</strong> mit dem 23. Oktober an. Endlich kommen dann auch Xbox360, Wii, Nintento DS und PC Spieler in den Genuss der Geisterjagd. <br /><br />'
featured_image: /wp-content/uploads/2009/07/ghostbusters_small.jpg

---
Durch einen Exklusivdeal zwischen Sony und <a href="http://www.de.atari.com" target="_blank" title="atari">Atari</a> wurde **Ghostbusters &#8211; The Video** Game bisher nur für die Playstation 2 und Playstation 3 veröffentlicht. In Nordamerika erschienen alle Varianten gleichzeitig.









 