---
title: GTA V – In der Entwicklung
author: gamevista
type: news
date: 2009-11-16T18:21:56+00:00
excerpt: '<p>[caption id="attachment_1045" align="alignright" width=""]<img class="caption alignright size-full wp-image-1045" src="http://www.gamevista.de/wp-content/uploads/2009/11/gta4.jpg" border="0" alt="Grand Theft Auto" title="Grand Theft Auto" align="right" width="140" height="100" />Grand Theft Auto[/caption]Das Actionspiel <strong>Grand Theft Auto</strong> erhält einen weiteren Nachfolger. So bestätigte <a href="http://www.rockstargames.com" target="_blank">Rockstar Games</a> gegenüber der Tageszeitung <a href="http://technology.timesonline.co.uk/tol/news/tech_and_web/article6916029.ece" target="_blank">The Times</a>, dass sie bereits an dem nunmehr fünften Teil arbeiten. Bisher ist die Version noch in einem sehr frühen Stadium der Entwicklung. Der Gründer des Entwicklerstudios Rockstar Games, Dan Houser, sagt das sie noch in der Ideensammlung für Schauplätze und Figuren seien.</p> '
featured_image: /wp-content/uploads/2009/11/gta4.jpg

---
Auch wird noch gerätselt in welcher Stadt der fünfte Teil spielen wird. Gerüchten zufolge wird es diesmal in die englische Hauptstadt London gehen.

 

* * *



* * *