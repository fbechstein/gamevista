---
title: Left 4 Dead 2 – Wer vorbestellt zahlt weniger
author: gamevista
type: news
date: 2009-10-04T19:12:03+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Wie der Entwickler <a href="http://www.valvesoftware.com" target="_blank">Valve </a>nun offiziell bestätigte werden Vorbesteller des Multiplayer Shooters <strong>Left 4 Dead 2</strong> in bestimmten Shops das Spiel bis zu zehn Prozent günstiger erhalten. Welche Händler das genau sein werden wollte <a href="http://www.valvesoftware.com" target="_blank">Valve </a>noch nicht preisgeben.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Die Onlineplattform Steam gehört natürlich mit dazu und dort zahlen Vorbesteller statt 49,99 Euro nur 44,99 Euro. Weiterhin wird es exklusive Inhalte wie Nahkampfwaffen oder auch ein früheren Zugang zur Demo geben.

 

* * *



* * *

 