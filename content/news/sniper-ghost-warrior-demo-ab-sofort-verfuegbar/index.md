---
title: 'Sniper: Ghost Warrior – Demo ab sofort verfügbar'
author: gamevista
type: news
date: 2010-06-16T20:34:51+00:00
excerpt: '<p>[caption id="attachment_1820" align="alignright" width=""]<img class="caption alignright size-full wp-image-1820" src="http://www.gamevista.de/wp-content/uploads/2010/05/sniper_ghost_warrior.jpg" border="0" alt="Sniper: Ghost Warrior" title="Sniper: Ghost Warrior" align="right" width="140" height="100" />Sniper: Ghost Warrior[/caption]Publisher <strong>City Interactive</strong> veröffentlichte vor kurzem die Demo zum Ego-Shooter <a href="http://www.sniperghostwarrior.com/" target="_blank">Sniper: Ghost Warrior</a>. Die Testversion umfasst 1,4 Gigabyte und steht ab sofort zum Download bereit.</p> '
featured_image: /wp-content/uploads/2010/05/sniper_ghost_warrior.jpg

---
[> Zum Sniper: Ghost Warrior &#8211; Demo Download  
][1]    
**Features:**

  * Erlebe ein absolut realistisches Ballistik-System, das nicht nur die Distanz, sondern auch Umgebungsparameter, wie beispielsweise den Wind, mit einberechnet.
  * Kontrolliere deine Atmung, um die Präzision deiner Schüsse zu erhöhen.
  * Arbeite mit deinem Beobachter zusammen, um noch effizienter zu agieren.
  * Verfolge im Bullet-Cam-Modus, wie deine Kugeln mit tödlicher Präzision ihr Ziel finden.
  * Zum Spielen von Sniper: Ghost Warrior ist keine permanente Internetverbindung nötig.

   

* * *

   



 [1]: downloads/demos/item/demos/sniper-ghost-warrior-demo