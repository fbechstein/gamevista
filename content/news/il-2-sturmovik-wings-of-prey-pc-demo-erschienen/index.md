---
title: 'IL-2 Sturmovik: Wings of Prey – PC Demo erschienen'
author: gamevista
type: news
date: 2010-01-02T12:03:26+00:00
excerpt: '<p>[caption id="attachment_1253" align="alignright" width=""]<img class="caption alignright size-full wp-image-1253" src="http://www.gamevista.de/wp-content/uploads/2010/01/wop_small.png" border="0" alt="Wings of Prey" title="Wings of Prey" align="right" width="140" height="100" />Wings of Prey[/caption]Der Entwickler <a href="http://www.gaijinent.com/" target="_blank">Gaijin</a> hat endlich auch an die PC-Spieler gedacht und nachdem vor ein paar Monaten die Flugsimulation<strong> IL-2 Sturmovik: Birds of Prey</strong> bereits für Xbox 360 und PlayStation 3 erschien, steht nun die Demo für den PC bereit. Die Demo ist 1,4 Gigabyte groß und bietet zwei Tutorials und drei Missionen (Berlin, Großbritannien).</p> '
featured_image: /wp-content/uploads/2010/01/wop_small.png

---
[> Zur IL-2 Sturmovik: Wings of Prey &#8211; PC Demo][1]

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/wings-of-prey-demo