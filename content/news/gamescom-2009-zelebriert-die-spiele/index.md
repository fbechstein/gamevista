---
title: Gamescom 2009 – Zelebriert die Spiele!
author: gamevista
type: news
date: 2009-07-22T22:30:17+00:00
excerpt: '<span class="caption"><strong><br />Gamescom 2009 - Zelebriert die Spiele!</strong></span><br /><br /><p class="caption">[caption id="attachment_109" align="alignright" width=""]<img class="caption alignright size-full wp-image-109" src="http://www.gamevista.de/wp-content/uploads/2009/07/gamescom_small.png" border="0" alt="gamescom 2009" title="gamescom 2009" align="right" width="140" height="100" />gamescom 2009[/caption]Unter dem Motto findet vom <strong>20.8.2009</strong> bis zum <strong>23.08.2009</strong> die größte Spiele- und Unterhaltungsmesse Europas in Köln statt. Viele große Namen werden vertreten sein. Um euch einen Überblick zu verschaffen was ihr erwarten und anzocken könnt, liefern wir hier unsere Prognose ab. </p>'
featured_image: /wp-content/uploads/2009/07/gamescom_small.png

---
Zum ersten mal öffnet die <a href="http://www.gamescom.de" target="_blank" title="gamescom 2009">gamescom </a>ihr Tore in Köln. Dabei wird laut Veranstalter für Gamer, Händler und Aussteller eine neue Ära eingeleitet. Als Spielmesse, Games-Festival und Business-Plattform lockt sie tausende Menschen nach Köln, zum Staunen, Kontakte knüpfen, Ausprobieren und Feiern. Es werden wohl über 300 Aussteller aus der ganzen Welt präsent sein und ihre Neuerungen aus dem Bereich der interaktiven Unterhaltungselektronik vorstellen. Ihr werdet zahlreiche Computerspiele zum ersten mal antesten können. Die wichtigsten stellen wir euch nun vor. 

**Die Neuheiten**  
<img class="caption alignright size-full wp-image-110" src="http://www.gamevista.de/wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="105" />wird trotz der gleichzeitig stattfindenden <a href="http://www.blizzcon.com" target="_blank" title="Blizzcon">Blizzcon</a> auf der gamescom ausstellen, was uns natürlich sehr freut und auf eventuell spielbare Versionen von **Diablo 3** und **Starcraft 2** hoffen lässt. Auf jeden Fall wird es jede Menge neues Material zu bestaunen geben. Neben der Demo zu **Diablo 3** der vergangenen Worldwide Invitational wird es sicher auch möglich sein **Starcraft 2** anzuspielen, schließlich gibt schon laufende Techdemos.

<p class="caption">
  <em style="font-style: normal"><br />D</em>iablo 3 (Publisher: Activision Blizzard, Entwickler: Blizzard, Release: unbekannt) <br />Starcraft 2: Wings of Liberty (Publisher: Activision Blizzard, Entwickler: Blizzard, Release: 2010)
</p>



  
<figure id="attachment_111" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Call of Duty: Modern Warfare 2</figcaption></figure>Ein weiteres Highlight wird **Call of Duty: Modern Warfare 2** sein, was ja schon auf der <a href="http://www.e3expo.com/" target="_blank" title="e3">E3 2009</a> ein must have war. Es wird mit Sicherheit auf der gamescom spielbar sein. Evtl. wird auch das Nachtsichtgerät aus der Prestige Edition ausgestellt.

<p class="caption">
  <em><font style="font-style: normal" color="#000000"><br />(Publisher: Activision, Entwickler: Infinity Ward, Release: November 2009)</font></em>
</p>

 

<figure id="attachment_112" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin's Creed 2" title="Assassin's Creed 2" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Assassin&#8217;s Creed 2</figcaption></figure>In **Assassin&#8217;s Creed 2** geht es diesmal nach Italien mit neuem Helden, neuen Waffen und natürlich jeder Menge neuer Meucheltricks. Spielen wird das ganze in der der Renaissance Zeit. ua. in Venedig, Rom oder Florenz. 

<p class="caption">
  (Publisher: Activision, Entwickler: Infinity Ward, Release: November 2009)<em><font style="font-style: normal" color="#000000"></p> 
  
  <p>
    <img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000"><strong>Dragon Age: Origins</strong> wird mit Sicherheit eines der wichtigsten Rollenspiele dieses Jahres. Kein anderer als <a href="http://www.bioware.com/" target="_blank" title="bioware">Bioware </a>(Baldur´s Gate / Neverwinter Nights) entwickelt die Monsterhatz. Genug Erfahrung haben sie um damit einen Hit zu landen. </p> 
    
    <p>
      (Entwickler: Bioware Publisher: EA, Release: Oktober 2009)</font></em></font></em>
    </p>
    
    <p style="margin-bottom: 0cm" class="caption">
       
    </p>
    
    <p style="margin-bottom: 0cm" class="caption">
      <figure id="attachment_114" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-114" src="http://www.gamevista.de/wp-content/uploads/2009/07/risentier.png" border="0" alt="Risen" title="Risen" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Risen</figcaption></figure><strong>Risen:</strong> Im neuen Rollenspiel von <strong>Gothic</strong> Entwickler <a href="http://www.piranha-bytes.com/" target="_blank" title="piranha bytes">Piranha Bytes</a> kämpft der Spieler auf einer mysteriösen Vulkaninsel. Interface, Kampf- und Dialogsystem erinnern sehr an <strong>Gothic</strong>. Die Grafik ist wie immer eine Pracht und sucht ihresgleichen. <strong>Risen</strong> soll insgesamt linearer und storylastiger als <strong>Gothic</strong> werden.
    </p>
    
    <p>
      (Publisher: Deep Silver, Entwickler: Piranha Bytes, Release: Oktober 2009)
    </p>
    
    <p>
      
    </p>
    
    <p style="margin-bottom: 0cm">
      <img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" /><span class="caption"><em><font style="font-style: normal" color="#000000"><strong>FIFA 10:</strong> Jedes Jahr widmet sich <a href="http://www.electronic-arts.de/games/easports/" target="_blank" title="ea">EA Sports</a> dem runden Leder auf ein Neues. Auch dieses Jahr wartet <strong>FIFA</strong> mit vielen Verbesserungen, aufgebohrter Grafik und wie immer 1. und 2. Bundesliga Lizenz. Die <a href="http://www.gamescom.de" target="_blank" title="gamescom">gamescom </a>bietet die perfekte Möglichkeit zu entscheiden wer das Rennen des besten Fußballspieles des Jahr gewinnt. <br /></font></em><em><font style="font-style: normal" color="#000000"><br />(Publisher: EA SPORTS, Entwickler: EA SPORTS, Release: 1. Oktober)</font></em><br /><em><font style="font-style: normal" color="#000000"><br /></font></em></span>
    </p>
    
    <p style="margin-bottom: 0cm">
      <span class="caption"><em><font style="font-style: normal" color="#000000"><strong><figure id="attachment_107" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Pro Evolution Soccer 2010</figcaption></figure>Pro Evolution Soccer 2010</strong> von <a href="http://de.games.konami-europe.com/" target="_blank" title="konami">Konami </a>wird natürlich auch vertreten sein und zeigt euch dieses Jahr warum man sich gegen <strong>FIFA</strong> entscheiden sollte. Tolle Grafik, ein verbesserter Meistermodus und ein stark verbesserter Multiplayerbereich lassen die Fußballfans träumen.</font></em></span> <font color="#000000"></p> 
      
      <p>
        (Publisher: Konami, Entwickler: PES Studios, Release: Oktober 2009)</font>
      </p>
      
      <p style="margin-bottom: 0cm" class="caption">
         
      </p>
      
      <p style="margin-bottom: 0cm" class="caption">
        <em><font style="font-style: normal" color="#000000"><figure id="attachment_116" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Left 4 Dead 2</figcaption></figure>Nachdem der erste Teil schon ein Riesenerfolg war, holt <a href="http://www.valvesoftware.com/" target="_blank" title="valve">Valve</a> nun zum 2. Streich aus. Mit neuen Waffen, natürlich mehr Zombies, ist ja klar, und einem Wettersystem will Valve euch und euren Freunden düstere Nächte bereiten. <strong>Left 4 Dead 2</strong> soll eine umfangreichere Kampagne als der 1. Teil bieten. Im November könnt ihr es kaufen, im August schon mal antesten. Allerdings solltet ihr schon 18 sein. </p> 
        
        <p>
          (Publisher: Electronic Arts, Entwickler: Valve, Release: 17. November 2009)</font></em>
        </p>
        
        <p style="margin-bottom: 0cm">
           
        </p>
        
        <p style="margin-bottom: 0cm">
          <span class="caption"><em style="font-style: normal"><figure id="attachment_117" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-117" src="http://www.gamevista.de/wp-content/uploads/2009/07/arcania_32_small.jpg" border="0" alt="Arcania: A Gothic Tale" title="Arcania: A Gothic Tale" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Arcania: A Gothic Tale</figcaption></figure>Ob der neue <strong>Gothic</strong> Nachfolger </em><em><font style="font-style: normal" color="#000000"><strong>Arcania: A Gothic Tale</strong>, diesmal von </font></em><em style="font-style: normal"><a href="http://www.spellbound.de/" target="_blank" title="spellbound">Spellbound </a>entwickelt, hält was er verspricht? <strong>Gothic 3</strong>, damals noch von <a href="http://www.piranha-bytes.com/" target="_blank" title="piranha bytes">Piranha Bytes</a>, war trotz der grausamen Bugs kein schlechtes Spiel. Wenn <a href="http://www.spellbound.de/" target="_blank" title="spellbound">Spellbound </a>es schafft neue Akzente zu setzen und den Bugs in ihrem neuen Titel den Kampf ansagt kann die Community wieder neue Hoffnung schöpfen. <br /></em></span>
        </p>
        
        <p style="margin-bottom: 0cm">
          <em><font style="font-style: normal" color="#000000">(Publisher: JoWood, Entwickler: Spellbound, Release: Winter 2009)<br /></font></em>
        </p>
        
        <p>
          
          
          <br /><em style="font-style: normal"><figure id="attachment_118" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-118" src="http://www.gamevista.de/wp-content/uploads/2009/07/halo3odst_small.jpg" border="0" alt="Halo 3: ODST" title="Halo 3: ODST" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Halo 3: ODST</figcaption></figure>Das Addon </em><strong>Halo 3: ODST</strong> wird ohne das Hauptspiel auskommen und als eigenständiges Spiel nutzbar sein. Die Kampagne könnt ihr allein oder mit 3 Freunden durchleben. Laut <a href="http://www.bungie.net/" target="_blank" title="bungie">Bungie </a>wird es fünf bis sechs Stunden Spielzeit enthalten und führt euch Storytechnisch zwischen <strong>Halo 2</strong> und 3, dabei durchsucht ihr die Ruinen von New Mombasa.<em><font style="font-style: normal" color="#000000"></p> 
          
          <p>
            (Publisher: Microsoft Game Studios, Entwickler: Bungie, Release: September 2009)</font></em>
          </p>
          
          <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
            <em style="font-style: normal"><strong><br />Wer kommt nicht zur gamescom ?</strong> </em>
          </p>
          
          <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
            <em style="font-style: normal">Sie wundern sich warum <strong>Operation Flashpoint: Dragon Rising</strong> oder <strong>Colin McRae: Dirt 2</strong> fehlen? <a href="http://www.codemasters.de" target="_blank" title="codemasters">Codemasters </a>entschied sich leider dazu nicht auf der <a href="http://www.gamescom.de" target="_blank" title="gamescom">gamescom </a>2009 auszustellen. Außerdem werden wir <a href="http://www.zuxxez.com" target="_blank" title="zuxxez">Zuxxez </a>auf der <a href="http://www.gamescom.de" target="_blank" title="gamescom">gamescom </a>vermissen, damit fehlen <strong>Two Worlds 2</strong> und <strong>Section 8</strong>.</em><em><font style="font-style: normal" color="#000000"><br /></font></em>
          </p>
          
          <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
            <em><font style="font-style: normal" color="#000000"><strong><br />Weitere Games auf der gamescom</strong></font></em>
          </p>
          
          <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
            <em><font style="font-style: normal" color="#000000">Neben den Highlights</font></em><em><font style="font-style: normal" color="#000000"> gibt es noch eine Menge anderer interessanter Spiele zu sehen. Um euch einen Überblick zu verschaffen, hier eine kleine aber feine Liste:</font></em><br /><em><font style="font-style: normal" color="#000000"><br /></font></em><img class="caption alignright size-full wp-image-119" src="http://www.gamevista.de/wp-content/uploads/2009/07/ap_small.jpg" border="0" alt="Alpha Protocol" title="Alpha Protocol" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Alpha Protocol</font></em> (Publisher: Sega, Entwickler: Obsidian Entertainment, Release: September 2009) <br /><em><font style="font-style: normal" color="#000000"></p> 
            
            <p>
              </font></em><img class="caption alignright size-full wp-image-120" src="http://www.gamevista.de/wp-content/uploads/2009/07/tropico3_small.jpg" border="0" alt="Tropico 3" title="Tropico 3" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Tropico 3 </font></em>(Publisher: Kalypso, Entwickler: Heamimont Games, Release: September 2009) <br /><em><font style="font-style: normal" color="#000000"></p> 
              
              <p>
                </font></em>
              </p>
              
              <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
                 
              </p>
              
              <p>
                
              </p>
              
              <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
                <em><font style="font-style: normal" color="#000000"><figure id="attachment_121" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-121" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderlands_small.jpg" border="0" alt="Borderlands" title="Borderlands" width="140" height="100" align="right" /><figcaption class="wp-caption-text">Borderlands</figcaption></figure>Borderlands </font></em><em><font style="font-style: normal" color="#000000">(Publisher: 2K Games, Entwickler: Gearbox, Release: Oktober 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000"></p> 
                
                <p>
                  </font></em><br /><img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Champions Online </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Atari, Entwickler Cryptic Studios, Release: September 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000"></p> 
                  
                  <p>
                    </font></em><br /><img class="caption alignright size-full wp-image-123" src="http://www.gamevista.de/wp-content/uploads/2009/07/batman_arkham_asylum_small.jpg" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Batman: Arkham Asylum </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Eidos, Entwickler: Rocksteady, Release: 28. August 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000"></p> 
                    
                    <p>
                      </font></em><img class="caption alignright size-full wp-image-124" src="http://www.gamevista.de/wp-content/uploads/2009/07/shift_small.jpg" border="0" alt="Need for Speed: Shift" title="Need for Speed: Shift" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Need for Speed: Shift </font></em><em><font style="font-style: normal" color="#000000">(Publisher: EA, Entwickler: Slightly Mad Studios, Release: September 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000"></p> 
                      
                      <p>
                        </font></em>
                      </p>
                      
                      <p>
                        
                      </p>
                      
                      <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
                        <em><font style="font-style: normal" color="#000000"><figure id="attachment_125" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-125" src="http://www.gamevista.de/wp-content/uploads/2009/07/nhl10_small.jpg" border="0" alt="NHL 10" title="NHL 10" width="140" height="100" align="right" /><figcaption class="wp-caption-text">NHL 10</figcaption></figure>NHL 10 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: EA SPORTS, Entwickler: EA SPORTS, Release: 17. September)</font></em> <br /><em><font style="font-style: normal" color="#000000"></p> 
                        
                        <p>
                          </font></em><img class="caption alignright size-full wp-image-126" src="http://www.gamevista.de/wp-content/uploads/2009/07/nfl10_small.jpg" border="0" alt="Madden NFL 10" title="Madden NFL 10" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">NFL 10 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: EA SPORTS, Entwickler: EA SPORTS, Release: TBA) <br /></font></em><em><font style="font-style: normal" color="#000000"></p> 
                          
                          <p>
                            </font></em><img class="caption alignright size-full wp-image-127" src="http://www.gamevista.de/wp-content/uploads/2009/07/just_cause2_small.jpg" border="0" alt="Juse Cause 2" title="Just Cause 2" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Just Cause 2 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Eidos, Entwickler: Avalanche Studios, Release: 2010) <br /></font></em><em><font style="font-style: normal" color="#000000"></p> 
                            
                            <p>
                              </font></em><img class="caption alignright size-full wp-image-128" src="http://www.gamevista.de/wp-content/uploads/2009/07/mini_ninjas_small.jpg" border="0" alt="Mini Ninjas" title="Mini Ninjas" align="right" width="140" height="100" /><em><font style="font-style: normal" color="#000000">Mini Ninjas </font></em><font color="#008000"><font color="#000000">(Publisher: Eidos, Entwickler: IO Interactive, Release: Herbst 2009) <br /></font></font><em><font style="font-style: normal" color="#000000"></p> 
                              
                              <p>
                                </font></em>
                              </p>
                              
                              <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
                                 
                              </p>
                              
                              <p>
                                <em><font style="font-style: normal" color="#000000"></p> 
                                
                                <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
                                  <strong>Weitere Spiele im Überblick</strong> 
                                </p>
                                
                                <p>
                                  </font></em>
                                </p>
                                
                                <p style="background: transparent none repeat scroll 0% 0%; margin-bottom: 0cm; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
                                  <em><font style="font-style: normal" color="#000000">Lost Planet 2 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Capcom, Entwickler: Capcom, Release: Winter 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Street Fighter 4 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Capcom, Entwickler: Capcom, Release: erschienen)</font></em> <br /><em><font style="font-style: normal" color="#000000">Splinter Cell: Conviction </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Ubisoft, Entwickler: Ubisoft Montreal, Release: 4. Q. 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Golden Sun DS </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Camelot, Release: 4. Q. 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">The Legend of Zelda: Spirit Tracks </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Nintendo, Release: 4. Q. 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">New Super Mario Bros. Wii </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Nintendo, Release: 2010)</font></em> <br /><em><font style="font-style: normal" color="#000000">Wii Fit Plus </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Nintendo, Release: Herbst 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Endless Ocean 2 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Arika, Release: 4. Q. 2009) <br /></font></em><em><font style="font-style: normal" color="#000000">Donkey Kong: Jungle Beat NPC </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Nintendo, Release: 5. Juni 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Rabbids Go Home </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Ubisoft, Entwickler: Ubisoft Frankreich, Release: Winter 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Red Steel 2 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Ubisoft, Entwickler: Ubisoft Frankreich, Release: Winter 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">The Conduit </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Sega, Entwickler: High Voltage, Release: 10. Juli 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Wii Sports Resort </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Nintendo, Release: 24. Juli 2009) <br /></font></em><em><font style="font-style: normal" color="#000000">EA SPORTS Grand Slam Tennis </font></em><em><font style="font-style: normal" color="#000000">(Publisher: EA SPORTS, Entwickler: EA SPORTS, Release: 10. Juni 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Virtua Tennis 2009 </font></em><em><font style="font-style: normal" color="#000000">(Publisher: Sega, Entwickler: Sega, Release: 29. Mai 2009)</font></em> <br /><em><font style="font-style: normal" color="#000000">Excitebots: Trick Racing</font></em><font color="#008000"><em><font style="font-style: normal" color="#000000"> </font></em></font><em><font style="font-style: normal" color="#000000">(Publisher: Nintendo, Entwickler: Monster Games, Release: erhältlich)</font></em> <br /><font color="#008000"><em><font style="font-style: normal" color="#000000">Shadow Complex </font></em></font><font color="#008000"><font color="#000000">(Publisher: Epic Games, Entwickler: Chair Entertainment, Release: Herbst 2009)</font></font> <br /><font color="#008000"><em><font style="font-style: normal" color="#000000">Lips: Number One Hits </font></em></font><em><font style="font-style: normal" color="#000000">(Publisher: Microsoft Game Studios, Entwickler: Microsoft, Release: Herbst 2009)</font></em> <br /><font color="#008000"><em><font style="font-style: normal" color="#000000">LEGO Rock Band </font></em></font><em><font style="font-style: normal" color="#000000">(Publisher: Warner Bros., Entwickler: TT Games, Release: 2009)</font></em> <br /><font color="#008000"><em><font style="font-style: normal" color="#000000">Guitar Hero 5</font></em></font> <em><font style="font-style: normal" color="#000000">(Publisher: Activision, Entwickler: Neversoft, Release: September)</font></em> <br /><font color="#008000"><em><font style="font-style: normal" color="#000000">Tekken 6 </font></em></font><em><font style="font-style: normal" color="#000000">(Publisher: Atari, Entwickler: Namco Bandai, Release: Winter 2009)</font></em>
                                </p>
                                
                                <p>
                                  <strong>Fazit</strong>
                                </p>
                                
                                <p>
                                  Mit Sicherheit werden noch einige Perlen auf der gamescom enthüllt werden. Man darf gespannt sein, was die einzelnen Publisher sonst noch zeigen werden. Wir sind mit dem gesamten Team vor Ort und werden Euch von den Neuigkeiten täglich und aktuell Berichten.
                                </p>
                                
                                
                                
                                <p>
                                  
                                </p>
                                
                                