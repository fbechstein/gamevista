---
title: 'Call of Duty: Modern Warfare – Game of the Year Edition 40% günstiger'
author: gamevista
type: news
date: 2009-10-30T16:25:20+00:00
excerpt: '<p>[caption id="attachment_971" align="alignright" width=""]<img class="caption alignright size-full wp-image-971" src="http://www.gamevista.de/wp-content/uploads/2009/10/codmw1_small.png" border="0" alt="Call of Duty: Modern Warfare" title="Call of Duty: Modern Warfare" align="right" width="140" height="100" />Call of Duty: Modern Warfare[/caption]Der Onlinedienst <a href="http://store.steampowered.com/app/7940/">Steam</a> bietet Shooter-Fans dieses Wochenende das Spiel <strong>Call of Duty: Modern Warfare</strong> um 40 Prozent günstiger an. So wird die <strong>Game of the Year Edition</strong> bis einschließlich Sonntag nur noch für 29,99 Euro angeboten. Grund dafür dürfte die Fertigstellung des zweiten Teils <strong>Call of Duty: Modern Warfare 2</strong> sein. Dieser wird am 10. November 2009 im Handel erscheinen.</p> '
featured_image: /wp-content/uploads/2009/10/codmw1_small.png

---
Wer also den ersten Teil noch nicht kennt sollte hier zugreifen, es lohnt sich.

 

* * *



* * *

 