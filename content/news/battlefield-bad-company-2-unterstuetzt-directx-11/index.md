---
title: 'Battlefield: Bad Company 2 – Unterstützt DirectX 11'
author: gamevista
type: news
date: 2009-11-15T17:49:15+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Der Hersteller AMD hat eine <a href="http://techreport.com/discussions.x/17963" target="_blank">Liste</a> veröffentlicht auf der Spiele aufgeführt sind die DirectX 11 unterstützen werden. Darunter ist auch der neuste Teil <strong>Battlefield: Bad Company 2</strong>. Vorraussetzung wird dafür das Betriebssystem Windows 7 von Microsoft sein. Zwar werden Nutzer anderer Betriebssysteme auch in den Genuss von<strong> Battlefield: Bad Company 2</strong> kommen können, allerdings ohne die Grafikvorzüge von DirectX 11.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Dies ist das nächste exklusive Feature für die PC Version des Shooters. Vor kurzem wurde vom Entwickler <a href="http://www.badcompany2.ea.com/" target="_blank">DICE</a> bereits angekündigt, dass die PC-Version von **Battlefield: Bad Company 2** dedizierte Server mit hoher Spielerzahl unterstützt.

 

* * *



* * *