---
title: 'Arcania: Gothic 4 – Systemvoraussetzungen bekannt'
author: gamevista
type: news
date: 2010-07-21T18:19:34+00:00
excerpt: '<p>[caption id="attachment_2040" align="alignright" width=""]<img class="caption alignright size-full wp-image-2040" src="http://www.gamevista.de/wp-content/uploads/2010/07/burg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Die Entwickler von <strong>Spellbound Entertainment</strong> haben die Systemvoraussetzungen für ihr Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> veröffentlicht. Der nächste Gothic-Titel wird im Oktober 2010 für Xbox 360, PlayStation und PC veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/07/burg_small.png

---
Nach einer Newsmeldung auf der Onlineplattform über die Systemvoraussetzungen kann davon ausgegangen werden das <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> zusätzlich auch dort vertrieben wird. Neben einem Dual Core-Prozessor wird auch eine halbwegs aktuelle Grafikkarte vorausgesetzt.

 

#### Minimale Anforderungen:

  * Windows XP/Vista/7
  * Dual Core-Prozessor (3500+)
  * 1 GByte Arbeitsspeicher (1,5 GByte bei Windows Vista/7)
  * Grafikkarte der Klasse 8600 GTS, Radeon X1900 
  * 10 GByte freier Festplattenplatz
  * DirectX 8.1

#### Empfohlene Anforderungen:

  * Windows XP/Vista/7
  * Dual Core-Prozessor
  * 3 GByte Arbeitsspeicher 
  * Grafikkarte der Klasse GeForce 9800, Radeon HD 4800
  * 10 GByte freier Festplattenplatz
  * DirectX 9.0c

   

* * *

   

