---
title: Diablo 3 und Starcraft 2 auf der gamescom?
author: gamevista
type: news
date: 2009-07-21T11:49:17+00:00
excerpt: '<p>[caption id="attachment_89" align="alignright" width=""]<img class="caption alignright size-full wp-image-89" src="http://www.gamevista.de/wp-content/uploads/2009/07/logo_blizzard.png" border="0" alt="Blizzard präsentiert neue Games auf der gamescom in Köln" title="Blizzard präsentiert neue Games auf der gamescom in Köln" align="right" width="140" height="100" />Blizzard präsentiert neue Games auf der gamescom in Köln[/caption]<a href="http://eu.blizzard.com/de/" target="_blank">Blizzard</a> teilte im offizielen Forum mit, dass auf der Spielemesse gamescom in Köln (20-23. August 2009) Neuheiten präsentiert werden:</p><p>"Es freut uns bekanntzugeben, dass Blizzard Entertainment auf der <strong>gamescom in Köln</strong> im nächsten Monat vertreten sein wird. Wir sind gespannt darauf, alte Bekannte und neue Freunde an unserem Stand begrüßen zu dürfen, die auch gleich unsere nächsten Spiele anspielen können. Unser Stand ist ab <em>Donnerstag, den 20. August, bis zum Sonntag, den 23. August</em>, öffentlich zugänglich."</p>'
featured_image: /wp-content/uploads/2009/07/logo_blizzard.png

---
Man kann voller Hoffnung sein, dass es sich dabei um die heißbegehrten Games **Diablo 3** und **Starcraft 2** handelt. Wir sind auf jeden Fall vor Ort und werden berichten. 





