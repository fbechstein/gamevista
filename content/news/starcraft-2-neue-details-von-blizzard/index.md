---
title: Starcraft 2 – Neue Details von Blizzard
author: gamevista
type: news
date: 2009-10-05T16:24:20+00:00
excerpt: '<p>[caption id="attachment_110" align="alignright" width=""]<img class="caption alignright size-full wp-image-110" src="http://www.gamevista.de/wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="105" />Starcraft 2[/caption]Entwickler <a href="http://www.blizzard.de">Blizzard Entertainment</a> hat vor kurzem im offiziellen <a href="http://forums.battle.net/thread.html?topicId=20143778681&sid=3000" target="_blank">Battlenet Forum</a> weitere Details zum Strategiekracher <strong>Starcraft 2</strong> veröffentlicht.  Zum einen wurde die Anzahl der Synchronsprecher genannt. Hier werden ganze 58 Mitarbeiter für die Sprachaufnahmen zuständig sein um den Charakteren ihr Leben einzuhauchen.</p> '
featured_image: /wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg

---
Weiterhin konkretisiert <a href="http://www.blizzard.de" target="_blank">Blizzard </a>die Idee dem Spiel eine Art „Post Gamelobby“ zur Verfügung zu stellen. Diese Art von Lobby wird Spielern die Möglichkeit geben, Replays gemeinsam anzuschauen oder zurückliegende Matches zu analysieren.

 

 

* * *



* * *

 