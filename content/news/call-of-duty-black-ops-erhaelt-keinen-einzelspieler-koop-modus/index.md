---
title: 'Call of Duty: Black Ops – Erhält keinen Einzelspieler-Koop-Modus'
author: gamevista
type: news
date: 2010-09-13T17:58:05+00:00
excerpt: '<p>[caption id="attachment_1747" align="alignright" width=""]<img class="caption alignright size-full wp-image-1747" src="http://www.gamevista.de/wp-content/uploads/2010/05/callofduty7.jpg" border="0" alt="Call of Duty: Black Ops" title="Call of Duty: Black Ops" align="right" width="140" height="100" />Call of Duty: Black Ops[/caption]Das Entwicklerteam von <strong>Treyarch </strong>hat offiziell bestätigt das die Einzelspieler-Kampagne von <a href="http://www.callofduty.com" target="_blank">Call of Duty: Black Ops</a> keinen Koop-Modus beinhalten wird. Stattdessen plant <strong>Treyarch </strong>eine lokale Variante mit einem Split-Screen Combat Training und Split-Screen Mehrspieler-Modus. <a href="http://www.callofduty.com" target="_blank">Call of Duty: Black Ops</a> wird am 9. November auf PS3, 360, Wii, PC und DS veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/05/callofduty7.jpg

---
Weitere Details zum Split-Screen-Modus wurden leider nicht verraten. Laut Community Boss Josh Olin wird es aber noch eine weitere Ankündigung zum Thema Koop geben.

 

   

* * *

   

