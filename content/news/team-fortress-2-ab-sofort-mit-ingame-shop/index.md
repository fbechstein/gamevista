---
title: Team Fortress 2 – Ab sofort mit Ingame-Shop
author: gamevista
type: news
date: 2010-10-01T17:04:55+00:00
excerpt: '<p><strong>[caption id="attachment_368" align="alignright" width=""]<img class="caption alignright size-full wp-image-368" src="http://www.gamevista.de/wp-content/uploads/2009/08/tf2.jpg" border="0" alt="Team Fortress 2" title="Team Fortress 2" align="right" width="140" height="100" />Team Fortress 2[/caption]Valve </strong>hat im Rahmen seines neusten Updates für den Ego-Shooter <a href="http://www.teamfortress.com" target="_blank">Team Fortress 2</a> einen Ingame-Shop implementiert. Dort können Spieler ab sofort Ausrüstungsgegenstände die sonst nur im Spiel verfügbar waren auch extern erwerben. Der Patch mit dem Namen MANN-Conomy-Update wird automatisch beim Start von Steam heruntergeladen und installiert.</p> '
featured_image: /wp-content/uploads/2009/08/tf2.jpg

---
Das MANN-Conomy-Update ermöglicht dem Spieler außerdem seine Gegenstände selbst zu individualisieren und zu verkaufen. Bisher gibt es aber nur wenige solcher Items, was sich in Zukunft durchaus aber ändern soll. Weiter Informationen, sowie ein ausführliches FAQ findet ihr auf der <a href="http://www.teamfortress.com/mannconomy/FAQ/" target="_blank">offiziellen Website</a> von <a href="http://www.teamfortress.com/mannconomy/FAQ/" target="_blank">Team Fortress 2</a>.<a href="http://www.teamfortress.com/" target="_blank"></a>

   

* * *

   

