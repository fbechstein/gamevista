---
title: Spiel-Stream-Service „OnLive“ startet Beta
author: gamevista
type: news
date: 2009-09-03T16:19:47+00:00
excerpt: '<p>[caption id="attachment_593" align="alignright" width=""]<img class="caption alignright size-full wp-image-593" src="http://www.gamevista.de/wp-content/uploads/2009/09/onlive.jpg" border="0" alt="OnLive" title="OnLive" align="right" width="140" height="100" />OnLive[/caption]Wie kann man neue Spiele auf alten Rechner spielen? Klar, indem man sie aufrüstet. Doch in 2010 soll es noch eine andere Antwort auf diese Frage geben. Bald startet eine geschlossene Beta von „OnLive“ in den USA.</p> <p>Statt den PC schon wieder aufzurüsten, soll man bei „OnLive“ die Spiele einfach streamen können. Diese werden nur nach hause übertragen, die eigentliche Arbeit leistet ein Rechenzentrum irgendwo auf der Welt. So soll man mit alten PCs und Notebooks spielen können – die notwendige (schnelle) Internetverbindung vorausgesetzt.</p> '
featured_image: /wp-content/uploads/2009/09/onlive.jpg

---
Es gibt zwei Firmen, die derzeit an so etwas arbeiten, OnLive und Gaikai. Ersterer startet nun eine geschlossene Beta in den USA. Wir können also gespannt sein über die Reaktionen der Amerikaner – interessant klingt das Prinzip auf jeden Fall! Wenn ihr einen Blick auf die offizielle Website werfen wollt, klickt ><a href="http://www.onlive.com" target="_top">hier</a><. Dort findet ihr auch einen ersten Trailer, der das Prinzip näher vorstellt.

* * *



* * *