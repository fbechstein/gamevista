---
title: Aion – Aion Open Beta startet morgen + Open Beta Client Download
author: gamevista
type: news
date: 2009-09-05T09:54:44+00:00
excerpt: '<p>[caption id="attachment_224" align="alignright" width=""]<img class="caption alignright size-full wp-image-224" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion" title="Aion" align="right" width="140" height="100" />Aion[/caption]Bald ist es soweit. Noch einen Tag warten und dann könnt ihr gemeinsam die Welt von <strong>Aion</strong> antesten. Zwar ist es eine Open Beta aber ihr benötigt trotzdem einen Beta Key um euch kurz vor Veröffentlichung des Online-Rollenspiel Hits noch mal einen letzten Eindruck holen zu können. Die <strong>Aion</strong> Open Beta läuft genau eine Woche, vom 6. September bis zum 13. September.</p> '
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
Der Startschuss fällt Sonntag 18.00 Uhr. Ab da könnt ihr euch dann einloggen. Um spielen zu können braucht ihr auch noch den Client. Diesen stellen wir euch gerne zur Verfügung. Weiterhin benötigt ihr noch den aktuellsten Patch. Diesen findet ihr natürlich auch bei uns im Downloadbereich.  
[  
> Zum Open Beta Client Download][1]  
[  
> Zum Open Beta Update Download][2]

* * *



* * *

 

 

 [1]: downloads/demos/item/demos/aion-open-beta-client-v1501
 [2]: downloads/patches/item/patches/aion-open-beta-client-update-v1501