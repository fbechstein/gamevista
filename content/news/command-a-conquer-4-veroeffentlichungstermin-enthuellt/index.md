---
title: 'Command & Conquer 4 – Veröffentlichungstermin enthüllt'
author: gamevista
type: news
date: 2009-11-12T19:16:28+00:00
excerpt: '<p>[caption id="attachment_1033" align="alignright" width=""]<img class="caption alignright size-full wp-image-1033" src="http://www.gamevista.de/wp-content/uploads/2009/11/cc4_small.png" border="0" alt="Command & Conquer 4" title="Command & Conquer 4" align="right" width="140" height="100" />Command & Conquer 4[/caption]Der Publisher <a href="http://www.commandandconquer.com" target="_blank">Electronic Arts</a> hat heute im Rahmen einer offiziellen Pressemitteilung den Veröffentlichungstermin für den nunmehr vierten Teil von <strong>Command & Conquer</strong> benannt. So wird das Strategiespiel aus dem Hause EA demnach am <strong>18. März 2010</strong> den Kampf zwischen der Bruderschaft von NOD und der GDI fortführen.</p> '
featured_image: /wp-content/uploads/2009/11/cc4_small.png

---
_„Endlich werden die Fans erleben, wie sich die große Geschichte um Kane, NOD, GDI, die Erde und Tiberium auflöst“_, so Mike Glosecki, Producer von C&C4. _„Wir können es kaum erwarten, wie die Fans auf die neue strategische Tiefe im Spiel reagieren werden, insbesondere wenn es um das neue Erfahrungssystem geht.“_ Vorbestellen könnt ihr das Spiel bei GameStop oder dem EA Store und bei weiteren Händlern. Fans dürfen sich dann über einen sofortigen Zugang zur Beta-Phase des Spiels freuen. Außerdem wird ein offizieller Soundtrack und eine Bonus Mission für **Command & Conquer 4: Tiberian Twilight** zur Verfügung gestellt.

 

* * *



* * *