---
title: Metro 2033 – Demo endlich erschienen
author: gamevista
type: news
date: 2010-08-25T16:13:30+00:00
excerpt: '<p>[caption id="attachment_1346" align="alignright" width=""]<img class="caption alignright size-full wp-image-1346" src="http://www.gamevista.de/wp-content/uploads/2010/01/metro_2033.jpg" border="0" alt="Metro 2033" title="Metro 2033" align="right" width="140" height="100" />Metro 2033[/caption]Mitte März 2010 wurde der postapokalyptische Ego-Shooter <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a> veröffentlicht. Das ukrainische Entwicklerteam <strong>4A Games</strong> teilte vor dem Release des Spiels mit, dass es eine Demoversion geben würde. Nun ist es endlich soweit und interessierte Fans die sich noch nicht zum Kauf hinreißen lassen konnten, können nun in der Demoversion ihre ersten Schritte wagen.</p> '
featured_image: /wp-content/uploads/2010/01/metro_2033.jpg

---
Die 3 Gigabyte große Demo steht ab sofort zum Download bereit.

[> Zur Metro 2033 &#8211; Demo][1]

   

* * *

   



 

 [1]: downloads/demos/item/demos/metro-2033-demo