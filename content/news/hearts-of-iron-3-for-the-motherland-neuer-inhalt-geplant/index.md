---
title: 'Hearts of Iron 3: For the Motherland – Neuer Inhalt geplant'
author: gamevista
type: news
date: 2011-01-25T20:20:26+00:00
excerpt: '<p>[caption id="attachment_178" align="alignright" width=""]<img class="caption alignright size-full wp-image-178" src="http://www.gamevista.de/wp-content/uploads/2009/07/heartsofiron3_small.jpg" border="0" alt="Hearts of Iron 3" title="Hearts of Iron 3" align="right" width="140" height="100" />Hearts of Iron 3[/caption]</p> <p>Entwickler<strong> Paradox Interactive </strong>plant ein neues Addon für das Strategiespiel <a href="http://www.paradox-games.de/hoi3/index.php" target="_blank">Hearts of Iron 3</a>. Derzeit befinde sich der Zusatzinhalt mit dem Namen <strong>For The Motherland</strong> in der Entwicklung, jedoch gibt es noch keine genaueren Informationen.</p> '
featured_image: /wp-content/uploads/2009/07/heartsofiron3_small.jpg

---
Erste Details sind zwar rar gesät, jedoch wird es möglich sein mit dem neuen Addon eine Partie des Spiels innerhalb weniger Stunden abzuschließen. Ein genaues Veröffentlichungsdatum liegt derzeit nicht vor. Weitere Neuerungen im Überblick:

  * neues Partisanensystem
  * Kriegsziele
  * Schlachtszenarios
  * eine große Anzahl neuer historischer Entscheidungen
  * tieferes Politiksystem
  * strategische Ressourcen
  * Kommunikation zwischen den Einsatzgebieten (&#8222;Theatres&#8220;)

* * *



* * *