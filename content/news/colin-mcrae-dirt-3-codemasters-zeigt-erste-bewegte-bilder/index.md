---
title: 'Colin McRae: DiRT 3 – Codemasters zeigt erste bewegte Bilder'
author: gamevista
type: news
date: 2010-08-03T17:37:09+00:00
excerpt: '<p>[caption id="attachment_2088" align="alignright" width=""]<img class="caption alignright size-full wp-image-2088" src="http://www.gamevista.de/wp-content/uploads/2010/08/dirt3.jpg" border="0" alt="Colin McRae: DiRT 3" title="Colin McRae: DiRT 3" align="right" width="140" height="100" />Colin McRae: DiRT 3[/caption]Der Publisher <strong>Codemasters </strong>präsentierte heute die ersten bewegten Bilder des dritten Teils der Colin McRae: DiRT-Reihe. <a href="http://www.codemasters.com/dirt/" target="_blank">Colin McRae: DiRT 3</a> wird voraussichtlich nächstes Jahr erscheinen und mehr als 50 lizensierte Rallyewagen aus fünf Jahrzehnten bieten.</p> '
featured_image: /wp-content/uploads/2010/08/dirt3.jpg

---
Dabei könnt ihr euch auf weit mehr Rennstrecken als beim Vorgänger freuen.

[> Zum Colin McRae: DiRT 3 &#8211; Teaser-Trailer][1]

   

* * *

   



 [1]: videos/item/root/colin-mcrae-dirt-3-teaser-trailer