---
title: 'Napoleon: Total War – SEGA schickt neuen DLC ins Rennen'
author: gamevista
type: news
date: 2010-06-23T15:54:55+00:00
excerpt: '<p><strong>[caption id="attachment_431" align="alignright" width=""]<img class="caption alignright size-full wp-image-431" src="http://www.gamevista.de/wp-content/uploads/2009/08/napoleon_totalwar.jpg" border="0" alt="Napoleon: Total War" title="Napoleon: Total War" align="right" width="140" height="100" />Napoleon: Total War[/caption]SEGA </strong>hat heute das Erweiterungspaket <strong>The Peninsular Campaign</strong> für das Strategiespiel <a href="http://www.totalwar.com/napoleon" target="_blank">Napoleon: Total War</a> veröffentlicht. Ab sofort könnt ihr über die Onlineplattform Steam für einen kleinen Obolus von 6,99 Euro den Download-Content herunterladen.</p> '
featured_image: /wp-content/uploads/2009/08/napoleon_totalwar.jpg

---
**The Peninsular Campaign** bietet neben einer neuen Kampagne auch die Option auf Seiten der Briten, Spanier oder Franzosen zu spielen.  Außerdem wird es neue Mehrspieler-Features geben und neue Agenten für die Völker.

 

   

* * *

   

