---
title: NBA 2K11 – Demo im Anmarsch
author: gamevista
type: news
date: 2010-09-12T15:48:11+00:00
excerpt: '<p>[caption id="attachment_2225" align="alignright" width=""]<img class="caption alignright size-full wp-image-2225" src="http://www.gamevista.de/wp-content/uploads/2010/09/nba2k11.jpg" border="0" alt="NBA 2K11" title="NBA 2K11" align="right" width="140" height="100" />NBA 2K11[/caption]Entwickler <strong>2K Games</strong> gab vor kurzem den Termin für die Demoversion des Basketballspiels <a href="http://2ksports.com/games/nba2k11" target="_blank">NBA 2K11</a> bekannt. Demnach wird im Laufe der nächsten Woche die anspielbare Demo im PlayStation Store und Live Marktplatz erscheinen. Die Probefassung wird ein fünfminütiges Viertel auf dem Schwierigkeitsgrad Pro bieten.</p> '
featured_image: /wp-content/uploads/2010/09/nba2k11.jpg

---
Als Teams werden die Boston Celtics und die Los Angeles Lakers zur Verfügung stehen.

In Nord Amerika erscheint der Titel am 5. Oktober 2010 für PC, PS2, PS3, PSP, Wii und Xbox 360. Der Veröffentlichungstermin in Deutschland ist auf den 8. Oktober 2010 gelegt.

   

* * *

   

