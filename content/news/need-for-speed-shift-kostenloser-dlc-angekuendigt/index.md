---
title: 'Need for Speed: Shift – Kostenloser DLC angekündigt'
author: gamevista
type: news
date: 2009-11-21T12:45:23+00:00
excerpt: '<p>[caption id="attachment_124" align="alignright" width=""]<img class="caption alignright size-full wp-image-124" src="http://www.gamevista.de/wp-content/uploads/2009/07/shift_small.jpg" border="0" alt="Need for Speed: Shift" title="Need for Speed: Shift" align="right" width="140" height="100" />Need for Speed: Shift[/caption]Der Publisher <a href="http://www.shift.needforspeed.com/de" target="_blank">Electronic Arts</a> hat im Rahmen einer offiziellen Pressemitteilung verkündet, dass ein neuer Downloadinhalt kostenlos für Xbox 360 und PlayStation 3 veröffentlicht wird. Demnach wird im Team Racing Pack für das Rennspiel <strong>Need for Speed: Shift</strong> fünf neue Fahrzeuge und ein komplett neuer Rennmodus enthalten sein.</p> '
featured_image: /wp-content/uploads/2009/07/shift_small.jpg

---
Der Zusatzinhalt wird am 1. Dezember 2009 auf dem Xbox Live Marktplatz, sowie am 3. Dezember 2009 im PlayStation 3 Network Store veröffentlicht. Im neuen Online-Rennmodus Team Racing könnt ihr in zwei Teams von jeweils bis zu sechs Spielern gegeneinander antreten. So wird die Grenze von der maximalen Anzahl an teilnehmenden Spielern von acht auf zwölf erhöht.

<span style="text-decoration: underline;"><strong>Hier die Liste der neuen Wagen:</strong></span>

  * 1967 Chevrolet Corvette Stingray 427
  * 1967 Shelby GT-500
  * 1969 Dodge Charger R/T
  * 1971 Dodge Challenger R/T
  * 1998 Toyota Supra Mark IV







 </p>