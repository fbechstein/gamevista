---
title: 'Dawn of War 2: Der letzte Widerstand – Doch erst im Oktober'
author: gamevista
type: news
date: 2009-08-30T15:44:43+00:00
excerpt: '<p>[caption id="attachment_552" align="alignright" width=""]<img class="caption alignright size-full wp-image-552" src="http://www.gamevista.de/wp-content/uploads/2009/08/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]So schnell kann’s gehen. Erst am Freitag wurde das neue Update für das Strategiespiel <strong>Dawn of War 2</strong> bekannt gegeben. Mit dem Namen „<strong>Der letzte Widerstand</strong>“ wurde der Patch von Entwickler <a href="http://www.relic.com">Relic </a>mit dem Veröffentlichungstermin Ende September angegeben. Nun müssen sich Fans doch etwas länger Gedulden, denn wie auf der Website von Steam zu lesen ist wird das Multiplayerupdate erst im Oktober 2009 erscheinen.</p> '
featured_image: /wp-content/uploads/2009/08/1_1280x800_small.jpg

---
Warum der Patch verschoben wurde ist bisher nicht bekannt.

 

* * *



* * *

 