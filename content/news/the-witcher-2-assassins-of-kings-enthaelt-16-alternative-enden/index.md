---
title: 'The Witcher 2: Assassins of Kings – Enthält 16 alternative Enden'
author: gamevista
type: news
date: 2010-09-01T17:30:22+00:00
excerpt: '<p>[caption id="attachment_2191" align="alignright" width=""]<img class="caption alignright size-full wp-image-2191" src="http://www.gamevista.de/wp-content/uploads/2010/09/thewitcher2.jpg" border="0" alt="The Witcher 2: Assassins of Kings" title="The Witcher 2: Assassins of Kings" align="right" width="140" height="100" />The Witcher 2: Assassins of Kings[/caption]Entwickler <strong>CD Projekt</strong> hat weitere Details zum Rollenspiel <a href="http://www.thewitcher.com/" target="_blank">The Witcher 2: Assassins of Kings</a> bekannt gegeben. Der zweite Teil wird weit mehr alternative Enden bieten als sein Vorgänger. Mit ganzen 16 Varianten dürfte damit The <a href="http://www.thewitcher.com" target="_blank">Witcher 2</a> einen neuen Rekord aufstellen.</p> '
featured_image: /wp-content/uploads/2010/09/thewitcher2.jpg

---
Weitere Informationen nannte das Entwicklerteam auch noch. Neben den 16 verschiedenen Enden gibt es noch drei unterschiedliche Anfänge, 256 Cut-Scenes, über 30 verschiedene Rüstungs-Typen. Verglichen mit dem ersten Teil werden leider nur vier Ladebildschirme geboten. Teil 1 enthielt noch knapp 700 Variationen. <a href="http://www.thewitcher.com" target="_blank">The Witcher 2: Assassins of Kings</a> wird voraussichtlich im Frühjahr 2011 für PC veröffentlicht.

   

* * *

   

