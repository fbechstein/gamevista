---
title: Tropico 3 – Bei Steam günstiger
author: gamevista
type: news
date: 2010-05-15T11:52:58+00:00
excerpt: '<p>[caption id="attachment_1804" align="alignright" width=""]<img class="caption alignright size-full wp-image-1804" src="http://www.gamevista.de/wp-content/uploads/2010/05/smallt4.jpg" border="0" alt="Tropico 3" title="Tropico 3" align="right" width="140" height="100" />Tropico 3[/caption]Die Onlineplattform <strong>Steam </strong>bietet für eine Woche das Strategie- und Aufbauspiel <a href="http://www.tropico3.com" target="_blank">Tropico 3</a> zum Schnäppchenpreis an. Wer schon immer mal seine eigene kleine Insel im Stil eines Diktators bzw. El Presidente führen wollte, hat nun die Möglichkeit <a href="http://www.tropico3.com" target="_blank">Tropico 3</a> um 75% günstiger zu erstehen.</p> '
featured_image: /wp-content/uploads/2010/05/smallt4.jpg

---
So kostet das Spiel derzeit gerade einmal 7,50 Euro. Das Angebot gilt eine Woche lang.

[> Zum Tropico 3 &#8211; Steam Angebot][1]

**Features:**

  * Umfangreiche Kampagne mit 15 Missionen 
  * Verschiedene Wirtschaftssektoren: Tourismus, Ölindustrie, Bergbau, Landwirtschaft, etc. 
  * Timeline-Editor zur Erstellung eigener historischer oder fiktiver Ereignisse 
  * Avatar-Funktion: Bewege Dich als El Presidente über die Insel und beeinflusse den Spielverlauf
  * Politische Reden, Edikte und „andere“ Einflussmöglichkeiten
  * Vielfältige Editor- und Modifizierungsfunktionen 
  * Missionsgenerator zur zufälligen Erstellung von Karten 
  * Die cineastische, detailreiche Grafik spiegelt perfekt das karibische Lebensgefühl in Tropico wieder. 
  * Toller Latin-Soundtrack
  * Verschiedene Online-Funktionen wie Highscore oder der Besuch von fremden Inseln anderer Spieler 

   

* * *

   



 [1]: http://store.steampowered.com/app/23490/