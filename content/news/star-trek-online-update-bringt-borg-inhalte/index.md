---
title: Star Trek Online – Update bringt Borg-Inhalte
author: gamevista
type: news
date: 2010-02-13T19:29:28+00:00
excerpt: '<p>[caption id="attachment_1445" align="alignright" width=""]<img class="caption alignright size-full wp-image-1445" src="http://www.gamevista.de/wp-content/uploads/2010/02/smallkonferenz.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Der Entwickler <strong>Cryptic Studios</strong> hat ein neues Update für das Online-Rollenspiel <a href="http://www.startrekonline.com/upcoming_content" target="_blank">Star Trek Online</a> veröffentlicht. Dabei handelt es sich um einen Inhaltspatch der sich um die Borg Geschichte dreht. Ab der Charakterstufe 43 werden neue Missionen bereitgestellt, die die Geschehnisse zwischen den Star Trek Filmen Nemesis und der aktuellen <a href="http://www.startrekonline.com/upcoming_content" target="_blank">Star Trek Online</a> Sternenzeit 2409 aufklären.</p> '
featured_image: /wp-content/uploads/2010/02/smallkonferenz.jpg

---
Weitere Details gibt es auf der <a href="http://www.startrekonline.com/upcoming_content" target="_blank">Website</a> des Entwicklers.

**<span style="text-decoration: underline;">Neue Highlevel Missionen:</span>**

  * **Collateral Damage**: Helfen sie Zivilisten, die in einem Kampf zwischen den Borg und Spezies 8472, zu überleben.
  * **Recovery**: Die Vorgeschichte zu Collateral Damage. Retten sie Kolonisten die mit dem Borg Virus infiziert sind und bekämpfen sie die Spezies 8472.
  * **State of Q**: Q lässt euch in die Vergangenheit reisen, damit ihr ihm einen speziellen Wunsch erfüllt.

**<span style="text-decoration: underline;">Neue Schlachtzug Missionen:</span>**

  * **The Cure:** Untersuchen sie eine föderierten Station mit die Sternenflotte den Kontakt verloren hat
  * **Khitomer Accord:** Reisen sie in der Zeit zurück und nehmen sie an den Ereignissen teil, die zur Freilassung der alten Borg geführt haben, die sie im Tutorial antreffen.
  * **Into the Hive:** Suchen sie einen vermissten Sternenflotten Kapitän und treten sie dabei der neuen Borgkönigin gegenüber

Desweiteren hat der Entwickler ein neues Video zu den Borg veröffentlicht.

[> Zum Star Trek Online &#8211; Borg Video][1]

* * *



* * *

 [1]: videos/item/root/star-trek-online-borg-trailer