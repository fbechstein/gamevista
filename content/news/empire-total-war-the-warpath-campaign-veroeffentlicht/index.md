---
title: 'Empire: Total War – The Warpath Campaign veröffentlicht'
author: gamevista
type: news
date: 2009-10-06T16:19:58+00:00
excerpt: '<p>[caption id="attachment_824" align="alignright" width=""]<img class="caption alignright size-full wp-image-824" src="http://www.gamevista.de/wp-content/uploads/2009/10/empire_thewarpath.jpg" border="0" alt="Empire: Total War - The Warpath Campaign" title="Empire: Total War - The Warpath Campaign" align="right" width="140" height="100" />Empire: Total War - The Warpath Campaign[/caption]Eine freudige Nachricht für alle <strong>Empire: Total War Fans</strong>. Publisher <a href="http://www.sega.com" target="_blank">SEGA </a>hat heute die erste Erweiterung für das Strategiespiel <strong>Empire: Total War</strong> veröffentlicht. Das Addon <em>The Warpath Campaign </em>könnt ihr ab sofort über Steam beziehen.</p> '
featured_image: /wp-content/uploads/2009/10/empire_thewarpath.jpg

---
Die Kosten dafür betragen 6,99 Euro. Mit **Empire: Total War – The Warpath Campaign** werden insgesamt fünf neue Fraktionen verfügbar sein. Bei diesen handelt es sich um die bekanntesten Indianerstämme Amerikas. Weiterhin sind neue Anführer, Einheiten und Technologien mit enthalten.

 

 

* * *



* * *

 