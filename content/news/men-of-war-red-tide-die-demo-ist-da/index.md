---
title: 'Men of War: Red Tide – Die Demo ist da!'
author: gamevista
type: news
date: 2009-11-20T18:06:39+00:00
excerpt: '<p>[caption id="attachment_174" align="alignright" width=""]<img class="caption alignright size-full wp-image-174" src="http://www.gamevista.de/wp-content/uploads/2009/07/men_of_war.jpg" border="0" alt="Men of War" title="Men of War" align="right" width="140" height="100" />Men of War[/caption]Der Publisher <a href="http://www.menofwargame.com" target="_blank">1C Company</a> hat heute die Demo zum eigenständig laufenden Addon <strong>Men of War: Red Tide</strong> veröffentlicht. Wer schon immer auf ausgefeilte Taktik und realistische Einheiten stand dem sei die Testversion von <strong>Red Tide</strong> ans Herz gelegt.</p> '
featured_image: /wp-content/uploads/2009/07/men_of_war.jpg

---
Zwar könnt ihr nur eine Mission anspielen, dafür könnt ihr aber auf der Seite der sowjetischen Armee den Ansturm der deutschen Wehrmacht auf die ukrainische Halbinsel Krim verteidigen. Die 1 Gigabyte große Demo könnt ihr bei uns downloaden.

[> Zum Men of War: Red Tide &#8211; Demo download][1]

 







 

 [1]: downloads/demos/item/demos/men-of-war-red-tide-demo