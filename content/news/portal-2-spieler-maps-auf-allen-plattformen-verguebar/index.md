---
title: Portal 2 – Spieler-Maps auf allen Plattformen vergübar
author: gamevista
type: news
date: 2011-01-24T19:30:54+00:00
excerpt: '<p>[caption id="attachment_2166" align="alignright" width=""]<img class="caption alignright size-full wp-image-2166" src="http://www.gamevista.de/wp-content/uploads/2010/08/portal2.jpg" border="0" alt="Portal 2" title="Portal 2" align="right" width="140" height="100" />Portal 2[/caption]Entwickler <strong>Valve </strong>kündigte erst vor kurzem an, dass der zweite Teil der Portal-Reihe auch plattformübergreifend im Koop-Modus gespielt werden kann. Nun wurde bekannt, dass <strong>Valve </strong>auch von Spielern erstellte Karten auf PlayStation 3, Xbox 360 sowie PC verfügbar machen will.</p> '
featured_image: /wp-content/uploads/2010/08/portal2.jpg

---
Demnach wird es möglich sein, eigene Level der PC-Version auch auf den Konsolen  weiterzuleiten. <a href="http://www.thinkwithportals.com" target="_blank">Portal 2</a> erscheint am 21.04.2011 für PlayStation 3, Xbox 360 und PC im Handel.

 

 

* * *



* * *