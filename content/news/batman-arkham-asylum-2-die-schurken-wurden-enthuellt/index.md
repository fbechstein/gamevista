---
title: 'Batman: Arkham Asylum 2 – Die Schurken wurden enthüllt'
author: gamevista
type: news
date: 2010-04-20T16:56:46+00:00
excerpt: '<p>[caption id="attachment_1707" align="alignright" width=""]<img class="caption alignright size-full wp-image-1707" src="http://www.gamevista.de/wp-content/uploads/2010/04/batman_aa2.jpg" border="0" alt="Batman: Arkham Asylum 2" title="Batman: Arkham Asylum 2" align="right" width="140" height="100" />Batman: Arkham Asylum 2[/caption]Die Synchronstimme von Batman, Kevin Conroy, aus dem Actionspiel Batman: Arkham Asylum hat sich auf der diesjährigen Chicago Comics & Entertainment Expo zu Wort gemeldet und ein wenig über den zweiten Teil des Actionspiels geredet. Dabei hat er den Auftritt von Two-Face erwähnt und damit inoffiziell bestätigt.</p> '
featured_image: /wp-content/uploads/2010/04/batman_aa2.jpg

---
Des Weiteren haben nun die Synchronsprecher von Talia al Ghul und Mr. Freeze, Maurice LaMarche und Stana Katic, Gerüchte aufkommen lassen, dass eben diese Charaktere in <a href="http://www.arkhamhasmoved.com" target="_blank">Batman: Arkham Asylum 2</a> dem schwarzen Ritter das Leben schwer machen sollen. Wir sind gespannt was sich in den nächsten Tagen davon bestätigen lässt.

 

 

   

* * *

   

