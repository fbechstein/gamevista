---
title: Torchlight – Termin steht fest
author: gamevista
type: news
date: 2009-09-05T21:33:44+00:00
excerpt: '<p><strong>[caption id="attachment_621" align="alignright" width=""]<img class="caption alignright size-full wp-image-621" src="http://www.gamevista.de/wp-content/uploads/2009/09/torchlight.jpg" border="0" alt="Torchlight" title="Torchlight" align="right" width="140" height="100" />Torchlight[/caption]Torchlight</strong> wurde bei seiner Ankündigung als Klon von Diablo, des wohl erfolgreichsten Action-Rollenspiels aller Zeiten, bezeichnet. Das Action-Rollenspiel Torchlight von dem Entwickler Runic Games hat nun einen Veröffentlichungstermin.</p> '
featured_image: /wp-content/uploads/2009/09/torchlight.jpg

---
Auf der „Penny Arcade Expo“ hat man den 27. Oktober 2009 als offiziellen Termin genannt. Das Online-Action-Rollenspiel erscheint nur als Downloadversion und wird 19,99 US-Dollar kosten. Das sind umgerechnet ungefähr 14 bis 15 €.

Zu hoffen bleibt nur, dass das Spiel mehr Erfolg haben wird als Hellgate: London, das letzte Projekt der Entwickler.

 

* * *



* * *