---
title: 'Starcraft 2: Wings of Liberty – Veröffentlichung im Juli'
author: gamevista
type: news
date: 2010-05-04T15:37:30+00:00
excerpt: '<p>[caption id="attachment_1752" align="alignright" width=""]<img class="caption alignright size-full wp-image-1752" src="http://www.gamevista.de/wp-content/uploads/2010/05/starcraft2_small.jpg" border="0" alt="Starcraft 2: Wings of Liberty" title="Starcraft 2: Wings of Liberty" align="right" width="140" height="88" />Starcraft 2: Wings of Liberty[/caption]Der Publisher <strong>Activision Blizzard</strong> hat den Release-Termin für das Echtzeitstrategiespiel <a href="http://www.starcraft2.com" target="_blank">Starcraft 2: Wings of Liberty</a> konkretisiert. Demnach wird die erste Episode des lang erwarteten zweiten Teils am 27. Juli 2010 in den Handel kommen. Wahlweise könnt ihr das Spiel neben der üblichen Handelsversion, auch per Battlenet erwerben.</p> '
featured_image: /wp-content/uploads/2010/05/starcraft2_small.jpg

---
Die Standard Edition von <a href="http://www.starcraft2.com" target="_blank">Starcraft 2: Wings of Liberty</a> wird für 59,99 Euro angeboten. Die Collectors Edition mit zusätzlichen Inhalten wird für 89,99 Euro erhältlich sein.

 

 

   

* * *

* * *