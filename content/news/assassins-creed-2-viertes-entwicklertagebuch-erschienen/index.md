---
title: Assassin’s Creed 2 – viertes Entwicklertagebuch erschienen
author: gamevista
type: news
date: 2009-10-16T19:32:40+00:00
excerpt: '<p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin’s Creed 2" title="Assassin’s Creed 2" align="right" width="140" height="100" />Assassin’s Creed 2[/caption]Publisher <a href="http://www.ubi.com" target="_blank">Ubisoft</a> hat heute das vierte Entwicklertagebuch zum Actionspiel <strong>Assassin’s Creed 2</strong> veröffentlicht. „All’s fair in love and war“, so der Name des Videos zeigt diesmal die Kämpfe mit den Wachen, die nun wesentlich abwechslungsreicher werden sollen.</p> '
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Laut Patrice Desilets, Creative Director von <a href="http://www.ubi.com" target="_blank">Ubisoft</a>, wird es drei unterschiedliche Varianten von Wachen geben. Der erste Typ wird das _Tier_ genannt, so soll es besonders schwierig sein gegen diesen Typ zu kämpfen da er ein sehr starker Gegner ist. Der zweite Typ von Wache ist der _Sucher_. Wie der Name schon vermuten lässt, sucht dieser leicht paranoide Kämpfer Enzio in jedem Winkel und Heuhaufen. Der dritte im Bunde ist der _Agile_. Er ist sehr flink auf den Beinen und hat die selbe Fähigkeit des „Free runnings“ wie Enzio. So ist es ihm möglich Enzio über die Häuserdächer zu verfolgen.

 

[> Zum Assassin’s Creed 2 &#8211; vierten Entwickler Video][1]

* * *



* * *

 

 [1]: videos/item/root/assassins-creed-2-viertes-entwicklertagebuch