---
title: Dead Rising 2 – Capcom verschiebt Zombietitel
author: gamevista
type: news
date: 2010-06-28T16:01:02+00:00
excerpt: '<p>[caption id="attachment_1442" align="alignright" width=""]<img class="caption alignright size-full wp-image-1442" src="http://www.gamevista.de/wp-content/uploads/2010/02/deadrising2.jpg" border="0" alt="Dead Rising 2" title="Dead Rising 2" align="right" width="140" height="100" />Dead Rising 2[/caption]<a href="http://www.capcom.com" target="_blank">Capcom</a> hat die Veröffentlichung für das Zombie-Actionspiel <strong>Dead Rising 2</strong> um einen ganzen Monat nach hinten verschoben.  Ursprünglich war der 31. August als Release geplant. Warum der Titel später in den Handel kommen soll hat <a href="http://www.capcom.com" target="_blank">Capcom</a> leider nicht verlauten lassen. Demnach wird <strong>Dead Rising 2</strong> in Europa am 1. Oktober erscheinen.</p> '
featured_image: /wp-content/uploads/2010/02/deadrising2.jpg

---
Der zweite Teil wird für PlayStation 3, Xbox 360 und PC entwickelt.

 

   

* * *

   

