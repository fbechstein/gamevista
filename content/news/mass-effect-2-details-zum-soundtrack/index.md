---
title: Mass Effect 2 – Details zum Soundtrack
author: gamevista
type: news
date: 2010-01-12T18:27:13+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2 Soundtrack komponiert von Jack Wall" align="right" width="140" height="100" />Mass Effect 2 Soundtrack komponiert von Jack Wall[/caption]In etwas über zwei Wochen ist es soweit, dann endlich erscheint <strong>Mass Effect 2</strong> im Handel. Der Entwickler <a href="http://www.masseffect.bioware.com" target="_blank">BioWare</a> hat nun weitere Details zum Spiel bekannt gegeben. So wird für den fast drei Stunden langen Soundtrack erneut auf den Komponisten Jack Wall gesetzt.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Dieser war schon beim ersten Teil für den orchestralen Soundtrack verantwortlich. Wer mag kann sich den Soundtrack gesondert als digitalen Download kaufen und herunterladen. Dies wird bereits ab dem 19. Januar 2010 möglich sein. **Mass Effect 2** erscheint in Deutschland am 28. Januar 2010.

 

 

<cite></cite>

* * *



* * *

 