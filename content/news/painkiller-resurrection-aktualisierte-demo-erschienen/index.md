---
title: 'Painkiller: Resurrection – Aktualisierte Demo erschienen'
author: gamevista
type: news
date: 2009-10-31T18:53:40+00:00
excerpt: '<p>[caption id="attachment_922" align="alignright" width=""]<img class="caption alignright size-full wp-image-922" src="http://www.gamevista.de/wp-content/uploads/2009/10/painkiller_small.png" border="0" alt="Painkiller: Resurrection" title="Painkiller: Resurrection" align="right" width="140" height="100" />Painkiller: Resurrection[/caption]Der Entwickler <a href="http://www.homegrowngames.at" target="_blank">Homegrown Games</a> hatte vor kurzem eine spielbare Demoversion des Egoshooters <strong>Painkiller: Resurrection</strong> veröffentlicht. So erschien die Demo vor dem 27. Oktober 2009 und wurde kurz danach wieder zurückgezogen. Heute wurde dann eine aktualisierte Version der Demo veröffentlicht. Diese soll nach Angaben des Entwicklerteams, die wirkliche Qualität des Spiels wiedergeben.</p> '
featured_image: /wp-content/uploads/2009/10/painkiller_small.png

---
 

Die Demo umfasst ganze drei Gigabyte und kann bei uns bezogen werden.

<a href="downloads/demos/item/demos/painkiller-resurrection-demo" target="_blank">> Zum Painkiller: Resurrection &#8211; Demo Download</a>

* * *



* * *

 