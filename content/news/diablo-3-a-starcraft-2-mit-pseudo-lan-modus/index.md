---
title: 'Diablo 3 & Starcraft 2 – Mit Pseudo-LAN Modus'
author: gamevista
type: news
date: 2009-08-23T13:17:40+00:00
excerpt: '<p>[caption id="attachment_456" align="alignright" width=""]<img class="caption alignright size-full wp-image-456" src="http://www.gamevista.de/wp-content/uploads/2009/08/diablo3_small.jpg" border="0" alt="Diablo 3" title="Diablo 3" align="right" width="140" height="100" />Diablo 3[/caption]Greg Canessa seineszeichen Entwickler bei <a href="http://www.eu.blizzard.com/de/">Blizzard </a>teilte der Community im Rahmen eines Interviews nun mit das sie momentan an einem Ersatz für den von den Fans vermissten Lan - Modus arbeiten. Dieser Pseudo Lan - Modus wird demnach ins neue Battlenet 2.0 integriert und soll es <strong>Diablo 3</strong> bzw. <strong>Starcraft 2</strong> Nutzern ermöglichen über ihr Netzwerk mit niedrigen Pings zusammen zu spielen.</p> '
featured_image: /wp-content/uploads/2009/08/diablo3_small.jpg

---
Bedingung wird dafür aber eine Verifizierung durch das Battlenet 2.0 sein. Das Bedeutet das mindestens ein Computer des Netzwerks am Internet angeschlossen sein muss. Wie genau alles funktionieren soll ist bisher aber noch nicht bekannt.

* * *



* * *

 