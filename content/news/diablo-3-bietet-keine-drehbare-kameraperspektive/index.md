---
title: Diablo 3 – Bietet keine drehbare Kameraperspektive
author: gamevista
type: news
date: 2009-11-03T19:23:02+00:00
excerpt: '<p>[caption id="attachment_456" align="alignright" width=""]<img class="caption alignright size-full wp-image-456" src="http://www.gamevista.de/wp-content/uploads/2009/08/diablo3_small.jpg" border="0" alt="Diablo 3" title="Diablo 3" align="right" width="140" height="100" />Diablo 3[/caption]Verschiedenste Promotionvideos zum Actionrollenspiel <a href="http://www.blizzard.com/diablo3" target="_blank">Diablo 3</a> erweckten bisher den Eindruck das es eine frei verstellbare Kameraansicht geben wird. So wurden extra für Trailer und Videos die Ansichten verstellt um das Spielgeschehen noch besser aussehen zu lassen.</p> '
featured_image: /wp-content/uploads/2009/08/diablo3_small.jpg

---
Diese Funktion wird in der Finalen Version von **Diablo 3** leider nicht enthalten sein. Dies bestätigte ein Blizzard-Mitarbeiter nun im [offiziellen Forum][1]. Bashiok sagt demzufolge das der Perspektivwechsel ausschließlich für Demonstrationszwecke benutzt würde. Zwar haben die Entwickler darüber nachgedacht diese Funktion beizubehalten, aber sie haben sich schließlich dagegen entschieden. Der Grund dafür ist laut <a href="http://www.blizzard.com/diablo3" target="_blank">Blizzard</a> eine Störung im Spielfluss der aus der Nutzung der Zoomfunktion resultiert. So wird **Diablo 3** das bleiben was es immer war, ein Spiel mit fester isometrischer Ansicht.

 

* * *



* * *

 [1]: http://forums.battle.net/thread.html?topicId=20677234788&postId=206757865595&sid=3000#5