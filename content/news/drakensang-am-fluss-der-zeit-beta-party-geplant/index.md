---
title: 'Drakensang: Am Fluss der Zeit – Beta Party geplant'
author: gamevista
type: news
date: 2009-09-01T17:02:31+00:00
excerpt: '<p>[caption id="attachment_437" align="alignright" width=""]<img class="caption alignright size-full wp-image-437" src="http://www.gamevista.de/wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg" border="0" alt="Drakensang: Am Fluss der Zeit" title="Drakensang: Am Fluss der Zeit" align="right" width="140" height="100" />Drakensang: Am Fluss der Zeit[/caption]Für diejenigen die das Rollenspiel<strong> Drakensang: Am Fluss der Zeit</strong> schon mal etwas früher antesten möchten wird es bald eine Möglichkeit geben. Publisher <a href="http://www.dtp-entertainment.com">dtp Entertainment </a>gab heute im Rahmen einer Pressemitteilung bekannt des es in der Zeit vom 12. bis 13. September 2009 ein paar Betatest Partys geben wird.</p> '
featured_image: /wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg

---
Diese sollen unter anderem in Berlin, Halle sowie Hamburg stattfinden. Wer dabei sein möchte muss sich per Email mit persönlichen Angaben wie Name und Telefonnummer, sowie Angaben zur Erfahrung mit Spielen bzw. „Das schwarze Auge“ machen. Die Email sollte dann an die Adresse <Beta-Party@drakensang.de> gehen. Details zum Ganzen gibt es unter anderem im [offiziellem Forum][1].

 

* * *



* * *

 [1]: http://forum.dtp-entertainment.com/viewtopic.php?f=195&t=13533