---
title: Metro 2033 – Eine Demoversion ist immer noch fraglich
author: gamevista
type: news
date: 2010-02-28T17:06:39+00:00
excerpt: '<p>[caption id="attachment_1346" align="alignright" width=""]<img class="caption alignright size-full wp-image-1346" src="http://www.gamevista.de/wp-content/uploads/2010/01/metro_2033.jpg" border="0" alt="Metro 2033" title="Metro 2033" align="right" width="140" height="100" />Metro 2033[/caption]Das Entwicklerteam rund um <strong>4A Games</strong> hat nun offiziell eine Demoversion vor Veröffentlichung des Endzeit-Shooters <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a> ausgeschlossen. So wurde über <a href="http://twitter.com/Metro2033/status/9679825278">Twitter</a> mitgeteilt, dass die Entwickler ihre volle Zeit für die finale Version investieren. Für eine Testversion bleibt so schlichtweg keine Zeit.</p> '
featured_image: /wp-content/uploads/2010/01/metro_2033.jpg

---
Ob es eine Demo nach der Veröffentlichung von <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a> geben wird, wollen die Entwickler nicht ausschließen. Einen Termin gibt es jedenfalls bisher nicht. <a href="http://www.metro2033game.com" target="_blank">Metro 2033</a> erscheint am 16. März 2010 im Handel.

 

   

* * *

   



 