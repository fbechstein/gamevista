---
title: 'Battlefield: Bad Company 2 – Kein Onslaught-DLC für PC-Besitzer'
author: gamevista
type: news
date: 2011-02-03T17:05:19+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Entwickler <strong>DICE </strong>hat im Rahmen einer Meldung auf dem <a href="http://blogs.battlefield.ea.com/battlefield_bad_company/archive/2011/02/03/looking-ahead.aspx##" target="_blank">offiziellen Battlefield-Blog</a> verkündet, dass die PC-Version von <strong>Battlefield 1943</strong> und der <strong>Onslaught</strong>-DLC für <a href="http://www.battlefieldbadcompany2.com" target="_blank">Battlefield: Bad Company 2</a> nicht weiter entwickelt werden würden. Als Grund dafür gab Karl Magnus Troedsson, General Manager von <strong>DICE</strong>, das sich derzeit in der Entwicklung befindlichen Battlefield 3 an.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
**DICE** möchte jede verfügbare Energie in den dritten Teil stecken und das beste Battlefield aller Zeiten auf den Markt bringen.

 

 

* * *



* * *