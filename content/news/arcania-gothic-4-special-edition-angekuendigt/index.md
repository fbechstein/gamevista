---
title: 'Arcania: Gothic 4 – Special Edition angekündigt'
author: gamevista
type: news
date: 2010-08-23T17:14:02+00:00
excerpt: '<p>[caption id="attachment_1648" align="alignright" width=""]<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Publisher <strong>JoWood </strong>bringt neben der normalen Verkaufsversion für das Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a>, auch eine Special Edition in den Handel. Diese Sonderedition bietet neben einer schicken Verpackung in Leder, ein 44 seitiges Storybook und ein Artbook mit Kunstledereinband.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
Preislich wird die Special Edition bei ungefähr 75 Euro liegen.   
**  
Weiter Inhalte in der Übersicht:**

 

Lederbox-Verpackung: Mit Folienprägung des Logos, verspiegelter 3D-Kristall im Logo   
Storybook „The Cleaved Maiden“: Edler Einband in Lederanmutung mit Prägung. Ca. 44 Seiten.   
Artbook: Kunstledereinband, geprägt. Druck auf hochwertigem Papier im Mittelalter-Stil. Ca. 72 Seiten.   
Stoff-Landkarte: Ca. 42 x 30 cm – farbiger Druck mit Kontur-Stanzung.   
Soundtrack: Audio-CD in Kartonstecktasche   
Aufkleber: DIN A6-Aufkleber mit Logo „ArcaniA – Gothic 4“   
Poster: Großes DIN A3-Poster, gedruckt auf hochwertigem Papier

   

* * *

   



 