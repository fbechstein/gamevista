---
title: 'Call of Duty: Black Ops – Erstmals mit 3D-Support'
author: gamevista
type: news
date: 2010-10-05T19:54:50+00:00
excerpt: '<p>[caption id="attachment_1747" align="alignright" width=""]<img class="caption alignright size-full wp-image-1747" src="http://www.gamevista.de/wp-content/uploads/2010/05/callofduty7.jpg" border="0" alt="Call of Duty: Black Ops" title="Call of Duty: Black Ops" align="right" width="140" height="100" />Call of Duty: Black Ops[/caption]Der Ego-Shooter <a href="http://www.callofduty.com" target="_blank">Call of Duty: Black Ops</a> wird erstmals in der Titelreihe auch 3D-Support bieten. Dazu benötigt man natürlich den passenden 3D-HD-Fernsehr oder eine NVIDIA 3D Vision-Ausstattung für den PC. „Die Entwicklung für räumliche 3D-Darstellung begann als ein Forschungsprojekt, doch als wir sahen, was diese Technologie der Call of Duty-Erfahrung bringt, wussten wir, dass wir das einfach für Black Ops anbieten müssen.</p> '
featured_image: /wp-content/uploads/2010/05/callofduty7.jpg

---
Mit der dreidimensionalen Waffe zu zielen, in Deckung zu gehen und sich in hochdetailliertem Terrain zu bewegen, ist eine der tollen Erfahrungen von Black Ops in 3D“, so Mark Lamia, Studio Head von Treyarch. Der Titel wird weltweit am 9. November 2010 in den Handel kommen.

 

   

* * *

   

