---
title: 'Operation Flashpoint 2: Dragon Rising – Special Edition mit Helm'
author: gamevista
type: news
date: 2009-08-05T20:07:22+00:00
excerpt: '<p>[caption id="attachment_198" align="alignright" width=""]<img class="caption alignright size-full wp-image-198" src="http://www.gamevista.de/wp-content/uploads/2009/07/opf2_small.jpg" border="0" alt="Operation Flashpoint 2: Dragon Rising" title="Operation Flashpoint 2: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint 2: Dragon Rising[/caption]Für die Pc Version von <strong>Operation Flashpiont 2: Dragon Rising</strong> werden 2 Versionen erscheinen. Eine Normale- und eine Specialedition. Diese Specialedition wird neben einer persönlichen so genannten <span> </span>„Hundemarke“ auch einen echten Helm enthalten. Laut dem deutschen Community Manager „Mukor“ von <a href="http://www.codemasters.com" target="_blank" title="codemasters">Codemasters </a>wird dieser aber nicht neu sein.</p> '
featured_image: /wp-content/uploads/2009/07/opf2_small.jpg

---
Da dieser &#8222;die üblichen Gebrauchsspuren und Beschädigungen aufweist, in Größe und Beschaffenheit variieren kann und ausschließlich für Dekorationszwecke bestimmt ist!” <a href="http://www.amazon.de/Operation-Flashpoint-Dragon-Rising-Special/dp/B002K8P35M/ref=sr_1_8?ie=UTF8&#038;s=software&#038;qid=1249376547&#038;sr=8-8" target="_blank" title="special edition bei amazon.de">Amazon.de listet</a> bereits den Artikel, bisher ist aber noch kein Preis bekannt. **Operation Flashpoint 2: Dragon Rising** wird mit amBX-Unterstützung am 08. Oktober  <span></span>erscheinen.





