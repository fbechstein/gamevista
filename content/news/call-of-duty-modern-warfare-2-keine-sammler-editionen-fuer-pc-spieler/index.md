---
title: 'Call of Duty: Modern Warfare 2 – Keine Sammler Editionen für PC Spieler'
author: gamevista
type: news
date: 2009-07-30T11:11:02+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Eine der einschlägigsten News der letzten Tage war sicher der Verkündung von <a href="http://www.activision.com" target="_blank" title="activions blizzard">Activision</a> <a href="http://www.eu.blizzard.com" target="_blank" title="blizzard">Blizzard</a>, diese berichteten über eine so genannte Prestige Edition für den kommenden Shooter Hit <strong>Call of Duty: Modern Warfare 2</strong>. Enthalten soll hier ein funktionierendes Nachtsichtgerät, Artbook und ein Coupon for Call of Duty 1 sein.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Nun hat Community Manager Robert Bowling vom Entwickler <a href="http://www.infinityward.com" target="_blank" title="infinity ward">Infinity Ward</a> der Presse mitgeteilt das die Steelbook bzw. Prestige Edtion Variante nicht für PC Spieler verfügbar sind. Das heißt es wird keine Sammler Edition für PC Spieler geben. Einen Grund konnte Bowling nicht nennen. Wir hoffen aber auf ein wenig Liebe für die PC Spieler und auf ein umdenken von <a href="http://www.infinityward.com" target="_blank" title="infinity ward">Infinity Ward</a>.







 

 