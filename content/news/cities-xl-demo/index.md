---
title: Cities XL – Demo
author: gamevista
type: news
date: 2009-09-01T12:07:02+00:00
excerpt: '<p>[caption id="attachment_563" align="alignright" width=""]<img class="caption alignright size-full wp-image-563" src="http://www.gamevista.de/wp-content/uploads/2009/09/citysxl.jpg" border="0" alt="Cities XL" title="Cities XL" align="right" width="140" height="100" />Cities XL[/caption]Ab sofort könnt Ihr die Demo zu <strong>Cities XL</strong> in unserer Downloadsektion herunterladen. In der Demo könnt Ihr Euch als Bürgermeister versuchen. Ferner steht ein Onlinemodus zur Verfügung, für welchen jedoch eine Registrierung notwendig ist. Diese Demo ist nur bis zum 30. September 2009 spielbar.</p> '
featured_image: /wp-content/uploads/2009/09/citysxl.jpg

---
[> Demo zu Cities XL herunterladen][1]

* * *



* * *

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=241