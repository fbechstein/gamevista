---
title: 'The Lord of the Rings: War in the North – Neues Herr der Ringe Spiel angekündigt'
author: gamevista
type: news
date: 2010-03-18T20:16:03+00:00
excerpt: '<p>[caption id="attachment_1584" align="alignright" width=""]<img class="caption alignright size-full wp-image-1584" src="http://www.gamevista.de/wp-content/uploads/2010/03/lotr_warinthenorth.jpg" border="0" alt="The Lord of the Rings: War in the North" title="The Lord of the Rings: War in the North" align="right" width="140" height="100" />The Lord of the Rings: War in the North[/caption]Das Entwicklerteam <strong>Snowblind Studios</strong> hat zusammen mit dem Publisher <strong>Warner Bros. </strong>ein neues Spiel im Herr der Ringe-Stil angekündigt. In <a href="http://www.warinthenorth.com/" target="_blank">Lord of the Rings: War in the North</a> ziehen sie im Koop-Modus mit bis zu drei anderen Spielkollegen, gegen Saurons Armeen in die Schlacht. Überhaupt soll das Action-Rollenspiel sehr viel Wert auf das Zusammenspiel legen.</p> '
featured_image: /wp-content/uploads/2010/03/lotr_warinthenorth.jpg

---
Der Fokus der Geschichte wird diesmal nicht auf der Hauptstory von Tolkiens Büchern liegen, sondern sich auf den Krieg im Norden von Mittelerde konzentrieren. Natürlich sind wieder bekannte Gesichter und Stimmen aus Büchern und Filmen mit dabei. Passend dazu hat der Publisher den Debüt-Trailer zu <a href="http://www.warinthenorth.com/" target="_blank">Lord of the Rings: War in the North</a> präsentiert.

[> Zum The Lord of the Rings: War in the North &#8211; Debüt Trailer][1]

<cite></cite>

   

* * *

   



 

 [1]: videos/item/root/the-lord-of-the-rings-war-in-the-north-debuet-trailer