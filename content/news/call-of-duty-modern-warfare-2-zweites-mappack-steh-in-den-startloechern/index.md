---
title: 'Call of Duty: Modern Warfare 2 – Zweites Mappack steh in den Startlöchern'
author: gamevista
type: news
date: 2010-05-13T19:49:25+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Der Publisher <strong>Activision </strong>kündigte vor kurzem das zweite Karten-Erweiterungspaket für den Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare 2</a> offiziell an. Der DLC mit dem Namen Resurgence Pack wird fünf neue Mehrspielerkarten enthalten und ist momentan nur für Xbox 360 angekündigt. Der Veröffentlichungstermin liegt auf dem 3. Juni 2010.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Die Kosten dürften sich ähnlich dem **Stimulus Pack** bei 15 Euro einpendeln. Auch wird gemutmaßt, dass wie beim ersten Zusatzinhalt, Xbox 360-Besitzer exklusiv einen Monat vor PlayStation 3 und PC-Nutzern in den Genuss des DLC kommen werden.

 

   

* * *

   

