---
title: Battlefield 2 – Neuer Bugfix für Alt+Tab Problem erschienen
author: gamevista
type: news
date: 2009-09-08T08:43:02+00:00
excerpt: '<p>[caption id="attachment_576" align="alignright" width=""]<a href="http://www.ea.com/games/battlefield-2-complete-collection"><img class="caption alignright size-full wp-image-576" src="http://www.gamevista.de/wp-content/uploads/2009/09/BF2.png" border="0" alt="Battlefield 2" title="Battlefield 2" align="right" width="140" height="100" />Electronic Arts</a>Battlefield 2[/caption] hat heute einen kleinen Patch für den Multiplayer Shooter <strong>Battlefield 2</strong> freigegeben. Grund für den neuen Patch ist der alte Patch 1.50, bei dem es Probleme gab nachdem man die Alt+Tab Funktion von Windows nutzte. <strong>Battlefield 2</strong> wurde dadurch nicht minimiert sondern schmierte komplett ab, sehr zum Ärger der Fans. Das knapp 1 Megabyte große Update soll dieses Problem nun beheben.</p> '
featured_image: /wp-content/uploads/2009/09/BF2.png

---
Wie immer findet ihr den Download bei uns im Patchbereich:

 

[> Zum Battlefield 2 1.50 Alt +Tab Bugfix][1]

Dazu noch ein Installationhinweis:

  * Laden Sie die Datei herunter
  * Kopieren Sie die entpackte Datei in Ihr Installationsverzeichnis von Battlefield 2 (Patch v1.50 muss bereits installiert sein)
  * Überschreiben Sie die bestehenden Dateien
  * Löschen Sie den &#8222;cache&#8220;-Ordner unter Eigene Dateien\Battlefield 2\mods\bf2
  * Starten Sie Battlefield 2

 

* * *



* * *

 [1]: downloads/patches/item/patches/battlefield-2-patch-150-alttab-hotfix