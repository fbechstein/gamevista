---
title: Guild Wars 2 – E3 Nein? Gamescom Ja!
author: gamevista
type: news
date: 2010-06-03T17:20:25+00:00
excerpt: '<p>[caption id="attachment_433" align="alignright" width=""]<img class="caption alignright size-full wp-image-433" src="http://www.gamevista.de/wp-content/uploads/2009/08/guildwars2.jpg" border="0" alt="Guild Wars 2" title="Guild Wars 2" align="right" width="140" height="100" />Guild Wars 2[/caption]Die Entwickler <strong>ArenaNet </strong>haben auf ihrer <a href="http://www.arena.net/blog/" target="_blank">offiziellen Internetseite</a> den Auftritt auf der kommenden E3-Spielemesse in Los Angeles dementiert. Den Grund liefert die Entwickler mit der Antwort das die E3 zu sehr auf Presse und Medien abgestimmt sei. Der Fan der sich am meisten über das Spiel freue kommt zu kurz. Deswegen entschieden sich die Entwickler gegen eine Präsentation auf der E3.</p> '
featured_image: /wp-content/uploads/2009/08/guildwars2.jpg

---
Doch damit nicht genug. Es gibt auch positives zu vermelden, denn <a href="http://www.guildwars2.com/en/" target="_blank">Guild Wars 2</a> wird dafür auf der gamescom 2010 in Köln und auf der Comi-Con in San Diego vorgestellt.  
Die gamescom findet vom 18. bis 22. August statt. Dort wird der zweite Teil an mehreren Stationen spielbar sein.

 

   

* * *

   

