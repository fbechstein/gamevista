---
title: Just Cause 2 – Neues Mini-DLC angekündigt
author: gamevista
type: news
date: 2010-04-22T19:18:33+00:00
excerpt: '<p>[caption id="attachment_1716" align="alignright" width=""]<img class="caption alignright size-full wp-image-1716" src="http://www.gamevista.de/wp-content/uploads/2010/04/justcause2_small.png" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Nachdem der Entwickler <strong>Avalanche Software</strong> für das Actionspiel <a href="http://www.justcause.com" target="_blank">Just Cause 2</a> schon einige Zusatzinhalte veröffentlicht hat, Monstertruck bzw. gepimpter Eiswagen, wird nun demnächst ein neues Gefährt nachgeschoben. Das <strong>Black Market Aerial Pack</strong> wird am 29. April veröffentlicht und liefert für 160 Microsoft Punkte, bzw. 1-2 Euro, das Flugzeug F-33 Dragonfly Jet Fighter auf die Insel Panau.</p> '
featured_image: /wp-content/uploads/2010/04/justcause2_small.png

---
Desweiteren wird der Fallschirm verbessert indem sie mit zwei Düsen wieder an Höhe gewinnen können. 

 

   

* * *

* * *