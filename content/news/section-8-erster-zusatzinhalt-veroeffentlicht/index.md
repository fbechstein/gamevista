---
title: Section 8 – Erster Zusatzinhalt veröffentlicht
author: gamevista
type: news
date: 2009-11-27T21:55:52+00:00
excerpt: '<p>[caption id="attachment_450" align="alignright" width=""]<img class="caption alignright size-full wp-image-450" src="http://www.gamevista.de/wp-content/uploads/2009/08/section8.jpg" border="0" alt="Section 8" title="Section 8" align="right" width="140" height="100" />Section 8[/caption]Der Entwickler <a href="http://www.joinsection8.com" target="_blank">TimeGate Studios</a> gab vor einiger Zeit die ersten Infos zum Zusatzinhalt „<strong>Seek and Destroy Mappack</strong>“ bekannt. Über einen Termin hielt sich der Entwickler aber bisher bedeckt und so verwundert es das schon seit Mittwoch den 25. November 2009 das DLC verfügbar ist. So könnt ihr ab sofort über den Games for Windows Marketplace das Kartenpaket herunterladen.</p> '
featured_image: /wp-content/uploads/2009/08/section8.jpg

---
Das kostenpflichtige Mappack mit den drei neuen Karten Devil\`s Backbone, Hornet\`s Nest und Azure Basin soll knapp 7 US Dollar kosten. Die Karten werden im Multiplayer-Modus und Instant-Action-Modus verfügbar sein.

 

 







 