---
title: 'Operation Flashpoint: Dragon Rising – Entwickler stellt Support ein'
author: gamevista
type: news
date: 2010-02-17T17:12:17+00:00
excerpt: '<p>[caption id="attachment_606" align="alignright" width=""]<img class="caption alignright size-full wp-image-606" src="http://www.gamevista.de/wp-content/uploads/2009/09/opf2_small.jpg" border="0" alt="Operation Flashpoint: Dragon Rising" title="Operation Flashpoint: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint: Dragon Rising[/caption]Der Entwickler <strong>Codemasters </strong>stellt den Support für sein vier Monate altes Spiel <a href="http://www.flashpointgame.com" target="_blank">Operation Flashpoint: Dragon Rising</a> ein. Diese Meldung geht aus einem Beitrag des Community Managers, Ian Webster, im offiziellen <a href="http://community.codemasters.com/forum/operation-flashpoint-dragon-rising-game-pc-113/407318-16-02-confirmation-no-further-patches-additional-dlc.html" target="_blank">Operation Flashpoint-Forum</a> hervor.</p> '
featured_image: /wp-content/uploads/2009/09/opf2_small.jpg

---
Laut Webster sollen keine weiteren Updates bzw. Zusatzinhalte erscheinen. Grund für die Einstellung des Supports soll die Entwicklung eines neuen Spiels sein. Damit bleiben Patch 1.2 und die beiden Erweiterungen die letzten Aktualisierungen für <a href="http://www.flashpointgame.com" target="_blank">Operation Flashpoint: Dragon Rising</a>.

 

* * *



* * *