---
title: Team Fortress 2 – Update ist da
author: gamevista
type: news
date: 2009-08-16T18:06:21+00:00
excerpt: '<p>[caption id="attachment_368" align="alignright" width=""]<img class="caption alignright size-full wp-image-368" src="http://www.gamevista.de/wp-content/uploads/2009/08/tf2.jpg" border="0" alt="Team Fortress 2" title="Team Fortress 2" align="right" width="140" height="100" />Team Fortress 2[/caption]So schnell kanns gehen. Erst vor kurzem kündigte <a href="http://www.valvesoftware.com" target="_blank">Valve</a> ein Update für den Ego Shooter <strong>Team Fortress 2</strong> an. Nun wurde überraschend das Update veröffentlicht. Enthalten sind neben 18 neuen Kopfbedeckungen auch ein neuer Spielmodus Namens King of the Hill. Weiterhin gibt es neben neuen Karten auch einige Überarbeitungen des Spiels. Das komplette Changelog zum Classless Update könnt ihr <a href="http://www.teamfortress.com/classless/" target="_blank">hier</a> finden.</p> '
featured_image: /wp-content/uploads/2009/08/tf2.jpg

---
 

<a href="http://www.teamfortress.com/classless/" target="_blank">> Zum Changelog</a>







 

 

 

 