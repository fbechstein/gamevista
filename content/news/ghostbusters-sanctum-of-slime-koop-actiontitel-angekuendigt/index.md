---
title: 'Ghostbusters: Sanctum of Slime – Koop-Actiontitel angekündigt'
author: gamevista
type: news
date: 2010-12-02T19:03:51+00:00
excerpt: '<p>[caption id="attachment_2471" align="alignright" width=""]<img class="caption alignright size-full wp-image-2471" src="http://www.gamevista.de/wp-content/uploads/2010/12/ghostbusters_sos.jpg" border="0" alt="Ghostbusters: Sanctum of Slime" title="Ghostbusters: Sanctum of Slime" align="right" width="140" height="100" />Ghostbusters: Sanctum of Slime[/caption]Publisher <strong>Atari </strong>hat offiziell einen neuen Ghostbusters-Titel angekündigt. <strong>Ghostbusters: Sanctum of Slime</strong> wird von den Machern der Wanako Studios für PC, PlayStation 3 und Xbox 360 entwickelt. Der Titel ist ein typischer Top-Down-Shooter, wobei ihr zu viert als Coop-Team das Actionspiel bestreiten könnt.</p> '
featured_image: /wp-content/uploads/2010/12/ghostbusters_sos.jpg

---
Der Koop-Modus wird sowohl als Offline- bzw. Online-Variante angeboten. Leider werden die Hauptakteure aus den bekannten Ghostbusters Filmen nicht als Spielfiguren herhalten.

 

 

   

* * *

   

