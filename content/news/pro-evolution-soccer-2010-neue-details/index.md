---
title: Pro Evolution Soccer 2010 – Neue Details
author: gamevista
type: news
date: 2009-08-17T18:34:42+00:00
excerpt: '<p class="MsoNormal">[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]Es gibt neue Details zum Fussballspiel <strong>Pro Evolution Soccer 2010</strong> von Entwickler <a href="http://www.de.games.konami-europe.com" target="_blank">Konami</a>. Laut Pressemitteilung hat sich <a href="http://www.de.games.konami-europe.com" target="_blank">Konami </a>erstmals die Rechte an der deutschen Nationalmannschaft gesichert. Weiterhin sagt der Entwickler das sie die 360-Grad Kontrolle in den neuesten Teil des Fussballspiels eingebaut haben. Demnach soll es nun möglich sein das man mit dem Analog Stick eine wesentlich besser Ballkontrolle hat. Das waren aber nicht die einzigen Neuerungen die uns Naoya Hatsmui mitgeteilt hat, Producer von <strong>Pro Evolution Soccer 2010</strong>, demnach gibt es noch diese neue Details die  komplett aus Fanideen enstanden.</p> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
<p class="MsoNormal">
   
</p>

  * <span>Erstmals komplette, manuelle Kontrolle über Torhüter möglich</span>
  * <span>Neue Animationen bei Torhütern (Unterarmwürfe), Dribblings, Läufen und Drehungen</span>
  * <span>Optimierungen bei der Ballannahme, den Dribblings und den Richtungswechseln</span>
  * <span>Manuelle Anforderung von Pässen in allen Spielmodi</span>
  * <span>Vereinfachung von Tricks und Körpertäuschungen</span>

<p class="textblock">
  Einen konkreten Veröffentlichungstermin gibt es bisher nicht.
</p>

* * *



* * *

 