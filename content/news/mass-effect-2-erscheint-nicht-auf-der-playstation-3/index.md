---
title: Mass Effect 2 – Erscheint nicht auf der PlayStation 3
author: gamevista
type: news
date: 2009-10-19T14:50:13+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]So geht’s zu in der Gerüchteküche. Vor ein paar Tagen wurde berichtet das das Actionrollenspiel <strong>Mass Effect 2,</strong> laut dem Communiy Manager Jay Watamaniuk, auch für PlayStation 3 erscheinen soll.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Dem Gerücht wurde nun mit einem Dementi ein Ende gesetzt, denn Jay Watamaniuk stellte nun klar das **Mass Effect 2** definitiv nicht für die Konsole PlayStation 3 erscheinen wird. Weiterhin entschuldigte sich der Community Manager bei den Fans und gab bekannt das es sich um ein Missverständnis handelte.

 

 

* * *



* * *

 