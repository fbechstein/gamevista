---
title: 'Fallout: New Vegas – Kommt geschnitten nach Deutschland'
author: gamevista
type: news
date: 2010-08-10T15:28:06+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Das Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> wird stark geschnitten auf den deutschen Markt erscheinen. Demnach verteilte die Unterhaltungssoftware Selbstkontrolle, kurz USK, das rote Siegel für den Titel. Publisher <strong>Namco Bandai</strong> teilte außerdem mit, dass sämtliche Bluteffekte bei Treffern ausgeblendet werden.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Auch wie schon im 3. Teil der Fallout-Reihe wird das abtrennen von Körperteilen entfernt.    
<a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> wird am 22. Oktober 2010 für PC, Xbox 360 und PlayStation 3 im Handel erscheinen.

 

   

* * *

   



 