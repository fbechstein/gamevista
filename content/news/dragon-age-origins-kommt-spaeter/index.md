---
title: 'Dragon Age: Origins – kommt später'
author: gamevista
type: news
date: 2009-08-10T11:19:12+00:00
excerpt: '<p>[caption id="attachment_317" align="alignright" width=""]<img class="caption alignright size-full wp-image-317" src="http://www.gamevista.de/wp-content/uploads/2009/08/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins kommt später" title="Dragon Age: Origins kommt später" align="right" width="140" height="100" />Dragon Age: Origins kommt später[/caption]Eigentlich war alles klar, dass der Toptitel 2009 - <strong>Dragon Age: Origins</strong> -  am 20. Oktober 2009 veröffentlicht wird. Nun gab der Publisher<a href="http://www.electronic-arts.de/" target="_blank"> Electronic Arts</a> die Verschiebung des Titels bekannt. Demnach wird der Streetday um ca. zwei Wochen, auf den 6. November 2009, gelegt. Dies betrifft sowohl die Xbox 360- als auch die PC-Version. Die Playstation 3 Version wird sogar auf Ende November geschoben.</p>'
featured_image: /wp-content/uploads/2009/08/dragon_age_small.jpg

---
Gründe für die Verschiebungen wurden keine genannt. 





