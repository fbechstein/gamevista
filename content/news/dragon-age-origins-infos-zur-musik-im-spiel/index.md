---
title: 'Dragon Age: Origins – Infos zur Musik im Spiel'
author: gamevista
type: news
date: 2009-09-29T08:21:44+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Über den Entwickler <a href="http://dragonage.bioware.com/">BioWare </a>sickern jetzt immer mehr Informationen des kommenden Rollenspielhits <strong>Dragon Age: Origins</strong> durch. Zum einen wäre da ein neuer Trailer in dem es um die musikalische Gestaltung des Spiels geht. Hier erklärt der Komponist Inon Zur wie er versucht die düstere Atmosphäre für <strong>Dragon Age</strong> über seine Musik wiederzugeben.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Weiterhin gibt es wieder zahlreiche Gameplay Szenen zu sehen. Zum anderen wäre da der der offiziell käufliche Soundtrack von **Dragon Age: Origins**. Dieser wird zeitgleich zur Veröffentlichung des Spiels erscheinen. Einige Stücke von dem Tonträger werden auch auf der **Dragon Age: Origins Collector\`s Edition** enthalten sein.

[> Zum Dragon Age: Origins &#8211; Music Trailer][1]

* * *



* * *

 

 [1]: videos/item/root/dragon-age-origins-music-trailer