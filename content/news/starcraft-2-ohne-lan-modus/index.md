---
title: Starcraft 2 ohne LAN Modus
author: gamevista
type: news
date: 2009-08-17T12:59:10+00:00
excerpt: '<p>[caption id="attachment_370" align="alignright" width=""]<img class="caption alignright size-full wp-image-370" src="http://www.gamevista.de/wp-content/uploads/2009/08/starcraft2_small.jpg" border="0" alt="Starcraft 2 ohne LAN Modus" title="Starcraft 2 ohne LAN Modus" align="right" width="140" height="88" />Starcraft 2 ohne LAN Modus[/caption]<strong>Starcraft 2</strong> soll komplett ohne LAN Modus auskommen. Dies bestätigte der Entwickler <a href="http://eu.blizzard.com/de/" target="_blank">Blizzard</a>. Ein Aus für Lan Parties? <br />Trotz einer Petition, wo bereits über 100.000 User unterschrieben haben, bleibt Blizzard hartnäckig bei seiner Entscheidung. Das Spiel wird über das neue Battle.Net spielbar sein. Wer darauf verzichtet, kann also zum einen nicht gegen andere spielen, noch seine Freundesliste erweitern und Erfolge freischalten. Es bleibt dann beim simplen Solo Game. Das Aus für den LAN Modus scheint auch auf Diablo 3 überzugreifen - auch da wird man womöglich vergeblich einen LAN Modus suchen.</p> '
featured_image: /wp-content/uploads/2009/08/starcraft2_small.jpg

---
Wir werden auf der gamescom Starcraft 2 und Diablo 3 für Euch ansehen und versuchen einige Statements von den Verantwortlichen zum Thema LAN Modus zu erhaschen.

* * *



* * *