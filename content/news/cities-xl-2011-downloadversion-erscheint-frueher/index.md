---
title: Cities XL 2011 – Downloadversion erscheint früher
author: gamevista
type: news
date: 2010-10-11T17:22:07+00:00
excerpt: '<p>[caption id="attachment_2328" align="alignright" width=""]<img class="caption alignright size-full wp-image-2328" src="http://www.gamevista.de/wp-content/uploads/2010/10/citysxl.jpg" border="0" alt="Cities XL 2011" title="Cities XL 2011" align="right" width="140" height="100" />Cities XL 2011[/caption]Publisher <strong>dtp </strong>hat heute im Rahmen einer Pressemitteilung den Veröffentlichungstermin der Download-Version des Aufbauspiels <a href="http://www2.citiesxl.com/index.php?rub=citiesxl2011" target="_blank">Cities XL 2011</a> verkündet. Dabei wurde auch der Termin der Version die im Handel verkauft wird veröffentlicht. Demnach erscheint die Download-Version bereits am 14. Oktober 2010.</p> '
featured_image: /wp-content/uploads/2010/10/citysxl.jpg

---
Ungefähr drei Wochen später erscheint dann die Retail-Version im Handel. <a href="http://www2.citiesxl.com/index.php?rub=citiesxl2011" target="_blank">Cities XL 2011</a> übernehmen sie die Stadtplanung einer Metropole. Mit mehr als 700 Gebäuden und einzigartigen Bauwerken sowie 47 Arten von bebaubaren Karten möchte der zweite Teil von Cities XL überzeugen.

 

   

* * *

   



 