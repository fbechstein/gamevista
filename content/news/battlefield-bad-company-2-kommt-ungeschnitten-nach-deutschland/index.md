---
title: 'Battlefield: Bad Company 2 – Kommt ungeschnitten nach Deutschland'
author: gamevista
type: news
date: 2010-02-16T21:17:12+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Sehr gute Nachrichten für alle Fans des Ego-Shooters <a href="http://www.badcompany2.ea.com/" target="_blank">Battlefield: Bad Company 2</a>. Wie der Publisher <strong>Electronic Arts</strong> nun offiziell verlauten lassen hat, wird der Titel in Deutschland komplett ungeschnitten auf den Markt kommen. Die Unterhaltungssoftware Selbstkontrolle, kurz USK, stufte <strong>Bad Company 2</strong> mit „keine Jugendfreigabe“ ein.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Damit wird das Spiel ausschließlich dem erwachsenen Leser vorbehalten bleiben. Einen ersten Eindruck der PC-Beta, könnt ihr in unserer demnächst veröffentlichten Preview zu <a href="http://www.badcompany2.ea.com/" target="_blank">Battlefield: Bad Company 2</a> erhalten.

 

* * *



* * *