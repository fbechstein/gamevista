---
title: Brink – Entwickler zeigen bewegtes Bildmaterial
author: gamevista
type: news
date: 2009-12-04T19:12:23+00:00
excerpt: '<p>[caption id="attachment_1143" align="alignright" width=""]<img class="caption alignright size-full wp-image-1143" src="http://www.gamevista.de/wp-content/uploads/2009/12/brink-game.jpg" border="0" alt="Brink" title="Brink" align="right" width="140" height="100" />Brink[/caption]Der Entwickler <a href="http://www.brinkthegame.com" target="_blank">Bethesda</a> hat heute gleich drei neue Videos zum Multiplayer-Shooter <strong>Brink </strong>veröffentlicht. Die drei Videos mit insgesamt 14 Minuten Laufzeit zeigen Spielszenen, aus einem Level, das in einem Containerhafen angesiedelt ist.  Kommentiert wird das Ganze von den Entwicklern mit englischen Kommentaren.</p> '
featured_image: /wp-content/uploads/2009/12/brink-game.jpg

---
Neben der schicken Grafik, wird das Interface und auch eine Selbstschussanlage gezeigt. Diese kann man frei im Level platzieren.

[> Zum Brink &#8211; Container City Walkthrough Video 1][1]

[> Zum Brink &#8211; Container City Walkthrough Video 2][2]

[> Zum Brink &#8211; Container City Walkthrough Video 3][3]

 

* * *



* * *

 

 [1]: videos/item/root/brink-container-city-trailer-teil-1
 [2]: videos/item/root/brink-container-city-trailer-teil-2
 [3]: videos/item/root/brink-container-city-trailer-teil-3