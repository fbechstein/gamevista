---
title: 'Call of Duty: Modern Warfare 2 – Weiteres Mappack angekündigt'
author: gamevista
type: news
date: 2010-05-07T19:49:28+00:00
excerpt: '<p>[caption id="attachment_1772" align="alignright" width=""]<img class="caption alignright size-full wp-image-1772" src="http://www.gamevista.de/wp-content/uploads/2010/05/smalleisig.jpg" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Nachdem das Stimulus-Mappack für den Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare 2</a> seit kurzem verfügbar ist, kündigte der Publisher <strong>Activision</strong> heute den nächsten Zusatzinhalt an. Derzeit gibt es aber nur wenige Informationen. So soll der nächste DLC in der zweiten Hälfte dieses Jahres erscheinen.</p> '
featured_image: /wp-content/uploads/2010/05/smalleisig.jpg

---
Über den Inhalt ist bisher nichts bekannt.  Eine konkrete Preisangabe gibt es schon, denn dieser wird wieder um die 15 US Dollar betragen.

 

 

   

* * *

* * *