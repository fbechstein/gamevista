---
title: 'Battlefield: Bad Company 2 – Demo für Xbox 360 und PlayStation 3 erschienen'
author: gamevista
type: news
date: 2010-02-04T08:27:59+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Die Beta für den Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> läuft nun schon seit einiger Zeit. Für PC ist sie erst kürzlich erschienen und lässt ausgiebiges Testen zu. Nun hat der Publisher <strong>Electronic Arts</strong> die Demo zum Spiel veröffentlicht. Wer also eine der beiden genannten Konsolen sein eigen nennt, kann ab sofort die Demo herunterladen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Während Xbox 360-Besitzer nur einen <a href="http://marketplace.xbox.com/de-DE/games/offers/0ddf0001-0000-4000-8000-0000454188ab?cid=majornelson&#038;partner=majornelson" target="_blank">Xbox Live Gold- Acount</a> benötigen, müssen PlayStation 3 Fans leider einen Download-Code ergattern. Diesen kann man nun bei der **Electronic Sports League** gewinnen. Die Online-Liga verlost ca. 10.000 dieser Keys, mit welchem ihr in den Genuss der Online-Demo des Kriegsshooters von **DICE** kommt. <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> erscheint am 04. März 2010 im deutschen Handel.

> <a href="http://www.esl.eu/de/bfbc2/demo" target="_blank">Zur ESL Verlosung</a>

 

* * *



* * *