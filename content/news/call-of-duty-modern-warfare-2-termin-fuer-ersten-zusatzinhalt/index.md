---
title: 'Call of Duty: Modern Warfare 2 – Termin für ersten Zusatzinhalt'
author: gamevista
type: news
date: 2010-03-10T22:58:22+00:00
excerpt: '<p>[caption id="attachment_1540" align="alignright" width=""]<img class="caption alignright size-full wp-image-1540" src="http://www.gamevista.de/wp-content/uploads/2010/03/smallhelis.jpg" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Community Manager Robert Bowling, vom Entwicklerteam <strong>Infinity Ward</strong>, bestätigte nun den offiziellen Termin für den ersten Download-Inhalt zum Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare 2</a>. Via <a href="http://twitter.com/fourzerotwo/statuses/10245410129" target="_blank">Twittermeldung</a> wurde nun der Termin auf auf Ende März eingeschränkt. Dann wird eine 30 Tage exklusiv Edition des DLC, für Xbox 360, über Xbox-Live veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/03/smallhelis.jpg

---
Darauf folgend wird der Download-Content für die PC- bzw. PlayStation 3-Systeme freigegeben. Somit ist für PC und PlayStation 3 ein Release Ende April denkbar. Leider gibt es bisher keine Details zu den Inhalten. Wir halten euch auf dem Laufenden.

 

**  
** 

   

* * *

   



 