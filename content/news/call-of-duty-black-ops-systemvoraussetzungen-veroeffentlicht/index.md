---
title: 'Call of Duty: Black Ops – Systemvoraussetzungen veröffentlicht'
author: gamevista
type: news
date: 2010-10-20T16:53:57+00:00
excerpt: '<p>[caption id="attachment_1747" align="alignright" width=""]<img class="caption alignright size-full wp-image-1747" src="http://www.gamevista.de/wp-content/uploads/2010/05/callofduty7.jpg" border="0" alt="Call of Duty: Black Ops" title="Call of Duty: Black Ops" align="right" width="140" height="100" />Call of Duty: Black Ops[/caption]Publisher <strong>Activision </strong>gab heute die Systemvoraussetzungen für den Ego-Shooter <a href="http://www.callofduty.com" target="_blank">Call of Duty: Black Ops</a> bekannt. Dabei wurden bisher nur die Mindestanforderungen veröffentlicht. Euer PC sollte demnach mindestens einen Intel Dual Core E6600 oder AMD Phenom X3 8750 betreiben und eine Grafikkarte mit Shadertechnik 3.0, NVIDIA Geforce 8600GT bzw. ATI Radeon X1950 Pro, besitzen.</p> '
featured_image: /wp-content/uploads/2010/05/callofduty7.jpg

---
Als Arbeitsspeicher werden 2 Gigabyte Ram benötigt. <a href="http://www.callofduty.com/" target="_blank">Call of Duty: Black Ops</a> erscheint am 09. November 2010 für PC, Xbox 360 und PlayStation 3.

 

Die Mindestanforderungen im Überblick:

**Mindestanforderungen:**

  * Betriebssystem: Windows XP / Vista / 7
  * Prozessor: Intel Core 2 Duo E6600 oder AMD Phenom X3 8750 oder besser
  * Grafikkarte: Shader 3.0 oder besser (256 MB NVIDIA GeForce 8600GT / ATI Radeon X1950 Pro)
  * Arbeitsspeicher: 2 GB Ram
  * Festplattenspeicher: 12 GB
  * Soundkarte: DirectX 9.0c-kompatibel
  * DirectX: 9.0c

   

* * *

   

