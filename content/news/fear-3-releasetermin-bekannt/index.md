---
title: F.E.A.R. 3 – Releasetermin bekannt
author: gamevista
type: news
date: 2010-10-28T20:01:35+00:00
excerpt: '<p>[caption id="attachment_1670" align="alignright" width=""]<img class="caption alignright size-full wp-image-1670" src="http://www.gamevista.de/wp-content/uploads/2010/04/fear3.jpg" border="0" alt="F.E.A.R. 3" title="F.E.A.R. 3" align="right" width="140" height="100" />F.E.A.R. 3[/caption]Publisher <strong>Warner Bros.</strong> hat das Veröffentlichungsdatum für den dritten Teil von F.E.A.R. bekannt gegebene. Der schon mehrmals verschobene Titel wird demnach am 22. März 2011 für PC, PlayStation 3 und Xbox 360 in den USA und 25. März 2011 in Europa.</p> '
featured_image: /wp-content/uploads/2010/04/fear3.jpg

---
Wichtigste Neuerung dürfte der Koop-Modus sein, der bis zu zwei Spielern die Möglichkeit bieten wird, zusammen gegen die Armacham Corporation vorzugehen. Beide Spieler können nun Alma\`s Söhne Point Man und Paxton Fettel steuern. Point Man ist ein genetisch modifizierter Super-Soldat der aus dem ersten Teil bekannt sein dürfte. Paxton Fettel benutzt hingegen seine telekinetischen Fähigkeiten.

 

   

* * *

   

