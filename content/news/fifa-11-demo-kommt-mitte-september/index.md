---
title: FIFA 11 – Demo kommt Mitte September
author: gamevista
type: news
date: 2010-08-25T08:11:22+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="FIFA 11" title="FIFA 11" align="right" width="140" height="100" />FIFA 11[/caption]Publisher <strong>Electronic Arts</strong> hat den Termin für die Demo von <a href="http://www.ea.com/de/spiele/fifa" target="_blank">FIFA 11</a> veröffentlicht. Noch bevor das fertige Spiel am 30. September 2010 in den Handel kommt, wird Mitte September die Demoversion den Weg auf die heimischen Systeme finden. Am 15. September 2010 dürfen PlayStation 3- Besitzer das Fussballspiel ausgiebig testen.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Einen Tag später, am 16. September, kommen dann Xbox 360 und PC-Fans in den Genuss von <a href="http://www.ea.com/de/spiele/fifa" target="_blank">FIFA 11</a>. Zum Inhalt äußerte sich **EA** nicht näher. Es ist aber davon auszugehen, dass die Demoversion die bereits auf der gamescom 2010 zu sehen war, auch am 15. September seinen Einsatz finden wird. Die spielbaren Mannschaften wären der FC Basrcelona, Real Madrid, Bayer 04 Leverkusen, FC Chelsea, Olympique Lyon oder Juventus Turin.

 

   

* * *

   



 