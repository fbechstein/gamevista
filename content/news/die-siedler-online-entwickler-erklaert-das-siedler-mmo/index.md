---
title: Die Siedler Online – Entwickler erklärt das Siedler-MMO
author: gamevista
type: news
date: 2010-08-05T18:02:15+00:00
excerpt: '<p>[caption id="attachment_2098" align="alignright" width=""]<img class="caption alignright size-full wp-image-2098" src="http://www.gamevista.de/wp-content/uploads/2010/08/diesiedleronline.jpg" border="0" alt="Die Siedler Online" title="Die Siedler Online" align="right" width="140" height="100" />Die Siedler Online[/caption]Erstmals wagt das deutsche Entwicklerstudio <strong>Blue Byte</strong> den Schritt in Richtung Browserspiel-Umsetzung von der mittlerweile siebenteiligen Die Siedler-Reihe. Passend dazu erklären die Entwickler im Ubisoft-TV-Trailer die Funktionen und Features des Spiels.</p> '
featured_image: /wp-content/uploads/2010/08/diesiedleronline.jpg

---
<a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Die Siedler Online</a> wird komplett kostenlos ohne jegliche Installation spielbar sein. Allerdings wird es möglich sein, wie bei vielen anderen kostenlosen Onlinespielen, gegen geringe Geldbeträge das Spielgeschehen etwas angenehmer zu machen. So könnt ihr über einen Shop z.B. neue Spielgegenstände erstehen. Laut **Ubisoft** sollen die Vorteile bzw. Gebäude oder Items aber auch mit reiner Spielzeit freigeschalten werden können.

Einen genauen Veröffentlichungstermin gibt es bisher nicht. Angepeilt ist der Release für Ende des Jahres.

[> Zum Die Siedler Online &#8211; Entwickler-Trailer][1]

   

* * *

   



 [1]: videos/item/root/die-siedler-online-entwickler-trailer