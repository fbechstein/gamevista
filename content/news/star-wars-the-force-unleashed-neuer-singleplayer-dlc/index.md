---
title: 'Star Wars: The Force Unleashed – Neuer Singleplayer DLC'
author: gamevista
type: news
date: 2009-08-28T17:11:14+00:00
excerpt: '<p>[caption id="attachment_233" align="alignright" width=""]<img class="caption alignright size-full wp-image-233" src="http://www.gamevista.de/wp-content/uploads/2009/07/swu_small.png" border="0" alt="Star Wars: The Force Unleashed" title="Star Wars: The Force Unleashed" align="right" width="140" height="100" />Star Wars: The Force Unleashed[/caption]<a href="http://www.activision.com">Publisher Activisio</a>n verkündete heute  per Pressemitteilung das ein neuer Download Level für das Spiel<strong> Star Wars: The Force Unleashed</strong> bereit steht. Die neue Mission knüpft an das Ende des Hauptspiels an und führt die Star Wars-Saga in eine ganz neue Richtung: Der dunkle Schüler hat sich seines einstigen Lehrmeisters Darth Vader entledigt und eine mächtige Position an der Seite des Imperators eingenommen.</p> '
featured_image: /wp-content/uploads/2009/07/swu_small.png

---
 

Der böse Herrscher entsendet seinen neuen Schützling auf den Planeten Tatooine, wo sich Obi-Wan Kenobi versteckt hält. Auf der Suche nach dem flüchtigen Jedi erkunden die Spieler in der Rolle des dunklen Schülers berühmte Schauplätze wie den Palast von Jabba the Hutt und treffen auf den berüchtigten Kopfgeldjäger Boba Fett.   
Das Downloadpaket ist ab sofort für Xbox 360 und PlayStation 3 verfügbar.

* * *



* * *

 