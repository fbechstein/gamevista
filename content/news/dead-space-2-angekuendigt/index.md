---
title: Dead Space 2 angekündigt
author: gamevista
type: news
date: 2009-12-07T16:26:27+00:00
excerpt: '<p>[caption id="attachment_1151" align="alignright" width=""]<img class="caption alignright size-full wp-image-1151" src="http://www.gamevista.de/wp-content/uploads/2009/12/deadspace2.jpg" border="0" alt="Dead Space 2" title="Dead Space 2" align="right" width="140" height="100" />Dead Space 2[/caption]Das Gerücht gibt es schon sehr lange und es wurde einiges gemunkelt. Nun hat <a href="http://www.electronic-arts.de/" target="_blank"><strong></strong>EA</a> <strong>Dead Space 2</strong> für PC, Xbox360 und PS3 angekündigt. Was aus einer Wii-Version werden soll ist noch unbekannt.</p> '
featured_image: /wp-content/uploads/2009/12/deadspace2.jpg

---
Erst vor kurzem gab <a href="http://www.electronic-arts.de/" target="_blank">EA</a> bekannt, dass man über 1500 Mitarbeiter entlässt. Kurz danach verkündete man, dass man im Jahr 2010 nur noch etwas 30 Spiele veröffentlichen will. Zuletzt waren es über 50 in 2009 gewesen. Eventuell ist dieser Kürzung auch die Wii-Version von **Dead Space 2** zum Opfer gefallen.

Der Veröffentlichungstermin von **Dead Space 2** ist noch nicht bekannt.

 







 