---
title: Battlefield Heroes – Update Heroes of the Fall Released
author: gamevista
type: news
date: 2009-09-30T13:41:44+00:00
excerpt: '<p>[caption id="attachment_774" align="alignright" width=""]<img class="caption alignright size-full wp-image-774" src="http://www.gamevista.de/wp-content/uploads/2009/09/battlefieldheroes.jpg" border="0" alt="Battlefield Heroes" title="Battlefield Heroes" align="right" width="140" height="100" />Battlefield Heroes[/caption]Publisher <a href="http://www.electronic-arts.de">Electronic Arts</a> hat vor kurzem das neueste Update für den Multiplayer Shooter <strong>Battlefield Heroes</strong> veröffentlicht. Der Patch trägt den Namen <em>Heroes of the Fall </em>und bietet unter anderem eine neue Karte "Riverside Rush" an. Weiterhin werden zahlreiche Bugs beseitigt und neue Items eingeführt.</p> '
featured_image: /wp-content/uploads/2009/09/battlefieldheroes.jpg

---
Den Download des Updates könnt ihr auf der offiziellen [Website][1] starten. Die Liste der Änderungen sind [hier][2] zu finden.

 

 

* * *



* * *

 

 [1]: http://www.battlefieldheroes.com/
 [2]: http://www.battlefieldheroes.com/forum/showthread.php?tid=66712