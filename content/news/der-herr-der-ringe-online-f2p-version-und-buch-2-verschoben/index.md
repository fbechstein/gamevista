---
title: Der Herr der Ringe Online – F2P-Version und Buch 2 verschoben
author: gamevista
type: news
date: 2010-09-09T15:53:25+00:00
excerpt: '<p>[caption id="attachment_2206" align="alignright" width=""]<img class="caption alignright size-full wp-image-2206" src="http://www.gamevista.de/wp-content/uploads/2010/09/lotro_small.jpg" border="0" alt="Der Herr der Ringe Online" title="Der Herr der Ringe Online" align="right" width="140" height="100" />Der Herr der Ringe Online[/caption]Entwickler <strong>Turbine </strong>teilte heute zum Leidwesen aller Fans des Online-Rollenspiels <a href="http://www.lotro-europe.com/?territory=German" target="_blank">Der Herr der Ringe Online</a> mit, dass die Free-to-Play-Variante definitiv nicht am morgigen Freitag erscheinen wird. Sicherlich hatten sich einige Interessenten des J.R.R Tolkiens-Spiel den 10. September 2010 rot im Kalender angekreuzt.</p> '
featured_image: /wp-content/uploads/2010/09/lotro_small.jpg

---
Einen neuen Termin nannten die Entwickler bisher nicht. Grund für die Verschiebung sollen die Server sein, die bisher noch nicht ausreichend Kapazität besitzen um den Ansturm Herr zu werden.

 

   

* * *

   

