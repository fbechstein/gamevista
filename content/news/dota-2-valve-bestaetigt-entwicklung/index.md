---
title: DotA 2 – Valve bestätigt Entwicklung
author: gamevista
type: news
date: 2010-10-14T20:18:39+00:00
excerpt: '<p>[caption id="attachment_2344" align="alignright" width=""]<img class="caption alignright size-full wp-image-2344" src="http://www.gamevista.de/wp-content/uploads/2010/10/dota2.jpg" border="0" alt="DotA 2" title="DotA 2" align="right" width="140" height="100" />DotA 2[/caption]Entwickler und Publisher <strong>Valve </strong>wird den zweiten Teil des beliebten Warcraft 3-Mods <em>Defense of the Ancients</em> voraussichtlich im nächsten Jahr veröffentlichen. Zusammen mit IceFrog wird der Titel für PC und Mac erscheinen. Laut ersten Informationen wird sich der der zweite Teil im Spieledesign nicht großartig vom Ersten unterscheiden.</p> '
featured_image: /wp-content/uploads/2010/10/dota2.jpg

---
Elemente wie Items, Upgrades und Skills und die beliebte Karte werden sich nah am ersten Teil von DotA orientieren. Natürlich dürfen die mehr als 100 einzigartigen Helden nicht fehlen. Als Grafikengine wird die allseits bekannte Source-Engine zum Einsatz kommen. Als Neurungen sind ein Coaching-System, ein verbessertes Matchmaking und Computergesteuerte Gegner, die beim Verlassen des laufenden Spiels die Einheiten übernehmen, geplant.

 

   

* * *

   

