---
title: 'Die Sims 3: Reiseabenteuer – Update 2.3.33 erschienen'
author: gamevista
type: news
date: 2009-12-17T17:52:51+00:00
excerpt: '<p>[caption id="attachment_204" align="alignright" width=""]<img class="caption alignright size-full wp-image-204" src="http://www.gamevista.de/wp-content/uploads/2009/07/sims_small.png" border="0" alt="Die Sims 3" title="Die Sims 3" align="right" width="140" height="100" />Die Sims 3[/caption]Zur Lebenssimulation <strong>Die Sims 3: Reiseabenteuer</strong> hat der Publisher <a href="http://www.de.thesims3.com" target="_blank">Electronic Arts</a> einen neuen Patch veröffentlicht. Das äußerst umfangreiche Update, für das Addon „Reiseabenteuer“, behebt diverse Fehler und ändert ein paar Einstellungen im Spiel.</p> '
featured_image: /wp-content/uploads/2009/07/sims_small.png

---
Die komplette Liste der Änderungen findet ihr auf der [zweiten Seite][1].  
[  
> Zum Die Sims 3: Reiseabenteuer &#8211; Patch 2.3.33 Download][2]

* * *



* * *

</p> 

<span style="font-size: x-small;"><img class="caption alignright size-full wp-image-204" src="http://www.gamevista.de/wp-content/uploads/2009/07/sims_small.png" border="0" alt="Die Sims 3" title="Die Sims 3" align="right" width="140" height="100" /><span class="contentpane"><span style="font-size: small;">Die Sims 3</span><br /></span></span>

 <span class="contentpane"></p> 

<li>
  <div>
    Ingame-Store: Spieler können jetzt über den Ingame-Store-Browser des Kauf-Modus auf Store-Inhalte zugreifen.
  </div>
</li>

<li>
  Deinstallationsprogramm für Mac: Mit dem Deinstallationsprogramm können Mac-Benutzer ganz einfach alle Anwendungen, Inhalte und Erweiterungspacks von Die Sims 3 deinstallieren.
</li>
<li>
  Eine automatische Aufforderung wurde hinzugefügt, mit der gespeicherte Spiele wiederhergestellt werden können, die von &#8222;Fehlercode 16&#8220; beschädigt wurden.
</li>
<li>
  Wurde der aktive Haushalt gewechselt, hängte sich das Spiel manchmal auf. Dieses Problem wurde behoben.
</li>
<li>
  Die Lebensdauer der Sims wird nicht mehr geändert, wenn die Interaktionen &#8222;Aussehen ändern&#8220; oder &#8222;Outfit planen&#8220; ausgeführt werden.
</li>
<li>
  Über die Taste &#8222;C&#8220; können nun Fotos mit dem Handy aufgenommen werden.
</li>
<li>
  Im Kühlschrank gelagerte Lebensmittel verderben nun langsamer.
</li>
<li>
  Eigene Geschäfte bringen jetzt wieder das korrekte Einkommen ein.
</li>
<li>
  Schwimmende Sims gehen nicht mehr über das Wasser, nachdem Die Sims™ 3 aktualisiert wurde.
</li>
<li>
  Nach der Aktualisierung wird benutzerdefinierte Lieblingsmusik nicht mehr zu französischer Lieblingsmusik geändert.
</li>
<li>
  Der &#8222;Lieblingsmusik&#8220;-Cheat wurde verbessert.
</li>
<li>
  Die Hintergrundmusik beginnt nicht mehr von vorn, wenn zwischen Bau- und Kauf-Modus umgeschaltet wird.
</li>
<li>
  Das Zypressenmodell auf niedrigem Detail-Level wurde verfeinert.
</li>
<li>
  Mischten Sims den Inhalt von Schüsseln, färbte sich dieser blau. Dieses Problem wurde behoben.
</li>
<li>
  Mac: Die Stabilität beim Spielen von Die Sims™ 3 mit der Nvidia 7300-Serie wurde erhöht.
</li>
<li>
  Geister suchen jetzt auch Zimmer ohne Türen heim.
</li>
<li>
  Beim Bau eines Zimmers um einen NPC-Besucher herum treten nun keine Fehler mehr auf.
</li>
<li>
  Die geschätzte Dauer für die Interaktionen &#8222;Schlafen&#8220; und &#8222;Trainieren&#8220; ist nun viel genauer.
</li>
<li>
  Die Interaktionswahl wird nun richtig ausgeführt, wenn sie vor Beginn der Interaktion getroffen wurde.
</li>
<li>
  Der Testcheat &#8222;Objekt löschen&#8220; wurde hinzugefügt.
</li>
<li>
  Mac: Spieler können mit dem Launcher keine Datei-Erweiterungen mehr ändern, da dies wiederholt zu Schwierigkeiten beim Hochladen führte.
</li>
<li>
  Mac: Der Computer stürzt nicht mehr ab, wenn sich der Spieler schnell durch das Installationsprogramm klickt.
</li>
<li>
  Wenn bei gewähltem Gelände-Anstrich löschen-Tool auf Rückgängig geklickt wird, wird nicht mehr automatisch ein Gelände-Anstrich-Pinsel gewählt.
</li>
<li>
  Wenn unverheiratete Sims ein Baby bekommen, wird in der Beziehungsleiste der richtige Beziehungsstatus angezeigt.
</li>
<li>
  Gartenzwerge können jetzt im Live-Modus auf Gemeinschaftsgrundstücke gezogen werden.
</li>
<li>
  Auf Müllhaufen wird jetzt korrekt reagiert, wenn das Grundstück, auf dem sie sich befanden, über &#8222;In Spiel bearbeiten&#8220; exportiert und ersetzt wurde.
</li>
<li>
  Ein kleinerer Fehler wurde behoben, der auftreten konnte, wenn ein Gast nicht begrüßt wurde, bevor er wieder gehen musste.
</li>
<li>
  Ein seltenes Problem wurde behoben, bei dem das Spiel beim Beenden abstürzen konnte.
</li>
<li>
  Der Computer stürzt nicht mehr ab, nachdem besonders unebenes Gelände begradigt wurde.
</li>
<li>
  Die Stabilität bei längerem Spielen wurde verbessert.
</li>
<li>
  In Kellern können keine unlöschbaren Wände mehr gebaut werden.
</li>
<li>
  Der &#8222;Nachbauen&#8220;-Cheat ist für Sims und Wände nicht mehr aktiv.
</li>
<li>
  Mac: Jetzt kann immer nur jeweils ein Launcher-Fenster geöffnet werden.
</li>
<li>
  Beim Öffnen des Cheat-Fensters, während im &#8222;In Spiel bearbeiten&#8220;-Modus ein Grundstück gedreht wurde, hängte sich der Computer manchmal auf. Dieses Problem wurde behoben.
</li>
<li>
  Die Stabilität wurde verbessert, wenn für längere Zeit mit Geschwindigkeit 3 gespielt wird.
</li>
<li>
  <div>
    Mit dem &#8222;Zu aktiver Familie hinzufügen&#8220;-Cheat können jetzt nur noch bis zu acht Sims zu einem Haushalt hinzugefügt werden. Haushalte mit mehr als acht Sims gefährden die Stabilität des Spiels.
  </div>
</li>

<p>
  </span> </ul> 
  
  <p class="contentpane">
    Reiseabenteuer:
  </p>
  
  <ul>
    <span class="contentpane"> </p> 
    
    <li>
      <div>
        Ein seltener Fehler wurde behoben, der auftreten konnte, wenn ein Sim sich bei der Ankunft eines ausländischen Besuchers weit weg von zu Hause befand.
      </div>
    </li>
    
    <li>
      <div>
        In der Gesetzeshüter-Karriere besteht jetzt eine größere Chance, dass eine Gelegenheit zum Erlernen der Kampfkunstfähigkeit entsteht.
      </div>
    </li>
    
    <li>
      <div>
        Die Gelegenheit &#8222;Eine unbezahlbare Wette&#8220; lässt sich nun einfacher abschließen.
      </div>
    </li>
    
    <li>
      <div>
        Ein seltenes Problem wurde behoben, bei dem ein Sim zurückgesetzt werden konnte, während er anderen Sims beim Gebrauch des Beschwörungskorbes für Schlangen zusah.
      </div>
    </li>
    
    <li>
      <div>
        Grabinschriften bleiben jetzt nach der Reise bestehen.
      </div>
    </li>
    
    <li>
      <div>
        Im Exchange online gestellte benutzerdefinierte Gemälde verlieren nicht mehr ihre Textur, wenn die Grundstücke wieder importiert werden.
      </div>
    </li>
    
    <li>
      <div>
        Ein kleiner Fehler wurde behoben, der auftrat, wenn ein Sim mit einer Urne im Inventar verreiste.
      </div>
    </li>
    
    <li>
      <div>
        Die Gelegenheit &#8222;Tiberium besorgen&#8220; erfordert jetzt eine realistischere Menge an Tiberium.
      </div>
    </li>
    
    <li>
      <div>
        Mit der Interaktion &#8222;Joggen&#8220; werden nun keine spielergesteuerten Mumien mehr verspottet.
      </div>
    </li>
    
    <li>
      <div>
        Die Stabilität bei längerem Spielen wurde verbessert.
      </div>
    </li>
    
    <li>
      <div>
        Sims knuddeln in Zelten nicht mehr mit falschen Partnern.
      </div>
    </li>
    
    <li>
      <div>
        Die Beziehungsanforderungen für Techtelmechtel im Sarkophag sind nun strenger.
      </div>
    </li>
    
    <li>
      <div>
        Für Kinder ist jetzt die Interaktion &#8222;Treppe nehmen&#8220; verfügbar.
      </div>
    </li>
    
    <li>
      <div>
        Jähzornige Sims flippen jetzt bei Gefahr aus.
      </div>
    </li>
    
    <li>
      <div>
        Werden Objekte in Grüften wieder in Besitz genommen, wird an deren Stelle kein Ersatzobjekt mehr erstellt.
      </div>
    </li>
    
    <li>
      <div>
        Die Dauer der Stimmung &#8222;Bewundernswerte Sammlung&#8220; schwankt nicht mehr.
      </div>
    </li>
    
    <li>
      <div>
        Ein Problem wurde behoben, bei dem die Grabsteine ausländischer Sims bei der Rückkehr in ihre heimatliche Nachbarschaft aus dem Inventar verschwanden.
      </div>
    </li>
    
    <li>
      <div>
        Mac: Eine Sicherheitsvorkehrung wurde hinzugefügt, so dass der Launcher während der Aktualisierung inaktiv ist.
      </div>
    </li>
    
    <li>
      <div>
        Jetzt diskutieren Mumien die Vor- und Nachteile des Mumiedaseins.
      </div>
    </li>
    
    <li>
      <div>
        Mumien können keine Lieder mehr lernen. Ihre Stimmbänder sind verkümmert, und sie sind somit stumm.
      </div>
    </li>
    
    <li>
      <div>
        Bei Sims bleiben keine Nachwirkungen zurück, wenn sie vom Mumiendasein geheilt wurden.
      </div>
    </li>
    
    <li>
      <div>
        In Kellern können keine unlöschbaren Wände mehr gebaut werden.
      </div>
    </li>
    
    <li>
      <div>
        Der Launcher gibt nun besser an, dass für Reiseabenteuer-Inhalte Reiseabenteuer vorhanden sein muss.
      </div>
    </li>
    
    <li>
      <div>
        Die Geister von Sims, die ihr Leben an den Fluch einer Mumie verloren haben, weisen jetzt eine angemessene Partikel-Aura auf.
      </div>
    </li>
    
    <li>
      <div>
        Der &#8222;Nachbauen&#8220;-Cheat ist für Sims und Wände nicht mehr aktiv.
      </div>
    </li>
    
    <li>
      <div>
        Mac: Jetzt kann immer nur jeweils ein Launcher-Fenster geöffnet werden.
      </div>
    </li>
    
    <li>
      <div>
        Beim Öffnen des Cheat-Fensters, während im &#8222;In Spiel bearbeiten&#8220;-Modus ein Grundstück gedreht wurde, hängte sich der Computer manchmal auf. Dieses Problem wurde behoben.
      </div>
    </li>
    
    <li>
      <div>
        Ein Fehler wurde behoben, der die Beziehung zwischen einem Sim und einem schwangeren Geist auf Reisen aufheben konnte.
      </div>
    </li>
    
    <li>
      <div>
        Der Sichtbarkeitsstatus für Fallen wird jetzt richtig importiert und exportiert (verborgen oder sichtbar).
      </div>
    </li>
    
    <li>
      <div>
        Die Stabilität wurde verbessert, wenn für längere Zeit mit Geschwindigkeit 3 gespielt wird.
      </div>
    </li>
    
    <li>
      <div>
        Spielbare Geister bleiben nicht mehr stecken, wenn sie ein geschlechtsspezifisches Badezimmer betreten.
      </div>
    </li>
    
    <li>
      <div>
        Ein Speicherleck wurde behoben, das bei Reisen mit unterschiedlichen Sims auftrat.
      </div>
    </li>
    
    <li>
      <div>
        Beziehungen werden jetzt richtig aktualisiert, wenn sich ein Paar trennt, während einer von beiden auf Reisen ist.
      </div>
    </li>
    
    <li>
      <div>
        Sims geraten nicht mehr in nicht routbare Bereiche, wenn lange Strecken auf chinesischen Straßen abgebrochen werden.
      </div>
    </li>
    
    <li>
      <div>
        Sims bleiben nicht mehr stecken, wenn sie in den unterirdischen Räumen der Gruft von Abu Simbel von versteckten Fallen erwischt werden.
      </div>
    </li>
    
    <li>
      <div>
        Logikprozessor-Gruft-Objekte behalten jetzt beim Import und Export alle Einstellungen bei.
      </div>
    </li>
    
    <li>
      <div>
        Fackelwandhebel bewegen sich jetzt, wenn sie betätigt werden.
      </div>
    </li>
    
    <li>
      <div>
        Der &#8222;Alterung&#8220;-Cheat kann nicht mehr an Reisezielen angewandt werden.
      </div>
    </li>
    
    <li>
      <div>
        Sims erhalten nicht mehr den Wunsch, Nektar mit Zwiebeln herzustellen.
      </div>
    </li>
    
    <li>
      <div>
        Ein kleiner Fehler wurde behoben, der beim Verbrennen von Mumien auftrat.
      </div>
    </li>
    
    <li>
      <div>
        An Reisezielen ohne Marktplatz erscheinen keine NPC-Händler mehr.
      </div>
    </li>
    
    <li>
      <div>
        Die Kartenmarkierung eines abreisenden Sims bleibt nicht mehr erhalten, nachdem er das Reiseziel verlassen hat.
      </div>
    </li>
    
    <li>
      <div>
        An Reisezielen kann nur noch jeweils ein Basiscamp erstellt werden.
      </div>
    </li>
    
    <li>
      <div>
        Die Interaktion &#8222;Auf Bett plaudern&#8220; steht Sims in Zelten nicht mehr zur Verfügung.
      </div>
    </li>
    
    <li>
      <div>
        Ein Problem wurde behoben, bei dem Sims in einem Sarkophag stecken bleiben konnten, wenn andere Sims vor dem Sarkophag Schlange standen.
      </div>
    </li>
    
    <li>
      <div>
        Mit dem &#8222;Zu aktiver Familie hinzufügen&#8220;-Cheat können jetzt nur noch bis zu acht Sims zu einem Haushalt hinzugefügt werden. Haushalte mit mehr als acht Sims gefährden die Stabilität des Spiels.
      </div>
    </li>
    
    <li>
      <div>
        Der Spieler wird jetzt über den Abbruch eines Abenteuers informiert, wenn ein anderer Sim mit den erforderlichen Voraussetzungen eingreift.
      </div>
    </li>
    
    <li>
      <div>
        Der Text in der Abenteuer-Anzeige für das Abenteuer &#8222;Ungeschmolzene Quecksilber-Stücke liefern&#8220; ist jetzt verständlicher.
      </div>
    </li>
    
    <li>
      <div>
        Die Verbesserung der Athletikfähigkeiten durch Schlafen im Königssarkophag wurde verringert.
      </div>
    </li>
    
    <li>
      <div>
        Versteckte Objekte werden nicht mehr sichtbar, wenn ein Grundstück importiert oder exportiert wird.
      </div>
    </li>
    
    <li>
      <div>
        Handy-Fotos, die auf einem Grundstück in der Tonne gespeichert werden, gehen bei der Aktualisierung nicht mehr verloren.
      </div>
    </li>
    
    <p>
      </span> </ul>
    </p>
    
    <hr />
    
    <p>
      
    </p>
    
    <hr />
    
    <p>
       
    </p>

 [1]: news/die-sims-3-reiseabenteuer-update-2-3-33-erschienen/seite-2
 [2]: downloads/patches/item/patches/die-sims-3-reiseabenteuer-patch-2333