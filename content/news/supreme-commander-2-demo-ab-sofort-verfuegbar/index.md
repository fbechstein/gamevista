---
title: Supreme Commander 2 – Demo ab sofort verfügbar
author: gamevista
type: news
date: 2010-02-25T17:29:34+00:00
excerpt: '<p>[caption id="attachment_1330" align="alignright" width=""]<img class="caption alignright size-full wp-image-1330" src="http://www.gamevista.de/wp-content/uploads/2010/01/supreme-commander-2.jpg" border="0" alt="Supreme Commander 2" title="Supreme Commander 2" align="right" width="140" height="100" />Supreme Commander 2[/caption]Zum Strategietitel <a href="http://www.supremecommander2.com/" target="_blank">Supreme Commander 2</a> hat der Publisher Square Enix am heutigen Tage die lang erwartete Demo veröffentlicht. Fans des ersten Teils, können nun die Probierversion des zweiten Teils über die Onlineplattform <a href="http://store.steampowered.com/app/40140/" target="_blank">Steam</a> downloaden.</p> '
featured_image: /wp-content/uploads/2010/01/supreme-commander-2.jpg

---
Neben einer Tutorialmission wird es erste Aufgaben für die United Earth Federation zu erfüllen geben.

 

* * *



* * *