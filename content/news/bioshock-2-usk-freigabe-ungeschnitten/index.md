---
title: Bioshock 2 – USK Freigabe ungeschnitten
author: gamevista
type: news
date: 2009-11-09T17:11:32+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Publisher <a href="http://www.bioshock2game.com" target="_blank">2K Games</a> hat heute im Rahmen einer Pressemitteilung ein sehr gute Nachricht für alle <strong>Bioshock 2</strong> Fans verkündet. So wird der Ego-Shooter aus dem Hause 2K Marin gänzlich ungeschnitten in die deutschen Läden kommen.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Vor kurzem hat die Unterhaltungssoftware Selbstkontrolle **Bioshock 2** mit einem „keine Jugendfreigabe“ Aufkleber versehen, deswegen werden nur erwachsene Spieler in den zensurfreien Genuss des Spiels kommen. Die Version des Spiels ist somit inhaltlich die Gleiche wie das amerikanische Pendant und wird am 9. Februar 2010 im Handel erscheinen.

 

<cite></cite>

* * *



* * *