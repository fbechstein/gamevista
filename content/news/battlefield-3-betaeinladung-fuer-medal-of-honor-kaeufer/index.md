---
title: Battlefield 3 – Betaeinladung für Medal of Honor-Käufer
author: gamevista
type: news
date: 2010-07-31T17:50:35+00:00
excerpt: '<p>[caption id="attachment_2071" align="alignright" width=""]<img class="caption alignright size-full wp-image-2071" src="http://www.gamevista.de/wp-content/uploads/2010/07/moh2010.jpg" border="0" alt="Medal of Honor" title="Medal of Honor" align="right" width="140" height="100" />Medal of Honor[/caption]Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Pressemitteilung angekündigt, dass Käufer der <strong>Medal of Honor</strong> Tier 1- und Limited Edition eine Einladung zur <strong>Battlefield 3</strong>-Beta erhalten werden.<br /> Die Tier 1-Edition von <strong>Medal of Honor</strong> wird exklusiv von Amazon und Gamestop vertrieben.</p> '
featured_image: /wp-content/uploads/2010/07/moh2010.jpg

---
Die Spezialedition bietet außerdem auch exklusiven Zugriff auf die MP7-Waffe. **Medal of Honor** wird am 14. Oktober 2010 für PlayStation 3, Xbox 360 und PC veröffentlicht. Wann die Beta von <a href="http://www.ea.com/de/spiele/battlefield" target="_blank">Battlefield 3</a> starten soll ist bisher nicht bekannt.

 

   

* * *

   

