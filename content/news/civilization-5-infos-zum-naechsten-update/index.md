---
title: Civilization 5 – Infos zum nächsten Update
author: gamevista
type: news
date: 2010-12-13T18:55:38+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Entwickler <strong>Firaxis Games</strong> hat weitere Details zum nächsten Patch für das Strategiespiel <a href="http://www.civilization5.com" target="_blank">Civilization 5</a> bekannt gegeben. Im <a href="http://forums.2kgames.com/showthread.php?99880-December-Patch-Full-Change-List" target="_blank">offiziellen Forum</a> des Publishers 2K Games hat jetzt der Community Manager weiter Informationen zum Update zusammengetragen. So soll am 16. Dezember 2010 zusammen mit einem neuen Download-Cotent der Patch veröffentlicht werden.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Das Update behebt unter anderem diverse Fehler und verbessert die KI der Computergegner. Weitere Informationen findet ihr im Überblick zu den Patchnotes:

 

<span style="font-size: medium;"><strong>[AI]</strong></span>

[TACTICAL AI]

  * Proper evaluation of which enemy units can reach my units next turn.
  * Sorting enemy targets (within a class) by damage.
  * Combine bombardment fire from cities with other ranged fire where possible.
  * Never target a city for a ranged attack when they are already at (or going to be at) 1 health.
  * Have the tactical AI be more aggressive about fortifying units that aren&#8217;t moving anyways.
  * Make all forms of guarding improvements the lowest priority tactical AI moves.
  * Don&#8217;t mark tiles adjacent to enemy at sea as good for flank attacks &#8212; there is no flanking at sea.
  * Update to tactical AI pillaging code &#8211; now prioritize enemy land resources and trade routes (never regular mines or farms). Always uses the check to make sure AI is not trying to pillage in an enemy dominance zone. Barbarians will still target everything.
  * Never use ranged units to provide flank bonuses.
  * Reduce chance of AI civs making &#8222;suicide&#8220; attacks.
  * Changes to better protect the capital or any city with an enemy within 5 tiles.

[VICTORY]

  * AI will be more aggressive about pursuing Diplo victory (bribing City States) if they are wealthy.
  * AI will be more aggressive about building Spaceship parts when going for Science victory.

[CIVS]

  * Tweak a few leader settings to be more likely to use their traits.
  * Adjust Napoleon to make more likely to go for culture.

[COMBAT/UNITS]

  * AI will not use Horse as defenders on hills as much.
  * AI will often build more defensive troops.
  * AI will more aggressively hunt barbs in the early game.
  * Slightly more naval units.
  * AI will now build ranged and mobile units more in line with the flavor settings for the team &#8211; in general, this means more mobile units.
  * AI now builds, deploys, and uses air units more effectively.
  * Allow AI to build more units if above Prince.
  * AI will be more likely to build and bring siege units in a city attack.
  * Better nuke targeting by AI.
  * Won&#8217;t build AA if no air threat.
  * Allow AI or automated human explorers to move to edge of sight range and then explore again.

[EXPLORATION/EXPANSION]

  * AI will emphasize getting an Ocean going explorer unit when the time comes.
  * AI slightly more likely to settle off home continent.
  * AI should colonize other continents regularly.
  * AI second wave expansion more aggressive.
  * Improve the AI&#8217;s chances of setting up protected bombard attacks.
  * Settlers: should handle watery maps better.
  * AI will grab goody huts on other land masses.
  * AI will grab empty barb camps more often.

[WORKERS/CITY AI]

  * Large Cities should be more willing to build happiness and gold buildings.
  * Workers prioritize repair builds higher than other builds.
  * AI will be more likely to build a wall on any city that was an original capital.
  * Builder tasking now calculates yields appropriately during golden ages and weighs tiles according to how much stuff they provide overall as well as their maximum yield of a certain type.
  * More likely to build up economy early.
  * Multiple worker AI improvements.

 

[MISC]

  * Factor GS into flavors more.
  * Disband obsolete units even if not losing money.
  * Upgrade units a bit more.
  * Tweak flavors of policies a bit.
  * Have AI factor Grand Strategy into picking policies.
  * AI will factor grand strategy into tech choices a bit more.
  * AI don&#8217;t send a barb expedition if defenses are critical.
  * AI less likely to pick a city on an inland sea for serious naval production.
  * Additional pathfinder optimization.

<span style="font-size: medium;"><strong>[GAMEPLAY]</strong></span>

[CITY/BUILDINGS]

  * Added &#8222;National Treasury&#8220; national wonder, which requires Markets in all cities. Provides +8 Gold per turn to the city in which it&#8217;s built.
  * Added “Circus Maximus” national wonder, which requires Colosseums in all cities.. Provides 5 Happiness.
  * Library now has no specialist slots.
  * Wat now has two specialist slots.
  * Public school now has 1 Science per pop, +1 free Great Scientist point, +1 Culture for 3 gold maintenance.
  * Observatory now has 1 specialist slot.
  * Research Lab has two specialist slots.
  * Public school now provides 1 beaker per pop for 3 gold maintenance.
  * Watermill now provides +2 good and +1 production for 2 gold maintenance.
  * Paper Maker now has no specialist slots.
  * Circus now has +2 happiness and no maintenance.
  * Theatre now has +5 happiness.
  * Stadium now has +5 happiness.
  * Reduced production cost and maintenance for the Courthouse.
  * Courthouse can now be purchased in a city (although it is expensive).
  * Removed maintenance from city defense buildings (Walls, Castle, Military Base).
  * City defense buildings now help cities heal.
  * Increased city strength ramp-up based on technology.
  * Reduced effects of Forbidden Palace and Meritocracy (Happiness per city).
  * Reduced amount of food needed for cities to grow at larger sizes.
  * Buildings can now no longer provide more Happiness than there is population in a city (wonders are excluded from this).
  * Ironworks now gives 10 hammers instead of a % bonus.
  * National College now gives +5 science in addition to the % bonus.
  * Hermitage gives 5 culture in addition to its previous bonus.
  * Raze/Unraze exploit fixed.
  * Cities being razed are unhappy about it (only during the razing process).
  * Cities heal more quickly.

[UNITS/PROMOTIONS]

  * Cavalry can now go obsolete with Combustion.
  * Stealth bombers cannot use carriers.
  * Only allow one upgrade per unit from a goody hut.
  * Add second embarkation promotion (&#8222;Defensive Embarkation&#8220;).
  * Amount of damage done during naval combat increased.
  * All melee horse units get penalty attacking cities.
  * Increased city attack penalty for mounted and armor units to 50% (from 40%).
  * Lancers (and Lancer UUs) upgrade to helicopter.
  * Lowered combat value of Horseman and Companion Cavalry.
  * Promotions must be picked the turn they&#8217;re earned.
  * Can no longer promote a unit that has fought during the turn (instead, the promotion comes up the beginning of the following turn).
  * Catapults and Trebuchets now weaker against units but stronger VS cities.
  * Reduced effectiveness of Archers & Crossbowmen (and their UUs) VS cities.
  * Reduced some combat bonuses: flanking (15 to 10), Great Generals (25 to 20), Discipline (15 to 10), Military Tradition (2x to 1.5x).
  * Remove requirement for aluminum on Mobile SAM.
  * Lower open terrain penalty to 10% .
  * Marsh is now 3 moves to enter (Chariots do not move quickly through it anymore).
  * Cavalry now upgrade to tanks.

[CITY STATES]

  * Reduced bonuses from Maritime city-states &#8211; Friends: +2 food in capital, +0 food in other cities &#8211; Allies: +3 food in capital, +1 food in other cities
  * Only the first 3 units gifted to a city-state will earn Influence now.
  * Killing a barb inside a city-state&#8217;s territory now gives a 5-turn buffer where there is no Influence intrusion penalty.

[HAPPINESS]

  * If an empire reaches -20 Happiness, it goes into revolt, and rebels start appearing throughout the empire, based on the number of cities.
  * Amount of Happiness needed to trigger a Golden Age reduced.
  * Amount of Happiness needed to trigger a Golden Age now increases as the number of cities in the empire goes up.

[POLICIES]

  * Landed Elite (Tradition Branch) now reduces culture cost of border growth by 2/3.
  * Monarchy (Tradition Branch) now provides +1 Gold per 2 pop in the Capital..
  * Liberty now provides a Settler training bonus to only the capital, and not every city.
  * Tradition now provides +50% growth in the capital.
  * Theocracy now reduces Unhappiness by 25% .
  * Reformation now gives a 10-turn GA.
  * Adopting Rationalism now gives a 4-turn GA.

[TECH TREE]

  * Add link between Military Science and Dynamite.
  * Add link between Civil Service and Education.
  * Add link between Economics and Scientific Theory.
  * Add link between Chivalry and Acoustics.
  * Research overflow now works correctly (extra beakers after completing a tech will rollover to the next tech).

[GENERAL]

  * Fixed bug where clicking on a city plot wouldn&#8217;t select the garrison.
  * Natural wonders now award culture (if worked) and happiness (if in border) if that trait is assigned to a wonder in XML.
  * Players must now always adopt Policies immediately, and cannot defer picking until later.
  * Have culture cost for policies never go down (trading away cities to reduce culture cost exploit). Razing cities will not raise your policy cost ceiling.
  * Reduced culture needed for first plot acquisition from 20 to 15.
  * 3 new Natural Wonders and rarity code for both base game and New World scenario.
  * Reduced points from Wonders (40 to 25) & Cities (10 to 8), increased points for pop by 1 (3 to 4).

<span style="font-size: medium;"><strong>[DIPLOMACY]</strong></span>

  * AI&#8217;s attitude towards you is now visible in the diplo screen and diplo drop-down.
  * Added info tooltip for an AI leader&#8217;s mood. Lists things that are making an AI player happy/upset.
  * New diplo system: Declaration of Friendship (public declaration with diplomatic repercussions).
  * New diplo system: Denounce (public declaration with diplomatic repercussions).
  * New custom leader responses (Serious Expansion Warning, Aggressive Military, Luxury Exchange, Borders Exchange, Gift Request, etc.).
  * Not agreeing to a friend&#8217;s request now results in a relations hit.
  * Third party AIs can now respond when a player makes a DoF or denounces someone. What they say is based on the situation &#8211; e.g. if you make friends with someone they don&#8217;t like, they&#8217;ll scold you.
  * AI leaders will now sometimes ask their friends to denounce one of their enemies as a show of support, and refusing to denounce someone when an AI asks can now make them very upset.
  * AI is now capable of denouncing friends (aka, backstabbing) and added backstabbing info to diplo overview screen.

<span style="font-size: medium;"><strong>[UI]</strong></span>

  * Add XP bar for air units, do not allow XP for air units attacking a city that is already down to its last hit-point.
  * Change ActivePlayer&#8217;s name to &#8222;You&#8220; in single player in score list.
  * Added game option to disable automated workers from removing features.
  * Fixed bug where Happiness from garrisoned units wasn&#8217;t being listed in Happiness info tooltip.
  * Load Map function will now display correct size and type of saved map.
  * New “Angry Genghis” loading screen (replaces the “fluffy-bunny Genghis” loading screen).
  * Added setup options to allow players to defer choosing Policies and Promotions right away.
  * Show the river penalty when attacking city across river (the penalty was there but was not being shown in the preview).
  * Global politics screen updated to reflect new diplo system.
  * Can no longer Force End Turn (shift-enter) through blocking notifications. CAN now use it to skip over units which need orders.
  * Multiple tweaks and bug fixes.

<span style="font-size: medium;"><strong>[MISC]</strong></span>

  * Fix small bugs with adding long roads around existing features.
  * Fixed bombard arrow across world wrap.

<span style="font-size: medium;"><strong>[MODDING]</strong></span>

  * Parent category counts now include counts of child categories.
  * Selecting/deselecting a category now automatically selects/deselects it&#8217;s children and its parent.
  * Tweaked category name truncation to better fit names.
  * Hide categories w/ no children and a count of 0.
  * Added support for fallback languages (if mod is not translated, fall-back to English so text keys are not showing).
  * The pager for the installed mods tab of the mods browser is now displayed in the correct location.
  * Long values for properties such as &#8222;Special Thanks&#8220; will no longer extend past the edge browser frame.
  * Categories refresh much faster now in the mods browser.
  * Multiple additional tweaks and fixes to the mod browser.
  * Support for mods that perform major restructuring of the tech tree including adding, deleting, and updating techs, buildings, and units.
  * Added GameEvents system for overriding Gameplay DLL specific functionality.
  * Fixed &#8222;Reload Landmark System&#8220; mod flag to now refresh landmarks defined in &#8222;ArtDefine_Landmarks&#8220;.
  * Multiple SDK updates (new version to go live shortly).

<span style="font-size: medium;"><strong>[SERIALIZATION/SAVES]</strong></span>

  * Fixed save format which causes saves to increase the memory footprint of the game drastically when loading frequently over the course of the game. This heightened the risk of late-game our of memory crashes significantly.

   

* * *

   

