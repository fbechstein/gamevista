---
title: 'Battlefield 1943 – neue  Infos zur PC Version'
author: gamevista
type: news
date: 2009-10-11T10:22:32+00:00
excerpt: '<p>[caption id="attachment_341" align="alignright" width=""]<img class="caption alignright size-full wp-image-341" src="http://www.gamevista.de/wp-content/uploads/2009/08/battle1943.png" border="0" alt="Battlefield 1943" title="Battlefield 1943" align="right" width="140" height="100" />Battlefield 1943[/caption]Das eine spätere Veröffentlichung einer PC Version auch mal etwas Gutes haben kann zeigt der Publisher <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a>. Denn es hagelte massig Kritik als bekannt wurde das die PC Version des Shooters <strong>Battlefield 1943 </strong>wesentlich später als die Konsolenversion erscheint.</p> '
featured_image: /wp-content/uploads/2009/08/battle1943.png

---
So wurde nun berichtet das es in der  PC Version eine Joystick Unterstützung geben wird. So kann man Flugzeuge und Panzer wesentlich besser steuern. Weiterhin meldet <a href="http://www.electronic-arts.de" target="_blank">EA</a> das es größere Server geben wird mit maximal 32 Spielern. Wohingegen sich Konsolenfans mit nur 24 Slots zufrieden geben müssen. Auch soll die Serverauswahl erleichtert werden.   
**Battlefield 1943** wird im Frühjahr 2010 für den PC in den deutschen Handel kommen.

 

* * *



* * *

 