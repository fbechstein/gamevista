---
title: Borderlands – Erhält Update mit Erhöhung der Levelgrenze
author: gamevista
type: news
date: 2010-09-06T16:27:47+00:00
excerpt: |
  |
    <p>[caption id="attachment_2204" align="alignright" width=""]<img class="caption alignright size-full wp-image-2204" src="http://www.gamevista.de/wp-content/uploads/2010/09/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Entwickler <strong>Gearbox </strong>kündigte heute einen kostenlosen Zusatzinhalt für das Actionspiel <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> an. Das Update wird die Levelgrenze auf die Stufe 69 anheben und bereitet das Spiel auf das kommende DLC C<strong>laptrap's New Robot Revolution</strong> vor.</p>
featured_image: /wp-content/uploads/2010/09/smalllogo.jpg

---
Der Download Content wird pünktlich am 21. September 2010 verfügbar sein. Vor kurzem wurde auch die Games of the Year Edition mit allen verfügbaren DLC\`s für <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> angekündigt. Diese erscheint am 12. Oktober 2010 im Handel.

 

   

* * *

   

