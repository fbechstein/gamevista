---
title: 'The Witcher 2: Assassins of Kings – Drittes Entwicklertagebuch erschienen'
author: gamevista
type: news
date: 2010-12-06T16:46:23+00:00
excerpt: '<p>[caption id="attachment_2191" align="alignright" width=""]<img class="caption alignright size-full wp-image-2191" src="http://www.gamevista.de/wp-content/uploads/2010/09/thewitcher2.jpg" border="0" alt="The Witcher 2: Assassins of Kings" title="The Witcher 2: Assassins of Kings" align="right" width="140" height="100" />The Witcher 2: Assassins of Kings[/caption]Das polnische Entwicklerstudio <strong>CD Projekt</strong> hat ein weiteres Entwicklertagebuch, in Form eines Videos, für das Rollenspiel <a href="http://www.thewitcher.com" target="_blank">The Witcher 2: Assassins of Kings</a> veröffentlicht. Thema der aktuellen Fassung ist erneut die Technik des Spiels und die damit verbundene RED-Grafikengine.</p> '
featured_image: /wp-content/uploads/2010/09/thewitcher2.jpg

---
Im Rahmen des dritten Videos wurden nun auch die Systemanforderungen für das Rollenspiel bekannt gegeben. Demnach solltet ihr als Minimum einen Dual-Core-Prozessor mit 2,2 GHz, sowie 1 GB Ram verbaut haben. Dazu benötigt ihr eine GeForce 8800 oder ein ähnliches Pendant von ATI. <a href="http://www.thewitcher.com/" target="_blank">The Witcher 2: Assassins of Kings</a> erscheint am 17. Mai 2011 für PC im Handel.

[> Zum The Witcher 2: Assassins of Kings – Entwicklertagebuch #3][1]

   

* * *

   



 [1]: videos/item/root/the-witcher-2-assassins-of-kings--drittes-entwicklertagebuch