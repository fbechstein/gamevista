---
title: 'Need for Speed: Nitro – Frühstart und Musikliste'
author: gamevista
type: news
date: 2009-10-20T08:13:45+00:00
excerpt: '<p><img class=" alignright size-full wp-image-907" src="http://www.gamevista.de/wp-content/uploads/2009/10/small_nfs_nitro.jpg" border="0" alt="Diablo III" title="Diablo III" align="right" width="140" height="100" />Der Publisher <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts (EA)</a> veröffentlichte nun eine Pressemitteilung, in der man schrieb, dass <strong>Need for Speed: Nitro</strong> bereits früher als geplant anstatt Mitte November schon am 06. November erscheint. Außerdem wurden die Musikauswahl präsentiert.</p> '
featured_image: /wp-content/uploads/2009/10/small_nfs_nitro.jpg

---
Die Musik von **Need for Speed: Nitro**:

  * Alex Metric &#8211; What Now
  * Bloody Beetroots feat. Cool Kids &#8211; Awesome
  * Crookers feat. Wiley and Thomas Jules &#8211; Business Man
  * Crystal Method feat. LMFAO &#8211; Sine Language
  * Danko Jones &#8211; Code Of The Road
  * Dizzee Rascal and Armand Van Helden &#8211; Bonkers
  * Drumagik &#8211; Make It Rock
  * Earl Greyhound &#8211; Oye Vaya
  * edIT feat. Wale and Tre&#8216; &#8211; Freaxxx
  * Evil 9 feat. El-P &#8211; All The Cash (Alex Metric Remix)
  * Hollywood Holt &#8211; Can&#8217;t Stop
  * k-os &#8211; FUN!
  * Lady Sovereign &#8211; I Got You Dancing
  * Major Lazer feat. Mr.Lex & Santigold Hold &#8211; The Line
  * Matt & Kim &#8211; Daylight (Troublemaker Remix)
  * Mickey Factz &#8211; Yeah Yeah
  * Pint Shot Riot &#8211; Not Thinking Straight
  * Placebo &#8211; Breathe Under Water
  * Rise Against &#8211; Kotov Syndrome
  * Roots Manuva &#8211; Buff Nuff
  * Rye Rye &#8211; Hardcore Girls
  * Street Sweeper Social Club &#8211; Fight! Smash! Win!
  * Taking Back Sunday &#8211; Lonely Lonely
  * The Enemy &#8211; No Time For Tears
  * The Gay Blades &#8211; O Shot (Dmerit Remix)
  * Two Fingers feat. Sway &#8211; Jewels And Gems

Wie immer kauft der Publisher jede Menge passende Musik ein. Leider wird **Need for Speed: Nitro** kein Online-Modus enthalten, was wohl viele Fans vom Kauf abschrecken wird.

 

* * *



* * *