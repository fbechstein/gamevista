---
title: Tomb Raider Trilogy – Releasetermin bekannt
author: gamevista
type: news
date: 2011-01-10T18:55:36+00:00
excerpt: '<p>[caption id="attachment_2500" align="alignright" width=""]<img class="caption alignright size-full wp-image-2500" src="http://www.gamevista.de/wp-content/uploads/2011/01/trilogy_screen.jpg" border="0" alt="Tomb Raider Trilogy " title="Tomb Raider Trilogy " align="right" width="140" height="100" />Tomb Raider Trilogy [/caption]Publisher <a href="http://www.square-enix.com/eu/de/" target="_blank">Square Enix</a> hat heute den Veröffentlichungstermin der <strong>Tomb Raider Trilogy</strong> im Rahmen einer Pressemitteilung bekannt gegeben. Die Neuauflage der Spiele <strong>Tomb Raider: Legend</strong>, <strong>Tomb Raider: Anniversary</strong> und <strong>Tomb Raider: Underworld</strong> werden im Form eines Pakets für die PlayStation 3 am 22. März 2011 in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2011/01/trilogy_screen.jpg

---
Als Inhalt zählen dazu die drei Spiele, in überarbeiteter HD-Grafik, ein exklusives Theme-Paket für die PS3 und ein Lara Croft PlayStation Home-Avatar.

 

   

* * *

   

