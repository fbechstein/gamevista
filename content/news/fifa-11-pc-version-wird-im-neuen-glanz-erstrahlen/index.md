---
title: FIFA 11 – PC Version wird im neuen Glanz erstrahlen
author: gamevista
type: news
date: 2010-07-08T15:57:23+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="FIFA 11" title="FIFA 11" align="right" width="140" height="100" />FIFA 11[/caption]Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Pressemitteilung angekündigt, dass die Grafikengine der NextGen-Konsolen erstmals auch ihren Weg auf den PC finden wird. Somit soll die diesjährige FIFA-Fassung auch auf dem PC so gut aussehen wie auf seinen Konsolen-Kollegen. Außerdem werden auch die neuen Spielmodi sowie das verbesserte Gameplay Einzug auf die Computer erhalten.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Weitere Features für die PC-Version möchte der Publisher in den nächsten Wochen verkünden. EA SPORTS <a href="http://www.fifa.easports.com " target="_blank">FIFA 11</a> erscheint am 30. September für PlayStation 3, Xbox 360, Wii, PC, PlayStation 2, NDS, PSP.

 

   

* * *

   

