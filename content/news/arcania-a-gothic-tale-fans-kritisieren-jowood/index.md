---
title: 'Arcania: A Gothic Tale – Fans kritisieren JoWood'
author: gamevista
type: news
date: 2009-08-28T06:56:10+00:00
excerpt: '<p>[caption id="attachment_538" align="alignright" width=""]<img class="caption alignright size-full wp-image-538" src="http://www.gamevista.de/wp-content/uploads/2009/08/held_small.png" border="0" alt="Arcania: A Gothic Tale" title="Arcania: A Gothic Tale" align="right" width="140" height="100" />Arcania: A Gothic Tale[/caption]Anscheinend stört einige Fans das Marketing von <a href="http://www.jowood.com">JoWood</a>, der Publisher rührt momentan nur sehr spärlich die Werbetrommel und rückt dementsprechend auch nur sehr zögerlich mit neuen Materialien zum Rollenspiel <strong>Arcania: A Gothic Tale</strong> raus. Dies prangerten nun mehrere Fans in einem offenen <a href="http://forum.worldofplayers.de/forum/showthread.php?t=662961">Brief </a>der an Publisher <a href="http://www.jowood.com">JoWood </a>gerichtet ist an.</p> '
featured_image: /wp-content/uploads/2009/08/held_small.png

---
Da geht es zum einen um das Marketing was sehr verbesserungswürdig sein soll, weil Publisher [JoWood][1] in den Pressemitteilungen zu wenig Enthusiasmus an den Tag legt. Auch Bildmaterial soll wohl, laut den Fans des [Briefes][2], teilweise 6 Monate alt sein bevor es veröffentlicht wird. Das demotiviert die Community nur Screenshots aus einem sehr frühen Entwicklungsstadium sehen zu können. Weiterhin gab es im Rahmen der gamescom Probleme. Hier war es möglich **Arcania: A Gothic Tale** als Fan zu präsentieren und anderen Fans nahe zu bringen. Dazu musste man aber vorher bei einem Gewinnspiel mitmachen. Die Glücklichen reisten an und wurden dann mit einer kurzfristigen Absage verprellt. Kosten für Anreise und Unterkunft sollen wohl auch inklusive gewesen sein, was [JoWood][1] aber teilweise nicht erstattete, so die Fans. Wir werden die Sache weiterhin verfolgen und sind gespannt auf eine Stellungnahme seitens [JoWood][1].

 

* * *



* * *

 

 

 [1]: http://www.jowood.com
 [2]: http://forum.worldofplayers.de/forum/showthread.php?t=662961