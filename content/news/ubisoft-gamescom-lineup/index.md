---
title: Ubisoft – gamescom Lineup
author: gamevista
type: news
date: 2009-08-07T09:41:54+00:00
excerpt: '<p>[caption id="attachment_307" align="alignright" width=""]<img class="caption alignright size-full wp-image-307" src="http://www.gamevista.de/wp-content/uploads/2009/08/ubisoftlogo_small.jpg" border="0" alt="Ubisoft" title="Ubisoft" align="right" width="140" height="100" />Ubisoft[/caption]Nachdem diverse Publisher ihr Lineup für die gamescom in Köln preisgegeben haben zieht <a href="http://www.ubi.com/" target="_blank" title="ubisoft">Ubisoft </a>nun auch nach. Neben dem Strategiespiel <strong>Ruse</strong> und der Uboot Sim <strong>Silent Hunter 5</strong> werden noch viele weiter Spiele präsentiert. <span>Hier eine komplette Liste:</span></p><p> </p>'
featured_image: /wp-content/uploads/2009/08/ubisoftlogo_small.jpg

---
  * Avatar
  * RUSE
  * Silent Hunter 5
  * Red Steel 2
  * <span>Rabbids Go Home</span>
  * <span>Academy of Champions</span>
  * <span>Assassin&#8217;s Creed 2</span>
  * <span>Shaun White Snowboarding 2</span>
  * <span>Your Shape</span>
  * <span>COP-The Recruit</span>
  * <span>Might & Magic &#8211; Clash Of Heroes</span>

 



 




</p> 

 