---
title: 'World of Warcraft: Cataclysm – Release im Dezember'
author: gamevista
type: news
date: 2010-10-04T16:44:32+00:00
excerpt: '<p>[caption id="attachment_2305" align="alignright" width=""]<img class="caption alignright size-full wp-image-2305" src="http://www.gamevista.de/wp-content/uploads/2010/10/wow_small2.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="140" height="100" />World of Warcraft[/caption]Publisher <strong>Activision </strong>hat im Rahmen einer Pressemitteilung den Releaser-Termin für das nächste Addon des Online-Rollenspiels <a href="http://www.worldofwarcraft.com/cataclysm/" target="_blank">World of Warcraft</a> bekannt gegeben. Pünktlich zum Weihnachtsgeschäft wird <a href="http://www.worldofwarcraft.com/cataclysm/" target="_blank">Cataclysm</a> am 7. Dezember 2010 in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2010/10/wow_small2.png

---
Wer keine Lust hat seine Kopie im Laden zu kaufen kann auch auf die digitale Download-Version im Blizzard Store zurückgreifen. Eine Collector\`s Edition wurde auch angekündigt, der Inhalt ist bisher aber noch nicht bekannt. <a href="http://www.worldofwarcraft.com/cataclysm/" target="_blank">Cataclysm</a> wird das Level-Cap auf Stufe 85 erhöhen und wird mit den Worgen und Goblins zwei neue Rassen einführen.

 

   

* * *

   

