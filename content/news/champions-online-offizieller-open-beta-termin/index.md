---
title: Champions Online – Offizieller Open Beta Termin
author: gamevista
type: news
date: 2009-07-30T10:14:30+00:00
excerpt: '<p>[caption id="attachment_122" align="alignright" width=""]<img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" />Champions Online[/caption]Endlich! Entwickler <a href="http://crypticstudios.com/" target="_blank" title="cryptic studios">Cryptic Studios</a> enthüllen den Termin für die Open Beta ihren Online Rollenspiels <strong>Champions Online</strong>. Laut Pressemitteilung startet die Testphase am 17. August 2009. Der Client für die besagte Open Beta wird bereits am 5. August 2009 veröffentlicht.</p>'
featured_image: /wp-content/uploads/2009/07/champions_online_small.jpg

---
Der Umfang der Beta wird nach Angaben von <a href="http://crypticstudios.com/" target="_blank" title="cryptic studios">Cryptic Studios</a> mit ca 50.000 Keys weltweit ausfallen. Da dies nicht allzu viele sind solltet ihr euch sputen und schnell Anmelden. Weitere Information und das <a href="http://www.fileplanet.com/promotions/champions-online/" target="_blank" title="beta anmeldung">Anmeldeformular</a> könnt ihr auf der Website von **<a href="http://www.champions-online.com/node/105997" target="_blank" title="champions online">Champions Online</a>** finden.







 