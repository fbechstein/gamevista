---
title: Steam – Am Montag Wartungsarbeiten
author: gamevista
type: news
date: 2010-10-10T15:55:24+00:00
excerpt: '<p>[caption id="attachment_1229" align="alignright" width=""]<img class="caption alignright size-full wp-image-1229" src="http://www.gamevista.de/wp-content/uploads/2009/12/steam.jpg" border="0" alt="Steam" title="Steam" align="right" width="140" height="100" />Steam[/caption]Publisher <strong>Valve </strong>kündigte auf seiner <a href="http://store.steampowered.com/news/4464/" target="_blank">offiziellen Website</a> des Onlinedienstes <strong>Steam </strong>an, dass am kommenden Montag, den 11. Oktober 2010, die Steam-Server im Rahmen von Wartungsarbeiten offline sein werden. Gegen 15 Uhr werden die Server für mindestens vier Stunden heruntergefahren und gewartet.</p> '
featured_image: /wp-content/uploads/2009/12/steam.jpg

---
Dadurch werden einige Funktionen des Steam-Matchmaking nicht zur Verfügung stehen, so z.B. für Call of Duty: Modern Warfare 2. Außerdem werden der Steam-Shop und die Download-Funktion von den Steam-Servern nicht funktionieren.

 

   

* * *

   



 