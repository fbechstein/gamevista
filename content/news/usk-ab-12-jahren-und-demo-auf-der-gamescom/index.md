---
title: Risen – ab 12 Jahren und Demo auf der Gamescom
author: gamevista
type: news
date: 2009-08-03T10:43:52+00:00
excerpt: '<p>[caption id="attachment_263" align="alignright" width=""]<img class="caption alignright size-full wp-image-263" src="http://www.gamevista.de/wp-content/uploads/2009/08/risentier.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Das Rollenspiel <strong>Risen </strong>vom Entwickler <a href="http://www.piranha-bytes.com" target="_blank" title="piranha bytes">Piranha Bytes</a> wurde nun von der Unterhaltungssoftware Selbstkontrolle (USK) getestet und diese vergibt eine Altersfreigabe von 12 Jahren. Weiterhin wird es möglich sein eine Demo von <strong>Risen </strong>auf der gamescom 2009 in Köln zu anzuspielen. </p>'
featured_image: /wp-content/uploads/2009/08/risentier.png

---
Die Messe findet vom 20. – 23. August 2009 statt und **Risen** könnt ihr am Stand von Deep Silver antesten. Wir werden uns auch ein Bild des inoffiziellen Gothic Nachfolgers machen und euch dann Live von der gamescom Berichten.[  
][1] 







 

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=132