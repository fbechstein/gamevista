---
title: Lost Planet 2 – Releasetermin bekannt
author: gamevista
type: news
date: 2010-03-29T18:48:02+00:00
excerpt: '<p>Gute Nachrichten für alle PC Spieler. Der [caption id="attachment_1380" align="alignright" width=""]<img class="caption alignright size-full wp-image-1380" src="http://www.gamevista.de/wp-content/uploads/2010/01/lp2.jpg" border="0" alt="Lost Planet 2" title="Lost Planet 2" align="right" width="140" height="100" />Lost Planet 2[/caption]Publisher <strong>Capcom </strong>hat im Rahmen des CapcomUnity Updates die Entwicklung der PC Version von <a href="http://www.lostplanet-thegame.com" target="_blank">Lost Planet 2</a> bestätigt. Ein Veröffentlichungstermin wie für die Xbox 360- bzw. PlayStation 3 Version, am 11. Mai, kann aber nicht eingehalten werden. So soll <a href="http://www.lostplanet-thegame.com" target="_blank">Lost Planet 2</a> für PC im Herbst mit aufgebohrter Grafik in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2010/01/lp2.jpg

---
<a href="http://www.lostplanet-thegame.com" target="_blank">Lost Planet 2</a> bietet eine actiongeladene Atomsphäre mit einem  tropischen Dschungelsetting und einem Koop-Modus mit bis zu vier Spielern.

 

 

<cite></cite>

   

* * *

   

