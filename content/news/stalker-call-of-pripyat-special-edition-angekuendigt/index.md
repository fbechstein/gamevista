---
title: 'Stalker: Call of Pripyat – Special Edition angekündigt'
author: gamevista
type: news
date: 2009-10-14T15:52:28+00:00
excerpt: '<p>[caption id="attachment_673" align="alignright" width=""]<img class="caption alignright size-full wp-image-673" src="http://www.gamevista.de/wp-content/uploads/2009/09/stalker_cop.jpg" border="0" alt="Stalker: Call of Pripyat" title="Stalker: Call of Pripyat" align="right" width="140" height="100" />Stalker: Call of Pripyat[/caption]Der Publisher <a href="http://www.bit-composer.com" target="_blank">bit Composer Games</a> hat für den Ego-Shooter <strong>Stalker: Call of Pripyat</strong> eine Special Edition angekündigt. Diese wird zusammen mit der normalen Version am 5. November 2009 in den deutschen Handel kommen.</p> '
featured_image: /wp-content/uploads/2009/09/stalker_cop.jpg

---
Preislich wird die Special Edition 10 Euro teurer daher kommen und zum Preis von 39,99 Euro angeboten. Die Inhalte werden sein:

 

<div>
  <ul id="nointelliTXT">
    <li>
      <p>
        Vollversion des Spiels
      </p>
    </li>
    
    <li>
      <p>
        eine exklusive Metallbox
      </p>
    </li>
    
    <li>
      <p>
        S.T.A.L.K.E.R-Sturmfeuerzeug
      </p>
    </li>
    
    <li>
      <p>
        eine große Übersichtskarte der Zielgebiete
      </p>
    </li>
    
    <li>
      <p>
        zwei hochwertige, gestickte Aufnäher
      </p>
    </li>
    
    <li>
      <p>
        S.T.A.L.K.E.R-Bandana
      </p>
    </li>
  </ul>
</div>

* * *



* * *

 