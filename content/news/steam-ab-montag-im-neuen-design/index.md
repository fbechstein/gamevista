---
title: 'Steam  – Ab Montag im neuen Design'
author: gamevista
type: news
date: 2010-04-21T19:03:33+00:00
excerpt: '<p>[caption id="attachment_1229" align="alignright" width=""]<img class="caption alignright size-full wp-image-1229" src="http://www.gamevista.de/wp-content/uploads/2009/12/steam.jpg" border="0" alt="Steam" title="Steam" align="right" width="140" height="100" />Steam[/caption]Die seit Februar laufende Betaphase zum Onlinedienst <a href="http://store.steampowered.com/news/" target="_blank">Steam</a> wird ab dem kommenden Montag beendet sein, denn ab diesem Zeitpunkt wird die neue Version Live gehen und der breiten Masse vorgestellt. Mit neuen Funktionen und ansprechendem Design soll <a href="http://store.steampowered.com/news/" target="_blank">Steam</a> noch zugänglicher und besser werden.</p> '
featured_image: /wp-content/uploads/2009/12/steam.jpg

---
Die neuen Funktionen im Überblick:

  * Graphische Ansicht der Spielebibliothek
  * Mehr Informationen über alle Spiele einsehbar
  * Verbesserte Newsansicht
  * Neue Community-Features
  * Verbesserte Freundschaftsliste
  * Neue Downloadübersicht
  * Unterstützt Windows 7 Mausgesten

   

* * *

* * *

 __

 