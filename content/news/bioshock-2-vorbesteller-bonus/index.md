---
title: Bioshock 2 – Vorbesteller Bonus
author: gamevista
type: news
date: 2009-08-12T21:11:23+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Wie Entwickler <a href="http://www.2kgames.com/" target="_blank">2kMarin</a> nun mitgeteilt hat wird für Vorbesteller von <strong>Bioshock 2</strong> ein nettes Extra bereitgehalten. Demnach werden 2 zusätzliche Charaktere für den Multiplayer Modus auswählbar sein. Leider gilt dies nur für amerikanische bzw. kanadische Gamestop und EBgames Vorbesteller.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Bei den Charakteren handelt es sich um Zigo d&#8217;Acosta und Mlle Blanche de Glace. Aber, <a href="http://www.2kgames.com/" target="_blank">2kMarin</a> bestätigte außerdem das europäische Spieler nicht leer ausgehen sollen. Ein Bonus Paket wird auch hier geschnürt. Was darin enthalten sein wird erfahrt ihr nächste Woche.

 

 





