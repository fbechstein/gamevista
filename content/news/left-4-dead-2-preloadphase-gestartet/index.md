---
title: Left 4 Dead 2 – Preloadphase gestartet
author: gamevista
type: news
date: 2009-11-08T17:22:37+00:00
excerpt: '<p>Der Entwickler <a href="http://www.l4d.com" target="_blank">Valve</a> hat auf der Onlineplattform Steam den Startschuss für den Zombie-Shooter<strong> Left 4 Dead 2</strong> gegeben. [caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]So können Vorbesteller des neusten <a href="http://www.l4d.com" target="_blank">Valve</a> Spiels dieses Wochenende den Steamdownloader anschmeißen um das Spiel schon vor der Veröffentlichung am 17. November 2009 auf ihre Festplatte zu befördern.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Dies soll garantieren das die Fans von **Left 4 Dead 2** am Release Tag pünktlich den Zombies saures geben können. <a href="http://www.l4d.com" target="_blank">Valve</a> hat heute auch ein Update für die Demo von **Left 4 Dead 2** veröffentlicht. So wurden diverse Fehler behoben, downloadbar ist der Patch automatisch über Steam.

 

* * *



* * *