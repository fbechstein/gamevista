---
title: 'Uncharted 2: Among Thieves – Ab sofort erhältlich'
author: gamevista
type: news
date: 2009-10-16T15:23:03+00:00
excerpt: '<p>[caption id="attachment_875" align="alignright" width=""]<img class="caption alignright size-full wp-image-875" src="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2_small.png" border="0" alt="Uncharted 2: Among Thieves" title="Uncharted 2: Among Thieves" align="right" width="140" height="100" />Uncharted 2: Among Thieves[/caption]Wie der Entwickler <a href="http://www.naughtydog.com" target="_blank">Naughty Dog</a> per Pressemitteilung verkündet hat ist das Actionspiel <strong>Uncharted 2: Among Thieves</strong> ab sofort im deutschen Handel offiziell verfügbar. Das Abenteuer rund um Nathan Drake geht in die nächste Runde und das exklusiv für die PlayStation 3.</p> '
featured_image: /wp-content/uploads/2009/10/uncharted2_small.png

---
Zum Releasetermin hat <a href="http://www.naughtydog.com" target="_blank">Naughty Dog</a> noch ein <a href="http://www.playstationweb.de/uncharted2/de/" target="_blank">Online Magazin</a> veröffentlicht, <a href="http://www.playstationweb.de/uncharted2/de/" target="_blank">hier</a> könnt ihr es finden. Das Spiele-Highlight dürfte viele PC- oder Xbox 360 Besitzer neidisch auf die Sony Konsole werden lassen. **Uncharted 2: Among Thieves** bietet nicht nur verschiedenste Charaktere, sondern führt euch auch an die exotischsten Schauplätze. Euch erwarten grüne Sümpfe und  dicht besiedelte Stadtteilen bis hin zu den vereisten und verschneiten Gipfeln des Himalayas.

 

* * *



* * *

 