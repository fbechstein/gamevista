---
title: Left 4 Dead 2 – Preload der Demo hat begonnen
author: gamevista
type: news
date: 2009-10-25T17:43:50+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Fans des Multiplayer Shooters <strong>Left 4 Dead 2</strong> können seit diesem Wochenende die Testversion der Zombiejagd herunterladen. So kommen Vorbesteller dann ab Dienstag, dem 27. Oktober 2009 in den Genuss den neusten Left 4 Dead 2 anspielen zu können.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Denn heute ist nur der so genannte Preload freigeschaltet, die Demo wird dann ab Dienstag aktiviert. Beziehen könnt ihr die Demo über den Onlinedienst <a href="http://www.store.steampowered.com" target="_blank">Steam</a>.

 

 

* * *



* * *

 