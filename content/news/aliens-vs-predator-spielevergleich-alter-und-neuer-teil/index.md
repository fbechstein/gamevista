---
title: Aliens vs. Predator – Alter und neuer Teil im Vergleich
author: gamevista
type: news
date: 2010-01-22T14:22:32+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Das Entwicklerteam <strong>Rebellion </strong>liefert pünktlich zum Wochenende einen neuen Trailer zum Ego-Shooter <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> ab. Im drei Minuten langen Video sprechen die Mitarbeiter des Studios über den aktuellen <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> Teil und seinen Vorgänger von 1999.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Gezeigt werden zahlreiche Spielszenen gepaart mit den Kommentaren der Entwickler. Besonders die neue Grafikengine kommt dabei zur Sprache. Wem der Trailer gefällt hat ab dem 16. Februar 2010 die Möglichkeit das Spiel über Importhändler zu beziehen. <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> erscheint aufgrund der hohen Gewaltdarstellung offiziell nicht in Deutschland.

[> Zum Aliens vs. Predator &#8211; Heritage Trailer][1]

<cite></cite>

* * *



* * *

 

 [1]: videos/item/root/aliens-vs-predator-heritage-trailer