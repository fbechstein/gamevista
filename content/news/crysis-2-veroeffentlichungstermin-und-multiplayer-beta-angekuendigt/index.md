---
title: Crysis 2 –Veröffentlichungstermin und Multiplayer-Beta angekündigt
author: gamevista
type: news
date: 2010-06-11T14:04:55+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Publisher <strong>Electronic Arts</strong> hat für den Ego-Shooter <a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a> den Veröffentlichungstermin konkretisiert. So wird der zweite Teil voraussichtlich im Herbst dieses Jahres in den Handel kommen.<br /> Es gibt weitere Informationen zur Multiplayer-Beta. Entwickler <strong>Crytek </strong>hat nun offiziell eine geschlossene Mehrspielertestphase angekündigt.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
Um den Ansturm der Fans Herr zu werden wurde heute die <a href="http://www.mycrysis.com/newsdetails.php?news=43632" target="_blank">Community-Website</a> für ein Forumsupgrade gesperrt. Denkbar ist das, sobald die Seite wieder Online ist, sich die Pforten für die Beta-Registrierungen öffnen werden. Wer unter den Glücklichen ist wird per Email über einen entsprechenden Beta-Key informiert. Genauere Details zur Beta sind leider noch nicht bekannt. Werden aber sicher in den nächsten Tagen der Öffentlichkeit präsentiert.

 

   

* * *

   

