---
title: ' Starcraft 2: Wings of Liberty'
author: gamevista
type: news
date: 2010-08-01T16:56:44+00:00
excerpt: '<p><strong>[caption id="attachment_2074" align="alignright" width=""]<img class="caption alignright size-full wp-image-2074" src="http://www.gamevista.de/wp-content/uploads/2010/08/starcraft2_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="88" />Starcraft 2[/caption]</strong></p> <p><a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> ist der lang erwartete Nachfolger des wohl erfolgreichsten Titels im Echzeitstrategie Genre. Nach über 12 Jahren des langen Wartens präsentieren die Entwickler von Blizzard, den zweiten Teil und gehen dabei neue Wege sowohl im Spieledesign als auch in der Storyführung. <a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> ist der Erste von zwei weiteren Teilen und führt den Spieler durch die Geschichte der Terraner-Fraktion. So kann der Spieler in knapp 30 Solo-Missionen die Story des Hauptcharakters Jim Raynor erleben.</p> '
featured_image: /wp-content/uploads/2010/08/starcraft2_small.jpg

---
Der Spieler wird im zweiten Teil nicht von Mission A nach B geleitet, sondern kann sich auf dem Raumkreuzer Hyperion seine Missionen auf einer Sternenkarte frei auswählen. Dabei trifft er zahlreiche Charaktere und neue Funktionen an. So könnt ihr euch über einen großen Fernsehbildschirm, der in der Schiffsbar angebracht ist, per Nachrichtenupdate aus der Starcraft 2-   
Welt informieren lassen oder im Labor neue Features für eure Truppen freischalten. Man klickt sich zwischen den Missionen, ähnlich einem Rollenspiel, durch Brücke, Labor, Waffenkammer und Schiffsbar. Schicke Render-Sequenzen präsentieren dabei die Geschichte, wie erwartet verzichtet **Blizzard** auf interaktive Gespräche. Eine Charakterentwicklung ist somit leider nicht möglich. 

<a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> ist aber schließlich auch kein Rollenspiel sondern ein waschechtes Echtzeit-Strategiespiel. Dies zeigt sich gleich in den ersten Missionen. In Windeseile erstellen wir Sammler die uns die nötigen Ressourcen für die ersten Marines zur Verfügung stellen. Dabei werden in den Missionen immer wieder Bonusziele und Archievements in Aussicht gestellt. Für jedes Abschließen einer Mission erhaltet ihr Geld und Forschungspunkte. Diese Punkte könnt ihr jeweils für die Weiterentwicklung eurer Einheiten ausgeben, z.B. bessere Panzerung für Fahrzeuge oder ihr verstärkt eure Bunker mit mehr Trefferpunkten. Die Grafik macht dabei einen sehr gelungenen Eindruck. Sie ist zwar für einen gerade erst erschienen Titel etwas angestaubt. Bietet dafür aber umso mehr beeindruckende Effekte und Animationen.

<p style="text-align: center;">
  <img class=" size-full wp-image-2075" src="http://www.gamevista.de/wp-content/uploads/2010/08/protoss_colossus_002-large.jpg" border="0" width="600" height="432" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/protoss_colossus_002-large.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/protoss_colossus_002-large-300x216.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Unser Protagonist Jim Raynor führt mit Hilfe eines alten Freundes mit dem Namen Tychus, Angriffe gegen die Terranische Liga an. Raynor wird dazu passend im Weltraum-Cowboy-Stil in Szene gesetzt. Country-Musik  aus der Jukebox erklingt und verbreitet dabei ihren Flair. Die Charaktere sind nicht sonderlich tiefgründig, eher stereotypisch, passen aber wunderbar in das Starcraft-Universum. Der gefallene Held Raynor stürzt sich mit anfänglichem Desinteresse ins Abenteuer zwischen Terranern, Zerg und Protoss. 

<a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> bietet neben dem typischen Hauptspielt viel Drumherum. Einen erstklassigen Sound, mit wunderbar vertonten Charakteren und Einheiten. Das Battlenet 2.0 was viele neue Funktionen mit sich bringt. Eine eher untypische Missionsauswahl, die den Anschein erweckt die freie Wahl zu haben. Zudem bietet das Spiel zahlreiche Entwicklungsmöglichkeiten für eure Einheiten und Gebäude. Auch sind die Missionen meist gespickt mit etwas Besonderem. **Blizzard** versprach schon im Vorfeld das der Spieler sich an Missionselemente erinnern wird. 

Da wäre eine Misssion in der ihr nachts auf verseuchte Zivilisten trefft. Diese greifen im Schutz der Dunkelheit eure Basis unentwegt an. Während ihr am Tage ungestört deren Brutstätten zerstören sollt, weil diese im Sonnenlicht nicht überleben.   
Ein weiteres Beispiel wäre eine Mission auf einem Lavaplaneten. Dort müsst ihr eine einzigartige Art von Kristall einsammeln. Kein Problem denkt man sich. Doch alle paar Minuten wird der Sammelort von Lava überfüllt und man muss aufpassen, dass eure Sammler rechtzeitig auf der erhöhten Position der Basis stehen um nicht in Flammen aufzugehen.    
Dieses Missionsdesign ist sehr abwechslungsreich  und man merkt es dem Spiel an, dass die Entwickler hier ganz klar der Maxime folgten ein einmaliges Spielerlebnis zu erschaffen.

<p style="text-align: center;">
  <img class=" size-full wp-image-2076" src="http://www.gamevista.de/wp-content/uploads/2010/08/wings_001-large.jpg" border="0" alt="Auf der Schiffsbrücke treffen wir zahlreiche Charaktere oder wählen einen neuen Auftrag aus" title="Auf der Schiffsbrücke treffen wir zahlreiche Charaktere oder wählen einen neuen Auftrag aus" width="600" height="450" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/wings_001-large.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/wings_001-large-300x225.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Spieler des ersten Teils werden sich zudem sofort zurecht finden. Die Einheitensteuerung und das Spielprinzip sind ziemlich gleich geblieben.  Wir erhalten während der Mission Hintergrundinformationen per Mini-Video-Botschaft oder sehen geskriptete Inszenierungen. Störend hierbei sind die geskripteten Missionsenden. In einer Mission müssen wir Zivilisten vor den Zerg in Sicherheit bringen. Wir eskortieren diese also mit einer riesen Armee aus Marines zum Abholort. Dort angekommen setzt das geskriptete Missionsende ein und zeigt einen Raumhafen der völlig überrannt wird, anstatt zu sehen wie unsere Armee bis zum bitteren Ende kämpft. 

Da <a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> das Hauptaugenmerk auf die Terraner-Kampagne gelegt hat, sind dementsprechend viele Missionen für diese eine Fraktion vorhanden. Dies führt vor allem am Anfang dazu, dass die Geschichte sehr langsam Fahrt aufnimmt und streckenweise einfach zu lang gebraucht wird um an die Hauptstory anzuknüpfen. Da **Wings of Liberty** der erste Teil einer Trilogie ist wirft das Ende, und der damit verbundene Trailer, mehr Fragen auf als das es beantwortet. **Blizzard** möchte sicherlich dadurch die Lust auf den zweiten Teil Heart of the Swarm wecken, trotzdem erhält man dadurch leicht den Eindruck ein Prequel-Produkt installiert zu haben und nicht ein an sich fertiges Spiel. Man kennt dies aus Kinofilmen und könnte dafür auch dem Produzenten manch unliebsames Wort an den Kopf werfen, wenn dieser den Ausgang des Films auf den zweiten bzw. dritten Teil verschiebt.

<p style="text-align: center;">
  <img class=" size-full wp-image-2077" src="http://www.gamevista.de/wp-content/uploads/2010/08/terran_battlecruiser_002-large.jpg" border="0" width="600" height="432" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/terran_battlecruiser_002-large.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/terran_battlecruiser_002-large-300x216.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Der Computergegner macht sowohl im Skirmishmodus sowie in der Kampagne einen recht soliden Eindruck. Dieser mischt seine Truppen intelligent zusammen und greift auch speziell die Schwachstellen in unserer Verteidigung an. Leider passiert es aber immer wieder, dass die Wegfindung ihre Aussetzer hat. Auch bleibt das leidige Einbauen der WBF\`s, der Baueinheit der Terraner, hinter Gebäuden auch im zweiten Teil erhalten.

Taktisch dürfen wir wieder auf eine Vielzahl von Einheiten und Gebäuden zurückgreifen. Zusammen mit der Upgrademöglichkeit von Einheiten, den verschiedenen Forschungsgebieten und rekrutierbaren Söldner bietet <a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> im Solo- bzw. Mehrspielermodus genug Potential der Hit zu werden, den sich alle Fans so sehr gewünscht hatten. Gerade wenn man mit Freunden im Koop-Modus gegen den Computer oder im Mehrspielermodus gegen menschliche Spieler antritt, merkt man wie fertig und stimmig dieses Spiel in der Verkaufsversion ist. Fehler sucht man hier vergeblich, auch wenn Blizzard schon den ersten Patch veröffentlicht hat.

<p style="text-align: center;">
  <img class=" size-full wp-image-2078" src="http://www.gamevista.de/wp-content/uploads/2010/08/wings_002-large.jpg" border="0" title="Hier kaufen wir uns Söldner. Standard-Einheiten mit besseren Kampfwerten" width="600" height="450" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/wings_002-large.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/wings_002-large-300x225.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**Fazit**

<a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> setzt das um was sich sicherlich viele Starcraft-Fans für einen Nachfolger gewünscht haben. Eine schicke Grafik die den **Blizzard**-Stil beibehält. Viele Details und ausgezeichnete Rendersequenzen die die Geschichte vorantreiben. Neue Einheiten und die verbesserte Forschung, sowie diverse Upgrades runden das Paket ab. Oft genug erwähnt und kritisiert ist sicherlich der Fakt, dass es nur eine Fraktion in der Kampagne spielbar ist. Hoffen wir, dass diese Vermarktungsstrategie nicht allzu viele Nachahmer findet. Mir fehlen einfach die Zerg und Protoss als Abwechslung. 

Zwar tröstet die Mini-Protoss-Kampagne ein wenig darüber hinweg, ersetzt aber niemals die Vielfalt, die sich ergeben hätte, wenn alle drei Fraktionen mit in die Story eingeflossen und spielbar wären. <a href="http://eu.battle.net/sc2/de/" target="_blank" rel="nofollow">Starcraft 2: Wings of Liberty</a> ist eine konsequente Weiterentwicklung des ersten Teils. **Blizzard** setzt dabei wie bei all ihren Titeln immer auf qualitative Entwicklungsarbeit. Somit wirkt der zweite Teil aus einem Guss und beherbergt kaum Fehler oder grobe Schnitzer im Gameplay. Ein fehlender LAN-Modus hin oder her, wer den ersten Teil mochte, wird den Zweiten lieben. 

**Gesamtwertung 90%**

**Multiplayerwertung 93%  
** 

<p style="text-align: center;">
  <img class=" size-full wp-image-2079" src="http://www.gamevista.de/wp-content/uploads/2010/08/protoss_carrier_002-large.jpg" border="0" width="600" height="432" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/protoss_carrier_002-large.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/protoss_carrier_002-large-300x216.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

