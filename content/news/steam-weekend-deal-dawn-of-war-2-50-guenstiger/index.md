---
title: Steam Weekend Deal – Dawn of War 2 50% günstiger
author: gamevista
type: news
date: 2009-07-31T16:46:18+00:00
excerpt: '<p>[caption id="attachment_236" align="alignright" width=""]<img class="caption alignright size-full wp-image-236" src="http://www.gamevista.de/wp-content/uploads/2009/07/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]Der neue <a href="http://store.steampowered.com" target="_blank" title="steam">Steam </a>Weekend Deal lässt mal wieder viele Augen ins stauen geraten. Denn ab Heute gibt es alle von Spiele von <a href="http://www.relic.com" target="_blank" title="relic">Relics </a><strong>Dawn of War</strong> Serie zum halben Preis. Mit enthalten ist natürlich das erst gestern erschienene Update „<strong>There is only War</strong>“, womit ihr unter anderem im Observer Modus Spielern über die Schulter schauen könnt.</p>'
featured_image: /wp-content/uploads/2009/07/1_1280x800_small.jpg

---
Außerdem werden bei sämtlichen **Company of Heroes** Titeln, Hauptspiel und Addons, die Preise halbiert. Den Strategiespielhit **Dawn of War 2** aus der **Warhammer 40.000** Serie wird demnach 24.99 Euro kosten. Das 2. Weltkriegsstrategiespiel **Company of Heroes** wird für 4,99 Euro angeboten. Für das neueste **Company of Heroes** Addon **Tales of Valor** werden dieses Wochenende nur 17.49 Euro verlangt. <a href="http://www.store.steampowered.com" target="_blank" title="steam">Steam </a>bietet zusätzlich noch ein Paket an das sich „**Relic Super Pack**“ nennt. Darin enthalten sind die **Dawn of War** Gold Edition, plus Addon **Dark Crusade**, **Company of Heroes**, plus Addon **Opposing Fronts** für ganze 24.99 Euro.







 

 