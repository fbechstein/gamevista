---
title: Civilization 5 – Vier Karten-Pakete veröffentlicht
author: gamevista
type: news
date: 2010-12-01T18:29:22+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Entwickler <strong>Firaxis Games</strong> hat heute vier verschiedene Karten, zum Download über <strong>Steam, </strong>für das Strategiespiel <a href="http://www.civilization5.com" target="_blank">Civilization 5</a> veröffentlicht. Die Downloadable-Content-Pakete mit den Namen Americas, Asia, Mediterranean und Mesopotamia entweder jeweils für 2,99 Euro einzeln gekauft werden oder zusammen für 7,99 Euro erworben werden.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Die Map-Packs sind allesamt von **Firaxis Games** nach realen Vorgaben entwickelt worden. Ihr könnt entweder einen zufällig gewählten Anführer spielen oder einen aus einer Auswahl für das Gebiet typischen Anführer auswählen.

 

   

* * *

   

