---
title: 'Dragon Age: Origins – Kämpft auf der Seite der dunklen Brut'
author: gamevista
type: news
date: 2010-05-06T17:29:18+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Der Publisher <strong>Electronic Arts</strong> kündigte heute im Rahmen einer Pressemitteilung den nächsten herunterladbaren Zusatzinhalt für das Rollenspiel <a href="http://www.dragonage.bioware.com" target="_blank">Dragon Age: Origins</a> an. Das DLC-Paket mit dem Namen <strong>Die Chroniken der Dunklen Brut</strong> bietet euch die Möglichkeit, den Kampf auf der dunklen Seite zu erleben.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Dabei seid ihr dem Willen des Erzdämons unterworfen und steuert die Armee der Verderbnis. **Die Chroniken der Dunklen Brut** ist ab dem 18. Mai 2010 für Xbox 360 sowie PC für erhältlich. Das Veröffentlichungsdatum für das PlayStation Network wird noch bekannt gegeben.

„Die Chroniken der Dunklen Brut ermöglicht den Spielern den Blick auf einen alternativen Verlauf der Geschichte innerhalb des Dragon Age-Universums. Der DLC beginnt mit dem Tod des Spielercharakters während des Beitrittsrituals, weshalb die Grauen Wächter anschließend unter Alistairs Führung in die Schlacht ziehen. Als Hurlock-Vorhut hat allein der Spieler die Macht, andere der Dunklen Brut zu seinen Knechten zu machen und sie in die Schlacht um das brennende Denerim zu führen. In diesem Abenteuer befehligt der Spieler Genlocks, Hurlocks, Kreischer und sogar die mächtigen Oger.“

Zu den Hauptfeatures zählen:

  * Stellt Euch eine Welt ohne Helden vor, in der der Größte unter Euch niemals ein Grauer Wächter wurde
  * Spielt den Untergang Denerims aus der Perspektive der Dunklen Brut durch
  * Schließt das Zusatzabenteuer ab und schaltet einen epischen Gegenstand der Dunklen Brut frei, der in Dragon Age: Origins und Dragon Age: Origins &#8211; Awakening verwendet werden kann
  * Setzt die &#8222;Knechten&#8220;-Fähigkeit ein und rekrutiert jeden Angehörigen der Dunklen Brut, dem Ihr begegnet, für Eure Gruppe

 

   

* * *

* * *

 __

 