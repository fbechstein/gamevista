---
title: Lara Croft and the Guardian of Light – Update bringt Online-Koop auf die Xbox
author: gamevista
type: news
date: 2010-10-26T18:13:11+00:00
excerpt: '<p>[caption id="attachment_2035" align="alignright" width=""]<img class="caption alignright size-full wp-image-2035" src="http://www.gamevista.de/wp-content/uploads/2010/07/laracroft_gol.jpg" border="0" alt="Lara Croft and the Guardian of Light " title="Lara Croft and the Guardian of Light " align="right" width="140" height="100" />Lara Croft and the Guardian of Light [/caption]<strong>Square Enix</strong> hat im Rahmen einer Pressemitteilung das nächste große Update für das Actionspiel <a href="http://www.laracroftandtheguardianoflight.com" target="_blank">Lara Croft and the Guardian of Light</a> angekündigt. Bereits am Mittwoch, den 27. Oktober 2010, erscheint der Patch auf Xbox Live Arcade und bietet endlich die Möglichkeit den lang erwarteten Online-Koop-Modus zu nutzen.</p> '
featured_image: /wp-content/uploads/2010/07/laracroft_gol.jpg

---
Außerdem wird zum selben Zeitpunkt der erste Download-Content „Fallengefahr“ auf Xbox Live Arcade veröffentlicht. Dieser kann die ersten 30 Tage kostenlos heruntergeladen werden. Das Herausforderungs-Paket 1 „Fallengefahr“ bietet ein neues Areal, in dem sich Spieler bei Rätseln, Kämpfen und beim Erkunden der Umgebung beweisen können.

 

   

* * *

   

