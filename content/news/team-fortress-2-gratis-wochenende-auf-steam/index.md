---
title: Team Fortress 2 – Gratis Wochenende auf Steam
author: gamevista
type: news
date: 2009-12-18T19:37:13+00:00
excerpt: '<p>[caption id="attachment_368" align="alignright" width=""]<img class="caption alignright size-full wp-image-368" src="http://www.gamevista.de/wp-content/uploads/2009/08/tf2.jpg" border="0" alt="Team Fortress 2" title="Team Fortress 2" align="right" width="140" height="100" />Team Fortress 2[/caption]Für den Ego-Shooter <strong>Team Fortress 2</strong> hat der Entwickler <a href="http://www.teamfortress.com" target="_blank">Valve</a> ein Gratis-Wochenende für noch unentschlossene Spieler eingerichtet. So kann man aktuell über den Online-Dienst Steam <strong>Team Fortress 2</strong> kostenlos herunterladen und bis zum Sonntag spielen. Weiterhin wurde vor kurzem das WAR-Update veröffentlicht.</p> '
featured_image: /wp-content/uploads/2009/08/tf2.jpg

---
Bei dem Wettstreit zwischen der Soldier- und der Demoman-Klasse, haben die Spieler des Soldiers mit einem knappen Vorsprung gewonnen und so ein Spezialitem freigeschaltet. Die <a href="http://www.teamfortress.com/war/victory/" target="_blank">Gunboats</a> sind Stahlkappenstiefel mit denen der Soldier weniger Schaden bei Rocketjumps nimmt. Mit dem WAR-Update wurden zahlreiche Änderungen vorgenommen. Die Liste dieser könnt ihr auf der [zweiten Seite][1] einsehen.

 

* * *



* * *

 



 

**<img class="caption alignright size-full wp-image-368" src="http://www.gamevista.de/wp-content/uploads/2009/08/tf2.jpg" border="0" alt="Team Fortress 2" title="Team Fortress 2" align="right" width="140" height="100" />New Content**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Added new control point map Gorge
  </li>
  <li>
    Added new capture the flag map Double Cross
  </li>
  <li>
    Added item crafting
  </li>
  <li>
    Added work-in-progress TF Bots for beta testing in KOTH maps (blog post coming with more info)
  </li>
  <li>
    Added headshot death animations
  </li>
  <li>
    Added more backstab animations
  </li>
</ul>

**Soldier**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Added 35 new Soldier achievements
  </li>
  <li>
    Added 4 new Soldier items
  </li>
  <li>
    Added new Soldier Domination lines
  </li>
</ul>

**Demoman**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Added 35 new Demoman achievements
  </li>
  <li>
    Added 3 new Demoman items
  </li>
  <li>
    Added new Demoman Domination lines
  </li>
</ul>

**Spy**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Now pretends to be carrying the weapons & wearables of the target he&#8217;s disguised as
  </li>
  <li>
    Moved the Spy&#8217;s camera-beard to the Misc loadout slot so he can equip it with a hat
  </li>
</ul>

**Scout**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    The Sandman now only stuns on a max range hit (when you hear the cheering)
  </li>
  <li>
    All shorter hits now force the enemy into the thirdperson fleeing state (also removed the damage reduction on them)
  </li>
</ul>

**Minor changes**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Added new options to the Multiplayer->Advanced dialog:<br /> <ul style="padding-bottom: 0px; margin-bottom: 0px;">
      <li>
        Combat text, which displays damage amounts you do to enemies.
      </li>
      <li>
        Medic auto caller, which automatically shows you nearby friends at low health.
      </li>
      <li>
        Heal target marker, which better highlights the target your medigun is locked onto.
      </li>
      <li>
        Alternative Spy disguise menu, which lets you choose disguises using just the 1-3 keys.
      </li>
    </ul>
  </li>
  
  <li>
    Fixed being able to affect friendly pipes with airblast (they would unstick)
  </li>
  <li>
    Added recharge sound to abilities with a recharge bar
  </li>
  <li>
    Fixed attachable wearables not staying on ragdolls
  </li>
  <li>
    Players no longer see the wearables that are a part of a friendly spy&#8217;s disguise, fixing various graphical glitches
  </li>
  <li>
    Added jiggle bones for Pyro&#8217;s chicken hat
  </li>
  <li>
    Added 2 new game startup songs
  </li>
</ul>

**Community Mapmaker requests**

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Implemented InputSetSetupTime() for team_round_timer (was in the .fgd but never implemented)
  </li>
  <li>
    Added &#8222;SetDispenserLevel&#8220; input to cart dispenser (1, 2, or 3)
  </li>
</ul>

* * *



* * *

 [1]: news/team-fortress-2-gratis-wochenende-auf-steam/seite-2