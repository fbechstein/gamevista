---
title: Bioshock 2 – Bewegte Bilder zum Multiplayer
author: gamevista
type: news
date: 2009-11-19T19:41:21+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Entwickler <a href="http://www.bioshock2game.com/" target="_blank">2K Marin</a> hat heute neue bewegte Bilder zum zweiten Teil von <strong>Bioshock </strong>veröffentlicht. So wird es im Ego-Shooter <strong>Bioshock 2</strong> endlich möglich sein gegen menschliche Kontrahenten antreten zu können. Das knapp einminütige Video zeigt diverse Spielszenen und macht Hoffnung auf mehr.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
[> Zum Bioshock 2 &#8211; Multiplayer Trailer][1]

 







 

 [1]: videos/item/root/bioshock-2-zweiter-multiplayer-trailer