---
title: 'Battlefield: Bad Company 2 – VIP Map Pack 7 erschienen'
author: gamevista
type: news
date: 2010-12-01T18:44:27+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Publisher <strong>Electronic Arts</strong> liefert weitere Inhalte für den Mehrspieler-Shooter <a href="http://www.battlefieldbadcompany2.com" target="_blank">Battlefield: Bad Company 2</a>. Kurz nachdem der Veröffentlichungstermin für das erste Addon mit dem Namen Vietnam bekannt gegeben wurde, ist heute das bereits siebte VIP Map Pack erschienen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Endlich gibt es wieder neue Mehrspieler-Karten und einige sehr gute Verbesserungen zu bestaunen. Im Rahmen der Veröffentlichung des VIP-Map-Pack 7 hat der Publisher auch den passenden Launch-Trailer zur Verfügung gestellt.

[> Zum Battlefield: Bad Company 2 – VIP Map Pack 7 Trailer][1]

   

* * *

   



 [1]: videos/item/root/battlefield-bad-company-2-vip-map-pack-7-launch-trailer