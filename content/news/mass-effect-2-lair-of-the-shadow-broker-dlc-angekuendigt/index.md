---
title: Mass Effect 2 – Lair of the Shadow Broker DLC angekündigt
author: gamevista
type: news
date: 2010-07-22T20:32:25+00:00
excerpt: |
  |
    <p><strong>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Bioware </strong>hat den nächsten Zusatzinhalt für den Action-Rollenspielmix <a href="http://masseffect.bioware.com/info/dlc/" target="_blank">Mass Effect 2</a> angekündigt. <strong>Lair oft he Shadow Broker</strong> wird der DLC heißen und handelt von den aus beiden Mass Effect-Teilen bekannten Informationshändler Namens Shadow Broker. Als neuer Charakter für Commander Shepards Team wird Liara T'Soni eingeführt.</p>
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Diese war eine ständige Begleiterin im ersten Teil. Wann ihr **The Lair of the Shadow Broker** kaufen könnt und zu welchem Preis ist bisher nicht bekannt.

 

   

* * *

   

