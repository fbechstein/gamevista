---
title: Medal of Honor – Open Beta-Client ab sofort verfügbar
author: gamevista
type: news
date: 2010-10-04T16:18:34+00:00
excerpt: '<p>[caption id="attachment_2302" align="alignright" width=""]<img class="caption alignright size-full wp-image-2302" src="http://www.gamevista.de/wp-content/uploads/2010/10/moh2010.jpg" border="0" alt="Medal of Honor" title="Medal of Honor" align="right" width="140" height="100" />Medal of Honor[/caption]Ab heute startet die offene Beta-Phase für den nächsten Teil von <a href="http://www.medalofhonor.com" target="_blank">Medal of Honor</a>. Der Ego-Shooter sorgte vor allem in der letzten Zeit für reichlich Schlagzeilen durch die spielbare Fraktion der Taliban. Diese wurde nun entschärft, Taliban heißt ab nun Opposing Force.</p> '
featured_image: /wp-content/uploads/2010/10/moh2010.jpg

---
Damit hat sich der Publisher **EA** dem Druck der Medien und Öffentlichkeit gebeugt und lenkte ein.

Vom 4. bis 7. Oktober werden interessierte Spieler die Möglichkeit haben den neusten Teil ausgiebig zu testen. Den Beta-Client findet ihr ab sofort bei uns als Download. Als Inhalt zählen die beiden Karten Shahikot Mountains und Kunar Base, die sowohl im Sector Control- und im Combar Missions-Modus gespielt werden können.

[> Zum Medal of Honor &#8211; Open Beta Client][1]  
 __

   

* * *

   



 [1]: downloads/demos/item/demos/medal-of-honor--open-beta-client