---
title: Civilization 5 – Mehrspieler-Modus wird voraussichtlich nachgereicht
author: gamevista
type: news
date: 2010-06-29T17:33:58+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Die Entwickler von<strong> Firaxis Games</strong> haben im Rahmen eines Interviews bestätigt, dass der Mehrspieler-Modus des Strategiespiels <a href="http://www.civilization5.com" target="_blank">Civilization 5</a>, erst nach dem Release per Patch auf den vollen Umfang erweitert wird. Dennis Shirk, Produzent von <a href="http://www.civilization5.com" target="_blank">Civilization 5</a>, sagte aber dass die Finale Version die am 24. September in den Handel kommen wird, auch schon über grundlegende Multiplayer-Features verfügt.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Ob **Firaxis Games** auch auf den Zug der herunterladbaren Inhalte (DLC) aufspringen wird ist bisher unklar. Ob die zusätzlichen Mehrspieler-Modis kostenlos sein werden wird sich daher noch zeigen.

 

   

* * *

   

