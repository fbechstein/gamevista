---
title: Bioshock 2 – Download-Inhalte für PS3, Xbox 360 und PC
author: gamevista
type: news
date: 2010-02-22T20:18:37+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Entwickler <strong>2K Games</strong> hat den ersten Download-Inhalt für den Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> angekündigt. Neue Inhalte soll es zukünftig sowohl für Einzel- bzw. Mehrspieler-Modus geben. Mit dem „<strong>Sinclair Solutions Test Pack</strong>“ erscheint bereits im März 2010 das erste Download-Paket für den Mehrspieler-Modus. Mit 3,99 EUR  bzw. 400 Microsoftpunkten sind PlayStation 3 und Xbox 360 Besitzer mit von der Partie.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Mit einer Rang-Steigerung bis Level 50, neuen spielbaren Charakteren, 20 neuen Prüfungen, einer dritten Waffenverbesserung und fünf zusätzlichen Masken wird das erste Download-Paket im März bereit stehen.

 

* * *



* * *