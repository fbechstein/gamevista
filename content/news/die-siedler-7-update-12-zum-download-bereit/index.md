---
title: Die Siedler 7 – Update 1.2 zum Download bereit
author: gamevista
type: news
date: 2010-04-06T16:55:06+00:00
excerpt: '<p>[caption id="attachment_1662" align="alignright" width=""]<img class="caption alignright size-full wp-image-1662" src="http://www.gamevista.de/wp-content/uploads/2010/04/small3.jpg" border="0" alt="Die Siedler 7" title="Die Siedler 7" align="right" width="140" height="100" />Die Siedler 7[/caption]Publisher Ubisoft hat das nächste Update für das Aufbauspiel <a href="http://www.siedler.de.ubi.com" target="_blank">Die Siedler 7</a> veröffentlicht. Das Update mit der Versionsnummer 1.2 mit einer Größe von gerade mal 25 Megabyte bietet nicht allzu viele Änderungen. Der Fokus liegt in der Verbesserung der Stabilität des Spiels.</p> '
featured_image: /wp-content/uploads/2010/04/small3.jpg

---
[> Zum Die Siedler 7 &#8211; Patch 1.2 (Full)][1]

[> Zum Die Siedler 7 &#8211; Patch 1.2 (1.1 to 1.2)][2]

   

* * *

   



 [1]: downloads/patches/item/patches/die-siedler-7-patch-12-full
 [2]: downloads/patches/item/patches/die-siedler-7-patch-12-11-to-12