---
title: Die Siedler 7 – Ubisoft kündigt ersten DLC an
author: gamevista
type: news
date: 2010-07-05T16:49:10+00:00
excerpt: '<p>[caption id="attachment_1984" align="alignright" width=""]<img class="caption alignright size-full wp-image-1984" src="http://www.gamevista.de/wp-content/uploads/2010/07/small1.jpg" border="0" alt="Die Siedler 7" title="Die Siedler 7" align="right" width="140" height="100" />Die Siedler 7[/caption]Publisher <strong>Ubisoft </strong>hat nun offiziell den ersten Zusatzinhalt für das Strategiespiel <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Die Siedler 7</a> bestätigt. Der DLC bietet für 3,99 Euro drei neue Karten und kann ab sofort im Ubisoft-Shop vorbestellt werden.</p> '
featured_image: /wp-content/uploads/2010/07/small1.jpg

---
Die neuen Karten im Überblick:

**Stürmische Taiga**

Diese 2-Spiele-Karte stellt dich vor die Herausforderung die geteilte Metropole Tanholm wiedervereinen und sie zurück zu alter Stärke führen. Inner- und außerhalb der Stadtmauern befindet sich viel fruchtbares Land. Das Reich wird ebenso schwer bewacht durch Festungen.

  * Spiele aufregende 2-Spieler Matches
  * Siedle in 32 teilweise Riesengrößen Sektoren
  * Expandiere dein Reich ausgehend von einem stark befestigten Sektor mit vorgebauter kleiner Wirtschaft
  * Entdecke die Kathedrale und die Antike Bücherei
  * Verschaffe dir Zugang zur Wassermühle und zur Kanonengießerei und nutze sie zu deinem Vorteil

**Schlacht um Tanholm**

Diese 4-Spieler-Karte stellt dich vor die Herausforderung, die wilde Taiga Küste im Norden von Tandria zu entdecken und die Schönheit der zerklüfteten Küste mit vielen Fjorden zu erleben. Auf der Karte befinden sich nur ein paar Kohleminen, aber sehr ertragreiche, dichte Wälder, die für Köhlerhütten genutzt werden können.

  * Bestreite aufregende 4-Spieler-Matches
  * Messe dich mit anderen auf einer gigantischen Karte mit 60 Sektoren
  * Entdecke den Alrauenhügel, die Knochenkirche, das Laboratorium und den verwunschenen Wald
  * Verschaffe dir Zugang zur Wassermühle und der Kanonengießerei und nutze sie zu deinem Vorteil

**Feuerschlucht**

Ein massiver, lavaspeiender Vulkan fordert dich auf dieser 3-Spieler-Karte heraus. Die Lavaströme des Vulkans pflügen durchs Land und die Grünflächen. Kämpfe um die Vorherrschaft in diesem Land der Konflikte.

  * Spiele fesselnde 3-Spieler-Matches
  * Erschaffe dein Königreich in 40 teilweise riesengroßen Sektoren
  * Starte deinen Eroberungsfeldzug in den verbliebenen fruchtbaren Flächen rund um den mächtigen Vulkan
  * Erforsche den feurigen Vulkan im Zentrum der Karte
  * Verschaffe dir Zugang zur Kanonengießerei und nutze sie zu deinem Vorteil

   

* * *

   

