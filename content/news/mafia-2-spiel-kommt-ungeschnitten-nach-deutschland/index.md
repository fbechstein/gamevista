---
title: Mafia 2 – Spiel kommt ungeschnitten nach Deutschland
author: gamevista
type: news
date: 2010-06-28T15:52:56+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]Gute Nachrichten für alle Fans des Actionspiels <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a>. Die Unterhaltungssoftware Selbstkontrolle, kurz USK, hat dem Titel zwar keine Jugendfreigabe erteilt, trotzdem wird <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> komplett ungeschnitten auf den Markt kommen. Dies bestätigte der Entwickler <strong>2K Games</strong> im Rahmen einer Pressemitteilung.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
<a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> wird am 27. August für PlayStation 3, Xbox 360 und PC in den Handel kommen.

 

   

* * *

   

