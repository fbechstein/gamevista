---
title: Mass Effect 2 – Neuer DLC im April
author: gamevista
type: news
date: 2010-03-22T22:30:23+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Der Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Pressemitteilung einen neuen Download Content für das Actionrollenspiel <a href="http://www.masseffect.bioware.com/" target="_blank">Mass Effect 2</a> angekündigt. Der DLC führt einen neuen Charakter in Shepards Gruppe ein. Die Meisterdiebin Kasumi Goto wird von Cerberus rekrutiert und bei einem gefährlichen Raub von Commander Shepard unterstützt.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Dabei geht es darum in einen Tresorraum des tödlichen Meisterverbrechers Donovan Hock einzudringen. Neben neuen Missionen und dem Charakter Kasumi Goto, wird es auch ein neues Forschungsupgrade, eine neue Waffe sowie neue Erfolge geben. Das Erweiterungspaket Katsumi – Gestohlene Erinnerungen erscheint im April für Xbox 360 und PC. 

 

<cite></cite>

   

* * *

   



 