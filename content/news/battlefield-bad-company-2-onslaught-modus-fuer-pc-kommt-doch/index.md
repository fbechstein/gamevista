---
title: 'Battlefield: Bad Company 2 – Onslaught-Modus für PC kommt doch!'
author: gamevista
type: news
date: 2010-07-13T21:01:35+00:00
excerpt: '<p>[caption id="attachment_2010" align="alignright" width=""]<img class="caption alignright size-full wp-image-2010" src="http://www.gamevista.de/wp-content/uploads/2010/07/badcompany2logo.jpg" border="0" alt="www.badcompany2.ea.com" title="www.badcompany2.ea.com" align="right" width="140" height="100" />www.badcompany2.ea.com[/caption]Die Konsolenfans durften den neuen Onslaught-Modus für den Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> ja schon zu genüge auf Herz und Nieren testen. Nun sollen auch PC-Spieler in den Genuss des Koop-Modus kommen. Dies bestätigten die Entwickler von <strong>DICE </strong>nun offiziell der Presse. Fraglich ist wann der Zusatzinhalt für den PC veröffentlicht wird und wie viel er kosten soll.</p> '
featured_image: /wp-content/uploads/2010/07/badcompany2logo.jpg

---
Auf den angepassten Karten Isla Inocentes, Valparaiso, Atacama Desert und Nelson Bay können bis zu vier Spieler als Koop-Team gegen die KI antreten.

 

   

* * *

   

