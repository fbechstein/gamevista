---
title: Bioshock 2 – Inhalte des nächsten DLC-Pakets vorgestellt
author: gamevista
type: news
date: 2010-04-22T19:47:17+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]<strong>2K Games</strong> hat nun neue Details zum nächsten herunterladbaren Zusatzpaket für den Ego-Shooter <a href="http://www.bioshock2game.com/" target="_blank">Bioshock 2</a> preisgegeben. Das <strong>Rapture Metro</strong> Download-Content-Pack wird neue Inhalte für den Mehrspielermodus mit sich bringen. Demnach werden neben sechs neuen Karten und dem neuen Deathmatch-Modus <strong>Kill`em Kindly</strong>, auch drei weitere Masken hinzugefügt.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Natürlich dürfen da neue Archievements nicht fehlen. Das **Rapture Metro** Pack für <a href="http://www.bioshock2game.com/" target="_blank">Bioshock 2</a> erscheint am 29. April für 800 Microsoft Punkte.

 

 

   

* * *

* * *