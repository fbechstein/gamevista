---
title: Deep Silver – Gamescom LineUp
author: gamevista
type: news
date: 2009-08-06T21:05:59+00:00
excerpt: '<p>[caption id="attachment_303" align="alignright" width=""]<img class="caption alignright size-full wp-image-303" src="http://www.gamevista.de/wp-content/uploads/2009/08/risen_packshot.png" border="0" alt="Risen gehört zum absolutem Highlight des Deep Silver Standes" title="Risen gehört zum absolutem Highlight des Deep Silver Standes" align="right" width="140" height="100" />Risen gehört zum absolutem Highlight des Deep Silver Standes[/caption]Neben einer riesigen <strong>Risen</strong> Area mit 30 Spielstationen (<a href="news/risen-prasentation-auf-der-gamescom">wir berichteten</a>) wird <a href="http://www.deepsilver.com/" target="_blank">Deep Silver</a> auf der <strong>gamescom</strong> noch weitere Hits präsentieren. Hierzu zählen:</p>'
featured_image: /wp-content/uploads/2009/08/risen_packshot.png

---
  * Risen 
  * Cursed Mountain 
  * Lost Horizon 
  * Sacred 2: Ice & Blood 
  * The Whispered World 
  * Geheimakte 2: Puritas Cordis 
  * Dissidia: Final Fantasy 
  * Batman: Arkham Asylum





