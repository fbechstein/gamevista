---
title: Champions Online – Lifetime-Abo verfügbar
author: gamevista
type: news
date: 2010-11-14T17:24:53+00:00
excerpt: '<p>[caption id="attachment_122" align="alignright" width=""]<img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" />Champions Online[/caption]Der Entwickler <strong>Cryptic Studios</strong> hat für das Online-Rollenspiel <a href="http://www.champions-online.com" target="_blank">Champions Online</a> das sogenannte Lifetime-Abo freigeschaltet. Damit könnt ihr für einen Zahlung von 300 USD unbegrenzte Spielzeit und weitere Extras erwerben. Warum sich der Entwickler dazu entschied dieses Angebot bereitzustellen, obwohl im nächsten Jahr der Titel als free 2 play-Variante auf den Markt kommen soll, ist unklar.</p> '
featured_image: /wp-content/uploads/2009/07/champions_online_small.jpg

---
Als Lifetime-Abonnementen wird euer Account dann automatisch in einen Gold-Account umgewandelt. Die Extras im Überblick:

 

  * Eine Foxbat Actionfigur
  * Zwei Kostüm-Sets
  * Acht Charakter-Slots
  * Zugang zum VIP-Raum
  * Exklusive Kostümteile
  * Exklusive Perks und Titel

   

* * *

   

