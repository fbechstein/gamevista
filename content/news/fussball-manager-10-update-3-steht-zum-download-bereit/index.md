---
title: Fussball Manager 10 – Update 3 steht zum Download bereit
author: gamevista
type: news
date: 2010-01-12T18:06:04+00:00
excerpt: '<p>[caption id="attachment_1296" align="alignright" width=""]<img class="caption alignright size-full wp-image-1296" src="http://www.gamevista.de/wp-content/uploads/2010/01/fussball-manager10.jpg" border="0" alt="Fussball Manager 10" title="Fussball Manager 10" align="right" width="140" height="100" />Fussball Manager 10[/caption]Der <strong>Fussball Manager 10</strong> vom Publisher <a href="http://www.fm09.de/publish/de/home/news/news.html" target="_blank">Electronic Arts</a> hat ein neues Update spendiert bekommen. Der sehr umfangreiche Patch, mit einer Größe von 328 Megabyte, behebt einige Fehler und beseitigt Abstürze aus dem Programmcode. Außerdem wurden viele Funktionen verbessert und zahlreiche Optionen überarbeitet.</p> '
featured_image: /wp-content/uploads/2010/01/fussball-manager10.jpg

---
Neben den vielen Verbesserungen wurden noch 2000 neue Spielerbilder importiert, diese könnt ihr über den Autoupdater herunterladen.  
[  
> Zum Fussball Manager 10 &#8211; Update 3 Download][1]

**<span style="text-decoration: underline;">Hier noch die Liste der Änderungen:</span>**

&#8211; Original-Trikots in der 3. Liga und in den Regionalligen in Deutschland   
&#8211; Option, die Sätze im Live Ticker satzweise auszugeben   
&#8211; Option, die Sätze im Live Ticker wortweise auszugeben   
&#8211; beendete Spieler im Live Ticker werden jetzt in einer anderen Farbe dargestellt   
&#8211; keine Kommentare mehr in der Konferenz im Live Ticker (besserer Überblick)   
&#8211; viele neue Spielerbilder and XXL-Spielerbilder mit Schwerpunkt UK (1.786 neue Bilder, 200 zusätzliche XXL-Bilder)   
&#8211; viele neue Städtebilder (103) und insgesamt 332 Vereine mit einem neuen Bild   
&#8211; Auf- und Abwertungen überarbeitet und schwieriger   
&#8211; Reserveligen zu verschiedenen Ländern hinzugefügt (z.B. Bulgarien)   
&#8211; Spieler können behalten werden, obwohl die Scouting-Liste gelöscht wird   
&#8211; bis zu 20 Spieler können mit einem Klick gescoutet werden (kein Drag & Drop mehr für jeden einzelnen Spieler)   
&#8211; FM RSS-Ticker integriert   
&#8211; ONLINE: Tore und Note werden in der Spielerinfo angezeigt, zusätzliche Warnmeldungen   
&#8211; ONLINE: Neuer Tooltipp über den Zustand eines Spielers   
&#8211; ONLINE: Erweiterung der Highscoreliste auf 50.000 Spieler 

**Bug Fixes:** 

&#8211; Absturz bei der Vereinsgründung in Portugal beseitigt   
&#8211; keine Vertragsangebote an Spieler möglich, die noch mehr als 6 Monate unter Vertrag stehen   
&#8211; weniger Talentänderungen bei Spielern mit hohen Talentwerten, mehr bei geringem Talent   
&#8211; maximal 10-Jahres-Vertrag am Spielbeginn   
&#8211; Farben funktionieren jetzt im Stadioneditor   
&#8211; Fanbannerprobleme im Stadioneditor beseitigt   
&#8211; Größe und Gewicht des Spielertrainers werden jetzt korrekt übernommen   
&#8211; Problem mit der Vorschau eines Meisterrundenspiels (Crash Team>Privatleben)   
&#8211; Überlagerung des Aufstellungsscreens im 3D-Spiel beseitigt (Ausnahme Verletzungen)   
&#8211; weniger Elfmeter im 3D-Spiel   
&#8211; weniger Handspiele im 3D-Modus   
&#8211; Torhüter nicht mehr so stark wie nach Update 2   
&#8211; Spielervorschläge mit Shift in der Aufstellung in der Halbzeit   
&#8211; Hall of Fame: Automatischer Eintrag nach x Spielen (Optionen) funktioniert jetzt

<cite></cite>

* * *



* * *

 

 [1]: downloads/patches/item/patches/fussball-manager-10-update-3