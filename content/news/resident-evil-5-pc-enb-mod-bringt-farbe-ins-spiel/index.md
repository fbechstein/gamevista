---
title: 'Resident Evil 5 (PC): ENB-Mod bringt Farbe ins Spiel'
author: gamevista
type: news
date: 2009-09-08T18:46:36+00:00
excerpt: '<p>Erst am 18. September erscheint die PC-Version von Resident Evil 5. Schon im Vorfeld veröffentlichte nun der ENB-Series-Modder Boris Vorontsov eine Mod, welche dem Spiel mehr Farben gibt und somit einem realistischeren Grafikstil.</p> '
featured_image: /wp-content/uploads/2009/09/smallsre1.jpg

---
Egal, ob du nun eher einen graulastigen oder einen farbigen Grafikstil bevorzugst &#8211; Resident Evil 5 aus dem Hause <a href="http://www.capcom-europe.com/" target="_top">Capcom</a> verspricht ein cooler Zombie-Shooter zu werden.

Hier noch ein paar Vergleichsscreenshots:

<a href="http://www.gamevista.de/wp-content/uploads/2009/09/sre1.jpg" rel="lightbox[sre1]"><img class="caption alignleft size-full wp-image-639" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallsre1.jpg" border="5" alt="Standard" title="Standard" align="left" width="140" height="100" /></a> <a href="http://www.gamevista.de/wp-content/uploads/2009/09/nre1.jpg" rel="lightbox[sre2]"><img class="caption alignleft size-full wp-image-642" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallnre1.jpg" border="5" alt="ENB-Mod" title="ENB-Mod" align="left" width="140" height="100" /></a>

 

 

 

 

 

<a href="http://www.gamevista.de/wp-content/uploads/2009/09/sre2.jpg" rel="lightbox[sre3]"><img class="caption alignleft size-full wp-image-644" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallsre2.jpg" border="5" alt="Standard" title="Standard" align="left" width="140" height="100" /></a> <a href="http://www.gamevista.de/wp-content/uploads/2009/09/nre2.jpg" rel="lightbox[sre4]"><img class="caption alignleft size-full wp-image-646" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallnre2.jpg" border="5" alt="ENB-Mod" title="ENB-Mod" align="left" width="140" height="100" /></a>

 

 

 

 

 

<a href="http://www.gamevista.de/wp-content/uploads/2009/09/sre3.jpg" rel="lightbox[sre5]"><img class="caption alignleft size-full wp-image-648" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallsre3.jpg" border="5" alt="Standard" title="Standard" align="left" width="140" height="100" /></a> <a href="http://www.gamevista.de/wp-content/uploads/2009/09/nre3.jpg" rel="lightbox[sre6]"><img class="caption alignleft size-full wp-image-650" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallnre3.jpg" border="5" alt="ENB-Mod" title="ENB-Mod" align="left" width="140" height="100" /></a>

Bei amazon.de kann man die PC-Version, welche auch das neue 3D-Vision von GeForce unterstützen wird, mit Steelbock für 39,95 € vorbestellen. Dies ist verglichen mit den Preis für die Standardversion von Modern Warfare 2 (rund 60 €) schon fast ein Schnäppchen.

 

* * *



* * *