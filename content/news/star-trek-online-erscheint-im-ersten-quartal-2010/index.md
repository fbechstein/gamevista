---
title: Star Trek Online – Erscheint im ersten Quartal 2010
author: gamevista
type: news
date: 2009-10-20T17:16:52+00:00
excerpt: '<p>[caption id="attachment_915" align="alignright" width=""]<img class="caption alignright size-full wp-image-915" src="http://www.gamevista.de/wp-content/uploads/2009/10/startrek_online.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Der Executive Producer Craig Zinkievich vom Entwicklerstudio <a href="http://www.startrekonline.com/" target="_blank">Cryptic</a> hat heute bekannt gegeben das das Onlinerollenspiel <strong>Star Trek Online</strong> bereits im ersten Quartal 2010 erscheinen wird. Weiterhin wurde mitgeteilt das die Closed Beta zum Rollenspiel in den Startlöchern steht.</p> '
featured_image: /wp-content/uploads/2009/10/startrek_online.jpg

---
In wenigen Tagen soll diesbezüglich eine Pressemitteilung den Start der Closed Beta verkünden und weitere Details verraten.

 

* * *



* * *

 