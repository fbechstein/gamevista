---
title: 'Top Demo: Call of Juarez: Bound in Blood'
author: gamevista
type: news
date: 2009-08-14T10:11:14+00:00
excerpt: '<p>[caption id="attachment_270" align="alignright" width=""]<img class="caption alignright size-full wp-image-270" src="http://www.gamevista.de/wp-content/uploads/2009/08/call_of_juarez_small.jpg" border="0" alt="Call of Juarez: Bound in Blood" title="Call of Juarez: Bound in Blood" align="right" width="140" height="100" />Call of Juarez: Bound in Blood[/caption]Unser Top Demo der Woche ist ganz klar <strong>Call of Juarez: Bound in Blood</strong>. Der WildWest Shooter von <a href="http://www.techland.pl/" target="_blank">Techland</a> ist bereits am 02.07.2009 erschienen, alle Unentschlossenen haben jedoch nun die Möglichkeit sich im Wilden Westen auszutoben.</p> '
featured_image: /wp-content/uploads/2009/08/call_of_juarez_small.jpg

---
Die Demo ist 1 GB gross und kann direkt von unseren Servern heruntergeladen werden. Alle Xbox 360- und PS3 Konsolenbesitzer haben ebenfalls die Möglichkeit das Spiel anzutesten. Die Demo steht jeweils unter Xbox Live bzw. Playstation Store zum Download zur Verfügung.

<a href="index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=176" target="_self">> Demo zu Call of Juarez: Bound in Blodd jetzt herunterladen</a>

* * *



* * *

 