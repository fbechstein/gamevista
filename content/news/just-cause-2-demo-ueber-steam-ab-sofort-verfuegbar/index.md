---
title: Just Cause 2 – Demo über Steam ab sofort verfügbar
author: gamevista
type: news
date: 2010-03-05T19:08:44+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]In genau drei Wochen wird das zweite Abenteuer rund um den Spezialagenten Rico Rodriguez in den Handel kommen. Nun hat der Publisher <strong>Square Enix</strong> die <a href="http://store.steampowered.com/app/35110/" target="_blank">Demoversion</a> zu <a href="http://www.justcause.com" target="_blank">Just Cause 2</a> auf der Onlineplattform <a href="http://store.steampowered.com/app/35110/" target="_blank">Steam</a> bereitgestellt. Als Umfang gibt der Publisher die 90 Quadratkilometer große Lautan-Lama-Wüste an, eine riesige, weitreichende Fläche mit einer atemberaubenden, sonnenverbrannten Szenerie.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
Dies ist nur eine der vielen Gebiete auf der über 1000 Quadratkilometer großen Inselgruppe Panau. Vollgepackt mit Städten, Dörfern, militärischen Stützpunkten, Gebirgszügen, Missionen und vielem mehr. <a href="http://www.justcause.com/" target="_blank">Just Cause 2 </a>wird für PC. Xbox 360 und PlayStation 3 am 26. März in den Handel kommen.

 

* * *



* * *