---
title: 'Battlefield: Bad Company 2 – Zweites VIP Mappack ab sofort verfügbar'
author: gamevista
type: news
date: 2010-03-30T18:55:04+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Der Entwickler <strong>DICE </strong>stellt ab heute, im Rahmen des zweiten VIP Mappack, weitere Spielmodi für die bekannten Karten Laguna Presa und Arica Harbour vor. So ist es ab sofort möglich die beiden Maps im Rush bzw. im Conquest-Modus zu spielen. Ob bald weitere neue Karten kostenlos im Zuge eines Download-Contents erscheinen ist unklar.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Passend zum Release des zweiten Mappacks hat der Publisher **Electronic Arts** ein Promotion-Video veröffentlicht, dass die beiden „neuen“ Karten mit Spielszenen zeigt.

[> Zum Battlefield: Bad Company 2 &#8211; VIP Mappack 2 Video][1]

 

<cite></cite>

   

* * *

   



 [1]: videos/item/root/battlefield-bad-company-2-vip-mappack-2-trailer