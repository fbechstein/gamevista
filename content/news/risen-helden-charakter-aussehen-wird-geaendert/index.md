---
title: Risen – Helden Charakter, Aussehen wird geändert
author: gamevista
type: news
date: 2009-09-01T18:15:28+00:00
excerpt: '<p>[caption id="attachment_582" align="alignright" width=""]<img class="caption alignright size-full wp-image-582" src="http://www.gamevista.de/wp-content/uploads/2009/09/risencombos.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]<a href="http://www.deepsilver.com">Deep Silver</a> reagierte nun auf den Wunsch vieler Fans und spendierte dem Protagonisten von Risen ein neues Gesicht. Denn viele Fans konnten mit dem Gesicht des namenlosen Helden nichts Anfangen. Wie die neue Version des Helden 2.0 Aussehen wird gibt <a href="http://www.deepsilver.com">Deep Silver</a> am Donnerstag zwischen 18.00 und 18.30 Uhr im Forum der deutschen Community-Seite <a href="http://forum.worldofplayers.de/forum/showthread.php?t=666767">www.worldofrisen.de</a> bekannt.</p> '
featured_image: /wp-content/uploads/2009/09/risencombos.png

---
Eine Bedingung gibt’s allerdings. Es müssen sich Minimum 400 Fans im Forum ansammeln. Denkbar ist das der neue Held nicht komplett anders Aussehen wird, denn es schon zahlreiche Spielecover und Poster im Umlauf die den alten Helden zeigen.

* * *



* * *

 