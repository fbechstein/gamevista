---
title: 'WRC: FIA World Rally Championship – Demo ab sofort verfügbar'
author: gamevista
type: news
date: 2010-09-27T16:58:37+00:00
excerpt: '<p>[caption id="attachment_2279" align="alignright" width=""]<img class="caption alignright size-full wp-image-2279" src="http://www.gamevista.de/wp-content/uploads/2010/09/wrc.jpg" border="0" alt="WRC: FIA World Rally Championship" title="WRC: FIA World Rally Championship" align="right" width="140" height="100" />WRC: FIA World Rally Championship[/caption]Im Rahmen der baldigen Veröffentlichung des Rallyespiels <a href="http://www.wrcthegame.com/DE-de/" target="_blank">WRC: FIA World Rally Championship</a>, hat der Entwickler <strong>Milestone Interactive </strong>die Demoversion des Titels bereit gestellt. Am 8. Oktober 2010 erscheint das Spiel mit den offiziellen Lizenzen der FIA World Rally Championship für Xbox 360, PlayStation 3 und PC im Handel.</p> '
featured_image: /wp-content/uploads/2010/09/wrc.jpg

---
In der Demo stehen zwei Strecken zur Auswahl. Darunter sind eine Piste in Jordanien und Finnland. Der Spieler kann zwischen einem Citroen C4 WRC oder Ford Focus RS WRC wählen. Die Demo umfasst 1 Gigabyte und steht bei uns ab sofort zum Download bereit.

[> Zum WRC: FIA World Rally Championship – Demo Download][1]

   

* * *

   



 [1]: downloads/demos/item/demos/wrc-fia-world-rally-championship--demo