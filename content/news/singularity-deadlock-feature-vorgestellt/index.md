---
title: Singularity – Deadlock-Feature vorgestellt
author: gamevista
type: news
date: 2010-05-21T20:11:41+00:00
excerpt: '<p><strong>[caption id="attachment_1824" align="alignright" width=""]<img class="caption alignright size-full wp-image-1824" src="http://www.gamevista.de/wp-content/uploads/2010/05/singularity.jpg" border="0" alt="Singularity" title="Singularity" align="right" width="140" height="100" />Singularity[/caption]Activision </strong>stellt einen neuen Trailer für das Actionspiel <a href="http://www.singularity-game.com" target="_blank">Singularity</a> vor. In diesem wird das Deadlock-Feature unter die Lupe genommen mit dem sie die Zeit manipulieren können. In einem gewissen Radius läuft dadurch die Zeit langsamer und der Spieler kann so gekonnt Gegner aussschalten.</p> '
featured_image: /wp-content/uploads/2010/05/singularity.jpg

---
<a href="http://www.singularity-game.com" target="_blank">Singularity</a> wird am 29. Juni 2010 in Europa veröffentlicht.

[> Zum Singularity &#8211; Deadlock-Feature Trailer][1]

   

* * *

   



 [1]: videos/item/root/singularity-deadlock-trailer