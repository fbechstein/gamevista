---
title: Left 4 Dead 2 – Vorbestellerbonis
author: gamevista
type: news
date: 2009-08-31T16:09:51+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Immer öfter kommt es nun vor das Entwickler sowie Publisher das Geld ihrer Kunden im Voraus haben möchten. Der Anreiz dazu sind meistens ein exklusiver Gegenstand oder eine tolle Rüstung für euren Protagonisten. Entwickler <a href="http://www.valvesoftware.com">Valve </a>spendiert den Fans des <strong>Left 4 Dead 2</strong> Survival Shooters nun einige interessante Boni. Unter anderem werden Vorbesteller die Möglichkeit haben die Demo zum Spiel früher antesten zu dürfen.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Weiterhin wird es per Code eine zusätzliche Waffe, den Baseballschläger, geben. Einen Haken hat das Ganze aber. Denn dieses Angebot gilt nur für Großbritannien, denn für Deutschland gibt es noch keine Bewertung der USK.

 

* * *



* * *