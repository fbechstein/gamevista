---
title: Greed – Die Singleplayerdemo ist da
author: gamevista
type: news
date: 2009-12-09T19:49:19+00:00
excerpt: '<p>[caption id="attachment_1160" align="alignright" width=""]<img class="caption alignright size-full wp-image-1160" src="http://www.gamevista.de/wp-content/uploads/2009/12/greed_small.png" border="0" alt="Greed" title="Greed" align="right" width="140" height="100" />Greed[/caption]Der österreichische Entwickler <a href="http://www.headupgames.com/conpresso/_rubric/index.php?rubric=DE+Games+GREED+Facts" target="_blank">Clockstone</a> hat eine Singleplayer-Demo zum Action-Rollenspiel <strong>Greed </strong>veröffentlicht. Der Grund dafür dürfte die morgige Veröffentlichung sein. Die Anspielversion, mit der Größe von 458 Megabyte, beinhaltet ein Turorial und zwei Level aus dem Singleplayer.</p> '
featured_image: /wp-content/uploads/2009/12/greed_small.png

---
Die Demo ist in englischer bzw. in deutscher Sprache gehalten und der Spieler kann in zwei verfügbare Charaktere schlüpfen, den Pyro und den Marine. Wenn ihr also einen Blick auf dieses doch sehr viel versprechende Spiel werfen wollt, könnt ihr sie kostenlos bei uns vom Server herunterladen.

[> Zum Greed &#8211; Demo Download][1]

 







 [1]: downloads/demos/item/demos/greed-demo