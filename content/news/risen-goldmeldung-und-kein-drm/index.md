---
title: Risen – Goldmeldung und kein DRM
author: gamevista
type: news
date: 2009-09-18T14:00:26+00:00
excerpt: '<p>[caption id="attachment_720" align="alignright" width=""]<img class="caption alignright size-full wp-image-720" src="http://www.gamevista.de/wp-content/uploads/2009/09/risenruinen.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Publisher <a href="http://www.deepsilver.com">Deep Silver</a> hat heute auf ihrer offiziellen Webseite bekannt gegeben das das Rollenspiel <strong>Risen </strong>den Goldstatus erreicht hat. Demnach wird der <strong>Gothic </strong>Nachfolger bereits für den Handel vorbereitet. Dem Veröffentlichungstermin am 2. Oktober 2009 dürfte damit nichts mehr im Wege stehen.</p> '
featured_image: /wp-content/uploads/2009/09/risenruinen.png

---
Weiterhin hat sich [Deep Silvers][1] Community Manager zu Wort gemeldet um ein paar Infos über den Kopierschutz weiterzugeben. So wird [Deep Silver][1] darauf verzichten den allseits beliebten DRM-Digital-Rights-Management Schutz zu nutzen. Es wird dafür ein reiner DVD Kopierschutz zum Einsatz kommen. Weiterhin wurde gesagt das die DVD nur zum Starten des Spiels benötigt wird, sie muss so nicht jeder Zeit im Laufwerk liegen.

 

* * *



* * *

 

 [1]: http://www.deepsilver.com