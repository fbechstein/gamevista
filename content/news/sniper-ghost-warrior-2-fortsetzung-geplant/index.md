---
title: 'Sniper: Ghost Warrior 2 – Fortsetzung geplant'
author: gamevista
type: news
date: 2011-01-11T20:02:35+00:00
excerpt: '<p>[caption id="attachment_1820" align="alignright" width=""]<img class="caption alignright size-full wp-image-1820" src="http://www.gamevista.de/wp-content/uploads/2010/05/sniper_ghost_warrior.jpg" border="0" alt="Sniper: Ghost Warrior" title="Sniper: Ghost Warrior" align="right" width="140" height="100" />Sniper: Ghost Warrior[/caption]Entwickler <a href="http://www.sniperghostwarrior.com" target="_blank">City Interactive</a> arbeitet derzeit an einer Fortsetzung zum Ego-Shooter <strong>Sniper: Ghost Warrior</strong>. Der zweite Teil soll mithilfe der CryEngine 3 grafisch auf dem neusten Stand der Zeit gebracht werden. Wirkliche Infos über Inhalt oder einen Veröffentlichungstermin gibt es bisher nicht.</p> '
featured_image: /wp-content/uploads/2010/05/sniper_ghost_warrior.jpg

---
 

 

   

* * *

   

