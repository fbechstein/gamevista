---
title: Bayonetta – Xbox-Demo veröffentlicht
author: gamevista
type: news
date: 2009-12-03T20:45:35+00:00
excerpt: '<p>[caption id="attachment_1141" align="alignright" width=""]<img class="caption alignright size-full wp-image-1141" src="http://www.gamevista.de/wp-content/uploads/2009/12/bayonetta.jpg" border="0" alt="Bayonetta" title="Bayonetta" align="right" width="140" height="100" />Bayonetta[/caption]Der Publisher <a href="http://www.sega.de/platinumgames/bayonetta/index2.php" target="_blank">SEGA </a>hat wie angekündigt die Anspielversion des Actiongames <strong>Bayonetta </strong>für die Xbox 360 veröffentlicht. Die Demo mit einer Größe von 770 Megabyte, findet man wie immer auf dem Xbox Live Marketplace. Unter anderem können diesmal neben den Gold- auch die Silbermitglieder sofort Downloaden. <strong>Bayonetta </strong>wird am 08. Januar 2010 in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2009/12/bayonetta.jpg

---
Weitere Details könnt ihr auf der Website des [Entwicklers][1] einsehen.

 







 

 [1]: http://www.sega.de/platinumgames/bayonetta/index2.php