---
title: Alien vs. Predator – Evtl. nicht in Deutschland?
author: gamevista
type: news
date: 2009-08-30T18:49:35+00:00
excerpt: '<p>[caption id="attachment_554" align="alignright" width=""]<img class="caption alignright size-full wp-image-554" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Alien vs. Predator" title="Alien vs. Predator" align="right" width="140" height="100" />Alien vs. Predator[/caption]Zur gamescom war <strong>Alien vs. Predator </strong>sicher eine sehr großen Blick wert und machte klar, dieses Spiel wird ein Hit. Allerdings werden Stimmen laut die sagen das ein Release in Deutschland auf der Kippe stehen könnte. Denn der Entwickler <a href="http://www.rebellion.co.uk/">Rebellion </a>plant Alien vs. Predator nicht zu zensieren bzw. für den deutschen Markt oder unsere liebe USK anzupassen.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Die Folge davon wäre ein Verbot des Ego-Shooters in Deutschland da es keine USK Einstufung bekommen würde, vorausgesetzt sie fänden Mängel. [Rebellion][1] zeigt sich ziemlich festgelegt mit ihrer eigenen Aussage „Wir werden das Spiel nicht für den deutschen Markt schneiden!“. Wir hoffen natürlich auf mehr Einsicht beider Parteien, ob USK oder [Rebellion][1]. Alien vs. Predator

 

* * *



* * *

 

 [1]: http://www.rebellion.co.uk/