---
title: Silent Hunter 5 – Kommt Anfang 2010
author: gamevista
type: news
date: 2009-08-17T21:04:15+00:00
excerpt: '<p>[caption id="attachment_384" align="alignright" width=""]<img class="caption alignright size-full wp-image-384" src="http://www.gamevista.de/wp-content/uploads/2009/08/sh4.jpg" border="0" alt="Silent Hunter 4" title="Silent Hunter 4" align="right" width="140" height="100" />Silent Hunter 4[/caption]<a href="http://www.ubi.com/DE/default.aspx" target="_blank">Ubisoft</a> hat eine weitere Fortsetzung seiner Erfolgreichen Uboot Simulation <strong>Silent Hunter 5</strong> angekündigt. Der französische Publisher legt damit den Termin auf Anfang 2010. <strong>Silent Hunter 5 </strong>wird exklusiv für PC erscheinen und viele Neuerungen enthalten. <span> </span>Unter anderem sind das technische Verbesserungen, mehr Komfort in der Bedienung und eine bessere Ego Perspektive damit die Schlachten noch besser rüberkommen.</p> '
featured_image: /wp-content/uploads/2009/08/sh4.jpg

---
Zudem soll es möglich sein in Ego-Shooter Manier durch die Bootsstationen zu streifen und der Besatzung auf die Finger zu schauen. In **Silent Hunter 5** werden sie als deutscher Uboot Kommandant in einer dynamischen Kampagne verschiedene Uboot Versionen navigieren.  <span></span>

 

 

 