---
title: 'Arcania: Gothic 4 – Releasetermin bekannt'
author: gamevista
type: news
date: 2010-06-29T19:34:27+00:00
excerpt: '<p>[caption id="attachment_1966" align="alignright" width=""]<img class="caption alignright size-full wp-image-1966" src="http://www.gamevista.de/wp-content/uploads/2010/06/burg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Publisher <strong>JoWood </strong>hat den Veröffentlichungstermin für das Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> konkretisiert. Im Rahmen eines Interviews hat der Geschäftsführer Franz Rossler den 12. Oktober 2010 als finales Datum benannt. Dabei lies der CEO verlauten das die Entwicklung des Titels bisher ganze sechs Millionen Dollar vereinnahmt haben.</p> '
featured_image: /wp-content/uploads/2010/06/burg_small.png

---
Derzeit sieht es für den Publisher auch danach aus als ob der vierte Teil sehr gut bei den Fans ankommen wird.

 

   

* * *

   

