---
title: Brink – Finaler Veröffentlichungstermin angekündigt
author: gamevista
type: news
date: 2011-01-27T19:33:06+00:00
excerpt: '<p><img class="caption alignright size-full wp-image-1143" src="http://www.gamevista.de/wp-content/uploads/2009/12/brink-game.jpg" border="0" alt="Brink" title="Brink" align="right" width="140" height="100" />Brink[/caption]Publisher <strong>Bethesda Softworks</strong> hat den Europa -Veröffentlichungstermin für den Multiplayer-Shooter <a href="http://www.brinkthegame.com" target="_blank">Brink</a> bekannt gegeben. Demnach wird der Titel von den Entwicklern der <strong>Splash Damage Studios</strong> ab dem 20. Mai 2011 im Handel stehen.</p> '
featured_image: /wp-content/uploads/2009/12/brink-game.jpg

---
 
<p><img class="caption alignright size-full wp-image-1143" src="http://www.gamevista.de/wp-content/uploads/2009/12/brink-game.jpg" border="0" alt="Brink" title="Brink" align="right" width="140" height="100" />Brink[/caption]Publisher <strong>Bethesda Softworks</strong> hat den Europa -Veröffentlichungstermin für den Multiplayer-Shooter <a href="http://www.brinkthegame.com" target="_blank">Brink</a> bekannt gegeben. Demnach wird der Titel von den Entwicklern der <strong>Splash Damage Studios</strong> ab dem 20. Mai 2011 im Handel stehen.</p>



* * *