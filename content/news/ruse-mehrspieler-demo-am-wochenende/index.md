---
title: R.U.S.E. – Mehrspieler-Demo am Wochenende
author: gamevista
type: news
date: 2010-07-14T17:05:49+00:00
excerpt: '<p>[caption id="attachment_2013" align="alignright" width=""]<img class="caption alignright size-full wp-image-2013" src="http://www.gamevista.de/wp-content/uploads/2010/07/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Publisher <strong>Ubisoft </strong>kündigte vor einiger Zeit eine große Überraschung für ihr bald erscheinendes Strategiespiel <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a> an. Nun wurde das Geheimnis gelüftet. Ab dem kommenden Wochenende wird es möglich sein<a href="http://www.rusegame.com" target="_blank"> R.U.S.E.</a> im Mehrspieler-Modus ausgiebig zu testen. Der Client kann bereits ab morgen den 15. Juli bei Steam heruntergeladen werden.</p> '
featured_image: /wp-content/uploads/2010/07/ruse_small.jpg

---
Die Demo bietet Platz für zwei bis acht Spieler auf verschiedenen Karten. Dabei stehen die sechs Nationen Frankreich, Italien, Deutschland, England, USA und Russland zur Auswahl.

 

 

   

* * *

   

