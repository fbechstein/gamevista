---
title: PlayStation 3 – Nächstes Update mit 3D-Support
author: gamevista
type: news
date: 2010-05-31T17:28:36+00:00
excerpt: '<p>[caption id="attachment_1863" align="alignright" width=""]<img class="caption alignright size-full wp-image-1863" src="http://www.gamevista.de/wp-content/uploads/2010/05/ps3slim_small.png" border="0" alt="PlayStation 3" title="PlayStation 3" align="right" width="140" height="100" />PlayStation 3[/caption]Im Juni ist es soweit. Dann wird die <a href="http://us.playstation.com/" target="_blank">PlayStation 3</a> mit dem Firmware-Update 3.20 den erhofften 3D-Support erhalten. So dürfen sich  glückliche Besitzer eines 3D-fähigen Fernsehers auf den 10. Juni 2010 freuen. Ab diesem Zeitpunkt können Spiele wie z.B. Stardust HD , Wipeout oder Motorstorm: Pacific in 3D genossen werden.</p> '
featured_image: /wp-content/uploads/2010/05/ps3slim_small.png

---
PS3 Besitzer die ihre Blu-rays in 3D anschauen wollen, müssen sich noch bis zum Ende des Jahres gedulden.



 

   

* * *

   

