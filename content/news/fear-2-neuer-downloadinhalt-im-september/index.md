---
title: F.E.A.R. 2 – Neuer Downloadinhalt im September
author: gamevista
type: news
date: 2009-07-30T11:16:26+00:00
excerpt: '<p>[caption id="attachment_210" align="alignright" width=""]<img class="caption alignright size-full wp-image-210" src="http://www.gamevista.de/wp-content/uploads/2009/07/fear2_small.jpg" border="0" alt="F.E.A.R. 2" title="F.E.A.R. 2" align="right" width="140" height="100" />F.E.A.R. 2[/caption]Wie der Entwickler von F.E.A.R. 2 nun bekannt gab wird es im September 2009 neue Download - Inhalte geben. Das Packet nennt sich „Reborn“ und wird insgesamt 4 neue Singleplayer Maps beinhalten. Der Releasetermin wird laut Monolith der 03. September 2009 sein. </p>'
featured_image: /wp-content/uploads/2009/07/fear2_small.jpg

---
Neben den Maps erhaltet ihr noch ein neues Belohnungssystem „Trophys & Archievements“ für die Nextgen Konsolen Playstation 3 und Xbox 360. Über eine PC Version ist bisher nichts bekannt, es wird aber vermutet das diese schnell nachgeliefert werden wird. 







 