---
title: Dead Space 2 – Käufer erhalten Rüstung für Dragon Age 2
author: gamevista
type: news
date: 2011-01-23T18:30:44+00:00
excerpt: '<p>[caption id="attachment_1151" align="alignright" width=""]<img class="caption alignright size-full wp-image-1151" src="http://www.gamevista.de/wp-content/uploads/2009/12/deadspace2.jpg" border="0" alt="Dead Space 2" title="Dead Space 2" align="right" width="140" height="100" />Dead Space 2[/caption]Wie Publisher <strong>Electronic Arts</strong> kürzlich per Pressemitteilung bekannt gab, werden Käufer des Horror-Shooters <a href="http://www.deadspace.ea.com/" target="_blank">Dead Space 2</a> ein exklusives Item für das Rollenspiel <strong>Dragon Age 2</strong> erhalten. Das Ser Isaac von Clarke-Rüstungsset ist der einzigartigen Panzerung von Issac Clarke nachempfunden und kann mit einem Code aktiviert werden der <a href="http://www.deadspace.ea.com/" target="_blank">Dead Space 2</a> beiliegt.</p> '
featured_image: /wp-content/uploads/2009/12/deadspace2.jpg

---
Das Set besteht aus einer Körperrüstung, Stiefeln, Handschuhen sowie einem Helm. Natürlich dürfen da besondere Vorteile und Boni nicht fehlen. <a href="http://www.deadspace.ea.com/" target="_blank">Dead Space 2</a> erscheint am 3. Februar 2011 für PlayStation 3, Xbox 360 und PC. **Dragon Age 2** erscheint am 10. März 2011 für dieselben Plattformen.

 

Das Ser Isaac von Clarke-Rüstungsset besteht aus:

  * Ser Isaacs Rüstung

o    Schwere Rüstung

o    Verfügt über ein Runenfeld

o    Erhöht den Rüstungswert

o    Erfordert hohe Geschicklichkeit und Klugheit

  * Ser Isaacs Stiefel

o    Erhöhen den Rüstungswert

o    Erfordern hohe Geschicklichkeit und Klugheit

  * Ser Isaacs Handschuhe

o    Erhöhen den Rüstungswert

o    Erfordern hohe Geschicklichkeit und Klugheit

  * Ser Isaacs Helm

o    Verfügt über ein Runenfeld

o    Erhöht den Rüstungswert

o    Beeinflusst den kritischen Trefferschaden

o    Erfordert hohe Geschicklichkeit und Klugheit

* * *



* * *