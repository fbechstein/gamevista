---
title: 'Fallout: New Vegas – Fünfter Teil der Entwicklertagbücher erschienen'
author: gamevista
type: news
date: 2010-10-14T20:50:21+00:00
excerpt: '<p><strong>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Bethesda </strong>und <strong>Obsidian </strong>haben heute den fünften Teil der Entwicklertagebücher für das Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> veröffentlicht. In dieser Folge dreht sich alles um das Wahrzeichen „The Strip“ von der Stadt New Vegas. Für die Straße mit den zahlreichen Casinos wurde laut Lead Artist Jeo Sanabria ein Großteil der Entwicklungszeit verbraucht.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Darum ist der Ort eines der Highlights des Spiels. Neben vielen Spielszenen unterlegt mit Kommentaren der Entwickler stimmt der Trailer auf den Release des Titels am 22. Oktober 2010 ein. <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> erscheint für Xbox 360, PlayStation 3 und PC.

[> Zum Fallout: New Vegas – Entwicklertagebuch #5][1]

   

* * *

   



 [1]: videos/item/root/fallout-new-vegas--fuenftes-entwicklertagbuch