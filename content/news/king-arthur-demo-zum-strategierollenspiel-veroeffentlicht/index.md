---
title: King Arthur – Demo zum Strategierollenspiel veröffentlicht
author: gamevista
type: news
date: 2009-12-19T14:39:53+00:00
excerpt: '<p>[caption id="attachment_1199" align="alignright" width=""]<img class="caption alignright size-full wp-image-1199" src="http://www.gamevista.de/wp-content/uploads/2009/12/kingarthur_small.png" border="0" alt="King Arthur" title="King Arthur" align="right" width="140" height="100" />King Arthur[/caption]Der Publisher <a href="http://www.kingarthurthewargame.com" target="_blank">Ubisoft</a> hat vor kurzem eine Demo zum rundenbasierten Strategierollenspiel <strong>King Arthur</strong> veröffentlicht. Das für Ende Januar 2010 geplante Spiel führt euch in das mittelalterliche mystische Großbritannien und lässt euch die Abenteuer rund um King Arthur bestreiten.</p> '
featured_image: /wp-content/uploads/2009/12/kingarthur_small.png

---
So müsst ihr als König die einzelnen Abschnitte der Insel vereinigen, Armeen steuern und eurer Länder bewirtschaften. Die 2 Gigabyte große Demo könnt ihr ab sofort kostenlos von unserem Server laden. 

[> Zum King Arthur &#8211; Demo Download][1]

[> Zur Screenshotgalerie][2]

 

* * *



* * *

 [1]: downloads/demos/item/demos/king-arthur-demo
 [2]: screenshots/item/root/king-arhur-screenshots