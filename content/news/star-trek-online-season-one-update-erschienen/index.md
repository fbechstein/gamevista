---
title: Star Trek Online – Season One Update erschienen
author: gamevista
type: news
date: 2010-03-27T12:48:49+00:00
excerpt: '<p>[caption id="attachment_915" align="alignright" width=""]<img class="caption alignright size-full wp-image-915" src="http://www.gamevista.de/wp-content/uploads/2009/10/startrek_online.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Der Entwickler <strong>Cryptic Studios</strong> hat das erste große Inhaltsupdate für das Onlinerollenspiel <a href="http://www.startrekonline.com/node/1403" target="_blank">Star Trek Online</a> veröffentlicht. Das <strong>Season One: Common Ground-Update</strong> bietet neue PvP-Optionen und ein weiteres Schlachtfeld. Neben einem neuen Logbuch sind weitere Frisuren und Bekleidungsmöglichkeiten verfügbar. Außerdem sind neue Skills freigeschaltet worden.</p> '
featured_image: /wp-content/uploads/2009/10/startrek_online.jpg

---
Diese sind in kritische Trefferchancen, Ausweichfähigkeit und Angriffsgenauigkeit unterteilt. Der Cryptic Store bietet gegen echtes Geld neue Charakter-Slots, Brücken für eure Raumschiffe und neue Schiffsvarianten. Eine weitere Option bietet die Möglichkeit euren Namen des Charakters zu ändern. Weitere Informationen findet ihr auf der <a href="http://www.startrekonline.com/node/1403" target="_blank">Website</a> von <a href="http://www.startrekonline.com/node/1403" target="_blank">Star Trek Online</a>.

 

<cite></cite>

   

* * *

   



 