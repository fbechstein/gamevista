---
title: THQ – Publisher gibt Lineup für die gamescom bekannt
author: gamevista
type: news
date: 2010-08-17T21:06:27+00:00
excerpt: '<p>[caption id="attachment_2062" align="alignright" width=""]<img class="caption alignright size-full wp-image-2062" src="http://www.gamevista.de/wp-content/uploads/2010/07/gamescom_small.png" border="0" alt="Gamescom 2010" title="Gamescom 2010" align="right" width="140" height="100" />Gamescom 2010[/caption]Der Publisher <a href="http://www.thq.com" target="_blank">THQ</a>, bekannt über die erfolgreichen Strategietitel wie <strong>Dawn of War</strong> und<strong> Company of Heroes</strong>, gab heute sein Lineup für die Spielemesse gamescom 2010 in Köln bekannt. Toptitel wie <strong>Homefront</strong>, <strong>Warhammer 40.000: Space Marine</strong> und <strong>Company of Heroes Online</strong> werden vorgestellt.</p> '
featured_image: /wp-content/uploads/2010/07/gamescom_small.png

---
Zusätzlich bietet der Publisher auch ein imposantes Bühnenprogramm wie eine BMX-Show, Interviews und zahlreiche Musiker.

  * Homefront (PC, PS3, Xbox 360)
  * Red Faction: Armageddon (PC, PS3, Xbox 360)
  * Warhammer 40.000: Space Marine (PC, PS3, Xbox 360)
  * WWE SmackDown vs. Raw 2011 (Konsole)
  * WWE All Stars (Konsole)
  * Darksiders (PC)
  * Company of Heroes Online (PC)
  * Warhammer 40.000: Dark Millennium Online (PC, PS3, Xbox 360)
  * Quantum Theory (Xbox 360, PS3)
  * Fist of the North Star (Xbox 360, PS3)

   

* * *

   



 