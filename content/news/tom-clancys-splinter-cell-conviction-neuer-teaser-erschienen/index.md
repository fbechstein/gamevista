---
title: 'Tom Clancy`s Splinter Cell: Conviction – Neuer Teaser erschienen'
author: gamevista
type: news
date: 2009-09-04T08:20:43+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Heute startet die Spielemesse <a href="http://www.paxsite.com">Penny Arcade Expo 2009</a>, kurz Pax, in Seattle. Ubisoft hat im Rahmen dieser Messe heute einen kurzen Teaser für ihr Actionspiel <strong>Tom Clancy´s Splinter Cell: Conviction</strong> veröffentlicht. Im Video ist Sam Fisher am Klavier zu sehen der ein phänomenales Finale spielt.<strong> Tom Clancy´s Splinter Cell: Conviction</strong> erscheint im ersten Quartal 2010 für Xbox 360 und PC.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
 

[> Zum Tom Clancy´s Splinter Cell: Conviction Teaser][1]

 

* * *



* * *

 [1]: videos/item/root/splinter-cell-conviction-keyboard-sam-teaser