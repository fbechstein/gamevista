---
title: 'Need for Speed: Hot Pursuit – Erster Premium DLC Pack angekündigt'
author: gamevista
type: news
date: 2010-12-06T16:24:25+00:00
excerpt: '<p>[caption id="attachment_2381" align="alignright" width=""]<img class="caption alignright size-full wp-image-2381" src="http://www.gamevista.de/wp-content/uploads/2010/10/nfshp.jpg" border="0" alt="Need for Speed: Hot Pursuit" title="Need for Speed: Hot Pursuit" align="right" width="140" height="100" />Need for Speed: Hot Pursuit[/caption]Publisher <strong>Electronic Arts</strong> hat offiziell das erste Premium-DLC-Pack für das Rennspiel <a href="http://www.hotpursuit.needforspeed.com" target="_blank">Need for Speed: Hot Pursuit</a> angekündigt. Der Downloadable-Conten wird zwölf neue Inhalte bieten. Darunter sind neue Events, Archievements, Trophäen, Fahrzeuge wie z.B. den Bugatti Veyron SS, Porsche 911 GT2, Gumpert Apollo. Bisher gibt es keine Angaben zum Preis und Veröffentlichungsdatum.</p> '
featured_image: /wp-content/uploads/2010/10/nfshp.jpg

---
 

 

   

* * *

   

