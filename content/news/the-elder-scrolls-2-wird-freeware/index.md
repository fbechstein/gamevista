---
title: The Elder Scrolls 2 wird Freeware
author: gamevista
type: news
date: 2009-07-10T13:34:05+00:00
excerpt: '<p>[caption id="attachment_35" align="alignright" width=""]<img class="caption alignright size-full wp-image-35" src="http://www.gamevista.de/wp-content/uploads/2009/07/elder_daggerfall_small.png" border="0" alt="Elder Scrolls 2 - Daggerfall ab sofort als Freeware erhältlich" title="Elder Scrolls 2 - Daggerfall ab sofort als Freeware erhältlich" align="right" width="140" height="100" />Elder Scrolls 2 - Daggerfall ab sofort als Freeware erhältlich[/caption]Für Nostalgiker gibt es gute News aus dem Hause <a href="http://www.bethsoft.com">Bethesda</a>. Zum 15-jährigen Jubiläum der Elder Scrolls-Reihe veröffentlicht die Software-Schmiede den zweiten Teil <strong>The Elder Scrolls 2 - Daggerfall</strong> als Freeware. Das Spiel wird wie das damalige Original ausgeliefert und funktioniert nur in einer DOS-Box. </p>'
featured_image: /wp-content/uploads/2009/07/elder_daggerfall_small.png

---
Die Installationsanleitung zum Spiel findet Ihr <a href="http://static.bethsoft.com/downloads/games/daggerfall_legal_and_installation.pdf" target="_blank">hier</a>, das Spiel selbst kann <a href="http://static.bethsoft.com/downloads/games/DFInstall.zip" target="_blank">hier</a> heruntergeladen werden. 





