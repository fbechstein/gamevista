---
title: 'Tropico 3: Absolute Power – Erstes Addon angekündigt'
author: gamevista
type: news
date: 2010-03-01T17:20:08+00:00
excerpt: '<p>[caption id="attachment_1504" align="alignright" width=""]<img class="caption alignright size-full wp-image-1504" src="http://www.gamevista.de/wp-content/uploads/2010/03/smallt3.jpg" border="0" alt="Tropico 3" title="Tropico 3" align="right" width="140" height="100" />Tropico 3[/caption]Der Publisher <strong>Kalypso Media</strong> hat heute im Rahmen einer Pressemitteilung, das erste Addon zum Aufbaustrategiespiel <a href="http://www.tropico3.com" target="_blank">Tropico 3</a> angekündigt. Auf den Namen <strong>Absolute Power</strong> wird das Erweiterungspaket hören und unter anderem eine neue Kampagne mit sich bringen. So werden insgesamt zehn neue Missionen und natürlich jede Menge neuer Inseln dazu kommen.</p> '
featured_image: /wp-content/uploads/2010/03/smallt3.jpg

---
Neben sechs neuen Gebäuden wird es auch weitere Sehenswürdigkeit geben, sowie neue Gruppierungen und Katastrophen. **Absolute Power** wird voraussichtlich im Mai 2010 erscheinen.

 

* * *



* * *