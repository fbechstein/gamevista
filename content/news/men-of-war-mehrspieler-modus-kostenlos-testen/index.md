---
title: Men of War – Mehrspieler-Modus kostenlos testen
author: gamevista
type: news
date: 2010-05-11T15:52:13+00:00
excerpt: '<p>[caption id="attachment_174" align="alignright" width=""]<img class="caption alignright size-full wp-image-174" src="http://www.gamevista.de/wp-content/uploads/2009/07/men_of_war.jpg" border="0" alt="Men of War" title="Men of War" align="right" width="140" height="100" />Men of War[/caption]Der Publisher 1C Company kündigt passend zum 65. Jahrestag vom Ende des 2. Weltkriegs die Möglichkeit an, das Strategiespiel <a href="http://www.digitalmindsoft.eu/forums/viewtopic.php?f=109&t=6756#p81650">Men of War</a> im Mehrspieler-Modus kostenlos anzutesten. In der Zeit vom 11. bis 23. Mai 2010 kann somit jeder Interessierte den Multiplayer-Client kostenlos herunterladen und mit einem temporären Spiele-Key nutzen.</p> '
featured_image: /wp-content/uploads/2009/07/men_of_war.jpg

---
Dabei sind alle Mehrspieler-Modi komplett nutzbar. Weitere Details gibt es auf der <a href="http://www.digitalmindsoft.eu/forums/viewtopic.php?f=109&#038;t=6756#p81650" target="_blank">offiziellen Website</a> von <a href="http://www.digitalmindsoft.eu/forums/viewtopic.php?f=109&#038;t=6756#p81650" target="_blank">Men of War</a>.

[> Zum Men of War &#8211; Multiplayer Game Client Download][1]

<a href="http://eng.bestway.com.ua/index.php/component/contact/12-contacts/2-get-key." target="_blank">> Men of War &#8211; Zugangscode ordern</a>

 

   

* * *

   



 [1]: downloads/demos/item/demos/men-of-war-mulitplayer-demo