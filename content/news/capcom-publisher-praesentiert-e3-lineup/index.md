---
title: Capcom – Publisher präsentiert E3 Lineup
author: gamevista
type: news
date: 2010-06-07T20:58:43+00:00
excerpt: '<p>[caption id="attachment_343" align="alignright" width=""]<img class="caption alignright size-full wp-image-343" src="http://www.gamevista.de/wp-content/uploads/2009/08/capcom.jpg" border="0" alt="Capcom" title="Capcom" align="right" width="140" height="100" />Capcom[/caption]Publisher <a href="http://www.capcom.com" target="_blank">Capcom</a> hat sein offizielles Lineup für die E3 Messe in Los Angeles vorgestellt. Die Highlights dürften natürlich <strong>Marvel vs Capcom 3</strong> und <strong>Dead Rising 2</strong> sein. <a href="http://www.capcom.com" target="_blank">Capcom</a> kündigte im Rahmen der Pressemitteilung an das sie noch einige Überraschungen bereit halten. Dazu aber nächste Woche mehr. Die E3 Messe in Los Angeles findet vom 15. – 17. Juni statt.<br /><br /></p> '
featured_image: /wp-content/uploads/2009/08/capcom.jpg

---
Das Lineup sieht wie folgt aus:

  * Marvel vs Capcom 3: Fate of Two Worlds
  * Okamiden
  * Bionic Commando Rearmed 2
  * Sengoku BASARA: Samuari Heroes
  * Ghost Trick: Phantom Detective
  * Dead Rising 2
  * Dead Rising 2: Case Zero
  * MotoGP 09/10

   

* * *

   

