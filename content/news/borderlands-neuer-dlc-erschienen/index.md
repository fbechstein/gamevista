---
title: Borderlands – Neuer DLC erschienen
author: gamevista
type: news
date: 2010-01-08T19:24:29+00:00
excerpt: '<p>[caption id="attachment_1284" align="alignright" width=""]<img class="caption alignright size-full wp-image-1284" src="http://www.gamevista.de/wp-content/uploads/2010/01/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Der Entwickler <a href="http://www.gearboxsoftware.com/games/borderlands" target="_blank">Gearbox</a> hat nun endlich den zweiten herunterladbaren Zusatzinhalt auch für PC-Spieler veröffentlicht. Nachdem <strong>Mad Moxxi`s Underdome Riot</strong>, so der Name des DLC, für Xbox 360 Besitzer bereits seit Ende Dezember verfügbar ist, mussten sich PC-Spieler bis heutigen Tage gedulden.</p> '
featured_image: /wp-content/uploads/2010/01/smalllogo.jpg

---
Das Mini-Addon kostet 7,99 EUR, kaufen und herunterladen kann man es entweder auf der <a href="https://store.gearboxsoftware.com/" target="_blank">Website</a> von Gearbox oder über den Onlinedienst <a href="http://store.steampowered.com/app/40940/" target="_blank">Steam</a>. In **Mad Moxxi\`s Underdome Riot** können sich Spieler über eine neue Questreihe, einen neuen Arena-Spielmodus und jede Menge neuer Waffen freuen.

 

 

<cite></cite>

* * *



* * *

 