---
title: 'Order of War: Challenge – Demo zum Addon veröffentlicht'
author: gamevista
type: news
date: 2010-03-08T16:11:46+00:00
excerpt: '<p>[caption id="attachment_1528" align="alignright" width=""]<img class="caption alignright size-full wp-image-1528" src="http://www.gamevista.de/wp-content/uploads/2010/03/orderofwar_small.png" border="0" alt="Order of War" title="Order of War" align="right" width="140" height="100" />Order of War[/caption]Der Publisher<strong> Square Enix</strong> veröffentlichte vor kurzem die Demoversion des Addons <strong>Challenge </strong>zum Strategietitel <a href="http://www.orderofwar.com" target="_blank">Order of War</a>. <a href="http://www.orderofwar.com" target="_blank">Order of War: Challenge</a> ist eine eigenständig Version die vor allem den Mehrspielerbereich zu gute kommt. Sie läuft ohne das Hauptprogramm <a href="http://www.orderofwar.com" target="_blank">Order of War</a>. In der Demoversion dazu können sie wahlweise auf Seiten der Amerikaner, Russen oder Deutschen im zweiten Weltkrieg spielen.</p> '
featured_image: /wp-content/uploads/2010/03/orderofwar_small.png

---
Wer bereits Besitzer des Hauptspiels ist kann sich das Addon kostenlos downloaden. Order of War: Challenge wird am 12. März 2010 veröffentlicht.

 

[> Zur Order of War: Challenge &#8211; Demo][1]<cite></cite>

   

* * *

   



 

 [1]: downloads/demos/item/demos/order-of-war-challenge-multiplayer-demo