---
title: 'Ghostbusters: Sanctum of Slime – 8 neue Screenshots veröffentlicht'
author: gamevista
type: news
date: 2011-01-12T18:56:48+00:00
excerpt: '<p><strong>[caption id="attachment_2471" align="alignright" width=""]<img class="caption alignright size-full wp-image-2471" src="http://www.gamevista.de/wp-content/uploads/2010/12/ghostbusters_sos.jpg" border="0" alt="Ghostbusters: Sanctum of Slime" title="Ghostbusters: Sanctum of Slime" align="right" width="140" height="100" />Ghostbusters: Sanctum of Slime[/caption]Atari </strong>hat neue Screenshots für seinen Actionspiel <a href="http://www.atari.com/gbsanctumofslime/" target="_blank">Ghostbusters: Sanctum of Slime</a> veröffentlicht. Das vier Spieler Koop-Adventure versetzt den Spieler in den erst kürzlich angeworbenen Ghostbuster-Anfänger, um New York City vor einer neuen übernatürlichen Katastrophe zu retten.</p> '
featured_image: /wp-content/uploads/2010/12/ghostbusters_sos.jpg

---
<a href="http://www.atari.com/gbsanctumofslime/" target="_blank">Ghostbusters: Sanctum of Slime</a> erscheint im Frühling für PC, PlayStation 3 und Xbox 360.

 

[> Zu den Ghostbusters: Sanctum of Slime Screenshots][1]

   

* * *

   



 [1]: screenshots/item/root/ghostbusters-sanctum-of-slime-screenshots