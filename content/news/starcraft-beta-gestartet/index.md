---
title: Starcraft 2 – Beta gestartet
author: gamevista
type: news
date: 2010-02-18T15:22:42+00:00
excerpt: |
  |
    <p>[caption id="attachment_1465" align="alignright" width=""]<img class="caption alignright size-full wp-image-1465" src="http://www.gamevista.de/wp-content/uploads/2010/02/starcraft2_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="88" />Starcraft 2[/caption]Endlich! Dies werden viele Fans des Strategiespiels <a href="hot-spots/starcraft-2-wings-of-liberty-review" target="_blank">Starcraft 2</a> sagen. Mit den Worten „It's about time!“, leitet der Publisher <strong>Blizzard Activision</strong> die Beta-Phase, des sich seit mehreren Jahren in der Entwicklung befindenden Spiels ein. <strong>Blizzard</strong> verschickt ab dem heutigen Tage die ersten Emails mit den begehrten Beta-Keys.</p>
featured_image: /wp-content/uploads/2010/02/starcraft2_small.jpg

---
Den Zugangscode müsst ihr dann mit einem zuvor erstellten Battle.net-Account verknüpfen.  Kurz darauf wird der Zugang zum 1,6 Gigabyte großen Beta-Client freigeschaltet. **Blizzard** kündigte weiterhin an, dass in den nächsten Tagen weitere Emails mit neuen Beta-Keys an die Fans verschickt werden. Wie lange die Beta laufen wird ist bisher nicht bekannt.

 

<p style="text-align: center;">
  <img class=" size-full wp-image-1466" src="http://www.gamevista.de/wp-content/uploads/2010/02/sc2.jpg" border="0" width="600" height="450" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/sc2.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/sc2-300x225.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *



* * *