---
title: World of Warcraft – Cataclysm Hotfix erschienen
author: gamevista
type: news
date: 2010-12-07T18:39:46+00:00
excerpt: '<p>[caption id="attachment_2476" align="alignright" width=""]<img class="caption alignright size-full wp-image-2476" src="http://www.gamevista.de/wp-content/uploads/2010/12/wow_small2.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="140" height="100" />World of Warcraft[/caption]Im Rahmen der heutigen Veröffentlichung des neusten <a href="http://www.worldofwarcraft.com/cataclysm/">World of Warcraft</a> Addons <strong>Cataclysm</strong>, wurde von den Entwicklern von <strong>Blizzard</strong>, ein Patch der die größten Probleme beheben soll bereit gestellt. Der Hotfix befasst sich hauptsächlich mit Fehlern in Quests in den neuen Gebieten.</p> '
featured_image: /wp-content/uploads/2010/12/wow_small2.png

---
Die komplette Liste der Änderungen im Überblick:

 

&#8211; It is no longer possible for an ill-mannered person of the opposing faction to one-shot a flight trainer in Stormwind or Orgrimmar, which in turn made the 500 players standing on top of the flight trainer with their mammoths really sad.

&#8211; Fallen Hero&#8217;s Spirits in Icecrown can no longer have their pockets picked.

&#8211; Ray&#8217;ma and Brew Vendor Brew of the Month Club vendors are now spawning properly.

&#8211; The achievement “Stood in the fire” is granted when standing in a zone boundary different than Deathwing&#8217;s spawn location. In addition, players can now get this achievement by walking into the wake of Deathwing&#8217;s fire after he has passed.

&#8211; If faction mounts do not translate to a mount of the opposite faction when performing a paid faction change, they will now be removed.

&#8211; Players are no longer able to endlessly kite the outdoor Battlemasters.

&#8211; Several combat issues involving mind control mechanics have been fixed.

&#8211; NPCs and vehicles now have the correct Cataclysm values for health and damage in the 80-84 and 85 Strand of the Ancients, Isle of Conquest, and Alterac Valley level brackets.

&#8211; Some players were unable to enter 10- vs. 10-player Battlegrounds when the option to enter became available. This has been corrected.

&#8211; Priests: Low-level priests will now properly take Shadow Word: Death backlash damage when casting Shadow Word: Death.

&#8211; Shamans: Lightning Shield again properly deals damage while the shaman is casting other spells.

&#8211; The Mana Burn ability that is cast when Arcane Aberrations die in Dire Maul has been replaced with an Arcane Explosion ability that does not drain mana.

&#8211; Al&#8217;akir&#8217;s Lightning Cloud now despawns when the encounter is defeated in Throne of the Four Winds.

&#8211; Players are no longer able to use Killing Spree on Ozumat in Throne of the Tides. The message &#8222;Invalid Target&#8220; is displayed.

&#8211; Players are no longer placed oddly when affected by Ulthok&#8217;s Squeeze in Throne of the Tides.

&#8211; Temple Guardian Anhuur in Halls of Origination will now lose Shield of Light at the appropriate times. He was cheating.

&#8211; Millhouse Manastorm in The Stonecore will not stay linked to trash packs he is no longer stationed at after a group wipe.

&#8211; Razorgore and Chromaggus in Blackwing Lair will no longer cause an infinite loop of encounter mind control mechanics when pets or companions are present.

&#8211; Tattooed Eyeball should not be epic quality and is now superior.

&#8211; Wailing Weeds no longer give experience, have any drops, or are able to be gathered in Mount Hyjal.

&#8211; Items which have refund options available no longer have their expiration cooldown recorded incorrectly when more than one is purchased within a short amount of time.

&#8211; An issue which prevented players from rolling Need on certain relics found in dungeons has been corrected.

&#8211; Helm of the Thousand Needles now has 12 Strength and 17 Stamina in addition to the 12 Hit Rating it had before.

&#8211; Spider&#8217;s Silk now drops from most all spiders in the level range of 16-25.

&#8211; Stag Flanks now drop from level-appropriate stags.

&#8211; Big Mouth Clams and the Black Pearls they sometimes contain again drop off of many creatures.

&#8211; Corpse Dust and Infernal Stones are no longer available from vendors.

&#8211; The stats for the one-handed mace Shatterscale Mightfish have been corrected.

&#8211; Rogues are now able to equip and use Morningstar of the Order. However, the tooltip for the item still shows Main Hand in red, and right clicking on the item when it is equipped returns the message &#8222;You do not have the required proficiency for that item.&#8220; This display issue will be corrected in a future patch.

&#8211; Passengers are now force ejected from multi-passenger mounts that have been converted into a Reindeer or Brewfest Kodo. This works for both NPCs and PCs.

&#8211; There is no longer a School of Fish in Elwynn Forest that like to think of themselves as in &#8222;testing,&#8220; nor do they yield Peacebloom when fished. This School of Fish is no longer a part of the counterculture and will no longer give you flowers when fished.

&#8211; Recipe: Feathered Lure is no longer on the vendor list for the Cooking reward vendors, as it&#8217;s returning an error &#8222;The Spell is not available to you&#8220; when players attempt to learn it.

&#8211; Electrostatic Condenser no longer provides inconsistent loot chances for each gathering profession.

&#8211; Training in Archaeology should now properly generate dig sites around the world according to the player&#8217;s skill level.

&#8211; Archaeology daily quests are now properly flagged as dailies.

&#8211; A Fossil Archaeology Find at the Un&#8217;Goro Terror Run dig site spawned beyond the range of the dig site area and has been removed.

&#8211; There was an inconsistency in Cooking skill-ups between Lavascale Minestrone and Lavascale Fillet which has been addressed.

&#8211; Some Leatherworking recipes which require a skill of 525 were missing from the vendor Misty Merriweather and have been returned.

&#8211; Mobs that spawn infinitely during the Temple of Earth finale within Deepholm do not grant experience.

&#8211; Players are no longer ported out of the Maelstrom if they have &#8222;The Eye of the Storm&#8220; in their quest log when they go through the portal to the Maelstrom.

&#8211; Twilight Proveditors for the quest &#8222;End of the Supply Line&#8220; in Mount Hyjal should respawn at a more consistent rate.

&#8211; Zin&#8217;jatar Raiders respawn more quickly for the &#8222;Buy Us Some Time&#8220; quest in Kelp&#8217;thar Forest, Vashj&#8217;ir.

&#8211; Completing the quest &#8222;Never Surrender, Sometimes Retreat&#8220; is now updating Gilneas properly.

&#8211; Forsaken Sailors were dealing too much damage to those questing in Gilneas. Their damage output has been reduced.

&#8211; If a player completes the objectives of &#8222;Bad Medicine&#8220; outside of Northern Stranglethorn, then the quest &#8222;Just Hatched&#8220; will not auto-launch, but Corporal Sethman will have the quest available when the player turns in the quest &#8222;Bad Medicine&#8220; to Brother Nimetz. If the player completes &#8222;Bad Medicine&#8220; within Northern Stranglethorn, then &#8222;Just Hatched&#8220; is auto-launched.

&#8211; Quest givers in The Exodar, Teldrassil and Dun Morogh are all now offering the level 50 mage quest &#8222;Meet with Maginor Dumas&#8220; leading players to Stormwind.

&#8211; The deprecated quests &#8222;Foreboding Plans&#8220; and &#8222;Encrypted Letter&#8220; are no longer being offered to players by the Syndicate Documents in Hillsbrad Foothills.

&#8211; The deprecated quests &#8222;Looking Further&#8220; and &#8222;Morganth&#8220; are no longer being offered by the Old Lion Statue in Redridge Mountains.

&#8211; The cutscene for &#8222;Lordaeron&#8220; in Silverpine Forest will now properly play if the player is mounted when starting the quest.

&#8211; High Priest Venoxis should spawn properly during the &#8222;High Priest Venoxis&#8220; quest event in The Cape of Stranglethorn.

&#8211; The quest &#8222;The Holy Water of Clarity&#8220; will be offered regardless of the order in which the player completes the prerequisite quests in The Cape of Stranglethorn.

&#8211; The NPCs of Bloodgulch no longer disappear when players skip the cutscene during the quest &#8222;Eye Spy&#8220; in Twilight Highlands.

&#8211; 60 completed quests in Winterspring are now required in order to earn the Winterspring Quests achievement.

&#8211; Players should no longer experience issues completing the quest &#8222;The Reason Why&#8220; when attempting to interact with the various Moonstones in Ashenvale.

&#8211; Players are no longer able to attack other players while in Diemetradon form during the quest &#8222;Ever Watching From Above&#8220; in Un&#8217;Goro Crater.

&#8211; It is no longer possible to pick up the deprecated quest &#8222;The Gordunni Scroll&#8220; in Feralas.

&#8211; The deprecated quest &#8222;Hunting in Stranglethorn&#8220; is no longer being offered by Roon Wildmane in Desolace.

&#8211; Questgiver Melizza Brimbuzzle in Desolace now respawns at a much more reasonable rate after dying.

&#8211; Jinky Twizzlefixxit now provides players with a replacement River Boat quest reward via her gossip text if a player deletes it while in Thousand Needles.

&#8211; Antechamber Guardian now spawns correctly for &#8222;What Lies Within&#8220; in Tanaris.

&#8211; The quest &#8222;Sandscraper&#8217;s Treasure&#8220; is no longer being offered to Alliance players via the Sandscraper&#8217;s Chest in Tanaris.

&#8211; The Antediluvean Chest is now correctly spawning inside the Antechamber of Uldum within Tanaris and there is no longer an issue with it spawning outside of the chamber.

   

* * *

   

