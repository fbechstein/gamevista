---
title: 'Battlefield: Bad Company 2 – Termin zum Vietnam DLC'
author: gamevista
type: news
date: 2010-11-30T17:15:14+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Pressemitteilung den Termin für das erste Addon von <a href="http://www.battlefieldbadcompany2.com" target="_blank">Battlefield: Bad Company 2</a> benannt. Demnach wird <strong>Vietnam </strong>am 18. Dezember 2010 exklusiv per Steam bzw. den EA-Store veröffentlicht.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Am 21. Dezember 2010 wird das Erweiterungspaket dann auch über andere Download-Plattformen erhältlich sein.

 

 

   

* * *

   

