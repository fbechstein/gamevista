---
title: Champions Online – An Halloween kostenlos
author: gamevista
type: news
date: 2009-10-26T08:54:46+00:00
excerpt: '<p>[caption id="attachment_122" align="alignright" width=""]<img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" />Champions Online[/caption]Wer mal Lust hat <a href="http://www.champions-online.com" target="_blank">Cryptic`s</a> Online Rollenspiel <strong>Champions Online </strong>zu testen und dafür nichts zu bezahlen, der sollte Halloween am kommenden Wochenende ausfallen lassen und stattdessen die Chance nutzen um <strong>Champions Online</strong> kostenlos anzuspielen.</p> '
featured_image: /wp-content/uploads/2009/07/champions_online_small.jpg

---
Die PR-Aktion läuft vom 30. Oktober ab 18 Uhr bis zum 2. November 19 Uhr. Wer pünktlich loslegen möchte der sollte sich schon mal einen Account anlegen und den **Champions Online** <a href="http://files.champions-online.com/installers/Champions_Online.exe" target="_blank">Client herunterladen</a>. Weitere Informationen über Client und Account könnt ihr auf der <a href="http://www.champions-online.com/node/594625" target="_blank">offiziellen Webseite</a> des Entwicklers finden.

 

* * *



* * *

 