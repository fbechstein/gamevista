---
title: Pro Evolution Soccer 2010 – Zusatzinhalte angekündigt
author: gamevista
type: news
date: 2009-10-22T17:14:00+00:00
excerpt: '<p>[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]Seit heute dürften die Herzen aller Fußballfans ein wenig höher Schlagen. Denn vor kurzem kann man das neue Fußballspiel <strong>Pro Evolution Soccer 2010</strong> käuflich erwerben. Aus diesem Grund hat sich auch der Entwickler <a href="http://www.de.games.konami-europe.com" target="_blank">Konami</a> auf ihrem <a href="http://uk.games.konami-europe.com/blog.do?pageSize=10&blogEntryList.page=0#blog-entry-127" target="_blank">offiziellen Blog</a> zu Wort gemeldet.</p> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
Darauf bedankt sich das Team rund um <a href="http://www.de.games.konami-europe.com" target="_blank">Konami</a>, wünscht allen Fans viel Spaß und hofft das die Neuerungen von **PES 2010** großen Zuspruch finden. Auch kündigte der Entwickler <a href="http://www.de.games.konami-europe.com" target="_blank">Konami</a> bereits das erste Update und den ersten downloadbaren Zusatzinhalt an. Beides wird demnächst erscheinen und völlig kostenlos zur Verfügung gestellt werden.

 

* * *



* * *

 