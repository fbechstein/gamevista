---
title: Risen – Wegen Sex und Drogen in Australien verboten
author: gamevista
type: news
date: 2009-08-11T18:30:38+00:00
excerpt: '<p>[caption id="attachment_332" align="alignright" width=""]<a href="http://www.piranha-bytes.com" target="_blank"><img class="caption alignright size-full wp-image-332" src="http://www.gamevista.de/wp-content/uploads/2009/08/risencombos.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Piranha Bytes</a>Risen[/caption] Rollenspiel <strong>Risen </strong>ist noch nichtmal Veröffentlicht worden und schon gibt es die ersten Aufhänger. Wegen der vorhandenen Anspielungen auf sexuelle Interaktionen mit im Spiel befindlichen Prostituierten und einer im Spiel erhältlichen Droge Namens „Brugleweed“ macht die australische OLFC (quasi die deutsche USK) den Aufstand. </p>'
featured_image: /wp-content/uploads/2009/08/risencombos.png

---
Denn in der vorgelegten Version von <a href="http://www.piranha-bytes.com" target="_blank">Piranha Bytes</a> wird **Risen** so nicht in Australien verkauft werden dürfen. Australien wurde schon in der Vergangenheit auf einschlägigen Medienseiten zum Synonym für übertriebene Zensur. Letztes Beispiel **Fallout 3**, hier fand die OLCF das das nutzen der Droge Med-X im Spiel für jugendliche Spieler und Erwachsene nicht das richtige sei. Danach änderte Bethesda ihr großartiges Meisterwerk dahingehend ab. Dies wird nun **Risen** auch passieren. Denn welcher Entwickler kann schon auf so viele Käufer verzichten.







 