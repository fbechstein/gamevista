---
title: World of Warcraft Film – Sam Raimi übernimmt Regie
author: gamevista
type: news
date: 2009-07-22T15:21:34+00:00
excerpt: '<p><img class=" alignright size-full wp-image-103" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="WoW" title="World of Warcraft" align="right" width="139" height="100" /><em><font style="font-style: normal" color="#000000">Super Nachrichten für alle <strong>World of Wacraft</strong> Fans. Wie gerade bekannt gegeben wurde, übernimmt kein anderer als <strong>Sam Raimi</strong> (Spider-Man Filme) die Regie zum Konflikt zwischen Horde und Allianz. </font></em></p>'
featured_image: /wp-content/uploads/2009/07/wow_small.png

---
_<font color="#000000"><span style="font-style: normal">Neben Raimi wird auch Charles Roven, Produzent vom Hit 2008 The Dark Knight, mit dabei sein und das Set tatkräftig unterstützen. Die Dreharbeiten werden nach dem Fertigstellen von Spider-Man 4 anlaufen. Über die Story ist noch nicht viel bekannt. Nur soviel sei gesagt, es wird um den Konflikt der Allianz und der Horde in Azeroth gehen. Uwe Boll hatte auch sein Interesse bekundet, bekam aber eine Absage seitens Blizzard die härter nicht ausfallen könnte: <br />„</span></font>_**<font color="#000000"><span style="font-style: normal">Wir werden die Filmrechte nicht verkaufen, nicht an Sie &#8230; ganz besonders nicht an Sie.&#8220;</p> 

<p>
  </span></font></strong>
</p>



<p>
  
</p>

