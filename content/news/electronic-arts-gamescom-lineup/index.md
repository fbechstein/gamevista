---
title: Electronic Arts – Gamescom LineUp
author: gamevista
type: news
date: 2009-08-06T20:42:18+00:00
excerpt: '<p>[caption id="attachment_301" align="alignright" width=""]<img class="caption alignright size-full wp-image-301" src="http://www.gamevista.de/wp-content/uploads/2009/08/nhl10_small.jpg" border="0" alt="NHL 10 wird neben Risen und Bad Company 2 eines der EA-Highlights sein" title="NHL 10 wird neben Risen und Bad Company 2 eines der EA-Highlights sein" align="right" width="140" height="100" />NHL 10 wird neben Risen und Bad Company 2 eines der EA-Highlights sein[/caption]Das <a href="http://www.electronic-arts.de/" target="_blank">Electronic Arts</a> <strong>gamescom</strong> LineUp ist nun bekannt geworden. Es erwarten Euch folgende Titel auf der Messe in Köln:</p>'
featured_image: /wp-content/uploads/2009/08/nhl10_small.jpg

---
  * APB: All Points Bulletin
  * Army of Two: The 40th Day
  * Battlefield Bad Company 2
  * Brutal Legend
  * Command & Conquer 4
  * Dante’s Inferno
  * Dead Space Extraction
  * Dragon Age: Origins
  * FIFA 10
  * Mass Effect 2
  * MySims Agents
  * Need for Speed: Nitro
  * Need for Speed: Shift
  * NHL 10
  * Rage
  * The Beatles: Rock Band
  * The Saboteur
  * Tiger Woods Online 





