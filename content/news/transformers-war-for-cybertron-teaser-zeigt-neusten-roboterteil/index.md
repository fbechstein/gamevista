---
title: 'Transformers: War for Cybertron – Teaser zeigt neusten Roboterteil'
author: gamevista
type: news
date: 2009-12-14T20:13:11+00:00
excerpt: '<p>[caption id="attachment_1181" align="alignright" width=""]<img class="caption alignright size-full wp-image-1181" src="http://www.gamevista.de/wp-content/uploads/2009/12/transformers_warforcybertron.jpg" border="0" alt="Transformers: War for Cybertron" title="Transformers: War for Cybertron" align="right" width="140" height="100" />Transformers: War for Cybertron[/caption]Der Publisher<strong> Activision Blizzard </strong>hat nun das Actionspiel <strong>Transformers: War for Cybertron</strong> offiziell Angekündigt. Passend dazu wurde auch gleich noch ein Teaser-Trailer hinterher geschoben. <strong>Transformers: War for Cybertron</strong> soll wie vermutet keine Umsetzung des letzten Kinofiilms sein. Damit bekommt das Spiel seine ganz eigene Storyline verpasst.</p> '
featured_image: /wp-content/uploads/2009/12/transformers_warforcybertron.jpg

---
Wie der Titel des Spiels angekündigt, wird es auf den Heimatplaneten Cybertron gehen. Wie gewohnt bekriegen sich die bösen Decepticons und die guten Autobots in umfangreichen Schlachten. Die Storyline soll noch vor den Kinofilmen spielen und erklären wie der Krieg auf dem Planeten Erde ausgebrochen ist. **Transformers: War for Cybertron** erscheint voraussichtlich im zweiten Quartal 2010.

[> Zum Transformers: War for Cybertron &#8211; Teaser][1]

 

* * *



* * *

 

 [1]: videos/item/root/transformers-war-for-cybertron-teaser