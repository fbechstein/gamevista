---
title: Guild Wars 2 – Entwickler stellen Programm für Veteranen-Belohnungen vor
author: gamevista
type: news
date: 2010-10-11T16:15:12+00:00
excerpt: '<p>[caption id="attachment_433" align="alignright" width=""]<img class="caption alignright size-full wp-image-433" src="http://www.gamevista.de/wp-content/uploads/2009/08/guildwars2.jpg" border="0" alt="Guild Wars 2" title="Guild Wars 2" align="right" width="140" height="100" />Guild Wars 2[/caption]Fans des ersten Teils von <strong>Guild Wars</strong> dürfen sich laut Entwickler <strong>ArenaNet</strong> auf diverse Extras im zweiten Teil des Online-Rollenspiels freuen. Abhängig davon wieviel Ruhm und Archievements ihr in <strong>Guild Wars</strong> freigeschalten habt, erhaltet ihr im zweiten Teil verschiedene Boni.</p> '
featured_image: /wp-content/uploads/2009/08/guildwars2.jpg

---
Dafür haben **ArenaNet** und Publisher **NCSoft** ein Programm auf ihrer Website zu <a href="http://hom.guildwars2.com/#page=welcome" target="_blank">Guild Wars 2</a> Online gestellt, das euch diese Extras ausrechnet. Als Spieler benötigt man lediglich einen gültigen Guild Wars-Account um so z.B. verschiedene Titel, Rüstungsteile, Waffen oder Pets freizuschalten.

[> Zum Guild Wars 2 &#8211; Archievements-Rechner][1]

   

* * *

   



 

 [1]: http://hom.guildwars2.com/#page=welcome