---
title: 'Arcania: Gothic 4 – Monsterkunde'
author: gamevista
type: news
date: 2010-05-21T19:44:59+00:00
excerpt: '<p>[caption id="attachment_1648" align="alignright" width=""]<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Pünktlich zum langen Wochenende zeigt uns der Publisher <strong>JoWood </strong>welchen Monstern ihr im Rollenspiel <a href="http://www.arcania-game.com">Arcania: Gothic 4</a> über den Weg laufen werdet. Mit spannenden Spielszenen werden so Eisgolems, riesige Insekten oder auch Lindwürmer gezeigt.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
Das Abenteuer beginnt im Herbst 2010. Dann wird <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> für PlayStation 3, Xbox 360 und PC im Handel erscheinen.

Die Features des Spiel sind:

  * Große Außenareale 
  * Verschiedene Klimazonen 
  * Fantastische Darstellung von Flora und Fauna 
  * Dynamischer Tag/Nacht-Wechsel mit fantastischer Beleuchtung 
  * Echtzeit-Schattenberechnungen für die gesamte Spielwelt 
  * Konsistente, bekannte und glaubwürdige Charaktere 
  * Hoher Stellenwert arkaner Fähigkeiten (Zauber etc.)

[> Zum Arcania: Gothic 4 &#8211; Monster Trailer][1]

   

* * *

   



 [1]: videos/item/root/arcania-gothic-4-monster-trailer