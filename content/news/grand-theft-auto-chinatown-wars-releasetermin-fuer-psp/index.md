---
title: 'Grand Theft Auto: Chinatown Wars – Releasetermin für PSP'
author: gamevista
type: news
date: 2009-08-26T09:23:59+00:00
excerpt: '<p>[caption id="attachment_503" align="alignright" width=""]<img class="caption alignright size-full wp-image-503" src="http://www.gamevista.de/wp-content/uploads/2009/08/gta_china_1280x1024_small.jpg" border="0" alt="GTA: Chinatown Wars" title="GTA: Chinatown Wars" align="right" width="140" height="100" />GTA: Chinatown Wars[/caption]Im Frühjahr erschien <strong>Grand Theft Auto: Chinatown Wars</strong> für den Nintendo DS. Die Story drehte sich um ein uraltes Schwert was wir zusammen mit Huang Lee zurück in den Familienbesitz zurückbringen sollten. Im Rahmen der E3 2009 berichtet der Entwickler <a href="http://www.rockstargames.com">Rockstar Games</a> das auch eine PlayStation-Portable Version geplant sei.</p> '
featured_image: /wp-content/uploads/2009/08/gta_china_1280x1024_small.jpg

---
**Grand Theft Auto: Chinatown Wars** für die PSP erscheint nun am 20. Oktober 2009, dies ging aus einer Twitter Meldung der Macher des Action Spiels hervor. Ihr habt die Wahl entweder den Titel über UMD oder auch als Download im PSP-Store zu erwerben.

* * *



* * *

 