---
title: Mafia 2 – Mit PhysiX und 3D-Vision Support
author: gamevista
type: news
date: 2010-05-19T19:06:54+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]Das Gangster-Actionspiel <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> wird auch in 3D zu bestaunen sein. Dies bestätigte der Publisher <strong>2K Games</strong> im Rahmen einer Pressemitteilung. Möglich macht dies die Nvidia 3D-Technologie in Form einer 3D Brille, einem geeigneten Monitor und einer Grafikkarte die diese Technologie unterstützt.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Außerdem wird das Spiel auch mit PhysX-Support versehen, was auf starke Physikeffekte hoffen lässt. <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> wird am 27. August 2010 für PC, PlayStation 3 und Xbox 360 erscheinen.

 

 

   

* * *

   

