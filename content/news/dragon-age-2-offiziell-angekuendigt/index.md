---
title: Dragon Age 2 – Offiziell angekündigt
author: gamevista
type: news
date: 2010-07-08T15:41:58+00:00
excerpt: '<p><strong>[caption id="attachment_1996" align="alignright" width=""]<img class="caption alignright size-full wp-image-1996" src="http://www.gamevista.de/wp-content/uploads/2010/07/dragonage21.jpg" border="0" alt="Dragon Age 2" title="Dragon Age 2" align="right" width="140" height="100" />Dragon Age 2[/caption]BioWare </strong>hat die Entwicklung des zweiten Teil des Rollenspiels <strong>Dragon Age</strong> offiziell bestätigt. Der zweite Teil wird für PC, PlayStation 3 und Xbox 360 im nächsten Jahr erhältlich sein. <strong>BioWare </strong>wird am 17. August 2010 den ersten Trailer veröffentlichen. Das Spiel wird einen neuen Helden hervorbringen der seine Spielwelt noch stärker als im ersten Teil beeinflussen wird.</p> <p> </p> '
featured_image: /wp-content/uploads/2010/07/dragonage21.jpg

---
Dabei wird <a href="http://dragonage.bioware.com/da2/" target="_blank">Dragon Age 2</a> mit neuen visuellen Effekten und neuer überarbeiteter Grafikengine daher kommen.

 

 

   

* * *

   



 