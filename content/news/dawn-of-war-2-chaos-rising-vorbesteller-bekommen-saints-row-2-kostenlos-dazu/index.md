---
title: 'Dawn of War 2: Chaos Rising – Vorbesteller bekommen Saints Row 2 kostenlos dazu'
author: gamevista
type: news
date: 2010-02-15T19:07:01+00:00
excerpt: '<p>[caption id="attachment_1451" align="alignright" width=""]<img class="caption alignright size-full wp-image-1451" src="http://www.gamevista.de/wp-content/uploads/2010/02/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]Wie den News der Online-Plattform Steam zu vernehmen ist, können Vorbesteller des Addons <a href="http://store.steampowered.com/app/20570/" target="_blank">Dawn of War 2: Chaos Rising</a> den GTA-Klon <strong>Saints Row 2</strong>, kostenlos als Dreingabe abstauben. Falls ihr <strong>Saints Row 2</strong> schon besitzt, könnt ihr die Bonusversion zur Vorbestellung von<a href="http://store.steampowered.com/app/20570/" target="_blank"> Dawn of War: Chaos Rising</a> auch an eure Freunde verschenken.</p> '
featured_image: /wp-content/uploads/2010/02/1_1280x800_small.jpg

---
<a href="http://store.steampowered.com/app/20570/" target="_blank">Dawn of War 2: Chaos Rising</a> führt die Chaos Space Marines in das Spiel ein. Außerdem wird eine komplett neue Solo- und Koop-Kampagne, eine Erweiterung der Level-Begrenzung von 20 auf 30, bessere Ausrüstungsteile und neue Mehrspieleroptionen angeboten. Das Addon wird am 12. März 2010 im Handel erscheinen.

* * *



* * *