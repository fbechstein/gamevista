---
title: Bioshock 2 – Vorbestellen und den ersten Teil umsonst abstauben
author: gamevista
type: news
date: 2010-01-20T17:45:19+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2 " title="Bioshock 2 " align="right" width="140" height="100" />Bioshock 2 [/caption]Ab heute kann man den Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> beim Onlinedienst <a href="http://store.steampowered.com/app/8859/" target="_blank">Steam</a> vorbestellen und bekommt zusätzlich den ersten Teil völlig kostenlos dazu. Der Anbieter Steam senkte dazu noch den Preis um 10%, dieser liegt für <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> nun bei 44,99 EUR. Das Spiel erscheint am 9. Februar 2010 im Handel.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Weiterhin gibt es auf <a href="http://store.steampowered.com/app/8859/" target="_blank">Steam</a> ein Angebot, dass vier Kopien von <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> zum Preis von 134,97 EUR anbietet. Auch hier werden vier Kopien vom ersten Teil dabei liegen. Diese könnt ihr dann an eure Freunde verschicken. 

Damit ihr wisst was für einen PC für Bioshock 2 benötigt wird um es flüssig spielen zu können, wurden nun die Systemanforderungen veröffentlicht:

**Minimum**   
&#8211; OS: Windows XP, Vista, Windows 7   
&#8211; CPU: AMD Athlon 64 3800+ 2.4Ghz oder besser, Intel Pentium 4 530 3.0Ghz oder besser   
&#8211; RAM: 2GB   
&#8211; Grafik: NVIDIA 7800GT mit 256MB oder besser, ATI Radeon X1900 mit 256MB oder besser   
&#8211; DirectX: DirectX 9.0c   
-HDD: 11GB   
&#8211; Sound: 100% DirectX 9.0c kompatible Soundkarte (auch onboard)

**Empfohlen**   
&#8211; CPU: AMD Athlon 64 X2 5200+ Dual Core 2.60Ghz, Intel Core 2 Duo E6420 Dual Core 2.13Ghz   
&#8211; RAM: 3GB   
&#8211; Grafik: NVIDIA 8800GT 512MB oder besser, ATI Radeon HD4830 512MB oder besser

 

 

* * *



* * *