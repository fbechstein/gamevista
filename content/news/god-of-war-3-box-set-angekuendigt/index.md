---
title: God of War 3 – Box-Set angekündigt
author: gamevista
type: news
date: 2010-05-06T19:32:37+00:00
excerpt: '<p>[caption id="attachment_919" align="alignright" width=""]<img class="caption alignright size-full wp-image-919" src="http://www.gamevista.de/wp-content/uploads/2009/10/gow.jpg" border="0" alt="God of War 3" title="God of War 3" align="right" width="140" height="100" />God of War 3[/caption]Am 19. März diesen Jahres erschien der dritte Teil von <a href="http://www.godofwar.com" target="_blank">God of War</a>. Der PlayStation 3 exklusive Titel, wird nun um eine weitere Edition erweitert. Demnach veröffentlicht der Publisher <strong>Sony </strong>für die erfolgreiche Reihe für PS3, auch die beiden ersten Teile in Deutschland. Am 07. Mai 2010 wird für alle Kratos-Jünger die <a href="http://www.godofwar.com" target="_blank">God of War 3</a> bereits besitzen, die God of War Collection erscheinen.</p> '
featured_image: /wp-content/uploads/2009/10/gow.jpg

---
Diese enthält den ersten, sowie den zweiten Teil von God of War. Beide erstrahlen nun im neuen High-Definition-Glanz. Teil 1 und 2 erscheinen überarbeitet auf Blu-ray Disc zum Preis von 40,95 Euro.   
Weiterhin teilt **Sony** mit das es auch eine <a href="http://www.godofwar.com" target="_blank">God of War</a> Trilogie-Box geben wird. Die enthält wie nicht anders zu erwarten, alle drei Teile des Actionspiels. Veröffentlichungstermin des Boxsets ist der 21. Mai 2010. Die Kosten werden sich hier auf 91,95 Euro belaufen.

 

   

* * *

   

