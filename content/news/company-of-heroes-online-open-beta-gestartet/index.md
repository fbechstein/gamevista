---
title: Company of Heroes Online – Open Beta gestartet
author: gamevista
type: news
date: 2010-09-05T06:00:23+00:00
excerpt: '<p>[caption id="attachment_2185" align="alignright" width=""]<img class="caption alignright size-full wp-image-2185" src="http://www.gamevista.de/wp-content/uploads/2010/08/coh.jpg" border="0" alt="Company of Heroes Online" title="Company of Heroes Online" align="right" width="140" height="100" />Company of Heroes Online[/caption]Die geschlossene Beta-Phase für das Online-Strategiespiel <a href="http://www.companyofheroes.com" target="_blank">Company of Heroes Online</a> ist beendet. Nun wurde die Open-Beta-Phase offiziell gestartet und alle Interessierten steht die Anmeldung auf der Website <a href="http://www.companyofheroes.com/">www.companyofheroes.com</a> offen.</p> '
featured_image: /wp-content/uploads/2010/08/coh.jpg

---
Ähnlich wie in **Company of Heroes** bietet <a href="http://www.companyofheroes.com" target="_blank">Company of Heroes Online</a> neuen Features wie Heldeneinheiten und Ausrüstungsgegenstände um eure Truppen zu verbessern. Wie gewohnt kann man mit bis zu acht Spielern im 4on4 gegeneinander spielen oder im 1on1 auf insgesamt 17 Mehrspielerkarten auf der Seite der Wehrmacht oder den USA sein Glück versuchen.   

* * *

   



 