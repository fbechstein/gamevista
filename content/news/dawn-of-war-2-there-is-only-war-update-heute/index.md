---
title: Dawn of War 2 – There is only War Update Heute
author: gamevista
type: news
date: 2009-07-30T16:06:55+00:00
excerpt: '<p>[caption id="attachment_220" align="alignright" width=""]<img class="caption alignright size-full wp-image-220" src="http://www.gamevista.de/wp-content/uploads/2009/07/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]Da kann das Wochenende kommen! Ab heute 20 Uhr wird das große <strong>Dawn of War 2</strong> Update „<strong>There is only War</strong>“ über die Onlineplattform <a href="http://store.steampowered.com/" target="_blank" title="steam">Steam </a>erhältlich sein. Es wird einige Änderungen und Anpassungen geben. </p>'
featured_image: /wp-content/uploads/2009/07/1_1280x800_small.jpg

---
Der Patch wird unter anderem das Balancing verbessern. Neben dem üblichen Bugfixing wird es acht neue Mehrspielerkarten geben. Außerdem werdet ihr nun Matches von Freunden mit dem Observer-Feature über die virtuelle Schulter schauen können. Weiterhin wird ein Karteneditor nun in der Community für einen großen Nachschub an Karten sorgen. Was ihr sonst noch erwarten könnt findet ihr in den kompletten <a href="http://community.dawnofwar2.com/viewtopic.php?f=17&#038;t=25492" target="_blank" title="patchnotes">Patchnotes </a>von <a href="http://www.relic.com/" target="_blank" title="relic">Relic</a>.

<a href="http://community.dawnofwar2.com/viewtopic.php?f=17&#038;t=25492" target="_blank" title="patchnotes">> Zu den Patchnotes </a>







 

 

 