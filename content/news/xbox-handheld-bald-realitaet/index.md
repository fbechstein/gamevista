---
title: Xbox-Handheld bald Realität?
author: gamevista
type: news
date: 2009-07-21T10:39:12+00:00
excerpt: '<p>[caption id="attachment_85" align="alignright" width=""]<img class="caption alignright size-full wp-image-85" src="http://www.gamevista.de/wp-content/uploads/2009/07/microsoft_small.png" border="0" alt="Kommt bald ein Xbox-Handheld von Microsoft?" title="Kommt bald ein Xbox-Handheld von Microsoft?" align="right" width="140" height="100" />Kommt bald ein Xbox-Handheld von Microsoft?[/caption]Viele Xbox-Fans haben auf der diesjährigen E3 vergeblich auf eine Xbox-Handheld Enthüllung seitens <a href="http://www.microsoft.com/" target="_blank">Microsoft</a> gewartet. Stattdessen wurde das revolutionäre <strong>Projekt Natal</strong> vorgestellt. Die Pläne für ein Handheld sind jedoch nicht aufgehoben, sondern wahrscheinlich nur verschoben. Microsoft Vice President Shane Kim teilte hierzu mit, dass man sich zur Zeit nur auf Projekt Natal konzentriere, man jedoch Interesse hat sich auf andere Plattformen auszubreiten. </p>'
featured_image: /wp-content/uploads/2009/07/microsoft_small.png

---
Warten wir ab, was aus der Hardware- und Software-Schmiede noch so kommen wird. 





