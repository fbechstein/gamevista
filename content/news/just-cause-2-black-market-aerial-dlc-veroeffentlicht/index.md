---
title: Just Cause 2 – Black Market Aerial DLC veröffentlicht
author: gamevista
type: news
date: 2010-04-30T19:08:05+00:00
excerpt: '<p>[caption id="attachment_1745" align="alignright" width=""]<img class="caption alignright size-full wp-image-1745" src="http://www.gamevista.de/wp-content/uploads/2010/04/justcause2_small.png" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Der Entwickler <strong>Avalanche Studios</strong> hat für das Actionspiel <a href="http://www.justcause.com" target="_blank">Just Cause 2</a> neue Inhalte per Download-Paket veröffentlicht. Das <strong>Black Market Aerial Pack</strong> steht demnach ab sofort zum herunterladen per Onlinedienst Steam bereit.  Inhalt des Packs ist neben dem F-33 Dragon Fly Jet, ein Düsenantrieb für den Fallschirm.</p> '
featured_image: /wp-content/uploads/2010/04/justcause2_small.png

---
Desweiteren gibt es einen Multi-Lock-Raketenwerfer auszuprobieren. Der DLC wird für 1,99 Euro angeboten. Passend zum Release des Packs haben die Entwickler gleich den nächsten Download-Content angekündigt, der am 25. Mai 2010 mit dem Namen **Black Market Boom** erscheinen soll.

 

[> Zum Just Cause 2 &#8211; DLC Trailer][1]

   

* * *

   



 [1]: videos/item/root/just-cause-2-dlc-trailer