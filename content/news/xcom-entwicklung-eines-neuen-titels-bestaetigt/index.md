---
title: XCOM – Entwicklung eines neuen Teils bestätigt
author: gamevista
type: news
date: 2010-04-14T16:55:17+00:00
excerpt: '<p>[caption id="attachment_1684" align="alignright" width=""]<img class="caption alignright size-full wp-image-1684" src="http://www.gamevista.de/wp-content/uploads/2010/04/xcom.jpg" border="0" alt="XCOM" title="XCOM" align="right" width="140" height="100" />XCOM[/caption]Es wird einen weiteren Teil im XCOM-Universum geben. Dies bestätigte der Publisher <strong>2K Games</strong> im Rahmen einer Pressemitteilung. Für die Entwicklung wird das Studio <strong>2K Marin</strong> beauftragt, dass unter anderem für die Produktion von <strong>Bioshock 2</strong> verantwortlich war. Leider bleibt das nicht die einzige Ankündigung. <a href="http://xcom.com" target="_blank">XCOM</a> wird nicht wie seine Vorgänger ein reiner Strategietitel werden, sondern im Genre des Ego-Shooters seinen Platz finden.</p> '
featured_image: /wp-content/uploads/2010/04/xcom.jpg

---
Natürlich wird die Geschichte der XCOM-Serie eine große Rolle spielen und einen guten Teil der Story bilden. Trotzdem bleibt abzuwarten wie **2K Marin** das in die Tat umsetzen möchte. <a href="http://xcom.com" target="_blank">XCOM</a> wird für PC und Xbox 360 erscheinen. Einen genauen Veröffentlichungstermin gibt es bisher nicht.

 

<cite></cite>

   

* * *

   

