---
title: Mass Effect 2 – Subject Zero stellt sich vor
author: gamevista
type: news
date: 2010-01-12T23:00:00+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Zum kommenden Action-Rollenspiel <strong>Mass Effect 2</strong>, hat der Publisher <a href="http://www.masseffect.bioware.com" target="_blank">Electronic Arts</a> einen weiteren Trailer veröffentlicht. In dem eine Minute langen Video stellt sich die Kriegerin Subject Zero mit Hilfe von Spielszenen und Zwischensequenzen vor.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Die Femme Fatale mit kahlgeschorenem Kopf und jede Menger Tattoos versucht sich nicht nur rein äußerlich in Szene zu setzen, sondern hat auch schlagende Argumente wenn es um Waffen und Nahkampf geht.

[> Zum Mass Effect 2 &#8211; Subject Zero Trailer][1]

<cite></cite>

* * *



* * *

 

 [1]: videos/item/root/mass-effect-2-subject-zero-trailer-2