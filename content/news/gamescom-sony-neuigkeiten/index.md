---
title: 'gamescom: weitere Sony Neuigkeiten!'
author: gamevista
type: news
date: 2009-08-19T00:06:18+00:00
excerpt: '<p>[caption id="attachment_394" align="alignright" width="140"]<img class="caption alignright size-full wp-image-394" src="http://www.gamevista.de/wp-content/uploads/2009/08/sony_presse.png" border="0" alt="Sony Pressekonferenz" title="Sony Pressekonferenz" width="140" height="100" align="right" />Sony Pressekonferenz[/caption]<strong>Es gibt einige Neuigkeiten für alle Playstation Fans!</strong></p> <p>Auf der Gamescom Sony Pressekonferenz wurden heute einige spannende Dinge vorgestellt. Neben der Enthüllung der PS3 Slim, wurden noch folgende Ankündigung gemacht, die wir hier zusammenfassen wollen.</p> <p> '
featured_image: /wp-content/uploads/2009/08/sony_presse.png

---
</p> 

**</p> 

PSP Digital Comics</strong>

<span style="text-decoration: underline;"><br /></span>

<img class="caption alignright size-full wp-image-395" src="http://www.gamevista.de/wp-content/uploads/2009/08/ira_small.png" border="0" alt="Ira Rubenstein von Marvell" width="140" height="100" align="right" />In Zusammenarbeit mit Marvel Comics und anderen Publishern sind ab 1. Oktober für die PSP digitale Comics für die Playstation erhältlich. Zum Start im Dezember werden mehrere hundert Comics zum Download bereitstehen und monatlich sollen viele folgen. Ira Rubenstein von Marvel Entertainment präsentierte viele Marvel Titel und hob die PSP als neues Medium und eine neue Art des Comic Lesens.

<span style="text-decoration: underline;"><a href="index.php?Itemid=102&option=com_zoo&view=item&category_id=0&item_id=200">Hier könnt Ihr den Gamescom Trailer zu den Comics ansehen</a></span>

 

 

**PSP Minis<img class=" alignright size-full wp-image-396" src="http://www.gamevista.de/wp-content/uploads/2009/08/minis.png" border="0" alt="Sonys Minispiele - minis" width="140" height="100" align="right" />**

Der Trend geht im Mobile Gaming Bereich stark zum &#8222;das kleine Spiel für Zwischendurch&#8220;, dies hat auch Sony erkannt und stellt mit PSP Minis eine neue Spielereihe vor. Die Spiele haben eine auf 100MB beschränkte Gesamtgröße und eignen sich so zum unkomplizierten Download auf die PSP. Wir haben den Teaser der PSP Minis für euch mitgeschnitten, Ihr findet ihn in unserer Video Section. Erhältlich sind die Minis ab 1. Oktober 2009 und Sony startet mit 15 neuen Spielen; für 2009 sind min. 50 Spiele geplant.

<span style="text-decoration: underline;"><br /></span>

<span style="text-decoration: underline;"><br /></span>

**PSP in neuen Farben und Paketen<img class=" alignright size-full wp-image-397" src="http://www.gamevista.de/wp-content/uploads/2009/08/color_psp.png" border="0" alt="PSP nun auch in Pink" width="140" height="100" align="right" />**

Ebenfalls wurde angekündigt, dass die PSP in neuen Farben und Paketen erhältlich sein wird, so wird die pinke PSP zusammen mit Hannah Montanas Spiel ausgeliefert, die hellblaue zusammen mit Little Big Planet.

<span style="text-decoration: underline;"><br /></span>

**  
** 

**PSP GO**  
Angekündigt wurde Sie bereits, aber für Käufer der ersten Stunde hält Sony noch eine Überaschung bereit, wer also seine PSP GO zwischen 1. &#8211; 10. Oktober bei Sony registriert, erhält Gran Turismo in einer downloadbaren Vollversion kostenlos dazu, keine Demo, keine Shareware, sondern das komplette Spiel.

 

**Playstation Network: Video Delivery Service  und Catch Up TV <img class=" alignright size-full wp-image-398" src="http://www.gamevista.de/wp-content/uploads/2009/08/video_small.png" border="0" alt="Videos on Demand" width="140" height="100" align="right" />**

Das Playstation Network erweitert sein Programm und einen Video-on-Demand Service und beabsichtigt damit zeitnah das Kino ins Wohnzimmer zu holen. Eine weitere Anmeldung oder ähnliches soll laut Sony berichten nicht nötig sein, der Service setzt komplett auf dem Sony Playstation Network auf wird auch darüber abgerechnet. Und für alle Video Fans, wird e alle Videos auch in HD geben.

<span style="text-decoration: underline;"><a href="videos/item/root/gamescom-playstation-network-video-delivery-service">Hier könnt Ihr mehr &#8222;sehen&#8220;</a><br /></span>

Ebenfalls wird das Playstation Network Catch Up TV mit neuen Sendern ausweiteten wird. Unter anderem BBC, ZDF, rtve, antena 3, laSexta und NOS.

 

**Playstation Network: PrePaid Cards<img class=" alignright size-full wp-image-399" src="http://www.gamevista.de/wp-content/uploads/2009/08/network_card.png" border="0" alt="Playstation Network Cards" width="140" height="100" align="right" />**

Für den Playstation Store wird es nun auch Pre-Paid Karten geben, für alle die bisher die Anmeldung aufgrund der benötigten Kreditkarte scheuten. Die Karten werden mit Aufladungen von 20 und 50 Euro erhältlich sein.

<span style="text-decoration: underline;"><br /></span>

 

**Playstation Network: Singstar Room**

Zu guter letzt noch eine Neuigkeit für Playstation Home, es wird in Kürze einen neuen Singstar Room geben, in dem aktuelle Hits sowie weitere Überraschungen zu finden sind.

[Hier könnt Ihr vorab einen kurzen Blick darauf werfen.][1]

Desweiteren wird es bald neue Tanzstile, Bewegungen und Gimmiks geben, wie zum Beispiel Würfeln, Münzen werfen usw.

 

So, dass wars fürs erste von der Gamescom und Sony Pressekonferenz, weiteres folgt später!

* * *



* * *

 [1]: videos/item/root/gamescom-playstation-home-singstar-room