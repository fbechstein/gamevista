---
title: 'Aion – Openbeta Termin *Update*'
author: gamevista
type: news
date: 2009-08-27T20:07:26+00:00
excerpt: '<p>[caption id="attachment_224" align="alignright" width=""]<img class="caption alignright size-full wp-image-224" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion" title="Aion" align="right" width="140" height="100" />Aion[/caption]Publisher <a href="http://www.eu.ncsoft.com/de/">NCSoft</a> teilte nun endlich den lang ersehnten Termin der Open Beta für das Online Rollenspiel <strong>Aion</strong> mit. Demnach soll <strong>Aion</strong> mit der derzeitigen Version 1.5 vom 6. bis 13. September 2009 angetestet werden können. Die Maximalstufe die ihr dabei erreichen könnt ist auf Level 30 begrenzt. Ein kleines Schmankerl für alle Vorbesteller gibt’s auch noch.</p> '
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
[NCSoft][1] bestätigte das Besitzer einer PreOrder Edition schon zwei Tage vor dem offiziellen Start auf die Server dürfen. Der Termin hierfür wäre dann der 23. September 2009.

**\*Update\***

Per Pressemitteilung hat NCSoft nun den PreOrder Start nochmals korrigiert. Für alle die die Vorbestell Version besitzen gehts es bereits am 20. September los. Also 5 Tage vor Release.

* * *



* * *

 

 [1]: http://www.eu.ncsoft.com/de/