---
title: Front Mission 3 – Offiziell angekündigt
author: gamevista
type: news
date: 2010-08-16T17:02:23+00:00
excerpt: '<p>[caption id="attachment_2126" align="alignright" width=""]<img class="caption alignright size-full wp-image-2126" src="http://www.gamevista.de/wp-content/uploads/2010/08/frontmission3.jpg" border="0" alt="Front Mission 3" title="Front Mission 3" align="right" width="140" height="100" />Front Mission 3[/caption]Der Publisher <strong>Square Enix</strong> hat den dritten Teil der Front Mission-Reihe im Rahmen einer Pressemitteilung für den PlayStation-Store angekündigt. Der Strategiespielklassiker, der ursprünglich für die PlayStation veröffentlicht wurde, wird bald über das PlayStation Network als Download für die PlayStation 3 und PSP zur Verfügung stehen.</p> '
featured_image: /wp-content/uploads/2010/08/frontmission3.jpg

---
<a href="http://na.square-enix.com/games/FM3/" target="_blank">Front Mission 3</a> ist ein dynamisches Strategie-Abenteuer, in dem dir in explosiver, hochtechnologischer und rundenbasierter Kriegsführung gigantische mechanische Kampfmaschinen namens „Wanzer“ zum Kommandieren zur Verfügung stehen.

   

* * *

   



 