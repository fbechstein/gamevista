---
title: Assassin’s Creed 2 – Erster Teil der Kurzfilmreihe erschienen
author: gamevista
type: news
date: 2009-10-27T16:33:22+00:00
excerpt: '<p>[caption id="attachment_112" align="alignright" width=""]<a href="http://www.ubi.com" target="_blank"><img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin’s Creed 2" title="Assassin’s Creed 2" align="right" width="140" height="100" />Ubisoft</a>Assassin’s Creed 2[/caption] hat heute den ersten Teil der Kurzfilm-Reihe für das Actionspiel <strong>Assassin’s Creed 2</strong> veröffentlicht. Mit dem Titel <strong>Assassin’s Creed 2: Lineage</strong> hat der Entwickler eine Kurzfilm-Reihe erschaffen die die Vorgeschichte des Spiels erzählen soll. Insgesamt werden drei Teile erscheinen und somit eine Trilogie bilden.</p> '
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Damit schafft <a href="http://www.ubi.com" target="_blank">Ubisoft</a> eine einzigartige Promotion-Aktion die seines Gleichen sucht. Mit fast 15 Minuten Filmszenen und echten Schauspielern wird hier kräftig die Werbetrommel gerührt. Gedreht wurden die Filmchen unter anderen von der Firma Hybride Technologies die Effekte für Kinofilme wie _300_ und _Sin City_ gezaubert haben. Hauptakteur der Filme ist Giovanni Auditore, Vater vom **Assassin’s Creed 2** Helden Ezio. 

[> Zum Assassin’s Creed 2: Lineage &#8211; Kurzfilm Teil 1][1]<cite></cite>

 

* * *



* * *

 

 [1]: videos/item/root/assassins-creed-2-lineage-kurzfilm-teil-1