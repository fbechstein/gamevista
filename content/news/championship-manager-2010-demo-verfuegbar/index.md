---
title: Championship Manager 2010 Demo verfügbar
author: gamevista
type: news
date: 2009-08-15T12:17:18+00:00
excerpt: '<p>[caption id="attachment_361" align="alignright" width="140"]<img class="caption alignright size-full wp-image-361" src="http://www.gamevista.de/wp-content/uploads/2009/08/cmanager_small.png" border="0" alt="Championship Manager 2010" title="Championship Manager 2010" width="140" height="100" align="right" />Championship Manager 2010[/caption]Ab sofort findet Ihr in unserem Downloadbereich eine Demo zum Fussballmanager <strong>Championship Manager 2010</strong> von <a href="http://www.eidos.de/" target="_blank">Eidos.</a><br /><br />In der Demo stehen Euch folgende Features zur Verfügung (Originalauszug / Englisch):</p> '
featured_image: /wp-content/uploads/2009/08/cmanager_small.png

---
  * Available in English, French, Italian, Polish & Spanish languages, enjoy 6 months worth of gameplay from July 1st 2009-December 31st 2009. 
  * Try the match engine featuring over 500 motion captured player animations
  * Create and design intelligent dead ball moves using the set piece creator
  * Invest wisely in the Scouting network (but keep an eye on balancing the books mind!) and see if you can unearth a raw talent
  * Use practice matches & training matches to tweak your side and test your tactics in the run up to games
  * Leagues available in the demo are as follows:  
    England, France, Germany, Italy, Poland, Scotland, Spain, Turkey

<a href="index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=179" target="_self">> Demo zum Championship Manager 2010 herunterladen</a>

* * *



* * *