---
title: 'Battleforge: Lost Souls – Das Addon kommt am 28. Januar 2010'
author: gamevista
type: news
date: 2010-01-05T20:34:27+00:00
excerpt: '<p>Der Publisher Electronic Arts wird am 28. Januar 2010 die Erweiterung „<strong>Lost So[caption id="attachment_1270" align="alignright" width=""]<img class="caption alignright size-full wp-image-1270" src="http://www.gamevista.de/wp-content/uploads/2010/01/battleforge.jpg" border="0" alt="Battleforge" title="Battleforge" align="right" width="140" height="100" />Battleforge[/caption]uls</strong>“ für das Strategiespiel <strong>Battleforge </strong>veröffentlichen. „<strong>Lost Souls</strong>“ wird äußerst Umfangreich sein und bietet insgesamt 120 neue Karten, 56 Einheiten, 32 Gebäude und Zaubersprüche. Mit den Fraktionen des <strong>Zwielichts</strong> und der <strong>Verlorenen Seelen</strong>, wird es damit auch zwei neue Rassen geben.</p> '
featured_image: /wp-content/uploads/2010/01/battleforge.jpg

---
Auch wird das Konzept der dualen Sphären weiter ausgebaut und verbessert. Weiter Informationen stellt der Entwickler auf seiner <a href="http://www.battleforge.com/de/karteneditionen/lost-souls/" target="_blank">Homepage </a>bereit.

 

 

* * *



* * *

 