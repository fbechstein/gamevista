---
title: World of Warcraft – Onyxias Comeback
author: gamevista
type: news
date: 2009-08-16T15:31:41+00:00
excerpt: '<p>[caption id="attachment_163" align="alignright" width=""]<img class="caption alignright size-full wp-image-163" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="139" height="100" />World of Warcraft[/caption]Erst vor kurzem veröffentlichte <a href="http://www.eu.blizzard.com/de/" target="_blank">Blizzard Entertainment</a> den großen Patch 3.2 für sein Online Rollenspiel <strong>World of Warcraft</strong>. Wie nun im offiziellen <a href="http://forums.wow-europe.com/thread.html?topicId=10546325009&sid=3" target="_blank" title="wow forum">WoW-Forum</a> von Mitarbeiter Cerunya mitgeteilt wurde wird mit dem nächsten Patch 3.2.2 der Drache Onyxia ein Comeback erleben. Der Ursprünglich für Stufe 60 erstellte Boss ist schon lange kein Gegner mehr für Schlachtzugsgruppen und wird deswegen überarbeitet.</p> '
featured_image: /wp-content/uploads/2009/07/wow_small.png

---
Neue Beute und ein Onyxia Reittiert werden neuen Anreiz geben diesen Boss, der dann auch der Stufe 80 angepasst wird, zu legen. Weiterhin wird im Rahmen des fünften Geburtstages ein Drachenwelpe von Onyxia im Briefkasten der Spieler zu finden sein. Wann genau der Patch erscheinen wird ist bisher unbekannt.

 







 

 

 

 