---
title: Dark Void – Demo veröffentlicht
author: gamevista
type: news
date: 2010-01-14T19:04:24+00:00
excerpt: '<p>[caption id="attachment_415" align="alignright" width=""]<img class="caption alignright size-full wp-image-415" src="http://www.gamevista.de/wp-content/uploads/2009/08/darkvoid_small.jpg" border="0" alt="Dark Void" title="Dark Void" align="right" width="140" height="100" />Dark Void[/caption]Der Entwickler <strong>Capcom </strong>hat nun doch eine Demo zum Actionspiel <a href="http://www.darkvoidgame.com" target="_blank">Dark Void</a> für den PC veröffentlicht. Kürzlich hatten Capcom-Mitarbeiter eine PC-Testversion noch ausgeschlossen haben. Nun könnt ihr euch, kurz vor dem Release des Jetpack-Shooters, eine Meinung bilden.</p> '
featured_image: /wp-content/uploads/2009/08/darkvoid_small.jpg

---
Die 1 Gigabyte große Demo beinhaltet erste Missionen von <a href="http://www.darkvoidgame.com" target="_blank">Dark Void</a>. Die Vollversion erscheint am 22. Januar 2010 im Handel.

[> Zur Dark Void &#8211; Demo][1]<cite></cite>

 

<cite></cite>

* * *



* * *

 

 [1]: downloads/demos/item/demos/dark-void