---
title: 'Star Wars: The Force Unleashed 2 – Demo kommt, aber nicht für PC'
author: gamevista
type: news
date: 2010-10-06 18:25:50 +0000
excerpt: '<img class="caption alignright size-full wp-image-1192" src="http://www.gamevista.de/wp-content/uploads/2009/12/tfu2.jpg"
  border="0" alt="Star Wars: The Force Unleashed 2" title="Star Wars: The Force Unleashed
  2" align="right" width="140" height="100" />Star Wars: The Force Unleashed 2 Das
  Actionspiel <a href="http://www.lucasarts.com/games/theforceunleashed2/" target="_blank">Star
  Wars: The Force Unleashed 2</a> wird demnächst eine Demo erhalten. Die Xbox 360
  und PlayStation 3-Version wird am 12. Oktober 2010 über PSN bzw. Xbox LIVE veröffentlicht.
  Publisher LucasArts hat nun eine PC-Demo offiziell ausgeschlossen.'
featured_image: "/wp-content/uploads/2009/12/tfu2.jpg"

---
Star Wars: The Force Unleashed 2 wird demnächst eine Demo erhalten. Die Xbox 360 und PlayStation 3-Version wird am 12. Oktober 2010 über PSN bzw. Xbox LIVE veröffentlicht. Publisher LucasArts hat nun eine PC-Demo offiziell ausgeschlossen.