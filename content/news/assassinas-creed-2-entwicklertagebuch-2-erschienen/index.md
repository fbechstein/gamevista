---
title: 'Assassin´s Creed 2 – Entwicklertagebuch #2 erschienen'
author: gamevista
type: news
date: 2009-08-12T11:32:03+00:00
excerpt: '<p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin´s Creed 2" title="Assassin´s Creed 2" align="right" width="140" height="100" />Assassin´s Creed 2[/caption]Ab sofort könnt Ihr Euch das zweite Entwicklertagebuch zu <strong>Assassin´s Creed 2</strong> von insgesamt sechs in unserer Videosektion anschauen.</p><p>Im zweiten Video-Tagebuch gibt es Informationen zu den Fraktionen, die Ezio in verschiedenen Situationen zur Seite stehen. Außerdem erfahrt Ihr Einzelheiten zum neuen Wirtschaftssystem von Assassin’s Creed II. Dieses ermöglicht Euch, auf verschiedene Art und Weise Geld zu verdienen und in Ausrüstung zu investieren.</p>'
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Assassin’s Creed II ist bereits über zwei Jahre in intensiver Entwicklungsarbeit. Verantwortlich zeigt sich das Team des erfolgreichen Vorgängers Assassin’s Creed. In einer riesigen offenen Welt verkörpert der Spieler den jungen Adligen Ezio, der von den rivalisierenden Herrscherfamilien im Italien des Renaissance-Zeitalters betrogen wird. Ezios folgender Rachefeldzug zieht den Spieler in eine epische Geschichte, die viel Abwechslung, überraschende Spielelemente, eine Vielzahl von Waffen und eine tiefgründige Charakterentwicklung verspricht, was sowohl Fans als auch Neueinsteigern begeistern wird. 

[> Assassin´s Creed 2  &#8211; Entwicklertagebuch #2 anschauen][1] 







 [1]: index.php?Itemid=102&option=com_zoo&view=item&category_id=0&item_id=170