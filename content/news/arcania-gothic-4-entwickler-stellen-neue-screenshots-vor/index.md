---
title: 'Arcania: Gothic 4 – Entwickler stellen neue Screenshots vor'
author: gamevista
type: news
date: 2010-04-26T17:20:09+00:00
excerpt: '<p>[caption id="attachment_1648" align="alignright" width=""]<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Publisher <strong>JoWood Entertainment</strong> präsentiert euch heute neue Screenshots zum Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a>. Der neue Teil der Serie spielt rund 12 Jahre nach den Ereignissen von Gothic 3 und nimmt die Spieler ein weiteres Mal auf die Reise in das vom Krieg gezeichnete Universum mit.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
Der alte namenlose Held ist nunmehr zum König gekrönt worden und regiert mit strenger Hand über Myrtana. Gothic 4 wird einen neuen Helden vorstellen der als unscheinbarer Hirte auf der Insel Feshyr zum Vorschein kommt. Dieser gibt sich nicht mit der Tyrannei des Königs zufrieden und tritt dem König entgegen.

[> Zu den Arcania: Gothic 4 &#8211; Screenshots][1]

<p style="text-align: center;">
  <img class=" size-full wp-image-1729" src="http://www.gamevista.de/wp-content/uploads/2010/04/arcania_gothic4_resize.jpg" border="0" width="600" height="356" srcset="http://www.gamevista.de/wp-content/uploads/2010/04/arcania_gothic4_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/04/arcania_gothic4_resize-300x178.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

* * *

 __

 

 [1]: screenshots/item/root/arcania-a-gothic-tale