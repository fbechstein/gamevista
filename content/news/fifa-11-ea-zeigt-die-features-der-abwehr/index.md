---
title: FIFA 11 – EA zeigt die Features der Abwehr
author: gamevista
type: news
date: 2010-07-19T13:58:18+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="FIFA 11" title="FIFA 11" align="right" width="140" height="100" />FIFA 11[/caption]Publisher <strong>Electronic Arts</strong> präsentiert für ihre diesjährige Ausgabe von FIFA ein weiteres Entwicklervideo. Der Trailer widmet sich diesmal der überarbeiteten Abwehr und stellt neue Features vor. Neben effektiverer Abseitsfalle soll auch die KI näher am Stürmer agieren und so das Tore erzielen ein Tick weit schwieriger machen als in FIFA 10.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Das Gameplay-Video zeigt somit alle Aspekte der neuen Abwehr in <a href="http://fifa.easports.com" target="_blank">FIFA 11</a>.

[> Zum FIFA 11 &#8211; Abwehr Trailer][1]

 

   

* * *

   



 [1]: videos/item/root/fifa-11-abwehr-trailer