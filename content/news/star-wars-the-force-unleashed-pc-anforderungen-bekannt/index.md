---
title: 'Star Wars: The Force Unleashed – PC-Anforderungen bekannt'
author: gamevista
type: news
date: 2009-10-18T12:24:05+00:00
excerpt: '<p>[caption id="attachment_233" align="alignright" width=""]<img class="caption alignright size-full wp-image-233" src="http://www.gamevista.de/wp-content/uploads/2009/07/swu_small.png" border="0" alt="Star Wars: The Force Unleashed" title="Star Wars: The Force Unleashed" align="right" width="140" height="100" />Star Wars: The Force Unleashed[/caption]Der Entwickler <a href="http://www.aspyr.com/" target="_blank">Aspyr</a>, welcher für PC- und Mac-Umsetzungen von Konsolenspielen bekannt ist, hat nun die Hardwareanforderungen für <strong>Star Wars: The Force Unleashed</strong> bekannt gegeben.</p> '
featured_image: /wp-content/uploads/2009/07/swu_small.png

---
Minium:

  * CPU Processor: 2.4 GHz Dual Core Processor (Intel Core 2 Duo or AMD Athlon X2)
  * Memory: 2 GB RAM
  * Hard Disk Space: 23.8 GB + 1 GB Swap File
  * Video Card: 3D Hardware Accelerator Card Required – 100% DirectX 9.0c compatible 256 MB Video Memory with Shader 3.0 support
  * Video Card (ATI): Radeon HD 2900
  * Video Card (Nvidia): Geforce 8800
  * Media Required: 8X DVD-ROM drive
  * Windows XP/Vista compatible mouse and keyboard or Microsoft Xbox 360 Wired Controller

 

Empfohlen:

  * Intel Core 2 Duo 2.8 GHz or AMD Athlon X2 Dual-Core 5200+
  * Memory: 2 GB RAM
  * 512 MB 3D Hardware Accelerator Card
  * Video Card (ATI): Radeon HD 4870
  * &#8211; Video Card (Nvidia): Geforce 9800 GT
  * Unterstütze Dektop-Grafikchips: ATI RADEON HD 2900, 3850, 3870, 4850, 4870 und NVIDIA GEFORCE 8800, 9600, 9800, 260, 280 

 

Das Spiel mit dem Beititel “Ultimate Sith Edition“ erscheint am 19. November. Beachtenswert ist, dass es unglaubliche 25 GB Festplattenspeicher belegt. Alle Star Wars Fans, die am PC spielen sind sicher schon sehr gespannt auf das Spiel. Natürlich testen wir das Spiel sobald es erscheint.

* * *



* * *

 