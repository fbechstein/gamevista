---
title: Tom Clancy’s H.A.W.X. 2 – Open Skies DLC veröffentlicht
author: gamevista
type: news
date: 2010-11-24T16:27:40+00:00
excerpt: |
  |
    <p>[caption id="attachment_2000" align="alignright" width=""]<img class="caption alignright size-full wp-image-2000" src="http://www.gamevista.de/wp-content/uploads/2010/07/hawx2_s_003.jpg" border="0" alt="Tom Clancy's H.A.W.X. 2" title="Tom Clancy's H.A.W.X. 2" align="right" width="140" height="100" />Tom Clancy's H.A.W.X. 2[/caption]Publisher <strong>Ubisoft </strong>hat den ersten Zusatzinhalt für die Flugkampfsimulation <a href="http://shop.ubi.com/store/ubiemea/de_DE/pd/ThemeID.8605700/productID.216992500/Tom-Clancys-HAWX-2.html" target="_blank">Tom Clancy's H.A.W.X. 2</a> veröffentlicht. Der Downloadable-Content mit dem Namen Open Skies steht ab sofort für knapp 10 US-Dollar zur Verfügung. Als Inhalt zählen drei neue Missionen, fünf zusätzliche Skins für eure Flugzeuge, sowie fünf neue Jäger.</p>
featured_image: /wp-content/uploads/2010/07/hawx2_s_003.jpg

---
Die komplette Liste der Inhalte im Überblick:

 

**Inhalt:** 

&#8211; Neues Flugzeug: F-4E-Phantom II   
&#8211; Neues Flugzeug: F-86 Sabre   
&#8211; Neues Flugzeug: Mig-15 Fagot   
&#8211; Neues Flugzeug: Mig-21 Fishbed   
&#8211; Neues Flugzeug: Su T-50 (PAK FA)   
&#8211; Bonus-Skin für die F-22 Raptor   
&#8211; Bonus-Skin für die F-35B Lightning II   
&#8211; Bonus-Skin für den Eurofighter Typhoon   
&#8211; Bonus-Skin für die A-10A Thunderbolt II   
&#8211; Bonus-Skin für die Su-35BM   
&#8211; Neue Team Battle Mission: Search and Destroy   
&#8211; Neue Team Battle Mission: High Altitude Warfare   
&#8211; Neue Team Battle Mission: Trigger-Happy

   

* * *

   

