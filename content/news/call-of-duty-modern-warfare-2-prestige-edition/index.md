---
title: 'Call of Duty: Modern Warfare 2 – Prestige Edition'
author: gamevista
type: news
date: 2009-07-14T12:37:25+00:00
excerpt: '<p>[caption id="attachment_43" align="alignright" width=""]<img class="caption alignright size-full wp-image-43" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Modern Warfare 2 Prestige Collection kommt mit einem Nachtsichtgerät" title="Modern Warfare 2 Prestige Collection kommt mit einem Nachtsichtgerät" align="right" width="140" height="100" />Modern Warfare 2 Prestige Collection kommt mit einem Nachtsichtgerät[/caption]Heutzutage ist es üblich zu jedem Game-Highlight eine Collectors Edition zu veröffentlichen. Was jedoch der Publisher <a href="http://www.activision.com/" target="_blank">Activision</a> vorhat, sprengt alle bisherigen Dimensionen. Für das Spiel <strong>Call of Duty: Modern Warfare 2</strong> wird es eine sogenannte Prestige Edition geben.  <br />Neben dem Spiel in einem Steelbook, einem Artbook und einem Downloadcode für Call of Duty Classic wird es ein Nachtsichtgerät geben. Dabei handelt es sich nicht um ein Nachbau oder Spielzeug ohne wirkliche Funktionen, sondern um ein Gerät mit einer kompletten Funktionsweise. </p>'
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Ob das Spiel jedoch in der Prestige Edition den Sprung nach Deutschland schafft ist allerdings unklar. Ebenso ist der Preis unklar, der jedoch um die $150,- liegen sollte. 

Wer es noch immer nicht glauben kann, der kann sich im folgendem Video davon überzeugen:

[> zum Call of Duty: Modern Warfare 2 &#8211; Prestige Edition Video][1]







 [1]: videos/item/root/call-of-duty-modern-warfare-2-prestige-edition-video