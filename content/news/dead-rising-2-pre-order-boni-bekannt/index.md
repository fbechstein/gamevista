---
title: Dead Rising 2 – Pre-Order-Boni bekannt
author: gamevista
type: news
date: 2010-07-12T19:48:12+00:00
excerpt: '<p>[caption id="attachment_1442" align="alignright" width=""]<img class="caption alignright size-full wp-image-1442" src="http://www.gamevista.de/wp-content/uploads/2010/02/deadrising2.jpg" border="0" alt="Dead Rising 2" title="Dead Rising 2" align="right" width="140" height="100" />Dead Rising 2[/caption]Publisher <strong>Capcom </strong>hat die Liste der Bonus-Items für alle Vorbesteller von <a href="http://www.capcom.com" target="_blank">Dead Rising 2</a> veröffentlicht. Als Inhalt zählt somit z.B. das Zusatzpaket mit dem Namen <em>Paradise Pack</em>. Den Inhalt des DLC gab <strong>Capcom </strong>bisher noch nicht bekannt. Fest steht das der Download-Content Vorbestellern exklusiv für einige Zeit zur Verfügung steht.</p> '
featured_image: /wp-content/uploads/2010/02/deadrising2.jpg

---
Danach kann man den Inhalt auch regulär über den PlayStation-Store erwerben.    
Ein weiterer Inhalt der Pre-Order-Edition besteht aus den sogenannten Combo Cards. Diese sind reale Nachdrucke aus den im Spiel enthaltenen Karten. Jeder Vorbesteller soll 3 von 31 Karten erhalten. <a href="http://www.capcom.com" target="_blank">Dead Rising 2</a> erscheint am 3. September 2010 im Handel.

 

   

* * *

   

