---
title: World of Warcraft – Über 12 Millionen Spieler
author: gamevista
type: news
date: 2010-10-07T20:39:12+00:00
excerpt: '<p>[caption id="attachment_2319" align="alignright" width=""]<img class="caption alignright size-full wp-image-2319" src="http://www.gamevista.de/wp-content/uploads/2010/10/wow_small2.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="140" height="100" />World of Warcraft[/caption]<strong>Blizzard Entertainment</strong> präsentierte heute die neusten Abonnementen-Zahlen für das wohl erfolgreichste Online-Rollenspiel <a href="http://www.worldofwarcraft.com/cataclysm" target="_blank">World of Warcraft</a>. Weltweit sind mittlerweile über 12 Millionen Spieler im Warcraft-Universum unterwegs. Grund dafür dürften die letzten sehr erfolgreichen Addons sein.</p> '
featured_image: /wp-content/uploads/2010/10/wow_small2.png

---
Der neuste Clou erscheint am 7. Dezember 2010 in Form des nächsten Zusatzinhalts **Cataclysm**. Dieses hebt die Levelgrenze auf die Stufe 85 an. Außerdem werden zwei neue Rassen in das Spiel eingeführt. Zum einen wären da die Goblins für die Fraktion der Horde, zum anderen werden die Worge spielbar für die Fraktion der Allianz sein.

 

   

* * *

   



 