---
title: 'Dragon Age: Origins – Charakter Editor zum Download verfügbar'
author: gamevista
type: news
date: 2009-10-13T23:00:00+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Der Publisher <a href="http://eu.dragonage.com" target="_blank">Electronic Arts</a> hat kurz vor dem Release des Rollenspiels <strong>Dragon Age: Origins</strong> den <strong>Charakter Editor</strong> veröffentlicht. Mit dem <strong>Dragon Age: Origins Character Creator</strong> könnt ihr eure Helden erschaffen und zwischen drei Völkern wählen.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Außerdem gibt es dazu noch den verschiedenen Klassen mit den dazugehörigen Hintergrundgeschichten. Neben dem Aussehen könnt ihr auch Namen und Stimme festlegen und Attributspunkte sowie Fähigkeiten und Talente wählen. Nachdem ihr euren Charakter erstellt habt könnt ihr dieses Speichern um diesen bei der Veröffentlichung der PC Version am 5. November 2009 zu verwenden. Auch könnt ihr euren Charakter online präsentieren und als online Avatar im <a href="http://social.bioware.com/" target="_blank">BioWare Social Network</a> nutzen.

[> Zum Dragon Age: Origins &#8211; Charakter Editor Download][1]

* * *



* * *

 

 

 [1]: downloads/demos/item/demos/dragon-age-origins-charakter-editor