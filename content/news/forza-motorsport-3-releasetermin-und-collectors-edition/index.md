---
title: Forza Motorsport 3 – Releasetermin und Collectors Edition
author: gamevista
type: news
date: 2009-07-29T11:22:38+00:00
excerpt: '<p>[caption id="attachment_200" align="alignright" width=""]<img class="caption alignright size-full wp-image-200" src="http://www.gamevista.de/wp-content/uploads/2009/07/fm3_eurosport_14_2_small.jpg" border="0" alt="Forza Motorsport 3" title="Forza Motorsport 3" align="right" width="140" height="100" />Forza Motorsport 3[/caption]Für das Rennspiel <strong>Forza Motorsport 3</strong> von <a href="http://forzamotorsport.net" target="_blank" title="microsoft">Microsoft</a> wurde nun ein Releasetermin bekannt gegeben. Demnach soll <strong>Forza Motorsport 3</strong> am 27. Oktober 2009 in den USA erscheinen. Eine weitere gute Nachricht ist das Gamestop in den USA, leider exclusiv, eine Collectiors Edition anbietet.</p>'
featured_image: /wp-content/uploads/2009/07/fm3_eurosport_14_2_small.jpg

---
Mit dieser erhaltet ihr einen VIP Zugang zur Forza – Community. Diese hat Zugang zu folgenden Wagen, 2009 Aston Martin DBS, 2009 Chevrolet Corvette ZR1, 2007 Ferrari 430 Scuderia, 2007 Lamborghini Gallardo Superleggera und den 2007 Porsche 911 GT3 RS (997). Ihr erlangt außerdem durch das im Paket enthaltene Collectors Car Pack weitere exklusive Wagen wie den <span> </span>Audi R8 5.2 FSI Quattro, Acura NSX, Nissan Fairlady, Aston Martin DB9 oder den 2008 Dodge Charger Stock Car. Natürlich ist das noch nicht alles. <a href="http://forzamotorsport.net" target="_blank" title="turn 10">Turn 10</a>, Entwickler von **Forza Motorsports 3**, verspricht zudem noch Design- und Tuningkits die exklusiv für die VIP Community erstellt würden. Ihr erhaltet dann noch einen schicken Schlüsselanhänger und 2GB Speicherplatz für Videos, Designs und Fotos. Einen genauen Deutschland Termin wurde bisher noch nicht genannt.

**\*Update\*</p> 

</strong>Wie wir gerade erfahren haben wird der Europa Release von Forza Motorsport am 23. Oktober 2009 sein.







 