---
title: Mini Ninjas – Veröffentlichungstermin
author: gamevista
type: news
date: 2009-08-17T20:47:54+00:00
excerpt: '<p class="MsoNormal">[caption id="attachment_128" align="alignright" width=""]<img class="caption alignright size-full wp-image-128" src="http://www.gamevista.de/wp-content/uploads/2009/07/mini_ninjas_small.jpg" border="0" alt="Mini Ninjas" title="Mini Ninjas" align="right" width="140" height="100" />Mini Ninjas[/caption]Heute hat <a href="http://www.eidos.de/" target="_blank">Eidos Interactive</a> per Pressemitteilung den Veröffentlichungstermin für das Spiel <strong>Mini Ninjas</strong> bestätigt. Demnach wird das Abenteuer um die kleinen Helden am 9. Oktober 2009 für Nintendo Wii, Nintendo DS, Xbox 360, Playstation 3 und Pc erscheinen. Eine Demoversion wird am 19. August für Pc, Xbox 360 und Playstation 3 bereit gestellt.</p> <p class="MsoNormal"> </p> '
featured_image: /wp-content/uploads/2009/07/mini_ninjas_small.jpg

---
**Mini Ninjas** entführt euch in ein Abenteuer der besonderen Art. Ihr helft Hiro, Futo und den Rest des Ninja-Clans bei ihrem größten Ninja-Abenteuer. Auf einer epischen und packenden Reise durchs mittelalterliche Japan machen die Mini-Kämpfer sich daran, Harmonie in eine Welt zurückzubringen, die sich am Rande des Chaos‘ bewegt.

 

* * *



* * *

 

 