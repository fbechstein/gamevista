---
title: 'Kane & Lynch 2: Dog Days – Releasetermin bekannt'
author: gamevista
type: news
date: 2010-03-17T19:43:12+00:00
excerpt: '<p>[caption id="attachment_1576" align="alignright" width=""]<img class="caption alignright size-full wp-image-1576" src="http://www.gamevista.de/wp-content/uploads/2010/03/kane-and-lynch2.jpg" border="0" alt="Kane & Lynch 2: Dog Days" title="Kane & Lynch 2: Dog Days" align="right" width="140" height="100" />Kane & Lynch 2: Dog Days[/caption]Der Publisher <strong>Square Enix</strong> hat im Rahmen einer Pressemitteilung, den genauen Termin für das Erscheinen von <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> bekannt gegeben. Demnach wird der zweite Teil in Europa am 26. August 2010 auf Xbox 360, PlayStation 3 und PC erscheinen. Passend dazu liefert der Publisher ein Video mit Spielszenen.</p> '
featured_image: /wp-content/uploads/2010/03/kane-and-lynch2.jpg

---
[> Zum Kane & Lynch 2: Dog Days &#8211; Trailer][1]

 

<cite></cite>

   

* * *

   



 

 [1]: videos/item/root/kane-a-lynch-2-dog-days-trailer