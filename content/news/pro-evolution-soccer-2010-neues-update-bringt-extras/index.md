---
title: Pro Evolution Soccer 2010 – Neues Update bringt Extras
author: gamevista
type: news
date: 2009-12-21T15:59:41+00:00
excerpt: '<p>[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]Zum Fußballspiel <strong>Pro Evolution Soccer 2010</strong> wurde ein neues Update veröffentlicht. Das ab sofort zum Download erhältliche Paket für Xbox 360, PlayStation 3 sowie PC enthält frei verfügbare Inhalte, unter anderem drei neue Bälle und zehn neue Schuhmodelle.</p> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
Außerdem wird das Interface rund um die Formationseinstellungen vereinfacht. So kann man nun alle Einstellungen speichern und die gespeicherten Daten werden automatisch geladen. Für den Multiplayer-Modus gibt es nun ein neues Icon für die Verbindungsqualität zwischen Online-Spielern. Der Patch wird automatisch über den Spielinternen Downloader installiert.

 

* * *



* * *

 