---
title: Borderlands – Zu Ostern über Steam günstiger
author: gamevista
type: news
date: 2010-04-03T12:36:18+00:00
excerpt: '<p>[caption id="attachment_1656" align="alignright" width=""]<img class="caption alignright size-full wp-image-1656" src="http://www.gamevista.de/wp-content/uploads/2010/04/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Der Online-Dienst <a href="http://store.steampowered.com/agecheck/app/8980/" target="_blank">Steam</a> bietet derzeit den Rollenspiel Ego-Shooter <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> passend zu den Osterfeiertagen günstig an. <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> wird somit bis Montag um 50% billiger angeboten. Demnach kostet das Spiel nur noch 24,99 EUR.</p> '
featured_image: /wp-content/uploads/2010/04/smalllogo.jpg

---
Das <a href="http://store.steampowered.com/agecheck/app/8980/" target="_blank">Angebot</a> gilt natürlich auch für das vierer Paket mit dem sie vier Kopien von <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> für Freunde und Bekannte für derzeit nur 74,98 EUR erhalten.

 

<cite></cite>

   

* * *

   

