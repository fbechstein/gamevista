---
title: Fifa 11 – Offiziell angekündigt und erste Details
author: gamevista
type: news
date: 2010-06-09T18:23:55+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="Fifa 11" title="Fifa 11" align="right" width="140" height="100" />Fifa 11[/caption]Publisher <a href="http://www.ea.com/" target="_blank">Electronic Arts</a> hat heute den offiziellen Veröffentlichungstermin für das Fussballspiel <strong>Fifa 11</strong> bekannt gegeben. Demnach wird der neuste Fifa Spross im Herbst 2010 erscheinen. Außerdem wurden erste Details gelüftet. Somit wird <a href="http://www.ea.com/" target="_blank">Fifa 11</a> wieder für alle drei Systeme, PC, PlayStation 3 und Xbox 360, entwickelt. Ob die PC-Umsetzung dieses Jahr besser ausfällt bleibt abzuwarten.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Fifa 10 war lange Zeit nicht pnline-fähig und viel grafisch weit hinter die Konsolenversionen zurück. 

Das neue Feature Persönlichkeit+ berücksichtigt neben den Standardattributen auch individuelle Fähigkeiten und ermöglicht es, jeden Spieler mittels seiner Physis und Spielanlagen klar unterscheiden zu können. Persönlichkeit+ arbeitet die Unterschiede einzelner Fußballer mithilfe einer Datenbank heraus, die deren Fähigkeiten-Sets mittels 36 Attributen und 57 Eigenschaften evaluiert und adaptiert.

**FIFA 11** führt auch das neue Pro Passing ein, wobei die Passgenauigkeit vom Können des Videospielers, den Fähigkeiten des virtuellen Fußballspielers selbst, der Situation und dem Druck auf dem Feld abhängt. Das ergibt im Ganzen ein enorm verbessertes Passsystem. Eine schlechte Entscheidung oder eine falsche Schusskraftregulierung beim Passen werden fehlerhafte Resultate produzieren. Mit neuen Passtypen wie dem Außenristpass können Spieler ein effektiveres Spiel aufziehen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1899" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11_rooney.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11_rooney.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/06/fifa11_rooney-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

