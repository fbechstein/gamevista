---
title: 'Spellforce 2: Faith in Destiny – Neue Details zum Addon'
author: gamevista
type: news
date: 2010-02-15T16:43:24+00:00
excerpt: '<p>[caption id="attachment_1447" align="alignright" width=""]<img class="caption alignright size-full wp-image-1447" src="http://www.gamevista.de/wp-content/uploads/2010/02/spellforcefid_small.jpg" border="0" alt="Spellforce 2: Faith in Destiny" title="Spellforce 2: Faith in Destiny" align="right" width="140" height="100" />Spellforce 2: Faith in Destiny[/caption]Der Publisher <strong>JoWood </strong>hat im Rahmen einer Pressemitteilung neue Details zur Erweiterung <a href="http://spellforce.jowood.com/" target="_blank">Faith in Destiny</a>, für das Strategiespiel <a href="http://spellforce.jowood.com/" target="_blank">Spellforce 2</a>, veröffentlicht. Des Weiteren gibt es drei neue Screenshots zu betrachten. Die Erweiterung wird die <a href="http://spellforce.jowood.com/" target="_blank">Spellforce 2</a>-typische Story weiterführen und dabei eine Vielzahl an neuen Einheiten, Gebäuden, Zaubersprüchen und Missionen implementiert.</p> '
featured_image: /wp-content/uploads/2010/02/spellforcefid_small.jpg

---
Die Geschichte der Fortsetzung dreht sich um eine geheimnisvolle Macht. Die Eo, die Welt von SpellForce, bedroht. Als Spieler schmieden Sie ein Bündnis zwischen Shaikan und ihren Gefährten, um sich der nahenden Gefahr zu stellen und das Unheil ein für alle Mal zu vernichten. _„Da die Nachfrage in den Foren und Communities nach wie vor ungebrochen ist, richtet sich das Addon SpellForce Faith in Destiny kl ar an Fans der Vorgängertitel. Wir haben dieses Spiel bewusst als Addon konzipiert. Es soll allen SpellForce begeisterten Spielern neuen Content, also Einheiten, Monster, Fraktionen, Karten und natürlich Story, für ihr geliebtes Universum liefern. Und wer weiß? Vielleicht ist es ja auch ein Brückenschlag zu einer neuen, noch viel größeren Geschichte?“_ erklärt Stefan Berger, Head of Publishing **JoWooD Entertainment**. Der Veröffentlichungstermin liegt bisher beim zweiten Quartal 2010. 

**Features:**    
• Viele spannende, aufwändig gestaltete Missionen    
• Der Avatar kann nun auf Drachen reiten    
• Alte und neue NPCs: Professor Drehlein ist wieder da – und mit ihm einige der bekanntesten NPCs der SpellForce-Reihe sowie neuen Charakteren    
• Ein komplett neue Rasse, die das Schicksal von Eo bedroht    
• Neue Umgebungen, Gebäude und Einheiten    
• Neue Zauber und Einheiten, die vielfältige taktische Möglichkeiten bieten

[> Zur Spellforce 2: Faith in Destiny &#8211; Screenshot Galerie][1]

 

* * *



* * *

 [1]: screenshots/item/root/spellforce-faith-of-destiny