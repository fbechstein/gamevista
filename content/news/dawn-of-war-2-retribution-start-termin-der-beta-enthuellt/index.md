---
title: 'Dawn of War 2: Retribution – Start Termin der Beta enthüllt'
author: gamevista
type: news
date: 2011-01-17T18:44:34+00:00
excerpt: '<p><strong>[caption id="attachment_2518" align="alignright" width=""]<img class="caption alignright size-full wp-image-2518" src="http://www.gamevista.de/wp-content/uploads/2011/01/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]THQ </strong>hat bestätigt, dass die Erweiterung zu <a href="http://www.dawnofwar2.com" target="_blank">Dawn of War 2</a> mit dem Namen Retribution, eine offizielle Mehrspieler-Beta erhalten wird. Diese wird vom 31. Januar bis zum 24. Februar 2011 laufen. Die Testversion wird alle sechs Mehrspieler-Rassen, inklusive der neuen Imperialen Garde enthalten.</p> '
featured_image: /wp-content/uploads/2011/01/1_1280x800_small.jpg

---
Als Neuerung wird erstmals Steam als Matchmaking-Feature genutzt, sowie als Bug-Report-System. Natürlich dürfen auch alle neuen Einheiten für jede Rasse ausprobiert werden, sowie aller neuen Mehrspielerkarten. Retribution, der nächste DLC für das Strategiespiel <a href="http://www.dawnofwar2.com" target="_blank">Warhammer 40k: Dawn of War 2</a>, erscheint am 1. März 2011.

 

 

* * *



* * *