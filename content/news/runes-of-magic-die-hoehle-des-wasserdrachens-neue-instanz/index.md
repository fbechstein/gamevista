---
title: Runes of Magic – Die Höhle des Wasserdrachens neue Instanz
author: gamevista
type: news
date: 2009-10-01T17:52:14+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Publisher <a href="http://www.frogster-interactive.de">Frogster Interactive</a> hat heute per offizieller Pressemitteilung neuen Content für das kostenlose Online Rollenspiel <strong>Runes of Magic</strong> angekündigt. Eine neue Instanz soll es sein und diese trägt den Namen <em>Die Höhle des Wasserdrachens</em>.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Mit dem Veröffentlichungstermin ist schon Anfang der nächsten Woche zu rechnen, denn der neue Dungeon wird im Rahmen eines Inhaltsupdates gemeinsam erscheinen. Damit ihr die Instanz betreten könnt, müssen Spieler von **Runes of Magic,** erstmal einmal den Zugang über den Thron des Wassers erkämpfen. Dieser liegt in der Region _Küste der Wehklagen._ __Es gibt auch ein neuen Trailer zur Instanz der euch erste Einblicke geben soll.

 

[> Zum Runes of Magic &#8211; Die Höhle des Wasserdrachens][1]

* * *



* * *

 

 [1]: videos/item/root/runes-of-magic-die-hoehle-des-wasserdrachens