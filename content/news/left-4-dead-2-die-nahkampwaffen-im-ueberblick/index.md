---
title: Left 4 Dead 2 – Die Nahkampwaffen im Überblick
author: gamevista
type: news
date: 2009-09-10T16:57:25+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Publisher <a href="http://www.l4d.com">Valve</a> hat heute eine komplette Liste der Nahkampfwaffen, welche im Zombieshooter <strong>Left 4 Dead 2</strong> enthalten sind, veröffentlicht. Laut einem Interview auf der Penny Arcade Expo in Seattle wurden bisher zehn Nahkampfwaffen vom <strong>Left 4 Dead</strong> Autor Chet Faliszek genannt. Hier die kleine aber feine Liste und ja es gibt eine Kettensäge!</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
  * Baseball-Schläger
  * Cricket-Schläger
  * Brecheisen
  * Elektrische Gitarre
  * Feuerwehraxt
  * Bratpfanne
  * Katana
  * Machete
  * Schlagstock
  * Kettensäge 

* * *



* * *