---
title: Herr der Ringe Online – Ist nun offiziell ein Free-to-Play-Titel
author: gamevista
type: news
date: 2010-09-12T11:20:31+00:00
excerpt: '<p>[caption id="attachment_1004" align="alignright" width=""]<img class="caption alignright size-full wp-image-1004" src="http://www.gamevista.de/wp-content/uploads/2009/11/lotro_small.jpg" border="0" alt="Herr der Ringe Online" title="Herr der Ringe Online" align="right" width="140" height="100" />Herr der Ringe Online[/caption]Publisher<strong> Warner Bros.</strong> und Entwickler <strong>Turbine </strong>haben die Tore zur kostenlosen Version des Online-Rollenspiels <a href="http://www.lotro.com/" target="_blank">Herr der Ringe Online</a> geöffnet, zumindest in Nord Amerika. Spieler können ab jetzt auf der <a href="http://www.lotro.com/" target="_blank">amerikanischen Website</a> von HDRO den Client herunterladen und einen Account erstellen.</p> '
featured_image: /wp-content/uploads/2009/11/lotro_small.jpg

---
Der neue <a href="http://www.lotro.com/" target="_blank">Herr der Ringe Online</a>-Shop ist wurde gleichzeitig mit dem Spiel freigeschaltet. Nun könnt ihr Erweiterungen, Quest-Pakete, Gegenstände oder das VIP-Paket erwerben, das euch unbegrenzten Zugang zum Spiel ermöglicht. Der europäische Publisher **Codemasters** verkündete vor kurzem eine Verschiebung des europäischen Starts des Spiels. Wann also die europäische Server Online gehen ist noch unklar.

 

   

* * *

   

