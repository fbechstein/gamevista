---
title: Xbox Live – Neues Dashboard-Update verfügbar
author: gamevista
type: news
date: 2010-11-01T20:07:06+00:00
excerpt: '<p>[caption id="attachment_530" align="alignright" width=""]<img class="caption alignright size-full wp-image-530" src="http://www.gamevista.de/wp-content/uploads/2009/08/xbox360.jpg" border="0" alt="Xbox 360" title="Xbox 360" align="right" width="140" height="95" />Xbox 360[/caption]Publisher und Entwickler <strong>Microsoft </strong>hat heute das neue Update für das <a href="http://www.xbox.com" target="_blank">Xbox Live Dashboard</a> veröffentlicht. Bisherigen Berichten zu folge sollte das Update erst in drei Tagen erscheinen. Der Patch für die Spielekonsole<strong> Xbox 360 </strong>wird automatisch bei Start heruntergeladen und installiert.</p> '
featured_image: /wp-content/uploads/2009/08/xbox360.jpg

---
Neben diversen Verbesserungen werden auch neue Funktionen hinzugefügt. Als wichtigste Neuerung gilt der Kinect-Bewegungssteuerung Support. So kann man zum Beispiel per Kinect mit der Hand durch die Menüs navigieren. Außerdem werden die Sprachqualität der Partychats mit einem neuen Audio-Codec verbessert.

 

   

* * *

   

