---
title: Bioshock 2 – Veröfflichungstermin bekannt
author: gamevista
type: news
date: 2009-09-18T19:32:49+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Publisher <a href="http://www.2kgames.de">2K Games</a> hat endlich einen Release Termin für den lang erwarteten Nachfolger von <strong>Bioshock </strong>bekannt gegeben. Demnach soll der zweite Teil von <strong>Bioshock </strong>vom Studio 2K Marin am 9. Februar 2010 in den Handel kommen. Allerdings gibt es da einen Haken. Der Termin ist bisher nur für die US Version bestätigt.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Termine für Europa oder Deutschland lassen noch auf sich warten. Allerdings dürften diese nicht sehr weit davon abweichen.

 

 

* * *



* * *

 