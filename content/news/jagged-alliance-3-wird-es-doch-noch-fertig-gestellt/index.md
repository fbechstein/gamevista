---
title: Jagged Alliance 3 – Wird es doch noch fertig gestellt?
author: gamevista
type: news
date: 2010-03-09T17:17:07+00:00
excerpt: '<p>[caption id="attachment_1535" align="alignright" width=""]<img class="caption alignright size-full wp-image-1535" src="http://www.gamevista.de/wp-content/uploads/2010/03/jaggedalliance3.jpg" border="0" alt="Jagged Alliance 3" title="Jagged Alliance 3" align="right" width="140" height="100" />Jagged Alliance 3[/caption]Die Fans des Strategietitels <strong>Jagged Alliance 3</strong> hatten im Dezember letzten Jares eine schwere Zeit, denn der Publisher <strong>Strategy First</strong> stellt die Arbeiten an dem doch sehr viel versprechenden Projekt ein. Nun hat der deutsche Publisher <strong>bitComposer Games</strong> im Rahmen einer Pressemitteilung verlauten lassen, dass die Entwicklung an dem rundenbasierten Taktikspiel weiter gehen.</p> '
featured_image: /wp-content/uploads/2010/03/jaggedalliance3.jpg

---
Bisher gibt es noch keine genauen Angaben wann der Release des Spiels ansteht, allerdings wird es das Ziel sein **Jagged Alliance 3** noch dieses Jahr fertig zu stellen. &#8222;Obwohl die Veröffentlichung von **Jagged Alliance 2** schon über zehn Jahre zurückliegt, gilt das Spiel nach wie vor als Meilenstein des Genres und erfreut sich einer großen Fangemeinschaft. Wir sind sehr glücklich darüber, dass wir uns diesen Klassiker für bitComposer sichern konnten und möchten an die Erfolge der Vorgänger anknüpfen, indem wir dem neuen Teil ein modernes Gewand verleihen, dabei aber die alten Stärken nicht unberücksichtigt lassen&#8220;, so Wolfang Dur, Managing Director von **bitComposer Games**.

 

<cite></cite>

   

* * *

   



 