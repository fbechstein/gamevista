---
title: Eidos Interactive / Square Enix – Lineup für die Gamescom
author: gamevista
type: news
date: 2009-07-28T17:52:06+00:00
excerpt: '<p>[caption id="attachment_45" align="alignright" width=""]<img class="caption alignright size-full wp-image-45" src="http://www.gamevista.de/wp-content/uploads/2009/07/batman_small.png" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Batman: Arkham Asylum[/caption]<a href="http://www.eidos.de" target="_blank" title="eidos">Eidos Interactive</a> und <a href="http://www.square-enix.com" target="_blank" title="square enix">Square Enix</a> haben heute ihr Lineup für die <strong>gamescom</strong> in Köln vorgestellt. Vom Action Prügelspiel <strong>Batman: Arkham Asylum</strong> bis hin zum Stragie und Onlinespielehit <strong>Supreme Commander 2</strong> bzw. <strong>Order of War </strong>ist einiges vertreten. </p>'
featured_image: /wp-content/uploads/2009/07/batman_small.png

---
Wir haben für euch eine Liste was ihr am Stand B54 in Halle 6 vorfinden werdet. Was es sonst noch so zu sehen gibt erfahrt ihr in unserem gamsecom [Special hier][1] zu finden.  
Außerdem werden wir vor Ort Live berichten. 

**<span>Batman: Arkham Asylum<br /> Final Fantasy 14</span><span><br />Front </span><span>Mission</span> Evolved  
Mini Ninjas<span><br />Order of War </span>  
Pony Friends 2**<span><strong><br />Supreme Commander 2<br /></strong> <br /></span>[][2] 







 [1]: hot-spots/gamescom-2009 "gamescom special"
 [2]: videos/item/root/risen-nightwish-trailer