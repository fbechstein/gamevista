---
title: Commandos 4 – Dementi seitens Entwickler
author: gamevista
type: news
date: 2009-08-25T20:59:02+00:00
excerpt: '<p>[caption id="attachment_478" align="alignright" width=""]<img class="caption alignright size-full wp-image-478" src="http://www.gamevista.de/wp-content/uploads/2009/08/commandos2_bi.jpg" border="0" alt="Commandos" title="Commandos" align="right" width="140" height="100" />Commandos[/caption]Erst gestern berichteten wir über eine Wiederbelebung der<strong> Commandos</strong> Serie des Entwicklers <a href="http://www.pyrostudios.com">Pyro Studios</a>. Nun kommt prompt der Widerruf von den <strong>Commandos</strong> Machern. Denn José Luis del Carpio, PR und Marketing Mann bei Pyro Studios, erklärte das die Ursprungsnews die durch die Presse gegangen war komplett Falsch ist.</p> '
featured_image: /wp-content/uploads/2009/08/commandos2_bi.jpg

---
Diese wurde per Webiste von Gamer.NL in die Medienwelt gedichtet, leider ohne Wahrheitsgehalt. [Pyro Studios][1] sagte weiterhin das derzeit keinerlei Pläne bestehen die **Commandos** Reihe zu reanimieren.

 

* * *



* * *

 

 [1]: http://www.pyrostudios.com