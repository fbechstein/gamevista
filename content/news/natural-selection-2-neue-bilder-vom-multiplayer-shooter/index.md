---
title: Natural Selection 2 – Neue Bilder vom Multiplayer-Shooter
author: gamevista
type: news
date: 2009-12-20T17:22:10+00:00
excerpt: '<p>[caption id="attachment_1219" align="alignright" width=""]<img class="caption alignright size-full wp-image-1219" src="http://www.gamevista.de/wp-content/uploads/2009/12/ns2_marine_hallway_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Die Community rund um das in der Entwicklung steckende <strong>Natural Selection 2</strong>, wurde vom Entwickler eingeladen ihnen Bilder von selbst gebastelten Karten zu schicken. Diese wurden mit dem im Alpha Stadium steckenden Level Editor erstellt. Sinn und Zweck war es der Öffentlichkeit zu zeigen wie prächtig das Leveldesign werden wird. Nun wurden die besten ausgewählt und auf der <a href="http://www.unknownworlds.com/ns2/news/2009/12/community_showcase" target="_blank">offiziellen Webseite</a> des Entwicklers präsentiert.</p> '
featured_image: /wp-content/uploads/2009/12/ns2_marine_hallway_small.jpg

---
Da wir euch die Bilder natürlich nicht vorenthalten wollten findet ihr ab sofort neue Screenshots zu **Natural Selection 2** in unserer Galerie.

[> Zur Natural Selection 2 &#8211; Galerie][1] 

<p style="text-align: center;">
  <img class="caption size-full wp-image-1220" src="http://www.gamevista.de/wp-content/uploads/2009/12/spark_shakewell_02_600x.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" width="600" height="329" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/spark_shakewell_02_600x.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/spark_shakewell_02_600x-300x165.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

 

* * *



* * *

 

 

 [1]: screenshots/item/root/natural-selection-2