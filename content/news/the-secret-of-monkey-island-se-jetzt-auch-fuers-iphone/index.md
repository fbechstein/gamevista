---
title: The Secret of Monkey Island SE jetzt auch fürs iPhone
author: gamevista
type: news
date: 2009-07-23T11:04:38+00:00
excerpt: '<p>[caption id="attachment_130" align="alignright" width=""]<img class="caption alignright size-full wp-image-130" src="http://www.gamevista.de/wp-content/uploads/2009/07/remakegame_small.png" border="0" alt="The Secret of Monkey Island SE ab sofort auch auf dem iPhone" title="The Secret of Monkey Island SE ab sofort auch auf dem iPhone" align="right" width="140" height="100" />The Secret of Monkey Island SE ab sofort auch auf dem iPhone[/caption]Es war leider nicht sicher, sondern nur ein Gerücht - eine Version von <strong>The Secret of Monkey Island SE</strong> für Apples iPhone. Nun aber ist es Wirklichkeit und Ihr könnt die Abenteuer von Guybrush Threepwood auf dem iPhone erleben. </p>'
featured_image: /wp-content/uploads/2009/07/remakegame_small.png

---
Das 351 MB grosse Abenteuer ist ab sofort für 5,99 EUR im App Store erhältlich.

&nbsp;







&nbsp;