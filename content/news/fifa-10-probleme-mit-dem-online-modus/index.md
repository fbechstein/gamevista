---
title: Fifa 10 – Probleme mit dem Online Modus
author: gamevista
type: news
date: 2009-10-11T18:41:51+00:00
excerpt: '<p>[caption id="attachment_115" align="alignright" width=""]<img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" />Fifa 10[/caption]<strong>Fifa 10</strong> für den PC ist wie immer hinten dran im Gegensatz zu den Konsolenversionen. So berichten viele Spieler über Probleme mit dem Online Modus, denn einige Fifa 10 PC Fans können keine Onlinepartien starten.</p> '
featured_image: /wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg

---
Sobald das Spiel beginnt findet man sich ganz schnell auf dem Desktop mit einer Fehlermeldung wieder. Dies ist dem Entwickler <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a> auch bekannt und so bestätigten sie im <a href="http://forumfifa.ea.com/t/6244.aspx" target="_blank">offiziellen Forum</a> das sie bereits an einem Hotfix Arbeiten. Wer trotzdem mit einem Freund zusammen spielen möchte sollte den LAN Direct IP Modus ausprobieren. Dieser funktioniert immerhin.

“ Die Entwickler in Kanada wissen Bescheid und der Fehler wird untersucht und schnellstmöglich behoben. Wir melden uns an dieser Stelle wieder, sobald es aktuelle Informationen gibt“, so <a href="http://www.electronic-arts.de" target="_blank">EA</a> Sprecher Heiko Ellinger im **Fifa 10** Forum.

 

* * *



* * *

 