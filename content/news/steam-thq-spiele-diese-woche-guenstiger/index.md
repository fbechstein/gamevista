---
title: Steam – THQ Spiele diese Woche günstiger
author: gamevista
type: news
date: 2009-10-13T15:07:57+00:00
excerpt: '<p>[caption id="attachment_860" align="alignright" width=""]<img class="caption alignright size-full wp-image-860" src="http://www.gamevista.de/wp-content/uploads/2009/10/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]Wie auf der Onlineplattform <a href="http://store.steampowered.com/publisher/THQ/" target="_blank">Steam</a> zu sehen ist wird in der Woche vom 12. bis 18. Oktober eine Vielzahl von Spielen des Publishers <a href="http://www.thq-games.com" target="_blank">THQ Entertainment</a> günstiger angeboten. Jeden Tag wird es neue Angebote geben. Heute fällt darunter das Actionrollenspiel <strong>Titan Quest</strong> sowie das dazugehörige Addon <strong>Titan Quest: Immortal Throne</strong>.</p> '
featured_image: /wp-content/uploads/2009/10/1_1280x800_small.jpg

---
Diese sind um ganze 50 Prozent reduziert worden.    
An jedem weiteren Wochentag werden neue <a href="http://www.thq-games.com" target="_blank">THQ</a> Spiele reduziert angeboten. Zwar gibt es noch keine Hinweise welche Spiele in den nächsten Tagen folgen werden, aber aus dem **THQ Collector Pack** kann man schon absehen das auch die Strategiespiele **Company of Heroes** und **Dawn of War 2** günstiger werden.

 

* * *



* * *

 