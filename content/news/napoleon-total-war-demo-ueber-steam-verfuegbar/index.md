---
title: 'Napoleon: Total War – Demo über Steam verfügbar'
author: gamevista
type: news
date: 2010-03-12T21:20:17+00:00
excerpt: '<p>[caption id="attachment_431" align="alignright" width=""]<img class="caption alignright size-full wp-image-431" src="http://www.gamevista.de/wp-content/uploads/2009/08/napoleon_totalwar.jpg" border="0" alt="Napoleon: Total War" title="Napoleon: Total War" align="right" width="140" height="100" />Napoleon: Total War[/caption]Der Publisher <strong>SEGA </strong>hat die Demoversion des Strategietitels <a href="http://www.totalwar.com" target="_blank">Napoleon: Total War,</a> auf der Online-Plattform Steam, zum kostenlosen Download bereitgestellt. Die <a href="http://store.steampowered.com/app/34050/" target="_blank">Demo</a> umfasst 2 Gigabyte und muss zuerst über Steam herunter geladen werden, ein Steam Account ist damit also Pflicht. Inhalt der Testversion ist die Schlacht von Ligny, die ihr nachspielen könnt.</p> '
featured_image: /wp-content/uploads/2009/08/napoleon_totalwar.jpg

---
Außerdem gibt es ein Tutorial und dazu ein Kampagnen- und Mehrspielervideo.

<a href="http://store.steampowered.com/app/34050/" target="_blank">> Zum Napoleon: Total War &#8211; Demo Steam Download</a>

 

<cite></cite>

   

* * *

   



 