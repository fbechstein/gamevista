---
title: Die Siedler 7 – Zum Betatest anmelden
author: gamevista
type: news
date: 2009-12-23T17:17:00+00:00
excerpt: '<p>[caption id="attachment_740" align="alignright" width=""]<img class="caption alignright size-full wp-image-740" src="http://www.gamevista.de/wp-content/uploads/2009/09/diesiedler7.jpg" border="0" alt="Die Siedler 7" title="Die Siedler 7" align="right" width="140" height="100" />Die Siedler 7[/caption]Der Publisher <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Ubisoft</a> hat zusammen mit dem Entwickler Blue Byte, die in Kürze startende Beta zum Strategie- und Aufbauspiel <strong>Die Siedler 7</strong> angekündigt. So werden Siedler Fans den nächsten Teil ab Mitte Januar 2010 anspielen können. Blue Byte stellt neben einer Tutorialmission auch den Multiplayer-Part zur Verfügung.</p> '
featured_image: /wp-content/uploads/2009/09/diesiedler7.jpg

---
Wie immer kann man natürlich sein Feedback in einem geschlossenen Forum abgeben und mit Fans und Entwicklern diskutieren. Damit ihr bald auch einen Betakey euer Eigen nennen könnt müsst ihr lediglich 16 Jahre alt sein und euch auf <a href="http://www.ubi.com" target="_blank">Ubi.com</a> anmelden. Das genauere Prozedere findet ihr <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">hier</a>.

 

<cite></cite>

* * *



* * *

 