---
title: 'Dragon Age: Origins – Erreicht Goldstatus'
author: gamevista
type: news
date: 2009-10-25T18:14:25+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Der Publisher <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a> hat per offizieller Pressemitteilung bestätigt das ihr neuster Titel <strong>Dragon Age: Origins</strong> nun endlich den Goldstatus erreicht hat. So dürfte der Veröffentlichung des Rollenspiels aus dem Haus BioWare am 5. November 2009 nichts mehr im Wege stehen.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
**Dragon Age: Origins** erscheint für PC, Xbox 360 und PlayStation 3.

 

 

* * *



* * *

 