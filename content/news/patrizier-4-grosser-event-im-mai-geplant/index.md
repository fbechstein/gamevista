---
title: Patrizier 4 – Großer Event im Mai geplant
author: gamevista
type: news
date: 2010-05-03T16:29:04+00:00
excerpt: '<p>[caption id="attachment_1615" align="alignright" width=""]<img class="caption alignright size-full wp-image-1615" src="http://www.gamevista.de/wp-content/uploads/2010/03/patrizier4.jpg" border="0" alt="Patrizier 4" title="Patrizier 4" align="right" width="140" height="100" />Patrizier 4[/caption]Der Publisher <strong>Kalypso Media</strong> bietet den Fans der Wirtschaftsimulation <a href="http://www.patrizier4.de/event/" target="_blank">Patrizier 4</a> die Möglichkeit das Spiel noch vor der Veröffentlichung anzuspielen.  <a href="http://www.patrizier4.de/event/" target="_blank">Patrizier 4</a> wird im 3. Quartal 2010 im Handel erscheinen. Doch bereits am 21. Mai 2010 wird im Rahmen eines großen Events in der Stadt Lübeck, eine Demo zur Verfügung gestellt.</p> '
featured_image: /wp-content/uploads/2010/03/patrizier4.jpg

---
Alles was ihr dafür tun müsst, ist euch auf der offiziellen Webseite zur registrieren. Aus allen Bewerbern werden 20 Gewinner ausgewählt, die dann an Bord des Schiffes „Lisa von Lübeck“, die Entwickler von **Gaming Minds** treffen und <a href="http://www.patrizier4.de/event/" target="_blank">Patrizier 4</a> antesten dürfen. Auf hoher See wird natürlich auch für Proviant und Getränke gesorgt. Zur Event Registration geht es <a href="http://www.patrizier4.de/event/" target="_blank">hier</a>.

 

   

* * *

   

