---
title: 'Killzone 2: Napalm & Cordite – Neue Waffen und Maps'
author: gamevista
type: news
date: 2009-07-23T17:46:27+00:00
excerpt: '<p>[caption id="attachment_138" align="alignright" width=""]<img class="caption alignright size-full wp-image-138" src="http://www.gamevista.de/wp-content/uploads/2009/07/wallpaper01_1920x1080_small.jpg" border="0" alt="Killzone 2" title="Killzone 2" align="right" width="140" height="100" />Killzone 2[/caption]<a href="http://de.playstation.com/" target="_blank" title="Sony">Sony</a> und <a href="http://www.guerrilla-games.com/" target="_blank" title="Killzone 2">Guerilla </a>werden im Laufe des heutigen Tages den dritten Download Content mit dem Namen <strong>Napalm </strong><strong>& Cordite</strong><strong> </strong>releasen. Inhalt des Pakets werden unter anderem zwei neue Multiplayermaps und zwei neue Waffen, Flammenwerfer und Boltgun, sein.</p> '
featured_image: /wp-content/uploads/2009/07/wallpaper01_1920x1080_small.jpg

---
<p style="margin-bottom: 0cm; background: none transparent scroll repeat 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
  <span class="caption"><strong><span style="color: #000000;"><span style="font-style: normal; font-weight: normal">Das ganze wird 5,99 Euro kosten und ist im PSN-Store verfügbar. Falls ihr die anderen beiden DLCs, „</span></span></strong></span><strong>Steel & Titanium</strong><span class="caption"><strong><span style="color: #000000;"><span style="font-style: normal; font-weight: normal">“ und </span></span></strong><strong><span style="color: #000000;"><span style="font-style: normal; font-weight: normal">&#8222;<strong>Flash & Thunder</strong>&#8222;</span></span></strong><strong><span style="color: #000000;"><span style="font-style: normal; font-weight: normal">, noch nicht haben solltet, bekommt ihr für 11,99 Euro ein Mappack was alle neuen Multiplayer Maps beinhaltet. <br /></span></span></strong></span><span class="caption"><strong><span style="color: #000000;"><span style="font-style: normal; font-weight: normal"><br /></span></span></strong></span>
</p>







<p style="margin-bottom: 0cm; background: none transparent scroll repeat 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
   
</p>