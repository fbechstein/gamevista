---
title: Natural Selection 2 – Alpha Level Editor und neue Screenhots
author: gamevista
type: news
date: 2009-11-24T16:01:09+00:00
excerpt: '<p>[caption id="attachment_1095" align="alignright" width=""]<img class="caption alignright size-full wp-image-1095" src="http://www.gamevista.de/wp-content/uploads/2009/11/gorge_render_01_800x597_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Der Entwickler <a href="http://www.naturalselection2.com" target="_blank">Unknown Worlds</a> hat am heutigen Tage den Level Editor für den Shooter-Strategie-Mix <strong>Natural Selection 2</strong> veröffentlicht. Zwar ist dieser noch im Alpha-Stadium. Dies soll aber Fans und Mapper nicht davon abhalten schon mal ihre ersten Gehversuche im neuen Level Editor zu machen.</p> '
featured_image: /wp-content/uploads/2009/11/gorge_render_01_800x597_small.jpg

---
Einen Haken hat die Sache aber, denn den Alpha Level Editor können nur Vorbesteller von **Natural Selection 2** nutzen. Trotzdem liefert <a href="http://www.naturalselection2.com" target="_blank">Unknown Worlds</a> so neue Screenhots und Bildmaterial. Weiterhin gibt es zum Editor zwei Tutorial Videos.

[> Zum Natural Selection 2 &#8211; Level Editor Download  
][1]   
[> Zu den Natural Selection 2 &#8211; Screenshots][2]

[> Zu den Natural Selection 2 &#8211; Level Editor Tutorial Videos][3]

 







 

 [1]: downloads/demos/item/demos/natural-selection-2-alpha-level-editor
 [2]: screenshots/item/root/natural-selection-2
 [3]: http://www.naturalselection2.com/tutorials