---
title: 'Aion – The Tower of Eternity – Beta Event #4'
author: gamevista
type: news
date: 2009-07-17T19:19:26+00:00
excerpt: '<p>[caption id="attachment_74" align="alignright" width=""]<img class="caption alignright size-full wp-image-74" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion - Beta Event #4 beginnt heute um 21.00 Uhr" title="Aion - Beta Event #4 beginnt heute um 21.00 Uhr" align="right" width="140" height="100" />Aion - Beta Event #4 beginnt heute um 21.00 Uhr[/caption]In einer Stunde startet der vierte Beta Event zu <a href="http://www.ncsoft.com" target="_blank">NCSofts </a><strong>Aion - The Tower of Eternity</strong>. Folgende Schwerpunkte werden bei dem Event behandelt:</p>'
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
**Schwerpunkt:** Elyos und Asmodier Stufen 1-25, inklusive Zugang zum Abyss. 

  * Verbesserung der Serverstabilität 
  * Verschiedene Korrekturen am Benutzerinterface 
  * Behebung einiger „NPC html load failure“ Meldungen 
  * Grammatikunterricht für einige unserer NPC’s 
  * Grammatikalische Änderungen bei einigen Glossardefintionen 
  * Neue Dialoge und Questtexte für NPC’s in Sanctum und Pandaemonium 
  * Aktualisierung von GameGuard 

**Beginn des Events:** 17. Juli 21:00 deutscher Zeit   
**Ende des Events:** 20. Juli 21:00 deutscher Zeit 

Ab nächster Woche werden wir **20x Aion Beta Keys** für die nächsten Events **verlosen**. Schaut einfach auf gamevista.de vorbei.





