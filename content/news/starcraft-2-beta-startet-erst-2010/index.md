---
title: Starcraft 2 – Beta startet erst 2010
author: gamevista
type: news
date: 2009-11-10T22:41:37+00:00
excerpt: '<p>[caption id="attachment_110" align="alignright" width=""]<img class="caption alignright size-full wp-image-110" src="http://www.gamevista.de/wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="105" />Starcraft 2[/caption]Der Entwickler <a href="http://www.starcraft2.com" target="_blank" rel="nofollow">Blizzard Entertainment</a> hatte bereits im Juni den Fans des Strategiespiels <strong>Starcraft 2</strong> Hoffnung auf eine baldige Beta gemacht. Nun hat der Lead Producer Chris Sigaty im Rahmen eines Interviews auf der russischen Spielemesse IgroMir verkündet das die Beta für den kommenden Strategiehit erst im Jahr 2010 an den Start gehen wird.</p> '
featured_image: /wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg

---
Einen genauen Termin wollte der <a href="http://www.starcraft2.com" target="_blank" rel="nofollow">Blizzard</a> Mitarbeiter nicht abgeben. Eine gute Seite hat das Ganze dann doch. Dem Release im 2. Quartal 2010 für PC und Mac soll nichts im Wege stehen.

 

<cite></cite>

* * *



* * *