---
title: Dead Rising 2 – Neuer DLC auch für PC
author: gamevista
type: news
date: 2010-10-07T20:31:43+00:00
excerpt: '<p><strong>[caption id="attachment_1442" align="alignright" width=""]<img class="caption alignright size-full wp-image-1442" src="http://www.gamevista.de/wp-content/uploads/2010/02/deadrising2.jpg" border="0" alt="Dead Rising 2" title="Dead Rising 2" align="right" width="140" height="100" />Dead Rising 2[/caption]Capcom </strong>hat bestätigt das die neusten Download-Contents für das Actionspiel <a href="http://deadrising-2.com" target="_blank">Dead Rising 2</a> auch für PC erscheinen werden. Bisher gibt es aber noch keinen genauen Termin. Der Packs werden damit sowohl für PC als auch für Xbox 360 und PlayStation 3 erscheinen.</p> '
featured_image: /wp-content/uploads/2010/02/deadrising2.jpg

---
Als Inhalt zählen neue Outfits und die dazu passenden Waffen. Der Psychopathen-DLC erscheint am 12. Oktober und der Soldier of Fortune-DLC erscheint am 19. Oktober 2010. Außerdem werden das Sports Fun Pack und das Ninja Theme erscheinen jeweils am 26. Oktober bzw. am 2. November für PlayStation 3 und Xbox 360. <a href="http://deadrising-2.com/" target="_blank">Dead Rising 2</a> ist ab sofort erhältlich.

 

   

* * *

   



 