---
title: Mafia 2 – Release Termin bekannt
author: gamevista
type: news
date: 2010-03-25T21:16:24+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]Nachdem der Publisher <strong>2K Games</strong> den Veröffentlichungstermin des Actionspiels <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> diverse Male verschoben hat, wurde heute ein genaues Datum zum Release benannt. Demnach wird <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> am 27. August 2010 für Xbox 360, PlayStation 3 und PC in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
</p> 

<cite></cite>

   

* * *

   



 