---
title: Darkest of Days Demo erschienen
author: gamevista
type: news
date: 2009-08-12T10:58:02+00:00
excerpt: '<p>[caption id="attachment_338" align="alignright" width=""]<img class="caption alignright size-full wp-image-338" src="http://www.gamevista.de/wp-content/uploads/2009/08/darkestofdays_small.png" border="0" alt="Darkest of Days" title="Darkest of Days" align="right" width="140" height="100" />Darkest of Days[/caption]Ab sofort könnt Ihr die Demo zum Shooter <strong>Darkest of Days</strong> in unserer Downloadsektion herunterladen. Bei dem Shooter habt Ihr die Möglichkeit in fünf Episoden die schrecklichsten Kriege der Menscheit nachzuspielen. Dabei führt Euch die Zeitreise über Pompei bis zu den beiden Weltkriegen.</p>'
featured_image: /wp-content/uploads/2009/08/darkestofdays_small.png

---
Die Demo ist 741 MB gross und kann unter folgendem Link heruntergeladen werden: 

[> zum Download von Darkest of Days Demo][1]







 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=169