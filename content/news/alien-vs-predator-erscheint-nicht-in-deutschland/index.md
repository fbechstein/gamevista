---
title: Alien vs. Predator – Erscheint nicht in Deutschland
author: gamevista
type: news
date: 2009-11-09T17:40:12+00:00
excerpt: '<p>[caption id="attachment_554" align="alignright" width=""]<img class="caption alignright size-full wp-image-554" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Alien vs. Predator" title="Alien vs. Predator" align="right" width="140" height="100" />Alien vs. Predator[/caption]Der Ego-Shooter<strong> Alien vs. Predator </strong>aus dem Hause <a href="http://www.sega.de/games/?g=6058" target="_blank">Sega</a> wird in Deutschland nicht in den Handel kommen. Sicher möchte nun jeder Fan des Shooters wieder gegen die Unterhaltungssoftware Selbstkontrolle wettern. Diese trifft aber diesmal keine Schuld. So hat <a href="http://www.sega.de/games/?g=6058" target="_blank">Sega Europe</a> im Rahmen einer Pressemitteilung bestätigt das sie <strong>Alien vs. Predator</strong> nicht für den deutschen Markt entwickeln werden.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Der Grund dafür liefert der Publisher nach eigenen Worten hiermit: _&#8222;Aliens vs. Predator wird in Deutschland nicht veröffentlicht, weil zu erwarten ist, dass der Titel keine Kennzeichnung durch die USK erhalten würde. SEGA Europe Ltd. hat gleichzeitig entschieden, dass das Spiel für Deutschland nicht mit größeren Änderungen erscheinen wird, da das Gameplay-Erlebnis beeinträchtigt würde. SEGA bietet in Deutschland keine Spiele ohne USK-Kennzeichnung an._&#8220; So ist es wie immer notwendig, für volljährige Spieler, auf eine Importversion aus dem Ausland zurückgreifen zu müssen.

 

<cite></cite>

* * *



* * *