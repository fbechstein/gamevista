---
title: 'Star Wars: The Force Unleashed 2 – Veröffentlichung im Oktober'
author: gamevista
type: news
date: 2010-06-06T09:46:19+00:00
excerpt: '<p>[caption id="attachment_1192" align="alignright" width=""]<img class="caption alignright size-full wp-image-1192" src="http://www.gamevista.de/wp-content/uploads/2009/12/tfu2.jpg" border="0" alt="Star Wars: The Force Unleashed 2" title="Star Wars: The Force Unleashed 2" align="right" width="140" height="100" />Star Wars: The Force Unleashed 2[/caption]Publisher <strong>LucasArts </strong>hat den Release-Termin für das Actionspiel <a href="http://www.lucasarts.com/games/theforceunleashed2/ " target="_blank">Star Wars: The Force Unleashed 2</a> offiziell bestätigt. Demnach wird der zweite Teil am 26. Oktober 2010 in den Handel kommen. Es wird keine gesonderten Veröffentlichungstermine für die Systeme Xbox 360, PC und PlayStation 3 wie beim ersten Teil geben.</p> '
featured_image: /wp-content/uploads/2009/12/tfu2.jpg

---
Angepeilt ist der 26. Oktober für Konsole, sowie für PC. Mit <a href="http://www.lucasarts.com/games/theforceunleashed2/ " target="_blank">Star Wars: The Force Unleashed 2</a> wird die epische Story fortgesetzt. Spieler schlüpfen ein weiteres Mal in die Rolle des vernichtend mächtigen Starkillers – Darth Vader’s heimlichen Schüler – in einem bahnbrechenden Abenteuer, das in der bisher kaum beleuchteten Ära zwischen Star Wars: Episode III – Die Rache der Sith und Star Wars: Episode IV – Eine neue Hoffnung angesiedelt ist.

 

   

* * *

   

