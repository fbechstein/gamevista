---
title: Tom Clancy’s Endwar 2 – In Entwicklung!
author: gamevista
type: news
date: 2009-07-23T14:06:18+00:00
excerpt: |
  [caption id="attachment_134" align="alignright" width=""]<img class="caption alignright size-full wp-image-134" src="http://www.gamevista.de/wp-content/uploads/2009/07/endwar_logo_small.jpg" border="0" alt="Tom Clancy's Endwar 2" title="Tom Clancy's Endwar 2" align="right" width="140" height="105" />Tom Clancy's Endwar 2[/caption] <strong><font color="#000000"><span style="font-style: normal"><span style="font-weight: normal"><a href="http://endwargame.us.ubi.com/" target="_blank" title="Ubisoft">Ubisoft</a> Shangai basteln momentan an <strong>Tom Clancy's Endwar 2</strong>. </span></span></font></strong>Das Team soll aber recht klein ausfallen da vieles vom ersten Teil übernommen wird. Hauptaugenmerk liegt laut Michael De Plater, <a href="http://endwargame.us.ubi.com/" target="_blank" title="Ubisoft">Ubisoft</a> Creative Director, auf der Entwicklung der Story um eine höhere Spieltiefe zu erzeugen. <br /><br />
featured_image: /wp-content/uploads/2009/07/endwar_logo_small.jpg

---
Der Singleplayermodus wird demnach wesentlich Spannender und Unterhaltsamer gestaltet. Plater sagt weiterhin, die Unterschiede zwischen den einzelnen Fraktionen werden wesentlich Umfangreicher ausfallen, um das Stein-Schere-Papier Prinzip in die Balance des Spiels besser zu integrieren. Einen Veröffentlichungstermin nannte er leider noch nicht. 

<p style="margin-bottom: 0cm; background: none transparent scroll repeat 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
  <strong><font color="#000000"><span style="font-style: normal"><span style="font-weight: normal">Ob und wie die Sprachbefehlssteuerung überarbeitet wird konnte nicht beantwortet werden, vermutet wird aber das mehr Befehle vorhanden sein werden als im Vorgänger.</span></span></font></strong>
</p>

<p style="margin-bottom: 0cm; background: none transparent scroll repeat 0% 0%; -moz-background-clip: border; -moz-background-origin: padding; -moz-background-inline-policy: continuous">
  <strong><font color="#000000"><span style="font-style: normal"></span></font></strong>
</p>





