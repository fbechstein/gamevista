---
title: 'James Cameron’s Avatar: Das Spiel – Demo zum Download bereit'
author: gamevista
type: news
date: 2009-11-30T17:35:42+00:00
excerpt: |
  |
    <p>[caption id="attachment_1047" align="alignright" width=""]<img class="caption alignright size-full wp-image-1047" src="http://www.gamevista.de/wp-content/uploads/2009/11/avatar.jpg" border="0" alt="Avatar: Das Spiel" title="Avatar: Das Spiel" align="right" width="140" height="100" />Avatar: Das Spiel[/caption]Der Publisher <a href="http://www.avatargame.de.ubi.com" target="_blank">Ubisoft</a> hat die Demo zum Actionspiel <strong>James Cameron's Avatar: Das Spiel</strong> veröffentlicht. Demnach können sie nun die Spielumsetzung zum Kimofilm antesten. In der bunten 1,6 Gigabyte großen Demo können sie auf dem exotischen Planeten Pandora die Gegend erkunden. So stehen ihnen neben diversen Waffen auch ein Kampfläufer zur Verfügung. Die Demo beinhaltet sowohl die DirectX 9 und DirectX 10 Version.</p>
featured_image: /wp-content/uploads/2009/11/avatar.jpg

---
[> Zum James Cameron&#8217;s Avatar: Das Spiel &#8211; Demo Download][1]

 







 

 [1]: downloads/demos/item/demos/avatar-das-spiel-demo