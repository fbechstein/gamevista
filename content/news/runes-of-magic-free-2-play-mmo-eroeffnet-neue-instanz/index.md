---
title: Runes of Magic – Free 2 Play MMO eröffnet neue Instanz
author: gamevista
type: news
date: 2009-12-15T20:27:17+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Für das kostenlose Online-Rollenspiel <strong>Runes of Magic</strong> vom Publisher <a href="http://www.runesofmagic.com" target="_blank">Frogster Interactive</a>, wurde nun offiziell bestätigt das im laufe des morgigen Tages, eine neue Schlachtzugs-Instanz eröffnet wird. Die Instanz mit dem Namen die Zurhidon-Feste, bietet 12-Spieler Gruppen jede Menge taktisch anspruchsvolle Kämpfe.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Neben der Instanz werden im Rahmen des morgigen Patches auch neue Elite-Skills implementiert und das Schneeflockenfest gestartet. Wer weitere Informationen sucht kann sich auf der <a href="http://www.runesofmagic.com/de/zurhidon-patch.html" target="_blank">Übersichtsseite</a> des Publishers informieren.

Passend zum Patch 2.1.5. wurde nun auch ein Trailer veröffentlicht.

[> Zum Runes of Magic &#8211; Zurhidon Feste Trailer][1]

* * *



* * *

 

 [1]: videos/item/root/runes-of-magic-zurhidon-feste-trailer