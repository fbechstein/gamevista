---
title: Assassin’s Creed 2 – Informationen zur Kurzfilmreihe
author: gamevista
type: news
date: 2009-10-19T14:08:39+00:00
excerpt: |
  |
    <p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin's Creed 2" title="Assassin's Creed 2" align="right" width="140" height="100" />Assassin's Creed 2[/caption]Der Entwickler <a href="http://www.ubi.com" target="_blank">Ubisoft</a> hat heute weitere Informationen zu den drei geplanten Kurzfilmen des Actionspiels <strong>Assassin's Creed 2</strong> preisgegeben. Diese sollen bereits Ende Oktober veröffentlicht werden.</p>
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Die drei Kurzfilme mit dem Namen **Assassin&#8217;s Creed: Linage** sollen die Vorgeschichte zum zweiten Teil von **Assassin&#8217;s Creed** erzählen. So dreht sich alles um Ezios Vater, Giovanni Auditore da Firenze und dessen Leben. Die Videos werden in dem kürzlich aufgekauften Special Effects Studio Hybride Technologies gedreht. Diese sind unter anderem für die Effekte in den Kinofilmen 300 und Sin City verantwortlich. Um euch einen kleinen Vorgeschmack auf die kommenden Filme zu geben hat Ubisoft noch einen Teaser veröffentlicht.

 

[> Zum Assassin&#8217;s Creed 2: Lineage &#8211; Teaser][1]

* * *



* * *

 

 [1]: videos/item/root/assassins-creed-2-linage-teaser