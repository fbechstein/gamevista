---
title: 'Fallout: New Vegas – Drittes Entwicklertagebuch & Systemvoraussetzungen'
author: gamevista
type: news
date: 2010-10-03T08:09:30+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Die Entwickler der <strong>Obsidian Entertainment </strong>Studios haben ihr drittes Entwicklertagebuch für das Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> veröffentlicht. Thema des neuen Videos ist das Design des Spiels, wie die Umgebung des Fallout-Titels entwickelt wurde. Im Rahmen der Veröffentlichungen am 22. Oktober 2010 wurden heute neben der Goldmeldung, auch die Systemvoraussetzungen bekannt gegeben.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Leider gaben die Entwickler nur die Mindestanforderungen bekannt. Demnach wird minimal ein Dual-Core-Prozessor, sowie eine ATI 1300 XT oder NVIDIA GeForce 6 benötigt.

[> Zum Fallout: New Vegas – Entwicklertagebuch #3][1]

Die Mindestanforderungen für Fallout: New Vegas:

  * Betriebssystem: Windows 7 / Vista / XP
  * Prozessor: Dual Core 2.0 GHz
  * Arbeitsspeicher: 2 GB RAM
  * Festplatte: 10 GB
  * Grafikkarte: NVIDIA GeForce 6-Serie oder ATI 1300XT *

   

* * *

   



 [1]: videos/item/root/fallout-new-vegas--entwicklertagbuch-3