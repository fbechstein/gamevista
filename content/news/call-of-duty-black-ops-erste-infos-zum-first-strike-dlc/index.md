---
title: 'Call of Duty: Black Ops – Erste Infos zum First Strike DLC'
author: gamevista
type: news
date: 2011-01-17T18:35:49+00:00
excerpt: '<p>[caption id="attachment_1747" align="alignright" width=""]<img class="caption alignright size-full wp-image-1747" src="http://www.gamevista.de/wp-content/uploads/2010/05/callofduty7.jpg" border="0" alt="Call of Duty: Black Ops" title="Call of Duty: Black Ops" align="right" width="140" height="100" />Call of Duty: Black Ops[/caption]Entwickler <strong>Treyarch </strong>hat erste Informationen zum <strong>Black Ops</strong> Zusatzpacket <strong>First Strike</strong> veröffentlicht. Der erste DLC zum Ego-Shooter wird fünft neue Karten, Berlin, Stadium, Discovery, Kowloon enthalten. Zusätzlich wird eine weitere Zombie-Karte mit dem Namen Ascension für 1200 MS-Punkte veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/05/callofduty7.jpg

---
Der geplante Termin ist der 1. Februar 2011. Passend zum baldigen Release präsentierten die Entwickler auch ein neues Video was weitere Details zu den Karten, Tips und Tricks beinhaltet. Bisher ist nur eine Veröffentlichung für die Xbox 360 Konsole geplant.

[> Zum Call of Duty: Black Ops – Multiplayer Preview Trailer][1]

* * *



* * *

 [1]: videos/item/root/call-of-duty-black-ops-first-strike-trailer