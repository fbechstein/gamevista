---
title: Mafia 2 – Joe´s Adventures DLC veröffentlicht
author: gamevista
type: news
date: 2010-11-23T18:47:37+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]<strong>2K Games </strong>hat für das Actionspiel <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> den Zusatzinhalt mit dem Namen <strong>Joe`s Adventures</strong> veröffentlicht. Der Downloadable Content ist für 7,99 Euro via Steam zum Kauf verfügbar. In <strong>Joe`s Adventures</strong> übernehmen sie die Rolle von Joe Barbaro, Freund und Kumpel von Vito Scaletta, dem Protagonisten von <a href="http://www.mafia2game.com/" target="_blank">Mafia 2</a>.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Neben neuen Missionen dürfen sie sich über weitere Zusatzaufträge und Musik freuen. Außerdem werden neue Schauplätze, sowie Fahrzeuge und Kleidungsstücke verfügbar sein.

   

* * *

   

