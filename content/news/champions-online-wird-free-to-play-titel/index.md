---
title: Champions Online – Wird Free to Play Titel
author: gamevista
type: news
date: 2010-10-26T18:02:47+00:00
excerpt: '<p><strong>[caption id="attachment_122" align="alignright" width=""]<img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" />Champions Online[/caption]Cryptic </strong>hat offiziell bestätigt, dass das Onlinerollenspiel <a href="http://www.champions-online.com" target="_blank">Champions Online</a> demnächst auch als Free to Play-Variante verfügbar sein wird. Der Titel wird aber nach wie vor ein Abomodell unterstützen, als Gold-Mitglied müsst ihr so weiter monatliche Gebühren zahlen. Neu ist die Silver-Mitgliedschaft.</p> '
featured_image: /wp-content/uploads/2009/07/champions_online_small.jpg

---
Hierbei können Spieler ohne jegliche Kosten den Titel aber mit Einschränkungen spielen. Spieler mit dem Silver-Status werden im Bezug auf Ausrüstung, Bank und Marktplatzgröße begrenzt. Außerdem werdet ihr nur vorgefertigte Charaktere nutzen können. Die Entwickler werden Lifetime-Abonomenten automatisch in Gold-Mitglieder umwandeln.

 

   

* * *

   

