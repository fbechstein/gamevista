---
title: Aliens vs. Predator – Nutzt Steamworks und Steam-Cloud Technik
author: gamevista
type: news
date: 2009-08-21T19:30:41+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Im Rahmen der gamescom 2009 wurden nun weitere Details zum Shooter <strong>Aliens vs. Predator</strong> bekannt. Demnach soll das Spiel unter anderem Steamworks und Steam-Cloud Features nutzen. Neben den üblichen Steamworks Features wie zum Beispiel InGame Download Pakete oder Matchmaking Lobby wird auch die Steam Cloud zum Einsatz kommen.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Mit dieser Technik können Savegames auf den Valve Servern abgespeichert werden, siehe Left 4 Dead. Der [Sega][1] Titel wird voraussichtlich im ersten Quartal 2010 erscheinen

&#8218;

* * *



* * *

 

 

 [1]: http://www.sega.de