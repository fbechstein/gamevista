---
title: Dead Nation – Ab 1. Dezember per PSN verfügbar
author: gamevista
type: news
date: 2010-11-15T19:20:19+00:00
excerpt: '<p>[caption id="attachment_2431" align="alignright" width=""]<img class="caption alignright size-full wp-image-2431" src="http://www.gamevista.de/wp-content/uploads/2010/11/deadnation.jpg" border="0" alt="Dead Nation" title="Dead Nation" align="right" width="140" height="100" />Dead Nation[/caption]Publisher <strong>Sony </strong>hat für das Zombie-Actionspiel <a href="http://uk.playstation.com/psn/games/detail/item228392/Dead-Nation%E2%84%A2/" target="_blank">Dead Nation</a> den Veröffentlichungstermin bekannt gegeben. Ab dem 1. Dezember 2010 wird die Download-Version zum Preis von 12,99 Euro über den Playstation Network Store verfügbar sein.</p> '
featured_image: /wp-content/uploads/2010/11/deadnation.jpg

---
<a href="http://uk.playstation.com/psn/games/detail/item228392/Dead-Nation%E2%84%A2/" target="_blank">Dead Nation</a> schickt sie mit zwei Charakteren in eine mit einem Virus verseuchte Welt. Natürlich versuchen diese sich mit allen Mitteln gegen die Zombiehorde zu wehren.

 

 

   

* * *

   

