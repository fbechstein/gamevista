---
title: God of War 3 – Entwickler geben weitere Details bekannt
author: gamevista
type: news
date: 2009-10-21T13:44:43+00:00
excerpt: '<p>[caption id="attachment_919" align="alignright" width=""]<img class="caption alignright size-full wp-image-919" src="http://www.gamevista.de/wp-content/uploads/2009/10/gow.jpg" border="0" alt="God of War 3" title="God of War 3" align="right" width="140" height="100" />God of War 3[/caption]Wie schon bekannt ist wird <strong>God of War 3</strong> keinen Mehrspieler Modus beinhalten. Darauf ging nun Sonys Game Director Stig Asmussen näher ein. So sagte dieser in einem Interview das man den fehlenden Multiplayer Modus mit anderen Inhalten ausgleichen möchte.</p> '
featured_image: /wp-content/uploads/2009/10/gow.jpg

---
Demnach ist es denkbar das man wie bei **Batman: Arkham Asylum** auf sogenannte „Challange Rooms“ setzt. Diese sollen im Rahmen von downloadbaren Zusatzinhalten angeboten und erweitert werden. Ob die Zusatzinhalte Geld kosten werden ist bisher nicht bekannt.

 

 

* * *



* * *

 