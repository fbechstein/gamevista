---
title: Aion – Es ist Fertig!
author: gamevista
type: news
date: 2009-08-17T18:18:00+00:00
excerpt: '<p>[caption id="attachment_380" align="alignright" width=""]<img class="caption alignright size-full wp-image-380" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion_small.png" border="0" alt="Aion: Tower of Eternity" title="Aion: Tower of Eternity" align="right" width="140" height="100" />Aion: Tower of Eternity[/caption]Endlich! Im Rahmen einer offiziellen Pressemitteilung kündigte Publisher <a href="http://www.eu.aiononline.com/de/ " target="_blank">NCSoft </a>nun an das die Arbeiten an ihrer europäischen Version des Online Rollenspiels <strong>Aion: The Tower of Eternity</strong> beendet sind. Demnach sollte dem Release am 15. September nichts im Wege stehen.</p> '
featured_image: /wp-content/uploads/2009/08/aion_small.png

---
Als Einstimmung zum fantastischen MMO aus Asien könnt ihr euch den erst vor kurzem erschienen deutschsprachigen Trailer anschauen.

<a href="videos/item/root/aion-gamescom-trailer" target="_blank">> Zum Aion gamescom Trailer</a>

* * *



* * *

 