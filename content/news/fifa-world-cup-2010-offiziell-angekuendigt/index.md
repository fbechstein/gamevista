---
title: FIFA World Cup 2010 – Offiziell angekündigt
author: gamevista
type: news
date: 2010-01-27T18:09:49+00:00
excerpt: '<p>[caption id="attachment_1373" align="alignright" width=""]<img class="caption alignright size-full wp-image-1373" src="http://www.gamevista.de/wp-content/uploads/2010/01/fifawc.jpg" border="0" alt="FIFA World Cup 2010" title="FIFA World Cup 2010" align="right" width="140" height="100" />FIFA World Cup 2010[/caption]Der Publisher <strong>Electronic Arts </strong>hat im Rahmen einer Pressemitteilung das offizielle Spiel zur FIFA Fußball-Weltmeisterschaft in Südafrika angekündigt. Der Anpfiff für PlayStation, Xbox 360, Nintendo Wii, PSP und Mobiltelefon Besitzer ertönt bereits am 29. April 2010. Über einem Monat vor Beginn der echten Weltmeisterschaft.</p> '
featured_image: /wp-content/uploads/2010/01/fifawc.jpg

---
Diese startet mit einer Partie zwischen Südafrika und Mexiko am 11. Juni 2010. EA\`s <a href="http://www.easports.com/" target="_blank">FIFA World Cup 2010</a> erscheint mit allen 199 Nationalmannschaften, die an der Qualifikation beteiligt waren. Dabei können sie in 10 im Original abgebildeten Stadien ihre Matches austragen. Um sich von dem noch nicht vor allzu langer Zeit erschienen FIFA 10 abzuheben, bringt der Entwickler auch neue Features ins Spiel. So wird es möglich sein Online die Weltmeisterschaft durchzuspielen. Auch bereits ausgeschiedene Mannschaften sollen dabei mit von der Partie sein. Weiterhin wird es Taktiken für Heim- und Auswärtsspiele geben und die Höhenlage der Stadien soll Auswirkung auf die Flugbahn des Balls haben.

* * *



* * *