---
title: Patrizier 4 – Erscheint auch als Limited Edition
author: gamevista
type: news
date: 2010-07-23T14:33:56+00:00
excerpt: '<p>[caption id="attachment_1615" align="alignright" width=""]<img class="caption alignright size-full wp-image-1615" src="http://www.gamevista.de/wp-content/uploads/2010/03/patrizier4.jpg" border="0" alt="Patrizier 4" title="Patrizier 4" align="right" width="140" height="100" />Patrizier 4[/caption]Die Wirtschaftssimulation <a href="http://www.patrizier4.de" target="_blank">Patrizier 4</a> wird auch als Limited Edition mit vielen Extras erhältlich sein. Dies bestätigte der Publisher <strong>Kalypso Media</strong> im Rahmen einer Pressemitteilung. Die Spezialausgabe wird stolze 59,99 Euro kosten, bietet dementsprechend aber zusätzlichen Inhalt.</p> '
featured_image: /wp-content/uploads/2010/03/patrizier4.jpg

---
Patrizier 4 wird am 2. September 2010 für PC erhältlich sein.

<span style="color: #333333;"><strong>Features der Limited Edition:</strong></span>

Patrizier IV mit Special Content (Exklusives Schiff und Kontor)   
Gedruckte Seekarte   
Patrizier IV Mousepad   
Patrizier IV Soundtrack   
Große Verpackung

   

* * *

   

