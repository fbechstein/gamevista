---
title: 'Batman: Arkham Asylum – Demo als Download'
author: gamevista
type: news
date: 2009-08-07T10:14:20+00:00
excerpt: '<p>[caption id="attachment_45" align="alignright" width=""]<a href="http://www.eidos.de" target="_blank" title="eidos"><img class="caption alignright size-full wp-image-45" src="http://www.gamevista.de/wp-content/uploads/2009/07/batman_small.png" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Eidos </a>eidos[/caption]hat pünktlich zum Wochende endlich seine lang erwartete Demo zu <strong>Batman: Arkham Asylum</strong> veröffentlicht. Ganze 2 GB werdet ihr zu bewältigen haben um in der Rolle des dunklen Ritters schlüpfen zu dürfen. Die Demo startet mit der Einlieferung <span> </span>des Jokers in eine Nervenheilanstalt, natürlich befreit sich dieser aus seinen Fesseln und ihr müsst diesen Schurken das Handwerk legen.</p>'
featured_image: /wp-content/uploads/2009/07/batman_small.png

---
Wie immer findet ihr die Demo in unserem Downloadbereich oder hier per Link. Wir wünschen viel Spaß beim antesten. 

[> Zur Batman: Arkham Asylum Demo][1]







 

 [1]: downloads/demos/item/demos/batman-arkham-asylum-demo "demo zu batman arkham asylum"