---
title: 'Red Orchestra 2: Heroes of Stalingrad –  Neue Details bekannt'
author: gamevista
type: news
date: 2009-08-10T18:43:21+00:00
excerpt: '[caption id="attachment_320" align="alignright" width=""]<img class="caption alignright size-full wp-image-320" src="http://www.gamevista.de/wp-content/uploads/2009/08/redorch_small.jpg" border="0" alt="Red Orchestra 2: Heroes of Stalingrad" title="Red Orchestra 2: Heroes of Stalingrad" align="right" width="140" height="100" />Red Orchestra 2: Heroes of Stalingrad[/caption]Publisher <a href="http://www.1cpublishing.eu" target="_blank">1C Company</a> teilt uns neue Details zu ihrem WW2 Ego Shooter Red <strong>Orchestra 2: Heroes of Stalingrad</strong> mit. <span> </span>Kurz vor der gamescom hat nun der russische Publisher eines der wichtigsten Spielneuerungen bekannt gegeben, den Coop Modus.<span style="font-size: 10pt; font-family: Symbol"><span><br /></span></span>'
featured_image: /wp-content/uploads/2009/08/redorch_small.jpg

---
  * <span><strong>Stalingrad-Kampagne:</strong> Ihr könnt jeweils auf deutscher oder sowjetischer Seite die Schlacht bestreiten.</span>
  * <span><strong>Squad Command:</strong> Mit Hilfe einer einfachen Benutzeroberfläche befehligt ihr Squads auf dem Schlachtfeld</span>
  * <span><strong>Heroes System:</strong> Wenn ihr den Status „Heroes“ erhaltet, inspiriert ihr andere Mitspieler und verbreitet Angst bei ihren Gegnern. Helden haben Zugang zu den seltensten Waffen und der besten Ausrüstung. Auch das Aussehen soll sich von „normalen“ Spielern unterscheiden.</span>
  * <span><strong>Statistiken / Fortschritt</strong>: Alle im Spiel erlangten Medaillen und Fähigkeiten sind von den Spielern einsehbar und direkt im Spiel integriert</span>
  * <span><strong>Koop-Modus: </strong>Es wird einen Co-Op Modus geben der es euch möglich macht mit Freunden die Stalingrad Kampagne oder den Skirmish Modus zu spielen.</span>
  * <span><strong>Deckung</strong>: Red Orchestra 2: Heroes of Stalingrad wird ein Deckungssystem System erhalten was es euch ermöglicht um Ecken zu schauen oder blind zu schießen ähnlch wie in Vegas 2</span>

<p class="MsoNormal">
  Bisher gibt es keinen Release Termin.
</p>







<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
   
</p>