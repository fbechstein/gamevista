---
title: Electronic Arts auf der gamescom – Webcam bringt Durchblick
author: gamevista
type: news
date: 2009-08-17T13:23:34+00:00
excerpt: '<p>[caption id="attachment_374" align="alignright" width=""]<img class="caption alignright size-full wp-image-374" src="http://www.gamevista.de/wp-content/uploads/2009/08/cc4_small.png" border="0" alt="Command &amp; Conquer 4 gehört u.a. zu den Highlights auf dem EA Stand" title="Command &amp; Conquer 4 gehört u.a. zu den Highlights auf dem EA Stand" align="right" width="140" height="100" />Command &amp; Conquer 4 gehört u.a. zu den Highlights auf dem EA Stand[/caption]Wer es nicht mehr erwarten kann den <a href="http://www.electronic-arts.de/" target="_blank">Electronic Arts</a> Stand auf der gamescom zu besuchen, den empfehlen wir den Blick auf die eigens eingerichtete Website von EA zur gamescom. Unter <a href="http://www.gamescom.electronic-arts.de">http://www.gamescom.electronic-arts.de</a> bekommt Ihr die Möglichkeit sich über die Neuheiten zu informieren. Als weiteres Feature bekommt Ihr den Webcam Blick auf die EA Bühne. So könnt Ihr jetzt schon alle Aufbauarbeiten auf dem EA Stand sehen.</p> '
featured_image: /wp-content/uploads/2009/08/cc4_small.png

---
<a href="http://www.gamescom.electronic-arts.de/publish/page206768566276405.php3" target="_blank">> zur EA-gamescom-Stand-Webcam</a>

* * *



* * *