---
title: R.U.S.E. – Demo mit Singleplayer-Mission veröffentlicht
author: gamevista
type: news
date: 2010-08-26T15:51:21+00:00
excerpt: '<p>[caption id="attachment_2163" align="alignright" width=""]<img class="caption alignright size-full wp-image-2163" src="http://www.gamevista.de/wp-content/uploads/2010/08/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Endlich hat der Publisher <strong>Ubisoft </strong>seine Demo zum Echtzeit-Strategiespiel <a href="http://ruse.de.ubi.com" target="_blank">R.U.S.E.</a> veröffentlicht. Nach mehreren Betas die ausschließlich den Mehrspieler-Inhalt wiedergaben, wird es nun auch möglich sein erste Einzelspieler-Eindrücke zu sammeln.</p> '
featured_image: /wp-content/uploads/2010/08/ruse_small.jpg

---
Passend dazu hat **Ubisoft** eine exklusive Demomission erstellt die ihr nun ausgiebig testen könnt. Die Demo steht ab sofort per Online-Plattform <a href="http://store.steampowered.com/news/4248/" target="_blank">Steam</a> zum Download bereit.

 

<a href="http://store.steampowered.com/news/4248/" target="_blank">> Zum R.U.S.E. &#8211; Demo Download</a>

   

* * *

   



 