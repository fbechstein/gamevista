---
title: Alien vs. Predator – Releasetermin für Europa
author: gamevista
type: news
date: 2009-12-10T18:06:04+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Für den Ego-Shooter <strong>Aliens vs. Predator</strong> war bisher kein genauer Veröffentlichungstermin bekannt. So sollte der neuste Teil der Alien-Shooter-Reihe im ersten Quartal 2010 erscheinen. Nun hat der Publisher <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">SEGA</a> den Release weiter eingegrenzt. Demnach soll <strong>Aliens vs. Predator</strong> bereits am 19. Februar 2010 in Europa erscheinen.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Deutsche Fans müssen leider in den saueren Apfel beißen und die Importversion ordern, denn <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">SEGA</a> hat darauf verzichtet den Shooter der Unterhaltungssoftware Selbstkontrolle zur Prüfung vorzulegen. Demnach wird **Aliens. vs. Predator** nicht im deutschen Handel erscheinen und nur über das Ausland importierbar sein.

 





