---
title: Star Trek Online – Drittes Inhalts-Update veröffentlicht
author: gamevista
type: news
date: 2010-12-13T19:28:21+00:00
excerpt: '<p><strong>[caption id="attachment_2491" align="alignright" width=""]<img class="caption alignright size-full wp-image-2491" src="http://www.gamevista.de/wp-content/uploads/2010/12/logo.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Atari </strong>und <strong>Cryptic Studios</strong> haben den offiziellen Start des dritten großen Updates mit dem Namen <strong>Genesis </strong>angekündigt. Die kostenlose Erweiterung bringt zahlreiche Neuerungen ins Spiel und verbessert bereits bekannte Features. Unter anderem wird es möglich sein Borg-Technologie in die Schiffe zu integrieren.</p> '
featured_image: /wp-content/uploads/2010/12/logo.jpg

---
Passend dazu gibt es einen offiziellen Launch-Trailer.

[> Zum Star Trek Online – Genesis Trailer][1]

   

* * *

   



 [1]: videos/item/root/star-trek-online-genesis-trailer