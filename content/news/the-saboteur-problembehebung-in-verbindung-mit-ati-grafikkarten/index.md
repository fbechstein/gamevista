---
title: The Saboteur – Problembehebung in Verbindung mit ATI-Grafikkarten
author: gamevista
type: news
date: 2009-12-07T18:59:20+00:00
excerpt: '<p>[caption id="attachment_1133" align="alignright" width=""]<img class="caption alignright size-full wp-image-1133" src="http://www.gamevista.de/wp-content/uploads/2009/12/the-saboteur.jpg" border="0" alt="The Saboteur" title="The Saboteur" align="right" width="140" height="100" />The Saboteur[/caption]Der Entwickler <a href="http://www.pandemicstudios.com/thesaboteur/" target="_blank">Pandemic Studios</a> hat in seinem <a href="http://forum.ea.com/eaforum/posts/list/346070.page" target="_blank">offiziellen Forum</a> zum Actionspiel <strong>The Saboteur</strong> eine Fehlerbehebung zum ATI-Grafikkarten Problem beschrieben. So haben viele Leute in Verbindung mit Windows Vista bzw. Windows und ATI-Grafikkarten Abstürze von <strong>The Saboteur</strong> zu beklagen.</p> '
featured_image: /wp-content/uploads/2009/12/the-saboteur.jpg

---
Nun hat sich der <a href="http://www.pandemicstudios.com/thesaboteur/" target="_blank">Entwickler </a>zu Wort gemeldet und mitgeteilt, dass man an dem Problem arbeite. Das Workaround sieht vor das man mehrere Kerne von Multikernprozessoren deaktivieren muss. Zwar läuft das Spiel so relativ Stabil aber sehr langsam da ja nur ein Teil der Rechenpower genutzt werden kann.

**<span style="text-decoration: underline;">So könnt ihr temporär The Saboteur zum laufen bekommen:</span>**

<div id="intelliTXT">
  <strong id="nointelliTXT">Windows 7:</strong></p> 
  
  <ol>
    <li>
      MSCONFIG öffnen
    </li>
    <li>
      die &#8222;Boot&#8220;-Sektion auswählen
    </li>
    <li>
      auf &#8222;Erweiterte Optionen&#8220; klicken
    </li>
    <li>
      “Anzahl der Prozessoren” überprüfen
    </li>
    <li>
      Aus dem Dropdown-Menü &#8222;1&#8220; ausfwählen
    </li>
    <li>
      auf &#8222;Übernehmen&#8220; klicken
    </li>
    <li>
      PC neustarten
    </li>
  </ol>
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Windows Vista</strong></p> 
  
  <ol>
    <li>
      MSCONFIG öffnen
    </li>
    <li>
      die &#8222;Boot.ini&#8220;-Sektion auswählen
    </li>
    <li>
      auf &#8222;Erweiterte Optionen&#8220; klicken
    </li>
    <li>
      den Haken bei /NUMPROC=&#8220; setzen und auf &#8222;1&#8220; setzen
    </li>
    <li>
      auf &#8222;OK&#8220; klicken
    </li>
    <li>
      PC neustarten
    </li>
  </ol>
</div>

 





