---
title: Medal of Honor – Beta endlich auch für Xbox 360-Besitzer
author: gamevista
type: news
date: 2010-07-21T21:59:10+00:00
excerpt: '<p>[caption id="attachment_2043" align="alignright" width=""]<img class="caption alignright size-full wp-image-2043" src="http://www.gamevista.de/wp-content/uploads/2010/07/moh2010.jpg" border="0" alt="Medal of Honor" title="Medal of Honor" align="right" width="140" height="100" />Medal of Honor[/caption]Via Twitter hat der Publisher <strong>Electronic Arts</strong> die Beta-Phase für den Ego-Shooter <a href="Medal of Honor" target="_blank">Medal of Honor</a> nun auch für Xbox 360-Besitzer gestartet. Wie lange ihr den neusten Teil testen könnt ist bisher nicht bekannt.</p> '
featured_image: /wp-content/uploads/2010/07/moh2010.jpg

---
Eine weitere Meldung zur PlayStation 3-Beta von <Medal of Honor> wurde auf der <a href="http://www.medalofhonor.com/en_GB/blog/2010/07/medal-honor-beta-extension" target="_blank">offiziellen Internetseite</a> von EA verkündet. Diese wurde vor kurzem verlängert und soll bis zum 31. Juli 2010 laufen.

 

   

* * *

   

