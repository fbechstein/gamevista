---
title: 'Arcania: Gothic 4 – PlayStation 3-Version verschoben auf 2011'
author: gamevista
type: news
date: 2010-08-05T19:29:54+00:00
excerpt: '<p>[caption id="attachment_2101" align="alignright" width=""]<img class="caption alignright size-full wp-image-2101" src="http://www.gamevista.de/wp-content/uploads/2010/08/burg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Publisher <strong>JoWood Entertainment</strong> teilte kürzlich im Rahmen einer Pressemitteilung die Verschiebung der PlayStation 3-Version des Rollenspiels <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> mit. Somit erscheint der Titel für die Sony-Konsole erst im ersten Halbjahr 2011. Die Xbox 360- und PC-Fassungen bleiben davon unberührt.</p> '
featured_image: /wp-content/uploads/2010/08/burg_small.png

---
Diese erscheinen wie gehabt am 12. Oktober 2010 im Handel. Grund dafür liefert der Publisher mit den hohen Qualitätsansprüchen die sie an die PS3-Version stellen. **JoWood** verspricht einen optimalen Spielspaß auf allen Plattformen.

 

   

* * *

   

