---
title: 'Fallout: New Vegas – Werbespot stimmt auf Veröffentlichung ein'
author: gamevista
type: news
date: 2010-09-26T15:56:28+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Publisher <strong>Namco Bandai </strong>hat einen weiteren Trailer für das Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> veröffentlicht. Im neusten Video wird der einminütige TV-Spot mit zahlreichen Render- und Spielszenen gezeigt. Dieser wird momentan hauptsächlich im Ausland ausgestrahlt.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
<a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> erscheint am 22. Oktober 2010 im Handel.

[> Zum Fallout: New Vegas &#8211; TV Spot Trailer][1]

   

* * *

   



 [1]: videos/item/root/fallout-new-vegas-tv-spot-trailer