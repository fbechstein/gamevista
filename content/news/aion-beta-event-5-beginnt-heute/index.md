---
title: 'Aion – Beta Event #5 beginnt heute'
author: gamevista
type: news
date: 2009-07-31T20:01:17+00:00
excerpt: '<p>[caption id="attachment_240" align="alignright" width=""]<img class="caption alignright size-full wp-image-240" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion - Das Beta Event #5 beginnt heute" title="Aion - Das Beta Event #5 beginnt heute" align="right" width="140" height="100" />Aion - Das Beta Event #5 beginnt heute[/caption]Nur noch eine Stunde und der <strong>Aion</strong> Beta Event #5 kann beginnen. Ihr habt dann bis zum 3. August 2009 Zeit Aion auf Herz und Nieren zu testen. Anbei ein Überblick der zu erwartenden Neuerungen: </p>'
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
  * Neue Audio-Dateien hinzugefügt 
      * NSC&#8217;s begrüßen und verabschieden euch jetzt mit hörbarer Stimme 
      * Zwischensequenzen sind jetzt vertont 
      * Die Prologe nach der Erstellung eines Elyos oder Asmodiers sind jetzt verfügbar 
      * Tutorial-Videos sind jetzt über das Menü &#8222;Hilfe&#8220; (Help) verfügbar 
      * Bei der Charaktererstellung können jetzt Stimmen ausgewählt werden 
      * Systemmeldungen jetzt auch vertont 
  * Neue NSC Dialoge/Questbeschreibungen für Theobomos und Brusthonin 
  * Mehrere Übersetzungskorrekturen (Englisch) 
      * Fehlerhafte HTML-Tags korrigiert 
      * Behebung einiger grammatikalischer Fehltritte 
      * Anpassung einiger Glossardefinitionen 
  * 2 neue Server für jede Region (EU und NA. Insgesamt 4)







&nbsp;