---
title: Serious Sam HD – Termin aktualisiert
author: gamevista
type: news
date: 2009-11-16T18:02:03+00:00
excerpt: '<p>[caption id="attachment_378" align="alignright" width=""]<img class="caption alignright size-full wp-image-378" src="http://www.gamevista.de/wp-content/uploads/2009/08/serious_sam_hd_small.png" border="0" alt="Serious Sam HD" title="Serious Sam HD" align="right" width="140" height="100" />Serious Sam HD[/caption]Der Publisher <a href="http://www.serioussam.com" target="_blank">cdv Software</a> hatte bisher das Veröffentlichungsdatum für das Actionspiel <strong>Serious Sam HD </strong>nicht aktualisiert. Demnach sollte der Shooter bereits am 28. Oktober 2009 im Handel erscheinen. Das daraus nichts geworden ist bemerkte der Publisher und verschob das Ganze auf den 19.11.2009.</p> '
featured_image: /wp-content/uploads/2009/08/serious_sam_hd_small.png

---
Doch nun lässt der Publisher auch diesen Termin ändern und so wird das hoffentlich endgültige Release auf den 10. Dezember festgelegt. Amazon.de hat bereits darauf reagiert und stellte den Termin prompt um.

 

* * *



* * *