---
title: 'Special: Games Convention vs. Gamescom'
author: gamevista
type: news
date: 2009-06-23T08:00:00+00:00
excerpt: '<img class=" alignright size-full wp-image-7" src="http://www.gamevista.de/wp-content/uploads/2009/06/games_convention.png" border="0" alt="Games Convention Online - 2009 mit neuem Konzept" title="Games Convention Online - 2009 mit neuem Konzept" vspace="30" width="140" height="91" align="right" /> <p>Wir schreiben das Jahr 2002, das Geburtsjahr der <a href="http://www.gamesconvention.com/" target="_blank">Games Convention</a> in Leipzig . Ca. 80.000 Besucher strömten damals auf die Leipziger Messe um sich über Computerspiele, Hard- und Software zu informieren.  Der Träger der Games Convention in Leipzig war die <strong>BIU</strong> (Bundesverband Interaktive Unterhaltungssoftware), einer Interessenvertretung der Entwickler und Publisher von Computerspielen in Deutschland.  Hierzu gehören u.a. namhafte Unternehmen wie Atari, Activision, Electronic Arts um nur einige zu nennen. </p>'
featured_image: /wp-content/uploads/2009/06/games_convention.png

---
Die  **Games Convention** wuchs im Laufe der Jahre zu einer der wichtigsten Messen aus dem Bereich Infotainment mit einer Rekordbesucherzahl von über 200.000 im Jahre 2008.  Die Messe war deswegen so beliebt, da sie sich sowohl an Fachpublikum als auch die breite Masse richtete. Für die Fachbesucher, Presse und Aussteller  wurde eigens ein Business Center errichtet, in welchem man die Gelegenheit hatte in Dialog mit den Publishern und Entwicklern zu treten.** ** 

**Das Jahr 2009</p> 

</strong>Im Jahre 2009 ändert sich jedoch einiges. Die **Games Convetion** in Leipzig wird nicht mehr von der BIU veranstaltet, sondern soll sich selbst finanzieren. Finanzielle Hilfe soll hier vom Freistaat Sachsen und der Stadt Leipzig folgen. Das Motto der diesjährigen Games Convention in Leipzig vom 31.07.2009 bis zum 02.08.2009 trägt den Namen „Games Convention Online“, dahinter steckt die Philosophie die „weltweit erste Messe für Online-Spiele“ zu veranstalten. Hierzu zählen nur noch Browser Games, Handyspiele und Online-Spiele.

Die Leipziger Messe versucht hier einen Neuanfang zu wagen. Es soll hier eine Art Community Treff diverser Gilden zu geben – eine der bekanntesten Gilden, die Gothic Gilde, hat sich bereits offiziell angemeldet. Begleitet wird die Messe von der „GCO Conference“ – einer Konferenz zu Browser-, Online- und Mobile-Games.  
**  
**

**Im Westen etwas Neues<img class=" alignright size-full wp-image-8" src="http://www.gamevista.de/wp-content/uploads/2009/06/gamescom_small.png" border="0" alt="gamescom" title="gamescom vom 19.08.2009 - 23.08.2009 in Köln" vspace="50" width="140" height="100" align="right" />**

Nach der Abwanderung der BIU von der Leipziger Messe, wurde ein neuer Standort für eine neue Infotainment Messe gefunden – Köln.   
Vom 19.08.2009 bis zum 23.08.2009 soll an der Kölner Messe die <a href="http://www.gamescom.de/" target="_blank">gamescom </a>stattfinden. Für den neuen Standort sprechen viele Faktoren:  
&#8211; das Messegelände samt Infrastruktur und Einzugsgebiet   
&#8211; die Internationalität und das weltweite Vertriebsnetz der Koelnmesse  
&#8211; die Erfahrung mit Großveranstaltungen   
&#8211; die Integration der gamescom in die Medien- und Eventstadt Köln

Alleine die Tatsache, dass das Kölner Messegelände das viertgrößte der Welt ist, spricht für sich.  
Der Name Games Convention durfte aus rechtlichen Gründen nicht genutzt werden, so dass der Name gamescom seine Geburtstunde feiert. Ironischerweise lassen sich sowohl die Games Convention, als auch die gamescom mit dem Kürzel GC abkürzen.  
Wie auch die Jahre zuvor wird die Messe von der Games Developer Conference Europe, eine Konferenz für Entwickler und Business, begleitet. Auch am neuem Standort dürfen sich Fachbesucher und Presse auf ein Business Area freuen, die in zwei Hallen der Koelnmesse zu finden ist.

**Konkurrenzkampf?**

Bisher ist kein Konkurrenzgedanke bei den Leipziger zu spüren, wagen die eher einen Neuanfang mit einem neuem Konzept, als eine Messe wie die gamescom auf die Beine zu stellen. Konkurrenz wäre wohl auch fehl am Platz, wenn man sich die Zahlen anschaut. Die Games Convention mit ca. 50 Ausstellern erwartet ca. 50.000-70.000 Besucher. Die gamescom erwartet mehr als 200.000 Besucher, was wohl bei den Zahlen des letzten Jahres von der Games Convention nicht allzu hoch gegriffen sein sollte.



**Zukunftsperspektiven  
**   
Wie sich beide Messen entwickeln werden bleibt abzuwarten. Spätestens Ende August können wir ein Resümee ziehen. Das Team von gamevista.de ist selbstverständlich für Euch vor Ort um täglich über Neuheiten zu berichten.  





