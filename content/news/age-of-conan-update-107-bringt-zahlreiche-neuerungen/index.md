---
title: Age of Conan – Update 1.07 bringt zahlreiche Neuerungen
author: gamevista
type: news
date: 2010-04-01T17:10:24+00:00
excerpt: '<p>[caption id="attachment_155" align="alignright" width=""]<img class="caption alignright size-full wp-image-155" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderranges_1024x768_small.jpg" border="0" alt="Age of Conan" title="Age of Conan" align="right" width="140" height="100" />Age of Conan[/caption]Ab sofort ist ein neuer Patch für das Online-Rollenspiel <a href="http://www.ageofconan.com" target="_blank" rel="nofollow">Age of Conan: Hyborian Adventures</a> verfügbar und auf den Servern aufgespielt worden. Das Update bringt zahlreiche Neuerungen mit sich. Hauptinhalt des Patches sind diverse PvP-Verbesserungen.</p> '
featured_image: /wp-content/uploads/2009/07/borderranges_1024x768_small.jpg

---
So wurden die PvP-Stufen 6 bis 10 eingeführt, sowie PvP-Token und neue Items in das Spiel implementiert. Außerdem wurden an der Klassenbalance gearbeitet. Das komplette Changelog findet ihr auf der [zweiten Seite][1].

<cite></cite>

 __

* * *

 __  



 



<img class=" alignright size-full wp-image-155" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderranges_1024x768_small.jpg" border="0" alt="Age of Conan" title="Age of Conan" align="right" width="140" height="100" /><strong id="nointelliTXT">Neuer PvP-Inhalt – &#8222;Schreine von Bori&#8220;</strong>

 

<div id="intelliTXT">
  Mit Schreine von Bori erwartet die Spieler ein aufregender, gildenbasierter PvP-Konflikt in einem Außenbereich mit Übernahmezielen am cimmerischen Ende der Grenzkönigreiche. Zum Gameplay gehört die Übernahme von Schreinen und deren Verteidigung gegen andere Spieler. Außerdem spielen neue seltene PvP-Ressourcen eine Rolle, die Spieler sammeln und zu den übernommenen Schreinen bringen müssen, um dem Kriegsgott Bori Geschenke darzubringen und dafür seine Gunst zu erlangen.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Zusätzliche PvP-Stufen, PvP-Token und neue Grad 2-PvP-Ausrüstung</strong><br />Das Update 1.07 öffnet außerdem die PvP-Stufen 6 bis 10 und umfasst neue Grad 2-PvP-Ausrüstung. Für diese Ausrüstung muss ein Spieler die entsprechende PvP-Stufe erreicht haben. Der Preis ist in den neuen PvP-Token angegeben, genau wie Raid-Ausrüstung in Raid-Token bezahlt wird. Alle PvP-Token, die man durch Opfer an Bori erhält, sind vom selben Typ. Einen anderen Typ von PvP-Token bekommt man in Minispielen. Sie werden nach Abschluss von Steckbrief-Quests für den Sieg in den verschiedenen Minispielen vergeben (eine Quest pro Minispiel-Typ). Diese Quests haben Abklingzeiten von mehreren Stunden.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Teaser-Event der Erweiterung – &#8222;Das Silberne Atrium&#8220;</strong><br />Erlebt &#8222;Das Silberne Atrium&#8220; – ein neues Event im Spiel, das euch mit neuen Quests in Stygien und Aquilonien herausfordert. Trefft und bekämpft geheimnisvolle Spione, Assassinen und andere zwielichtige Gestalten aus dem Fernen Osten, um neue großartige Belohnungen zu erhalten.
</div>

<div id="intelliTXT">
  Spieler ab Stufe 40 können das neue, geheimnisvolle Silberne Atrium besuchen (der Zugang ist schon für Spieler mit Stufe 20 oder höher möglich, aber die Quests bei diesem Event sind erst ab Stufe 40 verfügbar).
</div>

<div id="intelliTXT">
  Ihr betretet das Silberne Atrium, indem ihr euch zunächst das Verbrauchsobjekt &#8222;Bestickte Einladung&#8220; besorgt (mit dem Befehl &#8222;/claim&#8220;). Wenn ihr das Objekt dann benutzt, wird die Quest eurem Quest-Tagebuch hinzugefügt. Zuerst führt euch der Weg in die Stadt Khemi
</div>

<div id="intelliTXT">
  Das Silberne Atrium bietet zwei brandneue Einzelquests und eine neue Gruppenquest, die mit dem Auftauchen geheimnisvoller Vorboten aus östlichen Gefilden zu tun haben.
</div>

<div id="intelliTXT">
  Enthält großartige, neue Belohnungen mit Khitai-Thematik.
</div>

<div id="intelliTXT">
  Die Questreihe ist nur bis zum Erscheinen der Erweiterung verfügbar. Ihr solltet sie euch also ansehen, bevor sie wieder weg ist!
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Neues regionales System für schnelles Reisen</strong><br />Durch neue Kutscher-NPCs in größeren Außenposten und den Questzentren können Spieler für eine geringe Gebühr zu den wichtigsten Orten innerhalb des Spielfelds reisen. Im Eiglophianischen Gebirge steht im Dorf Dinog beispielsweise ein neuer NPC bereit, der einfache Fahrten zu mehreren Orten auf dem Spielfeld &#8222;Eiglophianisches Gebirge&#8220; anbietet (beispielsweise die Jagdhütte, den Eingang zum Ymir-Pass oder Yakhmars Höhle).</p> 
  
  <p>
    Außerdem stehen gleich neben den neuen Kutscher-NPCs auch neue Händler, die &#8222;Kutschenrouten-Karten&#8220; als Verbrauchsobjekte verkaufen. Benutzt ihr diese, bringen sie euch direkt zum Knotenpunkt des aktuellen Spielfelds.
  </p>
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Klassen</strong></p> 
  
  <p>
    Nekromant
  </p>
  
  <p>
    Der Zauber &#8222;Gefrorener Hass&#8220; bleibt nun bei Gebietswechseln bestehen.
  </p>
</div>

<div id="intelliTXT">
  Waldläufer</p> 
  
  <p>
    &#8222;Mezz-Schuss&#8220; gewährt jetzt die korrekten Massenkontrolle-Immunitäten.
  </p>
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br /><span style="text-decoration: underline;">PVP</span></strong><br /><strong id="nointelliTXT"><br />Allgemein</strong>
</div>

<div id="intelliTXT">
  Das Feldhandbuch-Tutorial, das beim Beginn eines &#8222;Capture the Skull&#8220;-Minispiels angezeigt wird, führt jetzt zum korrekten Eingang.
</div>

<div id="intelliTXT">
  Tortage: Charaktere mit Stufe 26 oder höher werden jetzt automatisch von Hellsand bzw. Katakomben am Tag entfernt.
</div>

<div id="intelliTXT">
  Khemi-Trainingsarena: Der Zuschauerbereich wurde bis zum unteren Ende der Treppe vergrößert. Es ist dadurch einfacher, die Kämpfe zu verfolgen.
</div>

<div id="intelliTXT">
  Die Art, wie PvP-EP für Kills vergeben werden, wurde angepasst, um mit den neuen Veränderungen am PvP-Fortschritt zu harmonieren.
</div>

<div id="intelliTXT">
  Ist ein Spieler einer Gruppe außerhalb der Reichweite, um PvP-EP zu erhalten, die seine Gruppe bekommt, erhält er jetzt auch die geschwächte Rückkehr nicht, die dem getöteten Ziel hinzugefügt wird.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Massive PVP</strong>
</div>

<div id="intelliTXT">
  Besitzer von Schlachtburgen können ihre Belagerungshändler erneut einsetzen.Belagerungswaffen haben zum Feuern jetzt Reichweitenanforderungen. Um eine
</div>

<div id="intelliTXT">
  Belagerungswaffe einzusetzen, dürft ihr höchstens 25 Meter entfernt sein. Beträgt die Distanz zwischen euch und eurer Belagerungswaffe mehr als 40 Meter, zerstört sich die Belagerungswaffe selbst. Außerdem kann eure Belagerungswaffe nicht feuern, solange Ihr versteckt seid.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Minispiele</strong>
</div>

<div id="intelliTXT">
  Die Erfahrung für Minispiele wurde überarbeitet. Der Hauptteil der Erfahrung kommt nun durch die Erfüllung der Ziele des Minispiels, wie etwa die Eroberung eines Schädels und die Zerstörung eines Totems. Übersteigt die Dauer eines Minispiels einen bestimmten Zeitraum, sinkt die Erfahrung, die man für das Abschließen des Minispiels erhält.
</div>

<div id="intelliTXT">
  Totem-Sturzbach: Bei der Bestimmung der Punkte wird nun auch der Status des Totems berücksichtigt. Wenn also eine Seite ihr Totem verliert, hat die Seite, die noch über ihr Totem verfügt, das Minispiel gewonnen. Wenn beide Seiten ihr Totem am Ende der Spielzeit des Minispiels noch haben (oder es beide verloren haben), wird der Sieger anhand der Kills ermittelt.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Raids </strong><br /><strong id="nointelliTXT"><br />Eisenturm</strong><br />Die fliehende Seele des Wächters wurde korrigiert. Er heult jetzt noch einmal, bevor er wegläuft.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Thoth-Amons Festung</strong><br />Alle Respawn-Zeiten von Thoth-Amons Trash-Mobs wurden erhöht. Einige davon wurden stärker erhöht als andere.
</div>

<div id="intelliTXT">
  Günstling von Louhi: Phylakterien bauen vor dem Kampf keine Aggro gegenüber Spielern mehr auf.Spieler können nicht mehr ausweichen, um den Auswirkungen der Reue zu entkommen.Unheilige Vergeltung wird jetzt korrekt ausgelöst und den richtigen Malus anwenden.
</div>

<div id="intelliTXT">
  Gyas: &#8222;Hinrichten&#8220; kann durch die Sichtlinie nicht länger blockiert werden.
</div>

<div id="intelliTXT">
  Hathor-Ka: Die Macht der Verbesserung durch &#8222;Verzehren&#8220; (Omen) wurde bei niedrigen Stapeln leicht und bei höheren Stapeln drastisch erhöht. Außerdem hassen die Vorboten des Todes ihre Ziele jetzt etwas mehr.
</div>

<div id="intelliTXT">
  Ixion: Die Zauberzeit von &#8222;Zorn&#8220; wurde verkürzt und seine Laufgeschwindigkeit erhöht.
</div>

<div id="intelliTXT">
  Arbanus: &#8222;Blutmagnet&#8220; und &#8222;Unheiliges Wort&#8220; können durch die Sichtlinie nicht mehr vermieden werden. Außerdem sollten Spieler hinter den Waffenständern nicht mehr stecken bleiben oder durch den Boden fallen.
</div>

<div id="intelliTXT">
  Der Hüter der Artefakte: Ein Problem wurde behoben, wegen dem dieser Kampf manchmal zum Absturz führte.
</div>

<div id="intelliTXT">
  Frost- und Feuerversion von &#8222;Der Hüter der Artefakte&#8220; wurden überarbeitet.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Grafische Benutzeroberfläche</strong><br />Das Weltkartenfenster hat eine neue Optik und eine zusätzliche Funktion. Ihr könnt jetzt ganz einfach zwischen verschiedenen Spielfeldern wechseln und außerdem eine Liste von Kartensymbolen aufrufen, die deren Bedeutung erklären.
</div>

<div id="intelliTXT">
  Fensterpositionen und Hotkeys werden jetzt online auf dem Server gespeichert und nicht wie bisher auf eurer lokalen Festplatte. Nach jedem erfolgreichen Abmelden werden die Einstellungen gespeichert.
</div>

<div id="intelliTXT">
  Der Befehl /claim öffnet jetzt ein neues Fenster, in dem Spieler auswählen können, welche Gegenstände sie beanspruchen möchten.
</div>

<div id="intelliTXT">
  Beim Start des Spielclients (nach dem Launcher-Fenster) seht ihr jetzt ein kleines Bild, während der Client geladen wird
</div>

<div id="intelliTXT">
  Ihr könnt jetzt einzelne Quests auf der Karte im Quest-Tracker-Fenster verfolgen und euch die Ziele mehrerer aktiver Quests gleichzeitig anzeigen lassen. Um die Anzahl der angezeigten Quests zu ändern, betätigt ihr den neuen Schieberegler unten im Fenster F10/Optionen.
</div>

<div id="intelliTXT">
  Automatische Zielerfassung und Automatische Blickrichtung sind nun standardmäßig deaktiviert.
</div>

<div id="intelliTXT">
  Spieler mit Abonnement erhalten nun eine Warnung, wenn sie einem Gastspieler eine Nachricht senden, der nicht auf ihrer Freundesliste steht.
</div>

<div id="intelliTXT">
  Die Aktivierung der Optionen im DX10-Abschnitt des Grafikoptionen-Menüs führt nicht mehr zu einem Flackern der grafischen Benutzeroberfläche.
</div>

<div id="intelliTXT">
  Spieler werden im Gildennachrichtenkanal nun wieder informiert, wenn Gildenmitglieder aufsteigen.
</div>

<div id="intelliTXT">
  JPG-Screenshots sind jetzt F11, PNG-Screenshots F12 zugewiesen.
</div>

<div id="intelliTXT">
  Der Befehl /camp bringt euch nicht mehr zum Anmeldungsbildschirm, sondern zum Charakterauswahlbildschirm. Der Befehl /quit bringt euch nach wie vor zum Anmeldungsbildschirm.
</div>

<div id="intelliTXT">
  Es gibt nun eine Einstellmöglichkeit in der Optionen des Launchers, der Spielern ermöglicht die benutzte Bandbreite beim laden von Spieldaten im Hintergrund während des Spielens einzustellen. Dadurch wird nicht die Geschwindigkeit des Downloadens beim Zonen verändert und wird nur für jene Spieler bemerkbar, die Inhaltspakete im Launcherfenster abgewählt haben.
</div>

<div id="intelliTXT">
  Es gibt jetzt eine Chatfilter Optionen, die man in den Allgemeinen Benutzeroberfläche Optionen aktivieren kann. Sie wird Schimpfwörter in eurer Sprache herausfiltern, wenn ihr sie aktiviert.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />NPCs/MOBS</strong><br />Eiglophianisches Gebirge: Der Schaden des von &#8222;Rachgieriger Geist&#8220; gewirkten Zaubers &#8222;Reißende Macht&#8220; wurde verringert.
</div>

<div id="intelliTXT">
  Ymir-Pass: Ein Problem wurde behoben, bei dem Atalis Brüder euch dauerhaft verwurzelt haben.
</div>

<div id="intelliTXT">
  Feld der Toten: Die Conriocht-Bedrohung wirkt nicht länger &#8222;Raserei&#8220; auf Spieler.
</div>

<div id="intelliTXT">
  Der Astrologe in Tarantia ist nicht mehr nackt.
</div>

<div id="intelliTXT">
  Amphitheater von Karutonia: Der Mana/Ausdauer-Entzug des Verzehrers wurde leicht verringert.
</div>

<div id="intelliTXT">
  Der Wachposten am Eingang von Tortage lässt keine Sklaven mehr in die Stadt.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Gegenstände</strong><br />Quest-Objekte, die NPCs übergeben werden müssen, werden jetzt wie vorgesehen aus dem Inventar gelöscht, wenn der Spieler die Quest löscht.
</div>

<div id="intelliTXT">
  Der dunkle Ösenmantel kann wie vorgesehen nicht mehr gehandelt werden.
</div>

<div id="intelliTXT">
  Die Quest-Belohnung &#8222;Scipios Halskette des Schutzes&#8220; aus der Quest &#8222;Die Fratze des Bösen&#8220; ist jetzt beim Aufheben gebunden.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Handwerksfähigkeiten</strong><br />Für alle Materialrezepte zum Schlachtburgbau gibt es nun Kunstfertigkeit.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Quests</strong><br />Das Entfernen von &#8222;Fluch der Fliegen&#8220; durch Ur-Emnet tötet euch nicht mehr.
</div>

<div id="intelliTXT">
  Tortage &#8211; Ein Brief an den König II: Die Zwischensequenz mit dem Vulkanausbruch wurde geändert, sodass der Spieler versteckt bleibt, während Mithrelle die Blutphiole holt.
</div>

<div id="intelliTXT">
  <strong id="nointelliTXT"><br />Übersetzung</strong><br />Emotes: Nach Update 1.07 werden Emotes im Chatfenster nur noch über ihren englischen Namen aufgerufen werden können. Dies ist nur temporär und wird mit einem der nächsten Updates korrigiert. Im Emote Fenster (aufrufbar durch das drücken von Y) werden sie aber weiterhin übersetzt angezeigt.
</div>

<div id="intelliTXT">
  Eisenturm: Ein paar der Endgegner, die noch nicht übersetzt waren sind jetzt übersetzt.
</div>

<cite></cite>

 __

* * *

 __  



 

 [1]: news/age-of-conan-update-1-07-bringt-zahlreiche-neuerungen/seite-2