---
title: 'Call of Duty: Modern Warfare 2 – Steam ist Pflicht'
author: gamevista
type: news
date: 2009-10-18T15:54:18+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Der Entwickler <a href="http://www.infinityward.com" target="_blank">Infinity Ward</a> hat in einem offiziellen Podcast mitgeteilt das für <strong>Call of Duty: Modern Warfare 2</strong> der Onlinedienst Steam installiert sein muss.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Dies bestätigte der Community Manager Robert Bowling von <a href="http://www.infinityward.com" target="_blank">Infinity Ward</a>. Das heißt man wird **Modern Warfare 2** nur mit einem aktiven Steam Account und mit einer Internetverbindung spielen können. Weiterhin wurde die Anticheat Frage angesprochen. So wird als Anticheat Software nicht Punkbuster, sondern das Steamprogramm VAC benutzt. Robert Bowling bestätigte auch das keine Dedicated Server geplant seien. Das sind schlechte Nachrichten für Mods, die oft auf solchen Servern laufen. Anstatt den Dedicated Servern wird es Private Matches geben, die auf Servern im IWNet laufen.

 

* * *



* * *

 