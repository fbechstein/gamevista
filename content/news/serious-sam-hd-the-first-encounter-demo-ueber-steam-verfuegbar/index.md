---
title: 'Serious Sam HD: The First Encounter – Demo über Steam verfügbar'
author: gamevista
type: news
date: 2010-04-03T18:40:02+00:00
excerpt: '<p>[caption id="attachment_1431" align="alignright" width=""]<img class="caption alignright size-full wp-image-1431" src="http://www.gamevista.de/wp-content/uploads/2010/02/serious_sam_hd_small.png" border="0" alt="Serious Sam HD: The First Encounter" title="Serious Sam HD: The First Encounter" align="right" width="140" height="100" />Serious Sam HD: The First Encounter[/caption]Das Entwicklerteam <strong>Croteam </strong>hat eine Demoversion des Ego-Shooters Serious <a href="http://www.serioussam.com" target="_blank">Sam HD: The Frst Encounter</a> bereitgestellt. Die Demo ist über den Online-Dienst <a href="http://store.steampowered.com/app/41020/" target="_blank">Steam</a> zu beziehen. Wer immer noch überlegt ob er sich das Spiel kaufen soll, kann nun in die grafisch neu aufgelegte Version von Serious Sam einen Blick riskieren. Umfang der Demo ist ein Level das ausschließlich für Testversion erstellt wurde.</p> '
featured_image: /wp-content/uploads/2010/02/serious_sam_hd_small.png

---
Ob im Koop-Modus mit Freunden oder allein im Singleplayer-Modus, die Wahl steht euch frei.

<a href="http://store.steampowered.com/app/41020/" target="_blank">> Zur Serious Sam HD: The First Encounter &#8211; Demo</a>

 

<cite></cite>

   

* * *

   

