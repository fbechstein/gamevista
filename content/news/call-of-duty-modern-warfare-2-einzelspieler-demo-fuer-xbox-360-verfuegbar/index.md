---
title: 'Call of Duty: Modern Warfare 2 – Einzelspieler-Demo für Xbox 360 verfügbar'
author: gamevista
type: news
date: 2010-08-06T18:58:59+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Publisher <strong>Activision </strong>veröffentlichte heute die Einzelspieler-Demo zum Ego-Shooter <a href="http://www.modernwarfare2.com" target="_blank">Call of Duty: Modern Warfare 2</a> für die Xbox 360. Ab sofort steht damit die Testversion auf dem Xbox LIVE Marktplatz zum Download bereit. In der Singleplayer Mission Cliffhanger führt euch euer Auftrag durch einen Schneesturm zu Fuß oder auf einem Schneemobil.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Die Mission ist einer der beliebtesten und zeigt euch die eisigen Berge von Kasachstan.

 

   

* * *

   

