---
title: Electronic Arts – Das E3 Lineup für Los Angeles
author: gamevista
type: news
date: 2010-06-10T16:05:14+00:00
excerpt: '<p><img class="caption alignright size-full wp-image-1905" src="http://www.gamevista.de/wp-content/uploads/2010/06/ea_logo.jpg" border="0" align="right" width="100" height="100" />Natürlich darf <a href="http://www.ea.com" target="_blank">EA</a> als Publisher auf der Spielemesse E3 in Los Angeles nicht fehlen. Somit kündigte der Publisher heute die Spiele die auf der Messe zu sehen sein werden im Rahmen einer Pressemitteilung an. Als Highlight dürften auf jeden Fall<strong> Crysis 2</strong> und <strong>Dead Space 2</strong> gelten. Auch das neue <strong>Medal of Honor</strong> und der neuste Fifa-Teil dürften einige Fans in Verzückung geraten lassen.</p> '
featured_image: /wp-content/uploads/2010/06/ea_logo.jpg

---
Die Spielemesse E3 in Los Angeles findet vom 15. – 17. Juni statt.

Die komplette Liste des Lineups:

&#8211; Bulletstorm    
&#8211; Crysis 2    
&#8211; Dead Space 2    
&#8211; EA Sports Active 2    
&#8211; EA Sports MMA    
&#8211; FIFA 11    
&#8211; Harry Potter and the Deathly Hollows Part One    
&#8211; Hasbro Family Game Night 3    
&#8211; Madden NFL 11    
&#8211; Medal of Honor    
&#8211; Monopoly Streets    
&#8211; MySims: SkyHeroes    
&#8211; NBA Elite 11    
&#8211; NBA Jam    
&#8211; NCAA Football 11    
&#8211; NHL 11    
&#8211; NHL Slapshot    
&#8211; RISK: Factions    
&#8211; The Sims 3 (Konsole & Handhelds)    
&#8211; The Sims 3: Ambitions    
&#8211; Spare Parts    
&#8211; Tiger Woods PGA Tour 11 

iPad Spiele: 

&#8211; Boggle    
&#8211; Scrabble    
&#8211; Tetris    
&#8211; Need for Speed: Shift    
&#8211; Mirror’s Edge    
&#8211; Command & Conquer: Red Alert

   

* * *

   

