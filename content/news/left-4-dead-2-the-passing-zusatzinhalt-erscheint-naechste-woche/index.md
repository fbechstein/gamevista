---
title: 'Left 4 Dead 2: The Passing  – Zusatzinhalt erscheint nächste Woche'
author: gamevista
type: news
date: 2010-04-18T17:29:30+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Wie der Entwickler <strong>Valve </strong>auf dem offiziellen Left 4 Dead-Blog bestätigte, wird der herunterladbare Zusatzinhalt <strong>The Passing</strong> für <a href="http://www.l4d.com" target="_blank">Left 4 Dead 2</a> im Laufe der nächsten Woche erscheinen. Inhalt des Addons wird eine weitere Kampagne sein, die zwischen den Geschichten Dark Carnival und Dead Center spielt. Dabei werden die Helden des ersten Teils  auf die neue Gruppe stoßen und gemeinsam die Flucht vor den Zombies antreten.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Des Weiteren wird es zwei neue Waffen geben und eine weitere Boss-Zombie-Art wird hinzugefügt. Kostenpunkt sind 560 Microsoft für die Xbox 360 Fassung. PC Spieler dürfen auf eine kostenlose Version hoffen.

 

   

* * *

   



<span style="font-size: 11pt; line-height: 115%; font-family: "></p> 

<p>
  </span>
</p>