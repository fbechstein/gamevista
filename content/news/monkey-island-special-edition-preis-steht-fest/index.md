---
title: The Secret of Monkey Island Special Edition – Preis steht fest
author: gamevista
type: news
date: 2009-07-10T11:20:07+00:00
excerpt: '<p>[caption id="attachment_33" align="alignright" width=""]<img class="caption alignright size-full wp-image-33" src="http://www.gamevista.de/wp-content/uploads/2009/07/monkeyse_small.png" border="0" alt="Monkey Island Special Edition" title="Monkey Island Special Edition" align="right" width="140" height="100" />Monkey Island Special Edition[/caption]Am 15.07.2009 ist es endlich soweit. <a href="http://www.lucasarts.com/" target="_blank">Lucasarts</a> veröffentlicht die <strong>The Secret of Monkey Island Special Edition</strong>. Der Preis hierfür beträgt für die Xbox 360 800 Microsoft Points und für die Steam PC-Version $9,99. Bei der The Secret of Monkey Island Special Edition handelt es sich um 1:1 Remake des Originals von 1990 mit aufgebohrter Grafik und neuem Interface. Der Spieler soll in der Lage sein zwischen dem neuem und dem alten Interface hin- und herzuwechseln.</p>'
featured_image: /wp-content/uploads/2009/07/monkeyse_small.png

---
In unserer Videosektion findet Ihr ein [Video zu der Special Edition][1], welches Euch die Unterschiede zwischen der alten und er neuen Version aufzeigt. 

Vor einigen Tagen wurde bereits der erste Teil der fünfteiligen **Tales of Monkey Island**-Reihe ([wir berichteten][2]) veröffentlicht. Hierzu steht in unserer Downloadsektion die Demoversion zur Verfügung und in der Videosektion ein neuer [Trailer][3].

[> Video zu The Secret of Monkey Island Special Edition ansehen][1]  
[> Trailer zu Tales of Monkey Island ansehen][3]  
[> Demo zu Tales of Monkey Island herunterladen][4]







 [1]: videos/item/root/the-secret-of-monkey-island-special-edition
 [2]: index.php?option=com_content&view=article&id=101:tales-of-monkey-island-screenshoots-und-gameplay-video&catid=34:news&Itemid=73
 [3]: videos/item/root/tales-of-monkey-island-trailer
 [4]: downloads/demos/item/root/tales-of-monkey-island-launch-of-the-screaming-narwhal-demo