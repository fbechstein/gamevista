---
title: 'Divinity 2: Flames of Vengeance – Erstes Add On für Xbox und PC bestätigt'
author: gamevista
type: news
date: 2010-03-02T17:09:46+00:00
excerpt: '<p>[caption id="attachment_310" align="alignright" width=""]<img class="caption alignright size-full wp-image-310" src="http://www.gamevista.de/wp-content/uploads/2009/08/divinity2.png" border="0" alt="Divinity 2" title="Divinity 2" align="right" width="140" height="100" />Divinity 2[/caption]Der Publisher<strong> dtp Entertainment</strong> hat im Rahmen einer Pressemitteilung offiziell das erste Erweiterungspaket für das Rollenspiel <a href="http://www.divinity2.com/ " target="_blank">Divinity 2</a> angekündigt. Mit dem Namen <a href="http://www.divinity2.com/ " target="_blank">Divinity 2: Flames of Vengeance</a> führt das Addon die Geschichte um den Drachenritter fort. Neben neuen Inhalten mit bis zu 30 neuen Quests bzw. 15 Stunden Spielspaß, wurde auch die Grafik-Engine überarbeitet.</p> '
featured_image: /wp-content/uploads/2009/08/divinity2.png

---
Dies führt zu einer besseren Grafik und Performance.    
Publisher **dtp entertainment** und **Larian** veröffentlichen <a href="http://www.divinity2.com/ " target="_blank">Divinity 2: Flames of Vengeance</a> für PC, sowie als kostenpflichtigen Download auf dem Xbox LIVE™ Marketplace im August 2010.

 

 

   

* * *

   



 