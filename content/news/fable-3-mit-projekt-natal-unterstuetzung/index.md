---
title: Fable 3 – mit Projekt Natal Unterstützung?
author: gamevista
type: news
date: 2009-10-05T19:54:52+00:00
excerpt: '<p>[caption id="attachment_821" align="alignright" width=""]<img class="caption alignright size-full wp-image-821" src="http://www.gamevista.de/wp-content/uploads/2009/10/fable3_small.jpg" border="0" alt="Fable 3" title="Fable 3" align="right" width="140" height="100" />Fable 3[/caption]Der Entwickler Peter Molyneux, vom Entwicklerstudio <a href="http://www.lionhead.com/" target="_blank" rel="nofollow">Lionhead </a>hat in einem Interview angedeutet, das sie für <strong>Fable 3</strong> wohl eine Projekt Natal Unterstützung mit anbieten werden.</p> '
featured_image: /wp-content/uploads/2009/10/fable3_small.jpg

---
&#8222;_Lasst es mich gerade heraus sagen: Ich habe nicht geäußert, dass wir in &#8218;Fable 3&#8216; kein Natal benutzen werden. Alles, was ich wirklich gesagt habe, ist, dass es eine Controller-Erfahrung ist_“, so Molyneux weiter. „_Das bedeutet nicht, dass es kein Natal geben wird. (&#8230;) Denkt Ihr ernsthaft, wo Ihr mich doch kennt, ich würde soetwas wie Natal nicht nutzen wollen? Ich meine, das ist doch verrückt“,_ so Molyneux. Im Rahmen der gamescom 2009 in Köln wurde der nunmehr dritte Teil der Rollenspielreihe **Fable** angekündigt. **Fable 3** soll exklusiv für Xbox 360 im Herbst 2010 erscheinen.

 

 

* * *



* * *

 