---
title: Tropico 3 – Gold-Edition geplant
author: gamevista
type: news
date: 2010-09-14T17:21:55+00:00
excerpt: '<p>[caption id="attachment_2232" align="alignright" width=""]<img class="caption alignright size-full wp-image-2232" src="http://www.gamevista.de/wp-content/uploads/2010/09/smallt4.jpg" border="0" alt="Tropico 3" title="Tropico 3" align="right" width="140" height="100" />Tropico 3[/caption]Publisher <strong>Kalypso Media</strong> kündigte heute die offizielle Gold-Edition des Aufbauspiels <a href="http://www.tropico3.com" target="_blank">Tropico 3</a> an. Die Sonderedition wird neben dem Hauptspiel auch das Absolute Power-Addon enthalten. Derzeit ist geplant die Gold-Edition am 23. September 2010 in den Handel zu bringen.</p> '
featured_image: /wp-content/uploads/2010/09/smallt4.jpg

---
Kostenfaktor werden 29,99 Euro sein.

 

   

* * *

   

