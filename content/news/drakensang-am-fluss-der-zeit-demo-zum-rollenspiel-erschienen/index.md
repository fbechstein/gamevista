---
title: 'Drakensang: Am Fluss der Zeit – Demo zum Rollenspiel erschienen'
author: gamevista
type: news
date: 2009-12-22T12:14:01+00:00
excerpt: '<p>[caption id="attachment_437" align="alignright" width=""]<img class="caption alignright size-full wp-image-437" src="http://www.gamevista.de/wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg" border="0" alt="Drakensang: Am Fluss der Zeit" title="Drakensang: Am Fluss der Zeit" align="right" width="140" height="100" />Drakensang: Am Fluss der Zeit[/caption]Der Publisher <a href="http://www.am-fluss-der-zeit.de/index.php" target="_blank">dtp Entertainment</a> hat heute die Demo zum Rollenspiel <strong>Drakensang: Am Fluss der Zeit</strong> veröffentlicht. Die 1,4 Gigabyte große Testversion soll nun erste Einblicke zum Nachfolger des Erfolgs-RPGs vermitteln. So kann der Spieler die Rolle eines Kriegers übernehmen der in der Stadt Nadoret einige Missionen absolvieren muss.</p> '
featured_image: /wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg

---
In typischer Das Schwarze Auge-Manier erwartet euch hier eine neue spannende Story, die für alle Anhänger des DAS-Universums und Drakensang-Fans ein Muss ist.  Die Demo könnt ihr wie immer kostenlos bei uns vom Server Downloaden.

[> Zum Drakensang: Am Fluss der Zeit &#8211; Demo Download][1]

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/drakensang-am-fluss-der-zeit-demo