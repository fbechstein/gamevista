---
title: 'Steam Weekend Deal – Diesmal mit Crysis- und Overlord-Reihe günstiger'
author: gamevista
type: news
date: 2009-11-13T11:06:07+00:00
excerpt: '<p>[caption id="attachment_1036" align="alignright" width=""]<img class="caption alignright size-full wp-image-1036" src="http://www.gamevista.de/wp-content/uploads/2009/11/overlord_small.png" border="0" alt="Overlord" title="Overlord" align="right" width="140" height="100" />Overlord[/caption]Der Onlinedienst Steam bietet dieses Wochenende im Rahmen des „<a href="http://store.steampowered.com/sub/2464/" target="_blank">Steam Weekend Deal</a>“ wieder neue Angebote an. Demnach kommen Actionfans mit der <a href="http://store.steampowered.com/sub/989/" target="_blank">Crysis Maximum Edition</a> voll auf ihre kosten. Diese beinhaltet die Shooter <strong>Crysis </strong>und <strong>Crysis Warhead </strong>und wird bis Montag nur 24,98 Euro kosten.</p> '
featured_image: /wp-content/uploads/2009/11/overlord_small.png

---
Wer einen dieser Titel schon besitzt kann auch **Crysis** für 9,99 Euro oder **Crysis Warhead** für 14,99 Euro einzeln erwerben. Wer sich schon immer mal am Action-Adventure **Overlord** und **Overlord 2** probieren wollte bekommt mit einem Paket die Chance beide Titel plus Addon **Overlord: Raising Hell** günstiger zu kaufen. So kostet dieses <a href="http://store.steampowered.com/sub/2464/" target="_blank">Bundle </a>8,75 Euro bei Steam. Natürlich können sie auch diese Titel einzeln erwerben.

 

* * *



* * *