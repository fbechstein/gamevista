---
title: 'Batman: Arkham Asylum – Neuer Inhalt kostenlos!'
author: gamevista
type: news
date: 2009-08-23T12:55:01+00:00
excerpt: '<p>[caption id="attachment_453" align="alignright" width=""]<img class="caption alignright size-full wp-image-453" src="http://www.gamevista.de/wp-content/uploads/2009/08/batman_arkham_asylum_small.jpg" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Batman: Arkham Asylum[/caption]Nächste Woche erscheint bereits das nächste Abenteuer des Dunklen Ritters. <strong>Batman: Arkham Asylum</strong> wird demnach am Freitag den 28. August für Xbox 360 und PlayStation 3 erscheinen. Um der Vorfreude noch eins draufzusetzen entschied sich Publisher <a href="http://www.eidos.de/">Eidos </a>kurz nach Release von <strong>Batman: Arkham Asylum</strong> ein kostenlosen Download Inhalt anzubieten.</p> '
featured_image: /wp-content/uploads/2009/08/batman_arkham_asylum_small.jpg

---
Worum es sich bei dem Angebot genau handelt ist bisher aber noch unklar. Denkbar wären neue Waffen oder einige neue Kostüme.

 

* * *



* * *

 