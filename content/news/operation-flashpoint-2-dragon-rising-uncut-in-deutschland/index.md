---
title: 'Operation Flashpoint 2: Dragon Rising – Uncut in Deutschland!'
author: gamevista
type: news
date: 2009-07-29T11:19:52+00:00
excerpt: '<p>[caption id="attachment_198" align="alignright" width=""]<img class="caption alignright size-full wp-image-198" src="http://www.gamevista.de/wp-content/uploads/2009/07/opf2_small.jpg" border="0" alt="Operation Flashpoint 2: Dragon Rising" title="Operation Flashpoint 2: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint 2: Dragon Rising[/caption]Sehr gute Nachrichten für die Freunde des Taktikshooters <strong>Operation Flashpoint 2: Dragon Rising</strong>. <a href="http://www.codemasters.de" target="_blank" title="codemasters">Codemasters </a>bestätigte nun Offiziell das die Unterhaltungssoftware Selbstkontrolle ihren Titel begutachtet hat und er für nicht zu blutig befunden wurde. Das heißt genau, die USK wird dem Kriegsshooter das Siegel „Freigegeben ab 16 Jahren“ <span> </span>geben.</p> '
featured_image: /wp-content/uploads/2009/07/opf2_small.jpg

---
Was das bedeutet? Ihr könnt **Operation Flashpoint 2: Dragon Rising** in Deutschland völlig ungeschnitten erhalten. Der Releasetermin wird der 08. Oktober 2009 sein. **Operation Flashpoint 2: Dragon Rising** wird für Xbox 360, Playstation 3 und für den PC erähltlich sein.[  
][1] 







 [1]: videos/item/root/risen-nightwish-trailer