---
title: R.U.S.E. – Pro Gamer Edition mit Xai Steelseries Laser Maus
author: gamevista
type: news
date: 2010-04-01T16:41:06+00:00
excerpt: '<p>[caption id="attachment_1651" align="alignright" width=""]<img class="caption alignright size-full wp-image-1651" src="http://www.gamevista.de/wp-content/uploads/2010/04/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Der Publisher <strong>Ubisoft </strong>hat im Rahmen einer Pressemitteilung verkündet, dass es eine Pro Gamer Edition zum Strategietitel <a href="http://www.ruse.us.ubi.com" target="_blank">R.U.S.E.</a> geben wird. Diese Sonderedition enthält neben dem Spiel eine Steelseries Xai Laser Maus im weißen Design. Die Maus zeichnet sich unter anderem durch 7 frei programmierbare Tasten und 5 Profilen aus.</p> '
featured_image: /wp-content/uploads/2010/04/ruse_small.jpg

---
Ein vorkonfiguriertes R.U.S.E.-Profil wird auch mit dabei sein.  Ab dem 2. Juni wird <a href="http://www.ruse.us.ubi.com" target="_blank">R.U.S.E.</a> im Handel erscheinen.

 

<p style="text-align: center;">
  <img class=" size-full wp-image-1652" src="http://www.gamevista.de/wp-content/uploads/2010/04/ruseprogamer.jpg" border="0" width="479" height="466" srcset="http://www.gamevista.de/wp-content/uploads/2010/04/ruseprogamer.jpg 479w, http://www.gamevista.de/wp-content/uploads/2010/04/ruseprogamer-300x292.jpg 300w" sizes="(max-width: 479px) 100vw, 479px" />
</p>

<cite></cite>

   

* * *

   

