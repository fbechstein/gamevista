---
title: 'Splinter Cell: Conviction – Spielbar im Splitscreen-Modus'
author: gamevista
type: news
date: 2010-01-11T18:38:15+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Der Publisher <a href="http://www.splintercell.us.ubi.com" target="_blank">Ubisoft</a> hat nun offiziell bestätigt das der Taktik-Shooter <strong>Splinter Cell: Convition</strong> einen neuen Spielmodus mit sich bringen wird. Neben dem bereits angekündigten Online-Koop-Modus, wird es einen Splitscreen-Modus geben, der Offline zusammen an der Konsole spielbar ist.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
**Splinter Cell: Conviction** erscheint am 25. Februar 2010 für PC und Xbox 360. Bereits Ende Januar 2010 soll es eine spielbare Demo geben.

 

<cite></cite>

* * *



* * *

 