---
title: 'PT Boats: Knights of the Sea – Demo zum Downloaden'
author: gamevista
type: news
date: 2010-01-03T18:00:30+00:00
excerpt: '<p>[caption id="attachment_1255" align="alignright" width=""]<img class="caption alignright size-full wp-image-1255" src="http://www.gamevista.de/wp-content/uploads/2010/01/ptboats.jpg" border="0" alt="PT Boats: Knights of the Sea" title="PT Boats: Knights of the Sea" align="right" width="140" height="100" />PT Boats: Knights of the Sea[/caption]Die Demo von <strong>PT Boats: Knights of the Sea</strong> führt euch in die Gewässer des 2. Weltkriegs. Hier könnt ihr Schiffe der Allierten oder Axenmächte steuern. Die Demoversion ist 840 Megabyte groß und enthält drei Tutorials und erste Missionen aus der Kampagne.</p> <p> </p> '
featured_image: /wp-content/uploads/2010/01/ptboats.jpg

---
[> Zur PT Boats: Knights of the Sea &#8211; Demo][1]

 

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/pt-boats-knights-of-the-sea-demo