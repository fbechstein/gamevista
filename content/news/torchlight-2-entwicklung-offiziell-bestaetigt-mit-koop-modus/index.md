---
title: Torchlight 2 – Entwicklung offiziell bestätigt mit Koop-Modus!
author: gamevista
type: news
date: 2010-08-04T19:21:01+00:00
excerpt: '<p>[caption id="attachment_2094" align="alignright" width=""]<img class="caption alignright size-full wp-image-2094" src="http://www.gamevista.de/wp-content/uploads/2010/08/torchlight2.jpg" border="0" alt="Torchlight 2" title="Torchlight 2" align="right" width="140" height="100" />Torchlight 2[/caption]Entwickler <strong>Runic Games</strong> hat nun offiziell bestätigt den nächsten Teil vom überaus erfolgreichen Action-Rollenspiel Torchlight zu entwickeln. Damit wird <a href="http://www.torchlight2game.com/" target="_blank">Torchlight 2</a> voraussichtlich schon im ersten Quartal 2011 veröffentlicht. Der Entwickler verpasst dem zweiten Teil auch den von vielen Fans gewünschten Koop-Modus.</p> '
featured_image: /wp-content/uploads/2010/08/torchlight2.jpg

---
Damit könnt ihr, ähnlich wie im Genrekollegen Diablo, zusammen mit Freunden spielen.  Außerdem werden Editor, Mod-Support, ein neues Interface mitgeliefert. Auch werden diesmal Missionen an der Erdoberfläche vorhanden sein.

 

   

* * *

   

