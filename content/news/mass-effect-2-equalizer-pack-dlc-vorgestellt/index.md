---
title: Mass Effect 2 – Equalizer Pack DLC vorgestellt
author: gamevista
type: news
date: 2010-05-04T16:37:51+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Entwickler <strong>BioWare </strong>bestätigte nun den erwarteten neuen Download-Inhalt für das Rollenspiel <a href="http://masseffect.bioware.com/info/dlc/" target="_blank">Mass Effect 2</a> auf ihrer offiziellen <a href="http://masseffect.bioware.com/info/dlc/" target="_blank">Webseite</a>. Das Zusatzpaket hört auf den Namen <strong>Equalizer Pack</strong> und wird heute im Laufe des Tages veröffentlicht. Inhalt des neuen Download-Contents sind drei neue Teile für eure Rüstung.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Zum einen wäre da der Capacitor Helm. Dieser verkürzt die Aufladezeit  von Shepards Schild. Mit dem Archor Visor kann Shepard seine Fähigkeiten öfter nutzen. Die Inferno Rüstung erhöht den Schaden unseres Helden. Kostenpunkt des Pakets sind 160 BioWare-Punkte.

Weitere Details findet ihr [hier][1].

 

   

* * *

* * *

 [1]: http://masseffect.bioware.com/info/dlc/