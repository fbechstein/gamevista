---
title: Borderlands – Zombie Island DLC auch für PC verfügbar
author: gamevista
type: news
date: 2009-12-10T09:27:14+00:00
excerpt: '<p>[caption id="attachment_1163" align="alignright" width=""]<img class="caption alignright size-full wp-image-1163" src="http://www.gamevista.de/wp-content/uploads/2009/12/smalllogo.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Für den Ego-Shooter <a href="hot-spots/review-borderlands-pc">Borderlands</a>, vom <a href="http://www.borderlandsthegame.com" target="_blank">Entwicklerstudio Gearbox</a>, wurde nun der erste Zusatzinhalt für PC veröffentlicht. Konsolenspieler kamen schon seit dem 24. November in den Genuß des DLCs. So wurde ab gestern Abend <strong>The Zombie Island of Dr. Ned</strong> auf dem Online-Dienst Steam für 7,99 EUR angeboten.</p> '
featured_image: /wp-content/uploads/2009/12/smalllogo.jpg

---
Der Download-Content bietet dem Spieler neben fünf neue Gebiete und einige zusätzliche Missionen, eine neue Stadt namens Jakobs Cave. Diese wird von jeder Menge Untoten besiedelt. Grund dafür dürfte das misslungene Experiment vom Wissenschaftler Dr. Ned sein. Logisch das der Spieler sich dem annimmt und ein Heilmittel für die Plage suchen soll.

 





