---
title: Gray Matter – Adventure erscheint auch für Xbox
author: gamevista
type: news
date: 2010-04-28T20:55:12+00:00
excerpt: '<p>[caption id="attachment_1734" align="alignright" width=""]<img class="caption alignright size-full wp-image-1734" src="http://www.gamevista.de/wp-content/uploads/2010/04/graymatter.jpg" border="0" alt="Gray Matter" title="Gray Matter" align="right" width="140" height="100" />Gray Matter[/caption]Der Publisher <strong>dtp entertainment</strong> hat heute offiziell bestätigt, dass das Jane Jensen Adventure <a href="http://www.graymatter-game.com" target="_blank">Gray Matter</a> definitiv auch für die Konsole Xbox 360 entwickelt wird. Bisher war nur eine PC-Version geplant. Beide Fassungen sollen zeitgleich im vierten Quartal 2010 erscheinen.</p> '
featured_image: /wp-content/uploads/2010/04/graymatter.jpg

---
Der Entwickler Wizarbox arbeitet derzeit  an der Anpassung der Steuerung des Xbox 360 Controllers. Inhaltlich sollen sich beide Versionen gleichen.

 

 

   

* * *

   

