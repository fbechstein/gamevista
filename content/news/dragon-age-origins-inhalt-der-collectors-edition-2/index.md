---
title: 'Dragon Age: Origins – Inhalt der Collectors Edition'
author: gamevista
type: news
date: 2009-08-13T11:14:19+00:00
excerpt: '<p class="MsoNormal">[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Entwickler <a href="http://www.bioware.com/" target="_blank">Bioware</a> hat heute offiziell bekannt gegeben das es eine Collectors Edition für ihr Rollenspiel <strong>Dragon Age: Origins</strong> geben wird. Mit der Meldung gaben sie weiterhin auch gleich noch den Inhalt bekannt.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
  * Steelbook
  * Weltkarte aus Stoff
  * digitaler Download des Soundtracks
  * DVD mit Making of, Gametrailer, Wallpapers & Strategie-Tipps
  * DLC &#8220;Stone Prophet&#8220; enthält einen Anzug und bietet zusätzliche Spielzeit sowie neue Gebiete 
  * Einen Downloadcode für die &#8220;Blood Dragon Armor&#8220;, die sowohl in Dragon Age als auch in Mass Effect 2 einsetzbar sein wird.







 

 