---
title: 'Need for Speed: Shift – Demo erst nach dem Release'
author: gamevista
type: news
date: 2009-09-06T14:34:36+00:00
excerpt: '<p>[caption id="attachment_124" align="alignright" width=""]<img class="caption alignright size-full wp-image-124" src="http://www.gamevista.de/wp-content/uploads/2009/07/shift_small.jpg" border="0" alt="Need for Speed: Shift" title="Need for Speed: Shift" align="right" width="140" height="100" />Need for Speed: Shift[/caption]Na das sind ja keine guten Nachrichten zum Wochenende. <a href="http://www.ea.com/games/need-for-speed-shift">Electronic Arts</a> bestätigte nun das die Demo zum Rennspiel <strong>Need for Speed: Shift</strong> erst nach der Veröffentlichung des Hauptspiels  erscheint. Der Releasetermin ist auf den 17. September gelegt. Einen genauen Termin für die Demo gibt es bisher aber nicht.</p> '
featured_image: /wp-content/uploads/2009/07/shift_small.jpg

---
[EA][1] wird wohl erstmal alle Kräfte dafür aufbringen das Spiel pünktlich zum Veröffentlichungstermin fertig zu stellen. Das [EA][1] hier einen Ruf zu verlieren hat dürfte klar sein. Denn **Need for Speed: Shift** stellt einen Neuanfang der Serie dar. Wir hoffen das sie beides schaffen. Ein gutes Spiel zu Entwickeln und den Fans auch eine gute Demo präsentieren zu können.

 

 

* * *



* * *

 [1]: http://www.ea.com/games/need-for-speed-shift