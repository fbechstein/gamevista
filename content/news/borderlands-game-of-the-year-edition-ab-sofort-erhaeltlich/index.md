---
title: Borderlands – Game of the Year Edition ab sofort erhältlich
author: gamevista
type: news
date: 2010-10-13T19:13:44+00:00
excerpt: '<p>[caption id="attachment_2342" align="alignright" width=""]<img class="caption alignright size-full wp-image-2342" src="http://www.gamevista.de/wp-content/uploads/2010/10/smallcoop.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]<strong>Take 2</strong> hat den offiziellen Verkaufsstart der Game of the Year Edition des Ego-Shooters <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands </a>verkündet. Das Spiel inklusive aller veröffentlichten DLC`s sind ab sofort für Xbox 360, PlayStation 3 und PC verfügbar. Neben den Spielinhalten wird auch ein Zugangscode für den Duke Nukem Forever First Access Club beiliegen.</p> '
featured_image: /wp-content/uploads/2010/10/smallcoop.jpg

---
Mitglieder dürfen sich über spezielle Boni und dem verfrühten Zugang zur Duke Nukem Forever Demo freuen.

 

   

* * *

   



 