---
title: Pro Evolution Soccer 2010 – Demo Termin bekannt
author: gamevista
type: news
date: 2009-09-16T17:33:55+00:00
excerpt: '<p>[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]Endlich zieht Publisher <a href="http://www.de.games.konami-europe.com">Konami </a>mit der Veröffentlichung ihrer Demo für das Fußballspiel <strong>Pro Evolution Soccer 2010</strong> nach. Nachdem EA nun ihre <strong>Fifa 10 </strong>Demo bereits vorgestellt hat, wird es für <strong>PES 2010</strong> ab morgen den 17. September auch soweit sein. Die Probierversion wird dann bei uns im Download Bereich verfügbar sein.</p> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
Unter anderem könnt ihr im Freundschaftsspiel mit FC Barcelona oder FC Liverpool antreten. Für die Länderspielvariante werden Deutschland, Frankreich, Italien und Spanien zur Auswahl stehen.  
Wir wünschen viel Spass beim antesten und der jährlichen Entscheidung. Fifa oder PES?

 

* * *



* * *