---
title: Nintendo Wii – Im kleinen schwarzen
author: gamevista
type: news
date: 2009-10-20T15:51:02+00:00
excerpt: '<p>[caption id="attachment_911" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/10/nixl_wii_black.png"><img class="caption alignright size-full wp-image-911" src="http://www.gamevista.de/wp-content/uploads/2009/10/nixl_wii_black_small.png" border="0" alt="Nintendo Wii - Black Edition" title="Nintendo Wii - Black Edition" align="right" width="140" height="100" /></a>Nintendo Wii - Black Edition[/caption]Endlich bekommt die <strong>Nintendo Wii</strong> einen neuen Anstrich. Gerade wenn man bedenkt wie farbenfroh der Entwickler <a href="http://www.nintendo.co.uk/NOE/en_GB/news/2009/limited_edition_black_wii_bundle_announced_for_europe_including_wii_sports_resort_and_wii_motionplus_14833.html" target="_blank">Nintendo</a> mit dem Gamecube war und mit dem Nintendo DS ist.  So freut es das die NextGen Konsole <strong>Wii</strong> nun in der Farbe schwarz ab dem 20. November erhältlich sein wird.</p> '
featured_image: /wp-content/uploads/2009/10/nixl_wii_black_small.png

---
In England übrigens bereits schon ab dem 6. November. Die Limited Edition der schwarzen **Wii** wird neben der Konsole selbst noch ein Bundle enthalten. Das umfasst neben **Wii Sports Resort** und dem originalen **Wii Sports**, einen Hardware Zusatz **Wii MotionPlus**. Dieser soll präzise Eingaben erlauben. Auch wird es ein **Nunchuck** in schwarz geben. Weiterhin wird die Zusatzhardware auch einzeln verfügbar sein.

* * *



* * *