---
title: Fairytale Fights – Interview mit Martin Janse
author: gamevista
type: news
date: 2009-10-22T21:38:59+00:00
excerpt: '<p>[caption id="attachment_928" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/10/ff_nackterkaiser.jpg" rel="lightbox[fairytalefights1]"><img class="caption alignright size-full wp-image-928" src="http://www.gamevista.de/wp-content/uploads/2009/10/small_ff_nackterkaiser.jpg" border="0" alt="Fairytale Fights" title="Fairytale Fights" align="right" width="140" height="100" /></a>Fairytale Fights[/caption] <a href="http://playlogic.com/" target="_blank">Playlogic</a> kündigte einen ganz besonderen Titel im niedrig Preissektor an: <strong>Fairytale Fights</strong>. Wer schon immer mal Märchen ganz anders erleben wollte oder auf Cartoons wie Itchy und Scratchy von den Simpsons oder Happy Tree Friends steht, wird an diesem Spiel wahrscheinlich auf seine Kosten kommen.  Einer der Entwicker stand uns nun Rede und Antwort um kommendem Hack ‘n’ Slash Adventure.</p> '
featured_image: /wp-content/uploads/2009/10/small_ff_nackterkaiser.jpg

---
 **Bitte stell dich unseren Lesern kurz vor.** 

_Martin Janse:_ Mein Name ist Martin Janse. Ich bin Executive Producer von Fairytale Fights.

**Ihr habt bis jetzt nicht mit Märchen gearbeitet. Warum entwickelt ihr ein Spiel ausgerechnet mit diesem Setting?**

_Martin Janse:_ Vor 3 Jahren hatten wir die Idee etwas Verrücktes zu entwickeln mit Märchencharakteren. Jetzt haben wir dieses total coole Hack ‘n’ Slash Adventure gemacht und wir in der Playlogic Game Fabrik sind sehr stolz darauf, was wir produziert haben. Die gute Sache bei Märchen ist, dass jeder damit etwas anfangen kann und wir nicht viel von der Hintergrundgeschichte erzählen müssen.

**Märchen wurden geschrieben für Kinder. Jetzt wollt ihr in Deutschland ein Spiel mit einer FSK-18-Wertung verkaufen. Glaubt ihr, dass es viele Menschen geben wird, die gerne im Märchen-Setting rummetzeln?  
**   
_Martin Janse:_ Magst du South Park? Oder Happy Tree Friends? Falls es so ist, stehen die Chancen gut, dass du dieses Spiel lieben wirst. Die bunte Welt und die Cartoon-Charakter gemischt mit cooler und vor allem unterhaltsamer Gewalt ist einfach richtig witzig.



<a href="http://www.gamevista.de/wp-content/uploads/2009/10/ff_rot.jpg" rel="lightbox[fairytalefights2]"><img class="caption alignright size-full wp-image-931" src="http://www.gamevista.de/wp-content/uploads/2009/10/small_ff_rot.jpg" border="0" alt="Ist sie nicht süß?" title="Ist sie nicht süß?" align="right" width="140" height="100" /></a>**Warum habt ihr einen Comic-Grafikstil gewählt?**

_Martin Janse:_ Wir mögen Farben und die Gewalt im Spiel ist inspirieret von Cartoons wie Ren & Stimpy, Happy Tree Friends und Itchy und Scratchy aus Simpsons, daher war es nahe liegend.

 **Was ist dein Liebelinscharakter und deine Lieblingswaffe und wieso?** 

_Martin Janse:_ Rotkäppchen oder der nackte Kaiser … ich kann mich einfach nicht entscheiden! Nun gut, sagen wir Rotkäppchen, weil sie herumläuft wie ein streitsüchtiger Ninja! Meine Lieblingswaffe ist wahrscheinlich der Säuretrank, welchen man entweder trinkt und dann tödliches Acid spuken kann, wie eine Granate wirft oder eine heldenhafte Attacke macht und alle Gegner auf dem Schirm „auflöst“. _(Kommentar des Autors: In diesem Moment, freue ich mich schon sehr auf meinen Test der PC-Version Anfang 2010)_

<a href="http://www.gamevista.de/wp-content/uploads/2009/10/ff_gewalt.jpg" rel="lightbox[fairytalefights1]"><img class="caption alignright size-full wp-image-933" src="http://www.gamevista.de/wp-content/uploads/2009/10/small_ff_gewalt.jpg" border="0" alt="An Blut und Gewalt wird nicht gespart." title="An Blut und Gewalt wird nicht gespart." align="right" width="140" height="100" /></a> **Kannst du uns etwas über die 20 verschiedenen Level erzählen?**

_Martin Janse:_ Es gibt vier Hauptgebiete im Spiel. Beispiele dafür sind “Das Holzfällerland”, “Die Süßigkeitenburg” (unsere Version der Geschichte von Hansel und Gretel) und “Das gigantische Haus”.



**   
Es gibt also 20 verschiedene Levels, die auf vier verschiedene Hauptgebiete basieren. Da stellt sich uns natürlich die Frage, ob es eine Geschichte gibt, die die verschiedenen Märchen verbindet? Was kannst du uns über die Geschichte / die Geschichten und die Aufgaben, die der Spieler gestellt bekommt verraten?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/10/ff_sss.jpg" rel="lightbox[fairytalefights1]"><img class="caption alignright size-full wp-image-935" src="http://www.gamevista.de/wp-content/uploads/2009/10/small_ff_sss.jpg" border="0" alt="Abwechslungsreich wirken die Level auf jeden Fall." title="Abwechslungsreich wirken die Level auf jeden Fall." align="right" width="140" height="100" /></a> _Martin Janse:_ Die Hauptgeschichte ist, dass ein böser Charakter das Ansehen / den Ruf / die Berühmtheit von vier spielbaren Charakteren geklaut hat. Es liegt bei unseren Helden ihren Ruf wieder herzustellen. In ihrem ersten Quest müssen sie dem magischen Kessel der drei Bären wieder finden, welcher von den Holzfällern gestohlen wurde. _(Kommentar des Autors: Warum glaube ich, dass ich die Kettensäge der Holzfäller sicher auch mögen werde?)_

**Habt ihr schon über DLC nachgedacht?**

_Martin Janse:_ Ja, wir werden jede Menge coole Sachen anbieten. Wir dachten dabei an neue Waffen, spielbare Charaktere, player vs payer Arenas und sogar ganz neue Level. Für nette DLC Überraschungen registriert euch bitte auf unserer <a href="www.fairytalefights.com" target="_blank">Website</a>.

**Wir bedanken uns bei dir und wünschen dir und dem Entwicklerteam weiterhin viel Erfolg.**

 

Natürlich werden wir das Spiel **Fairytale Fights** auch testen. Leider erscheint die PC-Version erst Anfang 2010 und so müssen sich die PC-Spieler die Zeit bis dahin wohl mit dem <a href="www.fairytalefights.com/minigame" target="_blank">Minigame</a> zum Spiel vertreiben. Anfang 2010 erscheint, dann natürlich auch unserer Test des Spiels.

 





