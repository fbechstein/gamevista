---
title: Fifa 11 – EA gibt Termin bekannt
author: gamevista
type: news
date: 2010-07-07T14:01:36+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="Fifa 11" title="Fifa 11" align="right" width="140" height="100" />Fifa 11[/caption]Publisher <strong>Electronic Arts</strong> hat den offiziellen Veröffentlichungstermin für den diesjährigen FIFA-Teil bekannt gegeben. <a href="http://fifa.easports.com" target="_blank">FIFA 11</a> erscheint am 30. September für PlayStation 3, Xbox 360, Nintendo Wii, PC, PlayStation 2, Nintendo DS und PSP. Als umfassendste Neuerung dürfte das Creation Center gelten.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Hiermit könnt ihr erstellte Spieler bzw. Mannschaften mit Freunden teilen, auf die eigene Konsole herunterladen und mit dem Team Online in Turnieren spielen. Zudem lassen sich mit der webbasierten Anwendung auf easportsfootball.de eigene virtuelle Fußballspieler oder ein eigenes Dreamteam erstellen. Mit einem neuen Audiotool wird es außerdem möglich sein eigene Fangesänge und Spieleransangen aufzunehmen.

 

   

* * *

   

