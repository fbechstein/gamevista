---
title: Electronic Arts – Publisher präsentiert E3 Lineup
author: gamevista
type: news
date: 2010-05-12T17:11:42+00:00
excerpt: '<p>[caption id="attachment_1264" align="alignright" width=""]<img class="caption alignright size-full wp-image-1264" src="http://www.gamevista.de/wp-content/uploads/2010/01/ea_logo.jpg" border="0" alt="Electronic Arts" title="Electronic Arts" align="right" width="100" height="100" />Electronic Arts[/caption]Nachdem der Publisher <strong>JoWood </strong>bereits am gestrigen Tage sein E3 Lineup vorgestellt hat, zieht der Publisher <a href="http://www.ea.com/" target="_blank">Electronic Arts</a> nun nach. Dieser stellte heute eine Liste mit den Spielen vor die auf der Messe in Los Angeles präsentiert werden sollen.</p> '
featured_image: /wp-content/uploads/2010/01/ea_logo.jpg

---
Dabei jagt ein Highlight das nächste. Vom Online-Rollenspiel **Star Wars: The Old Republic**, über den Ego-Shooter **Crysis 2**, bis hin zum Gruselspiel **Dead Space 2**. Wir sind gespannt auf viele neue Details und Videos.

Das E3 Lineup sieht wie folgt aus:

  * BulletStorm
  * Crysis 2
  * Dead Space 2
  * Die Sims 3
  * FIFA 11
  * Medal of Honor
  * neues Need for Speed-Spiel
  * neues Harry Potter-Spiel
  * neue Hasbro-Spiele
  * neue Microsoft- und Sony-Motion Control-Spiele
  * NBA Live 11
  * NHL 11
  * Star Wars: The Old Republic 
  * Tiger Woods PGA Tour 11

   

* * *

   



<div id="_mcePaste" style="position: absolute; left: -10000px; top: 0px; width: 1px; height: 1px; overflow: hidden;">
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span><span style="font: 7pt "> </span></span></span><a href="http://www.gamestar.de/spiele/action/46016/bulletstorm.html" target="_self" title="BulletStorm">BulletStorm</a>
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span><a href="http://www.gamestar.de/spiele/action/45056/crysis_2.html" target="_self" title="Crysis 2">Crysis 2</a>
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span><a href="http://www.gamestar.de/spiele/action/45684/dead_space_2.html" target="_self" title="Dead Space 2">Dead Space 2</a>
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span><a href="http://www.gamestar.de/spiele/strategie/44325/die_sims_3.html" target="_self" title="Die Sims 3">Die Sims 3</a>-Addons
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span><a href="http://www.gamestar.de/spiele/action/44539/medal_of_honor.html" target="_self" title="Medal of Honor">Medal of Honor</a>
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span>neues <strong id="nointelliTXT"><span style="font-family: ">Need for Speed</span></strong>-Spiel
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span>neues <strong id="nointelliTXT"><span style="font-family: ">Harry Potter</span></strong>-Spiel
  </p>
  
  <p class="textblock" style="margin-left: 36pt; text-indent: -18pt; line-height: normal;">
    <span style="font-size: 10pt; font-family: Symbol;"><span>·<span style="font: 7pt "> </span></span></span><a href="http://www.gamestar.de/spiele/rollenspiele/44513/star_wars_the_old_republic.html" target="_self" title="Star Wars: The Old Republic  ">Star Wars: The Old Republic</a>
  </p></p>
</div>