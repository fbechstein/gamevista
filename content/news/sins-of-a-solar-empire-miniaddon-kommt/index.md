---
title: Sins of a Solar Empire – Miniaddon kommt.
author: gamevista
type: news
date: 2009-08-26T15:57:51+00:00
excerpt: '<p>[caption id="attachment_506" align="alignright" width=""]<img class="caption alignright size-full wp-image-506" src="http://www.gamevista.de/wp-content/uploads/2009/08/sinsofasolarempire.jpg" border="0" alt="Sins of a Solar Empire" title="Sins of a Solar Empire" align="right" width="140" height="100" />Sins of a Solar Empire[/caption]Es wird für das Strategiespiel <strong>Sins of a Solar Empire</strong> schon bald ein Addon erscheinen. Dies verkündete Publisher <a href="http://www.stardock.com/products/?tab=mygames">Stardock </a>heute per Pressemitteilung Das Mini Update wird den Name <strong>The Diplomacy</strong> tragen. Details zum Zusatzinhalt gibt es bisher nicht allzuviel. Außer ein paar zusätzliche Aktionsmöglichkeiten im Diplomatiebereich waren nicht herauszulesen.</p> '
featured_image: /wp-content/uploads/2009/08/sinsofasolarempire.jpg

---
Zum Addon wird es eine Beta geben die im Herbst 2009 startet.

* * *



* * *

 