---
title: Rage – Demo unwahrscheinlich
author: gamevista
type: news
date: 2011-01-20T18:30:57+00:00
excerpt: '<p>[caption id="attachment_1936" align="alignright" width=""]<img class="caption alignright size-full wp-image-1936" src="http://www.gamevista.de/wp-content/uploads/2010/06/rage.png" border="0" alt="Rage " title="Rage " align="right" width="140" height="100" />Rage [/caption]Wie ein PR-Mitarbeiter des Entwicklers <strong>Bethesda </strong>kürzlich mitteilte, wird eine Demo für den Ego-Shooter <a href="http://rage.com/" target="_blank">Rage</a>, der im September 2011 veröffentlicht werden soll, immer unwahrscheinlicher.</p> '
featured_image: /wp-content/uploads/2010/06/rage.png

---
Zwar ist die Chance auf eine Testversion nicht völlig ausgeschlossen, dennoch wird vor dem Release des fertigen Spiels keine Zeit bleiben um eine Demo zu veröffentlichen.

 

 

* * *



* * *