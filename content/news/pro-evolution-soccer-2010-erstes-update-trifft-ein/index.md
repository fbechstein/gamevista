---
title: Pro Evolution Soccer 2010 – Erstes Update trifft ein
author: gamevista
type: news
date: 2009-10-28T20:08:43+00:00
excerpt: '<p>[caption id="attachment_964" align="alignright" width=""]<img class="caption alignright size-full wp-image-964" src="http://www.gamevista.de/wp-content/uploads/2009/10/pes2010_small.png" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="278" height="96" />Pro Evolution Soccer 2010[/caption]Der Entwickler <a href="http://www.de.games.konami-europe.com" target="_blank">Konami</a> hat heute den ersten Patch für das Fussballspiel <strong>Pro Evolution Soccer 2010</strong> veröffentlicht. Das etwas klein geratene Update ist neun Megabyte groß und kommt mit der Versionsnummer 1.01 daher. Leider hat <a href="http://www.de.games.konami-europe.com">Konami</a> keinen Changelog hinzugefügt. Sobald wir aber wissen was geändert wurde geben wir euch natürlich bescheid.</p> '
featured_image: /wp-content/uploads/2009/10/pes2010_small.png

---
[> Zum Pro Evolution Soccer 2010 &#8211; Patch 1.01 Download][1]

 

* * *



* * *

 

 [1]: downloads/patches/item/patches/pro-evolution-soccer-2010-patch-101