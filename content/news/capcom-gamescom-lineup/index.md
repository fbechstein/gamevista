---
title: Capcom – gamescom Lineup
author: gamevista
type: news
date: 2009-08-12T20:53:50+00:00
excerpt: '<p>[caption id="attachment_343" align="alignright" width=""]<a href="http://www.capcom.com/" target="_blank"><img class="caption alignright size-full wp-image-343" src="http://www.gamevista.de/wp-content/uploads/2009/08/capcom.jpg" border="0" alt="Capcom" title="Capcom" align="right" width="140" height="100" />Capcom </a>Capcom[/caption]teilte nun per Pressemitteilung sein Lineup für die in der nächsten Woche startende gamescom in Köln mit. Highlight dürfte damit wohl das Zombie-Survival-Horror Spiel <strong>Resident Evil.</strong> Das könnt ihr nämlich in Halle 6.1, Gang A am Stand von <a href="http://www.capcom.com/" target="_blank">Capcom </a>live antesten. Außerdem dürft ihr euch auf <strong>Dark Void</strong> und <strong>Lost Planet 2</strong> freuen.</p> '
featured_image: /wp-content/uploads/2009/08/capcom.jpg

---
Hier eine Liste der von Capcom gezeigten Spiele:

 

  * <span>Resident Evil 5 (PC)</span>
  * <span>Lost Planet 2 (PS3)</span>
  * <span>Dark Void</span>
  * <span>Monster Hunter Tri (Wii)</span>
  * <span>MotoGP 09/10</span>
  * <span>Spyborgs (Wii)</span>
  * <span>Tatsunoko vs. Capcom: Ultimate Allstars (Wii)<br /></span>





