---
title: Gamescom 2010 – Bethesda gibt Lineup bekannt
author: gamevista
type: news
date: 2010-07-27T18:42:30+00:00
excerpt: '<p>[caption id="attachment_8" align="alignright" width=""]<img class="caption alignright size-full wp-image-8" src="http://www.gamevista.de/wp-content/uploads/2009/06/gamescom_small.png" border="0" alt="gamescom" title="gamescom" align="right" width="140" height="100" />gamescom[/caption]Publisher <strong>Bethesda </strong>wird auf der <a href="http://www.gamescom.de" target="_blank">GamesCom 2010</a> in Köln zahlreiche Spiele der Öffentlichkeit vorstellen. Im Rahmen einer Pressemitteilung wurde nun das offizielle Lineup bekannt gegeben. Fans bekommen so vom 19. – 22. August 2010 die Möglichkeit unter anderem das Rollenspiel <strong>Fallout: New Vegas</strong>, Entwickelt von <strong>Obsidian Entertainment</strong> oder Brink, einen Shooter von Splash Damage zu testen.</p> '
featured_image: /wp-content/uploads/2009/06/gamescom_small.png

---
Außerdem wird das auf der E3 mit 45 Awards gekürte Actionspiel **RAGE** präsentiert.

 

   

* * *

   

