---
title: World of Wacraft – Neues Addon angekündigt
author: gamevista
type: news
date: 2009-08-22T12:26:37+00:00
excerpt: '<p>[caption id="attachment_442" align="alignright" width=""]<img class="caption alignright size-full wp-image-442" src="http://www.gamevista.de/wp-content/uploads/2009/08/wow_cataclysm.jpg" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="140" height="100" />World of Warcraft[/caption]It`s official! <a href="http://www.eu.blizzard.com/de">Blizzard </a>verkündete im Rahmen der gerade stattfindenden Blizzcon in Anaheim das es bald ein drittes <strong>World of Warcraft</strong> Addon geben wird. Der Name war ja schon lange bekannt. <strong>Cataclysm </strong>wird die <strong>WoW </strong>Welt komplett umkrempeln. Durch die Auferstehung vom Drachen Todesschwinge wird das Aussehen der Kontinente Kalimdor und den Östlichen Königreichen durch viele Umweltkatastrophen verändert.</p> '
featured_image: /wp-content/uploads/2009/08/wow_cataclysm.jpg

---
Es wird zwei neue Völker geben, einmal die verfluchten Worge auf Allianz Seite und die einfallsreichen Goblins auf der Horde Seite. Auch eine Level Erhöhung wird es geben, diesmal wird der maximal Level auf Stufe 85 erhöht. Wieder könnt ihr neue Talente und Fähigkeiten erlernen. Das neue Pfadsystem soll den Charakter mehr individualisieren. Klar wird es wieder neue Bereiche zu Erkunden geben und für Schlachtzüge sollen laut Blizzard mehr Inhalte den je vorhanden sein. Natürlich gibt es einen Trailer zum Addon den ihr hier sehen könnt:

[> Zum World of Warcraft: Cataclysm Addon Trailer][1]

Weiterhin sind zwei neue Trailer aufgetaucht:

[> Zum World of Warcraft: Cataclysm &#8211; Die Verlorenen Inseln Trailer][2]  
[> Zum World of Warcraft: Cataclysm &#8211; Gilneas Trailer][3]

Hier sind nochmal alle Änderungen zusammengefasst:

  * **Zwei neue spielbare Völker**  
    Begebt euch als Mitglied eines der zwei neuen Völker ins Abenteuer – als verfluchte [Worgen][4] in der Allianz oder als einfallsreiche [Goblins][5] in der Horde.**  
** 
  * **Maximalstufe auf 85 erhöht**  
    Lernt neue Fähigkeiten, macht euch neue Talente zunutze und schreitet voran durch das Pfadsystem, einem neuen Weg für Spieler, ihre Charaktere an ihre individuellen Wünschen anzupassen.
  * **Die Gebiete der Classic-Version sind neu gestaltet**  
    Bekannte Gebiete auf den ursprünglichen Kontinenten Kalimdor und die östlichen Königreiche sind für immer verändert und mit neuem Inhalt gefüllt – vom verwüsteten Ödland bis hin zum zerbrochenen Brachland, welches entzweigerissen wurde.
  * **Neue Bereiche für hohe Stufen**  
    Erforscht neu geöffnete Teile der Welt, inklusive Uldum, Grim Batol und die großartige versunkene Stadt Vashj’ir unter der Meeresoberfläche.
  * **Neue Bereiche für hohe Stufen**  
    Erforscht neu geöffnete Teile der Welt, inklusive Uldum, Grim Batol und die großartige versunkene Stadt Vashj’ir unter der Meeresoberfläche.
  * **Mehr Inhalte für Schlachtzüge als jemals zuvor**  
    Genießt mehr Inhalte für hochstufige Schlachtzüge als in vorherigen Erweiterungen, mit optionalen, herausfordernderen Versionen für alle Bosskämpfe.
  * **Neue Volks- und Klassenkombinationen**  
    Erforscht Azeroth als Gnomenpriester, Blutenelfenkrieger oder als eine der anderen, nun erstmals möglichen Volks- und Klassenkombinationen.
  * **Gildenentwicklung**  
    Macht als Gilde Fortschritte, um mit der Gilde den Stufenaufstieg zu erreichen und Gildenerfolge zu schaffen.
  * **Neue PvP-Zone & gewertete Schlachtfelder**  
    Verfolgt PvP-Ziele und nehmt tägliche Quests auf der Insel Tol Barad an, der neuen Tausendwinter-ähnlichen Zone, und schlagt Schlachten auf komplett neuen Schlachtfeldern.
  * **Archäologie**  
    Meistert eine neue sekundäre Fertigkeit, um wertvolle Artefakte zu bergen und einzigartige Belohungen zu erhalten.
  * **Fliegende Reittiere in Azeroth**  
    Erforscht Kalimdor und die östlichen Königreiche wie nie zuvor.

* * *



* * *

 

 

 [1]: videos/item/root/world-of-warcraft-cataclysm-blizzcon-trailer
 [2]: videos/item/root/world-of-warcraft-cataclysm-die-verlorenen-inseln-trailer
 [3]: videos/item/root/world-of-warcraft-cataclysm-gilneas-trailer
 [4]: http://www.wow-europe.com/cataclysm/features/worgen.html
 [5]: http://www.wow-europe.com/cataclysm/features/goblin.html