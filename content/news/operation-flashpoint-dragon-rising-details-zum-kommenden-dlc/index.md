---
title: 'Operation Flashpoint: Dragon Rising – Details zum kommenden DLC'
author: gamevista
type: news
date: 2009-12-21T15:47:15+00:00
excerpt: '<p>[caption id="attachment_606" align="alignright" width=""]<img class="caption alignright size-full wp-image-606" src="http://www.gamevista.de/wp-content/uploads/2009/09/opf2_small.jpg" border="0" alt="Operation Flashpoint: Dragon Rising" title="Operation Flashpoint: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint: Dragon Rising[/caption]Zum kommenden Zusatzinhalt des Ego-Shooters Operation <strong>Flashpoint: Dragon Rising</strong>, hat der Publisher <a href="http://www.flashpointgame.com" target="_blank">Codemasters</a> nun neue Details bekannt gegeben. So wird der DLC <strong>Overwatch </strong>unter anderem zwei neue Multiplayer-Modi bieten. In dem Modus <strong>Blindside </strong>treten zwei Teams gegeneinander an, wobei ein Team in kurzer Zeit möglichst viele Missionsziele erfüllen muss.</p> '
featured_image: /wp-content/uploads/2009/09/opf2_small.jpg

---
Das andere Team soll dies verhindern. In „**Supremacy**“ wird es die Aufgabe sein, bestimmte Punkte auf der Karte zu verteidigen. Außerdem wird es neue Missionen zum Fire-Team Engagement geben. Der Zusatzinhalt steht ab sofort für Xbox 360 zum Download bereit. Bald darauf soll für PC-Fans eine ähnliche Version folgen. Allerdings gibt es noch keine genauen Angaben zum Veröffentlichungstermin.

 

* * *



* * *

 