---
title: Pro Evolution Soccer 2011 – Lizenzen von FC Bayern und Werder Bremen
author: gamevista
type: news
date: 2010-09-07T13:43:03+00:00
excerpt: '<p>[caption id="attachment_1755" align="alignright" width=""]<img class="caption alignright size-full wp-image-1755" src="http://www.gamevista.de/wp-content/uploads/2010/05/pes2011.jpg" border="0" alt="Pro Evolution Soccer 2011" title="Pro Evolution Soccer 2011" align="right" width="140" height="49" />Pro Evolution Soccer 2011[/caption]Endlich hält die Bundesliga auch Einzug in das Fussballspiel <a href="http://de.games.konami-europe.com/game.do?idGame=294" target="_blank">Pro Evolution Soccer 2011</a> des Publishers <strong>Konami</strong>. Bisher konnte man nur mit Spielernamen die Bundesligamannschaften auf den Rasen schicken. Jetzt hat sich der japanische Publisher die Namensrechte am FC Bayern und SV Werder Bremen gesichert.</p> '
featured_image: /wp-content/uploads/2010/05/pes2011.jpg

---
Aber auch International sind einige Teams mit von der Partie. Neben den italienischen Clubs wie Lecce, Cesena und Brescia werden auch spanische Ligisten wie Real Zaragoza, Sporting Gijon, Almeria und Getafe dabei sein. Aus England dürfen Manchester United und die Tottenham Hotspurs antreten. Insgesamt sind das über 100 offiziell lizenzierte Club Mannschaften aus Europa. <a href="http://de.games.konami-europe.com/game.do?idGame=294" target="_blank">Pro Evolution Soccer 2011</a> erscheint am 30. September für PC, PlayStation 3 und Xbox 360.

 

   

* * *

   

