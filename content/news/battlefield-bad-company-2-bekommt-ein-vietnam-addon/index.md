---
title: 'Battlefield: Bad Company 2 – Bekommt ein Vietnam Addon'
author: gamevista
type: news
date: 2010-06-15T16:44:52+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Im Rahmen der Spielemesse E3 in Los Angeles, präsentierte der Publisher <strong>Electronic Arts</strong> der Presse das nächste Addon für den Ego-Shooter <a href="http://www.battlefieldbadcompany2.com/de" target="_blank">Battlefield: Bad Company 2</a>. Das Erweiterungspaket mit dem Namen <strong>Vietnam </strong>führt euch wie nicht anders zu erwarten war in den Vietnamkrieg.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Mit neuen Fahrzeugen und zahlreichen Waffen werden die US Army und die nordvietnamesische Armee ausgestattet. **DICE** zeigt passend dazu einen neuen Teaser-Trailer. <a href="http://www.battlefieldbadcompany2.com/de" target="_blank">Battlefield: Bad Company 2 – Vietnam</a> wird voraussichtlich im Dezember als kostenpflichtiger Download veröffentlicht. Die Xbox 360-Konsole wird dabei Vorrang bekommen. PC- und PlayStation 3-Version sollen später folgen.

[> Zum Battlefield: Bad Company 2 &#8211; Vietnam-Trailer][1]

 

   

* * *

   



 [1]: videos/item/root/battlefield-bad-company-2-vietnam-e3-trailer