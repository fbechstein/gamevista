---
title: 'Need for Speed: Shift – EA belohnt Vorbesteller'
author: gamevista
type: news
date: 2009-08-14T19:39:28+00:00
excerpt: '<p>[caption id="attachment_124" align="alignright" width=""]<img class="caption alignright size-full wp-image-124" src="http://www.gamevista.de/wp-content/uploads/2009/07/shift_small.jpg" border="0" alt="Need for Speed: Shift" title="Need for Speed: Shift" align="right" width="140" height="100" />Need for Speed: Shift[/caption]Sehr gute Nachrichten für alle Vorbesteller von <strong>Need for Speed: Shift</strong>, denn der Publisher <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a> veröffentlichte nun die Inhalte aller Vorbesteller-Versionen. Hier bekommt ihr einen kleinen Überblick über die vielen Extras die den Verkauf vor Release ankurbeln sollen.</p> '
featured_image: /wp-content/uploads/2009/07/shift_small.jpg

---
****

  * **Amazon:** Amazon-Kunden haben durch die Vorbestellung Zugriff auf die Elite-Serie. Diese beinhaltet fünf exklusive Renn-Challenges, die ausschließlich durch die Vorbestellung über Amazon erhältlich sind.

  * **EA Store:** Kunden des EA Store können den Porsche Cayman S für ihre Version herunterladen.

  * **Gamesload:** Für Gamesload-Kunden bietet die Vorbestellung Zugriff auf den ultimativen Härtetest: Ein 75 Runden Ausdauerrennen auf der Road America. Dieses Event ist ausschließlich per Vorbestellung erhältlich.

  * **GameStop:**  
    Vorbesteller von GameStop erhalten das Hero Car von Need for Speed Shift, den BMW E92 M3 GT2.







 

 