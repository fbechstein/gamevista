---
title: Left 4 Dead 2 – erhält keine Jugendfreigabe
author: gamevista
type: news
date: 2009-09-03T17:26:26+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Am 17. November erscheint der beliebte Nachfolger zum Zombie Mehrspielerschnetzelspiel <strong>Left 4 Dead 2</strong>. Mit neuen Waffen, Gegner und abwechslungsreicher Kampagne wird <strong>Left 4 Dead 2</strong> für PC und die Xbox 360 veröffentlicht. Nun hat die <a href="http://www.usk.de/">Unterhaltssoftware Selbstkontrolle</a>, kurz <a href="http://www.usk.de/">USK</a>, die deutsche Version getestet und dem Spiel den roten 18 Jahre Aufkleber verpasst.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Heißt, **Left 4 Dead 2** erhält keine Jugendfreigabe, wird aber in Deutschland für Erwachsene erhältlich sein. Weiterhin wird vermutet das diese Version auch geschnitten ist da das Spiel sonst wohl indiziert werden würde. Auch im ersten Teil gab es Kürzungen. Es gab keine abgetrennten Körperteile und die Leichen von Gegnern lösten sich nach kurzer Zeit auf.

 

 

* * *



* * *