---
title: Just Cause 2 – Offizielle Website ist online
author: gamevista
type: news
date: 2009-08-24T22:00:54+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Publisher <a href="http://www.eidos.de">Eidos </a>und Entwickler Avalance Studios eröffneten heute die neue Webseite zum Agenten Spiel <strong>Just Cause 2</strong>. Neben vielen Informationen wartet auf der Website ein Fansite-Kit zum Download. Laut <a href="http://www.eidos.de">Eidos </a>wird es in naher Zukunft ein Gewinnspiel geben. Was gewonnen werden kann und wann das Ganze soweit sein soll ist bisher noch nicht bekannt. Weiterhin wurde <strong>Just Cause 2</strong> erst vor kurzem auf das nächste Jahr verschoben.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
[> Zur Just Cause 2 Website][1]



 

* * *



* * *

 

 [1]: http://www.justcause.com/home