---
title: 'Operation Flashpoint: Dragon Rising – Demo steht bereit'
author: gamevista
type: news
date: 2009-11-18T17:53:21+00:00
excerpt: '<p>[caption id="attachment_606" align="alignright" width=""]<img class="caption alignright size-full wp-image-606" src="http://www.gamevista.de/wp-content/uploads/2009/09/opf2_small.jpg" border="0" alt="Operation Flashpoint: Dragon Rising" title="Operation Flashpoint: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint: Dragon Rising[/caption]Der Publisher<a href="http://www.flashpointgame.com" target="_blank"> Codemasters</a> hat nun endlich die Demo für die Kriegssimulation<strong> Operation Flashpoint: Dragon Rising</strong> veröffentlicht. Der erfolgreiche Ego-Shooter ist bereits seit einiger Zeit erhältlich und so war es nun an der Zeit die Testversion den noch unentschlossenen Spielern bereit zu stellen. In der 1,5 Gigabyte großen Demo könnt ihr die erste Mission allein oder mit vier Freunden im Koop-Modus anspielen.</p> '
featured_image: /wp-content/uploads/2009/09/opf2_small.jpg

---
Wahlweise könnt ihr die deutsche oder englische Version anspielen. So klickt euch nun zum Demodownload. Wir wünschen viel Spaß bei **Operation Flashpoint: Dragon Rising**.

[> Zur deutschen Demo von Operation Flashpoint: Dragon Rising][1]

[> Zur englischen Demo von Operation Flashpoint: Dragon Rising][2]







 

 [1]: downloads/demos/item/demos/operation-flashpoint-dragon-rising-demo-in-deutsch
 [2]: downloads/demos/item/demos/operation-flashpoint-dragon-rising-demo-in-englisch