---
title: Aliens vs. Predator – Es wird eine Demo geben
author: gamevista
type: news
date: 2010-01-10T16:50:09+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Der Lead Designer Alex Moore vom Entwicklerstudio <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Rebellion</a> hat nun offiziell bestätigt, dass es eine Demo zum Ego-Shooter <strong>Aliens vs. Predator</strong> geben wird. Allerdings ist bisher nicht bekannt wann und in welchem Umfang die Testversion des Spiels daher kommen wird.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
**Aliens vs. Predator** wird am 19. Februar 2010 in Europa veröffentlicht werden, eine deutsche Version wird es dabei nicht geben. Ob die Demo noch vor dem Veröffentlichungstermin den Weg ins Netz finden wird ist fraglich.

 

<cite></cite>

* * *



* * *

 