---
title: Mafia 2 – Bereits zweiter DLC in der Entwicklung
author: gamevista
type: news
date: 2010-07-18T16:42:06+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]<strong>2K Games</strong> entwickelt bereits den zweiten Zusatzinhalt für das Actionspiel <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> das am 27. August im Handel erscheinen soll. Der erste DLC wurde vor kurzem angekündigt und wird bei Verkaufsstart angeboten.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Der DLC mit dem Namen **Jimmy\`s Vendetta** erscheint ein paar Wochen nach dem Release von <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> für PlayStation 3, PC und Xbox 360. Zum Inhalt gibt es noch nicht allzu viele Informationen. Für neue Schauplätze und Charakter ist aber gesorgt. Über den Preis hat **2K Games** noch keine genaueren Details bekannt gegeben.

 

   

* * *

   

