---
title: Playstation 3 Slim offiziell enthüllt
author: gamevista
type: news
date: 2009-08-18T22:52:44+00:00
excerpt: '<p>[caption id="attachment_392" align="alignright" width=""]<img class="caption alignright size-full wp-image-392" src="http://www.gamevista.de/wp-content/uploads/2009/08/ps3slim_small.png" border="0" alt="PS3 Slim kommt!" title="PS3 Slim kommt!" align="right" width="140" height="100" />PS3 Slim kommt![/caption]Die Tage und Wochen der Gerüchte sind endlich vorbei. Auf der offiziellen Sony Pressekonferenz, die heute stattfand, wurde die Veröffentlichung der <strong>PS3 Slim</strong> offiziell bekanntgegeben. Die PS3 Slim wird demnach 33% kleiner, 36% leichter und 34% energieeffizienter sein. Die PS3 Slim soll bereits Anfang September zum Preis von 299,- EUR über die Ladentheke gehen. Ab morgen soll der Abverkauf der aktuellen PS3 losgehen - ebenfalls zum Preis von 299,- EUR.</p> '
featured_image: /wp-content/uploads/2009/08/ps3slim_small.png

---
Die PS3 Slim wird mit einer 120 GB HDD ausgeliefert.

Was sonst noch auf der Sony Pressekonferenz präsentiert wurde erfahrt Ihr in unseren News.

<a href="index.php?Itemid=102&option=com_zoo&view=item&category_id=0&item_id=189" target="_self">> In unserer Videogalerie könnt Ihr die Ankündigung der PS3 Slim von Sony CEO Kazuo Hirai sehen</a>

<a href="index.php?Itemid=111&option=com_zoo&view=item&category_id=0&item_id=188" target="_self">> In unsererer Screenshot Galerie könnt Ihr die Bilder von der Sony Pressekonferenz und der PS3 Enthüllung sehen</a>

* * *



* * *

 