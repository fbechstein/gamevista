---
title: 'Ghostbusters: Sanctum of Slime – Releasetermin bekannt'
author: gamevista
type: news
date: 2011-01-26T18:29:15+00:00
excerpt: '<p>[caption id="attachment_2471" align="alignright" width=""]<img class="caption alignright size-full wp-image-2471" src="http://www.gamevista.de/wp-content/uploads/2010/12/ghostbusters_sos.jpg" border="0" alt="Ghostbusters: Sanctum of Slime" title="Ghostbusters: Sanctum of Slime" align="right" width="140" height="100" />Ghostbusters: Sanctum of Slime[/caption]Publisher <strong>Atari </strong>hat den Veröffentlichungstermin des Actionspiels <a href="http://www.atari.com/gbsanctumofslime/" target="_blank">Ghostbusters: Sanctum of Slime</a> über den Informationsdienst <a href="http://twitter.com/atari/status/29704213265121282" target="_blank">Twitter</a> verlauten lassen. Demnach wird der als Koop-fähige angepriesene Titel als Download über XBL bzw. PSN am 23. März 2011 verfügbar sein.</p> '
featured_image: /wp-content/uploads/2010/12/ghostbusters_sos.jpg

---
Einen genauen Veröffentlichungstermin für PC gibt es bisher nicht.

 

 

* * *



* * *