---
title: 'Arcania: Gothic 4 – Vierter Teil der Gothic-Serie mit neuem Namen'
author: gamevista
type: news
date: 2010-03-31T20:07:37+00:00
excerpt: '<p>[caption id="attachment_1648" align="alignright" width=""]<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Der Publisher <strong>JoWood Entertainment</strong> hat im Rahmen einer Pressemitteilung eine Umbenennung des sich in der Entwicklung befindenden vierten Gothic-Teils. Demnach wird aus dem Namen des Rollenspiels Arcania: A Gothic Tale nun <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a>.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
Einen Veröffentlichungstermin wollte man aber noch nicht verraten, laut **JoWood** soll dieser aber in Kürze nachgereicht werden.

 

<cite></cite>

   

* * *

   

