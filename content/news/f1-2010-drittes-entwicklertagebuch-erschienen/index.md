---
title: F1 2010 – Drittes Entwicklertagebuch erschienen
author: gamevista
type: news
date: 2010-05-14T15:33:04+00:00
excerpt: '<p>[caption id="attachment_1800" align="alignright" width=""]<img class="caption alignright size-full wp-image-1800" src="http://www.gamevista.de/wp-content/uploads/2010/05/f1_2010.jpg" border="0" alt="F1 2010" title="F1 2010" align="right" width="140" height="100" />F1 2010[/caption]Der Publisher <strong>Codemasters </strong>stellte heute das dritte Entwicklertagebuch zum Formel 1-Rennspiel <a href="http://formula1-game.com/de_DE/index.php" target="_blank">F1 2010</a> vor. Im neusten Video präsentiert der Publisher das Wettersystem und erklärt anhand von diversen Mitarbeitern, die Auswirkungen die ein Regenschauer so mit sich bringt.</p> '
featured_image: /wp-content/uploads/2010/05/f1_2010.jpg

---
Natürlich darf wie im richtigen Rennalltag die Wahl der besten Reifen nicht fehlen.

[> Zum F1 2010 &#8211; Entwicklertagebuch #3][1]

   

* * *

   



 [1]: videos/item/root/f1-2010-entwicklertagebuch-3