---
title: Risen – kein DRM Kopierschutz
author: gamevista
type: news
date: 2009-07-20T12:51:17+00:00
excerpt: '<p>[caption id="attachment_81" align="alignright" width=""]<img class="caption alignright size-full wp-image-81" src="http://www.gamevista.de/wp-content/uploads/2009/07/risenisland.png" border="0" alt="Risen kommt ohne DRM" title="Risen kommt ohne DRM" align="right" width="140" height="100" />Risen kommt ohne DRM[/caption]Ein gute Nachricht kommt aus dem Hause <a href="http://www.deepsilver.com/" target="_blank">Deep Silver</a>, dem Publisher für das kommende Rollenspiel von <a href="http://www.piranha-bytes.com/" target="_blank">Piranha Bytes</a> - <strong>Risen</strong>. Demnach wird es das umstrittene DRM Verfahren bei Risen nicht geben. Die Online Aktivierung bzw. Aktivierung mit Seriennummer fällt damit weg. Es wird lediglich verlangt die Spiel-DVD im Laufwerk zu lassen.  </p>'
featured_image: /wp-content/uploads/2009/07/risenisland.png

---
&#8222;Hallo zusammen, Wie versprochen melde ich mich bei euch zurück um eine Neuigkeit zum Thema Kopierschutz zu vermelden. Ich darf nun offiziell bekannt geben das wir uns nach reiflichen Überlegungen für eine der Kopierschutz-Optionen für Risen entschieden haben. Wir werden in Risen einen Kopierschutz ohne Onlineaktivierung aka. DRM verwenden. Risen wir auch keine Seriennummer oder sonstige Aktivierung benötigen, die DVD muss lediglich zum Spielen im Laufwerk verbleiben. Ich bin davon überzeugt das wir mit dieser Entscheidung für einen einfachen Disk-Kopierschutz den richtigen Weg für Risen eingeschlagen haben und freue mich schon auf euer Feedback.&#8220;, so Brand Manager Daniel Oberlerchner im <a href="http://forum.worldofplayers.de/forum/showpost.php?p=9983455&#038;postcount=650" target="_blank">Worldofplayers-Forum</a>. 

Zum Einsatz soll der Kopiersschutz <a href="http://de.wikipedia.org/wiki/Tag%C3%A8s" target="_blank">Tagès</a> kommen.

 