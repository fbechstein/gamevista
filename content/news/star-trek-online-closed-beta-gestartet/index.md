---
title: Star Trek Online – Closed Beta gestartet
author: gamevista
type: news
date: 2009-10-23T19:07:15+00:00
excerpt: '<p>[caption id="attachment_915" align="alignright" width=""]<img class="caption alignright size-full wp-image-915" src="http://www.gamevista.de/wp-content/uploads/2009/10/startrek_online.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Der Publisher <a href="http://startrekonline.com" target="_blank">Atari</a> hat heute per offizieller Pressemitteilung gemeldet das die Closed Beta für das Onlinerollenspiel <strong>Star Trek Online</strong> gestartet wurde. Somit dürfen nun auserwählte Fans erstmals die Server besuchen und die Weiten des Weltraums erkunden.</p> '
featured_image: /wp-content/uploads/2009/10/startrek_online.jpg

---
Wenn ihr ebenfalls an der geschlossenen Beta teilnehmen wollt gibt es die Möglichkeit sich über dieses <a href="https://startrekonline.com/user/register?destination=preview_application" target="_blank">Anmeldeformular</a> auf der offiziellen Website von **Star Trek Online** dafür zu bewerben. Bisher gibt es keine genauen Termine wie lange die Closed Beta laufen wird.

 

* * *



* * *

 