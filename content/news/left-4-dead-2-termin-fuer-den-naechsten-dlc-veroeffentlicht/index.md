---
title: Left 4 Dead 2 – Termin für den nächsten DLC veröffentlicht
author: gamevista
type: news
date: 2010-09-28T18:19:15+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Entwickler <strong>Valve </strong>gab vor kurzem den Termin für den nächsten Zusatzinhalt von <a href="http://www.l4d.com" target="_blank">Left 4 Dead 2</a> bekannt. Bereits am 5. Oktober 2010 wird die neue Kampagne <strong>The Sacrifice</strong> für den Zombie-Shooter veröffentlicht. Als Bonus für alle Fans des ersten Teils von <strong>Left 4 Dead</strong>, wird der DLC für beide Teile erscheinen.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Dies bestätigte **Valve** im [offiziellen L4D-Blog][1]. Inhaltlich wird das Vierergespann seinen Weg zur Hängebrücke in Georgia antreten und dabei einen seiner Protagonisten verlieren. Welcher der vier Helden das Zeitliche segnen wird ist aber noch offen. Wie immer wird Download-Content für PC kostenlos veröffentlicht. Wohingegen Xbox 360-Besitzer 560 Microsoft Punkte bezahlen müssen.

 

   

* * *

   



 [1]: http://www.l4d.com/blog/post.php?id=4397