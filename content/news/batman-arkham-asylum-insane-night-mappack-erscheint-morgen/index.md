---
title: 'Batman: Arkham Asylum – Insane Night Mappack erscheint Morgen'
author: gamevista
type: news
date: 2009-09-16T15:40:29+00:00
excerpt: '<p>[caption id="attachment_707" align="alignright" width=""]<img class="caption alignright size-full wp-image-707" src="http://www.gamevista.de/wp-content/uploads/2009/09/batman_arkham_asylum_small.jpg" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Batman: Arkham Asylum[/caption]Publisher <a href="http://www.eidos.de">Eidos Interactive</a> gab heute per offizieller Pressemitteilung bekannt das ihr <strong>Insane Night Mappack</strong> morgen den 17. September veröffentlicht wird. Ab morgen könnt ihr im PlayStation Store, auf dem Xbox Live Marktplatz oder dem Games for Windows Marktplatz die kostenlosen Zusatzkarten herunterladen.</p> '
featured_image: /wp-content/uploads/2009/09/batman_arkham_asylum_small.jpg

---
„Auf den beiden neuen Herausforderungs-Karten können Spieler ihre FreeFlow-Kampffähigkeiten verbessern und ihr Invinsible-Predator-Können ausgiebig testen“ so der Pressesprecher von [Eidos][1]. Zum einen wird die Karte mit dem Namen „Totaly Insane“ angeboten. Diese ist eine FreeFlow Kampfkarte in der ihr Wellen von Angreifern zurückschlagen müsst. Nocturnal Hunter ist eine Invisible Predator Karte in der eure Schleichfähigkeiten gefordert werden. Es gilt mit der Predator Sicht im Schatten zu verharren und aus dem Hinterhalt zuzuschlagen.

 

* * *



* * *

 [1]: http://www.eidos.de