---
title: F.E.A.R. 3 – Release verschoben
author: gamevista
type: news
date: 2011-01-12T18:34:56+00:00
excerpt: '<p>[caption id="attachment_1670" align="alignright" width=""]<img class="caption alignright size-full wp-image-1670" src="http://www.gamevista.de/wp-content/uploads/2010/04/fear3.jpg" border="0" alt="F.E.A.R. 3" title="F.E.A.R. 3" align="right" width="140" height="100" />F.E.A.R. 3[/caption]Publisher <strong>Warner Bros. </strong>hat offiziell bestätigt, dass der Actiontitel <a href="http://www.whatisfear.com" target="_blank">F.E.A.R. 3</a>, der ursprünglich am 25. März 2011 veröffentlicht werden sollte, in den Mai dieses Jahres verschoben wird. Demnach erscheint der dritte Teil erst zwei Monate später weil die Entwickler ein bestmöglichstes Ergebnis abliefern möchten und weitere Zeit in den Feinschliff des Spiels stecken möchten.</p> '
featured_image: /wp-content/uploads/2010/04/fear3.jpg

---
 

 

   

* * *

   

