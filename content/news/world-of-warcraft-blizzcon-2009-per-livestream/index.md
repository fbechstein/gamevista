---
title: World of Warcraft – Blizzcon 2009 per Livestream
author: gamevista
type: news
date: 2009-08-05T20:27:34+00:00
excerpt: '<p>[caption id="attachment_288" align="alignright" width=""]<img class="caption alignright size-full wp-image-288" src="http://www.gamevista.de/wp-content/uploads/2009/08/blizzcon_small.jpg" border="0" alt="Blizzcon 2009" title="Blizzcon 2009" align="right" width="140" height="100" />Blizzcon 2009[/caption]Die <a href="http://www.blizzcon.com" target="_blank" title="blizzcon">Blizzcon </a>2009 die am 21. und 22. August 2009 in Anaheim USA stattfindet, wird per Livestream ins Internet übertragen. <a href="http://www.eu.blizzard.com/de/" target="_blank" title="Blizzard">Blizzard </a>will mit dem Service seinen größten Fans die Möglichkeit geben Live bei den Interviews, Turnieren oder <a href="http://www.eu.blizzard.com/de/" target="_blank" title="blizzard">Blizzard </a>Highlights dabei zu sein. </p>'
featured_image: /wp-content/uploads/2009/08/blizzcon_small.jpg

---
Allerdings gibt es da den kleinen Haken das der Zugang für 16 Stunden Übertragung ganze 29,- Euro kosten soll. Interessenten können natürlich, wenn sie etwas verpasst haben, auch alles noch mal nach der Show als Wiederholung ansehen. Außerdem wird das eigentlich für die Blizzconbesucher exklusive Ingame-Pet, Gurgli der Murloc Marine, für euch freigeschaltet.







 

 