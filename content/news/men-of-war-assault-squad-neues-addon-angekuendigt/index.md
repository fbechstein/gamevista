---
title: 'Men of War: Assault Squad – Neues Addon angekündigt'
author: gamevista
type: news
date: 2010-05-27T17:26:53+00:00
excerpt: '<p>[caption id="attachment_174" align="alignright" width=""]<img class="caption alignright size-full wp-image-174" src="http://www.gamevista.de/wp-content/uploads/2009/07/men_of_war.jpg" border="0" alt="Men of War" title="Men of War" align="right" width="140" height="100" />Men of War[/caption]Der Publisher <strong>1C Company</strong> hat für das Strategiespiel <a href="http://www.menofwargame.com" target="_blank">Men of War</a> ein weiteres Addon angekündigt. Das selbständig laufende Erweiterungspaket mit dem Namen <strong>Assault Squad</strong>, wird im dritten Quartal 2010 in den Handel kommen. Inhaltlich soll <strong>Assault Squad</strong> einen neuen Spielmodus bieten.</p> '
featured_image: /wp-content/uploads/2009/07/men_of_war.jpg

---
So wird es nun möglich sein im Skirmish-Modus sein Können zu beweisen. Außerdem wird es neue Munitionstypen für Panzer geben. Weitere Mehrspielerkarten sowie Verbesserungen am Realismus dürfen da natürlich nicht fehlen.

 

   

* * *

   

