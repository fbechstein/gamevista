---
title: 'Battlefield: Bad Company 2 – Viertes VIP Mappack veröffentlicht'
author: gamevista
type: news
date: 2010-07-08T20:10:14+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Entwickler <strong>DICE </strong>hat sein viertes VIP Mappack für den Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> veröffentlicht. Das Kartenpaket ist wie immer für PC Nutzer kostenlos. Hingegen Konsolen-Fans spezielle Download-Codes benötigen. Wirklich neue Karten gibt es beim vierten Ableger auch nicht zu bestaunen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Lediglich die Karten Atacama Desert und Port Valdez sind nun im Rush- bzw. Conquest-Modus spielbar. Als kleinen Vorgeschmack präsentiert **DICE** auch den passenden Trailer zum VIP Mappack #4.

[> Zum Battlefield: Bad Company 2 &#8211; Mappack #4 Trailer][1]

   

* * *

   



 [1]: videos/item/root/battlefield-bad-company-2-mappack-4-trailer