---
title: Mafia 2 – Collectors Edition bestätigt
author: gamevista
type: news
date: 2010-05-26T16:31:07+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]<strong>2K Games</strong> hat angekündigt, dass <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> auch als Collectors Edition für PC, PlayStation 3 und Xbox 360 in den Handel kommen wird. Jede Special Edition wird aus einem Steelbook-Case bestehen. Darin enthalten sind ein Hardcover Art Book, ein Soundtrack und eine Karte.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Zusätzlich werden Vorbesteller die in England ihre <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> CE ordern einen kostenlosen DLC als Dreingabe erhalten.    
Dieser Zusatzinhalt wird zwei luxuriöse Autos und zwei Anzüge bieten. Bisher gibt es keine Preisangaben für die Collectors Edition. <a href="http://www.mafia2game.com" target="_blank"></p> 

<p>
  Mafia 2</a> wird am 27. August 2010 veröffentlicht.
</p>

<p>
   
</p>

<p>
   
</p>

<p>
  <em> </p> 
  
  <hr />
  </em>
  <em> </p> 
  
  <hr />
  </em>
</p>