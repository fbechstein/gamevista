---
title: 'Operation Flashpoint: Dragon Rising – Termin für den ersten Zusatzinhalt'
author: gamevista
type: news
date: 2009-11-04T20:06:13+00:00
excerpt: '<p>[caption id="attachment_606" align="alignright" width=""]<img class="caption alignright size-full wp-image-606" src="http://www.gamevista.de/wp-content/uploads/2009/09/opf2_small.jpg" border="0" alt="Operation Flashpoint: Dragon Rising" title="Operation Flashpoint: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint: Dragon Rising[/caption]Laut dem Publisher <a href="http://www.codemasters.com/flashpoint/" target="_blank">Codemasters</a> sollte der erste Download-Inhalt für die Kriegssimulation <strong>Operation Flashpoint: Dragon Rising</strong> bereits Ende Oktober erscheinen, bisher war aber davon nichts zu sehen. Nun hat <a href="http://www.codemasters.com/flashpoint/" target="_blank">Codemasters</a> offiziell bestätigt das der Zusatzinhalt für <strong>Operation Flashpoint: Dragon Rising</strong> am 5. November 2009 veröffentlicht werden soll.</p> '
featured_image: /wp-content/uploads/2009/09/opf2_small.jpg

---
Das Paket mit dem Namen _Skirmish_ wird mit einem Patch kostenlos den Fans zur Verfügung gestellt. Unter anderem werden vier neue Karten für den Multiplayer-Modus enthalten sein. Außerdem sind ab dem 5. November zwei neue Fireteam Missionen verfügbar.

 

 

* * *



* * *