---
title: Cities XL – Zweiter Zusatzinhalt ist da
author: gamevista
type: news
date: 2009-12-08T18:12:22+00:00
excerpt: '<p>[caption id="attachment_563" align="alignright" width=""]<img class="caption alignright size-full wp-image-563" src="http://www.gamevista.de/wp-content/uploads/2009/09/citysxl.jpg" border="0" alt="Cities XL" title="Cities XL" align="right" width="140" height="100" />Cities XL[/caption]Zum Städtesimulation <strong>Cities XL</strong> vom Entwickler <a href="http://www.citiesxl.com" target="_blank">Monte Cristo</a> wurde nun ein neues Zusatzinhalt-Pack veröffentlicht. Der nunmehr zweite DLC ist völlig kostenlos und steht zum Download bereit. Wenn der Autoverkehr in eurer Stadt zum Problem geworden ist hat der Entwickler nun was Neues für euch.</p> '
featured_image: /wp-content/uploads/2009/09/citysxl.jpg

---
So wurde unter anderem ein Bussystem eingeführt das die Straßen entlasten soll. Dieses wird ab einer Einwohnerzahl von 50.000 freigeschaltet, im Expert-Modus sogar früher. Außerdem wird es eine Karte von New York geben neben Riesenrad und allerlei neuen Gebäuden. Weitere Details zum Inhaltspacket findet ihr auf der <a href="http://www.citiesxl.com/index.php?option=com_content&#038;task=view&#038;id=2709&#038;Itemid=128" target="_blank">Website</a> des Entwicklers.

 

<p style="text-align: center;">
  <img class=" size-full wp-image-1157" src="http://www.gamevista.de/wp-content/uploads/2009/12/buscloseup01sm.jpg" border="0" width="540" height="304" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/buscloseup01sm.jpg 540w, http://www.gamevista.de/wp-content/uploads/2009/12/buscloseup01sm-300x169.jpg 300w" sizes="(max-width: 540px) 100vw, 540px" />
</p>





