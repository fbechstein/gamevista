---
title: 'The Witcher 2: Assassins of Kings – Zweites Entwicklertagebuch erschienen'
author: gamevista
type: news
date: 2010-11-07T18:12:22+00:00
excerpt: '<p>[caption id="attachment_2191" align="alignright" width=""]<img class="caption alignright size-full wp-image-2191" src="http://www.gamevista.de/wp-content/uploads/2010/09/thewitcher2.jpg" border="0" alt="The Witcher 2: Assassins of Kings" title="The Witcher 2: Assassins of Kings" align="right" width="140" height="100" />The Witcher 2: Assassins of Kings[/caption]Der Entwickler <strong>CD Projekt</strong> präsentierte vor kurzem das zweite Entwicklertagebuch für das Rollenspiel <a href="http://www.thewitcher.com" target="_blank">The Witcher 2: Assassins of Kings</a>. Im neusten Trailer wird das Thema Technik und Grafikengine vorgestellt. The Witcher 2: Assassins of Kings wird im März 2011 veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/09/thewitcher2.jpg

---
[> Zum The Witcher 2: Assassins of Kings &#8211; Entwicklertagebuch #2][1]

 

   

* * *

   



 [1]: videos/item/root/the-witcher-2-assassins-of-kings-zweites-entwicklertagebuch