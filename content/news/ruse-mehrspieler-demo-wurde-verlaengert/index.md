---
title: R.U.S.E. – Mehrspieler-Demo wurde verlängert
author: gamevista
type: news
date: 2010-07-19T21:49:37+00:00
excerpt: '<p><strong>[caption id="attachment_2031" align="alignright" width=""]<img class="caption alignright size-full wp-image-2031" src="http://www.gamevista.de/wp-content/uploads/2010/07/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Ubisoft </strong>hat die Multiplayer Demo zum Strategiespiel <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a> bis zum kommenden Mittwoch verlängert. Damit reagierte der Publisher auf einen Anzeigefehler auf der Onlineplattform Steam. Die Aktion mit dem Namen Free Weekend wurde abrubt am Samstagabend zur Primetime beendet.</p> '
featured_image: /wp-content/uploads/2010/07/ruse_small.jpg

---
Sehr zum Unmut aller R.U.S.E. Fans. Darauf hat der Publisher reagiert und das Strategiespiel wird nun bis zum Mittwoch 3 Uhr nachmittags PTZ verfügbar sein.

 

   

* * *

   

