---
title: Emergency 2012 – Demo zur Rettungssimulation erschienen
author: gamevista
type: news
date: 2010-12-02T17:40:41+00:00
excerpt: '<p>[caption id="attachment_2449" align="alignright" width=""]<img class="caption alignright size-full wp-image-2449" src="http://www.gamevista.de/wp-content/uploads/2010/11/emergency2012_innsbruck.jpg" border="0" alt="Emergency 2012" title="Emergency 2012" align="right" width="140" height="100" />Emergency 2012[/caption]Publisher <strong>Deep Silver</strong> hat vor kurzem eine Probeversion des Rettungsspiels <a href="http://e2012.deepsilver.com" target="_blank">Emergency 2012</a> veröffentlicht. Die Demo bietet unter anderem das komplette Tutorial, inklusive zwei Missionen aus der Kampagne. Der Download mit insgesamt 1,7 GByte umfang steht ab sofort bereit.</p> '
featured_image: /wp-content/uploads/2010/11/emergency2012_innsbruck.jpg

---
[> Zur Emergency 2012 – Demo][1]

**Features des Spiels:  
** 

  * 12 Missionen mit packenden Großeinsätzen: Berlin, Frankfurt, Hamburg, Köln, München, Innsbruck, Zermatt und viele weitere europäische Metropolen. 
  * 3 Freeplay-Karten: Behalte in verschiedenen Szenarien wie Regen oder Sturm die Kontrolle über deine Stadt und wirtschafte klug, um dir die besten Feuerwehrfahrzeuge kaufen zu können. 
  * Online-Koop-Modus für bis zu 4 Spieler: Spiele Emergency 2012 gemeinsam mit deinen Freuden und verewige dich in den Highscore-Listen. 
  * Runderneuerte Steuerung: Emergency 2012 ist einfach und komfortabel zu bedienen. Dank eines interaktiven Tutorials auch für Einsteiger. 
  * Top-Grafik: Die Visions–Engine sorgt für die beste Emergency-Grafik aller Zeiten. Spezielle Lichteffekte lassen Brände und Blaulichter realistisch flackern und erhellen die Umgebung. 
  * Komplexe Spielwelt: Die Spielwelt von Emergency 2012 ist eine komplexe Simulation, in der unvorhergesehene Ereignisse und verschiedene Lösungswege möglich sind.

   

* * *

   



 [1]: downloads/demos/item/demos/emergency-2012--demo