---
title: Borderlands – 2K kündigt neuen DLC an
author: gamevista
type: news
date: 2010-08-11T15:38:25+00:00
excerpt: '<p>[caption id="attachment_2117" align="alignright" width=""]<img class="caption alignright size-full wp-image-2117" src="http://www.gamevista.de/wp-content/uploads/2010/08/smallgegner.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]<strong>2K Games</strong> hat den neusten Zusatzinhalt für den Ego-Shooter <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> angekündigt. Der Download-Content mit dem Namen <strong>Claptrap`s New Robot Revolution</strong> wird nächsten Monat für 800 MS-Punkte bzw. ca. 9 Euro für PlayStation 3 oder PC veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/08/smallgegner.jpg

---
Mit bis zu drei Freunden könnt ihr in den Konflikt zwischen den Robotern Claptraps und der Hyperion Corporation eingreifen. Zum Inhalt zählen neue Ausrüstungsgegenstände, weitere Gegner und Waffen. Mit mehr als 20 Quests dürfte führ reichlich Abwechslung gesorgt sein.

 

   

* * *

   

