---
title: 'Die Gilde 2: Renaissance – Neues Addon angekündigt'
author: gamevista
type: news
date: 2010-05-10T17:18:56+00:00
excerpt: '<p>[caption id="attachment_1778" align="alignright" width=""]<img class="caption alignright size-full wp-image-1778" src="http://www.gamevista.de/wp-content/uploads/2010/05/g2renaissance2.jpg" border="0" alt="Die Gilde 2: Renaissance" title="Die Gilde 2: Renaissance" align="right" width="140" height="100" />Die Gilde 2: Renaissance[/caption]Die Wirtschaftssimulation <a href="http://www.diegilde2.com" target="_blank">Die Gilde 2</a> wird ein neues Erweiterungspacket erhalten. Dies bestätigte der Publisher <strong>JoWood</strong> nun offiziell im Rahmen einer Pressemitteilung. Das Addon mit dem Namen <strong>Renaissance</strong> wird komplett ohne das Hauptspiel auskommen und damit selbständig laufen. <a href="http://www.diegilde2.com" target="_blank">Die Gilde 2: Renaissance</a> spielt in der Zeit des 14. Jahrhunderts das vom Würgegriff des Adels geprägt war.</p> '
featured_image: /wp-content/uploads/2010/05/g2renaissance2.jpg

---
Doch nun gewinnen Bürgerliche, Handwerker und Händler immer mehr an Einfluss. Es wird insgesamt neun neue Berufe geben. Darunter sind Söldner, Totengräber, Steinmetz, Gaukler, Kaschemmenwirt, Bankier, Müller, Obstbauer und Steinbrecher. Zusätzlich gibt es ein komplett überarbeitetes Titelsystem das ihr nutzen könnt um euren Ruf beim Adel zu erhöhen. Des weiteren wird die KI verbessert und es gibt wilde Tiere die durch die Spielwelt streifen.

**Neue Features**

  * **Neue Szenarien**   
    Zusätzliche Szenarien wie „Die Alpen“ und „Die freie Republik Niederlande“ ergänzen die Welt von „Die Gilde 2“

  * **Neun neue Berufe**   
    Der Spieler kann zwischen neun weiteren Berufen wählen und sich so zwischen verschiedenen Charakteren entscheiden

  * **Kartenerweiterung**   
    Die Karten sind bis zu vier Mal größer als im Original „Die Gilde 2“

  * **Krieg und Frieden**   
    In der neuen Spielwelt kann es immer wieder zum Krieg kommen, was taktisches Vorgehen des Spielers erfordert

  * **God-Measures**   
    Unterschiedliche Maßnahmen, wie Kauf eines Titels oder Amtsbewerbung, können ohne dem Beisein der Charaktere durchgeführt werden &#8211; so auch die „Zwangsehe“, was dem Spieler langwierige Liebeswerbung erspart

   

* * *

   

