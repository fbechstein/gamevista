---
title: Little Big Planet – Sony dementiert PSP Multiplayer
author: gamevista
type: news
date: 2010-05-23T13:04:40+00:00
excerpt: '<p>[caption id="attachment_1840" align="alignright" width=""]<img class="caption alignright size-full wp-image-1840" src="http://www.gamevista.de/wp-content/uploads/2010/05/littlebigplanet.jpg" border="0" alt="Little Big Planet" title="Little Big Planet" align="right" width="140" height="100" />Little Big Planet[/caption]Senior Producer Mark Green vom Publisher <strong>Sony </strong>hat im Rahmen eines Interviews bestätigt, dass die PlayStation Portable-Version von <a href="http://www.littlebigplanet.com" target="_blank">Little Big Planet</a> keinen Mehrspieler-Modus beinhalten wird.<br /> <br /></p> '
featured_image: /wp-content/uploads/2010/05/littlebigplanet.jpg

---
Bisher wurde fieberhaft an einer Lösung zum Multiplayer-Support gearbeitet, nun stellt Green fest das sobald Wi-Fi aktiviert wird, die PSP mehr als ein Drittel seiner Prozessor-Power verliert. Somit können zwar Minigames per Mehrspieler realisiert werden. Die komplexen Levels stellen die Entwickler jedoch vor ein unlösbares Problem.  <a href="http://www.littlebigplanet.com" target="_blank">Little Big Planet</a> für die PSP wurde im November 2009 veröffentlicht.

 

   

* * *

   

