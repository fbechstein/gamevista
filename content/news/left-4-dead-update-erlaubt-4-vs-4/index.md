---
title: Left 4 Dead – Update erlaubt 4 vs. 4
author: gamevista
type: news
date: 2009-11-10T17:50:50+00:00
excerpt: '<p>[caption id="attachment_1024" align="alignright" width=""]<img class="caption alignright size-full wp-image-1024" src="http://www.gamevista.de/wp-content/uploads/2009/11/l4d_small.jpg" border="0" alt="Left 4 Dead" title="Left 4 Dead" align="right" width="140" height="100" />Left 4 Dead[/caption]Der Entwickler <a href="http://www.valvesoftware.com" target="_blank">Valve</a> hat kurz vor der Veröffentlichung von <strong>Left 4 Dead 2</strong> den Fans  des ersten Teils noch ein Geschenk präsentiert. So wurde  dem ersten Left 4 Dead Teil ein Patch spendiert der unter anderem ein paar Fehler beseitigt und den lang erwarteten 4vs4 Match-Modus freischaltet. Finden könnt ihr diesen neuen Spielmodus unter dem Menüpunkt „Team Versus“.</p> '
featured_image: /wp-content/uploads/2009/11/l4d_small.jpg

---
Jetzt ist es möglich sich mit Freunden zu treffen und nach einem gegnerischen 4-Mann Team zu suchen. Wie immer wird das Update automatisch über den Onlinedienst Steam bezogen.

 

 

* * *



* * *