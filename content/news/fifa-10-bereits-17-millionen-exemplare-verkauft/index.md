---
title: Fifa 10 – Bereits 1,7 Millionen Exemplare verkauft
author: gamevista
type: news
date: 2009-10-07T11:14:44+00:00
excerpt: '<p>[caption id="attachment_115" align="alignright" width=""]<img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" />Fifa 10[/caption]Das Fussballspiel <strong>Fifa 10</strong> vom Entwickler <a href="http://www.electronic-arts.de" target="_blank">EA Sports</a> hat dieses Jahr einen neuen Rekord gebrochen. Denn das seit dem 02. Oktober erhältliche Spiel ist bereits schon über 1,7 Millionen mal über die Ladentheke gegangen, so <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a>.</p> '
featured_image: /wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg

---
Zudem sind bereits über zehn Millionen Online-Matches abgehalten worden und rund 250.000 Spieler gleichzeitig online gewesen. Dies sind sehr erfreuliche Zahlen für <a href="http://www.electronic-arts.de" target="_blank">EA</a> und wohl eine der besten Verkaufsstarts in der letzten Zeit.

 

* * *



* * *

 