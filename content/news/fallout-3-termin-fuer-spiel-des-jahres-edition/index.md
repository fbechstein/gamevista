---
title: Fallout 3 – Termin für Spiel des Jahres Edition
author: gamevista
type: news
date: 2009-09-17T18:12:16+00:00
excerpt: '<p>[caption id="attachment_260" align="alignright" width=""]<img class="caption alignright size-full wp-image-260" src="http://www.gamevista.de/wp-content/uploads/2009/08/mz_05b_small.jpg" border="0" alt="Fallout 3" title="Fallout 3" align="right" width="140" height="100" />Fallout 3[/caption]Entwickler <a href="http://www.bethsoft.com">Bethesda Softworks</a> hat heute per offizieller Pressemitteilung den Termin für die <strong>Fallout 3</strong> - <strong>Spiel des Jahres Editio</strong>n bekannt gegeben. Demnach erscheint die Spezial Edition am 15. Oktober 2009 in Deutschland. Der Preis wird mit ca. 50 Euro angegeben.</p> '
featured_image: /wp-content/uploads/2009/08/mz_05b_small.jpg

---
**Inhalt Fallout 3 – Spiel des Jahres Edition:**

 

  * Fallout 3
  * Operation Anchorage (Add-On)
  * The Pitt (Add-On)
  * Broken Steel (Add-On)
  * Point Lookout (Add-On)
  * Mothership Zeta (Add-On)

**Features:**

  * Exklusive neue Ausrüstungsgegenstände und Erfolge!
  * Neue Waffen und Gebiete!
  * Mehrere Stunden zusätzlicher Spielspaß durch die Add-Ons!
  * Level Up bis Level 30! 

* * *



* * *