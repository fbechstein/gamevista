---
title: 'Battlefield: Bad Company 2 – Limited Edition angekündigt'
author: gamevista
type: news
date: 2009-10-29T16:27:15+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Der Publisher <a href="http://www.electronic-arts.de" target="_blank">Electronic Arts</a> hat heute mit einer offiziellen Pressemitteilung angekündigt das es eine Limited Edition für den Shooter <strong>Battlefield: Bad Company 2</strong> geben wird. Diese spezielle Edition wird ab dem 4. März 2010 bei den Händlern zur Verfügung stehen und zum gleichen Preis wie die Standardedition angeboten. Natürlich nur solange der Vorrat reicht.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Die **Battlefield: Bad Company 2** Limited Edition wird unter anderem sechs Features enthalten. So wird es im Multiplayer Modus möglich sein das Militärfahrzeuge sich mir stärkerer Feuerkraft, besserem Radar und Panzerung ausstatten lassen. Ein weiteres Gimmick sind Peilsender. Diese könnt ihr an feindliche Fahrzeuge heften und euren Teamkamerade mit Panzerfaust ermöglichen, den Gegner zu verfolgen. Aus Battlefield 1943 werden die M1A1 Maschinenpistole und die M1911 Pistole übernommen.

<span style="text-decoration: underline;"><strong>Hier die komplette Liste der Features der Limited Edition im Detail:</strong></span>

  * Verbesserte Fahrzeugpanzerung: Ein Panzerungspaket für alle Fahrzeugtypen, das den durch Sprengstoffe oder panzerbrechende Sprengköpfe zugefügten Schaden verringert und so die Überlebenswahrscheinlichkeit des jeweiligen Fahrzeugs erhöht. 
  * Überlegene Fahrzeug-Feuerkraft: Zusätzliche Waffenpakete für Fahrer aller gepanzerten Fahrzeuge erhöhen die Auswahl an Zielen, die das Fahrzeug angreifen und zerstören kann. 
  * Radar: Mit diesem elektronischen Zusatzpaket können gegnerische Einheiten in unmittelbarer Nähe des Fahrzeugs lokalisiert werden. 
  * Pistole mit Peilsendern: Die magnetischen Pfeile dieser Pistole heften sich an die Oberfläche jedes Fahrzeugs, sodass Teamkameraden auch bewegliche Ziele außerhalb ihrer Sichtweite verfolgen, anvisieren und mit Raketen angreifen können. 

  * M1A1 Maschinenpistole: Diese MP wurde aus Battlefield 1943 übernommen, ist aber nach wie vor eine überzeugende Waffe. 
  * M1911 Pistole: Ebenfalls aus Battlefield 1943 stammt die M1911. Aufgrund der Durchschlagskraft der schweren .45-Kaliber-Projektile stellt sie die bevorzugte Waffe vieler Einheitentypen dar.

* * *



* * *