---
title: Natural Selection 2 – Erste Ingame Bilder veröffentlicht
author: gamevista
type: news
date: 2009-11-01T17:23:41+00:00
excerpt: '<p>[caption id="attachment_976" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/11/ns2_marine_hallway.jpg" rel="lightbox[NS2]"><img class="caption alignright size-full wp-image-976" src="http://www.gamevista.de/wp-content/uploads/2009/11/ns2_marine_hallway_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" /></a>Natural Selection 2[/caption]</p> <p>Schon lange ist der zweite Teil des Shooters <strong>Natural Selection</strong> in der Entwicklung. Doch so langsam kommt Bewegung in die Sache. An diesem Wochenende hat der Entwickler <a href="http://www.unknownworlds.com/ns2/" target="_blank">Unknown Worlds Entertainment</a> die ersten Screenshots veröffentlicht die unbearbeitete Ingame Szenen zeigen.</p> <p><br /><br /></p> '
featured_image: /wp-content/uploads/2009/11/ns2_marine_hallway_small.jpg

---
</p> <a href="http://www.gamevista.de/wp-content/uploads/2009/11/ns2_skulk_vent.jpg" rel="lightbox[NS2]"><img class="caption alignright size-full wp-image-978" src="http://www.gamevista.de/wp-content/uploads/2009/11/ns2_skulk_vent_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" /></a> 

So kann man auf den beiden Screenshots die jeweilige Sicht des Marines oder eines Skulks sehen. Weiterhin teilte <a href="http://www.unknownworlds.com/ns2/" target="_blank">Unknown Worlds Entertainment</a> per Email für alle Vorbesteller mit das sie im November den Level Editior „Spark“ veröffentlichen wollen. 

* * *



* * *