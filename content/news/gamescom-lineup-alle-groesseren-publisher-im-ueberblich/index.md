---
title: gamescom Lineup – Ein Überblick
author: gamevista
type: news
date: 2009-08-08T09:36:27+00:00
excerpt: '<p>[caption id="attachment_8" align="alignright" width=""]<img class="caption alignright size-full wp-image-8" src="http://www.gamevista.de/wp-content/uploads/2009/06/gamescom_small.png" border="0" alt="gamescom" title="gamescom" align="right" width="140" height="100" />gamescom[/caption]Da die <a href="http://www.gamescom.de" target="_blank" title="gamescom">gamescom 2009</a> ja nicht mehr weit ist und viele <strong>Entwickler</strong> bzw. <strong>Publisher</strong> nun ihre Lineups bekannt gegeben haben prästenieren wir euch nun eine kleine Liste damit ihr den Überblick behaltet. </p>'
featured_image: /wp-content/uploads/2009/06/gamescom_small.png

---
 

<p class="MsoNormal">
  <strong>Activision:</strong>
</p>

  * Modern Warfare 2
  * Bakugan
  * Singularity
  * Blur
  * Tony Hawk RIDE
  * DJ Hero
  * Band Hero
  * Guitar Hero 5

<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
  <strong>Bethesda:</strong>
</p>

  * Brink (nicht spielbar)
  * Wet (spielbar)
  * Wheelspin (spielbar)
  * Medieval Games (spielbar)

<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
  <strong>Eidos:</strong>
</p>

  * Batman: Arkham Asylum (PS3, Xbox 360)
  * Mini Ninjas (PS3, Xbox 360, Wii, NDS)
  * Pony Friends 2 (Wii, NDS)

<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
  <strong>Electronic Arts:</strong>
</p>

  * APB
  * Battlefield Bad Company 2
  * Brütal Legend
  * C&C 4
  * Dragon Age: Origins
  * Mass Effect 2
  * Need for Speed SHIFT
  * Need for Speed NITRO
  * RAGE
  * The Beatles Rock Band
  * FIFA 10
  * MySims Agents
  * TS3EP1
  * Army of Two: The 40th Day
  * Dante&#8217;s Inferno
  * Dead Space Extraction
  * The Saboteur
  * NHL 10
  * Tiger Woods Online

<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
  <strong>Sony:</strong>
</p>

  * Heavy Rain
  * Uncharted 2: Among Thieves
  * God of War III
  * MAG
  * EyePet
  * Ratchet and Clank: A Crack In Time
  * SingStar
  * Buzz!
  * White Knight Chronicles
  * DC Universe
  * Gran Turismo PSP
  * Invizimals
  * MotorStorm: Arctic Edge
  * LittleBigPlanet
  * Jak and Daxter: The Lost Frontier
  * SOCOM: Fireteam Bravo

<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
  <strong>Square Enix:</strong>
</p>

  * Dissidia Final Fantasy (PSP)
  * Final Fantasy XIII (PS3, Xbox 360)
  * Final Fantasy XIV (PS3)
  * Final Fantasy Crystal Chronicles: The Crystal Bearers (Wii)
  * Front Mission Evolved (PS3, Xbox 360)
  * Kingdom Hearts 358/2 (NDS)
  * Nier (PS3, Xbox 360)
  * Supreme Commander 2 (Xbox 360)

<p class="MsoNormal">
   
</p>

<p class="MsoNormal">
  <strong>Ubisoft:</strong>
</p>

  * Avatar
  * RUSE
  * Silent Hunter 5
  * Red Steel 2
  * Rabbids Go Home
  * Academy of Champions
  * Assassin&#8217;s Creed 2
  * Shaun White Snowboarding 2
  * Your Shape
  * COP-The Recruit
  * Might & Magic &#8211; Clash Of Heroes

<p class="MsoNormal">
   
</p>





