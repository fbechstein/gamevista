---
title: Risen – Demo zum Rollenspielhit
author: gamevista
type: news
date: 2009-10-01T23:00:00+00:00
excerpt: '<p>[caption id="attachment_805" align="alignright" width=""]<img class="caption alignright size-full wp-image-805" src="http://www.gamevista.de/wp-content/uploads/2009/10/risenruinen.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Am heutigen Tag erscheint das lang erwartete Rollenspiel <strong>Risen. </strong>Der Entwickler <a href="http://www.piranha-bytes.com/">Piranha Bytes</a> hat nun auch eine Demo dazu veröffentlicht. Mit dieser können sich noch unentschlossene Käufer einen ersten Eindruck vom <strong>Gothic </strong>Nachfolger machen. Die Demo bringt etwas über einen Gigabyte auf die Wage und kann bei uns bezogen werden.</p> '
featured_image: /wp-content/uploads/2009/10/risenruinen.png

---
Die Demo gibt es bisher nur für den PC. Ob und wann eine Risen Xbox 360 Testversion kommt ist bisher noch unbestätigt.

[> Zur Risen &#8211; Demo][1]

 

* * *



* * *

 

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=316