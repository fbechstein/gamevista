---
title: Assassin’s Creed 2 – Es wird keine Demo geben
author: gamevista
type: news
date: 2009-11-01T19:13:33+00:00
excerpt: |
  |
    <p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin's Creed 2" title="Assassin's Creed 2" align="right" width="140" height="100" />Assassin's Creed 2[/caption]Eine schlechte Nachricht für alle <strong>Assassin's Creed 2</strong> Fans. Wie heute bekannt wurde wird es keine Demo für das Actionspiel aus dem Hause <a href="http://www.ubi.com" target="_blank">Ubisoft Montreal</a> geben. Dies wurde während der EuroGamer Expo in London von einem <a href="http://www.ubi.com" target="_blank">Ubisoft</a> Mitarbeiter bestätigt.</p>
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Schon beim ersten Teil von **Assassin&#8217;s Creed** verfolgte der Entwickler nicht das Ziel eine Demo anzubieten, was Fans sehr enttäuschte. **Assassin&#8217;s Creed 2** wird im ersten Quartal 2010 im Handel erscheinen.

 

* * *



* * *

 