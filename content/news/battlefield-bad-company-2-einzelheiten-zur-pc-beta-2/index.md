---
title: Battlefield 1943 – PC Version verspätet sich erneut
author: gamevista
type: news
date: 2010-01-11T16:27:23+00:00
excerpt: '<p>[caption id="attachment_341" align="alignright" width=""]<img class="caption alignright size-full wp-image-341" src="http://www.gamevista.de/wp-content/uploads/2009/08/battle1943.png" border="0" alt="Battlefield 1943" title="Battlefield 1943" align="right" width="140" height="100" />Battlefield 1943[/caption]Karl Magnus, Produzent im Team von <a href="http://www.battlefield1943.com" target="_blank">DICE</a>, bestätigte per Twitter das die PC-Version des Ego-Shooters <strong>Battlefield 1943</strong> erst nach dem Release von <strong>Battlefield: Bad Company 2</strong> auf dem Markt kommen wird. Demnach wird <strong>Battlefield 1943</strong> aller Vorrausicht nach nicht vor März 2010 veröffentlicht.</p> '
featured_image: /wp-content/uploads/2009/08/battle1943.png

---
Die Konsolenversion des Multiplayer-Shooters ist nun schon mehrere Monate verfügbar. Laut Karl Magnus konzentriert sich <a href="http://www.battlefield1943.com" target="_blank">DICE </a>voll auf die Veröffentlichung von **Battlefield: Bad Company 2**, man habe **Battlefield 1943** aber nicht vergessen.

 

<cite></cite>

* * *



* * *

 