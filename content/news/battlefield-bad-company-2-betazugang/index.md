---
title: 'Battlefield: Bad Company 2 – Betazugang'
author: gamevista
type: news
date: 2009-11-12T12:49:13+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2 " title="Battlefield: Bad Company 2 " align="right" width="140" height="100" />Battlefield: Bad Company 2 [/caption]Der Entwickler <a href="http://www.ea.com/games/battlefield-bad-company-2" target="_blank">DICE</a> gab bereits letzte Woche bekannt das in Kürze ein Beta-Test für den Ego-Shooter <strong>Battlefield: Bad Company 2</strong> exklusiv auf der Spielekonsole PlayStation 3 starten würde. So können Fans des Shooters entweder die Vollversion des Spiels vorbestellen oder ihr Glück auf der <a href="http://badcompany2.ea.com/#/beta" target="_blank">offiziellen Webseite</a> des Entwicklers auf die Probe stellen um an einen der heißbegehrten Plätze zu kommen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Ab dem 19. November 2009 wird die Beta offiziell gestartet. <a href="http://www.ea.com/games/battlefield-bad-company-2" target="_blank">DICE</a> gab nun auch neue Details zur PC-Version bekannt. So wird berichtet, dass der Microsoft Dienst _Games for Windows Live_ vom Spiel ausgeschlossen bleiben wird.

> <a href="http://badcompany2.ea.com/#/beta" target="_blank">Zur Betaanmeldung</a>

 

* * *



* * *