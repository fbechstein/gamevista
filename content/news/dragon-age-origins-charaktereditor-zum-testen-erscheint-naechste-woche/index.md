---
title: 'Dragon Age: Origins – bald Charaktereditor zum Testen'
author: gamevista
type: news
date: 2009-10-04T12:47:36+00:00
excerpt: '<p>[caption id="attachment_814" align="alignright" width=""]<img class="caption alignright size-full wp-image-814" src="http://www.gamevista.de/wp-content/uploads/2009/10/dragon_small.png" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Entwickler <a href="http://www.bioware.com" target="_blank">BioWare </a>hat nun offiziell bestätigt das es eine Testversion des Charakter Editors vor dem Release von <strong>Dragon Age: Origins</strong> geben wird. Dies hat <a href="http://www.bioware.com" target="_blank">BioWares </a>Community Coordinator Chris Priestly im <a href="http://daforums.bioware.com/viewtopic.html?topic=696540&forum=135" target="_blank">offiziellen Forum</a> des Rollenspiels bestätigt.</p> '
featured_image: /wp-content/uploads/2009/10/dragon_small.png

---
Der kostenlose Download dazu wird am 13. Oktober verfügbar sein. Die Probierversion des Editors wird dem im Spiel enthaltenen Charakter Editor gleichen. Mit der Vorabversion will <a href="http://www.bioware.com" target="_blank">BioWare </a>den Fans des Rollenspiels die Möglichkeit geben die umfangreichen Funktionen bis zum Release von Dragon Age: Origins ausführlich zu testen. Weiterhin wird berichtet das am 13. Oktober auch eine neue Community Webssite online gehen wird. Auf dieser wird es möglich sein die erstellten Charaktere Hochzuladen.

 

* * *



* * *

 