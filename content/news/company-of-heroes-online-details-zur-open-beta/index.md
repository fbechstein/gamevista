---
title: Company of Heroes Online – Details zur Open Beta
author: gamevista
type: news
date: 2010-08-30T20:05:02+00:00
excerpt: '<p>[caption id="attachment_2185" align="alignright" width=""]<img class="caption alignright size-full wp-image-2185" src="http://www.gamevista.de/wp-content/uploads/2010/08/coh.jpg" border="0" alt="Company of Heroes Online" title="Company of Heroes Online" align="right" width="140" height="100" />Company of Heroes Online[/caption]Bald endet die geschlossene Beta-Phase zum Echtzeit-Strategietitel <a href="http://www.companyofheroes.com" target="_blank">Company of Heroes Online</a> und geht damit nahtlos über in ein eine offene Beta-Phase. Dies kündigte der Publisher <strong>THQ </strong>auf der <a href="http://www.companyofheroes.com" target="_blank">offiziellen Website</a> des Spiels an. Bereits diese Woche soll es soweit sein.</p> '
featured_image: /wp-content/uploads/2010/08/coh.jpg

---
Nach dem zurücksetzen der Statistiken, wird es für alle Interessierte einen Client zum Herunterladen geben. Einen genauen Termin hat **THQ** bisher leider nicht genannt. Eine Veröffentlichung zum Wochenende ist aber denkbar. Damit geht die Entwicklungsphase des kostenlosen Titels, der sich über sogenannte Micropayments finanziert, in eine heiße Phase.    
<a href="http://www.companyofheroes.com" target="_blank">Company of Heroes Online</a> spielt sich ähnlich wie seinem Vorgänger **Company of Heroes**. Das im Jahr  2007 erschienene Strategiespiel überzeugt mit neuen Ideen, klasse Grafik und vielen taktischen Möglichkeiten. <a href="http://www.companyofheroes.com" target="_blank">Company of Heroes Online</a> bietet, anders als bei seinem Vorgänger, Heldeneinheiten und Upgrade-Möglichkeiten für eure Truppen.

 

   

* * *

   



 