---
title: Crysis 2 – Release erst nächstes Jahr
author: gamevista
type: news
date: 2009-07-26T13:09:07+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption] <span class="caption">Wie <a href="http://www.crytek.com" target="_blank" title="crytek">Crytek</a> nach langem hin und her nun offiziell bekannt gegeben hat wird es der Release von <strong>Crysis 2 </strong>wohl nicht mehr in dieses Jahr schaffen. <a href="http://www.crytek.com" target="_blank" title="crytek">Crytek </a>Community Manager „Cry McGinge“, Nickname im Crytek Forum, bestätigte dies nun und verwies auf das Jahr 2010.<br /></span></p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
<span class="caption">Wir hoffen das im Rahmen der gamescom neue Details ans Tageslicht kommen. Seit der E3  im Juni gab es relativ wenig neues vom Ego Shooter <strong>Crysis 2</strong>, Spielszenen oder genauere Details lassen auf sich warten. Sobald etwas Neues auf der <a href="http://www.gamescom.de" target="_blank" title="gamescom">gamescom </a>auftaucht, berichten wir natürlich darüber.</span>







