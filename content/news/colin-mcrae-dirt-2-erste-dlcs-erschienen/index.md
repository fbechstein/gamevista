---
title: 'Colin McRae: DiRT 2 – Erste DLCs erschienen'
author: gamevista
type: news
date: 2009-10-17T18:07:50+00:00
excerpt: '<p>[caption id="attachment_386" align="alignright" width=""]<img class="caption alignright size-full wp-image-386" src="http://www.gamevista.de/wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg" border="0" alt="Colin McRae: DiRT 2" title="Colin McRae: DiRT 2" align="right" width="140" height="100" />Colin McRae: DiRT 2[/caption]Der Entwickler <a href="http://www.codemasters.de" target="_blank">Codemasters</a> hat für das Rennspiel<strong> Colin McRae: DiRT 2</strong> die ersten kostenpflichtigen Zusatzinhalte als Download angeboten. Für 400 MS Punkte bzw. 3,99 Euro im PlayStation Store erhaltet ihr ein „<strong>Access All Areas</strong>“ Paket.</p> '
featured_image: /wp-content/uploads/2009/08/colin_mcrae_dirt2.jpg

---
Dieses ermöglicht euch auf alle Strecken und Fahrzeuge zuzugreifen ohne **DiRT 2** durchgespielt zu haben. Das zweite Paket mit dem Namen „**Trust Fund Pack**“ schaltet alle Fahrzeuge, Updates und Lackierungen frei, ohne das ihr die DiRT-Tour bestehen müsst. Ebenfalls kostet der Download-Content 400 MS Punkte oder 3,99 Euro für die PS3.

 

* * *



* * *

 