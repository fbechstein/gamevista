---
title: Risen – Neues Update veröffentlicht
author: gamevista
type: news
date: 2010-04-21T19:17:20+00:00
excerpt: '<p>[caption id="attachment_1713" align="alignright" width=""]<img class="caption alignright size-full wp-image-1713" src="http://www.gamevista.de/wp-content/uploads/2010/04/risencombos.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Risen[/caption]Der Entwickler <strong>Piranha Bytes</strong> stellte heute einen neuen Patch für das Rollenspiel <a href="http://www.risen.deepsilver.com" target="_blank">Risen</a> zur Verfügung. Neben diversen Verbesserungen wurden auch viele Fehler behoben die unter anderem dazu führten, dass Quests nicht so abliefen wie sie sollten. Das Update wird automatisch beim Start von Steam heruntergeladen.</p> '
featured_image: /wp-content/uploads/2010/04/risencombos.png

---
Die komplette Liste der Änderungen sieht wie folgt aus:

<ul style="padding-bottom: 0px; margin-bottom: 0px;">
  <li>
    Placement of various objects in the world revised
  </li>
  <li>
    Collision revised: player could previously get stuck in certain locations
  </li>
  <li>
    Problem with Brogar Arena quest resolved: this quest was only offered twice in certain circumstances
  </li>
  <li>
    Problem with the camera and the interface after loading a saved game resolved
  </li>
  <li>
    Various locations revised where the player was teleported to the first floor upon entering a building
  </li>
  <li>
    The hunters are to hunt again: problem with a dialog option that immediately ended this quest resolved
  </li>
  <li>
    Problem with the trigger for the final battle resolved
  </li>
  <li>
    Golden sword for the Don: this sword could be duplicated by stealing
  </li>
  <li>
    Problem with signpost lighting resolved
  </li>
  <li>
    Position of various quest goals revised and improved
  </li>
  <li>
    Problem of the player not receiving the bastard sword after delivering the materials for it resolved
  </li>
  <li>
    Interaction with benches, chairs, etc. revised
  </li>
  <li>
    Various dialog options revised
  </li>
  <li>
    Various error corrections made in game texts and language files
  </li>
</ul>

   

* * *

* * *

 __

 