---
title: 'Battlefield: Bad Company 2 – Koop –Modus für Konsolen'
author: gamevista
type: news
date: 2010-05-13T19:13:05+00:00
excerpt: '<p>[caption id="attachment_1794" align="alignright" width=""]<img class="caption alignright size-full wp-image-1794" src="http://www.gamevista.de/wp-content/uploads/2010/05/smallbfbc27.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Wie der Publisher <strong>Electronic Arts</strong> per Pressemitteilung verkündet hat, wird der Multiplayer-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> einen neuen Spielmodus erhalten. Bisher ist der Koop-Modus mit dem Namen <strong>Onslaught </strong>nur für die Konsolen Xbox 360 und PlayStation 3 geplant. Ob PC Besitzer auch in den Genuss kommen werden ist bisher unklar.</p> '
featured_image: /wp-content/uploads/2010/05/smallbfbc27.jpg

---
Im Onslaught-Modus kämpfen bis zu vier Spieler zusammen auf den Mehrspieler-Karten Valparaiso, Atacama Desert, Isla Inocentes und Nelson Bay gegen computergesteuerte Soldaten.   
„Wir sind begeistert, Battlefield: Bad Company 2 mit etwas Neuem, Frischem zu bereichern, ohne das für Battlefield typische Gameplay zu verändern&#8220;, erklärt Patrick Bach, Senior Producer bei **DICE**. „Wir wollen den über 4 Millionen Spielern permanent neue Multiplayer-Inhalte bieten, aber auch Elite-Spielern neue Möglichkeiten eröffnen, um ihre Fähigkeiten online zu verbessern. Effizientes Squad-Play ist bei Multiplayer-Matches von Battlefield: Bad Company 2 der Schlüssel zum Erfolg, und dieser neue Modus bietet Spielern eine geeignete Umgebung, ihre Team- und Rang-Fähigkeiten mit denen der Konkurrenz zu messen.&#8220;

Ein Veröffentlichungstermin ist bisher nicht bekannt.

 

   

* * *

   

* * *