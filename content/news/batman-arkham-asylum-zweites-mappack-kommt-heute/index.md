---
title: 'Batman: Arkham Asylum – Zweites Mappack kommt heute'
author: gamevista
type: news
date: 2009-09-23T23:00:00+00:00
excerpt: '<p>[caption id="attachment_762" align="alignright" width=""]<img class="caption alignright size-full wp-image-762" src="http://www.gamevista.de/wp-content/uploads/2009/09/batman_arkham_asylum_small.jpg" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Batman: Arkham Asylum[/caption]Es hatte sich ja schon angekündigt, heute wird für das Actionspiel <strong>Batman: Arkham Asylum</strong> ein zweites Mappack mit dem Namen <strong>Prey in the Darkness</strong> veröffentlicht. Enthalten sind zwei neue Karten für den Challenge Modus.</p> '
featured_image: /wp-content/uploads/2009/09/batman_arkham_asylum_small.jpg

---
  * Heart of Darkness (Freeflow Combat Challenge)
  * Hothouse Prey (Stealth Challenge)

Erhältlich wird das Paket für die Xbox 360 und PlayStation 3 Konsolen sein. Wie immer kostenlos zum Download. Ob PC Besitzer auch in den Genuss kommen werden ist bisher noch unklar.

* * *



* * *

 