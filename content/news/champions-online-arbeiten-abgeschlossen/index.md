---
title: Champions Online – Arbeiten abgeschlossen
author: gamevista
type: news
date: 2009-08-16T16:29:18+00:00
excerpt: '<p>[caption id="attachment_122" align="alignright" width=""]<img class="caption alignright size-full wp-image-122" src="http://www.gamevista.de/wp-content/uploads/2009/07/champions_online_small.jpg" border="0" alt="Champions Online" title="Champions Online" align="right" width="140" height="100" />Champions Online[/caption]Bald ist es soweit. Wie Entwickler <a href="http://www.crypticstudios.com" target="_blank">Cryptic Studios</a> nun offiziell bekannt gab, sind die Arbeiten am Online Rollenspiel <strong>Champions Online</strong> abgeschlossen und das Ganze geht nun in die Presswerke. Demnach steht einem pünktlichen Release am 1. September 2009 nichts im Wege.</p> '
featured_image: /wp-content/uploads/2009/07/champions_online_small.jpg

---
Wem das noch zu lange ist der kann ab dem kommenden Montag, 17. August 2009, einen Blick in den offenen Beta Test riskieren. Die Anmeldung findet ihr hier auf der <a href="http://www.champions-online.com/" target="_blank">offiziellen Website</a>.

 







 

 