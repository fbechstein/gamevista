---
title: 'Arcania: Gothic 4 – Werde Arcania Gaming Correspondent'
author: gamevista
type: news
date: 2010-05-13T19:35:12+00:00
excerpt: '<p>[caption id="attachment_1797" align="alignright" width=""]<img class="caption alignright size-full wp-image-1797" src="http://www.gamevista.de/wp-content/uploads/2010/05/held_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Der Publisher <strong>JoWood Entertainment</strong> kündigte heute per Pressemitteilung ein ganz besonderen Event für alle Fans des Rollenspiels <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> an. Demnach sucht der Publisher für die kommende GAMESCOM 2010 in Köln einen fähigen Fan, der mit Videokamera bewaffnet, als „Arcania Gaming Correspondent“ Spieleentwickler interviewt, Industrie-Insider und Journalisten trifft und das Ergebnis auf dem offiziellen Arcania-Blog veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/05/held_small.png

---
 

Diesen dürft ihr selbst managen und mit neuen Texten, Bildern und Videos versehen. Alles was Ihr tun müsst ist ein maximal einminütiges Video zu drehen und **JoWood** zu erklären warum gerade Ihr der **Arcania Gaming Correspondent** sein wollt. Der Gewinner wird zur GAMESCOM 2010 in Köln eingeladen und erhält ein Laptop und eine Kamera. Weitere Informationen findet ihr unter folgendem <a href="http://www.veoh.com/browse/videos/category/technology_and_gaming/watch/v20084034ABmXxbhN" target="_blank">Link</a>.

Der Gewinner erhält:

  * Berichte im ArcaniA Blog live von der GamesCom! 
  * Eine ArcaniA Flip Video Cam
  * Ein ArcaniA Dell Notebook
  * Exclusivinterviews mit ArcaniA Produzenten und weiteren Brancheninsidern
  * Eintrittskarte, Backstagepass sowie Gratisunterkunft zur GamesCom 2010

   

* * *

   

