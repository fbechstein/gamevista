---
title: Bioshock 2 – Protector Trails DLC für PC verschoben
author: gamevista
type: news
date: 2010-08-08T16:21:16+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Entwickler <strong>Bioware </strong>hat nun offiziell bestätigt das der nächste Zusatzinhalt <strong>Protector Trails</strong>, für den Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a>, auf unbestimmte Zeit verschoben werden musste. Bisher hat der Publisher <strong>2K Games</strong> keinen Grund für die Verschiebung der PC-Version bekannt gegeben, während der DLC bereits für PlayStation 3 und Xbox 360 erhältlich ist.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Gestern wurde der schon der nächste Download-Content angekündigt. **Minerva’s Den** wird diesen Herbst für PlayStation 3, Xbox 360 und PC erscheinen.

   

* * *

   



 