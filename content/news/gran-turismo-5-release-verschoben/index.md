---
title: Gran Turismo 5 – Release verschoben
author: gamevista
type: news
date: 2010-10-13T18:57:53+00:00
excerpt: '<p>[caption id="attachment_2339" align="alignright" width=""]<img class="caption alignright size-full wp-image-2339" src="http://www.gamevista.de/wp-content/uploads/2010/10/gt5.jpg" border="0" alt="Gran Turismo 5" title="Gran Turismo 5" align="right" width="140" height="100" />Gran Turismo 5[/caption]Publisher <strong>Sony </strong>hat den Veröffentlichungstermin für das Rennspiel <a href="http://eu.gran-turismo.com" target="_blank">Gran Turismo 5</a> ein weiteres Mal verschoben. Dies bestätigte Produzent Taku Imasaki in einer heutigen Meldung auf dem <a href="http://blog.de.playstation.com/2010/10/13/gran-turismo-5-release/">PlayStation Blog</a>. Der ursprüngliche Release-Termin lag auf dem 5. November 2010 und wurde nun, laut dem japanischen Publisher, auf die nächste Urlaubs-Saison verschoben.</p> '
featured_image: /wp-content/uploads/2010/10/gt5.jpg

---
Demnach dürfte der fünfte Teil immerhin noch dieses Jahr erscheinen. Als Grund gaben die Entwickler die Vorgabe an „ein perfektes Spielerlebnis“ erschaffen zu wollen. Weitere Informationen sollen bald folgen.

 

   

* * *

   



 