---
title: Neues Medal of Honor angekündigt
author: gamevista
type: news
date: 2009-12-02T19:51:07+00:00
excerpt: '<p>[caption id="attachment_1135" align="alignright" width=""]<img class="caption alignright size-full wp-image-1135" src="http://www.gamevista.de/wp-content/uploads/2009/12/moh.jpg" border="0" alt="Medal of Honor" title="Medal of Honor" align="right" width="140" height="100" />Medal of Honor[/caption]<a href="http://www.electronic-arts.de/" target="_blank">EA</a> hat nun das Gerücht rund um einen neuen Teil der <strong>Medal of Honor</strong> - Serie bestätigt. So soll im Herbst 2010 ein neuer Teil erscheinen. Interessant ist hierbei vor allem das Setting und die gewählten Entwickler.</p> '
featured_image: /wp-content/uploads/2009/12/moh.jpg

---
Das Spiel spielt in Afghanistan, was viele wohl direkt an Call of Duty Modern Warfare und momentan auch den zweitel Teil <a href="hot-spots/review-call-of-duty-modern-warfare-2-pc-2" target="_blank">(> Review)</a> erinnert. Der Spieler wird die Rolle eines Soldaten von Tier-1 übernehmen, was eine Spezialeinheit ist, die direkt der Oberbefehl der USA unterstellt ist.

Die Entwicklung des Singleplayerparts übernimmt, wie auch bei den letzten Teilen, das Studio EA Los Angeles. Der Multiplayerpart wird hingegen von <a href="http://www.dice.se/" target="_blank">DICE</a> entwickelt, welche unter anderem für die **Battlefield**-Serien bekannt sind.

Der Titel wird (soweit bis jetzt bekannt) einfach nur **Medal of Honor** lauten.

 







 