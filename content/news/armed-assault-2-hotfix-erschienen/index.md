---
title: Armed Assault 2 – Hotfix erschienen
author: gamevista
type: news
date: 2009-12-28T19:43:49+00:00
excerpt: '<p>[caption id="attachment_293" align="alignright" width=""]<img class="caption alignright size-full wp-image-293" src="http://www.gamevista.de/wp-content/uploads/2009/08/arma2_small.png" border="0" alt="Armed Assault 2" title="Armed Assault 2" align="right" width="140" height="100" />Armed Assault 2[/caption]Der Entwickler <a href="http://www.arma2.com" target="_blank">Bohemia Interactive</a> hat einen Hotfix für den erschienen Patch 1.05 zum Ego-Shooter <strong>Armed Assault 2</strong> veröffentlicht. Das Update mit 65 Megabyte Umfang behebt einen Fehler der in Verbindung mit dem Abspann auftreten kann. Außerdem wird ein Fehler der beim Einsteigen in Fahrzeuge auftrat behoben.</p> '
featured_image: /wp-content/uploads/2009/08/arma2_small.png

---
[> Zum Armed Assault 2 – Hotfix Download][1]

 

 

* * *



* * *

 

 [1]: downloads/patches/item/patches/armed-assault-2-hotfix-fuer-patch-105