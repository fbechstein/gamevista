---
title: 'Uncharted 2: Unter Dieben – Neuer Multiplayer Modus'
author: gamevista
type: news
date: 2009-07-24T13:01:51+00:00
excerpt: '<p>[caption id="attachment_39" align="alignright" width=""]<img class="caption alignright size-full wp-image-39" src="http://www.gamevista.de/wp-content/uploads/2009/07/uncharted2_small.png" border="0" alt="Uncharted 2" title="Uncharted 2" align="right" width="140" height="100" />Uncharted 2[/caption]<span style="font-style: normal; font-weight: normal"><span class="caption">Für das Action-Adventure <strong>Uncharted 2: Unter Dieben</strong> wird es einen neuen Multiplayer Modus geben. Das bestätigte Entwickler <a href="http://www.naughtydog.com/" target="_blank" title="naughty dog">Naughty Dog</a> auf der Comic Con in San Diego. </span><br /></span></p>'
featured_image: /wp-content/uploads/2009/07/uncharted2_small.png

---
In unserer Videosektion erhaltet Ihr ein neues Video hierzu. 

[> zum Uncharted 2 &#8211; Gold Rush Walkthrough Trailer][1] 







 [1]: videos/item/root/uncharted-2-unter-dieben-gold-rush-walkthrough