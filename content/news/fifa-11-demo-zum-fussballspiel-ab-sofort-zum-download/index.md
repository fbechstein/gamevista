---
title: FIFA 11 – Demo zum Fussballspiel ab sofort zum Download
author: gamevista
type: news
date: 2010-09-16T14:59:31+00:00
excerpt: '<p>[caption id="attachment_1898" align="alignright" width=""]<img class="caption alignright size-full wp-image-1898" src="http://www.gamevista.de/wp-content/uploads/2010/06/fifa11.jpg" border="0" alt="FIFA 11" title="FIFA 11" align="right" width="140" height="100" />FIFA 11[/caption]Fast zeitgleich mit der Pro Evolution Soccer 2011 Demo des japanischen Publishers <strong>Konami</strong>, hat <strong>Electronic Arts</strong> sein Pendant von <a href="http://fifa.easports.com" target="_blank">FIFA 11</a> veröffentlicht. Die Testversion steht damit ab sofort für Xbox 360, PlayStation 3 und PC zur Verfügung.</p> '
featured_image: /wp-content/uploads/2010/06/fifa11.jpg

---
Damit kann der geneigte Fussballfan die Mannschaften von FC Chelsea, FC Barcelona, Juventus Turin, Real Madrid, Olympique Lyon oder Bayer Leverkusen mit der diesjährigen Fassung über den Rasen jagen.

[> Zur FIFA 11 &#8211; Demo][1]

 

   

* * *

   



 [1]: downloads/demos/item/demos/fifa-11-demo