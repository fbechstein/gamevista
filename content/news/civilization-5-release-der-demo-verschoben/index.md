---
title: Civilization 5 – Release der Demo verschoben
author: gamevista
type: news
date: 2010-08-28T13:36:57+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Die Demoversion für das Rundenstrategiespiel <a href="http://www.civilization5.com" target="_blank">Civilization 5</a> sollte ursprünglich noch vor der Veröffentlichung des fünften Teils erscheinen. Nun hat der Entwickler <strong>Firaxis </strong>den Demo-Termin verschoben. Am 21. September 2010 wird gleichzeitig mit dem USA-Start von <a href="http://www.civilization5.com" target="_blank">Civilization 5</a> die Demoversion veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Hierzulande wird der fünfte Teil am 24. September 2010 für PC im Handel erscheinen.

 

 

   

* * *

   

