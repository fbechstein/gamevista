---
title: Fraktionswechsel in WoW möglich
author: gamevista
type: news
date: 2009-09-03T15:06:43+00:00
excerpt: '<p>[caption id="attachment_163" align="alignright" width=""]<img class="caption alignright size-full wp-image-163" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="139" height="100" />World of Warcraft[/caption]Schon vor einer ganzen Weile hat <a href="http://eu.blizzard.com/de/" target="_top">Blizzard</a> bekannt gegeben, dass man bald die Möglichkeit bieten will, dass der Spieler die Fraktion seines Charakters wechseln kann. In den Foren stieß das teilweise auf Unverständnis und teilweise auf offene Arme.</p> <p>Seit Mittwoch, den 2. September, kann man diesen kostenpflichtigen Dienst nun in Anspruch nehmen.</p> '
featured_image: /wp-content/uploads/2009/07/wow_small.png

---
Dabei kostet der Wechsel eines Charakters 30 US-Dollar (etwa 25 €). Einziges Manko ist wohl, dass man die Erfolge beim &#8222;Argentumturnier&#8220; komplett verliert &#8211; ansonsten soll aber nichts verloren gehen.

Wahrscheinlich wird dieser Dienst auch bald auf den europäischen Servern angeboten. Den genauen Preis für den europäischen Raum hat Blizzard noch nicht verkündet. Hingegen wurde noch ein weiterer Dienst angekündigt. So soll man demnächst auch die Rasse des Charakters ändern können, die Fraktion und Klasse bleibt dann unverändert.

Wer von euch also schon immer einen Charakter auf der Seite der Horde spielt und wissen wollte, wie sich die andere Seite spielt, kann das bald mit selbigem Charakter tun.

* * *



* * *