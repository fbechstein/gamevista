---
title: Civilization 5 – Zwei neue DLC`s bringen weitere Zivilisationen
author: gamevista
type: news
date: 2010-10-19T20:06:22+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Entwickler<strong> Firaxis Games</strong> kündigte heute zwei neue Zusatzinhalte für das Strategiespiel <a href="http://www.civilization5.com" target="_blank">Civilization 5</a> an. Beide DLC`s sollen im Oktober 2010 erscheinen und bringen beide jeweils eine neue Zivilisation und Einheiten sowie Szenarien auf die Mattscheibe.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Der Download-Content mit dem Namen **Mongols Civilization and Scenario Pack** wird komplett kostenlos sein und neben den Mongolen auch zusätzliche Szenarien bieten. Das kostenpflichtige Zusatzpack mit dem Namen **Babylonian Civilization Pack** fügt neben der babylonischen Zivilisation neue Einheiten hinzu. Der DLC kostet allerdings 4,99 USD.

 

   

* * *

   

