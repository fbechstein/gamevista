---
title: 'Starcraft 2: Wings of Liberty – Charakter umbenennen ab sofort möglich'
author: gamevista
type: news
date: 2010-11-03T17:08:29+00:00
excerpt: '<p>[caption id="attachment_1752" align="alignright" width=""]<img class="caption alignright size-full wp-image-1752" src="http://www.gamevista.de/wp-content/uploads/2010/05/starcraft2_small.jpg" border="0" alt="Starcraft 2: Wings of Liberty" title="Starcraft 2: Wings of Liberty" align="right" width="140" height="88" />Starcraft 2: Wings of Liberty[/caption]Entwickler <strong>Blizzard </strong>hat Spielern des Strategietitels <a href="hot-spots/starcraft-2-wings-of-liberty-review" target="_blank">Starcraft 2: Wings of Liberty</a> einen Namenswechsel ihres Charakters im Battle.net zu ändern. Um die Umbenennung zu starten müsst ihr euch lediglich im Battle.net einloggen und euren Account-Manager aufrufen. Darunter findet ihr den Button Charakterbenennung die ihr kostenlos und einmalig nutzen könnt.</p> '
featured_image: /wp-content/uploads/2010/05/starcraft2_small.jpg

---
Habt ihr den Vorgang abgeschlossen, dann logt euch in Starcraft 2 ein und wählt einen neuen Namen aus.

 

 __

* * *

 __  



 