---
title: 'Alien Breed 2: Assault – Erscheint diese Woche'
author: gamevista
type: news
date: 2010-09-20T17:20:29+00:00
excerpt: '<p>[caption id="attachment_2252" align="alignright" width=""]<img class="caption alignright size-full wp-image-2252" src="http://www.gamevista.de/wp-content/uploads/2010/09/alienbreed2.gif" border="0" alt="Alien Breed 2: Assault" title="Alien Breed 2: Assault" align="right" width="140" height="100" />Alien Breed 2: Assault[/caption]Das Entwicklerteam von <a href="http://www.team17.com/" target="_blank">Team 17</a> wird an diesem Mittwoch den zweiten Teil der Alien Breed-Reihe veröffentlicht. <strong>Alien Breed 2: Assault</strong> wird für PC über die Onlineplattform Steam und für die Xbox 360 über den Xbox Live Marktplatz vertrieben. Bisher gaben die Entwickler keine Preisangabe bekannt.</p> '
featured_image: /wp-content/uploads/2010/09/alienbreed2.gif

---
Assault, der zweite Teil der Alien Breed-Reihe, wird einen Survivor-Mode bieten in denen ihr Welle für Welle in isometrischer Sicht, am besten im Koop mit Freunden, überleben müsst.

Weiterhin wird der zweite Teil mit neue Waffen, Alientypen und Gebiete aufwarten. **Alien Breed 2: Assault** wird am 22. September 2010 für PC und Xbox 360 veröffentlicht.

   

* * *

   

