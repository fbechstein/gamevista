---
title: Uncharted 2 – Releasetermin steht fest
author: gamevista
type: news
date: 2009-07-22T11:53:47+00:00
excerpt: '<p>[caption id="attachment_101" align="alignright" width=""]<img class="caption alignright size-full wp-image-101" src="http://www.gamevista.de/wp-content/uploads/2009/07/uncharted2_small.png" border="0" alt="Uncharted 2 - Releasetermin steht fest" title="Uncharted 2 - Releasetermin steht fest" align="right" width="140" height="100" />Uncharted 2 - Releasetermin steht fest[/caption]Pure Freude werden alle PS3 Besitzer haben. <a href="http://www.sony.de/" target="_blank">Sony</a> veröffentlichte den Releasetermin für das Actionspektakel <strong>Uncharted 2: Unter den Dieben</strong>. Demnach erscheint das Spiel in den USA am 13. Oktober 2009 und bei uns 10 Tage später, nämlich am 23. Oktober 2009. </p>'
featured_image: /wp-content/uploads/2009/07/uncharted2_small.png

---
Es wird ein heisser Oktober werden, denn neben Uncharted 2 erscheinen noch im Oktober Venetica für die Xbox 360, Risen für den PC und die Xbox 360 und natürlich Dragon Age: Origins für PC, Xbox 360 und PS3. 





