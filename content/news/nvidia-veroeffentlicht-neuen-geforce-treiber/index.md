---
title: NVIDIA – Veröffentlicht neuen GeForce Treiber
author: gamevista
type: news
date: 2010-01-19T20:09:16+00:00
excerpt: '<p>[caption id="attachment_1334" align="alignright" width=""]<img class="caption alignright size-full wp-image-1334" src="http://www.gamevista.de/wp-content/uploads/2010/01/nvidia-logo.jpg" border="0" alt="NVIDIA" title="NVIDIA" align="right" width="140" height="101" />NVIDIA[/caption]Der Entwickler <strong>NVIDA </strong>hat heute einen neuen Grafikkartentreiber veröffentlicht.  Dabei handelt es sich um den <strong>GeForce 196.21 WHQL Treiber</strong>. Dieser läuft auf allen Windows XP-. Vista- und Windows 7-Versionen.</p> '
featured_image: /wp-content/uploads/2010/01/nvidia-logo.jpg

---
 

Den neusten **GeForce Treiber** könnt ihr auf der <a href="http://www.nvidia.de/object/win7_winvista_32bit_196.21_whql_de.html" target="_blank">Webseite</a> von NVIDIA downloaden.  
<span style="text-decoration: underline;"><strong><br />Neu in Version 196.21:</strong></span>

  * Unterstützung von SLI und mehrerer Grafikprozessoren nun für viele neue Topspiele, unter anderem Avatar (Demo), Battlefield: Bad Company 2, City Bus Simulator, DiRT 2, Ferrari Virtual Race, GREED: Black Border, Mass Effect 2, Mortal Online, Ninja Blade, Operation Flashpoint: Dragon Rising (Demo), Planet 51, R.U.S.E., Serious Sam HD und Wings of Prey. 
  * Upgrade der PhysX-System-Software auf Version 9.09.1112. 
  * Zahlreiche Bugs behoben. Nähere Informationen zu den wichtigsten Fehlerkorrekturen in dieser Programmversion finden Sie in den Versionshinweisen auf der Dokumentations-Registerkarte.

* * *



* * *