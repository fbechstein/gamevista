---
title: Natural Selection 2 – Entwickler stellen erste Alpha-Demo vor
author: gamevista
type: news
date: 2010-04-12T17:49:19+00:00
excerpt: '<p>[caption id="attachment_348" align="alignright" width=""]<img class="caption alignright size-full wp-image-348" src="http://www.gamevista.de/wp-content/uploads/2009/08/ns2_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Um Fans des Strategie-Shooters <a href="http://www.naturalselection2.com" target="_blank">Natural Selection 2</a> am Entwicklungsprozess teilhaben zu lassen, versorgten die Entwickler <strong>Unknown Worlds</strong> in letzter Zeit die Öffentlichkeit mit jeder Menge Bildmaterial und einem Mapeditor zum bald erscheinenden zweiten Teil. Wie üblich gibt es dann noch Videos und eine lauffähige Demo zu einem neuen Spieleprojekt.</p> '
featured_image: /wp-content/uploads/2009/08/ns2_small.jpg

---
Eine sehr frühe Version einer  Demo wurde nun auf der <a href="http://www.unknownworlds.com/ns2/" target="_blank">offiziellen Website</a> von <a href="http://www.naturalselection2.com" target="_blank">Natural Selection 2</a> bereit gestellt. So können leider bisher nur Vorbesteller in den Genuss der doch recht kurzen Testversion kommen. In der Engine-Test Demo kann man auf zwei verschiedenen Karten umherlaufen und die neue Grafik-Engine begutachten und testen. Wann eine richtige Demo kommt und wann das Spiel im Handel steht ist bisher noch unbekannt.  
<a href="http://www.naturalselection2.com/download" target="_blank"><br />> Zur Natural Selection &#8211; Engine-Test Demo (Nur für Vorbesteller nutzbar)</a>

 

   

* * *

   

