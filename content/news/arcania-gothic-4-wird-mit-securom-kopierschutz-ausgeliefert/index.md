---
title: 'Arcania: Gothic 4 – Wird mit SecuROM-Kopierschutz ausgeliefert'
author: gamevista
type: news
date: 2010-09-20T15:23:03+00:00
excerpt: '<p>[caption id="attachment_1648" align="alignright" width=""]<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Im Rahmen einer Pressemitteilung kündigte der Publisher <strong>JoWood </strong>den Kopierschutz für das Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> an. Mit SecuROM wird es nötig sein das Spiel einmalig Online zu registrieren. Danach wird man den Titel auch ohne permanente Onlineverbindung spielen können.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
Eine DVD im Laufwerk ist daher auch nicht von Nöten. <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> wird nach der Onlineregistrierung auf unendlich vielen PCs installierbar sein und kann auf bis zu drei Rechnern gleichzeitig gespielt werden. <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> wird am 12. Oktober 2010 für PC, Xbox 360 und PlayStation 3 in den Handel kommen.

 

   

* * *

   

