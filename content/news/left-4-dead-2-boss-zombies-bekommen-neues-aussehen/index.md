---
title: Left 4 Dead 2 – Boss Zombies bekommen neues Aussehen
author: gamevista
type: news
date: 2009-07-31T11:50:15+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]<a href="http://www.valvesoftware.com" target="_blank" title="valve">Valves</a> Story Autor Chet Faliszek bestätigte in einem Interview das die Boss Zombies Hunter, Smoker, Boomer und Tank für <strong>Left 4 Dead 2</strong> ein neues Aussehen erhalten. Fans dachten bisher das diese nur recycelt werden und beschwerten sich. Nun wurden weitere Details bekannt. So sollen die neuen Boss Infizierten auch andere Stimmen und ein neues Modell erhalten. </p>'
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
<a href="http://www.valvesoftware.com" target="_blank" title="valve">Valve</a> veröffentlichte in den letzten Wochen regelmäßig neue neue Information um die aufgebrachte **Left 4 Dead** Community zu beruhigen. Diese wirft Valve reine Geldmacherei vor da der 2. Teil scheinbar nur rein optisch verbessert wurde, aber kein neues Spiel darstellt. <a href="http://www.valvesoftware.com" target="_blank" title="valve">Valve </a>konterte mit der Veröffentlichung diverse Details wie z.B. der neuen „Swamp Fever“ Kampagne oder den neuen Zombies „Mud Men“ und „Spitter“.







 