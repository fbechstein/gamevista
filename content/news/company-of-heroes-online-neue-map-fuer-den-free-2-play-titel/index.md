---
title: Company of Heroes Online – Neue Map für den Free-2-Play Titel
author: gamevista
type: news
date: 2011-01-13T20:40:17+00:00
excerpt: '<p><strong>[caption id="attachment_2185" align="alignright" width=""]<img class="caption alignright size-full wp-image-2185" src="http://www.gamevista.de/wp-content/uploads/2010/08/coh.jpg" border="0" alt="Company of Heroes Online" title="Company of Heroes Online" align="right" width="140" height="100" />Company of Heroes Online[/caption]THQ </strong>und <strong>Relic </strong>haben eine neue Karte für das kostenlose Onlinestrategiespiel <a href="http://www.companyofheroes.com/announcements/new-coho-map-beach-assault" target="_blank">Company of Heroes Online</a> angekündigt. Diese wird angelehnt sein an die Landung in der Normandie am D-Day. Die Karte trägt den passenden Namen Beach Assault und wird für Rang- sowie für erstellte Spiele verfügbar sein.</p> '
featured_image: /wp-content/uploads/2010/08/coh.jpg

---
Die Karte wird automatisch über den <a href="http://www.companyofheroes.com/announcements/new-coho-map-beach-assault" target="_blank">Company of Heroes Online</a> Launcher installiert. Weitere Informationen findet ihr auf der <a href="http://www.companyofheroes.com/announcements/new-coho-map-beach-assault" target="_blank">offiziellen Website</a> der Entwickler.

 

 

* * *



* * *