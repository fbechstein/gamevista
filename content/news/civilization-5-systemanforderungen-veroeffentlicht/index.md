---
title: Civilization 5 – Systemanforderungen veröffentlicht
author: gamevista
type: news
date: 2010-08-08T15:58:55+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Das Entwicklerteam von <strong>Firaxis Games </strong>hat die offiziellen Systemanforderungen für den bald erscheinenden fünften Teil der Civilization-Reihe bekannt gegeben.  Der Titel wird über die Online-Plattform <strong>Steam </strong>aktiviert und benötigt als Mindestanforderung einen Dualcore Prozessor, 2GB R AM und eine 256 MB ATI HD2600 XT oder 256 MB nVidia 7900 GS Grafikkarte.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
 

**Minimum Systemanforderungen</p> 

Operating System:</strong> Windows® XP SP3/ Windows® Vista SP2/ Windows® 7    
**Processor:** Dual Core CPU    
**Memory:** 2GB RAM    
**Hard Disk Space:** 8 GB Free    
**DVD-ROM Drive:** Required for disc-based installation    
**Video:** 256 MB ATI HD2600 XT or better, 256 MB nVidia 7900 GS or better, or Core i3 or better integrated graphics    
**Sound:** DirectX 9.0c-compatible sound card    
**DirectX®:** DirectX® version 9.0c

**Empfohlene Systemanforderungen**

**Operating System:** Windows® Vista SP2/ Windows® 7    
**Processor:** 1.8 GHz Quad Core CPU    
**Memory:** 4 GB RAM    
**Hard Disk Space:** 8 GB Free    
**DVD-ROM Drive:** Required for disc-based installation    
**Video:** 512 MB ATI 4800 series or better, 512 MB nVidia 9800 series or better    
**Sound:** DirectX 9.0c-compatible sound card    
**DirectX®:** DirectX® version 11

   

* * *

   

