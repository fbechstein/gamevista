---
title: 'Fallout: New Vegas – Zweites Entwicklertagebuch veröffentlicht'
author: gamevista
type: news
date: 2010-09-23T16:24:45+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]<strong>Bethesda Softworks</strong> hat heute das zweite Entwicklertagebuch zum Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> veröffentlicht. Im neuen Video präsentieren die Entwickler diesmal die Themen Technologie und Sound. <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> wird am 22. Oktober 2010 für PlayStation 3, Xbox 360 und PC im Handel erscheinen.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
[> Zum Fallout: New Vegas &#8211; Entwicklertagebuch #2][1]

   

* * *

   



 [1]: videos/item/root/fallout-new-vegas-zweites-entwicklertagebuch-veroeffentlicht