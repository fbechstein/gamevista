---
title: Bioshock 2 – Keine weiteren DLC`s für PC
author: gamevista
type: news
date: 2010-10-10T17:44:25+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Wie <strong>2K Games</strong> berichtete wird die PC-Version des Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> keine der beiden neuen Downloadable-Content`s, wie Minerva`s Den und Protector`s Trails, erhalten. Dies wurde im <a href="http://forums.2kgames.com/forums/showpost.php?s=39324c896aed7e03762a2fe930848cd8&p=1208765&postcount=122" target="_blank">offiziellen 2K-Forum</a> durch Elizabeth Tobey, Community Managerin, bestätigt.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Grund dafür sind die momentanen Update-Schwierigkeiten für die Konsolen-Versionen die viel Zeit in Anspruch nehmen. Beide DLC\`s sind für PlayStation 3 und Xbox 360 seit Sommer diesen Jahres verfügbar.

 

   

* * *

   



 