---
title: 'Hearts of Iron 3: Semper Fi – Releasetermin bekannt'
author: gamevista
type: news
date: 2010-05-20T17:46:33+00:00
excerpt: '<p>[caption id="attachment_178" align="alignright" width=""]<img class="caption alignright size-full wp-image-178" src="http://www.gamevista.de/wp-content/uploads/2009/07/heartsofiron3_small.jpg" border="0" alt="Hearts of Iron 3" title="Hearts of Iron 3" align="right" width="140" height="100" />Hearts of Iron 3[/caption]Lange wurde über den Veröffentlichungstermin für das Erweiterungspaket <a href="http://www.heartsofirongame.com" target="_blank">Hearts of Iron 3: Semper Fi</a> gemutmaßt, nun hat der Publisher<strong> Paradox Interactive</strong> den Termin für das Addon konkretisiert.</p> '
featured_image: /wp-content/uploads/2009/07/heartsofiron3_small.jpg

---
Am 6. Jun 2010, am Jahrestag des D-Day, wird **Semper Fi** als herunterladbares Erweiterungspaket erscheinen.    
Inhaltlich wird einiges geboten. So wird es jede Menge neuer Events geben und eine verbesserte KI wird euch das Spielerleben schwer machen. Weiterhin wird die Kooperation mit verbündeten Spielerparteien umfassender gestaltet. So kann man Unterstützung anfordern oder dem Partner Angriffsziele vorgeben.

 

   

* * *

   

