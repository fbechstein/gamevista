---
title: 'The Witcher 2: Assassins of Kings – Releasetermin und weitere Details angekündigt'
author: gamevista
type: news
date: 2010-11-16T20:34:22+00:00
excerpt: '<p>[caption id="attachment_2191" align="alignright" width=""]<img class="caption alignright size-full wp-image-2191" src="http://www.gamevista.de/wp-content/uploads/2010/09/thewitcher2.jpg" border="0" alt="The Witcher 2: Assassins of Kings" title="The Witcher 2: Assassins of Kings" align="right" width="140" height="100" />The Witcher 2: Assassins of Kings[/caption]Entwickler <strong>CD Projekt RED</strong> wird das offizielle Veröffentlichungsdatum für das Rollenspiel <a href="http://www.thewitcher.com" target="_blank">The Witcher 2: Assassins of Kings</a> am Donnerstag bekannt geben. Im Rahmen der CD Projekt-Pressekonferenz, die für den 18. November 2010 angesetzt ist, werden neben dem offiziellen Veröffentlichungsdatum auch neue Informationen zum Rollenspiel an die Öffentlichkeit weitergeben.</p> '
featured_image: /wp-content/uploads/2010/09/thewitcher2.jpg

---
Die Konferenz wird Online auf der <a href="http://www.facebook.com/thewitcher" target="_blank">offiziellen Facebook-Seite</a> ab 18.30 Uhr CET übertragen.

 

 

   

* * *

   

