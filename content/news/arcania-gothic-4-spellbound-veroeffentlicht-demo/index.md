---
title: 'Arcania: Gothic 4 – Spellbound veröffentlicht Demo'
author: gamevista
type: news
date: 2010-09-24T13:43:55+00:00
excerpt: '<p>[caption id="attachment_2270" align="alignright" width=""]<img class="caption alignright size-full wp-image-2270" src="http://www.gamevista.de/wp-content/uploads/2010/09/dungeon_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Arcania: Gothic 4[/caption]Publisher <strong>JoWood </strong>und das Entwicklerteam <strong>Spellbound</strong> haben heute die Demoversion zum Rollenspiel <a href="http://www.arcania-game.com" target="_blank">Arcania: Gothic 4</a> veröffentlicht. In der 1,8 Gigabyte großen Testfassung erkunden sie das Startgebiet auf der Insel Feshyr. Insgesamt gibt es dabei 10 Quests zu lösen.</p> '
featured_image: /wp-content/uploads/2010/09/dungeon_small.png

---
Die Demo steht ab sofort für PC und Xbox 360 zum Download bereit. <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> erscheint am 12. Oktober 2010 für Xbox 360 und PC im Handel. Die PlayStation 3-Fassung wird erst in einigen Monaten nachgereicht.

[> Zur Arcania: Gothic 4 &#8211; Demo][1]

   

* * *

   



 [1]: downloads/demos/item/demos/arcania-gothic-4-demo