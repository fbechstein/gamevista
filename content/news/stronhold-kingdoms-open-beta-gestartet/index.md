---
title: Stronghold Kingdoms – Open-Beta gestartet
author: gamevista
type: news
date: 2010-11-16T20:11:57+00:00
excerpt: '<p>[caption id="attachment_2434" align="alignright" width=""]<img class="caption alignright size-full wp-image-2434" src="http://www.gamevista.de/wp-content/uploads/2010/11/smallcombat.jpg" border="0" alt="Strongold Kingdoms" title="Strongold Kingdoms" align="right" width="140" height="100" />Strongold Kingdoms[/caption]</p> <p>Fans der Stronhold-Teile können nun auch endlich in die Online-Variante des Spiels schnuppern. Entwickler <strong>Firefly Studios</strong> bietet ab sofort die Möglichkeit an, euch für einen Open-Beta-Account zu bewerben. Dazu müsst ihr euch lediglich auf der <a href="http://www.strongholdkingdoms.com/" target="_blank">offiziellen Website</a> des Spiels registrieren und den englischsprachigen Beta-Client herunterladen.</p> '
featured_image: /wp-content/uploads/2010/11/smallcombat.jpg

---
Einen genauen Termin für eine deutschsprachige Beta-Version von <a href="http://www.strongholdkingdoms.com/" target="_blank">Stronghold Kingdoms</a> ist bisher nicht bekannt.

 

 

   

* * *

   



 