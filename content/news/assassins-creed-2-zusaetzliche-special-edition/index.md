---
title: Assassin’s Creed 2 – Zusätzliche Special Edition
author: gamevista
type: news
date: 2009-08-18T19:59:23+00:00
excerpt: |
  |
    <p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin's Creed 2" title="Assassin's Creed 2" align="right" width="140" height="100" />Assassin's Creed 2[/caption]Publisher <a href="http://www.ubi.com/de/" target="_blank">Ubisoft</a> hat nun offiziell die Planung einer weiteren Special Edition für ihr Action-Adventure <strong>Assassin's Creed 2 </strong>bekannt gegeben. Nach einer streng limitierten Black Edition wird nun eine White Edition folgen. Der Vorverkauf der für 75 Euro angebotenen White Edition wird Anfang September beginnen. Hier die Inhalte beider Editions im Überlick.</p>
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
 

<p class="MsoNormal">
  <strong>White-Edition (UVP: 75,00 Euro):</strong>
</p>

  * <span>Sammlerstatue „Ezio“ (20 cm groß)</span>
  * <span><span lang="IT">Bonusmission &#8222;Santa Maria Dei Frari&#8220;</span></span>

<p class="MsoNormal">
  <strong id="nointelliTXT">Black-Edition (UVP: 90,00 Euro):</strong>
</p>

  * <span>3 exklusive Bonusmissionen zum Freischalten</span>
  * <span>&#8222;Verschwörungsbuch&#8220; mit Artwork</span>
  * <span>Soundtrack zum Spiel</span>
  * <span>Schwarze, seltene “Meister-Assassinen” Figur von Ezio </span>
  * <span>Sammlerbox</span>

* * *



* * *

 

 