---
title: 'The Witcher 2: Assassins of Kings – Neue Website des Rollenspiels ist Online'
author: gamevista
type: news
date: 2010-04-07T22:22:12+00:00
excerpt: '<p>[caption id="attachment_1610" align="alignright" width=""]<img class="caption alignright size-full wp-image-1610" src="http://www.gamevista.de/wp-content/uploads/2010/03/thewitcher2.jpg" border="0" alt="The Witcher 2" title="The Witcher 2" align="right" width="140" height="100" />The Witcher 2[/caption]Die Entwickler <strong>CD Projekt</strong> haben für das Rollenspiel <a href="http://tw2.thewitcher.com/" target="_blank">The Witcher 2: Assassins of Kings</a> eine neue <a href="http://tw2.thewitcher.com/" target="_blank">Website </a>online gestellt. Mit Informationen über die Geschichte von <a href="http://tw2.thewitcher.com/" target="_blank">The Witcher 2</a> und deren Charakteren, sowie einem ersten Trailer, möchte das Entwicklerteam den zweiten Teil den Fans näher bringen.</p> '
featured_image: /wp-content/uploads/2010/03/thewitcher2.jpg

---
<a href="http://tw2.thewitcher.com/" target="_blank">The Witcher 2</a> wird voraussichtlich im ersten Quartal 2011 im Handel erscheinen.



<cite></cite>

   

* * *

   

