---
title: 'Assassin’s Creed: Bloodlines für PSP angekündigt'
author: gamevista
type: news
date: 2009-07-17T11:47:36+00:00
excerpt: '[caption id="attachment_68" align="alignright" width=""]<img class="caption alignright size-full wp-image-68" src="http://www.gamevista.de/wp-content/uploads/2009/07/acb_small3.png" border="0" alt="Assassin‘s Creed: Bloodlines kommt für PSP" title="Assassin‘s Creed: Bloodlines kommt für PSP" align="right" width="140" height="100" />Assassin‘s Creed: Bloodlines kommt für PSP[/caption]<a href="http://www.ubi.com/de/" target="_blank">Ubisoft </a>hat nun offiziell die Entwicklung von <strong>Assassin‘s Creed: Bloodlines</strong> angekündigt. Das Spiel wird exclusiv für Sonys Handheld PSP entwickelt und knüpft an die Ereignisse von Assassin‘s Creed an. <br /><br />'
featured_image: /wp-content/uploads/2009/07/acb_small3.png

---
Frederic Lefrancois, 3rd Party Producer für Assassin’s Creed: Bloodlines, stand Rede und Antwort zum neuem PSP Abenteuer. 

[> Screenshots zu Assassin’s Creed: Bloodlines][1]  
[> Video zu Assassin’s Creed: Bloodlines][2]  
[> zum Interview mit Frederic Lefrancois, 3rd Party Producer für Assassin’s Creed: Bloodlines][3]







 [1]: screenshots/item/root/assassins-creed-bloodlines
 [2]: videos/item/root/-assassins-creed-bloodlines-trailer
 [3]: hot-spots/50-pre-reviews-mobile/125-assassins-creed-bloodlines-interview-mit-frederic-lefrancois