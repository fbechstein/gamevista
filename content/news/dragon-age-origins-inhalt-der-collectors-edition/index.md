---
title: 'Dragon Age: Origins – Inhalt der Collectors Edition'
author: gamevista
type: news
date: 2009-07-16T11:55:24+00:00
excerpt: '<p>[caption id="attachment_51" align="alignright" width=""]<img class="caption alignright size-full wp-image-51" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragonce_small.png" border="0" alt="Dragon Age: Origins - Collectors Edition" title="Dragon Age: Origins - Collectors Edition" align="right" width="140" height="100" />Dragon Age: Origins - Collectors Edition[/caption]Jetzt wurde der exclusive Inhalt der Collectors Editions von <a href="http://www.bioware.com/">Biowares</a> <strong>Dragon Age: Origins</strong> enthüllt. Die Sammlerbox wird neben einer Making of-DVD, Soundtrack CD, Wallpaper und einer Stoffkarte noch drei exclusive InGame Items enthalten. Das aber ist noch nicht alles. Zusätzlich dazu wird die Edition ein exclusives InGame-Item für das kommende Rollenspiel Mass Effect 2 bekommen - für Sammler und Mass Effect-Fans ein Muss! </p>'
featured_image: /wp-content/uploads/2009/07/dragonce_small.png

---
In USA kostet die Edition $64,99, bei uns sollte sich der Preis im ähnlichen Rahmen bewegen. Das Spiel erscheint am 22. Oktober 2009 für PC, Xbox 360 und Playstation 3. 

[> zur Dragon Age: Origins &#8211; Screenshot Galerie][1]  
[> zur Dragon Age: Origins Videos][2]







&nbsp;

 [1]: index.php/screenshots/item/screenshots/dragon-age-origins
 [2]: videos/alphaindex/d