---
title: R.U.S.E. – The Rising Sun DLC ab sofort verfügbar
author: gamevista
type: news
date: 2011-02-15T18:16:20+00:00
excerpt: '<p>[caption id="attachment_2559" align="alignright" width=""]<img class="caption alignright size-full wp-image-2559" src="http://www.gamevista.de/wp-content/uploads/2011/02/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Publisher <strong>Ubisoft </strong>hat heute das Erweiterungspaket <strong>The Rising Sun</strong> für das Strategiespiel <a href="http://ruse.de.ubi.com" target="_blank" rel="nofollow">R.U.S.E.</a> veröffentlicht. Der Download-Content ist damit ab sofort für PC und Xbox 360 verfügbar, die PlayStation 3 Version erscheint am morgigen Tag. Die Anschaffungskosten belaufen sich auf 800 MS-Punkte bzw. 9,99 Euro. Der DLC erlaubt dem Spieler die japanische Armee im Einzel- sowie im Mehrspielermodus auszuwählen.</p> '
featured_image: /wp-content/uploads/2011/02/ruse_small.jpg

---
Insgesamt werden 28 neue Einheiten zur Verfügung stehen.

 

 

* * *



* * *