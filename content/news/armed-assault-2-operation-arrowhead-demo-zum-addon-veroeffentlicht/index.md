---
title: 'Armed Assault 2: Operation Arrowhead – Demo zum Addon veröffentlicht'
author: gamevista
type: news
date: 2010-07-20T18:59:32+00:00
excerpt: '<p>[caption id="attachment_2033" align="alignright" width=""]<img class="caption alignright size-full wp-image-2033" src="http://www.gamevista.de/wp-content/uploads/2010/07/arma2_small.png" border="0" alt="Armed Assault 2: Operation Arrowhead" title="Armed Assault 2: Operation Arrowhead" align="right" width="140" height="100" />Armed Assault 2: Operation Arrowhead[/caption]Auch führ ihr erstes Addon von <a href="http://www.arma2.com/demo/index_en.html" target="_blank">Armed Assault 2: Operation Arrowhead</a> bieten die Entwickler von <strong>Bomhemia Interactive</strong> eine Demoversion.  Die Testversion zeigt euch die Missionen Coltan Blues und Little Bird. Weiterhin könnt ihr euch in drei Trainingsszenarien beweisen und per Missionseditor neue Einsätze basteln.</p> '
featured_image: /wp-content/uploads/2010/07/arma2_small.png

---
Auch für den Mehrspielerpart gibt es zwei Einsätze Hike In The Hills und One Shot One Kil. Die <a href="http://www.arma2.com/demo/index_en.html" target="_blank">Armed Assault 2: Operation Arrowhead</a> umfasst 2,66 Gigabyte und steht ab sofort zum Download bereit.  
[  
> Zur Armed Assault 2: Operation Arrowhead &#8211; Demo][1]

 

   

* * *

   



 [1]: downloads/demos/item/demos/armed-assault-2-operation-arrowhead--demo