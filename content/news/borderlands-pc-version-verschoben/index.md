---
title: Borderlands – PC Version verschoben
author: gamevista
type: news
date: 2009-09-23T22:05:12+00:00
excerpt: '<p>[caption id="attachment_121" align="alignright" width=""]<img class="caption alignright size-full wp-image-121" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderlands_small.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Wie Publisher <a href="http://www.2kgamesinternational.com/de">2K Games</a> nun offiziell bestätigt hat wird die PC Version des Actionspiels <strong>Borderlands </strong>in Europa erst am 30. Oktober auf den Markt kommen. Demnach eine Woche später als die Konsolenvariante.<br /> Allerdings, in den USA wird <strong>Borderlands </strong>schon am 26. Oktober im Handel erhältlich sein.</p> '
featured_image: /wp-content/uploads/2009/07/borderlands_small.jpg

---
Als Grund wurde dringend benötigte Zeit angegeben die zur Optimierung der PC Version erforderlich ist.

 

 

* * *



* * *

 