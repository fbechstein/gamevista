---
title: Hearts of Iron 3 – Demo als Download
author: gamevista
type: news
date: 2009-08-05T10:57:58+00:00
excerpt: '<p>[caption id="attachment_178" align="alignright" width=""]<img class="caption alignright size-full wp-image-178" src="http://www.gamevista.de/wp-content/uploads/2009/07/heartsofiron3_small.jpg" border="0" alt="Hearts of Iron 3" title="Hearts of Iron 3" align="right" width="140" height="100" />Hearts of Iron 3[/caption]Entwickler <a href="http://www.paradoxplaza.com" target="_blank" title="paradox interactive">Paradox Interactive</a> hat nun die offizielle Demo zu seinem Strategie Titel <strong>Hearts of Iron 3</strong> released. Die Demo wird das komplette Spiel samt Tutorial enthalten. Es werden nicht alle Nationen anspielbar sein. Nur unter der deutschen, französischen, polnischen oder britischen Flagge wird es möglich sein die Welt zu erobern. </p>'
featured_image: /wp-content/uploads/2009/07/heartsofiron3_small.jpg

---
Außerdem wird es keine Speicherfunktion geben und die Demo ist im Spiel auf 4 Monate begrenzt. Im neuen Teil des für Hardcore Strategen entwickelten Spiel geht <a href="http://www.paradoxplaza.com" target="_parent" title="paradox interactive">Paradox Interactive</a> den gewohnten weg und verbessert hier und da diverse Funktionen. Die Welt wird auch insgesamt mehr Provinzen bekommen.  
<a href="downloads/patches/item/patches/world-of-warcraft-patch-32-der-ruf-des-kreuzzugs-teil-2-englische-version" target="_blank" title="wow patch 3.2"></a>

[> Zum Hearts of Iron 3 Demo Download][1] 







 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=148 "Hearts of Iron 3 - Demo"