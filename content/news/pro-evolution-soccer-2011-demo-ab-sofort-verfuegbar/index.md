---
title: Pro Evolution Soccer 2011 – Demo ab sofort verfügbar
author: gamevista
type: news
date: 2010-09-15T16:45:09+00:00
excerpt: '<p>[caption id="attachment_2236" align="alignright" width=""]<img class="caption alignright size-full wp-image-2236" src="http://www.gamevista.de/wp-content/uploads/2010/09/pes2011_1.jpg" border="0" alt="Pro Evolution Soccer 2011" title="Pro Evolution Soccer 2011" align="right" width="140" height="100" />Pro Evolution Soccer 2011[/caption]Am 30. September 2010 geht die diesjährige Version von <strong>Pro Evolution Soccer</strong> in die nächste Runde. Passend dazu hat der japanische Publisher <strong>Konami </strong>die Demoversion des Fussballspiels veröffentlicht. Mit vier Teams, darunter Bayern München, FC Barcelona, Chivas Guadalajara und Internacional Porto Alegre, dürfen die Fans die Testmatches bestreiten.</p> '
featured_image: /wp-content/uploads/2010/09/pes2011_1.jpg

---
Die Testfassung umfasst 1,3 Gigabyte und steht ab sofort zum Download bereit.

[> Zum Pro Evolution Soccer 2011 – Demo Download][1]

 

   

* * *

   



 [1]: downloads/demos/item/demos/pro-evolution-soccer-2011-demo