---
title: Diablo III – Kein Mana mehr?
author: gamevista
type: news
date: 2009-10-20T07:56:32+00:00
excerpt: '<p><img class=" alignright size-full wp-image-904" src="http://www.gamevista.de/wp-content/uploads/2009/10/diablo3_small.jpg" border="0" alt="Diablo III" title="Diablo III" align="right" width="140" height="100" />Der Entwickler <a href="http://eu.blizzard.com/de-de/" target="_blank">Blizzard</a> scheint einige Änderungen einführen zu wollen im neuen Diablo Teil. Zumindest wird der Vorschlag nun offen diskutiert, bevor er umgesetzt wird, was den Fans doch bedeutend lieber seien sollte.</p> '
featured_image: /wp-content/uploads/2009/10/diablo3_small.jpg

---
Die folgende Nachricht wurde im Twitter-Channel über **Diablo III** gezwitschert.

_We&#8217;re looking to give each class in Diablo III a unique resource system, instead of everyone using mana. Thoughts?_

Demnach überlegt man also, das Mana durch verschiedene Resourcen zu ersetzen. An sich eine interessante Idee, wenn man jedem Charakter auch eine eigene Art der &#8222;Energie&#8220;-Beschaffung gibt. Dann würde sich jeder Charakter noch unterschiedlicher spielen. DIe Fans sind wie immer zweigeteilt und so bleibt abzuwarten, was die Entwickler nun machen.

* * *



* * *

 