---
title: Star Trek Online – Erscheint bereits am 5. Februar 2010
author: gamevista
type: news
date: 2009-11-07T18:58:20+00:00
excerpt: '<p>[caption id="attachment_915" align="alignright" width=""]<img class="caption alignright size-full wp-image-915" src="http://www.gamevista.de/wp-content/uploads/2009/10/startrek_online.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption]Wie der Publisher <a href="http://www.atari.com/" target="_blank">Atari</a> nun offiziell bestätigt hat wird das Online Rollenspiel <strong>Star Trek Online</strong> bereits am 5. Februar 2010 in Europa veröffentlicht werden. Unsere amerikanischen Kollegen können mit einem früheren Termin, den 2. Februar, rechnen. Die geschlossen Beta läuft bereits und die offene Beta ist in Planung.</p> '
featured_image: /wp-content/uploads/2009/10/startrek_online.jpg

---
 

Zwar gibt es noch keinen genauen Termin für die offene Beta, spekulieren könnte man aber über einen Start im Januar 2010. Denn diese dürfte sicher ein bis zwei Wochen in Anspruch nehmen.



* * *



* * *