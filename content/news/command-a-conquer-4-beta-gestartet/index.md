---
title: 'Command & Conquer 4: Tiberian Twilight – Beta gestartet'
author: gamevista
type: news
date: 2010-01-28T21:23:36+00:00
excerpt: '<p>[caption id="attachment_1382" align="alignright" width=""]<img class="caption alignright size-full wp-image-1382" src="http://www.gamevista.de/wp-content/uploads/2010/01/shot_basg4y50_small.jpg" border="0" alt="Command & Conquer 4: Tiberian Twilight" title="Command & Conquer 4: Tiberian Twilight" align="right" width="140" height="100" />Command & Conquer 4: Tiberian Twilight[/caption]Der Publisher <strong>Electronic Arts</strong> hat ohne Vorankündigung und zur großen Überraschung vieler Fans des Strategiespiels <a href="http://www.commandandconquer.com/news/get-cc-4-public-beta" target="_blank">Command & Conquer 4: Tiberian Twilight</a>, die Beta zum Spiel gestartet. Passende Beta-Keys werden exklusiv bei <a href="http://uk.gamespot.com/event/codes/command-conquer-4-open/" target="_blank">Gamespot</a> verteilt. Start der Beta war am heutigen Tage um 18.00 Uhr.</p> '
featured_image: /wp-content/uploads/2010/01/shot_basg4y50_small.jpg

---
Der Beta-Client ist auf der offiziellen Seite von <a href="http://www.commandandconquer.com/news/get-cc-4-public-beta" target="_blank">Command & Conquer 4: Tiberian Twilight</a> herunterladbar. Wenn ihr einen der heißbegehrten Keys haben wollt, meldet euch einfach bei <a href="http://uk.gamespot.com/event/codes/command-conquer-4-open/" target="_blank">Gamespot</a> an. Die Beta umfasst einen Spielmodus der im 5vs5 Online ausgetragen wird. <a href="http://www.commandandconquer.com/news/get-cc-4-public-beta" target="_blank">Command & Conquer 4: Tiberian Twilight</a> erscheint am 19. März 2010 im Handel.

 

 

* * *



* * *