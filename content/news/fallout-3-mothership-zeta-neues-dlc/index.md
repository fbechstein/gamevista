---
title: 'Fallout 3: Mothership Zeta – neues DLC'
author: gamevista
type: news
date: 2009-08-03T13:29:07+00:00
excerpt: '<p>[caption id="attachment_267" align="alignright" width=""]<img class="caption alignright size-full wp-image-267" src="http://www.gamevista.de/wp-content/uploads/2009/08/mz_05b_small.jpg" border="0" alt="Fallout 3 - Mothership Zeta" title="Fallout 3 - Mothership Zeta" align="right" width="140" height="100" />Fallout 3 - Mothership Zeta[/caption]Ab heute könnt Ihr den fünften und letzten DLC zum RPG <strong>Fallout 3</strong> käuflich erwerben. Das Addon trägt den Titel <strong>Mothership Zeta</strong> und handelt nicht von Odländern und Mutanten, sondern von Aliens und einem Ufo. </p><p>Ihr empfängt ein ausserirdisches Signal und eher Ihr Euch versieht, werdet Ihr schon an Bord eines Raumschiffes gebeamt. Dort müsst Ihr Euch bis zur Brücke durchkämpfen und versuchen von dem Raumschfiff zu fliehen. Es erwarten Euch neue Feinde, neue Waffen und neue Vebündete.</p>'
featured_image: /wp-content/uploads/2009/08/mz_05b_small.jpg

---
Zum Start des neuen DLCs wurde auch ein Video veröffentlicht, welches wir Euch nicht vorenthalten wollen. 

[> zum Video von Fallout 3: Mothership Zeta][1]   
[> zu Screenshots von Fallout 3: Mothership Zeta][2] 







 [1]: index.php?Itemid=102&option=com_zoo&view=item&category_id=0&item_id=138
 [2]: index.php?Itemid=111&option=com_zoo&view=item&category_id=0&item_id=139