---
title: Patrizier 4 – Kalypso lädt zum Beta-Test
author: gamevista
type: news
date: 2010-06-27T16:10:15+00:00
excerpt: '<p>[caption id="attachment_1615" align="alignright" width=""]<img class="caption alignright size-full wp-image-1615" src="http://www.gamevista.de/wp-content/uploads/2010/03/patrizier4.jpg" border="0" alt="Patrizier 4" title="Patrizier 4" align="right" width="140" height="100" />Patrizier 4[/caption]Publisher <strong>Kalpyso Media</strong> hat die Bewerbungsphase zur geschlossenen Beta vom vierten Teil der Patrizier-Serie gestartet. Fans der Wirtschaftssimulation können sich ab sofort auf der <a href="http://www.patrizier4.de/gmbeta/index.php" target="_blank">offiziellen Website</a> registrieren und auf einen der begehrten Keys hoffen. Die Patrizier 4-Beta wird dann am 05. Juli 2010 freigeschaltet.</p> '
featured_image: /wp-content/uploads/2010/03/patrizier4.jpg

---
Die Finale Version erscheint am 02. September 2010 im Handel.

 

   

* * *

   

