---
title: 'World of Warcraft: Cataclysm – Nächstes Addon auch als Collector`s Edition erhältlich'
author: gamevista
type: news
date: 2010-08-18T17:20:35+00:00
excerpt: '<p>[caption id="attachment_2138" align="alignright" width=""]<img class="caption alignright size-full wp-image-2138" src="http://www.gamevista.de/wp-content/uploads/2010/08/wow_small2.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="140" height="100" />World of Warcraft[/caption]Das nächste Zusatzpaket für das Online-Rollenspiel <a href="http://www.wow-europe.com/de/" target="_blank">Word of Warcraft</a> wird auch als Sammleredition in den Handel kommen. <strong>Cataclysm </strong>wird voraussichtlich gegen Ende des Jahres erscheinen und mit der Collector`s Edition zahlreiche Extras bieten.</p> '
featured_image: /wp-content/uploads/2010/08/wow_small2.png

---
Als Inhalte gibt es neben dem Addon ein 176 Seiten starkes Artbook und ein WoW-Trading Card-Game mit 60-Karten-Starter-Deck, sowie vier exklusive Karten. 

Den kompletten Inhalt in der Übersicht:

  * World of Warcraft: Cataclysm 
  * 176-seitiges Artbook Art of the Cataclysm
  * exklusives Ingame-Haustier (Mini-Deathwing)
  * Bonus-DVD mit Making-of, Interviews etc.
  * offizieller Soundtrack mit zehn Musikstücken
  * Mouse-Pad mit Bild von Deathwing
  * WoW-Trading Card-Game mit 60-Karten-Starter-Deck sowie vier exklusive Karten

   

* * *

   



 