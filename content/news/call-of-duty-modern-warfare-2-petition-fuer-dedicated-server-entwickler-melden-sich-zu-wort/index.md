---
title: 'Call of Duty: Modern Warfare 2 – Petition für Dedicated Server, Entwickler melden sich zu Wort'
author: gamevista
type: news
date: 2009-10-21T15:13:01+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Seitdem der Entwickler <a href="http://www.infinityward.com" target="_blank">Infinity Ward</a> offiziell verkündet hat das zum neuen <strong>Call of Duty: Modern Warfare 2</strong> Teil es keine Dedicated Server geben wird, formierte sich reichlich Widerstand in vorm einer <a href="http://www.petitiononline.com/mod_perl/signed.cgi?dedis4mw" target="_blank">Online Petition</a>. Dies umfasst mittlerweile knapp 123.000 Stimmen gegen die Entscheidung des Entwicklers voll auf den Onlineservice Steam zu setzen und damit keine Dedicated Server zuzulassen.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Der Grund ist das es nicht möglich sein wird modifizierte Server zum laufen zu bringen. Auch seien keinerlei Mod Werkzeuge geplant. 

Nun hat sich dem Thema auch der Entwickler mit einem Statement gewidmet. Jetzt meldeten sich beide Chefs Jason West und Vince Zampella zu Wort. Demnach sagten sie zum wiederholten Male das es definitiv keine Dedicated Server für **Call of Duty: Modern Warfare 2** geben wird. <a href="http://www.infinityward.com" target="_blank">Infinity Ward</a> möchte den Mehrspielerpart für die große Mehrheit der Spieler verbessern und nicht auf die kleine Anzahl von Modern und Tunern hören. So wollen sie sich den Fans widmen die sich angeblich für eine Serversuche ohne Modserver aussprachen. So war es dieser Gruppe von Fans ein Wunsch ungestört spielen zu können, ohne die üblichen Cheater.  
Wo diese Art von Spielern ihre Stimme abgegeben haben fragt sich natürlich. Vielmehr bleibt das Gefühl das ein Entwickler die Vorteile von Steam entdeckt hat und mit dem damit verbundenen passiven Raubkopierschutz liebäugelt.

 

* * *



* * *

 