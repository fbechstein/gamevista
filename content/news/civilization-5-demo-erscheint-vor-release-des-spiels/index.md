---
title: Civilization 5 – Demo erscheint vor Release des Spiels
author: gamevista
type: news
date: 2010-08-04T15:22:33+00:00
excerpt: '<p>[caption id="attachment_1470" align="alignright" width=""]<img class="caption alignright size-full wp-image-1470" src="http://www.gamevista.de/wp-content/uploads/2010/02/civ5_2_small.jpg" border="0" alt="Civilization 5" title="Civilization 5" align="right" width="140" height="100" />Civilization 5[/caption]Publisher <strong>2K Games</strong> hat auf der <a href="http://www.civilization5.com/#/community/demo" target="_blank">offiziellen Website</a> des Strategiespiels <a href="http://www.civilization5.com/#/community/demo" target="_blank">Civilization 5</a> verkündet, dass noch vor der Veröffentlichung des fünften Teils eine Demo bereit gestellt werden wird. <a href="http://www.civilization5.com/#/community/demo" target="_blank">Civilization 5</a> wird am 24. September 2010 für PC im Handel erscheinen.</p> '
featured_image: /wp-content/uploads/2010/02/civ5_2_small.jpg

---
Voraussichtlich kurz davor wird **2K Games** die Demo veröffentlichen. Einen genauen Termin und weitere Infos zur Testversion nannte der Publisher leider nicht.

 

   

* * *

   

