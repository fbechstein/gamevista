---
title: Harry Potter und die Heiligtümer des Todes – Spieleumsetzung offiziell bestätigt
author: gamevista
type: news
date: 2010-06-01T21:06:51+00:00
excerpt: '<p>[caption id="attachment_1871" align="alignright" width=""]<img class="caption alignright size-full wp-image-1871" src="http://www.gamevista.de/wp-content/uploads/2010/06/hphdt_04.jpg" border="0" alt="Harry Potter und die Heiligtümer des Todes" title="Harry Potter und die Heiligtümer des Todes" align="right" width="140" height="100" />Harry Potter und die Heiligtümer des Todes[/caption]Publisher <a href="http://www.ea.com" target="_blank">Electronic Arts</a> kündigte heute im Rahmen einer Pressemitteilung die Entwicklung von zwei neuen Harry Potter-Spielen an. Das Videospiel <strong>Harry Potter und die Heiligtümer des Todes</strong> - Teil 1 erscheint diesen Herbst für den PC, PlayStation 3, Xbox 360, Wii, Nintendo DS und als Mobile Game. Über den Veröffentlichungstermin des zweiten Teils ist bisher nichts bekannt.</p> '
featured_image: /wp-content/uploads/2010/06/hphdt_04.jpg

---
In **Harry Potter und die Heiligtümer des Todes** – Teil 1 dürft ihr erstmals vollständig außerhalb von Hogwarts umherziehen und die Gegend erkunden. Dabei ist der Spieler ständig auf der Flucht vor Todessern oder Greifern, weit vom sicheren Schulgelände entfernt. Hier versucht ihr Voldemorts Horkruxe zu finden und zu zerstören. Der Kinofilm wird am 17. November 2010 auf der Leinwand erscheinen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1872" src="http://www.gamevista.de/wp-content/uploads/2010/06/hphdt_03.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/06/hphdt_03.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/06/hphdt_03-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

