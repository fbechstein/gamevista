---
title: Aion – Zum Start ohne Anti Cheat Software
author: gamevista
type: news
date: 2009-09-19T17:53:22+00:00
excerpt: '<p>[caption id="attachment_224" align="alignright" width=""]<img class="caption alignright size-full wp-image-224" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion" title="Aion" align="right" width="140" height="100" />Aion[/caption]Wie Publisher <a href="http://www.eu.aiononline.com/de/">NCSoft</a> auf ihrer offiziellen Website bekannt gegeben haben wird <strong>Aion </strong>zum Starttermin in Europa am 25. September über keinerlei Anti-Cheat Software verfügen. <strong>GameGuard </strong>wurde zwar extra für <strong>Aion </strong>entwickelt, sei aber noch nicht soweit um bei der Launch Version von <strong>Aion </strong>eingesetzt zu werden.</p> '
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
Die Entscheidung kam von diversen Analysen der Open Beta und den damit enthaltenen Feedbacks von Spielern. [NCSoft][1] prüft jetzt wann und ob **GameGuard** zu einem späteren Zeitpunkt kommen wird.

 

 

* * *



* * *

 

 [1]: http://www.eu.aiononline.com/de/