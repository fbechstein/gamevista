---
title: Overlord 2 – Downloadinhalte ab sofort verfügbar
author: gamevista
type: news
date: 2009-07-29T14:56:59+00:00
excerpt: '<p>[caption id="attachment_206" align="alignright" width=""]<img class="caption alignright size-full wp-image-206" src="http://www.gamevista.de/wp-content/uploads/2009/07/overlord2_small.png" border="0" alt="Overlord 2" title="Overlord 2" align="right" width="140" height="100" />Overlord 2[/caption]<a href="http://www.triumphstudios.com" target="_blank" title="triumph studios">Triumph Studios</a> verkündete heute das zum Actionspiel <strong>Overlord 2</strong> neue Downloadinhalte verfügbar seien. Das ganze nennt sich <strong>„Battle Rock Nemesis“</strong> – Pack und bietet euch eine Kampfarena in Netherworld und neue Kämpfe mit einem Satyr oder Phoenix. </p>'
featured_image: /wp-content/uploads/2009/07/overlord2_small.png

---
Neue Zaubersprüche und Items sollen ebenfalls enthalten sein und freigeschaltet werden falls ihr Siegreich wart. Das ganze ist zur Zeit nur für die Playstation 3 (3,99 Euro) und Xbox 360 (400 Microsoft Punkte) zum Download bereit







 