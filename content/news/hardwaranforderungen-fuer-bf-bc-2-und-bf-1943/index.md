---
title: Hardwaranforderungen für BF BC 2 und BF 1943
author: gamevista
type: news
date: 2009-12-02T20:17:22+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield Bad Company 2" title="Battlefield Bad Company 2" align="right" width="140" height="100" />Battlefield Bad Company 2[/caption]Der Entwickler <a href="http://www.dice.se/" target="_blank">DICE</a> hat nun die Hardwaranforderungen für <strong>Battlefield Bad Company 2</strong> und <strong>Battlefield 1943</strong> bekannt gegeben. Diese sind recht hoch und auffällig identisch.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
  * Minimum: 
      * Core 2 Duo 2,0 GHz 
      * 2 GB RAM 
      * GeForce 7800 GT / ATI X1900 mit 256MB

 

  * Empfohlen: 
      * Quadcore 
      * 2GB RAM 
      * GeForce GTX 260 mit 512MB 

 

**Battlefield Bad Company 2** erscheint am 2ten März 2010. Für **Battlefield 1943** gibt es noch keinen genauen Termin &#8211; bis jetzt wird Anfang 2010 genannt.

 







 