---
title: Lost Planet 2 – Mit Luftkämpfen!
author: gamevista
type: news
date: 2009-07-26T16:46:20+00:00
excerpt: '[caption id="attachment_166" align="alignright" width=""]<img class="caption alignright size-full wp-image-166" src="http://www.gamevista.de/wp-content/uploads/2009/07/lp_wallpaper1_1280x1024_small.jpg" border="0" alt="Lost Planet" title="Lost Planet" align="right" width="140" height="100" />Lost Planet[/caption]In einem Interview auf Comic-Con 2009 verriet Jun Takeuchi, Game Producer von <a href="http://www.capcom.com" target="_blank" title="capcom">Capcom</a>, dass es bei <strong>Lost Planet 2</strong> auch Luftkämpfe gegen die Akrid geben wird. <br />'
featured_image: /wp-content/uploads/2009/07/lp_wallpaper1_1280x1024_small.jpg

---
Diese Information wurde ihm durch eine Frage aus dem Publikum, ob es noch zusätzliche Fahrzeuge geben wird die im Koop-Modus verwendet werden können, entlockt. Dies bestätigte er und sagte außerdem das einige dieser Fahrzeuge wohl fliegen werden können. Dies hat dem 1. Teil eigentlich nur noch gefehlt. Wir freuen uns das es im 2. Teil enthalten sein wird.

[][1]







 [1]: downloads/patches/item/patches/spore-v105-patch