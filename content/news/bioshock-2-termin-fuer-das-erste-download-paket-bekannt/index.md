---
title: Bioshock 2 – Termin für das erste Download-Paket bekannt
author: gamevista
type: news
date: 2010-03-06T13:05:18+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Publisher <strong>2K Games</strong> hat endlich den Veröffentlichungstermin des ersten Download-Inhalts für den Ego-Shooter <a href="http://www.bioshock2game.com" target="_blank">Bioshock 2</a> bekannt gegeben. Das Paket mit dem Namen <strong>Sinclair Solutions,</strong> bringt neue Inhalte für den Mehrspielermodus und wird bereits nächste Woche am 11. März 2010 veröffentlicht. Was ihr vom neuen DLC erwarten könnt zeigt diese kleine Übersicht:</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
  * Rang-Steigerung bis Level 50 mit Rang-Belohnungen
  * Neue spielbare Charaktere: Louie McGraff und Oscar Calraca
  * 20 neue Prüfungen (ab Rang 41)
  * Eine dritte Waffenverbesserung für jede Waffe
  * Fünf zusätzliche Masken (ab Rang 41) 

   

* * *

   



 