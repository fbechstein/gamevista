---
title: Tom Clancy’s H.A.W.X. 2 – Hardwareanforderungen bekannt
author: gamevista
type: news
date: 2010-11-04T15:40:33+00:00
excerpt: |
  |
    <p>[caption id="attachment_2000" align="alignright" width=""]<img class="caption alignright size-full wp-image-2000" src="http://www.gamevista.de/wp-content/uploads/2010/07/hawx2_s_003.jpg" border="0" alt="Tom Clancy's H.A.W.X. 2" title="Tom Clancy's H.A.W.X. 2" align="right" width="140" height="100" />Tom Clancy's H.A.W.X. 2[/caption]Publisher <strong>Ubisoft </strong>hat die Hardwareanforderungen für die Flugsimulation <a href="http://hawxgame.de.ubi.com" target="_blank">Tom Clancy's H.A.W.X. 2</a> bekannt gegeben. Anders als bei neuen Titeln genügt dem Spiel lediglich ein Single-Core Prozessor mit 1 GB Ram Arbeitsspeicher. Als Grafikkarte werden Shader 3 und 4 Modelle unterstützt.</p>
featured_image: /wp-content/uploads/2010/07/hawx2_s_003.jpg

---
Um das Actionspiel auf höchsten Grafikeinstellungen genießen zu können, solltet ihr aber mindestens einen Intel Core 2 Dui E4300 oder AMD Atholn X2 5000 verbaut haben. Der zweite Teil wird am 12. November 2010 für PC im Handel erscheinen. Xbox 360 und PlayStation 3-Fassungen sind bereits erhältlich.

 

**Minimale Systemanforderungen**    
&#8211; CPU: 3,0 GHz Pentium D oder AMD Athlon 64 X2 3800+ @ 2,0 GHz   
&#8211; Grafikkarte: Shader Model 3 mit 256 MB   
&#8211; RAM: 1 GB (XP), 2 GB (Vista, Windows 7)   
&#8211; Festplatte: 9 GB

**Empfohlene Systemanforderungen**    
&#8211; CPU: Core 2 Duo E4300 oder AMD Athlon X2 5000+   
&#8211; Grafikkarte: Shader Model 4 mit 256 MB   
&#8211; RAM: 2 GB   
&#8211; Controller: Xbox 360 Controller für Windows

   

* * *

   

