---
title: Fable 3 – Verspätet sich
author: gamevista
type: news
date: 2010-08-03T18:56:14+00:00
excerpt: '<p>[caption id="attachment_2090" align="alignright" width=""]<img class="caption alignright size-full wp-image-2090" src="http://www.gamevista.de/wp-content/uploads/2010/08/fable3_small.jpg" border="0" alt="Fable III" title="Fable III" align="right" width="140" height="100" />Fable III[/caption]Publisher <strong>Microsoft </strong>hat offiziell bestätigt das der dritte Teil des Rollenspiels Fable nicht zeitglich für PC und Xbox 360 erscheinen zu lassen. Demnach soll die Xbox 360-Version am 26. Oktober 2010 erhältlich sein. Einen neuen Veröffentlichungstermin für die PC-Version gibt es bisher leider nicht.</p> '
featured_image: /wp-content/uploads/2010/08/fable3_small.jpg

---
Den Grund für die Verschiebung gibt der Publisher damit an, dass die verschiedenen Versionen noch für die beiden Systeme optimiert werden müssen.

 

   

* * *

   

