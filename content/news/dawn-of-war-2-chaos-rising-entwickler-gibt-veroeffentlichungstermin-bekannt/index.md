---
title: 'Dawn of War 2: Chaos Rising – Entwickler gibt Termin bekannt'
author: gamevista
type: news
date: 2009-11-27T21:42:27+00:00
excerpt: '<p>[caption id="attachment_1116" align="alignright" width=""]<img class="caption alignright size-full wp-image-1116" src="http://www.gamevista.de/wp-content/uploads/2009/11/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]Das Addon <strong>Dawn of War 2: Chaos Rising</strong> wird ja bekanntlich im März 2010 veröffentlicht. Nun hat der Entwickler <a href="http://de.thq.com/de/game/detail/5675" target="_blank">Relic Entertainment</a> einen konkreten Tag für das umfangreiche Update benannt. So wird <strong>Dawn of War 2: Chaos Rising</strong> am 12. März 2010 in Deutschland erscheinen.</p> '
featured_image: /wp-content/uploads/2009/11/1_1280x800_small.jpg

---
Im neusten Addon wird der Spieler das Kommando der Blood Ravens übernehmen um sich gegen die neue Rasse, die Chaos Space Marines, zu verteidigen. Unter anderem werden auch neue Spezialfähigkeiten, neue Units und eine Erhöhung der Levelgrenze auf das Level 30 in Chaos Rising enthalten sein.







 