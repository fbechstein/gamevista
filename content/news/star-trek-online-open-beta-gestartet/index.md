---
title: Star Trek Online – Open Beta gestartet
author: gamevista
type: news
date: 2010-01-12T20:43:37+00:00
excerpt: '<p>Die Open Beta Phase zum Online-Rollenspiel <strong>Star Trek Online</strong> ist heute um 19.00 Uhr offiziell gestartet worden. Fans des Star Trek Universums können vom 12. Januar bis zum 26.[caption id="attachment_1302" align="alignright" width=""]<img class="caption alignright size-full wp-image-1302" src="http://www.gamevista.de/wp-content/uploads/2010/01/logo.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" />Star Trek Online[/caption] Januar 2010 das MMO auf Herz und Nieren testen und sich in die Weiten des Alls begeben.</p> '
featured_image: /wp-content/uploads/2010/01/logo.jpg

---
**Star Trek Online** wird vom Entwicklerteam <a href="http://www.startrekonline.com" target="_blank">Cryptic Studios</a> entwickelt und erscheint in Deutschland am 2. Februar 2010 für den PC. Hinzukommen diverse Verkaufsversionen mit unterschiedlichen Inhalten. Das Spiel wird mit englischer Originalvertonung und deutschen Untertiteln erscheinen.

 

<cite></cite>

* * *



* * *

 