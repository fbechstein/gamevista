---
title: Section 8 – Keine Demo für PC
author: gamevista
type: news
date: 2009-09-06T17:14:49+00:00
excerpt: '<p>[caption id="attachment_450" align="alignright" width=""]<img class="caption alignright size-full wp-image-450" src="http://www.gamevista.de/wp-content/uploads/2009/08/section8.jpg" border="0" alt="Section 8" title="Section 8" align="right" width="140" height="100" />Section 8[/caption]Wie immer haben PC Spieler das nachsehen. Für die PC Fans des Actionspiels <strong>Section 8</strong> wird es keine Demo geben. Dies bestätigte Entwickler <a href="http://www.timegate.com/">TimeGate Studios</a> im <a href="http://forums.timegate.com/showthread.php?t=15318">offiziellen Forum</a>, demnach gebe es keine Pläne für eine PC Demo. Zurzeit gibt es nur die Testversion von <strong>Section 8 </strong>für die Xbox 360 Spielekonsole.</p> '
featured_image: /wp-content/uploads/2009/08/section8.jpg

---
Wer sich auch ohne Demo ein Bild machen möchte dem sei der offizielle Launchtrailer empfohlen. **Section 8** erscheint in Deutschland am 9. September für PC und Xbox 360.

[> Zum Section 8 Launchtrailer][1]

 

* * *



* * *

 [1]: videos/item/root/section-8-launch-trailer