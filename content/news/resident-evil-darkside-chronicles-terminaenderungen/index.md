---
title: 'Resident Evil: Darkside Chronicles – Terminänderungen'
author: gamevista
type: news
date: 2009-08-26T15:48:40+00:00
excerpt: '<p>[caption id="attachment_144" align="alignright" width=""]<img class="caption alignright size-full wp-image-144" src="http://www.gamevista.de/wp-content/uploads/2009/07/resident_evil_dc_small1.jpg" border="0" alt="Resident Evil: Darkside Chronicles" title="Resident Evil: Darkside Chronicles" align="right" width="140" height="100" />Resident Evil: Darkside Chronicles[/caption]<strong>Resident Evil: Darkside Chronicles</strong> sollte ja eigentlich am 31. Oktober 2009 in den USA im Handel stehen. <a href="http://www.capcom.com">Capcom </a>hat nun den Veröffentlichungstermin wieder einmal geändert. Demnach soll der exklusiv für die Nintendo Wii ercheinende Zombie Shooter nun endgültig am 27. November 2009 in die Läden kommen.</p> '
featured_image: /wp-content/uploads/2009/07/resident_evil_dc_small1.jpg

---
Außerdem gibt es ein Bundle das das Spiel und den Wii-Zapper enthält. Leider wird aber **Resident Evil: Darkside Chronicles** hierzulande nicht erscheinen da es der erste Teil des Lightgun Shooters **Resident Evil: Umbrella Chronicles** auch nicht geschafft hatte und importiert werden musste.

 

* * *



* * *

 