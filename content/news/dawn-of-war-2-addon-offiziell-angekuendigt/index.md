---
title: Dawn of War 2 – Addon offiziell angekündigt
author: gamevista
type: news
date: 2009-09-20T16:14:34+00:00
excerpt: '<p>Entwickler [caption id="attachment_726" align="alignright" width=""]<img class="caption alignright size-full wp-image-726" src="http://www.gamevista.de/wp-content/uploads/2009/09/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]<a href="http://www.relic.com">Relic Entertainment</a> bestätigte nun offiziell das sie an einem neuen Addon für ihr Strategiespiel <strong>Dawn of War 2</strong> arbeiten. Das Addon soll den Namen <strong>Chaos Rising</strong> bekommen und führt als Neuerung die Chaos Fraktion ein. Weitere Details oder einen Veröffentlichungstermin wurden leider nicht näher benannt.</p> '
featured_image: /wp-content/uploads/2009/09/1_1280x800_small.jpg

---
Zu vermuten ist aber ein Release Anfang des nächsten Jahres.

 

 

* * *



* * *

 