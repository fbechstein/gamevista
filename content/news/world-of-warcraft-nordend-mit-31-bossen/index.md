---
title: World of Warcraft – Nordend mit 31 Bossen?
author: gamevista
type: news
date: 2009-08-11T18:49:58+00:00
excerpt: '<p>[caption id="attachment_163" align="alignright" width=""]<img class="caption alignright size-full wp-image-163" src="http://www.gamevista.de/wp-content/uploads/2009/07/wow_small.png" border="0" alt="World of Warcraft " title="World of Warcraft " align="right" width="139" height="100" />World of Warcraft [/caption]Lead Game Designer Greg Street von <a href="http://www.eu.blizzard.com/de">Blizzard </a>ließ in einer Diskussion eine Behauptung im Raum stehen die selbst hartgesottene Fans des Online Rollenspiels <strong>World of Warcraft</strong><span> </span>überraschen dürfte. Denn nach seiner Aussage soll die nächste große Schlachtzugsinstanz, im Nordend Finale, ganze 31 Boss Monster enthalten. </p>'
featured_image: /wp-content/uploads/2009/07/wow_small.png

---
Der Lich Ling wird höchstwahrscheinlich der Endboss der Eiskronenzitadelle sein und da 31 eine sehr hohe Anzahl ist wird davon ausgegangen das man mehrere Bosse gleichzeitig bekämpfen darf. Nur mal ein kleiner Vergleich zu den letzten Raidinstanzen, mit Naxxramas 15 Bosse, Ulduar 14 oder Karazahn 12. Der Patch 3.3 mit dem die Instanz, die Eiskronenzitadelle, kommen soll ist noch für dieses Jahr geplant.







 

 