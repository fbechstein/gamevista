---
title: 'Battlefield: Bad Company 2 – Beta Client als Download'
author: gamevista
type: news
date: 2010-01-28T09:29:01+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Der Publisher <strong>Electronic Arts</strong> hat nun den PC-Beta Client für den Ego-Shooter <a href="http://www.badcompany2.ea.com/" target="_blank">Battlefield: Bad Company 2</a> freigegeben und ab sofort kann man ihn auf gamevista.de downloaden. Der Client ist 1,4Gb groß und enthält den Spielmodus <strong>Rush</strong>. Darin könnt ihr euch mit bis zu 32 Mitspielern auf der Schneekarte <strong>Port Valdez</strong> austoben.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
„Der Kampf auf Port Valdez erstreckt sich entlang des Wassers, bis hin zu einer großen Ölanlage vor dem Hintergrund Alaskas Gebirgskette. Mit allen Mitteln versuchen die russischen Einheiten einen Luftangriff zu starten, um die Ölindustrie in der Gegend zu vernichten. Zu ihrem Arsenal gehören sowohl Kampfpanzer, als auch Quad-Bikes und eine mobile und gepanzerte Anti-Luft-Einheit. Es ist ein Kampf, der sehr viel Fokus auf Fahrzeuge legt, bei dem aber auch Infanterieschlachten nicht zu kurz kommen.“

Die Beta startete heute um **18.00** Uhr mitteleuropäischer Zeit. Man kann den Client auch über den Onlinedienst **Steam** beziehen. Der Betazeitraum erstreckt sich von heute den 28. Januar 2010 bis zum 25. Februar 2010. Die Vollversion von <a href="http://www.badcompany2.ea.com/" target="_blank">Battlefield: Bad Company 2</a> wird dann am 4. März 2010 in Deutschland veröffentlicht. Weiterhin wurde für die Xbox 360 Konsole eine Battlefield: Bad Company 2 Demo veröffentlicht. Finden könnt ihr diese auf dem <a href="http://marketplace.xbox.com/en-GB/games/media/66acd000-77fe-1000-9115-d802454108a8/?p=1&#038;of=1&#038;bt=0&#038;sb=1" target="_blank">Xbox Live Marktplatz</a>. Auch diese Version beinhaltet die Karte Port Valdez. Allerdings nur für bis zu 24 Spielern.

[> Zum Battlefield: Bad Company 2 &#8211; PC-Beta Client Download][1]

* * *



* * *

 [1]: downloads/demos/item/demos/battlefield-bad-company-2-beta-client