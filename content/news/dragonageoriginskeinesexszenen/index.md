---
title: 'Dragon Age: Origins – Etwas Erotik, aber keine Sex Szenen'
author: gamevista
type: news
date: 2009-07-27T22:42:21+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Im kommenden <a href="http://www.bioware.com/" target="_blank" title="bioware">Bioware </a>Projekt <strong>Dragon Age: Origins</strong> wird ja nicht gerade mit wenig Gewalt und prüden Mädchen geworben. Aber wie nun auf der Offiziellen Website des Entertainment Rating Board ESRB zu lesen ist, wird es Sex in <strong>Dragon Age: Origins </strong>nicht zu sehen geben.</p>'
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Diverse erotische Zwischensequenzen sind aber enthalten. Leider enden diese aber nach dem „Vorspiel“ und werden ausgeblendet. Die ESRB ist die USK der USA. Klar soweit? <span> </span>Das Entertainment Software Rating Board stufte nun **Dragon Age: Origins** mit einer Altersfreigabe von 17 Jahren ein. Die deutsche Unterhaltungssoftware Selbstkontrolle wiederum teilte mit das sie **Dragon Age: Origins** mit einem Ab-18-Aufkleber versehen. Daraus resultierend erscheint **Dragon Age: Origins** in Deutschland ungeschnitten. Der Release ist auf den 22. Oktober 2009 datiert. 







 