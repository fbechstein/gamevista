---
title: Runes of Magic – Addon im September
author: gamevista
type: news
date: 2009-08-14T09:38:27+00:00
excerpt: '<p style="margin-bottom: 0cm;"> </p> <p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Laut Publisher <a href="http://www.frogster-interactive.de" target="_blank">Frogster Interactive</a>, wird für <strong>Runes of Magic</strong> bald ein Download Zusatzpaket erscheinen. Laut einer Pressemitteilung wird es den Namen <strong>Runes of Magic: The Elven Prophecy</strong> tragen und am 15. September 2009 veröffentlicht.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Neben dem als Download Content erhältlichen Addon wird es auch eine spezielle Box Version für den Handel geben. Diese wird mit 9,99 Euro zu Buche schlagen.

 

<p style="margin-bottom: 0cm;">
  Die Händler Version wird unter anderem ein nette Zugaben enthalten wie ein Mantikor-Haustier, ein zeitlich begrenztes Reittier und eine Weltkarte von <strong>Runes of Magic</strong> im Posterformat. Weiterhin werdet ihr ein so genanntes „Starter Paket“ erhalten was das Leveln etwas zügiger machen soll und kleine Extras enthält.
</p>

<p style="margin-bottom: 0cm;">
  Mit <strong>The Elven Legacy</strong> wird <strong>Runes of Magic</strong> mit den Elfen als spielbares Volk und zwei weitere Charakterklassen erweitert. Der Bewahrer ist sehr Naturverbunden und nutzt diese im Kampf ein um Geister zu beschwören. Der Druide, die zweite neue Klasse, setzt auf eine Mischung aus Heil- und Angriffszaubern.
</p>







 