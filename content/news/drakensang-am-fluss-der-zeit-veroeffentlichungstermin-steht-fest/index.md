---
title: 'Drakensang: Am Fluss der Zeit – Veröffentlichungstermin steht fest'
author: gamevista
type: news
date: 2010-01-07T16:17:06+00:00
excerpt: '<p>[caption id="attachment_437" align="alignright" width=""]<img class="caption alignright size-full wp-image-437" src="http://www.gamevista.de/wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg" border="0" alt="Drakensang: Am Fluss der Zeit" title="Drakensang: Am Fluss der Zeit" align="right" width="140" height="100" />Drakensang: Am Fluss der Zeit[/caption]Der Publisher <a href="http://www.am-fluss-der-zeit.de/index.php" target="_blank">dtp Entertainment</a> gab nun den offiziellen Veröffentlichungstermin für das Rollenspiel <strong>Drakensang: Am Fluss der Zeit</strong> bekannt. So wird das Spiel vom Entwicklerteam Radeon Labs bereits am 19. Februar 2010 in Deutschland erscheinen. Der zweite Teil von <strong>Drakensang </strong>wird auf den Regeln des Pen & Paper-Spiels „Das Schwarze Auge basieren.</p> '
featured_image: /wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg

---
Die Storyline von **Drakensang: Am Fluss der Zeit**, wird vor dem ersten Teil angesiedelt sein und bildet somit das Prequel zu **Drakensang**.

 

 

<cite></cite>

* * *



* * *

 