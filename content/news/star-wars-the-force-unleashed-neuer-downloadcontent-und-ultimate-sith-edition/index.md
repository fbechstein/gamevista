---
title: 'Star Wars: The Force Unleashed – Neuer DLC und Ultimate Sith Edition'
author: gamevista
type: news
date: 2009-07-31T16:24:36+00:00
excerpt: '<p>[caption id="attachment_233" align="alignright" width=""]<a href="http://www.lucasarts.com" target="_blank" title="lucasarts"><img class="caption alignright size-full wp-image-233" src="http://www.gamevista.de/wp-content/uploads/2009/07/swu_small.png" border="0" alt="Star Wars: The Force Unleashed" title="Star Wars: The Force Unleashed" align="right" width="140" height="100" />LucasArts </a>lucasarts[/caption]wird im Herbst eine neu aufgelegte Version von <strong>Star Wars: The Force Unleashed</strong> zusammen mit einem neuen Downloadcontent veröffentlichen. Dies verkündete <a href="http://www.lucasarts.com" target="_blank" title="lucasarts">LucasArts </a>im Rahmen der Comicon 2009 in San Diego. Das Downloadlevel wird für Xbox 360 und Playstation 3 erhältlich sein. Die neue Mission knüpft an das Storyende von The Force Unleashed an und bringt den Spieler in eine neue Position an der Seite des Imperators. </p>'
featured_image: /wp-content/uploads/2009/07/swu_small.png

---
Ihr werdet auf den Planeten Tatooine geschickt um Obi Wan Kenobi aufzuspüren. Dabei bekommt ihr jede Menge berühmter Schauplätze zu sehen wie den Palast von Jabba the Hutt oder ihr trefft den Kopfgeldjäger Boba Fett. Neben dem Downloadpaket wird <a href="http://www.lucasarts.com" target="_blank" title="lucasarts">LucasArts </a>noch eine so genannte **Star Wars: The Force Unleashed – Ultimate Sith Edition** releasen. Diese enthält dann das Hauptspielt plus alle zum Download erschienenen Downloadpakete inklusive der neuen Tatooine Mission. Außerdem wird ein noch unveröffentlichter Level, der den Spieler zum Planeten Hoth schickt, veröffentlicht. Auf dem Eisplaneten versucht ihr die Rebellen endgültig zu zerschlagen. Während der Spieler die aus **Star Wars: Episode V – Das Emperium** schlägt zurück bekannten Schauplätze erkundet, begegnet er auch dem legendären Jedi-Ritter Luke Skywalker. **Star Wars: The Force Unleashed** wurde über sechs Millionen mal verkauft und erschien für Nintendo DS, Nintendo Wii, PlayStation 3 und Xbox 360.







 

 