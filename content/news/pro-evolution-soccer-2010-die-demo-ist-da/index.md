---
title: Pro Evolution Soccer 2010 – Die Demo ist da
author: gamevista
type: news
date: 2009-09-17T08:07:35+00:00
excerpt: '<p>[caption id="attachment_107" align="alignright" width=""]<img class="caption alignright size-full wp-image-107" src="http://www.gamevista.de/wp-content/uploads/2009/07/pes2010_small.jpg" border="0" alt="Pro Evolution Soccer 2010" title="Pro Evolution Soccer 2010" align="right" width="140" height="100" />Pro Evolution Soccer 2010[/caption]Gestern wurde die Demo erst angekündigt und heute morgen ist sie nun endlich erhältlich.<br /> <a href="http://www.de.games.konami-europe.com">Konami </a>veröffentlichte nach langem Warten die <strong>Pro Evolution Soccer 2010 Demo</strong>. Anspielen dürft ihr die Vereinsmannschaften von FC Liverpool und FC Barcelona sowie die Nationalmannschaften von Italien, Deutschland, Spanien und Frankreich.</p> '
featured_image: /wp-content/uploads/2009/07/pes2010_small.jpg

---
In 5 Minuten Matches könnt ihr euch dann wie jedes Jahr die Frage stellen ob es PES oder Fifa sein soll. Die Demo bringt 900 Megabyte mit die herunter geladen werden wollen, wie immer findet ugr diese bei uns im Downloadbereich.

[> Zum Pro Evolution Soccer 2010 &#8211; Demo Download][1]

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/pro-evolution-soccer-2010