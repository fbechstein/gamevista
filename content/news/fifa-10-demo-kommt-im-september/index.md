---
title: Fifa 10 – Demo kommt im September
author: gamevista
type: news
date: 2009-08-24T18:30:20+00:00
excerpt: '<p>[caption id="attachment_115" align="alignright" width=""]<img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" />Fifa 10[/caption]Publisher <a href="http://www.electronic-arts.de">Electronic Arts</a> bestätigte heute per Pressemitteilung das die Demo zum Fußballspiel <strong>Fifa 10</strong> bereits in den Startlöchern stehen soll. Demnach soll die Version zum Antesten bereits am 10. September 2009 für PC, PlayStation 3 und Xbox 360 verfügbar sein.</p> '
featured_image: /wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg

---
Folgende Mannschaften stehen dabei zur Auswahl: Chelsea, Barcelona, Juventus Turin, Bayern München, Marseille und Chicago Fire. Sobald die Demo erhältlich ist könnt ihr sie natürlich mit schnellem Speed bei uns ziehen.

* * *



* * *

 