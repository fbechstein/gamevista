---
title: 'Left 4 Dead: Dead before Dawn – Neue Kampagne als Download veröffentlicht'
author: gamevista
type: news
date: 2009-11-02T16:43:15+00:00
excerpt: '<p>[caption id="attachment_984" align="alignright" width=""]<img class="caption alignright size-full wp-image-984" src="http://www.gamevista.de/wp-content/uploads/2009/11/l4d_small.jpg" border="0" alt="Left 4 Dead" title="Left 4 Dead" align="right" width="140" height="100" />Left 4 Dead[/caption]Für den Zombie-Shooter <strong>Left 4 Dead</strong> wurde heute endlich die die Mod Kampagne <strong>Dead before Dawn</strong> veröffentlicht. Hier ziehen die vier Überlebenden Bill, Louis, Zoey und Francis in die städtische Mall ein. Angelehnt ist das Ganze an den Zombiestreifen Dawn of the Dead.</p> '
featured_image: /wp-content/uploads/2009/11/l4d_small.jpg

---
Wohlgemerkt ist diese Version noch eine Beta, sie enthält also Bugs kann aber im Koop-Modus ohne größere Probleme durchgespielt werden. Die Kampagne kommt als Zip Archiv daher. Das Archiv sollten sie in \Steam\steamapps\common\left 4 dead\left4dead\addons entpacken. Sie müssen darauf achten das sie innerhalb des Mod-Ordners keine doppelten Unterordner haben. Wenn sie das getan haben starten sie **Left 4 Dead** und wählen sie unter dem Kampagnen Menü den Menüpunkt „Addon Kampagnen“ und starten dort **Dead before Dawn**. Wenn sie Soundprobleme bekommen sollten hilft hier der Konsolenbefehl snd_rebuildaudiocache. 

[> Zum Left 4 Dead: Dead before Dawn &#8211; Kampagnen Download][1]

Hier die Liste von den Entwicklern was alles in der Kampagne enthalten ist:

  * Versus and COOP mode
  * 6 Maps (5 + finale) ( NO FINALE in current release, this is COMING SOON)
  * Over 200 custom models/props
  * Randomised set pieces that ensure your DBD experience is never the same!
  * Custom composed music for special events, key locations and even custom director music exclusive to this campaign 
  * Professional voice overs
  * Unique never before seen DotD inspired vistas
  * + MUCH MORE

* * *



* * *

 

 [1]: downloads/patches/item/patches/left-4-dead-dead-before-dawn-kampagne