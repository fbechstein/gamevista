---
title: Herr der Ringe Online – Gutschein auf der gamescom 2010
author: gamevista
type: news
date: 2010-07-28T16:36:10+00:00
excerpt: '<p>[caption id="attachment_1004" align="alignright" width=""]<img class="caption alignright size-full wp-image-1004" src="http://www.gamevista.de/wp-content/uploads/2009/11/lotro_small.jpg" border="0" alt="Herr der Ringe Online" title="Herr der Ringe Online" align="right" width="140" height="100" />Herr der Ringe Online[/caption]Publisher <strong>Codemasters </strong>wird das ab Herbst kostenlos spielbare Online-Rollenspiel <a href="http://www.lotro-europe.com/" target="_blank">Herr der Ringe Online</a>, auf der Gamescom 2010 in Köln vorstellen. Als Free-To-Play-Titel wird der bereits im Jahr 2007 erschienene Titel neu aufgelegt und völlig kostenfrei spielbar sein.</p> '
featured_image: /wp-content/uploads/2009/11/lotro_small.jpg

---
Natürlich gibt es auch hier Upgrade-Möglichkeiten die bessere Gegenstände oder schnelleren Erfahrungszuwachs gegen echtes Geld ermöglichen. Im Zuge der Messe wird ein kleines Geschenk an die Besucher am Codemaster-Stand verteilt. Im neuen Itemshop von <a href="http://www.lotro-europe.com/" target="_blank">Herr der Ringe Online</a> könnt ihr dann einen Gutschein im Wert von 10 Euro einlösen.

 

   

* * *

   

