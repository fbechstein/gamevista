---
title: Tales of Monkey Island – Erste Episode kostenlos
author: gamevista
type: news
date: 2009-09-18T13:08:16+00:00
excerpt: '<p>[caption id="attachment_717" align="alignright" width=""]<img class="caption alignright size-full wp-image-717" src="http://www.gamevista.de/wp-content/uploads/2009/09/monkeyse_small.png" border="0" alt="Tales of Monkey Island" title="Tales of Monkey Island" align="right" width="140" height="100" />Tales of Monkey Island[/caption]Publisher <a href="http://www.lucasarts.com">LucasArts Entertainment</a> gab heute bekannt das sie die erste Episode ihrer <strong>Tales of Monkey Island</strong> Reihe am 19. September 2009 kostenlos zum Download anbieten werden. Ihr fragt euch warum? Am kommenden Samstag ist der <a href="http://www.talklikeapirate.com/"><em>International Talk Like A Pirate Day</em></a>! Kennt ihr noch nicht?</p> '
featured_image: /wp-content/uploads/2009/09/monkeyse_small.png

---
Nun an diesem Tag werden alle Piratenfans dazu aufgerufen ihren typischen Akzent freien Lauf zu lassen und diesen Tag damit zu würdigen.

[Telltales Games][1] möchte mit ihrem Angebot ihren Teil dazu beitragen und bieten euch ab 1 Uhr Samstag Nacht kostenlos Teil 1 „**The Screaming Narwhal**“ der Reihe an.   
Weiterhin wird auch am Samstag das komplette Paket der **Tales of Monkey Island** Reihe um fünf Euro günstiger angeboten.

 

* * *



* * *

 

 [1]: http://www.telltalegames.com/playlikeapirate