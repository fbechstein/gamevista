---
title: Unreal Tournament 3 – Neues Community Mappack erschienen
author: gamevista
type: news
date: 2009-09-01T23:00:00+00:00
excerpt: '<p>[caption id="attachment_574" align="alignright" width=""]<img class="caption alignright size-full wp-image-574" src="http://www.gamevista.de/wp-content/uploads/2009/09/ut3.jpg" border="0" alt="Unreal Tournament 3" title="Unreal Tournament 3" align="right" width="140" height="100" />Unreal Tournament 3[/caption]<a href="http://www.epicgames.com">Epic Games</a> hat heute ein weiteres Community Mappack veröffentlicht. Die Bonus Karten sind in der nunmehr vierten Ausführung für den Multiplayer Shooter <strong>Unreal Tournament 3</strong> ab heute bei uns zum Download verfügbar. Das Update enthält nur Karten die von den Fans des Spiels stammen, diese sind auch für verschiedene Spielmodi konzipiert worden.</p> '
featured_image: /wp-content/uploads/2009/09/ut3.jpg

---
 

[> Zum Unreal Tournament 3 &#8211; Bonus Pack #3 Vol 4. Download][1]

* * *



* * *

 [1]: downloads/patches/item/patches/unreal-tournament-3-bonus-pack-3-vol-4-mit-neuen-karten