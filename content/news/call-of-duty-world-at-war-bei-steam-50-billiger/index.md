---
title: 'Call of Duty: World at War – Bei Steam 50% billiger'
author: gamevista
type: news
date: 2009-08-12T10:54:26+00:00
excerpt: '<p>[caption id="attachment_231" align="alignright" width=""]<img class="caption alignright size-full wp-image-231" src="http://www.gamevista.de/wp-content/uploads/2009/07/1280x1024_small.jpg" border="0" alt="Call of Duty: World at War" title="Call of Duty: World at War" align="right" width="140" height="100" />Call of Duty: World at War[/caption]<a href="http://store.steampowered.com/app/10090/">Valve</a> hat mal wieder ein Schnäppchen für uns auf ihrer Onlineplattform <a href="http://store.steampowered.com/app/10090/">Steam</a> bereitgestellt. Denn den Ego <strong>Shooter Call of Duty: World at War</strong> gibt’s momentan für 50% weniger Moneten. Heißt im Klartext 24,99 Euro. </p>'
featured_image: /wp-content/uploads/2009/07/1280x1024_small.jpg

---
Wenn ihr zu den wenigen gehört die bisher den Action Titel noch nicht erstanden haben solltet ihr euch aber beeilen. Das Angebot gilt nämlich nur noch bis morgen, Donnerstag den 13. August. Hier geht’s zum [Steam Shop][1]. 







 

 

 

 [1]: http://store.steampowered.com/app/10090/