---
title: Resident Evil 5 – Zusätzliche Skins für PC Spieler
author: gamevista
type: news
date: 2009-07-22T11:17:07+00:00
excerpt: '<span style="font-weight: normal">[caption id="attachment_99" align="alignright" width=""]<img class="caption alignright size-full wp-image-99" src="http://www.gamevista.de/wp-content/uploads/2009/07/residentevil_small3.png" border="0" alt="Resident Evil 5" title="Resident Evil 5" align="right" width="140" height="100" />Resident Evil 5[/caption]Um die Wartezeit auf die PC Version von <strong>Resident Evil 5</strong> etwas zu entlohnen beschert euch <a href="http://www.residentevil.com/" target="_blank" title="Resident Evil 5">Capcom</a> zusätzliche Kostüme für eure Helden.</span> '
featured_image: /wp-content/uploads/2009/07/residentevil_small3.png

---
<p class="western">
  <span style="font-weight: normal">Chris wird in einem Gladiator Outfit und Sheva in einem Business Anzug zu bestaunen sein. Ob auch PS3 und Xbox 360 Besitzer in den Genuss kommen werden hat <a href="http://www.residentevil.com" target="_blank">Capcom</a> noch nicht bestätigt. <br />Im Vergleich zur Konsolenversion wurde im Mercenaries Modus die Gegner Anzahl um das dreifache erhöht, was für ordentliches Chaos auf dem Bildschirm sorgen sollte.</p> 
  
  <p>
    </span>
  </p>
  
  
  
  <p>
    
  </p>
  
  