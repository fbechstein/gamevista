---
title: Gamescom 2010 – Sony gibt Lineup bekannt
author: gamevista
type: news
date: 2010-07-28T16:48:17+00:00
excerpt: '<p>[caption id="attachment_2062" align="alignright" width=""]<img class="caption alignright size-full wp-image-2062" src="http://www.gamevista.de/wp-content/uploads/2010/07/gamescom_small.png" border="0" alt="Gamescom 2010" title="Gamescom 2010" align="right" width="140" height="100" />Gamescom 2010[/caption]Der Publisher <strong>Sony Online Entertainment</strong> hat den offiziellen Termin für die Pressekonferenz auf der Spielemesse <a href="http://www.gamescom.de" target="_blank">Gamescom 2010</a> in Köln veröffentlicht. Am 17. August um 18.00 Uhr wird der Publisher und PlayStation-Entwickler einige Spiele-Highlights vorstellen.</p> '
featured_image: /wp-content/uploads/2010/07/gamescom_small.png

---
Neben **Killzone 3** wird auch der lang erwartete Nachfolger von **LittleBigPlanet** präsentiert.

**Die Liste der Spieletitel sieht wie folgt aus:**

  * Killzone 3
  * Gran Turismo 5
  * inFamous 2
  * LittleBigPlanet 2
  * DC Universe Online
  * Journey
  * Motorstorm: Apocalypse
  * Sorcery

   

* * *

   

