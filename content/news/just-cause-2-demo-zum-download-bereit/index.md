---
title: Just Cause 2 – Demo zum Download bereit
author: gamevista
type: news
date: 2010-03-09T17:05:27+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Der Publisher <strong>Square Enix</strong> hat bereits seit Freitag die Demo des Actionspiels Just Cause 2 über die Onlineplattform Steam bereitgestellt. Nun ist auch die herunterladbare Version veröffentlicht worden. Die beiden Testversionen sind inhaltlich gleich, dennoch müssen sie sich auch bei dieser Version bei Steam anmelden um die Demo spielen zu können.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
[> Zur Juse Cause 2 &#8211; Demo    
][1] 

 

<cite></cite>

   

* * *

   



 

 [1]: downloads/demos/item/demos/just-cause-2-demo