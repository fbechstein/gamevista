---
title: Gran Turismo 5 – Veröffentlichungsdatum bekannt
author: gamevista
type: news
date: 2010-11-14T19:34:08+00:00
excerpt: '<p>[caption id="attachment_2339" align="alignright" width=""]<img class="caption alignright size-full wp-image-2339" src="http://www.gamevista.de/wp-content/uploads/2010/10/gt5.jpg" border="0" alt="Gran Turismo 5" title="Gran Turismo 5" align="right" width="140" height="100" />Gran Turismo 5[/caption]Publisher <strong>Sony </strong>hat endlich den genauen Veröffentlichungstermin für das Rennspiel <a href="http://www.gran-turismo.com" target="_blank">Gran Turismo 5</a> bekannt gegeben. Lange wurde gemutmaßt ob der fünfte Teil noch dieses Jahr in den Handel kommt. Nun wurde der 24. November 2010 als finaler Releasetermin benannt.</p> '
featured_image: /wp-content/uploads/2010/10/gt5.jpg

---
Der Titel erscheint exklusiv für Playstation 3.

 

   

* * *

   

