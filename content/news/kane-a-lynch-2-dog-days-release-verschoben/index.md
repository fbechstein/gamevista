---
title: 'Kane & Lynch 2: Dog Days – Release verschoben'
author: gamevista
type: news
date: 2010-07-13T16:52:14+00:00
excerpt: '<p>[caption id="attachment_1576" align="alignright" width=""]<img class="caption alignright size-full wp-image-1576" src="http://www.gamevista.de/wp-content/uploads/2010/03/kane-and-lynch2.jpg" border="0" alt="Kane & Lynch 2: Dog Days" title="Kane & Lynch 2: Dog Days" align="right" width="140" height="100" />Kane & Lynch 2: Dog Days[/caption]Gute Nachrichten für die Fans des Actionspiels <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a>. Der ursprüngliche Veröffentlichungstermin am 27. August 2010 ist nicht mehr gültig, denn Publisher Eidos hat offiziell verkündet das der Titel bereits eine Woche früher in den Läden stehen wird.</p> '
featured_image: /wp-content/uploads/2010/03/kane-and-lynch2.jpg

---
Demnach erscheint <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a> am 20. August 2010.

 

 

   

* * *

   



 