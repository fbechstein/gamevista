---
title: Pro Evolution Soccer 2010 – Bald zum Budget Preis
author: gamevista
type: news
date: 2010-04-19T19:36:29+00:00
excerpt: '<p>[caption id="attachment_1703" align="alignright" width=""]<img class="caption alignright size-full wp-image-1703" src="http://www.gamevista.de/wp-content/uploads/2010/04/pes2010_small.jpg" border="0" alt="PES 2010" title="PES 2010" align="right" width="140" height="100" />PES 2010[/caption]Der Entwickler <strong>Konami </strong>kündigte offiziell per Pressemitteilung an, dass das Fußballspiel <a href="http://www.konami.com/pes2010" target="_blank">Pro Evolution Soccer 2010</a> demnächst günstiger zu kaufen sein wird. Demnach wird es ab dem 29. April den Fußballtitel für PlayStation 3, Xbox 360 und Wii für ca. 29,95 EUR zu kaufen geben.</p> '
featured_image: /wp-content/uploads/2010/04/pes2010_small.jpg

---
PES 2010 für die PlayStation 2 und PSP wird für ca. 19,95 EUR erhältlich sein. Die PC Version wird sogar nur ca 15 Euro kosten.

 

   

* * *

   



<span style="font-size: 11pt; line-height: 115%; font-family: "><br /></span>