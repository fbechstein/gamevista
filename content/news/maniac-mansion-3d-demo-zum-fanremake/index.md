---
title: Maniac Mansion 3D – Demo zum Fanremake
author: gamevista
type: news
date: 2010-02-02T22:29:05+00:00
excerpt: '<p>[caption id="attachment_1407" align="alignright" width=""]<img class="caption alignright size-full wp-image-1407" src="http://www.gamevista.de/wp-content/uploads/2010/02/maniacmansion1.jpg" border="0" alt="Maniac Mansion" title="Maniac Mansion" align="right" width="140" height="100" />Maniac Mansion[/caption]Zum Kult-Adventure <strong>Maniac Mansion</strong> haben Fans des legendären Lucas Arts-Klassikers, ein Remake in 3D entwickelt. Dazu wurde nun die Demo-Version veröffentlicht. Das Team rund um den Entwickler Vampyre Games, hat im Rahmen des Studiums der Software 3D Game Studio, die Chance genutzt um <strong>Maniac Mansion</strong> in der dritten Dimension erstrahlen zu lassen.</p> '
featured_image: /wp-content/uploads/2010/02/maniacmansion1.jpg

---
So wurde die alte Grafik komplett überarbeitet und zeigt die Schauplätze des Point&Click-Adventures im neuen Glanz. Die 178 Megabyte große Testversion ist ab sofort bei uns zum Download verfügbar. Die Vollversion soll voraussichtlich im 2. Quartal 2010 als Freeware erscheinen.

[> Zur Maniac Mansion 3D &#8211; Demo][1]

* * *



* * *

 [1]: downloads/demos/item/demos/maniac-mansion-3d-demo