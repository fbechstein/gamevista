---
title: Starcraft 2 – Auch hier droht die Schere
author: gamevista
type: news
date: 2009-08-30T19:06:13+00:00
excerpt: '<p>[caption id="attachment_110" align="alignright" width=""]<img class="caption alignright size-full wp-image-110" src="http://www.gamevista.de/wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="105" />Starcraft 2[/caption]Computerspiele Fans sind heutzutage ein bevormundetes Volk. Gerade eben erst wurde bekannt das das neue <strong>Alien vs. Predator</strong> vielleicht nicht in Deutschland erscheinen wird. Dann die Nachricht vor ein paar Tagen das es <strong>Diablo 3</strong> nur geschnitten zu kaufen gibt. Heute die nächste Meldung über eins der Top Games der nächsten Generation. <strong>Starcraft 2</strong> wird für den deutschen Markt nur geschnitten erscheinen.</p> '
featured_image: /wp-content/uploads/2009/07/starcraft2_gameplay_small.jpg

---
Allen Dilling, Mitarbeiter bei [Blizzard Entertainment][1] teilte nun mit das man bereits dabei wäre **Starcraft 2: Wings of Liberty** für die verschiedenen Märkte anzupassen. Das heißt im Detail, es werden Blut und Sterbeanimationen verändert bzw. entfernt und Farben geändert. Als Beispiel gibt es Szenen worin Gegner zerschnitten werden. So etwas wird es in der deutschen Version nicht geben.  
Dilling sagte aber auch das sie ihr bestes geben werden um trotzdem ein gutes Spielgefühl zu vermitteln

 

* * *



* * *

 

 [1]: http://eu.blizzard.com/de/