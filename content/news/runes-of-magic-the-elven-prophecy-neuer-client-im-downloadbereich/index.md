---
title: 'Runes of Magic: The Elven Prophecy – Neuer Client im Downloadbereich'
author: gamevista
type: news
date: 2009-09-15T23:00:00+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Publisher <a href="http://www.runesofmagic.com">Frogster Entertainment</a> veröffentlichte gestern den neuen Patch 2.0 <strong>The Elven Prophecy</strong>. Über 1,9 Millionen Nutzer sind mittlerweile im kostenlosen Online Rollenspiel <strong>Runes of Magic</strong> registriert. Pünktlich um 12 Uhr gingen die Server am 15. September für den Spielstart wieder ans Netz.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Damit genug Platz für neue Spieler besteht wurde in Frankfurt am Main ein weiterer europäischer Server installiert. Damit gibt es nun 19 Server Weltweit. Natürlich bieten wir euch den neuen Client im Downloadbereich ab sofort an. Der neue Client ist in zwei Teile gegliedert, hier findet ihr beide Links.  
[  
> Zum Runes of Magic: The Elven Prophecy &#8211; Client Teil 1][1]  
[> Zum Runes of Magic: The Elven Prophecy &#8211; Client Teil 2  
][2] 

 

* * *



* * *

 [1]: downloads/demos/item/demos/runes-of-magic-the-elven-prophecy-client-20-teil-1
 [2]: downloads/demos/item/demos/runes-of-magic-the-elven-prophecy-client-20-teil-2