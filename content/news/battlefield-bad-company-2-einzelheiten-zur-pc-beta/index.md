---
title: 'Battlefield: Bad Company 2 – Einzelheiten zur PC-Beta'
author: gamevista
type: news
date: 2010-01-10T17:19:19+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Zur bald startenden PC-Beta von <strong>Battlefield: Bad Company 2</strong> wurden nun weitere Details bekannt gegeben. So wird die geschlossene Beta am 28. Januar 2010 starten und bis zum 25. Februar 2010 dauern. Dies wurde offiziell vom Entwickler <a href="http://www.badcompany2.ea.com" target="_blank">DICE</a> bestätigt. Mit bis zu 32 Mitspielern können die Auserwählten auf der Karte Port Valdez das Spiel auf Herz und Nieren testen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Wer sich noch keinen Beta-Key sichern konnte, hat die Möglichkeit sich über den EA-Store oder bei Gamestop.com **Battlefield: Bad Company 2** vorzubestellen. Dies soll eine Beta-Teilnahme garantieren. Zur offenen Beta gibt es noch keinerlei Details.

<cite></cite>

* * *



* * *

 