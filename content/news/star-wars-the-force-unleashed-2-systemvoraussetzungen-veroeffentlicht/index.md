---
title: 'Star Wars: The Force Unleashed 2 – Systemvoraussetzungen veröffentlicht'
author: gamevista
type: news
date: 2010-10-21T20:04:44+00:00
excerpt: '<p>[caption id="attachment_1192" align="alignright" width=""]<img class="caption alignright size-full wp-image-1192" src="http://www.gamevista.de/wp-content/uploads/2009/12/tfu2.jpg" border="0" alt="Star Wars: The Force Unleashed 2" title="Star Wars: The Force Unleashed 2" align="right" width="140" height="100" />Star Wars: The Force Unleashed 2[/caption]Publisher <strong>LucasArst </strong>hat die Systemvoraussetzungen für die PC-Version von <a href="http://www.lucasarts.com/games/theforceunleashed2" target="_blank">Star Wars: The Force Unleashed 2</a> veröffentlicht. Demnach benötigt ihr als Minimum einen Dual Core Prozessor und 2 Gigabyte Ram. Zusätzlich sollte eure Grafikkarte über 256 MB Video Memory mit Shader 3.0 Support verfügen. <a href="http://www.lucasarts.com/games/theforceunleashed2" target="_blank">Star Wars: The Force Unleashed 2</a> erscheint am 28. Oktober 2010 im Handel.</p> '
featured_image: /wp-content/uploads/2009/12/tfu2.jpg

---
Die Systemvoraussetzungen im Überblick:

**Minimale Systemvoraussetzungen**

Processor: Intel Core 2 Duo 2.4 GHz or AMD Athlon X2 5200+   
Memory: 2 GB RAM   
Video: 256 MB Video Memory with Shader 3.0 support – ATI Radeon HD 2600 / NVIDIA GeForce 8600 GT   
Hard disk space: 10GB + 1GB Swapfile   
Operating system: Windows XP SP3, Windows Vista SP2, or Windows 7   
Sound: 100% DirectX 9.0c compatible Audio Device   
DirectX: DirectX 9.0c (March 2009)

**Empfohlene Systemvoraussetzungen**

Processor: Intel Core 2 Duo 2.8 GHz or AMD Athlon X2 6000+   
Video: 256 MB Video Memory with Shader 3.0 support – ATI Radeon HD 4800 / NVIDIA GeForce GTX 260   
   

* * *

   

