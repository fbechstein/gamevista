---
title: Borderlands – Game of the Year Edition angekündigt
author: gamevista
type: news
date: 2010-08-30T19:51:45+00:00
excerpt: '<p>[caption id="attachment_2183" align="alignright" width=""]<img class="caption alignright size-full wp-image-2183" src="http://www.gamevista.de/wp-content/uploads/2010/08/smallhelden.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Entwickler <strong>Gearbox </strong>kündigte vor kurzem die Game of the Year Edition im Rahmen einer Twitter-Meldung für das Actionspiel <a href="http://www.borderlandsthegame.com" target="_blank">Borderlands</a> an. Die Sonderedition beinhaltet alle vier Zusatzinhalte die als Download-Content bereits erschienen sind. Zusätzlich gibt es eine Bonus Karte für Xbox 360, PlayStation 3 und PC.</p> '
featured_image: /wp-content/uploads/2010/08/smallhelden.jpg

---
Erscheinen wird die GOTY Edition im Oktober 2010. Die beinhalteten DLC\`s sind The Zombie Island of Dr. Ned, Mad Moxxi’s Underdome Riot, The Secret Armory of General Knoxx und Claptrap’s New Robot Revolution.

 

   

* * *

   



 