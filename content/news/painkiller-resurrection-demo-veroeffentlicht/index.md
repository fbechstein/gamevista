---
title: 'Painkiller: Resurrection – Demo veröffentlicht'
author: gamevista
type: news
date: 2009-10-21T21:16:42+00:00
excerpt: '<p />[caption id="attachment_922" align="alignright" width=""]<img class="caption alignright size-full wp-image-922" src="http://www.gamevista.de/wp-content/uploads/2009/10/painkiller_small.png" border="0" alt="Painkiller: Resurrection" title="Painkiller: Resurrection" align="right" width="140" height="100" />Painkiller: Resurrection[/caption]Der Publisher <a href="http://www.jowood.com" target="_blank">Jowood</a> hat heute seine Demo zum Shooter <strong>Painkiller: Resurrection</strong> angeboten.  '
featured_image: /wp-content/uploads/2009/10/painkiller_small.png

---
</p> 

Entwickelt von <a href="http://www.homegrowngames.at/" target="_top">Homegrown Games </a>mit ehemaligen Entwicklern und Moddern für die vorangegangenen Painkiller Spiele in Zusammenarbeit mit Studio Med-Art sowie unter Beratung eines katholischen Priesters, schafft Painkiller Resurrection den Spagat zwischen knallharter, unkomplizierter Action und spannender Story mit aufwändig recherchierten Details. In einer bizarren Welt beeinflusst von mittelalterlichen Vorstellungen über Hölle & Fegefeuer bis hin zu Dantes Purgatorium sorgt eine Mischung aus schnellen straightforward Level und riesigen, frei erkundbaren Arealen mit dem schlimmsten Abschaum sämtlicher Höllen für ein einzigartiges Spielerlebnis.

Die Demo ist 1,3 GB groß und umfasst das Anfangsgebiet des Spiels. 

<a href="downloads/demos/item/demos/painkiller-resurrection-demo" target="_blank">> Zum Painkiller: Resurrection &#8211; Demo Download</a>

* * *



* * *

 