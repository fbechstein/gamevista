---
title: Left 4 Dead 2 – Erstes Addon angekündigt
author: gamevista
type: news
date: 2009-12-15T11:32:57+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Zum Ego-Shooter <strong>Left 4 Dead 2</strong> hat der Entwickler <a href="http://www.l4d.com" target="_blank">Valve</a> ein erstes Addon angekündigt. Der Titel der Erweiterung wird <strong>The Passing</strong> lauten und eine große Überraschung enthalten. Denn wer sich fragte was aus den alten Überlebenden des ersten Teils geworden ist, der wird im neuen Addon eine Zusammenführen der alten und neuen Helden erleben.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
**The Passing** wird nach der Dead Center Kampagne von **Left 4 Dead 2** stattfinden. Dabei treffen sie in einer kleinen Stadt in Georgia auf die alten Überlebenden. Spielbar werden aber weiterhin nur Rochelle, Nick, Ellis und Coach sein.

Neben neuen Karten für die Spielmodi Survival, Versus und Scavange, wird es einen neuen Koop-basierten Spielmodus geben. <a href="http://www.l4d.com" target="_blank">Valve</a> kündigte unter anderem auch eine neue Zombie-Klasse und neue Waffen an. **The Passing** soll im Frühjahr 2010 für PC und Xbox 360 erscheinen.

* * *



* * *

 