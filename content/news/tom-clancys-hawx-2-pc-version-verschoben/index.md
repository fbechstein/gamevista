---
title: Tom Clancy`s H.A.W.X. 2 – PC-Version verschoben
author: gamevista
type: news
date: 2010-09-22T18:40:30+00:00
excerpt: '<p>[caption id="attachment_2000" align="alignright" width=""]<img class="caption alignright size-full wp-image-2000" src="http://www.gamevista.de/wp-content/uploads/2010/07/hawx2_s_003.jpg" border="0" alt="Tom Clancy`s H.A.W.X. 2 " title="Tom Clancy`s H.A.W.X. 2 " align="right" width="140" height="100" />Tom Clancy`s H.A.W.X. 2 [/caption]Im Rahmen einer offiziellen Pressemitteilung hat der Publisher <strong>Ubisoft </strong>bekannt gegeben, dass die PC-Version des Titels <a href="http://hawxgame.de.ubi.com" target="_blank">Tom Clancy`s H.A.W.X. 2</a> verschoben werden musste. Bereits letzten Monat kündigte <strong>Ubisoft </strong>an den Titel erst am 1. Oktober 2010 veröffentlichen zu können.</p> '
featured_image: /wp-content/uploads/2010/07/hawx2_s_003.jpg

---
Nun wurde die Flugzeugsimulation für den PC nochmals verschoben. Unklar ist der neue Veröffentlichungstermin. Auch die Wii-Version wurde auf den 9. November 2010 verschoben. Für die PC-Version ist beim Onlinedienst Steam ist bisher nur die Rede vom Herbst 2010.

 

   

* * *

   

