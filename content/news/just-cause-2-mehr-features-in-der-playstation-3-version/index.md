---
title: Just Cause 2 – Mehr Features in der PlayStation 3 Version
author: gamevista
type: news
date: 2009-11-14T16:43:01+00:00
excerpt: '<p>[caption id="attachment_482" align="alignright" width=""]<img class="caption alignright size-full wp-image-482" src="http://www.gamevista.de/wp-content/uploads/2009/08/just_cause2_small.jpg" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Just Cause 2[/caption]Der Publisher <a href="http://www.justcause.com" target="_blank">Eidos</a> hat auf einem Event in London neues zum Actionspiel<strong> Just Cause 2</strong> bekannt gegeben. So werden in der PlayStation 3-Version exklusive Features vorhanden sein. Die Xbox 360-Fassung wird diese nicht beinhalten. Demnach wird es möglich sein, manuell ein bis zu zehn Minuten langes Video vom Spielgeschehen aufzunehmen.</p> '
featured_image: /wp-content/uploads/2009/08/just_cause2_small.jpg

---
Außerdem wird es eine Automatik-Aufnahmefunktion geben die die letzten 30 Sekunden des Gameplays speichert. Diese Videos werden dann direkt bei Youtube veröffentlicht.

 

 

* * *



* * *