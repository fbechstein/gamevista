---
title: 'Kane & Lynch 2: Dog Days – Entwickler veröffentlichen drei neue DLC`s'
author: gamevista
type: news
date: 2010-09-02T17:25:16+00:00
excerpt: '<p>[caption id="attachment_1576" align="alignright" width=""]<img class="caption alignright size-full wp-image-1576" src="http://www.gamevista.de/wp-content/uploads/2010/03/kane-and-lynch2.jpg" border="0" alt="Kane & Lynch 2: Dog Days" title="Kane & Lynch 2: Dog Days" align="right" width="140" height="100" />Kane & Lynch 2: Dog Days[/caption]Publisher <strong>Square Enix</strong> veröffentlichte heute ganze drei neue Zusatzinhalte für das Actionspiel <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a>. Damit werden vorallem Mehrspieler-Fans auf ihre Kosten kommen. Drei neue Downloadpakete inklusive neuer Maps und neuen Waffen sind ab sofort über Xbox Live, das Playstation Network sowie Steam erhältlich.</p> '
featured_image: /wp-content/uploads/2010/03/kane-and-lynch2.jpg

---
Alle Inhalte können in den drei unterschiedlichen Multiplayer-Modi sowie im Single-Player-Arcade-Modus genutzt werden.

Die Inhalte der DLC Pakete in der Übersicht:

 

**Doggie-Bag-Paket:**

Begebe Dich auf einen neuen Raubzug und schalte neue Achievement Punkte in zwei noch unbekannten Karten frei. Es warten außerdem fünf Waffen in den drei Karten Docks, Hochhaus und Funkturm auf Dich. Letztere war bisher lediglich als Inhalt der Limited Edition erhältlich.

Das Doggie-Bag-Paket ist auf Xbox Live für 560 Microsoft Punkte, auf PSN und über Steam für jeweils €5.99 erhältlich.

**Allianz-Waffen-Paket:**

Erweitere Dein persönliches Waffen-Arsenal durch das Allianz-Waffen-Paket mit sieben neuen, gewaltigen Waffen. Darunter ein eine Handfeuerwaffe mit Schalldämpfer, eine Steele 870 Standard Schrotflinte, ein US Sturmgewehr TOQ SBR, eine Kaliningrad 47 – maßgeschneiderte Maschinenpistole, eine PAC 10 Maschinenpistole mit Schalldämpfer, ein klassischer Polizei-Revolver N 77P und ein TOQ Elite US Sturmgewehr.

Das Alliance Waffen Pack ist auf Xbox Live für 240 Microsoft Punkte, auf PSN und über Steam für jeweils €2.99 erhältlich.

**Multiplayer-Masken-Paket:**

Das Multiplayer-Masken-Paket umfasst acht exklusive Kostüm-Masken, geklaut aus einem chinesischen Souvenirshop! Schließlich möchtest du unerkannt bleiben. Vor allem, wenn Du ein Verräter bist! Das Maskenpaket ist Teil der Limited Edition und ab sofort für alle anderen Spieler bei Xbox Live für 160 Microsoft Punkte, auf PSN und über Steam für jeweils €1.99 erhältlich.

   

* * *

   

