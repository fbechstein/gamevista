---
title: 'Call of Duty: Modern Warfare 2 – Download Content Inhalt bekannt'
author: gamevista
type: news
date: 2010-03-14T18:07:12+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Zum ersten herunterladbaren Inhaltspaket für den Ego-Shooter <a href="http://www.modernwarfare2.infinityward.com" target="_blank">Call of Duty: Modern Warfare </a>2, sind neue Informationen aufgetaucht. So wurde auf dem Microsoft Xbox Live Marketplace, der Inhalt des DLC gesichtet. Laut der Produktbeschreibung des <strong>Stimulus Package</strong>, wird es insgesamt drei neue Karten und zwei überarbeitete Karten aus dem ersten Modern Warfare-Teil geben.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Demnach wurden die Maps Crash und Overgrown grafisch überarbeitet und erstrahlen nun im neuen Glanz. Die drei neuen Karten Bailout, Storm und Salvage führen euch jeweils in einen mehrstöckigen Apartmentkomplex, einen offenen Industriepark mit schweren Maschinen oder in ein weiteres Schneegebiet.

 

<cite></cite>

   

* * *

   



 