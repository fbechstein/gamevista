---
title: 'Command & Conquer 4 – Beta Test und Namensgebung'
author: gamevista
type: news
date: 2009-07-26T12:05:36+00:00
excerpt: '[caption id="attachment_157" align="alignright" width=""]<img class="caption alignright size-full wp-image-157" src="http://www.gamevista.de/wp-content/uploads/2009/07/shot_basg4y50_small.jpg" border="0" alt="Command & Conquer 4" title="Command & Conquer 4" align="right" width="140" height="100" />Command & Conquer 4[/caption] <strong>Command and Conquer</strong> Entwickler <a href="http://www.electronic-arts.de/" target="_blank" title="ea">EA </a>braucht eure Hilfe. Für den neuesten Teil wurde nun ein Wettbewerb ins Leben gerufen, <a href="http://www.electronic-arts.de/" target="_blank" title="ea">EA </a>sucht einen Untertitel für sein Werk und fragt nun die Community. Auf <a href="http://contest.commandandconquer.com/" target="_blank" title="c&c4">dieser Seite</a> könnt ihr eure Nameswünsche vorstellen.<br />'
featured_image: /wp-content/uploads/2009/07/shot_basg4y50_small.jpg

---
Weiterhin soll es wohl eine Beta geben, der Gewinner des Contest erhält einen automatischen Zugang. Einen Termin und Details lies <a href="http://www.electronic-arts.de/" target="_blank" title="ea">EA </a>noch nicht verlauten, natürlich sagen wir euch Bescheid wenn es etwas neues gibt.





