---
title: Mass Effect 2 – New Game+ Modus angekündigt
author: gamevista
type: news
date: 2010-01-04T18:55:10+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Der Spielmodus “<strong>New Game+</strong>", bekannt aus dem ersten Teil von <strong>Mass Effect</strong>, wird laut einer offiziellen Ankündigung im Bioware Community Forum nun auch seinen Platz im zweiten Teil finden. So gibt der „<strong>New Game+</strong>“ Modus dem Spieler die Möglichkeit, nachdem er das Spiel das erste Mal absolviert hat, das Spiel neu zu starten und seinen alten Charakter aus dem Vorgänger inkl. Waffen und Fähigkeiten zu importieren.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Daraufhin werden dann aber Gegner sowie Herausforderungen wesentlich schwieriger. Wer noch fragen hat kann sich das FAQ zum „**New Game+**“ Modus für **Mass Effect 2** durchlesen. Diese Frage- und Antwortsammlung wurde im <a href="http://meforums.bioware.com/forums/viewtopic.html?topic=714910&#038;forum=144&#038;sp=105" target="_blank">offiziellen Forum</a> von BioWare veröffentlicht.

 

* * *



* * *

 