---
title: 'Empire: Total War – Neuer Zusatzinhalt ab sofort verfügbar'
author: gamevista
type: news
date: 2010-02-11T17:42:51+00:00
excerpt: '<p>[caption id="attachment_585" align="alignright" width=""]<img class="caption alignright size-full wp-image-585" src="http://www.gamevista.de/wp-content/uploads/2009/09/empire_totalwar.jpg" border="0" alt="Empire: Total War" title="Empire: Total War" align="right" width="140" height="100" />Empire: Total War[/caption]Ab sofort steht einer neuer Download-Inhalt für das Strategiespiel <a href="http://www.totalwar.com/" target="_blank">Empire: Total War</a> zum herunterladen bereit. Dies teilte der Publisher <strong>SEGA, </strong>im Rahmen einer Pressemitteilung, am heutigen Tage mit. Wie immer gibt es den DLC über den Online-Dienst Steam zu kaufen. Mit 12 neuen Eliteeinheiten des Maratha- und Osmanenvolks, kommt der neue Zusatzinhalt „<a href="http://store.steampowered.com/app/10608/" target="_blank">Elite Units of the East</a>“ und erweitert das Hauptspiel für 2,69 €.</p> '
featured_image: /wp-content/uploads/2009/09/empire_totalwar.jpg

---
Wenn sie noch kein Download-Inhalt besitzen, können sie auch das gesamte Paket mit dem Namen „<a href="http://store.steampowered.com/sub/2892/" target="_blank">Downloadable Content Pack</a>“ erwerben. Die kosten belaufen sich dann auf 11,69 €. Das „<a href="http://store.steampowered.com/sub/2892/" target="_blank">Downloadable Content Pack</a>“ bietet neben dem neuen DLC, auch die drei in der Vergangenheit erschienen Zusatzpakete und das Addon „The Warpath Campaign“

 

* * *



* * *