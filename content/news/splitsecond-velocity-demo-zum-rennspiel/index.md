---
title: 'Split/Second: Velocity – Demo zum Rennspiel'
author: gamevista
type: news
date: 2010-05-23T17:47:10+00:00
excerpt: '<p>[caption id="attachment_1691" align="alignright" width=""]<img class="caption alignright size-full wp-image-1691" src="http://www.gamevista.de/wp-content/uploads/2010/04/splitsecond_hero.jpg" border="0" alt="Split/Second: Velocity" title="Split/Second: Velocity" align="right" width="140" height="100" />Split/Second: Velocity[/caption]Der Publisher <strong>Disney Interactive</strong> veröffentlicht pünktlich zum langen Wochenende die Demo zum vor kurzem veröffentlichten Rennspiel <a href="www2.disney.co.uk/split-second-velocity/" target="_blank">Split/Second: Velocity</a>. PlayStation 3 und Xbox 360 Besitzer konnten schon seit längerem in den Geschmack einer Testversion kommen. Nun sind die PC-Spieler dran.</p> '
featured_image: /wp-content/uploads/2010/04/splitsecond_hero.jpg

---
Mit 1 Gigabyte Umfang enthält die Demo eine komplette Rennstrecke.

 

[> Zum Split/Second: Velocity &#8211; Demo Download][1]

   

* * *

   



 [1]: downloads/demos/item/demos/splitsecond-velocity-demo