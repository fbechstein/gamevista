---
title: R.U.S.E. – Neue Beta-Phase geplant
author: gamevista
type: news
date: 2010-06-02T16:55:43+00:00
excerpt: '<p>[caption id="attachment_1876" align="alignright" width=""]<img class="caption alignright size-full wp-image-1876" src="http://www.gamevista.de/wp-content/uploads/2010/06/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Die letzte Beta ist ja nun schon eine Weile her. Geplant war der Release für das Strategiespiel <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a> ursprünglich im Sommer diesen Jahres. Leider musste der Publisher <strong>Ubisoft </strong>die Veröffentlichung auf September 2010 verschieben. Aus diesem Grund wird über eine neue Beta-Phase nachgedacht.</p> '
featured_image: /wp-content/uploads/2010/06/ruse_small.jpg

---
Dies äußerten **Ubisoft** im Rahmen eines Interviews. Offiziell ist das Ganze zwar noch nicht. Jedoch werden die Fans in den nächsten Wochen weitere Informationen erhalten.

 

 

   

* * *

   

