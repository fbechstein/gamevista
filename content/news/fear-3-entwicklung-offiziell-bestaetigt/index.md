---
title: F.E.A.R. 3 – Entwicklung offiziell bestätigt
author: gamevista
type: news
date: 2010-04-08T18:13:47+00:00
excerpt: '<p>[caption id="attachment_1670" align="alignright" width=""]<img class="caption alignright size-full wp-image-1670" src="http://www.gamevista.de/wp-content/uploads/2010/04/fear3.jpg" border="0" alt="F.E.A.R. 3" title="F.E.A.R. 3" align="right" width="140" height="100" />F.E.A.R. 3[/caption]Es wurde in der letzten Zeit ja viel über einen dritten Teil der F.E.A.R. Serie gemunkelt. Nun hat der Publisher <strong>Warner Bros.</strong> im Rahmen einer Pressemitteilung offiziell die Entwicklung von <a href="http://www.whatisfear.com/" target="_blank">F.E.A.R. 3</a> bestätigt. Erste Neuerung wird ein Koop-Modus sein, der bis zu zwei Spielern die Möglichkeit bieten wird zusammen gegen die Armacham Corporation vorzugehen.</p> '
featured_image: /wp-content/uploads/2010/04/fear3.jpg

---
Beide Spieler können nun Alma\`s Söhne Point Man und Paxton Fettel steuern. Point Man ist ein genetisch modifizierter Super-Soldat der aus dem ersten Teil bekannt sein dürfte. Paxton Fettel benutzt hingegen seine telekinetischen Fähigkeiten. Weiterhin gibt **Warner Bros.** bekannt, dass der Horror-Regisseur John Carpenter mit am Projekt beteiligt ist.

 

 

<cite></cite>

   

* * *

   

