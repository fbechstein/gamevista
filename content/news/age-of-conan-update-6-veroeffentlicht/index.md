---
title: Age of Conan – Update 6 veröffentlicht
author: gamevista
type: news
date: 2010-02-17T16:54:16+00:00
excerpt: '<p>[caption id="attachment_155" align="alignright" width=""]<img class="caption alignright size-full wp-image-155" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderranges_1024x768_small.jpg" border="0" alt="Age of Conan" title="Age of Conan" align="right" width="140" height="100" />Age of Conan[/caption]Zum Online-Rollenspiel <a href="http://www.ageofconan.com" target="_blank">Age of Conan</a>, hat der Entwickler <strong>Funcom </strong>einen neues Update veröffentlicht. Der mittlerweile sechste Inhalts-Patch bringt dabei zahlreiche Neuerungen mit sich. Ab sofort wird es möglich sein, die neue Raid-Instanz <strong>Thoth-Amons Festung</strong> zu besuchen. Mit sieben Bossgegnern und neuen Items steht dem Kampf gegen Thoth Amon nichts mehr im Wege.</p> '
featured_image: /wp-content/uploads/2009/07/borderranges_1024x768_small.jpg

---
Neben der Raidinstanz, wurde auch eine neue Spielmechanik mit dem Namen Gilden-Ansehen eingeführt. „Dabei werden die Aktionen aller Gildenmitglieder dem Ansehen der Gilde zugeführt. Mit fantastischen neuen Gildengebäuden und Treffpunkten, neuen Quests, neuen Händlern für Sozialgegenstände und öffentlichen Ranglisten ergänzt dieses neue Spielelement den Gilden-Spielspaß um eine weitere Ebene“, so **Funcom**. Das komplette <a href="http://forums-eu.ageofconan.com/showthread.php?t=141493" target="_blank">Changelog</a> könnt ihr im offiziellen Forum von <a href="http://www.ageofconan.com" target="_blank">Age of Conan</a> nachlesen.

 

* * *



* * *