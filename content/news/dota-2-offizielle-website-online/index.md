---
title: DotA 2 – Offizielle Website Online
author: gamevista
type: news
date: 2010-11-02T19:25:59+00:00
excerpt: '<p>[caption id="attachment_2344" align="alignright" width=""]<img class="caption alignright size-full wp-image-2344" src="http://www.gamevista.de/wp-content/uploads/2010/10/dota2.jpg" border="0" alt="DotA 2" title="DotA 2" align="right" width="140" height="100" />DotA 2[/caption]Fans des Warcraft-Spielmodus <strong>Defense of the Ancients</strong> dürfen sich über die Entwicklung des zweiten Teils freuen. Nun haben die Entwickler von <strong>Valve </strong>die <a href="http://www.dota2.com/" target="_blank">offizielle Website</a> online gestellt. Zwar gibt es bisher nicht allzu viele Inhalte zu sehen, dennoch ihr habt die Möglichkeit euch per RSS feed, Facebook und Twitter für neue Informationen anzumelden.</p> '
featured_image: /wp-content/uploads/2010/10/dota2.jpg

---
Außerdem gibt es ein Q&A zum zweiten Teil der einige neue Features des Spiels vorstellt. Darunter sind optionale KI und Bots, verschiedene Regionen mit denen ihr zusammen spielen könnt, reconnect support, replays, bookmarking und ein Spectator-Modus.

 

   

* * *

   

