---
title: R.U.S.E. – Demo-Version erscheint bald
author: gamevista
type: news
date: 2010-08-24T10:46:40+00:00
excerpt: '<p>[caption id="attachment_2155" align="alignright" width=""]<img class="caption alignright size-full wp-image-2155" src="http://www.gamevista.de/wp-content/uploads/2010/08/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Bereits mehrere Male konnten Fans des Echtzeit-Strategiespiels <a href="http://twitter.com/UBISOFT" target="_blank">R.U.S.E.</a> den Mehrspielermodus im Rahmen einer Beta ausgiebig testen. Nun kündigte der Publisher <strong>Ubisoft </strong>die Veröffentlichung einer Demo-Version an. Laut einer <a href="http://twitter.com/UBISOFT" target="_blank">Twitter-Meldung</a> seitens Ubisoft wird sehr bald die Testversion von <a href="http://twitter.com/UBISOFT" target="_blank">R.U.S.E.</a> veröffentlicht.</p> '
featured_image: /wp-content/uploads/2010/08/ruse_small.jpg

---
Zum Umfang der Demo gab der Publisher leider keine weiteren Details bekannt.

 

   

* * *

   



 