---
title: Mass Effect 2 – Veröffentlichungstermin bekannt
author: gamevista
type: news
date: 2009-10-17T16:27:31+00:00
excerpt: '<p>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]Der Entwickler <a href="http://www.bioware.com" target="_blank">BioWare</a> hat für das Actionrollenspiel <strong>Mass Effect 2</strong> einen genauen Veröffentlichungstermin bekannt gegeben. Demnach wird der lang erwartete Nachfolger am 29. Januar in Europa in den Läden stehen. Dies teilte <a href="http://www.bioware.com" target="_blank">BioWare</a> offiziell mit einer Pressemitteilung mit.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
 

Weiterhin wurde bekannt das Vorbesteller beim Onlinedienst Gamestop die Terminusrüstung erhalten. Diese erhöht eure Laufgeschwindigkeit und Schilde. Außerdem wird es die Gravitationswaffe M90-Blackstorm geben. Wer sich Mass Effect 2 aber anderweitig vorbestellt erhält als Bonus die Inferno Rüstung. Diese Items gibt es aber nur auf dem amerikanischen Markt. Was unsereins in Europa bekommen wird ist noch unbekannt.

* * *



* * *

 