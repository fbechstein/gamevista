---
title: 'Star Wars: The Force Unleashed 2 – Demo erscheint noch vor Release'
author: gamevista
type: news
date: 2010-08-22T12:44:20+00:00
excerpt: '<p>[caption id="attachment_2148" align="alignright" width=""]<img class="caption alignright size-full wp-image-2148" src="http://www.gamevista.de/wp-content/uploads/2010/08/swu_small.png" border="0" alt="Star Wars: The Force Unleashed 2" title="Star Wars: The Force Unleashed 2" align="right" width="140" height="100" />Star Wars: The Force Unleashed 2[/caption]Publisher <strong>LucasArts </strong>hat bestätigt das noch vor dem Release des Actionspiels <a href="http://www.lucasarts.com/games/theforceunleashed2/" target="_blank">Star Wars: The Force Unleashed 2</a> eine Demo veröffentlicht wird. Der zweite Teil soll am 29. Oktober 2010 für Konsolen und PC in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2010/08/swu_small.png

---
Es ist wahrscheinlich, dass die Testversion die auch schon auf der gamescom 2010 zu sehen war ebenfalls ihren Weg auf die heimischen Systeme finden wird. Einen genauen Termin nannte der Publisher nicht.

   

* * *

   



 