---
title: Red Faction PC-Version mit einigen Boni
author: gamevista
type: news
date: 2009-09-03T17:50:44+00:00
excerpt: '<p>[caption id="attachment_597" align="alignright" width=""]<img class="caption alignright size-full wp-image-597" src="http://www.gamevista.de/wp-content/uploads/2009/09/logo.jpg" border="0" alt="Red Faction" title="Red Faction" align="right" width="140" height="100" />Red Faction[/caption]Schon lange ist <strong>Red Faction – Guerrilla</strong> für die Xbox360 und die PS3 erhältlich. Nur die PC-Spieler wurden wieder vertröstet und erhalten das Spiel erst Mitte November. Doch die PC-Version bietet mehr als die Konsolenversionen.</p> <p>Erst ab dem 11.November dürfen auch PC-Spieler dem Mars-Untergrund beitreten und ordentlich Feinde abballern. Die Entwickler scheinen sich nun entschuldigen zu wollen und so wird die PC-Version eine überarbeitete Grafik und größeren Umfang haben.</p> '
featured_image: /wp-content/uploads/2009/09/logo.jpg

---
Enthalten sind die ersten beiden Download-Pakete, das heißt die Singleplayer-Kampagne „Demons of the Badlands“ und zusätzliche Maps für den Multiplayermodus. Für beide Pakete müssen die Konsolenspieler einiges zahlen. Darüber hinaus werden auch noch zwei weitere Multiplayermodi enthalten sein.

* * *



* * *