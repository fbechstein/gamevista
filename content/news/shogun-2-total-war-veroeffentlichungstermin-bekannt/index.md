---
title: 'Shogun 2: Total War – Veröffentlichungstermin bekannt'
author: gamevista
type: news
date: 2010-11-11T18:21:46+00:00
excerpt: '<p>[caption id="attachment_1882" align="alignright" width=""]<img class="caption alignright size-full wp-image-1882" src="http://www.gamevista.de/wp-content/uploads/2010/06/shogun2.jpg" border="0" alt="Shogun 2: Total War" title="Shogun 2: Total War" align="right" width="140" height="100" />Shogun 2: Total War[/caption]Publisher <strong>SEGA </strong>hat den genauen Veröffentlichungstermin für das Strategiespiel <a href="http://www.totalwar.com/shogun2" target="_blank">Shogun 2: Total War</a> bekannt gegeben. Per Pressemitteilung wurde heute der 15. März 2011 genannt, bisher war nur vom Frühjahr 2011 die Rede. Der Titel führt den Spieler in das mittelalterliche Japan des 16. Jahrhunderts.</p> '
featured_image: /wp-content/uploads/2010/06/shogun2.jpg

---
Das japanische Reich ist unter einer Vielzahl von verfeindeten Kriegsherren aufgeteilt.Dabei übernimmt der Spieler die Rolle eines Daimyos, eines Clanführers der mit Hilfe militärischer, wirtschaftlicher und diplomatischer Mittel versucht das Reich zu einen.  Das Video zeigt zahlreiche Zwischensequenzen und erklärt die Hintergründe des Spiels.

 

 

   

* * *

   

