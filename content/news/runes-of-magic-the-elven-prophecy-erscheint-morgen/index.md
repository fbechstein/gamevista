---
title: Runes of Magic – Patch 2.0 The Elven Prophecy erscheint morgen
author: gamevista
type: news
date: 2009-09-14T08:51:57+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Wie Entwickler <a href="http://www.frogster-interactive.de">Frogster Entertainment</a> offiziell bekannt gegeben hat wird der neue Inhaltspatch Kapitel 2 <strong>The Elven Prophecy</strong> ab morgen, Dienstag den 15. September 2009 verfügbar sein.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Das Content Update wird unter anderem zwei neue Klassen, neue Gebiete und Instanzen enthalten. Weiterhin wird das Maximallevel auf 55 erhöht. Als Vorbereitung für den Patch werden heute um 12 Uhr die Server heruntergefahren um am Dienstag um 12 Uhr wieder ihre Pforten zu öffnen.  
Die komplette Liste der Änderungen könnt ihr [hier][1] finden.

* * *



* * *

 [1]: http://www.runesofmagic.com/de/chapter2.html