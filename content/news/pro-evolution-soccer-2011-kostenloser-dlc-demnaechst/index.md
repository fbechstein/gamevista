---
title: Pro Evolution Soccer 2011 – Kostenloser DLC demnächst
author: gamevista
type: news
date: 2010-12-17T21:02:25+00:00
excerpt: '<p>[caption id="attachment_1755" align="alignright" width=""]<img class="caption alignright size-full wp-image-1755" src="http://www.gamevista.de/wp-content/uploads/2010/05/pes2011.jpg" border="0" alt="Pro Evolution Soccer 2011" title="Pro Evolution Soccer 2011" align="right" width="140" height="49" />Pro Evolution Soccer 2011[/caption]Publisher <strong>Konami </strong>hat im Rahmen einer Pressemitteilung das nächste kostenlose Erweiterungspaket für das Fussballspiel <a href="http://www.pes2011.com" target="_blank">Pro Evolution Soccer 2011</a> angekündigt. Der Downloadable-Content soll bereits am 21. Dezember 2010 veröffentlicht werden.</p> '
featured_image: /wp-content/uploads/2010/05/pes2011.jpg

---
Das Update liefert diverse neue Trikots und weitere lizensierte Schuhe von Puma, Adidas, Umbro und Mizuno.

 

 

   

* * *

   

