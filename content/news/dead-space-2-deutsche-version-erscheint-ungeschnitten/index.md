---
title: Dead Space 2 – Deutsche Version erscheint ungeschnitten
author: gamevista
type: news
date: 2010-12-15T19:25:31+00:00
excerpt: '<p>[caption id="attachment_1151" align="alignright" width=""]<img class="caption alignright size-full wp-image-1151" src="http://www.gamevista.de/wp-content/uploads/2009/12/deadspace2.jpg" border="0" alt="Dead Space 2" title="Dead Space 2" align="right" width="140" height="100" />Dead Space 2[/caption]Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Pressemitteilung bekannt gegeben, dass die deutsche Version des Horror-Shooters <a href="http://www.deadspace.ea.com/" target="_blank">Dead Space 2</a> ungeschnitten und mit einer USK-Einstufung „Keine Jugendfreigabe“ veröffentlicht wird. Demnach wird der Titel ab dem 27. Januar 2011 für PC, PlayStation 3 und Xbox 360 nur für Käufer ab 18 Jahren erhältlich sein.</p> '
featured_image: /wp-content/uploads/2009/12/deadspace2.jpg

---
Im Mehrspielermodus wird als einzige Änderungen der Friendly Fire-Modus deaktiviert sein.

 

 

   

* * *

   

