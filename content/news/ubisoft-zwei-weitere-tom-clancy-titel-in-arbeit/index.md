---
title: Ubisoft – Zwei weitere Tom Clancy-Titel in Arbeit
author: gamevista
type: news
date: 2010-04-28T21:36:51+00:00
excerpt: '<p>[caption id="attachment_307" align="alignright" width=""]<img class="caption alignright size-full wp-image-307" src="http://www.gamevista.de/wp-content/uploads/2009/08/ubisoftlogo_small.jpg" border="0" alt="Ubisoft" title="Ubisoft" align="right" width="140" height="100" />Ubisoft[/caption]Seit kurzem ist der neueste Splinter Cell-Teil Conviction im Handel erhältlich. Grund genug das sich der Publisher <strong>Ubisoft </strong>über die Zukunftspläne der Tom Clancy-Spiele äußert. Klar dieses Jahr erscheint noch <strong>Ghost Recon: Future Soldier</strong>. Trotzdem arbeiten bereits Entwickler von Ubi an zwei neuen Tom Clancy Teilen.</p> '
featured_image: /wp-content/uploads/2009/08/ubisoftlogo_small.jpg

---
Dies gab Michel Verheijdt, Verantwortlicher für die Tom Clancy-Produktreihe, offiziell bekannt. Nähere Details gibt es bisher nicht. Denkbar wäre eine Vorsetzung der sehr erfolgreichen Rainbow Six-Reihe. Sobald neue Infos auftauchen geben wir euch bescheid.

 

 

   

* * *

   

