---
title: Take 2 – Verschiebungen
author: gamevista
type: news
date: 2009-07-14T15:20:33+00:00
excerpt: '<p>[caption id="attachment_47" align="alignright" width=""]<img class="caption alignright size-full wp-image-47" src="http://www.gamevista.de/wp-content/uploads/2009/07/bioshock2_small.png" border="0" alt="Bioshock 2 gehört zu den Top Titeln, die verschoben wurden" title="Bioshock 2 gehört zu den Top Titeln, die verschoben wurden" align="right" width="140" height="100" />Bioshock 2 gehört zu den Top Titeln, die verschoben wurden[/caption]Wie der Publisher <a href="http://www.take2.de/" target="_blank">Take2</a> jetzt bekanntgab, wird das Veröffentlichungsdatum einiger Spiele nach hinten geschoben. Von der Verschiebung betroffen sind <strong>Bioshock2, Mafia 2, Max Payne 3 und Red Dead Dedemption</strong>. Die vier Titel sollen erst im Geschäftsjahr 2010 erscheinen, welches am 01. November 2009 beginnt. </p>'
featured_image: /wp-content/uploads/2009/07/bioshock2_small.png

---
Die Verschiebung hängt laut Ben Feder, Geschäftsführer von Take2, mit der Qualitätssicherung der Spiele zusammen. 

&nbsp;





