---
title: Alpha Protocol – Infos zum Kopierschutz
author: gamevista
type: news
date: 2010-05-03T16:54:02+00:00
excerpt: '<p>[caption id="attachment_119" align="alignright" width=""]<img class="caption alignright size-full wp-image-119" src="http://www.gamevista.de/wp-content/uploads/2009/07/ap_small.jpg" border="0" alt="Alpha Protocol" title="Alpha Protocol" align="right" width="140" height="100" />Alpha Protocol[/caption]Der Publisher <strong>SEGA </strong>liefert neue Informationen zum Rollenspiel <a href="http://www.alphaprotocol.com" target="_blank">Alpha Protocol</a> das am 28. Mai 2010 im Handel erscheint. Demnach muss man das Spiel über das Internet aktivieren, allerdings wird keine permanente Internetverbindung wie beim Ubisoft-Kopierschutz vorausgesetzt. <a href="http://www.alphaprotocol.com" target="_blank">Alpha Protocol</a> kann man auf fünf komplett verschiedenen Systemen installieren.</p> '
featured_image: /wp-content/uploads/2009/07/ap_small.jpg

---
Die benutzten Lizenzen kann man bei der Deinstallation des Spiels wieder rückgängig machen. Weiterhin verspricht **SEGA** das in den kommenden zwei Jahren ein Patch nachgereicht wird, der die benötigte Internetaktivierung entfernt.

 

 

   

* * *

   

