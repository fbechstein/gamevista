---
title: Fifa 10 – Termin enthüllt
author: gamevista
type: news
date: 2009-08-05T19:55:24+00:00
excerpt: '<p>[caption id="attachment_115" align="alignright" width=""]<img class="caption alignright size-full wp-image-115" src="http://www.gamevista.de/wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg" border="0" alt="Fifa 10" title="Fifa 10" align="right" width="140" height="100" />Fifa 10[/caption]Wer so Fussballvernarrt ist wie ich fragt sich jedes Jahr aufs Neue ob es nun <a href="http://www.electronic-arts.de" target="_blank" title="ea">EA </a>oder <a href="http://www.de.games.konami-europe.com" target="_blank" title="konami">Konami </a>sein soll. Für <a href="http://www.electronic-arts.de" target="_blank" title="ea">Electronic Arts</a> <strong>Fifa 10</strong> wurde nun ein konkreter Release Termin enthüllt. </p>'
featured_image: /wp-content/uploads/2009/07/fifa10_ps3_xavi_small.jpg

---
Am 02. Oktober 2009 wird **Fifa 10** für die Nextgen Konsolen Playstation 3, Xbox 360 und Nintendo Wii erscheinen. Außerdem auch für PC, Nintendo DS und Playstation Portable. Weiterhin wird es natürlich auch was für die Handys geben. Auf der gamescom könnt ihr euch schon mal die Entscheidung etwas leichter machen. <a href="http://www.electronic-arts.de" target="_blank" title="ea">EA </a>oder <a href="http://www.de.games.konami-europe.com" target="_blank" title="konami">Konami </a>dieses Jahr?







 