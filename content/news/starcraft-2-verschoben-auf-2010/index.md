---
title: Starcraft 2 – Verschoben auf 2010
author: gamevista
type: news
date: 2009-08-06T18:21:42+00:00
excerpt: '<p>[caption id="attachment_299" align="alignright" width=""]<img class="caption alignright size-full wp-image-299" src="http://www.gamevista.de/wp-content/uploads/2009/08/starcraft2_small.jpg" border="0" alt="Starcraft 2" title="Starcraft 2" align="right" width="140" height="88" />Starcraft 2[/caption]Lange war es ein Gerücht doch nun ist es leider offiziell. <a href="http://www.eu.blizzard.com" target="_blank" title="blizzard">Blizzards </a>lang erwartetes Sequel <strong>Starcraft 2</strong> verschiebt sich auf das Jahr 2010. Als Grund wurde angegeben das <a href="http://www.eu.blizzard.com" target="_blank" title="blizzard">Blizzard </a>wohl Probleme hat mit der Entwicklung der Onlineplattform Battlenet. </p>'
featured_image: /wp-content/uploads/2009/08/starcraft2_small.jpg

---
“In den vergangenen Wochen ist klar geworden, dass es länger als erwartet dauert, dass neue Battle.net für den Start vorzubereiten. Das verbesserte Battle.net ist ein ein wichtiger Bestandteil der **Starcraft 2**-Erfahrung und aller folgenden Spiele. Die zusätzliche Entwicklungszeit ist immens wichtig, um unsere ursprünglichen Pläne für diesen Service umzusetzen.“

Demnach wird wahrscheinlich das der Betatest im Herbst starten wird da dieser ungefähr sechs Monate in Anspruch nehmen wird. 







 