---
title: Mafia 2 – Gratisaktion auf Steam
author: gamevista
type: news
date: 2010-07-22T14:59:30+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]Fans des Actionspiels <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> können ab sofort auf der Onlineplattform Steam den Titel vorbestellen. Als Anreiz erhält jeder Kunde den ersten Teil des Mafia-Epos gratis dazu. Dies gilt sowohl für die normale- sowie für die Digital Deluxe Edition für jeweils 49,99 Euro bzw. 59,99 Euro.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
<a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> erscheint am 27. August 2010 für PC, PlayStation 3 und Xbox 360.

 

 

   

* * *

   

