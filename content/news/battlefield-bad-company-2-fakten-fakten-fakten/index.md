---
title: 'Battlefield: Bad Company 2 – Fakten, Fakten, Fakten'
author: gamevista
type: news
date: 2009-08-20T14:47:36+00:00
excerpt: '<p />[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Publisher <a href="http://www.electronic-arts.de">Electronic Arts</a> und Entwickler <a href="http://www.dice.se">Dice </a>kündigten im Rahmen der gamescom in Köln weitere Details zum Multiplayer Modus von <strong>Battlefield: Bad Company 2</strong>. <br /> Es werden demnach vier Spielmodi geben, darunter Conquest und Rush. Desweiteren sind vier Charakterklassen geplant, darunter Assault, Recon, Medic und Engineer. <br /> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Laut EA werden bis zu 15.000 unterschiedliche Kombinationen der Ausrüstung möglich sein um euch individuell zu bewaffnen. Insgesamt gibt stehen 40 Waffen zur Auswahl, sicher wird es per Update zusätzliche geben. Insgesamt werden sie bis zu 15 Fahrzeuge Steuern dürfen. Ob Quad, APC oder Panzer, dürfen auf acht Multiplayer Karten zusammengeschossen werden. Auch hier wird per zusätzlichen Downloadpaketen Nachschub abgeworfen. Weitere Features sind:

  * 13 Spezialisierungen erlauben unter anderem, den Munitionsvorrat, die Ausdauer oder die Körperpanzerung zu erhöhen
  * 200 Anpassungsmöglichkeiten für Waffen verändern auf Wunsch deren Eigenschaften
  * 3 exklusive Waffen erhalten Spieler, die parallel Battlefield 1943 installiert haben
  * 15 Ausrüstungsgegenstände vom Defibrilator bis zum Bewegungsmelder enthält das Spiel
  * 50 Ränge können Spieler im Multiplayer-Modus erreichen
  * 40 Anstecknadeln, so genannte Pins, verleiht das Spiel für besondere Leistungen innerhalb einer Partie
  * 228 Sterne in den Stufen Bronze, Silber und Gold geben an, wie erfahren ein Spieler im Umgang mit bestimmten Waffen ist
  * 40 Insignien werden ausschließlich an besonders ausdauernde Spieler verliehen, zum Beispiel für das Heilen von 1.000 Verwundeten
  * 24 Spieler werden in den Konsolen-Fassungen (Xbox 360, Playstation 3) gegeneinander antreten. In der PC Fassung werden es 64 Spieler sein.

* * *



* * *

 

 