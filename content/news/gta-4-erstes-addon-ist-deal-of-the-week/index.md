---
title: GTA 4 – Erstes Addon ist „Deal of the week“
author: gamevista
type: news
date: 2009-08-25T10:08:30+00:00
excerpt: '<p>[caption id="attachment_488" align="alignright" width=""]<img class="caption alignright size-full wp-image-488" src="http://www.gamevista.de/wp-content/uploads/2009/08/gta4_small.jpg" border="0" alt="GTA IV" title="GTA IV" align="right" width="140" height="100" />GTA IV[/caption]Auf dem  <a href="http://www.xbox.com/de-DE/live/deal-of-the-week.htm">Xbox Live Markplatz</a> findet man heute das erste Addon zu <strong>GTA 4</strong> günstiger im Rahmen des „<a href="http://www.xbox.com/de-DE/live/deal-of-the-week.htm">Deal of the week</a>“. <strong>The Lost and Damned</strong> könnt ihr ab heute für 1200 Punkte, 15 Euro, anstatt den üblichen 1600 Microsoft Punkten erwerben. Wer die erste Erweiterung noch nicht besitzt kann sich innerhalb dieser Woche die Kaufentscheidung überlegen.</p> '
featured_image: /wp-content/uploads/2009/08/gta4_small.jpg

---
Wer noch warten möchte dem sei die Extra DVD Version von **GTA 4** ans Herz gelegt. Diese wird demnächst erscheinen und alle Erweiterungen enthalten. Bisher gibt es dazu aber noch keinen Veröffentlichungstermin.

* * *



* * *

 