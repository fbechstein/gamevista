---
title: 'Anno 1404: Venedig – Addon angekündigt'
author: gamevista
type: news
date: 2009-11-24T22:24:36+00:00
excerpt: '<p>[caption id="attachment_831" align="alignright" width=""]<img class="caption alignright size-full wp-image-831" src="http://www.gamevista.de/wp-content/uploads/2009/10/anno1404_small.png" border="0" alt="Anno 1404" title="Anno 1404" align="right" width="140" height="100" />Anno 1404[/caption]<a href="http://www.anno.de.ubi.com" target="_blank">Ubisoft</a> hat heute in einer offiziellen Pressemitteilung angekündigt das  für das Strategiespiel <strong>Anno 1404</strong> ein Addon geplant sei. So wird das Erweiterungspaket mit dem Namen <strong>Anno 1404: Venedig</strong> neue Inhalte bieten, unter anderem ein neuer Inseltyp im Venedig Stil. Außerdem werden neue Gebäude, Schiffe und Errungenschaften dem Addon beiliegen.</p> '
featured_image: /wp-content/uploads/2009/10/anno1404_small.png

---
Eine weitere sehr freudige Nachricht ist das **Anno 1404** endlich einen Mehrspieler Part bekommen wird. So könnt ihr nun mit Freunden zusammen die die Welt von Anno erforschen. Das Addon wird ab Februar 2010 im deutschen Handel erhältlich sein.

<span style="text-decoration: underline;"><strong>Hier die Neuerungen im Detail:</strong></span>  
****

  * **Die abendländische Spielumgebung wird nun um die venezianische Hafeninsel erweitert. Sie bietet neue Errungenschaften, Schiffe, Gebäude und verbessert bestehende Spielfeatures.**

  * Zwei neue Schiffstypen – die kleine, schnelle und wendige Handelskogge und die große, vielseitige Handelskogge, bestückt mit wehrhaften Kanonen. 
  * Spionage: Venezianische Geheimkabinette ermöglichen es, fremde Wohnhäuser zu infiltrieren und von dort aus Sabotageaktionen gegen Mitspieler durchzuführen.
  * Venezianische Ratsversammlung: Gegnerische Städte können nun auch nicht-militärisch erobert werden. Durch großzügige Goldspenden kann die Mehrheit im Stadtrat und letztendlich der Stadtschlüssel errungen werden. 
    **Ab sofort kann im Multiplayer-Modus gespielt werden:  
** </li> 
    
      * Bis zu 8 Spieler, On- und Offline
      * Freunde im Versus-Modus herausfordern oder gemeinsam im Koop-Modus mit bis zu vier Spielern auf derselben Seite agieren</ul> 
    
    <div>
    </div>
    
    <div>
      <ul>
        <li>
          <strong>Neue Herausforderungen und Aufträge:<br /></strong>
        </li>
        <li>
          Über 300 neue Aufträge mit zwei neuen Auftragstypen: Handelsrennen und Kapern
        </li>
        <li>
          60 neue Gegenstände, darunter die Zisterne für Norias, welche die Wasservorräte weiter erhöht
        </li>
        <li>
          Neue Inselwelten, von großen flachen Inseln mit riesiger Baufläche bis hin zu zerklüfteten, wilden und ungezähmten Eilanden. Mächtige Vulkaninseln mit schier unendlichen Ressourcenmengen, die der gigantische Vulkanausbruch aber auch mit großer Zerstörungskraft vernichten kann
        </li>
        <li>
          Zahlreiche neue Szenarien stellen die Spieler vor neue, ungewöhnliche Herausforderungen, die so weder im Endlosspiel noch in der Kampagne zu finden sind. Dies reicht vom Besiedeln einer Inselwelt, die nur aus Südinsel besteht bis hin zu Szenarien, in denen die Bewohner keinerlei Steuern zahlen
        </li>
        <li>
          Neue Medaillen, Erfolge und neue Spielerwappen
        </li>
      </ul>
    </div>
    
    
    
    
    
    
    
     