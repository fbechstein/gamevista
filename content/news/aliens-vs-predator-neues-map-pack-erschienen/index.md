---
title: Aliens vs. Predator – Swarm Map Pack erschienen
author: gamevista
type: news
date: 2010-03-20T13:13:17+00:00
excerpt: '<p>[caption id="attachment_439" align="alignright" width=""]<img class="caption alignright size-full wp-image-439" src="http://www.gamevista.de/wp-content/uploads/2009/08/aliensvspredator.jpg" border="0" alt="Aliens vs. Predator" title="Aliens vs. Predator" align="right" width="140" height="100" />Aliens vs. Predator[/caption]Für den Ego-Shooter <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> gibt es ab sofort  den ersten Download-Inhalt über die Onlineplattform <strong>Steam </strong>zu erwerben. Der Download-Content mit dem Namen <strong>Swarm Map Pac</strong>k umfasst vier neuen Karten für den Mehrspielermodus. Kostenpunkt sind 7,99 USD Haken an der Sache ist immer noch das Problem das <a href="http://www.sega.com/games/aliens-vs-predator/" target="_blank">Aliens vs. Predator</a> nicht in Deutschland veröffentlicht wurde.</p> '
featured_image: /wp-content/uploads/2009/08/aliensvspredator.jpg

---
Demnach steht der DLC nur im Ausland zum Herunterladen bereit. Ob und wann deutsche Spieler in den Genuss des Map Pack kommen werden ist noch unbekannt. Um einen Vorgeschmack auf den DLC zu liefern hat der Publisher **SEGA** einen Trailer veröffentlicht.

[> Zum Aliens vs. Predator &#8211; Swarm Map Pack Trailer][1]

 

<cite></cite>

   

* * *

   



 

 [1]: videos/item/root/aliens-vs-predator-swarm-map-pack