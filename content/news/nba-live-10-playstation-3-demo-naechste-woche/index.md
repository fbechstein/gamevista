---
title: NBA Live 10 – PlayStation 3 Demo nächste Woche
author: gamevista
type: news
date: 2009-09-12T20:42:32+00:00
excerpt: '<p>[caption id="attachment_335" align="alignright" width=""]<img class="caption alignright size-full wp-image-335" src="http://www.gamevista.de/wp-content/uploads/2009/08/nba10_small.jpg" border="0" alt="NBA Live 10" title="NBA Live 10" align="right" width="140" height="100" />NBA Live 10[/caption]Xbox 360 Besitzer durften ja dank der veröffentlichten Demo für das Basketballspiel <strong>NBA Live 10</strong> vom Entwickler<a href="http://www.nba-live.easports.com"> Electronic Arts</a> schon diverse Eindrücke sammeln. Damit auch PlayStation 3 Nutzer in den Genuss kommen dürfen steht schon nächste Woche die Demo für die Sony Konsole vor der Tür. Einen genauen Termin gibt es bisher aber noch nicht. Wir werden natürlich bescheid geben sobald die Demo veröffentlicht ist.</p> '
featured_image: /wp-content/uploads/2009/08/nba10_small.jpg

---
 

* * *



* * *