---
title: 'Batman: Arkham Asylum – Bei Steam im Preis 50% reduziert'
author: gamevista
type: news
date: 2010-06-09T19:35:59+00:00
excerpt: '<p>[caption id="attachment_1902" align="alignright" width=""]<img class="caption alignright size-full wp-image-1902" src="http://www.gamevista.de/wp-content/uploads/2010/06/batman_arkham_asylum_small.jpg" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Batman: Arkham Asylum[/caption]Das Actionspiel <a href="http://www.batmanarkhamasylum.com" target="_blank">Batman: Arkham Asylum</a> gibt es derzeit beim Onlineportal Steam zum halben Preis. Im Rahmen der Midweek Madness wird das Angebot bereits Donnerstagnachmittag enden. Man sollte sich also sputen, denn es handelt sich hierbei um die Game oft he Year-Edition.</p> '
featured_image: /wp-content/uploads/2010/06/batman_arkham_asylum_small.jpg

---
Diese enthält neben dem Hauptspiel vier weitere Challenge Karten. Als schnell ab auf die [Angebotsseite][1] von Steam und zum Preis von 24,99 Euro zugreifen.

 

   

* * *

   



 

 [1]: http://store.steampowered.com/app/35140/