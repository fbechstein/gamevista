---
title: Die Sims 3 – Erstes Addon heißt World Adventures
author: gamevista
type: news
date: 2009-08-03T20:32:06+00:00
excerpt: '<p>[caption id="attachment_204" align="alignright" width=""]<img class="caption alignright size-full wp-image-204" src="http://www.gamevista.de/wp-content/uploads/2009/07/sims_small.png" border="0" alt="Die Sims 3" title="Die Sims 3" align="right" width="140" height="100" />Die Sims 3[/caption]Wie <a href="http://www.de.thesims3.com" target="_blank" title="ea maxis">EA Maxis</a> in einer Pressemitteilung berichteten wird <strong>Die Sims 3</strong> wie nicht anders erwartet ein Addon bekommen. Seit Juli kursierten Gerüchte ob und wann denn das erste Addon für die Lebenssimulation kommt. </p>'
featured_image: /wp-content/uploads/2009/07/sims_small.png

---
Das Erweiterungspaket wird auf den Namen **World Adventures** hören und dem Spieler die Möglichkeit geben seine Sims auf Weltreise zu schicken. Dabei können sie berühmte Wahrzeichen in China oder Ägypten besuchen. Außerdem werden neue Herausforderungen, persönliche Lebensziele und Sims-Fähigkeiten hinzugefügt. Der Veröffentlichungstermin für **Die Sims 3: World Adventures** wurde auf den 19. November 2009 gelegt.[  
][1] 







 

 [1]: index.php?Itemid=111&option=com_zoo&view=item&category_id=0&item_id=139