---
title: Killing Floor – Kostenlose Zusatzinhalte und DLC veröffentlicht
author: gamevista
type: news
date: 2010-10-08T18:15:18+00:00
excerpt: '<p>[caption id="attachment_2321" align="alignright" width=""]<img class="caption alignright size-full wp-image-2321" src="http://www.gamevista.de/wp-content/uploads/2010/10/killing_floor.jpg" border="0" alt="Killing Floor " title="Killing Floor " align="right" width="140" height="100" />Killing Floor [/caption]Der Koop-Zombie-Shooter <a href="http://www.killingfloorthegame.com" target="_blank">Killing Floor</a> hat neue Inhalte über das Onlineportal Steam erhalten. Der Download mit dem Namen <strong>Incendiary Content Pack</strong> erhält neben vier neuen Karten, einer neuen Waffe auch ein weiteres System im Spiel Geld zu verdienen.</p> '
featured_image: /wp-content/uploads/2010/10/killing_floor.jpg

---
Dazu wurden neue Verbesserungen am Balancing hinzugefügt und einige Fehler behoben. Neben dem kostenlosen Patch ist ab sofort ein kostenpflichtiger Downloadable-Content mit dem Namen **Post Mortem Charackter Pack** erhältlich. Dieser enthält vier neue Charaktere und kostet 74 Cent.

 

   

* * *

   

