---
title: 'Ubisoft – Splinter Cell: Conviction und R.U.S.E. verschoben'
author: gamevista
type: news
date: 2010-01-13T19:21:14+00:00
excerpt: '<p>[caption id="attachment_1306" align="alignright" width=""]<img class="caption alignright size-full wp-image-1306" src="http://www.gamevista.de/wp-content/uploads/2010/01/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Der Publisher <a href="http://www.rusegame.com" target="_blank">Ubisoft</a> hat am Abend im Rahmen einer Pressemitteilung bekannt gegeben, dass der Taktik-Shooter <strong>Splinter Cell: Conviction</strong> und das Strategiespiel R.U.S.E. nicht mehr dieses Quartal erscheinen würden. Für <strong>Splinter Cell: Conviction</strong> wurde nun als Termin der April 2010 genannt.</p> '
featured_image: /wp-content/uploads/2010/01/ruse_small.jpg

---
Für das sehr viel versprechende Strategiespiel **R.U.S.E.** wurde noch kein genauer Termin angegeben. Warum <a href="http://www.rusegame.com" target="_blank">Ubisoft</a> die beiden Spiele verschieben musste wurde nicht gesagt.

 

 

<cite></cite>

* * *



* * *

 