---
title: 'Drakensang: Am Fluss der Zeit – Goldstatus'
author: gamevista
type: news
date: 2010-01-20T22:05:52+00:00
excerpt: '<p>[caption id="attachment_437" align="alignright" width=""]<img class="caption alignright size-full wp-image-437" src="http://www.gamevista.de/wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg" border="0" alt="Drakensang: Am Fluss der Zeit" title="Drakensang: Am Fluss der Zeit" align="right" width="140" height="100" />Drakensang: Am Fluss der Zeit[/caption]Der <strong>Publisher dtp Entertainment</strong>, zusammen mit dem <strong>Entwickler Radon Labs</strong>, bestätigten den Veröffentlichungstermin für das Rollenspiel <a href="http://www.am-fluss-der-zeit.de/main.html" target="_blank">Drakensang: Am Fluss der Zeit</a> mit einer Gold-Meldung. Demnach befindet sich die Master Kopie vom Spiel auf dem Weg in die Presswerke, um genügend DVD`s für alle Fans des Rollenspiels zu produzieren.</p> '
featured_image: /wp-content/uploads/2009/08/drakensang_amflussderzeit.jpg

---
So dürfte dem Veröffentlichungstermin am **19. Februar 2010** nichts mehr im Wege stehen.

<a href="http://www.am-fluss-der-zeit.de/main.html" target="_blank">Drakensang: Am Fluss der Zeit</a> wird in zwei Versionen verkauft. Zum einen wird es die Standard Edition geben, sowie Collector\`s Edition mit zusätzlichen Inhalten. Der zweite Teil bildet den Prolog zum letzten Drakensang-Spiel und führt den Rollenspiel-Fan 23 Jahre vor die Ereignisse.

* * *



* * *