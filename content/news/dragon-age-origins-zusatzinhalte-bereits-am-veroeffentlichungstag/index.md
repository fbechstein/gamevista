---
title: 'Dragon Age: Origins – Zusatzinhalte bereits am Veröffentlichungstag'
author: gamevista
type: news
date: 2009-10-08T14:40:34+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Wie der Entwickler <a href="http://www.bioware.com" target="_blank">BioWare </a>offiziell bestätigte wird es für das Rollenspiel <strong>Dragon Age: Origins</strong> bereits am Verkaufstag den ersten Zusatzinhalt zum Kauf geben. Dieser wird gegen eine Gebühr von 5 Euro für PlayStation 3 und PC Besitzer eine Quest, sechs neue Fähigkeiten und exklusive Gegenstände hinzufügen.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Xbox 360 Besitzer kostet der Spaß ganze 560 Microsoft Punkte. **Dragon Age: Origins** erscheint am 5. November 2009 in Deutschland.

\*Update\*

Es wurde auch ein neuer Trailer zum Downloadinhalt veröffentlicht. Wir wünschen viel Spaß beim anschauen.

[> Zum Dragon Age: Origins &#8211; Wardens Keep Trailer][1]

* * *



* * *

 

 [1]: videos/item/root/dragon-age-origins-wardens-keep-trailer