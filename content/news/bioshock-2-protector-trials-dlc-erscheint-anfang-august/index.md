---
title: Bioshock 2 – Protector Trials DLC erscheint Anfang August
author: gamevista
type: news
date: 2010-07-01T17:05:19+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Entwickler <strong>2K Marin</strong> kündigte heute den nächsten Zusatzinhalt für den Ego-Shooter <a href="http://www.bioshock2game.com/" target="_blank">Bioshock 2</a> an. Der <strong>Protector Trials</strong> DLC wird am 3. August veröffentlicht und umfasst mehrere Einzelspielder-Inhalte. So wird man als Big Daddy in sechs neuen Level für das Wohl einer Little Sister gegen unzählige Gegner sorgen.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
Außerdem wird es sieben weitere Archievements und drei neue Schwierigkeitsgrade geben. Kostenpunkt sind 400 MS-Punkte bzw. 4,99 Dollar.

 

   

* * *

   

