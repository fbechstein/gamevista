---
title: Tomb Raider – Erste Screenshots und Artworks veröffentlicht
author: gamevista
type: news
date: 2011-01-11T18:09:55+00:00
excerpt: '<p>[caption id="attachment_2502" align="alignright" width=""]<img class="caption alignright size-full wp-image-2502" src="http://www.gamevista.de/wp-content/uploads/2011/01/tombraider.jpg" border="0" alt="Tomb Raider" title="Tomb Raider" align="right" width="140" height="100" />Tomb Raider[/caption]Publisher <strong>Square Enix</strong> präsentierte heute die ersten Screenshots zum sich momentan in der Entwicklung befindenden Actionspiel <a href="http://www.tombraider.com/" target="_blank">Tomb Raider</a>.  Das neue Abenteuer rund um die Heldin Lara Croft soll in diesem Teil realistischer und weitaus düsterer werden.</p> '
featured_image: /wp-content/uploads/2011/01/tombraider.jpg

---
Um euch weitere Eindrücke über die Atmosphäre des Spiels zu geben packte der Publisher noch ein paar Artworks in die Pressemitteilung.

 

[> Zu den Tomb Raider Screenshots][1]

   

* * *

   



 [1]: screenshots/item/root/tomb-raider