---
title: Lost Planet 2 – Capcom präsentiert die Lady Pirates
author: gamevista
type: news
date: 2010-04-26T16:53:39+00:00
excerpt: '<p>[caption id="attachment_1380" align="alignright" width=""]<img class="caption alignright size-full wp-image-1380" src="http://www.gamevista.de/wp-content/uploads/2010/01/lp2.jpg" border="0" alt="Lost Planet 2" title="Lost Planet 2" align="right" width="140" height="100" />Lost Planet 2[/caption]Der Entwickler <strong>Capcom </strong>veröffentlichte heute einen neuen Trailer zum Actionspiel <a href="http://www.lostplanet-thegame.com" target="_blank">Lost Planet 2</a>. Das Video mit dem Namen Lady Pirates lässt es ordentlich Krachen und zeigt viele neue Ingame-Szenen in denen diverse Damen gegen Roboter und Akrid-Aliens kämpfen.</p> '
featured_image: /wp-content/uploads/2010/01/lp2.jpg

---
In <a href="http://www.lostplanet-thegame.com" target="_blank">Lost Planet 2</a> wird die Geschichte rund um den ehemals mit Eis bedeckten Planeten E.D.N. III fortgesetzt. Dieser wurde durch Umweltverschmutzung und Terraforming in eine erdähnliche Welt verwandelt. Der zweite Teil von Lost Planet erscheint im Mai 2010 für Xbox 360 und PlayStation 3. Die PC Version wird im Herbst in den Handel kommen.

[> Zum Lost Planet 2 &#8211; Lady Pirates Trailer][1]

   

* * *

* * *

 __

 

 [1]: videos/item/root/lost-planet-2-lady-pirates-trailer