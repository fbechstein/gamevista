---
title: 'Mafia 2: Jimmy`s Vendetta – Neuer DLC erhält Veröffentlichungstermin'
author: gamevista
type: news
date: 2010-08-31T17:56:45+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]Publisher<strong> 2K Games</strong> hat heute den Veröffentlichungstermin für den ersten Zusatzinhalt des Actionspiels <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> bekannt gegeben. Der DLC mit dem Namen <strong>Jimmy`s Vendetta</strong> wird demnach am 7. September 2010 für PC, Playstation 3 und Xbox 360 erscheinen.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Der Kostenpunkt wird bei 800 Microsoft-Punkten bzw. ungefähr 10 Euro liegen. Als Inhalt gibt der Publisher den neuen Charakter Jimmy an. Dieser erledigt vor allem die Drecksarbeit der Mafia in über 30 neuen Missionen.

 

   

* * *

   

