---
title: Aion – Beta / Preorder Keys Verlosung
author: gamevista
type: news
date: 2009-07-21T14:49:52+00:00
excerpt: '<p>[caption id="attachment_91" align="alignright" width=""]<img class="caption alignright size-full wp-image-91" src="http://www.gamevista.de/wp-content/uploads/2009/07/aion_small.png" border="0" alt="Aion - Wir verlosen 25x Beta / Preorder Keys" title="Aion - Wir verlosen 25x Beta / Preorder Keys" align="right" width="140" height="100" />Aion - Wir verlosen 25x Beta / Preorder Keys[/caption]Wie bereits letzte Woche angekündigt, verlosen wir <strong>25x Aion - The Tower of Eternity Beta / Preorder Keys</strong> für die nächsten Beta Events am 31. Juli 2009, 14. August 2009 und folgenden. Alles was Ihr dafür machen müsst ist sich in der <a href="community/registers">gamevista.de Community anzumelden</a> und uns im <a href="forum/mmo/aion-beta-key-verlosung">folgendem Thread mitzuteilen</a> warum genau Ihr an den Beta Events teilnehmen wollt.</p>'
featured_image: /wp-content/uploads/2009/07/aion_small.png

---
Mit dem Beta Key habt Ihr <span>Zugriff auf alle kommenden Aion Beta-Events, sowie weiteren Gimmicks:</span> 

  * <div class="MsoNormal" align="left">
      <span>Teilnahme an allen Aion Beta Wochenenden</span>
    </div>

  * <div class="MsoNormal" align="left">
      <span>Zugriff auf Server und Charakter Vorauswahl</span>
    </div>

<p class="MsoNormal" align="left">
  <span>Exklusiv sind folgende virtuelle Items enthalten<strong>:</strong></span>
</p>

  * <div class="MsoNormal" align="left">
      <span>Talisman des Lodas (pro Stunde 20% Erfahrung extra)</span>
    </div>

  * <div class="MsoNormal" align="left">
      <span>Dunkelwolkenhut (steigert Attribute)</span>
    </div>

  * <div class="MsoNormal" align="left">
      <span>Ancient Ring (Elementar Effekt)</span>
    </div>

<span>Die nächsten Events sind am: </span><span></p> 

<ul>
  <li>
    <span class="beta-title">Geschlossenes Beta-Event 5 31. Juli – 3. August Schwerpunkt: Folgt später   </span>
  </li>
  <li>
    <span class="beta-title">Geschlossenes Beta-Event 6</span> <span class="beta-date">14. August – 17. August</span> <span class="beta-desc">Schwerpunkt: Folgt später</span>
  </li>
</ul>

<p>
  Einsendeschluss ist der 30. Juli 2009 um 18.00 Uhr. Die Gewinner werden wir per PM unterrichten.
</p>

<p>
  Und nun Viel Glück!
</p>

<p>
  <a href="community/registers">> zum Registrieren im Community Bereich</a><br /><a href="forum/mmo/aion-beta-key-verlosung">> zum Aion-Gewinnspiel Forum Thread</a>
</p>



<p>
  
</p>


</span>