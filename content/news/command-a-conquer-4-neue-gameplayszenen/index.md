---
title: 'Command & Conquer 4 – Neue Gameplayszenen'
author: gamevista
type: news
date: 2009-11-17T19:55:49+00:00
excerpt: '<p>[caption id="attachment_1050" align="alignright" width=""]<img class="caption alignright size-full wp-image-1050" src="http://www.gamevista.de/wp-content/uploads/2009/11/cc4_small.png" border="0" alt="Command & Conquer 4" title="Command & Conquer 4" align="right" width="140" height="100" />Command & Conquer 4[/caption]Der Publisher <a href="http://www.commandandconquer.com" target="_blank">Electronic Arts</a> möchte den Fans des neusten Teils von<strong> Command & Conquer</strong> mal wieder die Wartezeit etwas verkürzen. So wurde heute ein neuer Trailer zum Strategiespiel veröffentlicht. Mit jeder Menge Spielszenen und Zwischensequenzen lebt der Konflikt zwischen der Nod und GDI erneut auf. <strong>Command & Conquer 4</strong> erscheint am 18. März 2010 im deutschen Handel.</p> '
featured_image: /wp-content/uploads/2009/11/cc4_small.png

---
<a href="videos/item/root/command-a-conquer-4-gameplay-trailer" target="_blank">> Zum Command & Conquer 4 &#8211; Gameplay Trailer</a>

* * *



* * *

 