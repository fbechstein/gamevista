---
title: 'Need for Speed: Shift – Neue Demo mit Falken Tire Porsche GT3 RSR'
author: gamevista
type: news
date: 2009-12-17T18:25:06+00:00
excerpt: '<p>[caption id="attachment_124" align="alignright" width=""]<img class="caption alignright size-full wp-image-124" src="http://www.gamevista.de/wp-content/uploads/2009/07/shift_small.jpg" border="0" alt="Need for Speed: Shift" title="Need for Speed: Shift" align="right" width="140" height="100" />Need for Speed: Shift[/caption]Zwar ist <strong>Need for Speed: Shift</strong> schon seit September erhältlich und eine erste Demo gab es auch schon. Trotzdem möchte der Publisher <a href="http://www.shift.needforspeed.com/de" target="_blank">Electronic Arts</a> den noch unentschlossenen Spielern, die sich überlegen ob <strong>Need for Speed: Shift</strong> das richtige Spiel zu Weihnachten ist, eine weitere neue Demo anbieten.</p> '
featured_image: /wp-content/uploads/2009/07/shift_small.jpg

---
Neu hierbei ist das die Demo den Falken Tire Porsche GT3 RSR zur Probefahrt bereitstellt. Zwar ist diese mit der alten Testversion weitestgehend identisch, wer aber die erste Demo verpasst hat sollte einen Blick riskieren. Auf zwei Strecken mit fünf weiteren Wagen geht es dann ums Ganze.

[> Zum Need for Speed: Shift &#8211; Falken Demo Download][1]

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/need-for-speed-shift-falken-demo