---
title: Crysis 2 – Entwickler zeigen Making of-Reihe zum Ego-Shooter
author: gamevista
type: news
date: 2010-07-05T20:49:08+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Entwickler <strong>Crytek </strong>präsentierte heute das erste Making of Video zum kommenden Shooter-Hit <a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a>. Der Trailer zeigt nicht nur die Studios der Entwickler sondern hält noch zahlreiche Spielszenen für die Fans von <a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a> parat. Der zweite Teil erscheint für Xbox 360, PlayStation 3 und PC voraussichtlich im vierten Quartal 2010.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
[> Zum Crysis 2 &#8211; Entwicklertagebuch #1][1]

 

 

   

* * *

   



 [1]: videos/item/root/crysis-2--making-of-video-1