---
title: 'Wings of Prey: Wings of Luftwaffe – Zusatzinhalt ab sofort erhältlich'
author: gamevista
type: news
date: 2010-04-09T07:13:46+00:00
excerpt: '<p>[caption id="attachment_1253" align="alignright" width=""]<img class="caption alignright size-full wp-image-1253" src="http://www.gamevista.de/wp-content/uploads/2010/01/wop_small.png" border="0" alt="Wings of Prey" title="Wings of Prey" align="right" width="140" height="100" />Wings of Prey[/caption]Für den Nachfolger der IL-2 Sturmovik –Serie, <a href="http://www.airwargame.com" target="_blank">Wings of Prey</a>,ist ab sofort eine neue Download-Erweiterung verfügbar. Mit Wings of Luftwaffe wird für das Flugspiel 10 neue Missionen, die sie allein oder mit bis zu 3 Freunden im Koop-Modus bestreiten können, eingeführt. Neben zwei neuen Flugzeugtypen können sie nun auch auf der Seite der Deutschen fliegen.</p> '
featured_image: /wp-content/uploads/2010/01/wop_small.png

---
Kostenpunkt für den Download-Content sind 15 US Dollar. Diesen können sie auf der Webseite <a href="http://yuplay.com/story.php?title=Wings-Prey--Wings-Luftwaffe-1" target="_blank">www.yuplay.com</a> erwerben.

 

 

<cite></cite>

   

* * *

   

