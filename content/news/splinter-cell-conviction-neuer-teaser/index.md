---
title: 'Splinter Cell: Conviction – Neuer Teaser'
author: gamevista
type: news
date: 2009-12-27T12:40:44+00:00
excerpt: '<p>[caption id="attachment_142" align="alignright" width=""]<img class="caption alignright size-full wp-image-142" src="http://www.gamevista.de/wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg" border="0" alt="Splinter Cell: Conviction" title="Splinter Cell: Conviction" align="right" width="140" height="100" />Splinter Cell: Conviction[/caption]Zum neusten Actiontitel<strong> Splinter Cell: Conviction</strong>, vom Publisher Ubisoft, wurde ein neues kurzes Teaservideo veröffentlicht. Mit dem Namen <strong>Code Red</strong> wird ein noch unbekannter Mann mit einem rot leuchtenden Nachtsichtgerät gezeigt. Weiterhin wurde eine Website im Trailer verlinkt die auf <a href="http://codeofconviction.com/" target="_blank">codeofconviction.com</a> zeigt.</p> '
featured_image: /wp-content/uploads/2009/07/splinter_cell_conviction_small.jpg

---
[> Zum Splinter Cell: Conviction &#8211; Teaser][1]

 

 

<cite></cite>

* * *



* * *

 

 [1]: videos/item/root/splinter-cell-conviction-teaser