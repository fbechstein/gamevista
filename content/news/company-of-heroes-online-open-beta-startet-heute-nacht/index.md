---
title: Company of Heroes Online – Open Beta startet heute Nacht
author: gamevista
type: news
date: 2010-09-01T15:04:01+00:00
excerpt: '<p>[caption id="attachment_2185" align="alignright" width=""]<img class="caption alignright size-full wp-image-2185" src="http://www.gamevista.de/wp-content/uploads/2010/08/coh.jpg" border="0" alt="Company of Heroes Online" title="Company of Heroes Online" align="right" width="140" height="100" />Company of Heroes Online[/caption]Entwickler <strong>Relic </strong>führt heute größere Arbeiten an den Server von <a href="http://www.companyofheroes.com/" target="_blank">Company of Heroes Online</a> durch. Damit ist die geschlossene Betaphase offiziell beendet und die erspielten Erfolge werden gelöscht. Ab heute Nacht 04:00 Uhr werden die Server, soweit alles glatt läuft, wieder Online gehen und die offene Betaphase wird eingeläutet.</p> '
featured_image: /wp-content/uploads/2010/08/coh.jpg

---
Damit steht das Strategiespiel für alle Interessenten zum ausführlichen Test zur Verfügung. Die Accounts Closed Beta-Spieler werden nach wie vor funktionieren. Wie lange die offene Betaphase laufen wird ist bisher nicht bekannt. Auch wurde der Veröffentlichungstermin bisher nicht verkündet.

 

   

* * *

   

