---
title: Mass Effect 2 – Demo steht zum Download bereit
author: gamevista
type: news
date: 2010-06-14T16:15:47+00:00
excerpt: '<p><strong>[caption id="attachment_401" align="alignright" width=""]<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mass Effect 2[/caption]EA </strong>und <strong>BioWare </strong>haben endlich die Demo zum Ego-Shooter Highlight <a href="http://www.masseffect.bioware.com/" target="_blank">Mass Effect 2</a> veröffentlicht. Ab sofort steht die Demo auf Xbox Live und für PC bei uns zum Herunterladen bereit. Neben zwei Missionen aus dem Hauptspiel, gibt es auch noch eine Bonus-Mission zu entdecken.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
Dabei habt ihr bis zu 90 Minuten Zeit die Welt des zweiten Teils mit Commander Shepard zu erkunden.

Eine weitere Meldung gibt es noch. Ab morgen, den 15. Juni, wird das neue Erweiterungspaket mit dem Namen Overlord freigeschaltet. Für 560 Microsoft-Punkte bzw. 560 BioWare-Punkte ist der Download Content ab Dienstag verfügbar. Neben fünf neuen Gebieten gibt es auch noch zwei neue Erfolge freizuspielen. 

[> Zur Mass Effect 2 &#8211; Demo][1]

   

* * *

   



 

 [1]: downloads/demos/item/demos/mass-effect-2-demo