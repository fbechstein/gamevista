---
title: God of War 3 – Ultimate Edition angekündigt
author: gamevista
type: news
date: 2009-11-02T09:24:17+00:00
excerpt: '<p>[caption id="attachment_981" align="alignright" width=""]<img class="caption alignright size-full wp-image-981" src="http://www.gamevista.de/wp-content/uploads/2009/11/gow.jpg" border="0" alt="God of War III" title="God of War III" align="right" width="140" height="100" />God of War III[/caption]Der Publisher <a href="http://www.station.sony.com" target="_blank">Sony</a> hat bekannt gegeben das zum kommenden Actiontitel <strong>God of War 3</strong> es eine <strong>Ultimate Edition</strong> geben wird. Diese wird wird im März 2010 erscheinen und hat folgende Zusatzinhalte:</p> '
featured_image: /wp-content/uploads/2009/11/gow.jpg

---
 

  * God of War 3 Spiel
  * Artbook
  * Die Video-Dokumentation &#8222;Unearthing the Legend Director&#8217;s Cut&#8220;
  * Musik-Download &#8222;God of War Trilogy Soundtrack&#8220; plus &#8222;God of War Heavy Metal EP&#8220;
  * Zugang zur &#8222;God of War Combat Arena&#8220; (enthält eine exklusive Spielumgebung mit sieben zusätzlichen Herausforderungen)
  * Kratos Skin &#8222;Dominus&#8220;

Kosten wird die **Ultimate Edition** 99,99 $ und die zusätzlichen Inhalte werden allesamt über das PlayStation Network verfügbar sein.

* * *



* * *