---
title: Duke Nukem Forever – Demo-Key in der GOTY von Borderlands enthalten
author: gamevista
type: news
date: 2010-10-05T20:06:42+00:00
excerpt: '<p>[caption id="attachment_2299" align="alignright" width=""]<img class="caption alignright size-full wp-image-2299" src="http://www.gamevista.de/wp-content/uploads/2010/10/dnf.jpg" border="0" alt="Duke Nukem Forever" title="Duke Nukem Forever" align="right" width="140" height="100" />Duke Nukem Forever[/caption]So langsam nimmt der Duke Gestalt an. <strong>2K Games</strong> hat offiziell angekündigt, der Game of the Year Edition von <strong>Borderlands</strong>, einen exklusiven Code für die erste Demo von <a href="http://www.dukenukem.com" target="_blank">Duke Nukem Forever </a>beizulegen. Der Key wird außerdem eine Vielzahl an Extras enthalten. Bisher gibt es zu den Bonis aber keine Aussage was genau damit gemeint ist.</p> '
featured_image: /wp-content/uploads/2010/10/dnf.jpg

---
Auch ist noch unklar wann die Demo veröffentlicht werden wird. Die **Borderlands** Game of the Year Edition wird am 12. Oktober 2010 für PC, PlayStation 3 und Xbox 360 inklusive aller vier DLC\`s in den Handel kommen.

 

   

* * *

   

