---
title: Crysis 2 – Crytek gibt Veröffentlichungstermin bekannt
author: gamevista
type: news
date: 2010-08-14T13:36:07+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Lange wurde spekuliert wann der zweite Teil vom Ego-Shooter <strong>Crysis </strong>in den Handel kommt. Nun hat der Entwickler <a href="http://www.ea.com/games/crysis-2" target="_blank">Crytek</a> über den Online-NachrichtendienstTwitter die Meldung zum offiziellen Release-Termin veröffentlicht.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
Am 25. März 2011 wird der Spieler in den Straßen von New York mit Nanosuit und Hightech-Waffen die Welt retten.

 

   

* * *

   

