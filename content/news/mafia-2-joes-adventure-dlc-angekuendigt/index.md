---
title: Mafia 2 – Joe`s Adventure DLC angekündigt
author: gamevista
type: news
date: 2010-09-16T18:00:19+00:00
excerpt: '<p>[caption id="attachment_429" align="alignright" width=""]<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" />Mafia 2[/caption]Für das Actionspiel <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> wurde der nächste Zusatzinhalt angekündigt. In Joe`s Adventure wird es um den Charakter Joe Barbaro gehen. Dieser ist ein enger Freund von Vito Scaletta. Als neue Inhalte zählen unter anderem neue Gebiete und Objekte. Darunter sind eine Werft, ein Bordell, ein Bahnhof und ein Seeufer in der Metropole Empire Bay.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Das Addon wird auch weitere optionale Nebenquests bieten. Preis und einen Veröffentlichungstermin nannte der Publisher **2K Games** nicht.

 

  

* * *

   

