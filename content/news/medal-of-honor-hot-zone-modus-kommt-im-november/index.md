---
title: Medal of Honor – Hot Zone-Modus kommt im November
author: gamevista
type: news
date: 2010-10-27T17:23:34+00:00
excerpt: '<p>[caption id="attachment_2386" align="alignright" width=""]<img class="caption alignright size-full wp-image-2386" src="http://www.gamevista.de/wp-content/uploads/2010/10/moh2010.jpg" border="0" alt="Medal of Honor " title="Medal of Honor " align="right" width="140" height="100" />Medal of Honor [/caption]<strong>Electronic Arts</strong> hat bestätigt, dass am 2. November 2010 nicht nur der <strong>Clean Sweep DLC</strong> veröffentlicht wird, sondern auch ein weiterer Mehrspieler-Modus mit dem Namen <strong>Hot Zone </strong>erscheint. <strong>Hot Zone</strong> wird faktisch ein King of the Hill Gameplay besitzen, zwei Teams kämpfen um einen festgelegten Ort auf der Karte.</p> '
featured_image: /wp-content/uploads/2010/10/moh2010.jpg

---
Der neue Spielmodus wird für die beiden neuen Karten, Hindukush Pass und Korengal Outpost, sowie für die überarbeiteten Karten Shahikot Valley und Helmand Valley verfügbar sein. Hot Zone wird veröffentlicht für PC, PlayStation 3 und Xbox 360. Der Preis wird bei 9,99 USD bzw. 800 Microsoft Punkte liegen.

 

   

* * *

   

