---
title: 'Need for Speed: Hot Pursuit – Erster DLC verfügbar'
author: gamevista
type: news
date: 2010-11-22T17:27:43+00:00
excerpt: '<p>[caption id="attachment_2381" align="alignright" width=""]<img class="caption alignright size-full wp-image-2381" src="http://www.gamevista.de/wp-content/uploads/2010/10/nfshp.jpg" border="0" alt="Need for Speed: Hot Pursuit" title="Need for Speed: Hot Pursuit" align="right" width="140" height="100" />Need for Speed: Hot Pursuit[/caption]Nachdem die Veröffentlichung des Rennspiels <a href="http://www.hotpursuit.needforspeed.com" target="_blank">Need for Speed: Hot Pursuit</a> erst ein paar Tage her ist, liefert der Publisher Electronic Arts gleich den ersten Download-Inhalt für das Spiel nach. Beide Pakete liefern zwar keine neuen Inhalte, sparen euch aber Zeit bei der Freischaltung der einzelnen Wagen.</p> '
featured_image: /wp-content/uploads/2010/10/nfshp.jpg

---
Für 320 MS-Punkte per Xbox Live, bzw. 3,99 USD über das PlayStation Network, könnt ihr sofort mit allen Wagen starten.

 

   

* * *

   

