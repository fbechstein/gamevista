---
title: Left 4 Dead – Neuer Inhalt kommt am Donnerstag
author: gamevista
type: news
date: 2009-09-06T17:11:33+00:00
excerpt: '<p>[caption id="attachment_630" align="alignright" width=""]<a href="http://www.valvesoftware.com"><img class="caption alignright size-full wp-image-630" src="http://www.gamevista.de/wp-content/uploads/2009/09/l4d_small.jpg" border="0" alt="Left 4 Dead" title="Left 4 Dead" align="right" width="140" height="100" />Valve</a>Left 4 Dead[/caption], Entwickler des Horror Survival Shooters<strong> Left 4 Dead</strong>, haben über ihren offiziellen <a href="http://twitter.com/ValveNews/status/3794806521">Twitter Account</a> bekannt gegeben das am kommenden <strong>Donnerstag</strong> den <strong>10.09. 2009</strong> ein weiteres Download Content Paket für die Fans bereit steht. Unter anderem werden neue Gebiete, neue Dialoge der Original Charaktere und ein „explosives Finale“ in das Spiel integriert.</p> '
featured_image: /wp-content/uploads/2009/09/l4d_small.jpg

---
Wie immer wird das Update für PC Spieler kostenlos sein. Die Xbox 360 Version des Updates veranschlagt 560 Microsoft Punkte.

 

 

* * *



* * *