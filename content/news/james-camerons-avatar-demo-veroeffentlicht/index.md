---
title: James Cameron`s Avatar – Demo veröffentlicht
author: gamevista
type: news
date: 2009-11-16T18:46:37+00:00
excerpt: '<p>[caption id="attachment_1047" align="alignright" width=""]<img class="caption alignright size-full wp-image-1047" src="http://www.gamevista.de/wp-content/uploads/2009/11/avatar.jpg" border="0" alt="Avatar - Das Spiel" title="Avatar - Das Spiel" align="right" width="140" height="100" />Avatar - Das Spiel[/caption]Der Publisher <a href="http://www.ubi.com/DE/Games/Info.aspx?pId=7945" target="_blank">Ubisoft</a> hat heute die Demo zum kommenden Actionspiel <strong>James Cameron`s Avatar</strong> veröffentlicht. Allerdings wird der Launch der Demo von einer Firma die koffeinhaltige Getränke anbietet vermarktet. So bietet die Website <a href="http://www.cokezero.de/" target="_blank">cokezero.de</a> exklusiv den Download der Demo zu <strong>Avatar - Das Spiel </strong>an.</p> '
featured_image: /wp-content/uploads/2009/11/avatar.jpg

---
Ob Konsolenbesitzer auch in den Genuss einer Demo kommen ist bisher nicht bekannt. Immerhin hat der Entwickler aber den Releasetermin für das Spiel angegeben. Demnach wird Avatar &#8211; Das Spiel am 03. Dezember 2009 für PC, PlayStation 3 und PSP, Xbox 360, Nintendo Wii und DS erscheinen. Hoffen wir darauf das dieser Mirror dem Ansturm standhält.

 

* * *



* * *