---
title: 'Operation Flashpoint 2: Dragon Rising – Vorbesteller Bonus enthüllt'
author: gamevista
type: news
date: 2009-08-24T19:22:26+00:00
excerpt: '<p>[caption id="attachment_198" align="alignright" width=""]<img class="caption alignright size-full wp-image-198" src="http://www.gamevista.de/wp-content/uploads/2009/07/opf2_small.jpg" border="0" alt="Operation Flashpoint 2: Dragon Rising" title="Operation Flashpoint 2: Dragon Rising" align="right" width="140" height="100" />Operation Flashpoint 2: Dragon Rising[/caption]Fans der Kriegssimulation <strong>Operation Flashpoint 2: Dragon Rising</strong> die ihre Kopie des Spiels bei Amazon.de vorbestellt haben erhalten einen Code der eine Bonus Mission Freischalten soll. Diese nennt sich „Angriff aus der Dunkelheit“ und kann via Zugangscode zwei bis drei Tage nach der Veröffentlichung von <strong>Operation Flashpoint 2: Dragon Rising</strong> auf der <a href="http://www.amazon.de/gp/feature.html/ref=amb_link_84558273_1?ie=UTF8&docId=1000328843&pf_rd_m=A3JWKAKR8XB7XF&pf_rd_s=special-offers-1&pf_rd_r=1839VN055Y4RKCZP341A&pf_rd_t=201&pf_rd_p=471294733&pf_rd_i=B001T0HGMK">Amazon.de</a> Website aktiviert werden.</p> '
featured_image: /wp-content/uploads/2009/07/opf2_small.jpg

---
„Die Spieler übernehmen die Rolle eines Spezialeinsatz-Kommandos, das einen Angriff auf eine strategische, von VBA-Einheiten verteidigte Schlüsselposition durchführt. Durch Nachtsichtgeräte haben die Spieler einen taktischen Vorteil gegenüber den Verteidigern der VBA und können frei entscheiden, ob sie schnell und laut vorgehen oder die Nacht zur Tarnung nutzen – dabei aber riskieren, dass die Verstärkung in der Zwischenzeit eintrifft.“ Soviel von Amazon.de zum Missionsablauf. Der Kriegsshooter erscheint am 8. Oktober und unterstützt die neue [Philips amBX Technologie][1].

* * *



* * *

 

 [1]: http://www.ambx.com