---
title: Die Siedler 7 – Limited Edition auch für Mac
author: gamevista
type: news
date: 2010-02-09T21:12:45+00:00
excerpt: '<p>[caption id="attachment_740" align="alignright" width=""]<img class="caption alignright size-full wp-image-740" src="http://www.gamevista.de/wp-content/uploads/2009/09/diesiedler7.jpg" border="0" alt="Die Siedler 7" title="Die Siedler 7" align="right" width="140" height="100" />Die Siedler 7[/caption]Der Publisher <strong>Ubisoft </strong>hat im Rahmen einer Pressemitteilung das erscheinen einer Limited Edition für das Aufbaustrategiespiel <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Die Siedler 7</a> angekündigt. Die Spezial-Edition wird neben einigen Extras in der DVD-Box auch zahlreiche zusätzliche Spielinhalte bieten.</p> '
featured_image: /wp-content/uploads/2009/09/diesiedler7.jpg

---
Weiterhin hat **Ubisoft** bekannt gegeben, das <a href="http://siedler.de.ubi.com/siedler-7/" target="_blank">Die Siedler 7</a> auch als Mac-Version erscheint. Die Siedler 7 wird am 25. März 2010 in Deutschland und Europa veröffentlicht.

  * Exklusive Spieleigenschaften: 
      * Eine exklusive Map
      * Castle Forge-Elemente um Siedlungen zu individualisieren und die eigene Burg weiter nach eigenen Vorstellungen anzupassen: 1 Torbogen, 2 Fenster, 1 Erker, 1 Balkon und 1 Gargoyle
  * Exklusive Goodies: 
      * Der Soundtrack zum Spiel
      * Eine 16 cm große Siedler-Figur
      * Eine Packung Pflanzensamen für Mais, Weizen und Gerste, die die Spieler an die Basis beim Aufbau von Siedlungen erinnern soll (Getreideanbau)
      * Ein Poster mit Siedler-Charakteren im A2-Format

* * *



* * *