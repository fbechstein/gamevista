---
title: EA Chief Creative Officer – PlayStation 3.5 kommt vor PlayStation 4
author: gamevista
type: news
date: 2009-08-28T07:38:46+00:00
excerpt: '<p />[caption id="attachment_540" align="alignright" width=""]<a href="http://www.electronic-arts.de"><img class="caption alignright size-full wp-image-540" src="http://www.gamevista.de/wp-content/uploads/2009/08/ps3.jpg" border="0" alt="PlayStation 3" title="PlayStation 3" align="right" width="140" height="100" />Electronic Arts</a>PlayStation 3[/caption] Chief Creative Officer Rich Hilleman teilte im Rahmen eines Interviews mit das Sony noch vor der Planung einer <strong>PlayStation 4 </strong>eine neue <strong>PlayStation 3</strong> Version entwickeln würde. Das gleiche sieht er für die <strong>Xbox 360</strong>, bevor die <strong>Xbox 720</strong> erscheint wird es eine neue Version der Xbox 360 laut seiner Meinung geben.<br /> '
featured_image: /wp-content/uploads/2009/08/ps3.jpg

---
Grund dafür ist das aktuelle Konsolen laut Hilleman an den derzeitigen Standards kaum noch Schritt halten können. Die Spielentwicklung hat momentan ein so schnelles Tempo das auch für die Konsolen bald die nächste stärkere Version entwickelt werden müsste. Erst vor kurzem stellte Sony ja seine **PlayStation 3 Slim** vor. Ob da noch was vor der PlayStation 4 kommen wird ist bisher aber noch nicht klar, eventuell meldet sich ja Sony noch zu dieser Aussage. 

* * *



* * *

 