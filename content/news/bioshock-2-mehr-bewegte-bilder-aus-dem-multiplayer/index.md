---
title: Bioshock 2 – Mehr bewegte Bilder aus dem Multiplayer
author: gamevista
type: news
date: 2009-12-02T20:50:01+00:00
excerpt: '<p>[caption id="attachment_345" align="alignright" width=""]<img class="caption alignright size-full wp-image-345" src="http://www.gamevista.de/wp-content/uploads/2009/08/bioshock2_small.png" border="0" alt="Bioshock 2" title="Bioshock 2" align="right" width="140" height="100" />Bioshock 2[/caption]Der Entwickler <a href="http://www.bioshock2game.com" target="_blank">2K Games</a> hat neue bewegte Bilder zum Ego-Shooter<strong> Bioshock 2</strong> veröffentlicht. So gibt es zum Multiplayer-Modus actionreiche Spielszenen zu bestaunen. Gezeigt wird in dem zwei Minuten langen Video auch ein neues Feature das den Gegner in die Luft schleudert. <strong>Bioshock 2</strong> wird hierzulande am 09. Februar 2010 in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2009/08/bioshock2_small.png

---
[>Zum Bioshock 2 &#8211; Sinclair Solutions Trailer][1]

 







 

 [1]: videos/item/root/bioshock-2-sinclair-solutions-trailer