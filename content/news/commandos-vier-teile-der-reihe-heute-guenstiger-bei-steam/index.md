---
title: Commandos – Vier Teile der Reihe heute günstiger bei Steam
author: gamevista
type: news
date: 2010-04-14T17:33:44+00:00
excerpt: '<p>[caption id="attachment_478" align="alignright" width=""]<img class="caption alignright size-full wp-image-478" src="http://www.gamevista.de/wp-content/uploads/2009/08/commandos2_bi.jpg" border="0" alt="Commandos " title="Commandos " align="right" width="140" height="100" />Commandos [/caption]Im Rahmen der Midweek Madness beim Onlineportal <strong>Steam </strong>gibt es nur heute ein Angebot das einige Strategiefans hellhörig werden lassen dürfte. Mit einem speziellen Angebot könnt ihr für gerade mal fünf Euro die Teile der Commandos-Reihe:  <strong>Commandos </strong>+ Addon <strong>Beyond the Call of Duty</strong>, <strong>Commandos 2: Men of Courage</strong> und <strong>Commandos 3: Destination Berlin</strong> erwerben.</p> '
featured_image: /wp-content/uploads/2009/08/commandos2_bi.jpg

---
Insgesamt sind das also vier Spiele die es in sich haben.

 

<a href="http://store.steampowered.com/sub/4156" target="_blank">> Zum Commandos &#8211; Steam Angebot</a>

<cite></cite>

   

* * *

   

