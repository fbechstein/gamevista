---
title: 'Dragon Age: Origins – Neuer DLC für PC verfügbar'
author: gamevista
type: news
date: 2010-01-31T21:04:29+00:00
excerpt: '<p>[caption id="attachment_113" align="alignright" width=""]<img class="caption alignright size-full wp-image-113" src="http://www.gamevista.de/wp-content/uploads/2009/07/dragon_age_small.jpg" border="0" alt="Dragon Age: Origins" title="Dragon Age: Origins" align="right" width="140" height="100" />Dragon Age: Origins[/caption]Für das Rollenspiel <a href="http://dragonage.bioware.com/addon/rto" target="_blank">Dragon Age: Origins</a> ist nun offiziell der Download-Zusatzinhalt <strong>Rückkehr nach Ostagar</strong> als PC-Version verfügbar. Eigentlich sollte der DLC bereits am 15. Januar 2010 veröffentlicht werden. Erwerben könnt ihr den Download-Content im Shop auf der offiziellen <a href="http://dragonage.bioware.com/addon/rto" target="_blank">BioWare Website</a>.</p> '
featured_image: /wp-content/uploads/2009/07/dragon_age_small.jpg

---
Kostenpunkt sind 5 USD und ihr benötigt einen Paypal-Account oder eine Kreditkarte.

 

* * *



* * *