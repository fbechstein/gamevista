---
title: 'Assassin’s Creed 2: Lineage – Teil 2 und 3 der Kurzfilmreihe erschienen'
author: gamevista
type: news
date: 2009-11-14T16:28:16+00:00
excerpt: '<p>Der Publisher <a href="http://www.assassinscreed.de.ubi.com" target="_blank">Ubisoft</a> hatte vor kurzem den ersten Teil der Trilogie von <strong>Assassin’s Creed 2: Lineage</strong> veröffentlic[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin’s Creed 2" title="Assassin’s Creed 2" align="right" width="140" height="100" />Assassin’s Creed 2[/caption]ht. In dieser Kurzfilmreihe wird die Geschichte des Vaters von Ezio erzählt. Nun hat <a href="http://www.assassinscreed.de.ubi.com" target="_blank">Ubisoft</a>, kurz vor Release des Actionspiels, die beiden anderen Teile von <strong>Assassin’s Creed 2: Lineage</strong> den Fans präsentiert.</p> '
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Leider sind diese bisher nur auf Englisch verfügbar. Sobald diese von <a href="http://www.assassinscreed.de.ubi.com" target="_blank">Ubisoft</a> mit Untertitel erscheinen oder sogar Synchronisiert werden, geben wir euch natürlich bescheid. **Assassin’s Creed 2** erscheint am 19. November 2009 für Xbox 360 und PlayStation 3. Die PC Version wird erst im Frühling 2010 veröffentlicht werden.

[> Zum Assassin’s Creed 2: Lineage &#8211; Teil 2][1]

[> Zum Assassin’s Creed 2: Lineage &#8211; Teil 3][2]

* * *



* * *

 

 [1]: videos/item/root/assassins-creed-2-lineage-kurzfilm-teil-2
 [2]: videos/item/root/assassins-creed-2-lineage-kurzfilm-teil-3