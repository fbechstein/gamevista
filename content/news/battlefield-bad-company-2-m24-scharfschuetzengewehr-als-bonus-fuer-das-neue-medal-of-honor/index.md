---
title: 'Battlefield: Bad Company 2 – M24 Scharfschützengewehr als Bonus für das neue Medal of Honor'
author: gamevista
type: news
date: 2010-04-29T17:11:18+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Das überaus mächtige Scharfschützengewehr M24, das bekannt ist aus dem Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a>, wird auch im neuen Medal of Honor seinen Platz haben. Nun gab der Publisher <strong>Electronic Arts</strong> im offiziellen <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> Blog bekannt, dass die Käufer von BC2 ihre M24 Waffe im kommenden Medal of Honor-Teil nicht mehr freischalten müssen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Alles was ihr dafür machen müsst, ist euer EA-Benutzerkonto womit ihr <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> registriert und verknüpft habt, auch mit dem neuen Medal of Honor-Teil zu verbinden.

 

 

   

* * *

   

