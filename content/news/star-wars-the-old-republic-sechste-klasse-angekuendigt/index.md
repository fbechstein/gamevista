---
title: 'Star Wars: The Old Republic – Sechste Klasse angekündigt'
author: gamevista
type: news
date: 2009-11-07T16:52:37+00:00
excerpt: '<p>[caption id="attachment_1013" align="alignright" width=""]<img class="caption alignright size-full wp-image-1013" src="http://www.gamevista.de/wp-content/uploads/2009/11/sttor_small.png" border="0" alt="Star Wars: The Old Republic" title="Star Wars: The Old Republic" align="right" width="140" height="100" />Star Wars: The Old Republic[/caption]Der Entwickler <a href="http://www.swtor.com" target="_blank">BioWare</a> hat die nunmehr sechste Klasse für das Online-Rollenspiel <strong>Star Wars: The Old Republic</strong> angekündigt. Dies geht aus einer Nachricht des Community-Managers Sean Dahlberg im <a href="http://www.swtor.com/community/showthread.php?t=86536" target="_blank">offiziellen Forum</a> hervor. Der imperiale Agent wird die sechse Klasse einnehmen und soll sich ähnlich spielen wie ein Schurke aus bekannten Online-Rollenspielen.</p> '
featured_image: /wp-content/uploads/2009/11/sttor_small.png

---
So nutzt dieser gerne technisches Kleinstgerät und begibt sich in den Schatten um so Gegner auszuschalten. Die ersten Screenhots zum imperialen Agenten könnt ihr euch in unserer [Galerie][1] anschauen.

 

 

* * *



* * *

 [1]: screenshots/item/root/star-wars-the-old-republic-screenshots