---
title: Fighters Inc. verklagt EA auf 15 Millionen US-Dollar
author: gamevista
type: news
date: 2009-09-06T10:40:13+00:00
excerpt: '<p>[caption id="attachment_623" align="alignright" width=""]<img class="caption alignright size-full wp-image-623" src="http://www.gamevista.de/wp-content/uploads/2009/09/fightnight4.jpg" border="0" alt="Fight Night Round 4" title="Fight Night Round 4" align="right" width="140" height="100" />Fight Night Round 4[/caption]Fighters Inc. Hat seit 2007 eine Gruppenlizenz mit mehr als 32 Profi-Boxern. Nun verklagen sie wegen der Verhandlungen für Fight Night Round 4 den Publisher <a href="http://www.electronic-arts.de" target="_top">Electronic Arts</a>. Der Schaden soll sich auf 15 Millionen US-Dollar belaufen.</p> '
featured_image: /wp-content/uploads/2009/09/fightnight4.jpg

---
Chip Meyers, Mitglied des Management von Fighters Inc. schrieb: „Nicht nur, dass EA Geld aus den Taschen der Profi Boxer zieht, die sich an dem Gruppen-Lizenz Modell beteiligen – nein EA hat uns schamlos hintergangen und ging in direkte Verhandlungen mit einzelnen Boxern was anderen, die Teil unseres Programms sind, einfach die Einnahmen gefährdete.“ [Zitat wurde aus dem Englischen übersetzt]

<a href="http://www.electronic-arts.de" target="_top">EA</a> wird nun wegen „unfairem Wettbewerb und anderen Vergehen“ angeklagt. Sie sollen das Lizenzprogramm vom Fighters Inc. hintergangen haben, indem sie sich in direkte Verhandlungen mit einzelnen Boxern begaben.

Noch ist offen ob EA mit einem blauen Auge davon kommt oder K.O. zu Boden sacken wird – spannend wird der Kampf aber sicher.

* * *



* * *