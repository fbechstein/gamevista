---
title: Natural Selection 2 – Start der Closed-Beta
author: gamevista
type: news
date: 2010-11-21T18:48:20+00:00
excerpt: '<p>[caption id="attachment_348" align="alignright" width=""]<img class="caption alignright size-full wp-image-348" src="http://www.gamevista.de/wp-content/uploads/2009/08/ns2_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Entwickler <strong>Unknown Worlds</strong> hat nach der mehrmonatigen Alpha-Phase des Mehrspieler-Shooters <a href="http://www.unknownworlds.com/ns2/" target="_blank">Natural Selection2</a>, den Beta-Start eingeläutet. Vorbesteller des Titels durften in der Vergangenheit tatkräftig am Entwicklungsprozess teilnehmen und die Alpha-Version des Spiels ausgiebig testen.</p> '
featured_image: /wp-content/uploads/2009/08/ns2_small.jpg

---
Die geschlossene Beta-Phase bietet weiterhin die Möglichkeit bereits kleinere Matches auf neuen Karten zu bestreiten und die neuen Features des Spiels zu testen. Passend dazu haben die Entwickler auch ein weiteren Trailer präsentiert. Dieser stellt die überarbeitete Alien-Form des Fades vor.

[> Zum Natural Selection 2 &#8211; Fade Reveal Trailer][1]

   

* * *

   



 [1]: videos/item/root/natural-selection-2-fade-reveal-trailer