---
title: Left 4 Dead 2 – Termin für die Demo ist da
author: gamevista
type: news
date: 2009-09-23T15:35:23+00:00
excerpt: '<p>[caption id="attachment_116" align="alignright" width=""]<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" />Left 4 Dead 2[/caption]Entwickler <a href="http://www.valvesoftware.com">Valve </a>hat heute den Termin für die kommende Demo des Zombie Shooters <strong>Left 4 Dead 2</strong> im Rahmen einer Pressekonferenz zur Tokyo Game Show 2009 angekündigt. Der Stichtag ist der 27. Oktober 2009, dann könnt ihr endlich den neuen Teil kostenlos antesten.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
Bisher gibt und gab es aber keinerlei Details zur Demo. Ob PC Demo oder Xbox Live, niemand weiß bisher welchen Umfang die Demo am 27. haben wird. Sobald wir Neuigkeiten erfahren teilen wir sie euch natürlich umgehend mit.

 

 

* * *



* * *

 