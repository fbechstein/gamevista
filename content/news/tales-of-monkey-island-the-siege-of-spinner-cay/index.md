---
title: Tales of Monkey Island – The Siege of Spinner Cay
author: gamevista
type: news
date: 2009-08-10T11:17:33+00:00
excerpt: '<p>[caption id="attachment_315" align="alignright" width=""]<img class="caption alignright size-full wp-image-315" src="http://www.gamevista.de/wp-content/uploads/2009/08/scummbar_small.png" border="0" alt="Tales of Monkey Island" title="Tales of Monkey Island" align="right" width="140" height="100" />Tales of Monkey Island[/caption]Wie Entwickler <a href="http://www.telltalegames.com" target="_blank" title="telltalegames">Telltale Games</a> per Pressemitteilung berichtet wird die zweite <strong>Tales of Monkey Island</strong> Episode „<strong>The Siege of Spinner Cay</strong>“ noch diesen Monat kommen. Die PC Version soll demnach am 20. August 2009 veröffentlicht werden. Für die Nintendo Wii Umsetzung gibt es bisher keinen genauen Termin. </p>'
featured_image: /wp-content/uploads/2009/08/scummbar_small.png

---
Wer die Neuveröffentlichung des ersten Teils noch nicht kennt kann in unserem [Artikel][1] dazu Weiterlesen.







 

 

 [1]: hot-spots/42-pre-reviews-pc/124-review-the-secret-of-monkey-island-special-edition-xbox-360-pc