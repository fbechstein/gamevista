---
title: Venetica – Demo veröffentlicht
author: gamevista
type: news
date: 2009-10-09T18:20:15+00:00
excerpt: '<p style="margin-bottom: 0cm;"> </p> <p>[caption id="attachment_532" align="alignright" width=""]<img class="caption alignright size-full wp-image-532" src="http://www.gamevista.de/wp-content/uploads/2009/08/venetica_small.png" border="0" alt="Venetica" title="Venetica" align="right" width="140" height="100" />Venetica[/caption]Der Publisher <a href="http://www.venetica-game.com" target="_blank">dtp Entertainment</a> hat heute die Vollversion zum Rollenspiel <strong>Venetica </strong>veröffentlicht. Wie in der Vollversion startet sie im Bergdorf San Pasquale.</p> '
featured_image: /wp-content/uploads/2009/08/venetica_small.png

---
Hier wird der Spieler in die Welt von **Venetica** eingeführt. Leider wird ihnen aber in der Testversion der Zugang zu Venedig verwehrt. Venedig selbst zu betreten funktioniert nur in der Vollversion. Die Demo umfasst knapp einen Gigabyte. Die Anspielversion findet ihr wie immer auf **gamevista.de** im Downloadbereich. **Venetica** ist für PC bereits erschienen. Die Xbox 360 Version erscheint vorraussichtlich am 27. November 2009 in Deutschland.

[> Zum Venetica &#8211; Demo Download][1]

* * *



* * *

 

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=335