---
title: Venetica – Erreicht Goldstatus
author: gamevista
type: news
date: 2009-08-27T18:50:42+00:00
excerpt: '<p />[caption id="attachment_532" align="alignright" width=""]<img class="caption alignright size-full wp-image-532" src="http://www.gamevista.de/wp-content/uploads/2009/08/venetica_small.png" border="0" alt="Venetica" title="Venetica" align="right" width="140" height="100" />Venetica[/caption]Wieder ein Tag der Freude für alle Rollenspielsympathisanten. Der deutsche Entwickler <a href="http://www.venetica-game.com/">Deck13</a> hat heute per Pressemitteilung offiziell bekannt gegeben das ihr Rollenspiel <strong>Venetica</strong> nun Goldstatus erreicht hat und auf dem Weg in die Presswerke ist. Erscheinungstermin der PC Version wird der 4. September sein. <br /> '
featured_image: /wp-content/uploads/2009/08/venetica_small.png

---
Eine Xbox Variante ist auch in Arbeit, aber bisher gibt es noch keine genauen Details dazu. Mit einem Release rechnet [Deck13][1] gegen Ende 2009 bzw. Anfang 2010.</p> 

* * *



* * *

 

 [1]: http://www.venetica-game.com/