---
title: Dragon Age 2 – Publisher gibt Veröffentlichungstermin bekannt
author: gamevista
type: news
date: 2010-08-17T15:13:15+00:00
excerpt: '<p>[caption id="attachment_1996" align="alignright" width=""]<img class="caption alignright size-full wp-image-1996" src="http://www.gamevista.de/wp-content/uploads/2010/07/dragonage21.jpg" border="0" alt="Dragon Age 2" title="Dragon Age 2" align="right" width="140" height="100" />Dragon Age 2[/caption]Publisher <strong>Electronic Arts</strong> hat zusammen mit den Entwicklern von <strong>BioWare </strong>den Veröffentlichungstermin für den zweiten Teil des Rollenspiels <strong>Dragon Age</strong> bekannt gegeben. <a href="http://dragonage.bioware.com/da2/" target="_blank">Dragon Age 2</a> wird am 11. März 2011 in Europa in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2010/07/dragonage21.jpg

---
Die USA erwarten das Rollenspiel bereits am 8. März 2011.

 

   

* * *

   



 