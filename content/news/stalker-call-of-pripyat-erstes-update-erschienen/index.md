---
title: 'Stalker: Call of Pripyat – Erstes Update erschienen'
author: gamevista
type: news
date: 2010-02-10T20:19:11+00:00
excerpt: '<p>[caption id="attachment_673" align="alignright" width=""]<img class="caption alignright size-full wp-image-673" src="http://www.gamevista.de/wp-content/uploads/2009/09/stalker_cop.jpg" border="0" alt="Stalker: Call of Pripyat" title="Stalker: Call of Pripyat" align="right" width="140" height="100" />Stalker: Call of Pripyat[/caption]Dem Ego-Shooter <a href="http://www.cop.stalker-game.com" target="_blank">Stalker: Call of Pripyat</a> hat der Publisher <strong>bitCompose Games</strong> einen ersten Patch spendiert. Das Update mit der Versionsnummer 1.6.02 beseitigt neben ein paar Fehlern, auch diverse Absturzprobleme des Spiels. Außerdem werden unterschiedliche Änderungen im Einzel- sowie im Mehrspielerbereich vorgenommen.</p> '
featured_image: /wp-content/uploads/2009/09/stalker_cop.jpg

---
Die Liste der Änderungen findet ihr auf der zweiten Seite.

[> Zum Stalker: Call of Pripyat &#8211; Patch 1.6.02 Download][1]

* * *



* * *

</p> 

**<span style="text-decoration: underline;"><img class="caption alignright size-full wp-image-673" src="http://www.gamevista.de/wp-content/uploads/2009/09/stalker_cop.jpg" border="0" alt="Stalker: Call of Pripyat" title="Stalker: Call of Pripyat" align="right" width="140" height="100" />Changelog Stalker: Call of Pripyat &#8211; Patch 1.6.02</span>**

  * Fehler behoben, der dazu führte, dass das Spiel im Fenstermodus statt im Vollbild gestartet wurde.
  * Tessellationsparameter wurde geändert, wodurch die Performance bei Modellen mit vielen Polygonen verbessert wird.
  * Fehler behoben, der sich auf Konfigurationsdumps auswirkte und dazu führte, dass falsche Einstellungen angezeigt wurden.
  * Die Partikeldarstellung bei der Interaktion mit Wasser wurde optimiert.
  * Fehler behoben, der sich auf die Belegung der Maustasten auswirkte.
  * Der Wechsel zwischen Windows und dem Spielfenster wurde beschleunigt.
  * Die Online-Charaktersynchronisierung wurde optimiert (Datentraffic um den Faktor 3 verringert).
  * Fehler behoben, der dazu führte, dass bei niedrigen Grafikeinstellungen dynamisches Licht für Lichtquellen verwendet wurde.
  * Fehler behoben, der auf dedizierten Servern zum Absturz führte, wenn die Anmeldung über die Serverkonsole erfolgte.
  * Fehler behoben, der zum Absturz führte, wenn bei Online-Niederlagen Screenshots oder Konfigurationsdumps erstellt wurden.
  * Automatisches Zielen kann nicht mehr genutzt werden, wenn mit dem Unterlaufoder Bulldog-Granatwerfer geschossen wird.
  * Abstürze behoben, die auftraten, wenn zu lange im Einzelspielermodus gespielt wurde.
  * Eine neue Funktion für dedizierte Server wurde hinzugefügt, mit der die Werte von Spielern noch für eine gewisse Zeit gespeichert werden, nachdem ihre Verbindung getrennt wurde (Konsolenbefehl: sv\_client\_reconnect_time).
  * Im Remote-Administratormenü wird nun das korrekte Limit für den maximalen Ping angezeigt.
  * Das automatische Wechseln zur besten Waffe im Inventar wird nun deaktiviert, wenn im Mehrspielermodus das Artefakt aufgehoben wird.
  * Fehler behoben, der dazu führte, dass Granaten in Unterlaufgranatwerfern angezeigt wurden.
  * Fehler behoben, der sich auf den Profiltransfer auswirkte.
  * Die Qualität der dynamischen Schattendarstellung wurde verbessert
  * Viele Fehler behoben, die zum Absturz im Mehrspielermodus führten.
  * Das Anti-Cheat-System wurde verbessert.
  * Die Charaktergeschwindigkeit, Sprunghöhe und Steuerung im Sprung wurden geändert.
  * Die Eigenschaften einiger Waffen wurden geändert.
  * „No available phrase to say, dialog[zat\_b30\_owl\_stalker\_trader\_start\_dialog]“-Fehler behoben.
  * „No available phrase to say, dialog[about\_quests\_dialog_stalkers]“-Fehler behoben.
  * Fehler behoben, der dazu führte, dass das Spiel vor dem Gespräch mit Kowalski vor dem Verlassen des Labors X8 einfror.
  * Die Unterstützung für folgende Onboard-Grafikkarten wurde verbessert: Intel GMA G43/45 und besser.

* * *



* * *

 [1]: downloads/patches/item/patches/stalker-call-of-pripyat-patch-1602