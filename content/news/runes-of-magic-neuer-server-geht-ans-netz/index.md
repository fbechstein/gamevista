---
title: Runes of Magic – Neuer Server geht ans Netz
author: gamevista
type: news
date: 2009-11-12T19:31:39+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Der Publisher <a href="http://www.runesofmagic.com" target="_blank">Frogster Interactive</a> hatte heute offiziell bestätigt das bereits am Freitag den 13. November 2009 ein neuer Server für die deutschen Spieler des Online-Rollenspiels <strong>Runes of Magic</strong> bereit gestellt wird. Dieser wird mit dem Namen „Draiochta“ pünktlich um 18.00 Uhr online gehen, so <a href="http://www.runesofmagic.com" target="_blank">Frogster Interactive</a>.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Der Publisher berichtet außerdem, dass zum Start des neuen Servers ein kleines Event für die Neueinsteiger gestartet wird. Demnach wird den Spielern vom 13. bis 20. November die doppelte Anzahl an Erfahrungspunkten gewährt sobald sie Quests abschließen bzw. Monster töten.

 

 

* * *



* * *