---
title: Hearts of Iron 3 – Goldstatus
author: gamevista
type: news
date: 2009-07-27T21:48:11+00:00
excerpt: '<p>[caption id="attachment_178" align="alignright" width=""]<img class="caption alignright size-full wp-image-178" src="http://www.gamevista.de/wp-content/uploads/2009/07/heartsofiron3_small.jpg" border="0" alt="Hearts of Iron 3" title="Hearts of Iron 3" align="right" width="140" height="100" />Hearts of Iron 3[/caption]<a href="http://www.paradoxplaza.com//" target="_blank" title="paradox">Paradox Interactive</a> verkündete heute eine freudige Botschaft. Im Rahmen einer offiziellen Pressemeldung wurde mitgeteilt das <strong>Hearts of Iron 3</strong> fertig ist. Die Master CD sei bereits auf dem Weg zum Presswerk so das dem pünktlichen Release nichts mehr im Wege stehen kann.</p>'
featured_image: /wp-content/uploads/2009/07/heartsofiron3_small.jpg

---
Das Hardcore Strategiespiel **Hearts of Iron 3** wird in mehreren Bereichen verbessert. Ungefähr 15000 Provinzen wird **Hearts Iron 3** enthalten, das sind 5 mal mehr als im Vorgänger. <span> </span>Außerdem wird die Diplomatie eine größere Rolle spielen, zum Beispiel können sie nun Waffenimporte von Alliierten ordern. Das Interface wird wie die Karte überarbeitet und natürlich gibt es neue Einheiten zu bestaunen. 







 

 