---
title: Uncharted 2 – keine Installation notwendig
author: gamevista
type: news
date: 2009-07-29T13:37:52+00:00
excerpt: '<p>[caption id="attachment_202" align="alignright" width=""]<img class="caption alignright size-full wp-image-202" src="http://www.gamevista.de/wp-content/uploads/2009/07/uncharted2_small.png" border="0" alt="Uncharted 2 wird keine Installation benötigen" title="Uncharted 2 wird keine Installation benötigen" align="right" width="140" height="100" />Uncharted 2 wird keine Installation benötigen[/caption]Wie Christophe Balestra vom Entwickler <a href="http://www.naughtydog.com/" target="_blank">Naughy Dog</a> über seinen Twitter Account berichtet, wird <strong>Uncharted 2</strong> komplett ohne Installation auskommen:</p><p>"<span class="entry-content">Uncharted 2 does not require any install" - </span><span class="entry-content">D.h. Verpackung auf, Disc rein und Spiel an. '
featured_image: /wp-content/uploads/2009/07/uncharted2_small.png

---
</span>

<span class="entry-content">Uncharted 2 kommt am 23. Oktober 2009 exclusiv für die PS3.</span> <span class="entry-content"></p> 



<p>
  
</p>


</span>