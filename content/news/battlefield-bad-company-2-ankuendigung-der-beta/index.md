---
title: 'Battlefield: Bad Company 2 – Ankündigung der Beta'
author: gamevista
type: news
date: 2009-11-07T10:19:58+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Der Entwickler <a href="http://www.badcompany2.ea.com" target="_blank">DICE</a> hat heute im Rahmen einer offiziellen Pressemitteilung angekündigt das es eine Mehrspieler-Betaphase exklusiv auf der PlayStation 3 für den Mehrspieler-Shooter <strong>Battlefield: Bad Company 2</strong> geben wird.  Ab dem 19. November können so Fans des neusten Battlefield-Teils die neuen Fahrzeuge, Waffen  und das  Squad-Play testen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
&#8222;Für PlayStation-Spieler ist dies eine außerordentliche Gelegenheit, mit Battlefield: Bad Company 2 einen der actionreichsten und ausgefeiltesten Shooter auf dem Markt noch vor der offiziellen Veröffentlichung anspielen zu können&#8220;, erklärt Rob Dyer, Senior Vice President Publisher Relations.

 

&#8222;Wir haben es uns zum Ziel gesetzt, mit Battlefield: Bad Company 2 die Messlatte für alle Aspekte unserer Multiplayer-Erfahrung höher zu legen und einen neuen Standard für Onlinespiele zu setzen&#8220;, so Patrick Bach, Senior Producer von Battlefield: Bad Company 2. &#8222;Von der Grafik über Fahrzeuge und Waffen bis hin zur Zerstörung werden wir im nächsten Jahr das beste Online-Multiplayerspiel herausbringen, und wir möchten, dass sich die Spieler einen eigenen Eindruck davon verschaffen, indem sie die Beta und die Demo spielen.&#8220;

In der **Battlefield: Bad Company 2** Beta wird die Karte Arica Harbour anspielbar sein. Hier werden bis zu 24 Spieler gleichzeitig im Rush-Modus, einen von vier Mehrspieler-Modi, gegeneinander antreten. Im Dezember, so der Entwickler <a href="http://www.badcompany2.ea.com" target="_blank">DICE</a>, werden auch PC Spieler in den Genuss der Beta kommen. So kann man hier an einer geschlossenen Beta teilnehmen. Im Frühjahr 2010 kündigte der Entwickler die Mehrspieler-Demo für PC, Xbox 360 und PlayStation 3 an. **Battlefield: Bad Company 2** kommt am 4. März 2010 in die Händlerregale.

* * *



* * *