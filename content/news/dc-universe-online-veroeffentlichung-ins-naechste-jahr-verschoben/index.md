---
title: DC Universe Online – Veröffentlichung ins nächste Jahr verschoben
author: gamevista
type: news
date: 2010-10-05T14:02:28+00:00
excerpt: '<p>[caption id="attachment_1932" align="alignright" width=""]<img class="caption alignright size-full wp-image-1932" src="http://www.gamevista.de/wp-content/uploads/2010/06/dcuo.jpg" border="0" alt="DC Universe Online" title="DC Universe Online" align="right" width="140" height="100" />DC Universe Online[/caption]Publisher <strong>Sony </strong>hat im Rahmen einer Pressemitteilung den Release-Termin des Online-Rollenspiels <a href="http://www.dcuniverseonline.com" target="_blank">DC Universe Online</a> verschoben. Demnach wird der Titel nicht wie erwartet in diesem Jahr veröffentlicht, sondern im Frühjahr 2011 in den Handel kommen.</p> '
featured_image: /wp-content/uploads/2010/06/dcuo.jpg

---
Grund dafür sind weitere Feinarbeiten am Helden-Spiel und die Umsetzung der Wünsche der Community, die aus der momentan noch laufenden Beta resultieren. <a href="http://www.dcuniverseonline.com/" target="_blank">DC Universe Online</a> spielt wie schon im Titel zu erahnen ist im DC-Comic-Universum. Der Spieler kann sich Superhelden erstellen und Quests zusammen mit den Helden wie z.B. Batman oder Superman erfüllen.

   

* * *

   



 