---
title: 'Fallout: New Vegas – Infos zur Collectors Edition'
author: gamevista
type: news
date: 2010-05-11T17:36:45+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Neben der Standard Edition des Rollenspiels <a href="http://fallout.bethsoft.com/eng/home/pr-051110.php" target="_blank">Fallout: New Vegas</a> die im 4. Quartal 2010 in den Handel kommen soll, wird es auch eine Collectors Edition geben. Dies kündigte <strong>Bethesda </strong>im Rahmen einer Pressemitteilung der Öffentlichkeit mit. Neben vielen Extras wird es Spielnamentypisch auch Pokerchips und ein New Vegas-Kartenspiel geben.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Natürlich darf da eine Making of DVD nicht fehlen. Diese enthält zahlreiche Hintergrundinformationen zur Entwicklung des neusten Fallout Spiels.

Die Collectors Edition von Fallout: New Vegas enthält:

  * 1 Fallout: New Vegas
  * 7 Poker-Chips, diese repräsentieren die Chips aus den sieben großen Casinos aus Las Vegas
  * 1 komplettes Karten Deck. Die Motive sind den Charakteren und Fraktionen aus dem Spiel nachempfunden
  * 1 &#8222;Lucky 38&#8220; Platinchip
  * 1 Hardcover Ausgabe des Comic &#8222;All Roads&#8220;
  * 1 Making of DVD

<p style="text-align: center;">
  <img class=" size-full wp-image-1786" src="http://www.gamevista.de/wp-content/uploads/2010/05/fnv_ce-generic-sm.jpg" border="0" width="200" height="159" />
</p>

   

* * *

   

