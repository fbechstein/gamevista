---
title: Assassin’s Creed 2 – Auf März verschoben
author: gamevista
type: news
date: 2010-01-18T15:07:54+00:00
excerpt: |
  |
    <p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin's Creed 2" title="Assassin's Creed 2" align="right" width="140" height="100" />Assassin's Creed 2[/caption]Wie der Publisher <strong>Ubisoft </strong>nun offiziell bestätigt hat, wird die PC Version des Actiontitels <a href="http://www.assassinscreed.uk.ubi.com" target="_blank">Assassin's Creed 2</a> erst im März 2010 erscheinen. Der bisherige Termin am 15. Februar 2010 kann somit nicht eingehalten werden. Den Grund für die Verschiebung benannte der Publisher nicht.</p>
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Als Ezio Auditore da Firenze, einem jungen italienischen Adligen, zieht ihr meuchelnd durch das Italien des 15. Jahrhunderts nachdem eure Familie betrogen wurde.

<cite></cite>

* * *



* * *