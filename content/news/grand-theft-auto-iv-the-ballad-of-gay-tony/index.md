---
title: 'Grand Theft Auto IV: The Ballad of Gay Tony – Release 29. Oktober'
author: gamevista
type: news
date: 2009-07-23T15:18:21+00:00
excerpt: '<p>[caption id="attachment_136" align="alignright" width=""]<img class="caption alignright size-full wp-image-136" src="http://www.gamevista.de/wp-content/uploads/2009/07/gta4_small.jpg" border="0" alt="Grand Theft Auto 4 - The Ballad of Gay Tony" title="GTA IV - The Ballad of Gay Tony" align="right" width="140" height="100" />GTA IV - The Ballad of Gay Tony[/caption]<strong><font color="#000000"><span style="font-style: normal; font-weight: normal">Die zweite Epidsode für die Xbox 360 Fassung von <strong>Grand Theft Auto IV</strong> erscheint am 29. Oktober. Dies Kündigte <a href="http://www.rockstargames.com" target="_blank" title="rockstar games">Rockstar </a>nun offiziell an. </span></font></strong><span style="font-style: normal; font-weight: normal">In <strong>The Ballad of Gay Tony</strong> wird der Spieler die Figur Luis Lopez steuern, dieser arbeitet für den Nachtclubbesitzer Tony Prince. Konkrete Hinweise auf die Story gibt es bisher nicht.<br /></span></p>'
featured_image: /wp-content/uploads/2009/07/gta4_small.jpg

---
<span style="font-style: normal; font-weight: normal">Wenn ihr noch nicht <strong>The Lost and The Damned</strong> besitzt oder die Downloadinhalte nicht übers Internet beziehen wollt könnt ihr euch auch zum gleichen Zeitpunkt die im Handel erscheinende <strong>GTA IV: Episodes from Liberty City</strong> für 40 Euro ersteigern. Diese Disc beinhaltet dann beide Episoden und ist ohne Hauptspiel nutzbar.</span> 





