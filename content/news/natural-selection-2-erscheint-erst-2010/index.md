---
title: Natural Selection 2 – Erscheint erst 2010
author: gamevista
type: news
date: 2009-11-03T15:44:32+00:00
excerpt: '<p>[caption id="attachment_989" align="alignright" width=""]<img class="caption alignright size-full wp-image-989" src="http://www.gamevista.de/wp-content/uploads/2009/11/ns2_marine_hallway_small.jpg" border="0" alt="Natural Selection 2" title="Natural Selection 2" align="right" width="140" height="100" />Natural Selection 2[/caption]Nachdem der Entwickler <a href="http://www.unknownworlds.com/ns2/" target="_blank">Unknown Worlds Entertainment</a> vor kurzem erst neue Screenhots und Details zum Shooter-und Strategiemix <strong>Natural Selection 2</strong> veröffentlicht hat meldet sich dieser im Rahmen eines Interviews erneut zu Wort.</p> '
featured_image: /wp-content/uploads/2009/11/ns2_marine_hallway_small.jpg

---
Charlie Cleveland, Chef von <a href="http://www.unknownworlds.com/ns2/" target="_blank">Unknown Worlds</a>, erklärt darin das sie die geplante Veröffentlichung im Herbst 2009 definitiv nicht einhalten können und somit den Release auf das Frühjahr 2010 verschieben müssen. Weiterhin wird verkündet das man das Spiel mindestens ein Jahr lang Unterstützen würde in Form von Patches und Updates. Hier sind neuen Karten und Waffen und auch neue Spielmodi denkbar. Auch wird über Clan-Support und einen so genannten Beobachtungsmodus nachgedacht.

 

* * *



* * *