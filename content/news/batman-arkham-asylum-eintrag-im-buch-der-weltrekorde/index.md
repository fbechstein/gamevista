---
title: 'Batman: Arkham Asylum – Eintrag im Buch der Weltrekorde'
author: gamevista
type: news
date: 2009-08-29T19:24:32+00:00
excerpt: '<p>[caption id="attachment_45" align="alignright" width=""]<img class="caption alignright size-full wp-image-45" src="http://www.gamevista.de/wp-content/uploads/2009/07/batman_small.png" border="0" alt="Batman: Arkham Asylum" title="Batman: Arkham Asylum" align="right" width="140" height="100" />Batman: Arkham Asylum[/caption]Vor kurzem erst erschien das neue Actionspiel <strong>Batman: Arkham Asylum</strong> rund um den Fledermaushelden. Heute traf eine Nachricht ein die man auch nicht alle Tage hört. Denn damit ist <strong>Batman</strong> nun eindeutig besser als Superman. Der dunkle Ritter wurde nun mit einem Eintrag ins <strong>Guinness Buch der Rekorde</strong> gewürdigt. Der Grund dafür dürften die weltweiten Rekordwertungen sein.</p> '
featured_image: /wp-content/uploads/2009/07/batman_small.png

---
Denn das Action-Spiel ist das von Kritikern am besten bewertete Superheldenspiel aller Zeiten. Mit einer Wertung, die im Durchschnitt die 90 Prozent Marke übertrifft, ist **Batman: Arkham Asylum** wohl einer der ganz großen Spielehits dieses Jahr.

[> Zur Demo][1]

* * *



* * *

 

 [1]: downloads/demos/item/demos/batman-arkham-asylum-demo