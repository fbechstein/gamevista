---
title: Lara Croft and the Guardian of Light – Koop-Modus auf später verschoben
author: gamevista
type: news
date: 2010-09-21T16:23:21+00:00
excerpt: '<p>[caption id="attachment_2035" align="alignright" width=""]<img class="caption alignright size-full wp-image-2035" src="http://www.gamevista.de/wp-content/uploads/2010/07/laracroft_gol.jpg" border="0" alt="Lara Croft and the Guardian of Light" title="Lara Croft and the Guardian of Light" align="right" width="140" height="100" />Lara Croft and the Guardian of Light[/caption]<a href="http://www.laracroftandtheguardianoflight.com" target="_blank">Lara Croft and the Guardian of Light</a> wird zwar nächste Woche über PSN und Steam veröffentlicht, leider aber wird der heiß ersehnte Koop-Modus nicht mit von der Partie sein. Dies verkündete der Entwickler <strong>Crystal Dynamics</strong> im Rahmen einer offiziellen Pressemitteilung.</p> '
featured_image: /wp-content/uploads/2010/07/laracroft_gol.jpg

---
Der lokale Koop-Modus solll dennoch bereits am 28. September 2010 funktionieren. Online müsst ihr euch noch mit der KI begnügen.

 

   

* * *

   

