---
title: 'Call of Duty: Black Ops – Details zur Collector’s Edition'
author: gamevista
type: news
date: 2010-08-13T07:00:29+00:00
excerpt: '<p>[caption id="attachment_1747" align="alignright" width=""]<img class="caption alignright size-full wp-image-1747" src="http://www.gamevista.de/wp-content/uploads/2010/05/callofduty7.jpg" border="0" alt="Call of Duty: Black Ops" title="Call of Duty: Black Ops" align="right" width="140" height="100" />Call of Duty: Black Ops[/caption]Publisher <strong>Activision Blizzard</strong> schickt auch für den siebten Teil der Call of Duty-Reihe eine Sammleredition ins Gefecht. Zum einen gibt es die Prestige Edition die die Spezialedition von <strong>Modern Warfare 2</strong>, mit Nachtsichtgerät, noch einmal toppen dürfte.</p> '
featured_image: /wp-content/uploads/2010/05/callofduty7.jpg

---
Zum anderen bietet die **Hardened Edition** eine limitierte Black Ops-Medaille, einen exklusiven Avatar-Outfit, sowie vier spielbare Karten für den Koop-Modus.   
Der Prestige Edition von <a href="http://www.callofduty.com" target="_blank">Call of Duty: Black Ops</a>, die auch die Features der Hardened Edition beinhaltet, wird ein RC-XD Surveillance Vehicle beiliegen. Dieses kleine Gefährt wird über eine Fernbedienung gesteuert und übermittelt dabei Video- sowie Audiodaten an seinen Benutzer. Dies erfolgt über einen 2“ Zoll-Display mit 220&#215;176 Pixeln und einem Mikrofon.   
Dabei soll die Reichweite bis zu 70 Metern liegen. Bisher kündigte der Publisher die Prestige- und Hardened Edition lediglich für die Xbox 360- und PlayStation 3-Versionen an. Ob PC Spieler auch in den Genuss des Vehikels kommen werden ist noch unklar. <a href="http://www.callofduty.com" target="_blank">Call of Duty: Black Ops</a> erscheint am 9. November 2010 für Xbox 360, PlayStation3, Nintendo Wii, DS und PC.

   

* * *

   



 