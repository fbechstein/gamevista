---
title: Crysis 2 – Verschoben auf 2011
author: gamevista
type: news
date: 2010-08-04T19:43:43+00:00
excerpt: '<p>[caption id="attachment_161" align="alignright" width=""]<img class="caption alignright size-full wp-image-161" src="http://www.gamevista.de/wp-content/uploads/2009/07/crysis2_small.jpg" border="0" alt="Crysis 2" title="Crysis 2" align="right" width="140" height="100" />Crysis 2[/caption]Publisher <strong>Electronic Arts</strong> hat im Rahmen einer Geschäftskonferenz unter anderem <strong>Need for Speed SHIFT 2</strong> angekündigt und den Ego-Shooter <a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a> auf das Jahr 2011 verschoben. Damit wird der lang erwartete zweite Teil nicht mehr dieses Jahr zum Weihnachtsgeschäft erhältlich sein.</p> '
featured_image: /wp-content/uploads/2009/07/crysis2_small.jpg

---
Wenn alles glatt läuft, so EA, soll der Titel spätestens im März 2011 erscheinen.  Als Grund nannte der Publisher das weitere finale Arbeiten an <a href="http://www.ea.com/games/crysis-2" target="_blank">Crysis 2</a> nötig seien.

 

   

* * *

   

