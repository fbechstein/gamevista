---
title: 'Fallout: New Vegas – Termin für Dead Money-DLC'
author: gamevista
type: news
date: 2011-02-06T20:22:18+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Entwickler <strong>Bethesda </strong>hat den Termin für das Erweiterungspaket <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> - Dead Money angekündigt. Am 22. Februar 2011 wird der Download-Content für PlayStation 3 und PC veröffentlicht. Bisher ist der Titel exklusiv für Xbox 360 für 800 Microsoft-Punkte erhältlich.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Die Geschichte führt den Spieler zum Sierra Madre Kasino wo ihn zahlreiche Reichtümer erwarten.

 

* * *



* * *