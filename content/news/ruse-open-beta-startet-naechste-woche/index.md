---
title: R.U.S.E. – Open Beta startet nächste Woche
author: gamevista
type: news
date: 2010-03-04T16:43:19+00:00
excerpt: '<p>[caption id="attachment_1518" align="alignright" width=""]<img class="caption alignright size-full wp-image-1518" src="http://www.gamevista.de/wp-content/uploads/2010/03/ruse_small.jpg" border="0" alt="R.U.S.E." title="R.U.S.E." align="right" width="140" height="100" />R.U.S.E.[/caption]Der Publisher <strong>Ubisoft </strong>hat im Rahmen eines Beitrags im <a href="http://forums-de.ubi.com/eve/forums?a=tpc&s=59010161&f=9981008247&m=8331028638&r=1261018638#1261018638" target="_blank">offiziellen Forum</a> des Strategiespiels <a href="http://www.rusegame.com" target="_blank">R.U.S.E.</a>, eine offene Betaphase für die nächste Woche angekündigt. Demnach werden alle Fans des 2. Weltkriegspiels ab dem 9. März in den Genuss einer mehrtätigen Testphase kommen. Wie lange die Open-Beta verfügbar sein wird ist nicht bekannt. Über die Onlineplattform Steam wird ab Dienstag der Client zum Herunterladen bereitstehen.</p> '
featured_image: /wp-content/uploads/2010/03/ruse_small.jpg

---
Passend dazu veröffentlichte **Ubisoft** heute ein neues Video, das die Verbesserungen zeigt die nach der Closed-Beta dem Spiel hinzugefügt wurden. Weitere Details wie Systemanforderungen und Inhalt der Beta folgen in Kürze.

 

* * *



* * *