---
title: EA – Publisher schaltet etliche Server ab
author: gamevista
type: news
date: 2010-01-04T20:30:10+00:00
excerpt: '<p>[caption id="attachment_1264" align="alignright" width=""]<img class="caption alignright size-full wp-image-1264" src="http://www.gamevista.de/wp-content/uploads/2010/01/ea_logo.jpg" border="0" alt="Electronic Arts" title="Electronic Arts" align="right" width="100" height="100" />Electronic Arts[/caption]Der Publisher <a href="http://www.ea.com/2/service-updates" target="_blank">Electronic Arts</a> hat auf seiner Website verkündet, dass zum 2. Februar 2010 einige Server für ältere Spiele abgeschaltet werden würden. Folglich wird für diese Spiele kein Onlinemodus mehr verfügbar sein. Natürlich ist es verständlich das der Support alter Spiele nicht ewig anhalten kann.</p> '
featured_image: /wp-content/uploads/2010/01/ea_logo.jpg

---
Leider sind auf der Liste auch Titel mit 09-er Kennung, also Spiele die im Jahre 2008 veröffentlich wurden. Die komplette Liste der betroffenen Spiele könnt ihr hier Nachlesen:

 

**February 9****, 2010 Online Service Shutdown  
** UEFA Champions League 07 PC  
FIFA 07 PC  
Madden 08 PC  
NHL 08 PC   
Tiger Woods 07 PC

**February 2****, 2010 Online Service Shutdown**   
UEFA Champions League 07 x360    
Facebreaker x360 and PS3    
Fantasy Football 09 x360 and PS3    
FIFA 07 PSP, PS2   
Fight Night Round 3 PS2    
Madden 08 Wii    
Madden 09 Xbox1    
Madden 09 Wii and PSP    
March Madness 07 x360    
NBA 07 PSP, x360    
NBA 08 PS2, PSP, Wii    
NBA 09 Wii &#8211; Europe only    
NBA Street (2007) PS3 and x360    
NCAA Football 08 PS2    
NCAA Football 09 PS2    
NASCAR 08 PS2    
NASCAR 09 PS2    
NASCAR 09 PS3 and x360 &#8211; Europe Only    
NFL Tour PS3 and x360    
NHL 07 PSP and x360    
Madden 09 x360 and PS3    
Madden 07 Xbox 360

* * *



* * *

 