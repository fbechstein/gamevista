---
title: Heavy Rain – Demo Termin bestätigt
author: gamevista
type: news
date: 2010-01-21T21:43:35+00:00
excerpt: '<p>[caption id="attachment_1338" align="alignright" width=""]<img class="caption alignright size-full wp-image-1338" src="http://www.gamevista.de/wp-content/uploads/2010/01/heavyrain.jpg" border="0" alt="Heavy Rain" title="Heavy Rain" align="right" width="140" height="100" />Heavy Rain[/caption]Mike Kebby hat heute in einem veröffentlichten <a href="http://blog.eu.playstation.com/2010/01/21/%E2%80%98heads-up%E2%80%99-playstation-store-update-21st-january-2010/comment-page-4/#comment-52228">Beitrag</a>, der im offiziellen Playstation-Blog verfasst wurde, die bald folgenden Updates für den Playstation Store verkündet. So wird es ab dem 11. Februar 2010 möglich sein, die Demo vom Heavy Rain herunter zu laden.</p> '
featured_image: /wp-content/uploads/2010/01/heavyrain.jpg

---
Die Vollversion von <a href="http://www.quanticdream.com" target="_blank">Heavy Rain</a> wird mit Spannung zum 24. Februar 2010 erwartet. Da freut es doch die Demo bereits zwei Wochen früher anspielen zu dürfen.

 

 

* * *



* * *