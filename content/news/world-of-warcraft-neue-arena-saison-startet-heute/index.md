---
title: World of Warcraft – Neue Arena Saison startet heute
author: gamevista
type: news
date: 2009-09-02T07:35:35+00:00
excerpt: '<p>[caption id="attachment_588" align="alignright" width=""]<img class="caption alignright size-full wp-image-588" src="http://www.gamevista.de/wp-content/uploads/2009/09/wow_small2.png" border="0" alt="World of Warcraft" title="World of Warcraft" align="right" width="140" height="100" />World of Warcraft[/caption]Sobald die Wartungsarbeiten beendet sind wird heute die nunmehr siebte Arena Saison für das Online Rollenspiel <strong>World of Warcraft</strong> starten. Diesmal habt ihr wieder die Möglichkeit allein oder mit Freunden auf der frischen Rangliste euch nach oben zu spielen. Weiterhin wird es eine kleine Änderung geben.</p> '
featured_image: /wp-content/uploads/2009/09/wow_small2.png

---
Es wird demnach nicht mehr möglich sein die neusten Waffen bzw. Schulterstücke allein durch eine hohe 2vs2 Wertung zu erhalten. Nun wird es nötig sein auch im 3vs3 und 5vs5 zu spielen um wirklich alle Waffen und Rüstungen zugänglich zu machen.

 

* * *



* * *