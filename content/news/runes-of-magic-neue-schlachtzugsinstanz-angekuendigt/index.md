---
title: Runes of Magic – Neue Schlachtzugsinstanz angekündigt
author: gamevista
type: news
date: 2009-09-10T19:21:08+00:00
excerpt: '<p>[caption id="attachment_149" align="alignright" width=""]<img class="caption alignright size-full wp-image-149" src="http://www.gamevista.de/wp-content/uploads/2009/07/runes_of_magic_small.jpg" border="0" alt="Runes of Magic" title="Runes of Magic" align="right" width="140" height="100" />Runes of Magic[/caption]Publisher <a href="http://www.runesofmagic.com">Frogster Interactive</a> hat heute per offizieller Pressemitteilung mitgeteilt das es eine neue Schlachtzugsinstanz für das Online Rollenspiel <strong>Runes of Magic</strong> geben wird. Der neue Dungeon Namens „<strong>Die Halle der Überlebenden</strong>“ wird mit dem zweiten Kapitel „<strong>The Elven Prophecy</strong>“  geöffnet.</p> '
featured_image: /wp-content/uploads/2009/07/runes_of_magic_small.jpg

---
Es wird möglich sein mit einer Schlachtzugsgruppe, die eine größe von maximal 12 Spielern haben wird,  „**Die Halle der Überlebenden**“ zu erkunden. Als Monstertypen wird es unter anderem Golems zu sehen geben und insgesamt acht Bossgegner warten darauf besiegt zu werden. Weiterhin gibt es ein Trailer zur neuen Raid Instanz. Wie immer findet ihr diesen bei uns im Videobereich.

[> Zum Runes of Magic &#8211; Die Halle der Überlebenden Trailer][1]

 

 

* * *



* * *

 [1]: videos/item/root/runes-of-magic-die-hallen-der-ueberlebenden