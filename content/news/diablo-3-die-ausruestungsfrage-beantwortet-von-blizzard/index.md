---
title: Diablo 3 – Die Ausrüstungsfrage beantwortet von Blizzard
author: gamevista
type: news
date: 2009-09-07T20:26:30+00:00
excerpt: '<p>[caption id="attachment_456" align="alignright" width=""]<img class="caption alignright size-full wp-image-456" src="http://www.gamevista.de/wp-content/uploads/2009/08/diablo3_small.jpg" border="0" alt="Diablo 3" title="Diablo 3" align="right" width="140" height="100" />Diablo 3[/caption]Bashiok ist ein Mitarbeiter von <a href="http://www.eu.blizzard.com/de/">Blizzard Entertainment</a>. Dieser hat im <a href="http://forums.battle.net/thread.html?topicId=19595522940&pageNo=1#15">offiziellen Forum</a> von <a href="http://www.eu.blizzard.com/de/">Blizzard </a>das Thema Ausrüstung des Action Adventures <strong>Diablo 3</strong> aufgegriffen und einige Dinge klargestellt. Zum ersten wäre da die Frage ob es so genannte „Bind on Pickup“ Gegenstände geben wird.</p> '
featured_image: /wp-content/uploads/2009/08/diablo3_small.jpg

---
Wie Bashiok erklärte wird es in **Diablo 3** keine Gegenstände geben die beim Aufheben an den Charakter gebunden werden. Ausnahmen gibt es natürlich auch hier. Denn Questgegenstände sind nach wie vor nur für den Charakter verfügbar der auch die Quest hat.    
Momentan ist es geplant das es Rüstungen so wie Waffen geben wird die beim Anlegen an den Charakter gebunden werden. Man kann natürlich bevor man sie anzieht frei Handeln. Eine weitere Idee gibt es auch. So beschäftigt sich das [Blizzard Team][1] mit dem Gedanken Account gebundene Gegenstände einzuführen. Diese könnt ihr dann von euren Charakteren frei nutzen lassen und sie so von Charakter A zu Charakter B transferieren.

 

* * *



* * *

 [1]: http://www.eu.blizzard.com/de/