---
title: 'Dawn of War 2: Chaos Rising – Erster Teaser zum Addon'
author: gamevista
type: news
date: 2009-09-28T20:24:24+00:00
excerpt: '<p>[caption id="attachment_783" align="alignright" width=""]<img class="caption alignright size-full wp-image-783" src="http://www.gamevista.de/wp-content/uploads/2009/09/1_1280x800_small.jpg" border="0" alt="Dawn of War 2" title="Dawn of War 2" align="right" width="140" height="100" />Dawn of War 2[/caption]Endlich hat das Entwicklerteam rund um <a href="http://www.relic.com">Relic </a>erste bewegte Bilder zum Addon <strong>Dawn of War 2: Chaos Rising</strong> veröffentlicht. Der kurze Teaser zeigt euch das neue Setting in eisiger Umgebung. Das Addon <strong>Dawn of War 2: Dragon Rising</strong> erscheint im Frühjahr 2010.</p> '
featured_image: /wp-content/uploads/2009/09/1_1280x800_small.jpg

---
Die wichtigste Neuerung wird wohl die Klasse der Chaos Space Marines sein. Mit dieser zusätzlich spielbaren Armee  führt ihr den epischen Feldzug der Blood Ravens fort.  
[  
> Zum Dawn of War 2: Chaos Rising][1]

* * *



* * *

 

 [1]: videos/item/root/dawn-of-war-2-chaos-rising-teaser