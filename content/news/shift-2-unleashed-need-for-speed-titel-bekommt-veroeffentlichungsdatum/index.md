---
title: 'Shift 2: Unleashed – Need for Speed-Titel bekommt Veröffentlichungsdatum'
author: gamevista
type: news
date: 2011-01-13T17:20:00+00:00
excerpt: '<p>[caption id="attachment_2510" align="alignright" width=""]<img class="caption alignright size-full wp-image-2510" src="http://www.gamevista.de/wp-content/uploads/2011/01/shft2ugenscrngt1_04.jpg" border="0" alt="Shift 2: Unleashed" title="Shift 2: Unleashed" align="right" width="140" height="100" />Shift 2: Unleashed[/caption]Publisher <strong>Electronic Arts</strong> hat den Veröffentlichungstermin für den Nachfolger von Need for Speed Shift bekannt gegeben. Demnach wird <a href="http://www.ea.com/de/spiele/need-for-speed-shift" target="_blank">Need for Speed Shift 2: Unleashed</a> in Europa am 24. März 2011 in den Handel kommen. <a href="http://www.ea.com/de/spiele/need-for-speed-shift" target="_blank">Shift 2 Unleashed</a> wird für PC, Xbox 360 und PlayStation 3 entwickelt, passend zu der Ankündigung spendiert der Publisher noch einen neuen Trailer zum Rennspiel.</p> '
featured_image: /wp-content/uploads/2011/01/shft2ugenscrngt1_04.jpg

---
[> Zum Shift 2: Unleashed &#8211; Trailer][1]

* * *



* * *

 [1]: videos/item/root/shift-2-unleashed-trailer