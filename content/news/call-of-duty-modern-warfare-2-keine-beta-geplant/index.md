---
title: 'Call of Duty: Modern Warfare 2 – Keine Beta geplant'
author: gamevista
type: news
date: 2009-08-20T11:14:55+00:00
excerpt: '<p>[caption id="attachment_111" align="alignright" width=""]<img class="caption alignright size-full wp-image-111" src="http://www.gamevista.de/wp-content/uploads/2009/07/codmw2_small.png" border="0" alt="Call of Duty: Modern Warfare 2" title="Call of Duty: Modern Warfare 2" align="right" width="140" height="100" />Call of Duty: Modern Warfare 2[/caption]Jeder kennt sie jeder liebt sie, Beta Tests. Vor allem wenn es um solche Highlights wie <strong>Call of Duty</strong> geht. Immerhin macht der Multiplayermodus einen Großteil des Spiels aus, deswegen waren die Multiplayer Betas von <strong>Call of Duty</strong> in der Vergangenheit immer eine gute Gelegenheit das Spiel auf Herz und Nieren zu testen.</p> '
featured_image: /wp-content/uploads/2009/07/codmw2_small.png

---
Leider entschied sich Entwickler [Infinity Ward][1] im Rahmen von Modern Warfare 2 keinen öffentlichen Betatest anzubieten. Sie wollen das Spiel lieber intern testen, dies gab Robert Bowling bekannt.

 

* * *



* * *

 

 

 

 [1]: http://www.infinityward.com