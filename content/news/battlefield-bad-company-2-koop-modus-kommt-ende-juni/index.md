---
title: 'Battlefield: Bad Company 2 – Koop-Modus kommt Ende Juni'
author: gamevista
type: news
date: 2010-06-10T18:53:27+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Publisher <strong>Electronic Arts</strong> hat zusammen mit dem Entwicklerteam von <strong>DICE </strong>im Rahmen einer Pressemitteilung den Veröffentlichtungstermin für den Onslaught-Modus bekannt gegeben. Der neue Spielmodus für den Ego-Shooter <a href="http://www.badcompany2.ea.com" target="_blank">Battlefield: Bad Company 2</a> wird bereits Ende Juni auf die Konsolen kommen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Bis zu vier Spieler können ab dem 22. Juni auf der Xbox 360 und ab dem 23. Juni auf der PlayStation 3 gegen computergesteuerte Soldaten antreten. Kostenpunkt werden 8,99 Euro für die PS3- und 800 Microsoft-Punkte für die Xbox 360-Version sein. Neben Achievements wird  es auch eine Onslaught Rangliste geben.

Für Neueinsteiger gibt es das Angebot für 560 Microsoft-Punkte bzw. 6,49 Euro die Waffen der jeweiligen Klasse freizuschalten. Für 17,99 Euro bzw. 1600 Microsoft-Punkte gibt es sofort alle Waffen zu bestaunen. Diese Funktion steht ab dem 16. Juni auf dem Xbox Live Marktplatz sowie im PlayStation Netzwerk zur Verfügung.

   

* * *

   

