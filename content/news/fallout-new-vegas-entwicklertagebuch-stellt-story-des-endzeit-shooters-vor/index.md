---
title: 'Fallout: New Vegas – Entwicklertagebuch stellt die Story vor'
author: gamevista
type: news
date: 2010-09-19T09:15:41+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Das Entwicklerteam von <strong>Obsidian Entertainment</strong> stellt den ersten Teil der Entwicklertagebuch-Reihe in Form eines neuen Videos vor. Im neuen Trailer zum Endzeit-Shooter <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> zeigen die Entwickler die Handlungsorte und erklären die Hintergrundgeschichte.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Dabei wurde die Wüstenmetropole Las Vegas als Vorbild genommen und in eine postapokalyptische Zeit versetzt. Das Rollenspiel im Fallout-Universum wird am 22. Oktober 2010 für PC, PlayStation 3 und Xbox 360 in den Handel kommen.

[> Zum Fallout: New Vegas &#8211; Entwicklertagebuch #1][1]

   

* * *

   



 [1]: videos/item/root/fallout-new-vegas-story-trailer