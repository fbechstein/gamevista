---
title: 'Fallout: New Vegas – Bethesda stellt die Begleiter im Spiel vor'
author: gamevista
type: news
date: 2010-09-15T17:07:26+00:00
excerpt: '<p>[caption id="attachment_1418" align="alignright" width=""]<img class="caption alignright size-full wp-image-1418" src="http://www.gamevista.de/wp-content/uploads/2010/02/fallout_newvegas.jpg" border="0" alt="Fallout: New Vegas" title="Fallout: New Vegas" align="right" width="140" height="100" />Fallout: New Vegas[/caption]Matt Grandstaff, seines Zeichens Senior Community Manager von <strong>Bethesda, </strong>hat weitere Details zum Endzeit-Rollenspiel <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> bekannt gegeben. Wie im Vorgänger <strong>Fallout 3 </strong>wird es wieder möglich sein zahlreiche Begleiter an seiner Seite zu haben. Darunter sind neben Menschen auch Super Mutanten und Ghoule.</p> '
featured_image: /wp-content/uploads/2010/02/fallout_newvegas.jpg

---
Darunter finden sich die Charaktere Arcade, Boone, Cass, Lily, Raul und Veronica. Auch wird es wieder Roboter als treue Gefährten geben. Darunter sind ED-E, ein modifizierter Eyebot und Rex the Cyber-Hound. <a href="http://fallout.bethsoft.com/" target="_blank">Fallout: New Vegas</a> wird am 22. Oktober 2010 für PC, PlayStation 3 und Xbox 360 in den Handel kommen. Weitere Details findet ihr auf dem <a href="http://blog.us.playstation.com/2010/09/15/fallout-new-vegas-for-ps3-meet-the-companions/?utm_source=feedburner&#038;utm_medium=feed&#038;utm_campaign=Feed%3A+PSBlog+%28PlayStation.Blog%29" target="_blank">US Playstation Blog</a>.

 

   

* * *

   

