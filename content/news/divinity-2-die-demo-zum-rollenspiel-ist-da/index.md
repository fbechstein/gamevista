---
title: Divinity 2 – Die Demo zum Rollenspiel ist da
author: gamevista
type: news
date: 2009-12-18T19:50:43+00:00
excerpt: '<p>[caption id="attachment_310" align="alignright" width=""]<img class="caption alignright size-full wp-image-310" src="http://www.gamevista.de/wp-content/uploads/2009/08/divinity2.png" border="0" alt="Divinity 2" title="Divinity 2" align="right" width="140" height="100" />Divinity 2[/caption]Der Publisher <a href="http://www.divinity2.com" target="_blank">dtp Entertainment</a> hat kurz vor den Feiertagen die Demo zum Rollenspiel <strong>Divinity 2</strong> veröffentlicht. Damit steht ab sofort eine PC-Demo, zum bereits im Juli 2009 erschienenen Rollenspiel, kostenlos zum Download bereit. Die Probierversion mit einer Größe von 1,8 Gigabyte, enthält das Tutorial und einige Quests die zum ausgiebigen Testen einladen sollen.</p> '
featured_image: /wp-content/uploads/2009/08/divinity2.png

---
In **Divinity 2** wird der Spieler im laufe der Geschichte vom Drachentöter zum Drachenritter und später selbst zum Drachen verwandelt.

[> Zum Divinity 2 &#8211; Demo Download][1]

 

* * *



* * *

 

 [1]: downloads/demos/item/demos/divinity-2-ego-draconis-demo