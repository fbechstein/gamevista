---
title: 'Red Dead Redemption  – Rockstar dementiert PC-Version'
author: gamevista
type: news
date: 2010-06-11T12:52:34+00:00
excerpt: '<p>[caption id="attachment_1908" align="alignright" width=""]<img class="caption alignright size-full wp-image-1908" src="http://www.gamevista.de/wp-content/uploads/2010/06/rdr.jpg" border="0" alt="Red Dead Redemption " title="Red Dead Redemption " align="right" width="140" height="100" />Red Dead Redemption [/caption]Das Western-GTA <a href="http://www.rockstargames.com/reddeadredemption" target="_blank">Red Dead Redemption</a> ist nun seit einiger Zeit für Konsolen erhältlich. PC-Spieler hatten bisher hoffnungsvoll auf eine PC-Umsetzung gewartet. Leider hat das Entwicklerteam <strong>Rockstar</strong> dieser Hoffnung nun ein Ende bereitet. Laut einer offiziellen Meldung vom Entwickler ist derzeit keine PC-Fassung von <a href="http://www.rockstargames.com/reddeadredemption" target="_blank">Read Dead Redemption</a> geplant.</p> '
featured_image: /wp-content/uploads/2010/06/rdr.jpg

---
„Bisher gibt es keine aktuellen Pläne Red Dead Redemption für PC zu entwickeln. Falls sich dies ändert lassen wir es euch wissen“, so ein Mitarbeiter von **Rockstar**.

 

   

* * *

   

