---
title: Steam – Alle Strategy First Games 50% günstiger
author: gamevista
type: news
date: 2009-09-03T08:59:10+00:00
excerpt: '<p>[caption id="attachment_590" align="alignright" width=""]<img class="caption alignright size-full wp-image-590" src="http://www.gamevista.de/wp-content/uploads/2009/09/jagged-alliance-2.jpg" border="0" alt="Jagged Alliance 2" title="Jagged Alliance 2" align="right" width="140" height="100" />Jagged Alliance 2[/caption]Der <a href="http://store.steampowered.com/">Steamshop</a> bietet euch diese Woche wieder jede Menge Schnäppchen an. So könnt ihr bis zum 7. September im Valve Onlineshop <a href="http://store.steampowered.com/">Steam</a> alle Spiele des Publishers <a href="http://www.strategyfirst.com">Strategy First</a> zum halben Preis bekommen. Zwar sind schon einige Games etwas älter, aber den ein oder anderen Schatz findet man ja vielleicht doch.</p> '
featured_image: /wp-content/uploads/2009/09/jagged-alliance-2.jpg

---
Hier die Liste mit den Spielen von [Strategy First][1]:

 

  * Jack Keane
  * Sacred Gold
  * SlamIt Pinball: Big Score
  * Ankh &#8211; Heart of Osiris
  * Ankh &#8211; Battle of the Gods
  * The Ankh Pack
  * Port Royale 2
  * FlatOut
  * Vegas &#8211; Make It Big
  * DarkStar One
  * Making History: The Calm and The Storm
  * Ducati World Championship
  * Space Empires IV-Pack
  * Space Empires V-Pack
  * Culpa Innata
  * Exodus From the Earth
  * Perimeter 2
  * Ghost Master
  * Alien Shooter: Vengeance
  * SFI Complete Pack
  * Complete Naval Combat Pack
  * Classic Naval Combat Pack
  * Iron Warriors
  * Space Empires V
  * Birth of America
  * Jagged Alliance 2 / Disciples II Gold Combo
  * Disciples II Gold
  * Jagged Alliance 2
  * Dangerous Waters
  * Space Empires IV Deluxe
  * BC Kings

 

 

* * *



* * *

 [1]: http://www.strategyfirst.com