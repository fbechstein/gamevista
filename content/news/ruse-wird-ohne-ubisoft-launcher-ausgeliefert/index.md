---
title: R.U.S.E. – Wird ohne Ubisoft-Launcher ausgeliefert
author: gamevista
type: news
date: 2010-08-12T17:17:59+00:00
excerpt: '<p>[caption id="attachment_2122" align="alignright" width=""]<img class="caption alignright size-full wp-image-2122" src="http://www.gamevista.de/wp-content/uploads/2010/08/ruse_small.jpg" border="0" alt="R.U.S.E. " title="R.U.S.E. " align="right" width="140" height="100" />R.U.S.E. [/caption]Der nervige Kopierschutz in Form des Ubisoft-Launchers wird dem bald erscheinenden Strategiespiel <a href="http://ruse.de.ubi.com/index.php?page=news&newsid=11781" target="_blank">R.U.S.E.</a> erspart bleiben. Dies teilte der Publisher auf der <a href="http://ruse.de.ubi.com/index.php?page=news&newsid=11781" target="_blank">offiziellen Website</a> des Spiels mit. Zwar wird ein Steam-Account Voraussetzung für die Installation des Spiels sein.</p> '
featured_image: /wp-content/uploads/2010/08/ruse_small.jpg

---
Eine permanente Internetverbindung wie z.B. beim Strategiespiel **Die Siedler 7** wird daher aber nicht benötigt. Der Kopierschutz machte gerade beim misslungenen Start von **Die Siedler 7** bzw. **Silent Hunter 5** negative Schlagzeilen. Durch ständige Offline-Zeiten des Ubisoft-Launch-Servers war es nicht möglich den Singleplayer-Part der beiden Titel zu spielen. **Ubisoft** teilt aber gleichzeitig mit, dass dies nicht das Ende des Launcher-Systems sei.

   

* * *

   



 