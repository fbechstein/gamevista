---
title: Blur – Verschoben auf 2010
author: gamevista
type: news
date: 2009-09-25T17:29:12+00:00
excerpt: '<p>[caption id="attachment_769" align="alignright" width=""]<img class="caption alignright size-full wp-image-769" src="http://www.gamevista.de/wp-content/uploads/2009/09/blur.jpg" border="0" alt="Blur" title="Blur" align="right" width="140" height="100" />Blur[/caption]Wie Publisher <a href="http://www.activision.com">Activision </a>heute per Pressemitteilung übermittelt hat wird das Rennspiel Blur auf das Jahr 2010 verschoben. Der Grund dafür ist das die Entwickler <a href="http://www.bizarrecreations.com">Bizzare Creations</a> mehr Zeit brauchen um den Online Modus fertig zu stellen.</p> '
featured_image: /wp-content/uploads/2009/09/blur.jpg

---
Das auf den Mehrspieler Part abzielende Rennspiel will durch ein Community basierendes Interface, eine Art Social Network aufbauen. Hier könnt ihr gegen Freunde und Fans von **Blur** antreten und mit Teambasierten Mehrspielerrennen mit bis zu 20 Teilnehmern euer Können beweisen. **Blur** erscheint 2010 für Xbox 360, PlayStation 3 und PC.

 

* * *



* * *

 