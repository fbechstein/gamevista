---
title: Majesty 2 Demo veröffentlicht
author: gamevista
type: news
date: 2009-09-15T12:19:25+00:00
excerpt: '<p>[caption id="attachment_697" align="alignright" width=""]<img class="caption alignright size-full wp-image-697" src="http://www.gamevista.de/wp-content/uploads/2009/09/majesty2_small.png" border="0" alt="Majesty 2" title="Majesty 2" align="right" width="140" height="100" />Majesty 2[/caption]Der Publisher <a href="http://www.paradoxplaza.com/" target="_top">Paradox Interactive</a> hat die Demo zum Strategiespiel <strong>Majesty 2</strong> veröffentlicht. In der Demo könnt Ihr das Tutorial und zwei Missionen anspielen. Die 660 MB große Demo ist nur in englischer Sprache erhältlich und kann direkt von unserem Server heruntergeladen werden.</p> '
featured_image: /wp-content/uploads/2009/09/majesty2_small.png

---
[> Majesty 2 Demo herunterladen][1]



* * *

 



* * *

 [1]: index.php?Itemid=95&option=com_zoo&view=item&category_id=4&item_id=280