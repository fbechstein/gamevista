---
title: 'Battlefield: Bad Company 2 – Wird nicht an Steam gebunden sein'
author: gamevista
type: news
date: 2010-01-06T20:03:42+00:00
excerpt: '<p>[caption id="attachment_390" align="alignright" width=""]<img class="caption alignright size-full wp-image-390" src="http://www.gamevista.de/wp-content/uploads/2009/08/badcompany2logo.jpg" border="0" alt="Battlefield: Bad Company 2" title="Battlefield: Bad Company 2" align="right" width="140" height="100" />Battlefield: Bad Company 2[/caption]Laut einer offiziellen <strong>Battlefield: Bad Company 2</strong> Twitter Meldung wird der nächste Ego-Shooter der Battlefield-Reihe nicht an Steam gebunden sein. Zwar wird der Titel bei Steam erwerbbar sein aber man kann auch ohne den Online-Dienst auskommen.</p> '
featured_image: /wp-content/uploads/2009/08/badcompany2logo.jpg

---
Demnach wird **Battlefield: Bad Company 2** im normalen Handel erscheinen und ist damit auch ohne Steam-Account spielbar. Weiterhin wurde verkündet das die bald folgende Beta nicht exklusiv an Steam gekoppelt sein wird. 

_&#8222;If you buy the retail version of BFBC2 it&#8217;s not compatible with your steam account just like any other non Valve Retail PC game.&#8220;_(BFBC2-Twitter)”

**Battlefield: Bad Company 2** erscheimt am 05. März 2010 für Xbox 360, PC und PlayStation 3.

 

* * *



* * *

 