---
title: Borderlands – erreicht Goldstatus
author: gamevista
type: news
date: 2009-10-10T19:07:19+00:00
excerpt: '<p>[caption id="attachment_121" align="alignright" width=""]<img class="caption alignright size-full wp-image-121" src="http://www.gamevista.de/wp-content/uploads/2009/07/borderlands_small.jpg" border="0" alt="Borderlands" title="Borderlands" align="right" width="140" height="100" />Borderlands[/caption]Der Entwickler <a href="http://www.gearboxsoftware.com" target="_blank">Gearbox Software</a> hat per offizieller Pressemitteilung verlauten lassen das ihr Actionspiel <strong>Borderlands </strong>den Goldstatus erreicht hat. Das heißt das Spiel ist auf dem Weg in die Presswerke um bald darauf Verschifft zu werden und in den Handel zu kommen.</p> '
featured_image: /wp-content/uploads/2009/07/borderlands_small.jpg

---
Das Actionspiel geht neue Wege in Sachen Grafik und nutzt den Cell-Shading-Look um sich von der Konkurrenz abzuheben. Weiterhin werden viele Waffen bis ins unendliche Modifizierbar sein. **Borderlands** erscheint am 30. Oktober 2009 in Deutschland im Handel und über die Onlineplattform Steam.

 

 

* * *



* * *

 