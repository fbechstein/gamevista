---
title: Football Manager 2011 – SEGA veröffentlicht Demoversion
author: gamevista
type: news
date: 2010-10-24T16:09:30+00:00
excerpt: '<p>[caption id="attachment_2373" align="alignright" width=""]<img class="caption alignright size-full wp-image-2373" src="http://www.gamevista.de/wp-content/uploads/2010/10/fm2011.jpg" border="0" alt="Football Manager 2011" title="Football Manager 2011" align="right" width="140" height="100" />Football Manager 2011[/caption]Die Demo zur diesjährigen Fassung des <a href="http://www.footballmanager.com" target="_blank">Football Managers</a> wurde vor kurzem vom Publisher <strong>SEGA </strong>veröffentlicht. Diese lädt zum Testen der ersten Hälfte der Saison 2010/2011 ein. Neben England, stehen Schottland, Frankreich, Spanien, Portugal, Italien, Norwegen, Dänemark, Schweden, Brasilien, Argentinien und Chile als Schnellstart-Liga zur Verfügung.</p> '
featured_image: /wp-content/uploads/2010/10/fm2011.jpg

---
Die Demo zum <a href="http://www.footballmanager.com" target="_blank">Football Manager 2011</a> steht bei uns ab sofort zum Download bereit.

[> Zur Football Manager 2011 &#8211; Demo][1]

   

* * *

   



 [1]: downloads/demos/item/demos/football-manager-2011--demo