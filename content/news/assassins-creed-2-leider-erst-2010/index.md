---
title: Assassin’s Creed 2 – Leider erst 2010
author: gamevista
type: news
date: 2009-09-24T16:30:45+00:00
excerpt: '<p>[caption id="attachment_112" align="alignright" width=""]<img class="caption alignright size-full wp-image-112" src="http://www.gamevista.de/wp-content/uploads/2009/07/ac2_small.jpg" border="0" alt="Assassin’s Creed 2" title="Assassin’s Creed 2" align="right" width="140" height="100" />Assassin’s Creed 2[/caption]Schade Schade. Wie der Publisher <a href="http://www.ubi.com/de/">Ubisoft </a>nun offiziell bestätigte wird die PC Version des Actionspiels <strong>Assassin’s Creed 2</strong> leider nicht zusammen mit den Xbox 360 und PlayStation 3 Varianten erscheinen.</p> '
featured_image: /wp-content/uploads/2009/07/ac2_small.jpg

---
Der Veröffentlichungstermin liegt für die Konsolenversionen auf dem 17. November 2009.  
PC Spieler müssen sich nun auf eine doch sehr lange Wartezeit einstellen, denn **Assassin’s Creed 2** erscheint erst im Frühjahr 2010. Der Grund dürfte wie immer der Fokus auf die Konsolenversionen sein.

 

 

* * *



* * *

 