---
title: Patrizier 4 – Demo ab sofort verfügbar
author: gamevista
type: news
date: 2010-08-18T15:41:38+00:00
excerpt: '<p><strong>[caption id="attachment_1615" align="alignright" width=""]<img class="caption alignright size-full wp-image-1615" src="http://www.gamevista.de/wp-content/uploads/2010/03/patrizier4.jpg" border="0" alt="Patrizier 4" title="Patrizier 4" align="right" width="140" height="100" />Patrizier 4[/caption]Kalypso Media</strong> veröffentlichte vor kurzem die Demoversion der Wirtschaftssimulation <a href="http://www.patrizier4.de" target="_blank">Patrizier 4</a>. Als Inhalt gibt der Publisher diverse Handelsmöglichkeiten an. Dabei könnt ihr die Stadt Lübeck betreten. Die <a href="http://www.patrizier4.de" target="_blank">Patrizier 4</a> Demo steht ab sofort zum Download bereit und umfasst 800 Megabyte.</p> '
featured_image: /wp-content/uploads/2010/03/patrizier4.jpg

---
[> Zum Patrizier 4 &#8211; Demo Download][1]

 

 

   

* * *

   



 

 [1]: downloads/demos/item/demos/patrizier-4-demo