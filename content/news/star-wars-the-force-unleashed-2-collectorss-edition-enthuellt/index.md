---
title: 'Star Wars: The Force Unleashed 2 – Collectors`s Edition enthüllt'
author: gamevista
type: news
date: 2010-06-09T19:51:12+00:00
excerpt: '<p>[caption id="attachment_1192" align="alignright" width=""]<img class="caption alignright size-full wp-image-1192" src="http://www.gamevista.de/wp-content/uploads/2009/12/tfu2.jpg" border="0" alt="Star Wars: The Force Unleashed 2" title="Star Wars: The Force Unleashed 2" align="right" width="140" height="100" />Star Wars: The Force Unleashed 2[/caption]<strong>LucasArts</strong> hat bestätigt das <a href="http://www.unleashed2010.com" target="_blank">Star Wars: The Force Unleashed 2</a> auch als Collector`s Edition erhältlich sein wird. Die Sammleredition wird am 26. Oktober 2010 in den Handel kommen. Als Inhalt hat sich der Publisher etwas ganz besonderes einfallen lassen.</p> '
featured_image: /wp-content/uploads/2009/12/tfu2.jpg

---
Wer wollte nicht schon immer mal einen USB-Stick der aussieht wie ein kleines Männchen mit zwei Lichtschwertern. Das Teil bringt zwei Gigabyte Speicher mit und ein Artbook in digitaler Form ist auch schon vor installiert. Die CE wird sich preislich bei ca. 65 Euro einpendeln.

Weitere Inhalte sind:

&#8211; Steel Book Casing   
&#8211; 3 Challenge Mode Level   
&#8211; zusätzliche Skins   
&#8211; 1 gelber Lichtschwert Kristall   
&#8211; 1 USB-Stick designed in Form eines Starkillers   
&#8211; 1 Digitales Artbook   
&#8211; diverse Wallpaper   
&#8211; 1 Skript gezeichnet vom Serien Produzenten Haden Blackman

 

   

* * *

   



 