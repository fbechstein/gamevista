---
title: 'Kane & Lynch 2: Dog Days – Zweites Entwicklertagebuch zeigt die Charaktere'
author: gamevista
type: news
date: 2010-08-02T17:56:10+00:00
excerpt: '<p>[caption id="attachment_1576" align="alignright" width=""]<img class="caption alignright size-full wp-image-1576" src="http://www.gamevista.de/wp-content/uploads/2010/03/kane-and-lynch2.jpg" border="0" alt="Kane & Lynch 2: Dog Days" title="Kane & Lynch 2: Dog Days" align="right" width="140" height="100" />Kane & Lynch 2: Dog Days[/caption]Publisher <strong>Square Enix</strong> präsentierte heute einen neuen Trailer zum Actionspiel <a href="http://www.kaneandlynch.com" target="_blank">Kane & Lynch 2: Dog Days</a>. Im zweiten Entwicklertagebuch erklären der Gamedirector Karsten Lund und Artdirector Rasmus Poulsen die Hintergründe über die beiden Protagonisten des Spiels. Die Antihelden ergeben gerade zusammen eine explosive Mischung.</p> '
featured_image: /wp-content/uploads/2010/03/kane-and-lynch2.jpg

---
Das Video steht ab sofort bereit zum Anschauen und zeigt zahlreiche Spielszenen, kommentiert von den Entwicklern.

[> Zum Kane & Lynch 2: Dog Days &#8211; Entwicklertagebuch #2][1]

   

* * *

   



 [1]: videos/item/root/kane-a-lynch-2-dog-days-entwicklertagebuch-2