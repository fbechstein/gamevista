---
title: Alien Swarm – Kostenloses Spiel von Valve am Montag
author: gamevista
type: news
date: 2010-07-18T16:27:31+00:00
excerpt: '<p>[caption id="attachment_2024" align="alignright" width=""]<img class="caption alignright size-full wp-image-2024" src="http://www.gamevista.de/wp-content/uploads/2010/07/alienswarm.jpg" border="0" alt="Alien Swarm" title="Alien Swarm" align="right" width="140" height="100" />Alien Swarm[/caption]Entwickler <strong>Valve </strong>präsentiert am kommenden Montag einen kostenlosen Topdown-Shooter mit dem Namen <a href="http://www.alienswarm.com/" target="_blank">Alien Swarm</a>. Am 19. Juli wird über die Onlineplattform Steam das Spiel ohne weitere Kosten herunterladbar sein. Verantwortlich sind unter anderem Mitarbeiter die mit an der Entwicklung von Portal 2 oder der Left 4 Dead-Reihe beteiligt waren.</p> '
featured_image: /wp-content/uploads/2010/07/alienswarm.jpg

---
Ähnlich dem Spiel <a href="http://store.steampowered.com/app/22610/" target="_blank">Alien Breed: Impact</a> steuert ihr einen Marine der zahlreiche Aliens unter die Erde befördern muss. Zur Seite stehen ihm dabei bis zu drei weiteren Mitspielern.

 

   

* * *

   

