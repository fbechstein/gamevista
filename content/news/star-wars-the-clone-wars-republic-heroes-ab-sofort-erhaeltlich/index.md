---
title: 'Star Wars The Clone Wars: Republic Heroes – Ab sofort erhältlich'
author: gamevista
type: news
date: 2009-10-09T07:46:41+00:00
excerpt: '<p>[caption id="attachment_159" align="alignright" width=""]<img class="caption alignright size-full wp-image-159" src="http://www.gamevista.de/wp-content/uploads/2009/07/cwrh4_small.jpg" border="0" alt="Star Wars The Clone Wars: Republic Heroes" title="Star Wars The Clone Wars: Republic Heroes" align="right" width="140" height="100" />Star Wars The Clone Wars: Republic Heroes[/caption]Das neueste Videospiel aus der legendären <strong>Star Wars</strong>-Welt ist da. Dies berichtet der Publisher <a href="http://www.activision.com" target="_blank">Activision</a> per offizieller Pressemitteilung. Im neusten <strong>Star Wars</strong> Titel könnt ihr als Jediritter und Klonkrieger in über 40 Missionen, die im visuellen Stil der <strong>Clone Wars</strong> Serie gehalten sind, das Imperium bekämpfen.</p> '
featured_image: /wp-content/uploads/2009/07/cwrh4_small.jpg

---
Weiterhin gibt es einen Koop Modus für bis zu zwei Spieler. **Star Wars The Clone Wars: Republic Heroes** steht seit gestern in den Läden und ist für Xbox 360, PlayStation 3, Wii, PSP, Nintendo DS und PC erhältlich und wurde von der USK ab 12 Jahren freigegeben.

 

 

* * *



* * *

 