---
title: Piranha Bytes im Risen Interview
author: gamevista
type: post
date: 2009-08-23T22:41:44+00:00
excerpt: '<p>[caption id="attachment_459" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/08/mike_bjoern2.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-459" src="http://www.gamevista.de/wp-content/uploads/2009/08/mike_bjoern2_small.jpg" border="0" alt="Mike und Björn von Piranha Bytes im Interview" title="Mike und Björn von Piranha Bytes im Interview" align="right" width="140" height="100" /></a>Mike und Björn von Piranha Bytes im Interview[/caption]Am 2. Oktober 2009 erscheint das neue Werk der Gothic-Entwickler <a href="http://www.piranha-bytes.com/" target="_top">Piranha Bytes</a> <strong>Risen</strong>. Wir haben auf der gamescom die Piranhas Michael "Mike" Hoge und Björn Pankratz getroffen und sie ein wenig über den Anfang der neuen Serie ausgefragt.<br /><br /></p> '
featured_image: /wp-content/uploads/2009/08/mike_bjoern2_small.jpg

---
**Hallo Mike, Hallo Björn! Danke, dass Ihr Euch Zeit für unsere Leser genommen habt. Erzählt uns doch bitte erstmal etwas über den Stand des Spiels und etwas über die Story und das Konzept.**

_Mike_: Die Version, die wir hier vorführen ist ca. 1 Monat alt und meiner Meinung nach bereits bugfrei. Das ganze Spiel spielt sich auf einer Insel ab. Einer der Hauptaspekte des Spiels ist die Erkundung der Welt wie auch bei den alten Gothic Teilen. D.h. Du kannst prinzipiell alles machen &#8211; Dich überall frei bewegen, klettern, in Häuser hineingehen u.s.w. Risen ist ein Rollenspiel, d.h. Du löst Quests, kämpfst, quatscht mit Leuten &#8211; der Hauptaspekt ist jedoch die Erkundung der Welt.

_Björn_: Du kannst bei der Erkundung der Welt jederzeit die Hauptstoryline verlassen und zb. zum Strand gehen um dort alles zu untersuchen. Es ist dann problemlos möglich wieder auf den alten Pfad zu kommen um Deine Hauptquests zu lösen.

**Erzählt uns bitte etwas über die Kämpfe.**

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/1024_risen30.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-462" src="http://www.gamevista.de/wp-content/uploads/2009/08/risencombos.png" border="0" alt="Bei Risen sind diverse Kombos möglich" title="Bei Risen sind diverse Kombos möglich" align="right" width="140" height="100" /></a>_Mike_: Es gibt verschiedene Kämpfercharaktere im Spiel. Dann gibt es Quests, bei denen Du entscheiden kannst ob gekämpft wird oder nicht, man kann so Kämpfe vermeiden. Bei vielen Quests jedoch wirst Du um das Kämpfen nicht herum kommen. In Risen kannst Du einen Schwertkampf, Nahkampf, Stabkampf ausführen. Es gibt auch schwere Waffen wie z.B. Äxte und Schwerter. Du kannst z.B. mit einem Schwert drei Schläge machen &#8211; in einem richtigen Takt entstehen so Kombos. Du kannst auch parieren. Von Stufe zu Stufe gibt es immer mehr Kombinationen bei den Kombos. Du kannst z.B. Kombinationen aus Kombos und Parieren zusammenstellen, die dann von Stufe zu Stufe verbessert werden können. Also da gibt es viele verschiedene taktische Möglichkeiten einen Gegner niederzustrecken.



**  
Warum heißt das Spiel Risen? Wie kommt Ihr auf den Namen?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/1024_risen25.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-464" src="http://www.gamevista.de/wp-content/uploads/2009/08/risenruinen.png" border="0" alt="Die Tempel auf der Insel gehören zu dem grossen Geheimnis von Risen" title="Die Tempel auf der Insel gehören zu dem grossen Geheimnis von Risen" align="right" width="140" height="100" /></a>_Björn_: Das Spiel heißt Risen, weil merkwürdige Tempel aus dem Boden gekommen sind und keiner so genau weiß was damit los ist und Du als Spieler weißt es schon mal gar nicht. Bei den Tempeln gibt es massig Gold und Artefakte, so dass sich die Banditen Fraktion darauf gestürzt hat und so die Tempel besetzt hat. Der Inquisitor hat ein Gesetz erlassen, dass sich keiner auf der Insel frei bewegen soll. Wenn Du es trotzdem tust, wirst Du zwangsrekrutiert und bekommst eine (Charakter)Klasse aufgedrückt. Sagen wir mal, Du wirst gleich am Anfang von einem Ordenskrieger der Inquisition gefangen genommen. Dann bekommst Du eine Mischklasse aus Magier und Kämpfer aufgedrückt. Wenn man es nicht möchte, kann man einfach den letzten Spielstand laden oder man geht einfach dem Ordenskrieger aus dem Weg und läuft gleich zu den Banditen und wählt so seine Fraktion.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/mike.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-466" src="http://www.gamevista.de/wp-content/uploads/2009/08/mike_small.jpg" border="0" alt="Mit der Hälfte der Nebenquests wird man auf ca. 40-50 Stunden kommen. (Mike, Piranha Bytes)" title="Mit der Hälfte der Nebenquests wird man auf ca. 40-50 Stunden kommen. (Mike, Piranha Bytes)" align="right" width="140" height="100" /></a>_Mike_: Man kann sich aber auch freiwillig melden. Das Spiel ist so aufgebaut, dass wenn man geschnappt wird das Spiel dann nicht endet, sondern man spielt quasi so weiter und kommt irgendwann auf den Hauptquest zurück. Das ist das bemerkenswerte an dem Spiel. Du kannst Dich überall frei bewegen und Du kommst immer wieder zum Hauptquest zurück.

**Was für eine Spielzeit habt Ihr für Risen angesetzt?**

_Mike_: So mit der Hälfte der Nebenquests wird man auf ca. 40-50 Stunden kommen. Das kommt natürlich auf den Spieler an, wie gut der ist. Aber als Richtwert kann man 40-50 Stunden nehmen.



**Welche Interaktionen in der Welt sind möglich?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/bjoern.jpg" rel="lightbox[risen1]"></a>_Mike_: Die Welt erklärt sich eigentlich von selbst. Wenn Du z.B. Stückchen frisch aufgeschüttete Erde entdeckst, kannst Du mit einer Schaufel dort graben und evtl. einen Schatz entdecken. Es gibt unheimlich viele Verstecke wo Kleinigkeiten zu finden sind wie z.B. Schlüssel, mit denen Du Zugang zu anderen Schätzen erlangst. Das macht auch viel von dem Charme der Welt aus. Es gibt verschiedene Lösungen für die Quests, wie man an diverse Sachen kommen kann, man kann auf Dächer klettern, man kann Wände erklimmen, man kann mit verschiedenen Leuten quatschen. Durch das Meditieren bekommst Du die Möglichkeit in der Luft zu schweben. Dies kostet solange keine Energie, solange Du Dich nicht nach oben bewegst. Du hast also nur eine Höhenlimitierung, jedoch keine Weitenlimitierung. Weiterhin reagiert die Welt auf alles was Du machst. Du kannst z.B. in der Stadt auf die Wache losgehen, dann hast Du jedoch die halbe Stadt gegen Dich. Wenn Du an einer Wache vorbeikommen willst und diese lässt Dich nicht rein, dann kannst Du entweder ein Monster herbeirufen oder Du verwandelst Dich in eine Maus oder etwas kleines. Dies ist z.B. nützlich wenn Du eine Quest erfüllen musst. Da gibt es immer verschiedene Möglichkeiten.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/1024_risen44.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-469" src="http://www.gamevista.de/wp-content/uploads/2009/08/risenhafen.png" border="0" alt="Die Hafenstadt gehört zum  zentralen Punkt von Risen" title="Die Hafenstadt gehört zum  zentralen Punkt von Risen" align="right" width="140" height="100" /></a>_Björn_: In der Hafenstadt sind sämtliche Fraktionen der Welt vertreten. Hier ist sozusagen der zentrale Punkt des Spiels, man kann von hier aus die Welt erkunden, man kann alles machen. Wenn man sich in der Hafenstadt umguckt, kann man die eine oder andere Fraktion wählen und diese dann spielen. z.B. Rodriguez ist ein Ordenskrieger und man kann für ihn eine Quest erfüllen. Hier entscheidet sich wie die Quest gespielt wird &#8211; für die Banditen oder für die Inquisition.

_Mike_: Wenn Du Dich z.B. für die Banditen entscheidest, bekommst Du schnell einen schlechten Ruf. Du arbeitest Dich im Spiel aber nach vorne, Du kannst nicht irgendwie noch schlechter werden.

_Björn_: Wir haben darüber nachgedacht eine Seite zu wählen, denn es gibt Spiele wo Du entweder gut oder böse bist, aber so kannst Du eine coole Geschichte nicht erzählen. Es ist nicht plausibel wenn Du z.B. jemanden etwas aus der Tasche klaust und nachher ist das noch immer Dein bester Freund.

_Mike_: Du kannst z.B. auch lange spielen, viele Quests erfüllen und dann entscheidest Du Dich anders. Die Protagonisten merken sich Deine Entscheidung und reagieren entsprechend.



 

_Björn_: Hier ein Beispiel um die Questmechanik etwas zu verdeutlichen. Es gibt in einem Verlies einen Piratenkapitän, mit dem muss ich sprechen, davor steht jedoch eine Wache. Jetzt kann ich folgendes machen: Ich kann ihn mit Geld bestechen, ich kann ihm den Schlüssel aus der Tasche klauen, wenn ich Taschendiebstahl bei den Banditen oder irgend woanders gelernt habe, oder ich kann versuchen ihn zu überreden. Beim letzteren sagt mir der Wächter &#8222;Mir ist langweilig, andere vergnügen sich in den Kneipen mit Frauen und ich darf in der prallen Sonne herumstehen&#8220;. So kannst Du ihm z.B. eine Frau besorgen, mit der er sich vergnügt, und erhälst so den Schlüssel. Oder aber Du haust ihn einfach um und klaust ihm den Schlüssel.

_Mike_: Wobei der natürlich stärkerer Spielcharakter ist, so dass Du hier um einiges stärker sein müsstest.

_Björn_: Es zeigt halt, dass es viele Möglichkeiten gibt wie man im Spiel an die Lösung herankommt. Du kannst auf geheim spielen, die wilde Sau loslassen und einfach alles umkloppen oder versuchen alles demokratisch zu regeln.

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/mike_pc.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-471" src="http://www.gamevista.de/wp-content/uploads/2009/08/mike_pc_small.jpg" border="0" alt="Risen ist ein Teil einer größeren Serie. (Mike, Piranha Bytes)" title="Risen ist ein Teil einer größeren Serie. (Mike, Piranha Bytes)" align="right" width="140" height="100" /></a>_Mike_: Das gute an dem Spiel ist, dass Du schwere Waffen wie z.B. Axt skillen kannst. Wenn Du das machst bist Du eigentlich relativ schnell sehr gut. Wenn Du dann in den Wald gehst, dann kannst Du schnell alle wilden Tiere umhauen. Wenn Du aber keine Lust hast die Kampfmaschine zu machen, besteht natürlich auch die Möglichkeit beispielsweise Taschendiebstahl zu skillen.

**Wird die Story von Risen in sich abgeschlossen sein oder ist das Ende offen?**

_Mike_: Risen ist ein Teil einer größeren Serie, aber die Geschichte findet schon ihr Ende.

**Habt Ihr für Risen DLCs geplant?**

_Mike_: Nein, bisher noch nicht.



**Sind bei Risen irgendwelche Ideen, Kritiken aus der Community mit eingeflossen?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/bjoern.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-472" src="http://www.gamevista.de/wp-content/uploads/2009/08/bjoern_small.jpg" border="0" alt="Mit Risen kehren wir zu unserem alten Konzept zurück (Björn, Piranha Bytes)" title="Mit Risen kehren wir zu unserem alten Konzept zurück (Björn, Piranha Bytes)" align="right" width="140" height="100" /></a>_Björn_: Ja, ohne Ende. Wie man weiß, haben wir eine Schelte für unser vorheriges Produkt gekriegt. Unsere letzten Games waren vom Umfang her gross und dann immer größer. Unser jetziges Produkt ist groß, jedoch komplex. Das ist das, was uns von Piranha Bytes ausmacht. Viele Charaktere, die man auch später in diversen Quests wiedertrifft, haben wir ausgearbeitet, Dungeons sind interessanter geworden, viele Schalterrätsel mit Fallen u.s.w.  
Alles was bemängelt wurde und wir selbst bemerkt haben, haben wir korrigiert. Einige Anregungen haben wir ebenfalls mit aufgenommen. Wir haben sehr viel Feedback von der Community bekommen nach Gothic 3, was die Leute von uns erwarten. Bisher haben wir ein recht positives Feedback zurückbekommen. Es sind auch Sachen weggefallen, die teilweise bereits entwickelt wurden. z.B. gibt es verschiedene Arten von Fallen, von denen haben wir nur welche drin gelassen, die sich auch gut anfühlen. Es gab auch einige Sachen, die keinen Sinn ergaben, die wurden dann auch aus dem Spiel entfernt. Es sind eher Kleinigkeiten.  
Wir wollten mit Risen zu unseren alten Konzept, wie bei den Gothic Spielen, zurückkehren. Alles ist ausgefeilter, in sich interessanter,  komplexer. Kurzum: wir haben die &#8222;What you see, what you get&#8220;-Politik gewählt. Freie Welt, Du kannst alles machen, nahezu jeden töten. Plausibel &#8211; die NPCs reagieren nicht falsch auf Dich, wenn Du irgendetwas machst.

**Woher nimmt Ihr die Inspiration für die Spiele, bzw. speziell für Risen?**

_Björn_: Das kann man plakativ so nicht sagen. Die einen werden sagen an Herr der Ringe, die anderen werden sagen an irgendwelchen Spielen. Wir haben sehr viel gespielt. Da z.B. Risen auch auf der Konsole herauskommt, haben wir uns angeschaut ob die Elemente, Struktur überhaupt bei einer Konsolenversion passen. Was die Inspiration angeht &#8211; wir sind alle Zocker. Die ganze Firma besteht aus Zockern &#8211; das findet man eigentlich nicht immer. Man kann sagen, wir sind Idealisten und wir machen ein Spiel was uns selber auch Spaß macht.



**Zum Thema Soundtrack. Gothic 3 Soundtrack wurde ja mit einem Orchester aufgenommen. Erwartet den Spieler dies bei Risen auch?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/08/mike_bjoern.jpg" rel="lightbox[risen1]"><img class="caption alignright size-full wp-image-474" src="http://www.gamevista.de/wp-content/uploads/2009/08/mike_bjoern_small.jpg" border="0" alt="Wir sind Idealisten und wir machen ein Spiel was uns selber auch Spaß macht." title="Wir sind Idealisten und wir machen ein Spiel was uns selber auch Spaß macht." align="right" width="140" height="100" /></a>_Björn_: Dies war bei Gothic 3 ein Versuch, jedoch haben wir bei Risen mehr darauf geachtet, dass der Soundtrack ins Spiel passt. Der orchestrale Soundtrack war sehr pompös und hat die Spieler eher abgelenkt. Wir haben im Spiel verschiedene Musikthemen, beispielsweise die Hafenstadt hat einen eigenen Soundtrack, oder aber auch während des Kampfes. Wichtige Locations und Dungeons sind z.B. mit eigenen Soundtracks ausgestattet.

**Ist die Grafikengine komplett neu entwickelt oder ist diese von Gothic 3 übernommen?**

_Björn_: Die Grafikengine ist eine Weiterentwicklung. D.h. wir haben Teile aus der Alten herausgerissen und neu zusammengesetzt.

**Letzte Frage von uns an Euch. Wie groß war Euer Entwicklungsteam und wie lange habt Ihr für die Entwicklung von Risen benötigt?**

_Björn_: 2,5 Jahre mit einem Team von 21 Leuten.

**Vielen Dank für das Interview und viel Erfolg mit Risen. Wir können es kaum erwarten Euer neustes Werk in den Händen zu halten.  
** 

 

* * *



* * *

 