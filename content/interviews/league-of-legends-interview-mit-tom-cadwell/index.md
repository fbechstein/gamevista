---
title: League of Legends – Interview mit Tom Cadwell
author: gamevista
type: post
date: 2009-11-09T19:16:50+00:00
excerpt: '<p>[caption id="attachment_995" align="alignright" width=""]<img class="caption alignright size-full wp-image-995" src="http://www.gamevista.de/wp-content/uploads/2009/11/logo.jpg" border="0" alt="League of Legends" title="League of Legends" align="right" width="140" height="100" />League of Legends[/caption]Das Spiel <strong>League of Legends</strong> ist seit Freitag in Europa verfügbar. Die Amerikaner können <strong>League of Legends</strong> bereits seit mehr als zwei Wochen spielen. Design Director Tom Cadwell war bereit sich von uns interviewen zu lassen und wir haben diese Gelegenheit natürlich ergriffen. Ein Vergleich zu <strong>DotA</strong>, der Grafikstil und die Hardwareanforderungen sind nur einige der Themen, die wir angeschnitten haben.</p> '
featured_image: /wp-content/uploads/2009/11/logo.jpg

---
**Bitte stell dich unseren Lesern kurz vor.**

<img class="caption alignright size-full wp-image-996" src="http://www.gamevista.de/wp-content/uploads/2009/11/portrai.jpg" border="0" alt="Tom Cadwell" title="Tom Cadwell" align="right" width="140" height="100" />Ich bin Tom Cadwell, Design Director bei <a href="http://www.riotgames.com/" target="_blank">Riot Games</a>. Ich arbeite seit einem Jahr vollzeit an dem Projekt und habe zuvor für etwa zweieinhalb Jahre bei Riot Games hin und wieder ausgeholfen.

**Was ist League of Legends, wenn man es mit den Worten eines Entwicklers und nicht mit denen einer PR-Frau oder eines PR-Mannes, beschreibt?**

Unsere PR und Marketing-Frauen und -Männer müssen sich gut mit Spielen auskennen und auch selber Spieler sein – du zollst Ihnen nicht den Respekt, den sie verdient haben!

Aber ich freue mich natürlich, wenn ich helfen kann: **League of Legends** ist ein geistiger Nachfolger des bekannten Mods **DotA**. Der Spieler spielt einen Beschwörer, der einen Champion herbeiruft für eine intensive Team PVP Erfahrung. Eine Schlachten sind ungefähr 30 Minuten lang. Fünf Spieler bilden dabei ein Team um die Ziele zu erreichen und das gegnerische Team aus zu schalten.

Wenn man eine Schlacht gewinnt, erhält vom Erfahrungspunkte (für den Beschwörer). Zusätzlich dazu kann Zusatzcontent frei gespielt werden. Das wirkliche Tolle an diesem Genre ist, dass man ein befriedigendes Rollenspiel-Erlebnis mit einem schönen Mix aus Action bekommt.  
Ich möchte natürlich auch erwähnen, dass League of Legends komplett kostenlos ist und man es unbedingt einfach mal ausprobieren sollte. Wir bezahlen unsere Kosten durch den Verkauf von kleinen Modifikationen und Anpassungen.



******Wenn man DotA nicht mag, gibt es eine Chance, dass man League of Legends mögen wird?**

Es ist durchaus möglich, dass einem **League of Legends** trotzdem sehr viel Spaß machen wird. **DotA** hat eine Reihe von Problemen, die wir versucht haben, in unserem Spiel zu verbessern. Dank diesen Korrekturen ist das Spiel flüssiger und es hat gleichzeitig mehr Tiefgang. Einige Features, die man sich bei **DotA** gewünscht hat, haben wir umgesetzt.

<a href="http://www.gamevista.de/wp-content/uploads/2009/11/lol.jpg" rel="lightbox[lol1]"><img class="caption alignright size-full wp-image-998" src="http://www.gamevista.de/wp-content/uploads/2009/11/small_lol.jpg" border="0" alt="League of Legends" title="League of Legends" align="right" width="140" height="100" /></a>Als erstes haben wir versucht das Spiel einfach zugänglich zu machen. Das reicht vom Schreiben einfacher Tooltipps, über ein Tutorial, die Möglichkeit gegen Bots zu trainieren, ein System das entsprechend deiner Stärke die menschlichen Gegner sucht bis hin zum Feature, dass dem Spieler angezeigt werden kann, woran er gestorben ist.

Wir haben auch versucht dem Spieler den Anstieg zu vereinfachen, indem er weniger wissen muss, um das Spiel gut zu verstehen. Das haben wir vor allem durch die Verwendung von einfachen Symbolen und einem klaren Design erreicht. Das gleiche Prinzip wird auch bei einigen Echtzeitstrategie-Spielen angewandt. Das Ziel ist es „Newbies“ schnell zu einem „zumindest ein wenig kompetenten Spieler“ zu machen. Ein Spiel perfekt zu beherrschen dauert natürlich sehr lange, aber keiner möchte lange als verwirrter „Newbie“ in einem Spiel sein.

Zweitens haben wir eine positive Community gebaut. **DotA** hat eine durch Konkurrenz geprägte Community. Wir haben bei der Bildung darauf geachtet, dass die Community freundlicher, offener und allgemein weniger konkurrenzbedacht wird. Dies wird realisiert durch fähiges Personal und vor allem durch den Konkurrenzabbau untereinander in einem Team. An einigen Stellen haben wir das Gamedesign so verändert, dass es zu weniger Konkurrenzverhalten führt. Ein Beispiel dafür ist, dass man für das Assistieren bei einem Kill fast genau so viel Gold erhält, wie für den Kill an sich. Dadurch ist „kill stealing“ nicht mehr schlimm. Auch gibt es keine Fähigkeiten mehr mit denen ich Mitglieder meines Teams verletzen oder benachteiligen kann. Ebenso gibt es weniger Gold dafür jemanden zu töten, der bereits sehr oft getötet wurde (das verringert evtl. Konflikte im Team, weil ein Anfänger / Schwächerer öfters stirbt).



**Wann begann die Entwicklung? Wie viele Leute arbeiten an diesem Projekt?**

Der grundlegende Beginn war vor etwa drei Jahren, aber wir mussten ein Unternehmen, ein Team und ein Spiel erstellen. Wir haben etwa ein Jahr in der Prototype-Phase verbracht und die eigentliche Entwicklung hat zwei Jahre in Anspruch genommen. Anfangs war das Team recht klein, nach und nach kamen mehr dazu und inzwischen sind wir bei 50 Entwicklern angekommen.

<a href="http://www.gamevista.de/wp-content/uploads/2009/11/anivia.jpg" rel="lightbox[lol2]"><img class="caption alignright size-full wp-image-1000" src="http://www.gamevista.de/wp-content/uploads/2009/11/small_anivia.jpg" border="0" alt="Sieht auch cool aus!" title="Sieht auch cool aus!" align="right" width="140" height="100" /></a>**Welcher ist dein Lieblingschampion und wieso?**

Ich bin ein großer Fan von Anivia, die „Cryophoenix”. Ich mag sie, weil sie einige einzigartige Fähigkeiten hat, die sich anders als alles in **DotA** spielen. Ein Beispiel: Sie kann eine Rakete abschießen, die an einem Ziel verbleibt bis man den Detonationsknopf drückt und dann erst explodiert. Ich finde es auch sehr schön, dass man sehr viel Erfahrung braucht, wenn sie auf dem höchsten Level ist – der Unterschied zwischen einer guten, sehr guten und genialen „Cryophoenix“ ist groß.

**Kannst du mir etwas über die Hardwareanforderungen sagen?**

Unsere Mindestanforderungen sind:

  * 2.0 GHz dual core
  * 512 MB of RAM
  * Geforce 6 or greater card (DirectX 9.0 capable video card)
  * 750 MB of available hard disk space
  * Windows XP / Vista / 7 (We will hopefully get Mac compatibility soon)



**Wie kommt ihr auf den Namen?**

Wir nehmen uns selbst nicht all zu ernst und uns gefiel der Name LoL gut. Man kann ihn sich einfach merken und League of Legends drückt gut aus, was das Spiel beinhaltet. League steht für Wettkampf und Legends für die Champions.

**Das Spiel wird komplett kostenlos sein. Habt ihr schon über DLC oder irgendeine Art des Addons nachgedacht? Werden die Spieler für Sachen wie neue Charaktere, neue Fähigkeiten etc. zahlen müssen?**

<a href="http://www.gamevista.de/wp-content/uploads/2009/11/lol2.jpg" rel="lightbox[lol3]"><img class="caption alignright size-full wp-image-1002" src="http://www.gamevista.de/wp-content/uploads/2009/11/small_lol2.jpg" border="0" alt="League of Legends" title="League of Legends" align="right" width="140" height="100" /></a>Vorweg möchte ich klarstellen, dass wir keine Fähigkeiten, Zauber etc. an Spieler verkaufen und damit keiner sich irgendeine Art der Macht kaufen kann. Die Spieler können die Rate mit der sie neuen Content frei schalten verbessern, indem sie dafür zahlen. Als Beispiel: Jeder Champion kann frei gespielt werden oder frei gekauft werden. Auf der anderen Seite können Runen für Beschwörer nur frei geschaltet werden durch das Spielen &#8211; diese sind also nicht käuflich. Wir gehen davon aus, dass die meisten Spieler uns nichts zahlen werden und das ist okay für uns. Das heißt auch, dass das Spiel nur dann wirtschaftlich erfolgreich wird, wenn es viele spielen, von denen nur einige uns unterstützen, indem sie Content frei kaufen.

Natürlich planen wir League of Legends langfristig weiter aus zu bauen. Wir stehen gerade erst am Anfang und haben noch große Pläne für die Zukunft des Spiels.

**Vielen Dank für deine Zeit und deine Antworten.**

 

League of Legends ist seit dem 06. November im europäischen Raum erhältlich. Man kann sich das Spiel einfach auf der Website zum Spiel herunterladen.

 

<a href="http://www.lol-europe.com/" target="_blank">>Zur Website des Spiels</a>

<a href="http://www.lol-europe.com/telechargement.php" target="_blank">>Zum Download des Spiels</a>

 





