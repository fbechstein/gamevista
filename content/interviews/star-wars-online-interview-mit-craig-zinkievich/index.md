---
title: Star Trek Online – Interview mit Craig Zinkievich
author: gamevista
type: post
date: 2009-12-29T13:05:40+00:00
excerpt: '<p><img class=" alignright size-full wp-image-1237" src="http://www.gamevista.de/wp-content/uploads/2009/12/logo.jpg" border="0" alt="Star Trek Online" title="Star Trek Online" align="right" width="140" height="100" /><strong>Roter Alarm!</strong> Die Entwickler von <strong>Star Trek Online</strong> standen uns Rede und Antwort und das nutzen wir natürlich aus. Bei uns lest ihr das Interview über das erste MMO angesiedelt im Star Trek – Universum.</p> '
featured_image: /wp-content/uploads/2009/12/logo.jpg

---
**1. Wer bist du?**

Ich heiße **Craig Zinkievich** und ich bin Executive Producer von **Star Trek Online**. Ich arbeite seit fast 7 Jahren bei Cryptic Studios – damals waren wir weniger als 15 Leute. Vor **Star Trek Online** war ich Producer von **City of Heroes** und **City of Villains**.<img class="caption alignright size-full wp-image-1238" src="http://www.gamevista.de/wp-content/uploads/2009/12/craig.jpg" border="0" alt="Craig Zinkievich" title="Craig Zinkievich" align="right" width="140" height="100" />

**2. So wie ich das gehört habe spielt das Spiel einige Jahre nach dem 10ten Film – das heißt leider, dass der Spieler nichtBerühmtheiten wie Jean-Luc Picard und Catherin Janeway treffen können wird. Warum habt ihr das so gemacht?**

**Star Trek Online** spielt im Jahr 2409, 30 Jahre nach Star Trek Nemesis und 22 Jahre nach der Zerstörung von Romulus. Der Alpha Quadrant ist nicht so sicher wie er einmal war. Das romulanische Imperium (Romulan Empire) ist zersplittert, aber immer noch stark und einflussreich. Das Abkommen zwischen der Föderation (United Federation of Planets) und den Klingonen (Klingon Empire) ist gebrochen worden und die Kräfte der beiden Fraktionen wirken gegeneinander im Krieg. Die Borg sind zusätzlich zurück gekehrt, besser und immer noch mit dem Ziel alles zu assimilieren …

Der Grund für die Wahl dieses Zeitabschnitts ist, dass wir dem Spieler auf der einen Seite etwas bieten wollten, was er kennt, und auf der anderen Seite sind wir auch ein Team kreativer Individuen und wir wollten nicht einfach nur die vorgefertigten Szenen aus den Filmen und Serien nutzen. Wir wollten nicht, dass der Spieler sich so fühlt, dass er unter Picard oder Janeway steht. Zusätzlich soll sich jeder in STO wie Kirk, Picard oder Janeway fühlen. Dennoch gibt es durchaus Star Trek &#8211; Charaktere im Spiel, wobei Picard und Janeway nicht dazu gehören.

<a href="http://www.gamevista.de/wp-content/uploads/2009/12/konferenz.jpg" rel="lightbox[sto1]"><img class="caption alignright size-full wp-image-1240" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallkonferenz.jpg" border="0" alt="Wir sind der  Captain!" title="Wir sind der Captain!" align="right" width="140" height="100" /></a>**3. Wir sahen einige wirklich coolen Videos über Kämpfe im Weltraum, aber keine über Kämpfe auf Planeten. In einem „normalen“ MMO übernimmt der Spieler die Rolle eines Charakters und das ist alles. Wie habt ihr das in STO gelöst?**

In STO bist du ein Captain und übernimmst natürlich auch die Kontrolle über ein Schiff, dass du steuerst und mit dem du kämpfen kannst. Wenn du dich auf einen Planten runter beamst, leitest du das Außenteam. Um es zu vereinfachen und damit der Spieler sich mit einem Charakter identifizieren kann ist dein Avatar der Captain. Wir haben einige interessante Änderungen in den Landkampf eingearbeitet. Davon werden wir aber bald noch ein paar Videos veröffentlichen.

**4. Es gibt also Kämpfe im Weltraum und auf Planeten. Wie umfangreich ist jeder einzelne Teil? Wie habt ihr einen Übergang zwischen den beiden Teilen erstellt?**

Die Verteilung ist momentan etwa 60% Weltraum- zu 40% Boden-Kämpfen. Der Inhalt von STO ist so ausgelegt, dass der Spieler sich wie in einer TV-Serie oder einem Film fühlt. Der Wechsel ist recht häufig und man sollte erwarten, dass man beides in jeder Session spielt. Boden meint dabei natürlich nicht unbedingt Erde. Wir haben eine große Varianz von Raumschiffdecks über Raumstationen und exotische Landschaften auf fremden Planten bis hin zu Dörfern.

<p style="text-align: center;">
  <img class=" size-full wp-image-1241" src="http://www.gamevista.de/wp-content/uploads/2009/12/schlacht.jpg" border="0" width="600" height="340" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/schlacht.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/schlacht-300x170.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**5. Kannst du mir etwas über die Hardwareanforderungen sagen?**

Ja, natürlich. Es könnte aber sein, dass sie sich noch mal etwas ändern.

Minimum System Requirements:

  * OS: Windows XP SP2 / Windows Vista / Windows 7 (32 or 64-bit)
  * CPU: Intel Core 2 Duo 1.8 Ghz or AMD Athlon X2 3800+
  * Memory: 1GB RAM
  * Video: NVIDIA GeForce 7950 / ATI Radeon X1800 / Intel HD Graphics
  * Sound: DirectX 9.0c compatible sound card
  * DirectX: Version 9.0c or higher
  * HDD: 10GB free disk space
  * Network: Broadband required
  * Disc: 6x DVD-ROM

Recommended System Configuration:

  * OS: Windows XP SP2 / Windows Vista / Windows 7 (32 or 64-bit)
  * CPU: Intel E8400 Core 2 Duo or AMD Athlon X2 5600+
  * Memory: 2GB RAM or better
  * Video: NVIDIA GeForce 8800 / ATI Radeon HD 3850+
  * Sound: DirectXcCompatible soundcard
  * DirectX: Version 9.0c or higher
  * HDD: 10GB free disk space
  * Network: Broadband required
  * Disc: 6x DVD-ROM

**6. Wird es die Möglichkeit geben einen Ingame-Charakter zu heiraten? (Falls ja: Wird es irgendwelche Einschränkungen?)** 

Ich denke nicht, dass es die Möglichkeit geben wird jemanden zu heiraten, außer der, dass zwei Charakter über den Chat verkünden, dass sie nun verheiratet sind. Es gibt beides: So viele Inhalte im Star Trek – Universum und so viele Features, die wir einbauen könnten – aber die auf keinen Fall alle rein passen. Es ist schwer zu entscheiden, welche wir für den Release enthalten haben wollen und welche nicht. Wie auch immer, nichts ist vom Tisch. Wenn das etwas ist, was die Spieler wirklich wollen, sind wir natürlich bereit es einzufügen.

<a href="http://www.gamevista.de/wp-content/uploads/2009/12/klingon.jpg" rel="lightbox[sto2]"><img class="caption alignright size-full wp-image-1243" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallklingon.jpg" border="0" alt="Klingon sind Krieger!" title="Klingonen sind Krieger!" align="right" width="140" height="100" /></a>**7. Es wird ja leider nur zwei spielbare Fraktionen geben. Was ist die nächste spielbare Fraktion? Habt ihr schon über so was, Add-ons und DLCs nachgedacht?** 

Das hängt davon ab, was die Spieler wollen. Wir machen genau das, was die Menschen wollen – so einfach ist das. Wenn wir etwas machen, machen wir es richtig und wir machen es groß. Bei den Klingonen versuchen wir etwas anderes zu bieten, etwas, was das Gameplay der Föderation komplimentiert und sich nicht einfach gleich spielt. Wenn die Spieler das mögen, werden wir das erweitern.

**8. Nächstes Jahr wird es zwei große Sci-Fi-MMOs geben, beide basieren auf einer starken Lizenz: Stargate Worlds und euer Spiel. Macht euch diese Situation irgendwelche Sorgen?** 

Nein, überhaupt nicht. Man kann nie genug guten Sci-Fi-Stuff haben. Und welcher Fan von SG-1 oder Atlantis hat noch nicht davon geträumt durch das Gate zu gehen? Weißt du, dass ist das lustige daran Entwickler zu sein. Natürlich ist das hier Business und letzten Endes wollen wir es gut machen und der Beste im Wettstreit sein, aber zur gleichen Zeit ist es eine Leidenschaft – es ist etwas, was wir lieben. Ich denke niemand sieht einem Release gespannter entgegen als ein Entwickler, auch wenn es ein „Konkurrenz-Titel“ ist. Auch denke ich, dass Stargate und Star Trek unterschiedlich genug sind, wodurch ich kein Problem darin sehe, dass beide Titel in 2010 veröffentlicht werden.

**9. Die gezeigte Grafik und das gezeigte Gameplay in den Videos war von der PC- oder der Konsolen-Version?**

PC. Wir haben keine Konsolen-Version explizit angekündigt und so haben wir nichts, was wir davon momentan zeigen könnten.

**10. Warum habt ihr euch entschieden ein MMO für beide Plattformen (PC und Konsolen) zu entwickeln.** 

Die bessere Frage ist: Warum würde irgendjemand nicht wollen, dass sein Spiel auf so vielen Plattformen wie möglich entwickelt und veröffentlicht wird?

<p style="text-align: center;">
  <img class=" size-full wp-image-1244" src="http://www.gamevista.de/wp-content/uploads/2009/12/federation.jpg" border="0" width="600" height="342" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/federation.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/federation-300x171.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**11. Werden PC-Spieler und Konsolen-Spieler denn eventuell zusammen spielen können?**

Auch wenn wir noch keine Konsolen-Version offiziell angekündigt haben, wird dieses Feature wohl nicht enthalten sein werden. Wir würden so etwas sehr gerne machen, aber es gibt geschäftliche Gründe, die uns davon abhalten.

<a href="http://www.gamevista.de/wp-content/uploads/2009/12/space.jpg" rel="lightbox[sto3]"><img class="caption alignright size-full wp-image-1246" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallspace.jpg" border="0" alt="My ship is my castle" title="My ship is my castle" align="right" width="140" height="100" /></a>**12. Was ist deine Lieblingsschiffklasse und warum? Wirst du als Klingone oder als Föderationsmitglied spielen?**

Ich mochte die Akira-Klassen (Escort ship) sehr, aber gerade tendiere ich zu unserem neuen Tier 5 Kreuzer. Das ist wirklich ein beeindruckendes Schiff. Unser Schiffteam hat wirklich eine beeindruckende Arbeit geleistet und ich denke die Fans werden die Schiffe wirklich mögen. Ich werde beide spielen – dafür sind sie ja da. Es muss ja nicht unbedingt die eine oder die andere sein.

**13. Wie viel wird ein Spieler pro Monat zahlen müssen, um das Spiel spielen zu dürfen? Habt ihr das bereits entschieden?**

Wir haben unser Businessmodel noch nicht detailliert. Es wird bald eine Ankündigung dazu geben.

**Vielen Dank für das Beantworten unserer Fragen. Wir wünschen euch gutes Gelingen und ein frohes neues Jahr.  
** 

Star Trek – Spiele gibt es schon einige, doch bis jetzt sind die meisten nur mit schlechten Wertungen bei Spielemagazinen und –portal weggekommen. Ob STO diesem Trend entkommen kann und ob es die hohen Erwartungen erfüllt erfahren wir 2010.

**Beam mich runter, Scotty!**

 

* * *



* * *