---
title: Uncharted 2 – Interview mit Naughty Dog
author: gamevista
type: post
date: 2009-10-01T09:50:00+00:00
excerpt: '<p>[caption id="attachment_790" align="alignright" width=""]<a href="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_2_big.png" rel="lightbox[uncharted2]"><img class="caption alignright size-full wp-image-790" src="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_2_small.png" border="0" alt="Justin Richmond und Arne Meyer von Naughty Dog" title="Justin Richmond und Arne Meyer von Naughty Dog" align="right" width="140" height="100" /></a>Justin Richmond und Arne Meyer von Naughty Dog[/caption]Am 16. Oktober 2009 erscheint die langersehnte Fortsetzung von Uncharted, <strong>Uncharted 2 - Among Thieves</strong>. Der Playstation 3 exclusive Titel ist bereits vor dem Release mit vielen Awards ausgezeichnet worden und gehört bereits jetzt schon zum PS3-Bestseller 2009. <br />Wir haben Arne Mayer (Senior Studio Manager) und Justin Richmond (Lead Multiplayer Designer) von <a href="http://www.naughtydog.com/" target="_top">Naughty Dog</a> getroffen und sie über das kommende Spiele-Highlight ausgefragt.</p> '
featured_image: /wp-content/uploads/2009/10/uncharted2iv_2_small.png

---
**Hallo und herzlichen Dank, dass Ihr Euch für uns und unsere Leser Zeit genommen habt.   
Welche Länder / Locations wird Nathan in Uncharted 2 bereisen?** 

_Arne:_ Den Spieler erwarten Sümpfe, Dschungel, Städte, eine geheimnisvolle Stadt im Himalaya Gebirge u.v.m. 

**Uncharted 2 ist eine Mischung aus Adventure und Shooter &#8211; Was überwiegt im Spiel?** 

_Justin_: Man kann so keine genaue Verteilung festlegen, da es auf den Spieler ankommt. Die Mischung macht es aus. Hier kletterst Du, ziehst eine Waffe, schiesst und kletterst weiter. In einigen Räumen kannst Du z.B. hineinlaufen und wild um Dich schiessen, Du kannst Dich aber auch von hinten anschleichen und die Gegner lautlos erledigen. Daher ist eine Aufteilung in z.B. 60% Shooter und 40% Adventure nicht möglich. 

**Was hat euch  bei der Entwicklung des Games inspiriert?** 

<a href="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_3_big.png" rel="lightbox[uncharted2]"><img class="caption alignright size-full wp-image-793" src="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_3_small.png" border="0" alt="In etwa 12 Stunden sollte man das Spiel durchgespielt haben (Arne Meyer, Naughty Dog)" title="In etwa 12 Stunden sollte man das Spiel durchgespielt haben (Arne Meyer, Naughty Dog)" align="right" width="140" height="100" /></a>_Arne_: Die Story handelt ja von Marco Polo, welcher sich mit großen Schätzen, vielen Schiffen und starker Besatzung in Richtung Heimat aufmachte, doch nur mit einem Schiff und einer Hand voll Männer zuhause eintraf. Wir und unsere Storywriter haben lange überlegt, wo würde Marco Polo seinen gefunden Schatz verstecken und waren froh, dass wir soviele Orte und Mysterien fanden, welche wir mit in das Spiel einbauen konnten. z.B. eine verschollene Stadt auf dem Himalaya. 

**Was für eine Spielzeit kann der Spieler erwarten?** 

_Arne_: So in etwa 12 Stunden sollte man das Spiel durchgespielt haben. 

**Wurde etwas am Kampfsystem geändert? Das Kämpfen im ersten Teil war ziemlich hart und verlief sehr linear.   
**   
_Justin_: Das Kampfsystem sollte mehr ins Spiel integriert werden, wir wollten keine abgehackten Sequenzen mehr, hier eine Klettersequenz und dann boom ab in den Shooterteil. Es sollte alles nahtlos ineinander übergehen und das ist uns, denke ich, auch ganz gut gelungen. Die Gegner KI wurde extrem verbessert, so warnen sich die Gegner gegenseitig, wenn sie Dich entdecken, sie verraten sich wo du Dich versteckst und koordinieren ihre Angriffe. Jeder Gegner hat seine Eigenheiten, so werden sich auch Gegner an dich anschleichen, während andere auf Dich zurennen oder sich verschanzen und aus der Deckung schießen.

 



<img class="caption alignright size-full wp-image-795" src="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_4_small.png" border="0" alt="An dem Spiel haben 110 Personen über zwei Jahre lang gearbeitet (Arne Meyer, Naughty Dog)" title="An dem Spiel haben 110 Personen über zwei Jahre lang gearbeitet (Arne Meyer, Naughty Dog)" align="right" width="140" height="100" /></a>**Welche Charaktere begleiten Nathan bei in seinem Abenteuer? Inwieweit spielen sie in der Handlung mit?**   
_  
Arne_: Die verschiedenen Charaktere bringen verschiedene Aspekte in Nathans Persönlichkeit zum Vorschein, zum Beispiel Chloe Frazer, mit der es bereits eine Geschichte gibt. Sie ist sehr unabhängig und hat Ihre eigene Art Dinge zu lösen. Oder Lena, mit ihr gibt es zwar keine Vorgeschichte, aber so oder so klappt es mit den beiden einfach nicht. 

**Wie lange habt ihr an dem Spiel gearbeitet? Wie gross ist Euer Team?   
**   
_Arne_: An dem Spiel haben 110 Personen über zwei Jahre lang gearbeitet. 

**Erzählt uns etwas über den Multiplayermodus. Welche Modi / Optionen wird es geben?** 

_Justin_: Es wird verschiedene Multiplayer-Modi geben, welche sehr vielfältig und unterschiedlich sind. Es gibt verschiedene Coop Missionen, welche man zu zweit bewältigen kann und muss, wobei sich diese von der Singleplayer Mission sehr unterscheiden. Auch wenn die Locations manchmal ähnlich sind, unterscheiden sich die Rätsel und Gegner total. Des Weiteren wird es ähnliche Modi zu Goldrush, Capture the Flag und Deathmatch geben, in denen bis zu 10 Spieler gleichzeitig gegeneinander spielen können. Die Multiplayer-Modi werden allerdings nur über das Internet spielbar sein, da es dazu nötig ist, zu unserem Server Verbindung aufzunehmen. 

**Wird es DLC bzw. Addons für Uncharted 2 geben?** 

<a href="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_1_big.png" rel="lightbox[uncharted2]"><img class="caption alignright size-full wp-image-797" src="http://www.gamevista.de/wp-content/uploads/2009/10/uncharted2iv_1_small.png" border="0" alt="Uncharted 2 gehört zu den meisterwarteten Spielen für die PS3" title="Uncharted 2 gehört zu den meisterwarteten Spielen für die PS3" align="right" width="140" height="100" /></a>_Justin_: Ja, definitiv. DLC ist in Planung, aber erstmal müssen wir uns auf den Release von Uncharted 2 konzentrieren. 

**Welchen Einfluss hat die Community auf die Entwicklung des Spiels gehabt?   
**   
_Justin_: Wir haben zu Uncharted (1) sehr viel Lob und Kritik erhalten, daher wussten wir, was viele Spieler von uns erwarten. Sei es ein einfacher Forumpost, bishin zu Umfragen unter unseren Testern, wir haben versucht uns alles zu Herzen zu nehmen und so viel wie möglich umzusetzen. 

**Gibt es schon Pläne bzw. Ideen für einen dritten Teil?** 

_Justin_: Nach den 2 Jahren mit Uncharted 2 werden wir hoffentlich erstmal 3-4 Monate Pause machen dürfen (lacht) und dann schauen wir mal was die Zukunft bringt.  

**Vielen Dank für das Interview!** 

_Arne_: Sehr gerne und viel Spaß mit dem Game!

* * *



* * *

 