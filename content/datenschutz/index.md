---
title: Datenschutz
author: gamevista
type: post
date: 2009-07-07T20:51:27+00:00

---
<p align="left">
  <strong>Allgemeines</p> 
  
  <p>
    </strong>Wir nehmen den Schutz Ihrer persönlichen Daten sehr ernst.
  </p>
  
  <p>
    Personenbezogene Daten werden auf dieser Webseite nur im technisch notwendigen Umfang erhoben. In keinem Fall werden die erhobenen Daten verkauft oder aus anderen Gründen an Dritte weitergegeben.
  </p>
  
  <p>
    Als Diensteanbieter messen wir dem Schutz Ihrer Privatsphäre höchste Bedeutung zu. <br />Die Erhebung Ihrer Daten erfolgt im Rahmen der gesetzlichen Vorschriften. Über diese können Sie sich unter <a href="http://www.datenschutz.de/" target="_blank"><span style="color: #003366;">www.datenschutz.de</span></a> umfassend informieren.<br />Wir erheben und nutzen personenbezogene Daten nur, wenn Sie uns dazu Ihre Zustimmung gegeben haben. In der Regel müssen Sie für die Nutzung unseres Internetangebotes keine personenbezogenen Daten preisgeben.
  </p>
  
  <p>
    Für die Abwicklung bestimmter Dienstleistungen ist die Erhebung personenbezogener Daten notwendig. Situationsabhängig werden Name, Anschrift, Geschlecht, Geburtsdatum, Augenfarbe, Haarfarbe, Persöhnliche Informationen, Kundennummer, Telefonnummer, Faxnummer, Bankverbindung, Zahlungsart und E-Mail-Adresse erhoben und gespeichert.<br />Diese Daten werden ausschliesslich im Rahmen der Vertragszwecke und in einem Umfang verarbeitet und genutzt, der für die Begründung, Ausgestaltung, Änderung und Erfüllung des Vertragsverhältnisses erforderlich ist.
  </p>
  
  <p>
    <strong>Anonymisierte Nutzungsprofile</strong>
  </p>
  
  <p>
    Bei der Nutzung von Internetangeboten fallen nicht personenbezogene Daten an.<br />Diese Daten lassen in der Regel keine Identifizierung des Nutzers zu. Bei jedem Zugriff eines Nutzers auf eine Seite von gamevista.de und bei jedem Abruf einer Datei werden über diesen Vorgang Daten in einer Protokolldatei gespeichert. Die Speicherung dient internen systembezogenen und statistischen Zwecken. Außerdem verwenden wir diese Daten in pseudonymisierter Form, um unser Angebot für Sie noch attraktiver und interessanter zu gestalten. Es findet keine personenbezogene Verwertung statt.
  </p>
  
  <p>
    Im Einzelnen wird über jeden Abruf folgender Datensatz gespeichert:
  </p>
  
  <p>
    * Name der abgerufenen Datei<br />* Datum und Uhrzeit des Abrufs<br />* Übertragene Datenmenge<br />* Meldung, ob der Abruf erfolgreich war<br />* Beschreibung des Typs des verwendeten Webbrowsers<br />* Anfragende Domain<br />* Herkunftsland der Domain
  </p>
  
  <p>
    <strong>Cookies</strong>
  </p>
  
  <p>
    Wir verwenden sogenannte &#8222;Cookies&#8220;. Cookies sind Dateien, die auf Ihrer Festplatte abgelegt werden. Sollten Sie nicht wünschen, dass wir Ihren Computer wiedererkennen (Festplatten-Cookies), können Sie Ihren Browser so einstellen, dass er Cookies von Ihrer Festplatte löscht, alle Cookies blockiert oder Sie warnt, bevor ein Cookie gespeichert wird. Unter Umständen kann das Löschen von Cookies dazu führen, dass Sie nicht alle von uns angebotenen Dienste ohne Einschränkungen nutzen können.
  </p>
  
  <p>
    <strong>Weitergabe personenbezogener Daten an Dritte</strong>
  </p>
  
  <p>
    Wir werden personenbezogene Daten ohne Ihre Zustimmung nicht an Dritte weiterleiten, es sei denn wir sind aufgrund zwingender gesetzlicher Vorschriften zu einer Herausgabe gezwungen. Diese Zustimmung können Sie jederzeit widerrufen. Dazu genügt eine E-Mail an <a href="mailto:redaktion@gamevista.de">redaktion@gamevista.de</a>
  </p>
  
  <p>
    <strong>Sicherheit</strong>
  </p>
  
  <p>
    Alle unter gamevista.de befindlichen Daten, die sich auf unseren Servern befinden und dort &#8222;gehostet&#8220; (also aufbewahrt) werden, sind durch hintereinander geschaltete Sicherheitssysteme vor dem Zugriff Unberechtigter gesichert.
  </p>
  
  <p align="left">
    Mitarbeiter von gamevista.de und ihre System-Dienstleister überprüfen die Wirksamkeit des Schutzes täglich. Wir weisen darauf hin, dass die Datenübertragung im Internet (z.B. bei der Kommunikation per E-Mail) trotzdem Sicherheitslücken aufweisen könnte. Ein lückenloser Schutz der Daten ist aus diesem Grunde vor dem Zugriff durch Dritte nicht möglich.
  </p>
  
  <p>
    <strong>Änderung der Datenschutzerklärung<br /></strong><br />Jede Änderung der Datenschutzerklärung wird an dieser Stelle bekannt gegeben.<br />Sollte gamevista.de Ihre Daten für weitergehende Zwecke verwenden wollen, so werden wir Sie dafür um Ihre Erlaubnis bitten. Nur mit Ihrer ausdrücklichen Zustimmung kann sich so der Nutzungsbereich dieser Daten ändern.
  </p>
  
  <p>
    <strong>Auskunfts- und Widerspruchsrecht</strong>
  </p>
  
  <p>
    Ihr Vertrauen ist uns sehr wichtig. Daher möchten wir Ihnen jederzeit Rede und Antwort bezüglich der Verarbeitung Ihrer personenbezogenen Daten stehen. Sie können aus diesem Grunde jederzeit Auskunft über die von uns über Sie gespeicherten Daten erhalten. Ausserdem können Sie natürlich jederzeit Ihre Zustimmung zur Erhebung und Speicherung Ihrer personenbezogenen Daten durch die Firma gamevista.de widerrufen.
  </p>
  
  <p>
    Wenn Sie Fragen haben, die Ihnen diese Datenschutzerklärung nicht beantworten konnte oder wenn Sie zu einem Punkt dieser Datenschutzerklärung vertiefendere Informationen wünschen, wenden Sie sich bitte jederzeit per E-Mail an unseren Datenschutzbeauftragten:
  </p>
  
  <p>
    eMaill: <a href="mailto:redaktion@gamevista.de">redaktion@gamevista.de</a>
  </p>
  
  <p>
    oder einen Brief an: 
  </p>
  
  <p>
    <strong>Fragen und Anregungen</strong>
  </p>
  
  <p>
    Bitte schreiben Sie Ihre Fragen, Anregungen und Wünsche bezüglich Datenschutz an folgende E-Mail-Adresse: <a href="mailto:redaktion@gamevista.de">redaktion@gamevista.de</a>
  </p>
  
  <p style="margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px;">
    Oder als Brief:
  </p>
  
  <p style="margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px;">
    Frank Bechstein <br />Bahnhofstr. 11 <br />22880 Wedel <br />Germany
  </p>
  
  <p>
    Telefon: +49 4103 7018423<br />Kleinunternehmer
  </p>
  
  <p style="margin-top: 1em; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; padding: 0px;">
    &#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8211;<br /><strong>Finanzamt: FA Pinneberg</strong> <br /><strong>Steuernummer: 31/004/03170</strong>
  </p>