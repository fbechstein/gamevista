---
title: ' Painkiller: Resurrection (PC)'
author: gamevista
type: post
date: 2009-11-27T15:37:44+00:00
excerpt: '<img class="caption alignright size-full wp-image-1109" src="http://www.gamevista.de/wp-content/uploads/2009/11/small1.jpg" border="0" alt="Painkiller: Resurrection" title="Painkiller: Resurrection" align="right" width="140" height="100" />Wieder ein Spiel vom Publisher <a href="http://www.jowood.com/" target="_top">JoWooD</a>, der in letzter Zeit vor allem durch Spiele mit vielen Bugs, wenig Spielspaß und schlechter Qualität auf sich aufmerksam machte. Ein möglicher Grund für so schlechte Qualität ist zum Beispiel der Zeitdruck, damit ein Spiel noch das Weihnachtsgeschäft erwischt. Ob <strong>Painkiller: Resurrection </strong>diesem Druck zum Opfer gefallen ist und ob man Schmerzmittel braucht um das Spiel überhaupt zu spielen, haben wir für euch getestet.</p> '
featured_image: http://www.gamevista.de/wp-content/uploads/2009/11/small1.jpg

---
Meine Erwartungen waren niedrig, aber der Kontrast zu **Call of Duty Modern Warfare 2** ist so gravierend, dass es mir beinah die Sprache verschlagen hätte. Bereits das Menü, das nach dem Einlegen der DVD auf meinem Bildschirm erschienen ist, wirkte als hätte ich gerade ein Spiel von 1999 eingelegt. Auch die Installation brauchte im Grunde viel zu lange, vor allem da das Spiel gerade mal rund 4 GB einnimmt. Zum Vergleich **Borderlands** beansprucht rund 6 GB, **Call of Duty Modern Warfare 2** gleich 11 GB und **Star Wars The Force Unleashed** sogar ganze 25 GB.

Man sagt so schön, dass der erste Eindruck der wichtigste ist und leider stimmt es auch bei diesem Spiel. **Painkiller: Resurrection** wirkt nicht wie ein Spiel aus dem Jahre 2009. Das fängt beim Menü an, geht über die Grafik, das Gamedesign, das Storytelling und die Vertonung weiter und endet bei einem wohl absolut frustrierten Spieler.

Der Hauptcharakter heißt William „Wild Bill“ Sherman und die Geschichte beginnt mit seinem Ende. Er stirbt und an seinen Händen klebt das Blut vieler unschuldiger Menschen. Deshalb kämpft er dann von Level zu Level gegen Dämon, priesterähnliche Gestalten und immer noch mehr davon. Präsentiert wird die Geschichte mit vertonten Zwischenszenen, in denen sogenannte „Graphic Novell“ gezeigt werden. Das war zu Zeiten von Max Payne noch interessant, heute wirkt so etwas eher arm. Bei Anno 1404 wurde etwas ähnliches verwendet um die Geschichte zu erzählen – die Grafiken waren aber wenigstens teilweise animiert, was die Sache bedeutend ansprechender machte.



<a href="http://www.gamevista.de/wp-content/uploads/2009/11/2.jpg" rel="lightbox[painkiller2]"><img class="caption alignright size-full wp-image-1111" src="http://www.gamevista.de/wp-content/uploads/2009/11/small2.jpg" border="0" alt="Texturen sind eine Schwachstelle des Spiels" title="Texturen sind eine Schwachstelle des Spiels" align="right" width="140" height="100" /></a>Spielerisch ist es ein reinrassiger Ego-Shooter und mehr kann man einfach nicht erwarten. Es gibt zwei elementare Spielfunktionen: Schießen und springen! In den Leveln gibt es nun auch Geheimwege, in denen man zusätzliche Munition und Schätze finden kann. Auf der einen Seite eine gute Erweiterung, doch muss man jeden dieser Gänge „gesäubert“ haben um gegen den Endgegner antreten zu dürfen bzw. weiter gehen zu können. Das kann teilweise zu großem Frust führen. Die Gegner spawnen manchmal direkt vor dem Charakter. Auch so etwas sollte eigentlich vermieden werden. Die Entwickler haben sich nicht bemüht, dass auch nur ansatzweise zu vertuschen.

Auf der Rückseite der Hülle steht groß „Wenn es einen Gott gibt – dann ist er gegen dich!“. Ich weiß zwar nicht, ob es einen Gott gibt, aber meine Augen haben definitiv etwas gegen die Grafik dieses Spiels. Selbst auf den höchsten Einstellungen sieht das Spiel schlecht aus. Nostalgiker werden sich vielleicht daran erfreuen, aber für alle die wenigsten eine recht gute Grafik mögen, ist dieses Spiel definitiv zu hässlich. Auch von der „atemberaubender Physik“ habe ich nur wenig gesehen. Es gibt nur wenige zerstörbare Objekte und teilweise hängen die toten Gegner merkwürdig in der Luft herum.

<a href="http://www.gamevista.de/wp-content/uploads/2009/11/3.jpg" rel="lightbox[painkiller3]"><img class="caption alignright size-full wp-image-1113" src="http://www.gamevista.de/wp-content/uploads/2009/11/small3.jpg" border="0" alt="Lichter hingegen sind die größte, wenn auch kleine Stärke" title="Lichter hingegen sind die größte, wenn auch kleine Stärke" align="right" width="140" height="100" /></a>Der Sound rundet das Gesamterlebnis würdig ab. Die Vertonung wirkt veraltet, die Musik ist zwar passend aber auch eher unterdurchschnittlich. Dazu kommt die unglaublich nervige Meldung, dass eine neue Version verfügbar ist. Dies lässt sich nicht abschalten und während des gesamten Spielens bleiben die Worte eingeblendet. Nach anfänglichen Miseren mit fatalen Fehlern, ist die neuste Version relativ stabil. Hier wurde nachträglich ausgebessert.

Man kann bis zu drei Freunde mitnehmen auf dieses Abenteuer, ob man drei Freunde finden, die es spielen wollen, ist eine andere Frage. Es gibt zwei neue Waffen und ganze sechs neue Gegner(-typen). Überragend sind alle Neuerungen nicht.



**Fazit**

Für wen ist **Painkiller: Resurrection** interessant? Nur für die, die Painkiller und das Spielprinzip von reinrassigen Ego-Shootern mögen und nichts gegen eine stark angestaubte Grafik haben. Ich kann mir durchaus vorstellen, dass es ein paar Fanboys gibt, die sich freuen wieder mit dem „Painkiller“, was ein Name für eine Nahkampfwaffe ist, und der Pflockkanone auf Dämonjagd zu gehen.

Leider ist es aber für die breite Masse der Spieler nicht zu empfehlen, auch nicht für Spieler, die an sich gerne Ego-Shooter spielen. Wer **Crysis** mag, wird Augenkrebs beim Spielen von **Painkiller: Resurrection** kriegen. Leider ist das Spiel nicht der erhoffte Geheimtipp zu Weihnachten.

**Wertung: 49%**
