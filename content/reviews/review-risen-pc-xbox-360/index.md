---
title: ' Risen (PC, Xbox 360)'
author: gamevista
type: post
date: 2009-10-08T11:25:38+00:00
excerpt: '<img class="caption alignright size-full wp-image-834" src="http://www.gamevista.de/wp-content/uploads/2009/10/risen_packshot.png" border="0" alt="Risen" title="Risen" align="right" width="140" height="100" />Piranha Bytes sind zurück mit Ihrem langersehnten Rollenspiel Risen. Ob die Jungs aus dem Ruhrpott nach dem Bug Debakel um Gothic 3 Ihre Ehre wiederherstellen können, klären wir in unserem Test.</p> <p> </p> '
featured_image: /wp-content/uploads/2009/10/risen_packshot.png

---
 

**Es war einmal&#8230;**

So könnte die Geschichte um den namenlosen schiffbrüchigen Helden beginnen. Ihr beginnt das Abenteuer am Strand der Insel Faranga.  
Die Insel ist eine geheimnisvolle Insel, denn uralte Tempelruinen kamen aus dem Boden an die Oberfläche empor und keiner weiß warum. Die Ruinen verbergen jedoch viele Schätze, und da wo viele Schätze sind auch viele Schatzjäger, die sich gegenseitig bekämpfen. Die Konkurrenten könnte man auch in Fraktionen unterteilen &#8211; die Inquisition, die Banditen und die Magier.  
Mehr ist von der Geschichte leider nicht bekannt. Ähnlich wie bei Gothic 1, wird man auch bei Risen im &#8222;Regen stehengelassen&#8220; und muss zusehen, dass man selbst herausfindet was genau zu tun ist.

**Die Fraktionen**

Das System der Fraktionen ist aus den drei Gothic-Teilen bestens bekannt. Piranha Bytes scheint auch hier auf altbewährtes zurückzugreifen. Bei Risen gibt es drei Fraktionen. Die Inquisition ist die &#8222;Hauptfraktion&#8220; der Insel. Alles und jeder wird von dieser Organisation kontrolliert. Laut der Inquisition darf sich die Bevölkerung auf der Insel nicht frei bewegen &#8211; sollte man von der Inquisition erwischt werden, so drohen Konsequenzen in Form von Zwangsrekrutierung.   
Der Gegner der Inquisition sind die Banditen, die sich in den Sümpfen von Faranga niedergelassen haben. Diese versuchen die von der Inquisition besetzte Hafenstadt wieder zurückzuerobern. Als dritte Fraktion stehen die Magier bereit den Helden aufzunehmen.  
><img class="caption alignright size-full wp-image-835" src="http://www.gamevista.de/wp-content/uploads/2009/10/risenisland.png" border="0" alt="Risen - der Strand von Faranga" title="Risen - der Strand von Faranga" align="right" width="140" height="100" />Welcher Fraktion sich unser Held anschließt kann man innerhalb des ersten Kapitels entscheiden. Wenn man z.B. von der Inquisition erwischt wird, so wird man zwangsrekrutiert. Will man sich z.B. den Banditen anschließen, so müssen zuerst einige Quests wie z.B. das Vorsprechen bei dem Banditen Anführer Don Esteban erfüllt werden, bevor man sich als würdig erweist der Fraktion beizutreten.  
Ist man dann einer Fraktion beigetreten, so verläuft die weitere Storyline aus Sicht der gewählten Fraktion. Wie man merkt, sind auch in diesem Punkt die Piranhas zurück zu Ihren Wurzeln gekehrt. Genau wie bei Gothic 1 und 2 ändert sich die Geschichte entsprechend, je nach dem, welcher Fraktion man zugehörig ist.

**Die Welt von Risen**

Die Welt von Risen ist kleiner als bei Gothic 3, jedoch dichter. Man kann Gebirge erklimmen, in Höhlen herumkriechen. Die Insel kann man sich als eine mehrstufige Torte vorstellen. Dadurch wird der Eindruck eines großen Areals erweckt, so dass die dichte Story mit den vielen Quests auch viele Locations erhält. Oberhalb Farangas erwarten den Spieler Graslandschaften, Wälder, Seen, Wasserfälle und steile Klippen.  
Unterhalb Farangas düstere Höhlen und Dungeons. So findet man in jeder Ecke eine Quest, neue Gegner, Heilpflanzen u.v.m.  Im Gegensatz zu Gothic 3, wo man kilometerweit ohne eine Quest laufen musste, finden wir diesen (Rück-)Schritt sehr gut. Die Flora & Fauna von Risen ist nicht nur gefährlich, sondern auch &#8211; trotz der etwas nicht mehr zeitgemäßen Grafik &#8211; ganz hübsch anzusehen.   
Die Hafenstadt ist der Dreh- und Angelpunkt der Welt von Risen. Hier treffen alle Fraktionen aufeinander, hier erhält der Spieler die meisten Quests und hier trifft man mit Sicherheit die Entscheidung welcher Fraktion man sich anschließt.


**Die Quests**

<img class="caption alignright size-full wp-image-836" src="http://www.gamevista.de/wp-content/uploads/2009/10/risenhafen.png" border="0" alt="Risen - Im Hafen erwarten den Spieler eine Menge Quests" title="Risen - Im Hafen erwarten den Spieler eine Menge Quests" align="right" width="140" height="100" />Der Aufbau der Quests erinnert sehr stark an die ersten beiden Gothic Teile. Es gibt sehr viele Aufträge, die untereinander verschachtelt sind.  
Bei manchen Quests hat man die Freiheit selbst zu entscheiden, wie man die Lösung erreicht. Im Prinzip eine gute Idee, leider wird dies nicht zu häufig praktiziert, so dass die meisten Quests doch sehr linear in der Lösung verlaufen. Frage A nach B, gehe zu B, erhalte Auftrag für C und löse das Problem.  
Die Handlungsweise, wie man jede Quest löst und welcher Fraktion man loyal gegenüber steht, wirkt sich auf die anderen NPCs des Spiels aus.  
Je nachdem, welcher Fraktion man sich anschließt, gibt es unterschiedliche Quests, die dann jedoch zum selben Ziel führen. In den ersten beiden Kapiteln erwarten den Spieler eine Reihe von verschiedenen Quests, um die Story voranzutreiben. Die Gesamtstory selbst braucht leider einige Stunden, bis sie anfängt sich zu entfalten.   
Neben den Quests gibt es die Möglichkeit Fertigkeiten wie das Schmieden, die Jagd oder aber die Alchemie zu erlernen.

**Das Interface**

Das Interface besteht aus dem Inventar, dem Questlog, dem Charakterbildschirm und der Map. Alles scheint geordnet und ordentlich zu sein. Das Inventar ist unterteilt in diverse Kategorien wie Waffen, Rüstungen, Alchemie etc. Man kann das Inventar auch überladen, ohne eine Schatztruhe suchen zu müssen, um Gegenstände abzuladen. Wie bei jedem ordentlichen Rollenspiel, sind auch Quickslots vorhanden.  
Im Questlog findet man alle aktiven und erledigten Quests nebst einer Karte, wo alle relavanten Personen und Orte zu der jeweiligen Quest wiederzufinden sind. Ferner werden im Questlog alle Lehrer und Händler verzeichnet, die man bereits getroffen hat.  
<img class="caption alignright size-full wp-image-837" src="http://www.gamevista.de/wp-content/uploads/2009/10/risencombos.png" border="0" alt="Risen - es können je nach Skill verschiedene Kombos ausgeführt werden" title="Risen - es können je nach Skill verschiedene Kombos ausgeführt werden" align="right" width="140" height="100" />Im Charakterbildschirm werden Details zu dem jeweiligen Level, der Erfahrung, Handwerk usw. angezeigt.   
Die Map ist in die Weltkarte und Regonalkarte unterteilt. Diese ist zwar nicht detailreich, jedoch erfüllt voll und ganz ihren Zweck.

**Die Kämpfe**

Die Kämpfe wurden gegenüber Gothic 3 geändert &#8211; weg von einer sinnlosen &#8222;Hau den Lukas&#8220;-Kampftaktik. Bei Risen darf der Spieler mit der linken Maustaste zuschlagen und mit der rechten sich verteidigen bzw. parieren.  
Auf den ersten Blick scheint es kaum einen Unterschied zum Gothic 3 Kampfsystem zu geben, doch die Gegner sind intelligenter geworden und können ebenfalls parieren und den Schlägen ausweichen.   
Weiterhin steigt je nach vorhandenen Waffen-Skills die Möglichkeit verschiedene Kombos auszuführen. Nach einigen Level entstehen so nicht nur optisch schöne Kämpfe, sondern auch kritische Treffer mit denen der Gegner mehr Schaden einstecken muss.



**Die Technik**

<img class="caption alignright size-full wp-image-838" src="http://www.gamevista.de/wp-content/uploads/2009/10/risenruinen.png" border="0" alt="Risen - die geheimnisvollen Ruinen" title="Risen - die geheimnisvollen Ruinen" align="right" width="140" height="100" />Erst einmal gibt es zu sagen, dass Risen recht stabil läuft. Während des Tests ist das Spiel kein einziges mal abgestürzt. Hier haben die Jungs von Piranha Bytes aus den Fehlern der Vergangenheit gelernt bzw. standen unter einem Druck ein fehlerfreies Game abzuliefern.  
Optisch ist das Spiel leider etwas altbacken. Man erwartet heutzutage einfach eine andere Generation von Grafik. Vorteil der bestehenden Grafik &#8211; auch Spieler mit etwas schwächeren Rechnern bzw. Grafikkarten können das Spiel weitestgehend flüssig spielen, ohne gleich in den nächsten Laden rennen zu müssen um sich eine neue Grafikkarte zu kaufen.  
Im Spiel selbst sind einige Clippingfehler aufgetaucht, die aber bei der enormen Größe des Spiels kaum ins Gewicht fallen.

**Xbox 360 Version**

Bei unserem Test konnten wir ebenfalls die Xbox 360 Version anschauen, welche von der Softwareschmiede Wizarbox auf die Microsoft Konsole adaptiert wurde. Die Steuerung wurde dabei sehr gut auf das Gamepad umgesetzt. Leider wurde bei der Grafik sehr lieblos gearbeitet. Verwaschene Texturen, blockartige Kanten und Ecken, kaum Vegetation auf dem Boden wo die PC Version von der Flora & Fauna lebt.   
Weiteres Manko der Xbox 360 Version ist die kleine Schrift z.B. bei Gesprächen mit NPCs. Hier wurde leider komplett vorbeigedacht. Spieler, die eine Xbox 360 haben, sitzen meistens auf der Couch vor einer 3-4 Meter entferneten HD Flimmerkiste. Um die kleine Schrift entziffern zu können muss man entweder sehr gute Augen haben oder aber sich doch von der Couch bewegen. Ebenfalls sind die kleinen Symbole aus dem Inventar so klein, dass aus der Entfernung das Inventar wie ein bunter Block aussieht. Man kann hier nur hoffen, dass Wizarbox ein Patch nachliefert, der die grafischen Probleme behebt.

**Fazit**

<img class="caption alignright size-full wp-image-839" src="http://www.gamevista.de/wp-content/uploads/2009/10/mike_bjoern2_small.jpg" border="0" alt="Die Macher von Risen - Mike &#038; Björn (Piranha Bytes)" title="Die Macher von Risen - Mike &#038; Björn (Piranha Bytes)" align="right" width="140" height="100" />Risen ist im Großen und Ganzen ein ordentliches Spiel geworden. Leider fehlen mir hier Innovationen. Man hat das Gefühl, Piranha Bytes gehen einen Schritt zurück in die ersten Jahre, wo noch Gothic 1 & 2 ein Hit war &#8211; nach dem Motto &#8222;nehmen wir uns einfach mal die alten Spiele vor, dann können wir nichts falsch machen&#8220;. Eigentlich hätte ich gedacht, dass man mit Risen, dem Beginn einer größeren Serie, etwas mehr riskiert und etwas Neues ausprobiert &#8211; dem ist leider nicht so.   
Genug der Kritik, nun zum Positiven &#8211; das Spiel motiviert und überzeugt.  
Als alter Gothic Veteran fühlt man sich in die alten Gothic Zeiten zurückversetzt und ja, es macht Spaß. Von Stunde zu Stunde wird man mehr und mehr in die gehemnisvolle Insel hineingezogen, auch wenn die Geschichte erst nach zig Stunden Spielzeit anfängt sich zu entfalten.  
Habt Ihr das erste bis zweite Kapitel geschafft, erwartet Euch ein herrliches Rollenspiel.  
Sollte Risen 2 irgendwann entwickelt werden, so bitte, Ihr Jungs von Pirananha Bytes, riskiert etwas mehr sowohl was Story, Gameplay angeht, als auch die Grafik.

 