---
title: ' Medal of Honor'
author: gamevista
type: post
date: 2010-10-18T18:28:22+00:00
excerpt: '<img class="caption alignright size-full wp-image-2354" src="http://www.gamevista.de/wp-content/uploads/2010/10/moh2010.jpg" border="0" alt="Medal of Honor" title="Medal of Honor" align="right" width="140" height="100" />Publisher und Entwickler Electronic Arts führt den ehemals im 2. Weltkrieg spielenden Ego-Shooter <a href="http://www.medalofhonor.com" target="_blank" rel="nofollow">Medal of Honor</a> in die Moderne. Seit der Veröffentlichung des ersten Teils im Jahre 1999 ist viel passiert. Ob der Titel hält was er verspricht und an die alten erfolgreichen Tage der Serie anknüpfen kann, erfahrt ihr in unserer Review.</p> '
featured_image: /wp-content/uploads/2010/10/moh2010.jpg

---
Wie beim Genrekollegen Call of Duty: Modern Warfare wird nun auch im neuen <a href="http://www.medalofhonor.com/" target="_blank" rel="nofollow">Medal of Honor</a> heutige Ereignisse thematisiert. Neu hierbei ist, dass ein Kriegsspiel einen noch laufenden Konflikt skizziert. EA erhielt dafür in der Vergangenheit zahlreiche Kritik, den andauernden Krieg in Afghanistan als Thema für ein Spiel zu nutzen. Durch den Druck von Medien und Briefen von Angehörigen gefallener Soldaten wurde die Taliban-Partei kurzerhand in „feindliche Kämpfer“ umbenannt.

So schickt sie EA als Elitesoldat in die Krisenregion Afghanistan im Kampf gegen die Taliban. Mit einer Spielzeit von weniger als sechs Stunden ist der Solopart des Spiels eher dürftig ausgefallen und erreicht damit einen neuen Negativrekord in der Spielentwicklung. Klar liegt der Fokus auf dem Mehrspielerteil des Titels, dennoch sollte EA in Zukunft etwas weniger Geld in die Promotion stecken und den groß angekündigten Storymodus etwas mehr aufpeppen.

Ihr übernehmt unterschiedliche Rollen in verschiedenen Einheiten, die zusammen an einem Einsatz gegen feindliche Truppen teilnehmen. Als Hauptdarsteller gilt darunter die sogenannte Tier 1 Operator-Einheit. Eine Spezialeinheit die hinter feindlichen Linien agiert. Neben der Steuerung dieser Spezialeinheit, dürfen sie außerdem auf einen Ranger mit dem Namen Adams oder auf einen Helikopter zurückgreifen.

Dabei gilt es unter zahlreichen geskripteten Ereignissen im Feindesland Sabotageakte, Kampfeinsätze oder Flugmissionen zu absolvieren. Die Geschichte wird durch sehenswerte Zwischensequenzen weitergesponnen und ergänzt.

<p style="text-align: center;">
  <img class=" size-full wp-image-2355" src="http://www.gamevista.de/wp-content/uploads/2010/10/image001.jpg" border="0" width="600" height="360" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/image001.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/image001-300x180.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Die Missionen basieren aufeinander und sind inhaltlich zusammenhängend. Wenn ihr mit eure AFO-Einheit in schwere Gefechte geratet holen euch die US Ranger unter der Leitung des Charakters Adams wieder raus. Klar ist auch EA nicht von Heldenklischees gefeit und schreibt so dem ein oder anderen Charakter etwas zu viel Pathos zu.

Die Rahmenhandlung präsentiert sich dabei leider etwas langweilig. Es fehlt an so imposanten Inszenierungen wie in Modern Warfare. Ständig fragt man sich, ob EA hier eher schlecht als recht kopiert hat. Auch wenn der Publisher Activision das Rad mit Modern Warfare nicht neu erfunden hat, so wirkt das Produkt als fertiges Spiel und spannend in Szene gesetzt. Das lässt <a href="http://www.medalofhonor.com/" target="_blank">Medal of Honor</a> stark vermissen. Missionen bei Nacht,  wo ihr als Soldat feindliche Lager infiltrieren müsst oder diverse Scharfschützenmissionen bringen Abwechslung , dennoch erwartet man mehr von einem innovativen Spiel.

Man merkt <a href="http://www.medalofhonor.com/" target="_blank" rel="nofollow">Medal of Honor</a> an das auf eine spannende Geschichte, bzw. ein Drehbuch, verzichtet wurde. Es gibt keinen Oberschurken oder gar ein genaues Ziel in der Hauptstory. Auch sind die vorkommenden Hauptcharaktere nur sehr ungenau gezeichnet. So werden ihre Vorgesetzen, ob General oder Truppführer, nur grob umrissen und ihre Motive bleiben verschwommen.

Auch fehlen uns diverse Sounds, wie z.B. beim Genre-Kollegen Bad Company 2, die das Spiel aufpeppen würden. Sehr gut finden wir den Aufbau der Kampagne. In abwechslungsreichen Missionen erobern wir ein Flugfeld wo unsere Kommandobasis eingerichtet wird oder infiltrieren eine Taliban-Festung auf der Suche nach Informationen. Als Fahrzeuge stehen euch dabei der schon angesprochene AH64-Apache Helikopter oder ATV Quad durch die Landschaften Afghanistans. Dabei wirkt <a href="http://www.medalofhonor.com/" target="_blank">Medal of Honor</a> realistischer, dabei weniger cineastischer, als z.B. die Modern Warfare-Reihe. Ohne viel Heldenepos scheint es nur darum zu gehen, eine eher nüchterne Reflektion des Afghanistan-Szenarios zu präsentieren. Kinoreife Momente sucht man so vergeblich.

<p style="text-align: center;">
  <img class=" size-full wp-image-2356" src="http://www.gamevista.de/wp-content/uploads/2010/10/image003.jpg" border="0" width="600" height="360" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/image003.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/image003-300x180.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Die Gegner-KI ist wie üblich in Shootern nicht die Beste. Gerade aus dem Grund weil wir nicht allzu viele Treffer einstecken können, müssen wir trotzdem das ein oder andere Mal in Deckung gehen. Auch tendieren ihre Kollegen gelegentlich dazu ihr Schussfeld zu kreuzen. Eine Innovation wäre auch hier sehr wünschenswert.

Wie es sich für einen echten Shooter gehört darf eine auswahlreiche Waffenkammer natürlich nicht fehlen. Über diverse Sturm-, Maschinen- bis hin zu Scharfschützengewehren, <a href="http://www.medalofhonor.com/" target="_blank">Medal of Honor</a> macht hier alles richtig. Munition erhaltet ihr bei schweren und langen Feuergefechten von euren Teamkollegen oder über fallengelassene Waffen der Gegner.

Technisch präsentiert sich der Titel grundsolide. Wenn man bedenkt, dass die felsigen Berge Afghanistans eigentlich nicht viel Atmosphäre hergeben  dürften, hat sich EA hier mächtig ins Zeug gelegt. Durch geschickten Einsatz von Beleuchtung und Partikeleffekten, detailreichen Landschaften sowie anmutigen schneebedeckten eisigen Bergen, schafft es der Entwickler ein phantasiereiches Bild des Landes abzugeben.    
Dennoch ist das für einen Titel der es mit der Konkurrenz aufnehmen will zu wenig. Modern Warfare zeigte schon in der Vergangenheit ein besseres Bild.

Den gleichen Aspekt gibt es beim Sound auszusetzen. Die Waffengeräusche und Explosionen hämmern durchaus anständig aus den Boxen. Dennoch sind Genre-Kollegen ein Tick weiter vorn.   
Die deutsche Synchronisation lässt aber keine Wünsche offen und wurde professionell umgesetzt. Dieser Punkt trägt stark zur Atmosphäre bei.

Im echten Online-Modus, wo die KI mal Pause hat und ihr euch gegen echte Gegner zur Wehr setzen müsst, dürft ihr euch mit bis zu 24 Spielern in vier Spielmodi auf acht Karten tummeln. Darunter ist eine Art Team-Deathmatch, der Kampfeinsatz-Modus mit Missionszielen, der Domination-Modus mit drei neutralen Flaggenpunkten auf der Karte die ihr halten müsst und der typische Search & Destroy Modus mit zwei Zielen auf der Karte die gesprengt werden müssen.    
Habt ihr eine Karte und Spielmodus gewählt, müsst ihr euch noch für eine der drei Klasse entscheiden. Neben dem Schützen mit leichtem Sturmgewehr, gibt es noch den Spec Ops mit leichtem bzw. schwerem Sturmgewehr und den Scharfschützen. Ähnlich wie in Modern Warfare erhaltet ihr für erzielte Punkte z.B. Luftschläge. Dazu bringt das Erfahrungspunktesystem neue Waffen und Ausrüstungsgegenstände wie Visiere oder Magazine. Archievements gibt es dazu noch oben drauf.

<p style="text-align: center;">
  <img class=" size-full wp-image-2357" src="http://www.gamevista.de/wp-content/uploads/2010/10/image006.jpg" border="0" width="600" height="360" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/image006.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/image006-300x180.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

**Fazit**

Mit dem prägnanten Titel <a href="http://www.medalofhonor.com/" target="_blank" rel="nofollow">Medal of Honor</a> wollte Electronic Arts den Ruhm der alten Tage aufleben lassen. Leider tut sich der der Publisher und Entwickler damit dermaßen schwer, dass man die Gelder die in die Promotion des Spiels geflossen sind besser in mehr Entwicklungszeit geflossen wären. Das durchaus kritikwürdige Szenario bleibt bei unserer Review außen vor. Jeder sollte selbst entscheiden ob er aktuelle Konflikte auf dem PC oder der Konsole erleben möchte. Der Solopart von <a href="http://www.medalofhonor.com/" target="_blank" rel="nofollow">Medal of Honor</a> ist solide und kurzweilig. Leider sind knapp 6 Stunden Spielzeit definitiv zu wenig für einen Vollpreistitel. Klar machen Genre-Kollegen es nicht unbedingt besser. Sie inszenieren ihre Geschichte, die sie erzählen wollen, aber wesentlich bildgewaltiger und spannender. Richtige Spannung und Höhepunkte kommen in <a href="http://www.medalofhonor.com/" target="_blank" rel="nofollow">Medal of Honor</a> nicht wirklich auf. Alles in allem ist der Shooter damit ein solides Spiel was kaum Innovationen mit sich bringt. Wer sich damit noch zu unsicher in der Kaufentscheidung ist, sollte die Veröffentlichung von **Call of Duty: Black Ops** im November abwarten und dann vergleichen.

<span style="text-decoration: underline;"><strong>Wertung 79%</strong></span>

<p style="text-align: center;">
  <img class=" size-full wp-image-2358" src="http://www.gamevista.de/wp-content/uploads/2010/10/image002.jpg" border="0" width="600" height="360" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/image002.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/image002-300x180.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p> 