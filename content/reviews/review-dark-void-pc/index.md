---
title: ' Dark Void (PC)'
author: gamevista
type: post
date: 2010-01-28T01:09:17+00:00
excerpt: '<img class="caption alignright size-full wp-image-1348" src="http://www.gamevista.de/wp-content/uploads/2010/01/small_dv_held.jpg" border="0" alt="Dark Void (Dunkle Leere)" title="Dark Void (Dunkle Leere)" align="right" width="140" height="100" /><strong>Dark Void</strong> wurde mit vielen Vorschuss-Lorbeeren im Jahr 2010 begrüßt. Die PR-Abteilung ist auch der festen Meinung, dass gleich drei Arten von Kämpfen so viel Abwechslung bedeutet, dass das Spiel gar nicht schlecht sein kann. Ob das so ist und ob die Vorschuss-Lorbeeren wirklich gerechtfertigt waren, lest ihr in unserer Review.</p> '
featured_image: /wp-content/uploads/2010/01/small_dv_held.jpg

---
**Dark Void** bietet auf den ersten Blick ein unverbrauchtes Setting mit einer unverbrachten Geschichte – aber leider nur auf den ersten Blick. Wir übernehmen die Rolle von Will, der Pilot ist und um 1930 lebt. Was war kurz nach 1930? Genau, der zweite Weltkrieg. Will und Ava, die weibliche Hauptrolle im Spiel, fliegen über das Bermuda-Dreieck und laden auf einer Insel eher unfreiwillig Not. Im Spielverlauf wird schnell klar, dass die beiden nicht mehr auf der Erde sind. In dem Paralleluniversum gibt es drei Fraktionen: Die Einheimischen (zurückgeblieben), die Aliens namens &#8222;Die Beobachter&#8220;, die natürlich böse sind, und die Widerstandskämpfer, die ganz dringend unsere Hilfe brauchen. So entsteht eine unglaublich langweilige Story in einem unglaublich oft gewählten Zeitabschnitt mit Wendepunkten, die so vorhersehbar wie langweilig sind.

Leider bietet **Dark Void** auch grafisch eher wenig. Die Charaktermodelle stechen dabei besonders negativ hervor, ebenso wie die Effekte im Spiel. Einzig die Level können mit hohem Detailgrad und vielen hübschen Details punkten. Leider sind diese viel zu linear und schränken die Spielmechanik damit zu sehr ein.

<a href="http://www.gamevista.de/wp-content/uploads/2010/01/dv_boden.jpg" rel="lightbox[dv2]"><img class="caption alignright size-full wp-image-1351" src="http://www.gamevista.de/wp-content/uploads/2010/01/small_dv_boden.jpg" border="0" alt="Dark Void spielt sich am Boden wie ein Standard-Third-Person-Shooter" title="Dark Void spielt sich am Boden wie ein Standard-Third-Person-Shooter" align="right" width="140" height="100" /></a>Spielerischen setzen die Entwickler [Airtight Games][1]<span style="font-family: Arial;"><strong></strong></span>, und noch viel mehr die PR-Leute, auf drei Arten des Kampfes. Da haben wir zum einem den Standard am Boden. Hier kämpfen wir uns in der dritten Perspektive von Deckung zu Deckung und schießen auf alles, was sich bewegt. Hierbei offenbaren sich zwei Schwächen: Schießen aus der Deckung ist so präzise, dass es wie cheaten wirkt und der Nahkampfangriff ist zu effektiv. Es genügt oft auf einen Feind zuzulaufen und drei Mal auf ihn einzuschlagen.

Dazu kommt der &#8222;vertikalen Kampf&#8220;. Will schwingt sich, dank dem Jetpack auf seinem Rücken an einen Vorsprung oder Ähnliches und springt wieder von Deckung zu Deckung. Es ist eine interessante Idee den Kampf aus der einfachen horizontalen Sicht auch in die Vertikale zu tragen, doch leider ist dies in **Dark Void** nur an einigen Stellen möglich. Spontan in einem Gebäude die Kampfachse ändern und dann an den Wänden durch den Raum springen, darf der Spieler nicht.

Zu letzt haben wir dann noch einige Level, wo der Spieler fliegen darf oder muss &#8211; je nachdem wie man es sieht. Dies spielt sich an sich ganz gut ist aber auf Dauer nicht abwechslungsreich genug. Im Grunde geht es immer darum möglichst schnell alle Feinde zu töten. Zusätzlich ist das Fliegen anfangs gewöhnungsbedürftig, doch es macht später durchaus Spaß durch die Lüfte zu gleiten:

<p style="text-align: center;">
  <img class=" size-full wp-image-1352" src="http://www.gamevista.de/wp-content/uploads/2010/01/dv_fliegen.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/01/dv_fliegen.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/01/dv_fliegen-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



<a href="http://www.gamevista.de/wp-content/uploads/2010/01/dv_nahkampf.jpg" rel="lightbox[dv3]"><img class="caption alignright size-full wp-image-1354" src="http://www.gamevista.de/wp-content/uploads/2010/01/small_dv_nahkampf.jpg" border="0" alt="Cool sieht es schon aus, aber es macht das Spiel leider zu einfach!" title="Cool sieht es schon aus, aber es macht das Spiel leider zu einfach!" align="right" width="140" height="100" /></a>Spielerisch bietet es also drei Arten des Kampfs, die das Spiel relativ abwechslungsreich machen. Leider sind die einzelnen Bereiche aber nicht abwechlungsreich und auch in der Vertikalen geht es nur um das Eine. Der Spieler erhält für jeden getöteten Feind sogenannten Technik-Punkte, die er dann in Upgrades investieren kann. Warum es auf einmal Upgrades für Wills Waffen und Jetpack gibt, fragt der Spieler sich vergebens – eine Antwort findet man im Spiel nicht. Ernsthafte Neuerungen gibt es aber auch nicht. Einzig die Präzision und der Schaden werden erhöht. Ausgenommen ist hierbei das Jetpack, dass auf dem höchsten Level, also Level 3, mit kleinen Raketen bestückt wird &#8211; was in der letzten Mission sehr hilfreich ist.

**Fazit**

**Dark Void** bringt ein paar Neuerungen mit, aber leider retten die das Spiel auch nicht mehr. Es fehlt an allen Enden: Die Story ist zu kurz, langweilig und vorhersehbar, die Steuerung ist nicht präzise genug, die RPG-ähnlichen-Elemente sind unrealistisch und zu klein ausgefallen, die Dialoge sind öde und die Grafik wirkt angestaubt.    
Mit einer nur rund 7 stündigen Kampagne, keinem Mulitplayermodus und fehlender deutscher Sprachausgabe ist **Dark Void** erst ein Kauftipp, wenn es für unter 15 € verfügbar ist.

Plan B Entertainment, die Produktionsfirma von Brad Pitt, hat sich bereits vor dem Release des Spiels, die Filmrechte gesichert und könnte jetzt einen Film zu **Dark Void**, was soviel heißt wie dunkle Leere, produzieren. Es bleibt zu hoffen, dass der Film eine interessantere Geschichte bekommt und nicht auch in gähende Leere umbenannt werden müsste.

<p style="text-align: center;">
  <img class=" size-full wp-image-1355" src="http://www.gamevista.de/wp-content/uploads/2010/01/dv_ritter.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/01/dv_ritter.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/01/dv_ritter-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Beeindruckende Gegner gibt es in **Dark Void** durchaus, doch leider machen sie die Geschichte auch nicht wirklich spannender.

<span style="text-decoration: underline;"><strong>Wertung: 71%</strong></span>