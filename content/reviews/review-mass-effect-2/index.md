---
title: ' Mass Effect 2'
author: gamevista
type: post
date: 2010-02-01T17:31:04+00:00
excerpt: '<img class="caption alignright size-full wp-image-401" src="http://www.gamevista.de/wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg" border="0" alt="Mass Effect 2" title="Mass Effect 2" align="right" width="140" height="100" />Mit <strong>Mass Effect 2</strong> will der Entwickler <a href="http://www.bioware.com/">BioWare</a>, zusammen mit dem Publisher <a href="http://www.electronic-arts.de/">Electronic Arts</a>, das erste Spiele Highlight des neuen Jahres präsentieren. Schon der erste Teil setzte mit der imposanten Story, schicken Grafik und jeder Menge Action neue Akzente im Rollenspiel-Genre. Ob der zweite Teil das liefert was er verspricht wollen wir euch nun auf den folgenden Seiten ausführlich berichten.</p> '
featured_image: /wp-content/uploads/2009/08/masseffect2_screenshot_012_1280x720_small.jpg

---
**Mass Effect** ist eine Mischung aus Science-Fiction Rollenspiel und taktischem Shooter. Dabei ist der zweite Teil der geplanten Trilogie um einiges actionreicher und viel düsterer. Dies zeigt auch das gelungene Intro das [BioWare][1] präsentiert. Die Normandy, das Raumschiff des Protagonisten Commander Shepard, wird von einer unbekannten Macht angegriffen und zerstört. Auf dem Weg zur Rettungskapsel muss Shepard durch ein flammendes Inferno flüchten, wird aber Aufgrund der Strukturschäden am Schiff ins All gezogen. Unser Held „überlebt“ dies nur knapp und man findet sich kurz darauf auf einer Art Krankenstation wieder. 

**Mass Effect 2** bietet natürlich jede Menge neuer Schauplätze, sowie alter und neuer Kollegen, die ihm im Kampf zur Seite stehen. Weiterhin gibt es einen Schiffscomputer auf der neuen Normandy, der am Anfang doch sehr geheimnisvoll wirkt und uns nur unfreiwillig Informationen mitteilt. Stets hat man das Gefühl in eine Welt eintauchen zu dürfen, die mehr als ein paar hineinprogrammierte Charaktere bietet. So ist unserem altbekannten Piloten Joker der Bordcomputer E.D.I. nicht sehr sympathisch. Die Entwickler von [BioWare][1] ****haben jedem Charaktere glaubhaft Leben eingehaucht. Manche der Charaktere sind absolut schräg, andere zurückhaltend und manche verbergen auch Geheimnisse vor dem Spieler. So treffen wir auf unsere ersten Weggefährten, die der Cerberus Organisation angehören. Dies eine eine Pro-Mensch-Organisation, die bereits im ersten Teil, da aber eher eine negative Rolle spielte. In einer angenehmen Fülle von Dialogen wird die Story dann mit den üblichen Wahlmöglichkeiten, positiv bzw. negativ, eingestimmt.

<p style="text-align: center;">
  <img class=" size-full wp-image-1399" src="http://www.gamevista.de/wp-content/uploads/2010/02/engineer-01-o.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/engineer-01-o.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/engineer-01-o-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Im Auftrag eines undurchsichtigen Charakters Namens „Der Unbekannte“, der die Cerberus Organisation zu leiten scheint, wird uns über ein Art Alien-Insektenvolk berichtet die Menschen sammeln und entführen. Die Kollektoren, so der Name der Rasse, vernichten dabei immer mehr Kolonien der menschlichen Rasse. Commander Shepard soll die Beweggründe aufdecken und das Vorhaben unterbinden. Natürlich sind die aus dem ersten Teil bekannten Reaper wieder mit von der Partie. Hier stellt sich die Frage, für diejenigen die den ersten Teil noch nicht gespielt haben, ob sie sich zuerst die Vorgeschichte zum zweiten Teil zu Gemühte führen sollten. Wir empfehlen diesen Schritt, denn der erste Teil von Mass Effect bietet immer noch eine ansehnliche Grafik und macht so noch mehr Lust auf den zweiten Teil.



Wie im ersten Teil stellen wir uns aus den im Spiel angetroffenen Charakteren, einen Trupp aus Shepard und zwei computergesteuerten Kollegen zusammen, die uns mit Waffen- bzw. mentaler Gewalt unterstützt. Keinesfalls sind dies alles Helden. Rekrutiert wird hier was kämpfen kann. Darunter sind Schwerverbrecher, Attentäter oder Forschungssubjekte. Allesamt interessante Charaktere mit Ecken und Kanten.

Daraus entwickeln sich dann Nebenaufträge, die wir für unsere neuen Mitglieder erfüllen müssen um deren Vertrauen zu gewinnen. Leider sind die Missionen von **Mass Effect 2** oftmals als Schlauchlevel aufgebaut. Dies mindert aber selten den Spaß am Spiel, denn dafür gibt es jede Menge abwechslungsreiche Gebiete zu entdecken.

Wie es sich für ein gutes Rollenspiel gehört kann man natürlich seinen Protagonisten per Editor modifizieren. Ob Aussehen, Geschlecht oder Klasse. Es stehen wieder vielseitige Varianten zur Verfügung. Wie schon bereits im Vorfeld berichtet wurde, kann man den erstellten Charakter aus dem ersten Teil in **Mass Effect 2** importieren. Natürlich starten sie dabei immer mit Level 1. Fähigkeiten werden beim Import nicht übernommen. Die sechs Klassen Engineer, Adept, Infiltrator, Sentinel, Vanguard und Soldier bieten unterschiedlichste Spielweisen und begrenzen dabei die Wahl der Waffen bzw. Fähigkeiten.

<p style="text-align: center;">
  <img class=" size-full wp-image-1400" src="http://www.gamevista.de/wp-content/uploads/2010/02/miranda-02-o.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/miranda-02-o.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/miranda-02-o-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Das Setting von **Mass Effect 2** wirkt von Anfang an düster, schmutzig und erwachsen. So wird Commander Shepard gleich zu Beginn der Story in den beeindruckenden Afterlife Club auf der Station Omega geschickt. Dort treiben sich allerhand Gesindel und zwielichtige Gestalten herum. Hier soll unser Held nach Hintergrundinformationen und neuen Teammitgliedern Ausschau halten. Wie im ersten Teil, lebt **Mass Effect 2** davon, dass man sich mit vielen Charakteren unterhalten kann. **Mass Effect 2** entwickelt dabei eine Tiefe, die es nicht oft in einem Rollenspiel gibt. Die Welt ist laut, rau und geheimnisvoll. Zwar liefert nicht jeder NPC brauchbare Informationen, aber es macht Spaß sich durch die Multiple-Choice Dialoge zu klicken.



In **Mass Effect 2** wird wie im Vorgänger per Schulterkamera gesteuert. Kämpfe kann man jederzeit pausieren, um wichtige taktische Einstellungen vorzunehmen. Irgendwie wirkt der zweite Teil wesentlich schneller, dynamischer und actionreicher.

So sucht man mit seinen zwei ausgesuchten KI-Kollegen Deckung auf wenn man unter Beschuss kommt. Dabei benehmen sich die computergesteuerten Charaktere manchmal recht unbeholfen. So springen unsere Kollegen aus der besten Deckung direkt ins Sperrfeuer der Gegner bzw. Gegner reagieren auf lange Entfernungen nur spärlich auf unser Scharfschützengewehr. Leider vergibt **Mass Effect 2** hier durchaus Potential. Immer weniger nutzen wir die Taktikfunktionen des Pausenmodus. Dieser wurde leider um ein paar Features abgespeckt. Weiterhin werden die Spezialtalente, wie das überlasten der Schilde oder schweben lassen der Gegner, nur selten gebraucht und scheinbar nur auf höheren Schwierigkeitsgraden benötigt. Auch ist das Talentsystem merkbar ausgedünnt. So konnte man im ersten Teil noch den Umgang mit bestimmten Waffengattungen trainieren. Wünschenswert wären Modifikationen der Waffen gewesen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1401" src="http://www.gamevista.de/wp-content/uploads/2010/02/minuteman_screenshot.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/minuteman_screenshot.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/02/minuteman_screenshot-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

**Mass Effect 2** bietet Grafik auf sehr hohem Niveau, die sich in den Dialogen und Kampfszenen widerspiegelt. Zwar bieten die Effekte in den Kämpfen nichts Neues. Die Dialoge hingegen sind mit den gelungenen Kameraeinstellungen ein Augenschmaus. So sind die cineastischen Bilder der Gespräche zwischen Charakteren, auch in Nebenmissionen, immer wieder beeindruckend. Hier wird mit Details nicht gegeizt und die Grafik-Engine entfaltet so ihre volle Leistung. Ladezeiten zwischen den Level, gerne genutzt in den Aufzugfahrten der Normandy, sind erfreulich kurz gehalten. Klar ist die genutzte Unreal Engine 3 schon etwas in die Tage gekommen. Die Entwickler von **Mass Effect 2** schaffen es aber daraus eine zeitgemäße Grafik zu zaubern.



**Fazit und Wertung**

[BioWare][1] schafft es mal wieder seinem Ruf gerecht zu werden und liefert mit **Mass Effect 2** ein hochklassiges Actionrollenspiel im Science-Fiction Setting ab. Mittlerweile ist man nichts anderes von den Mitarbeitern des Studios gewohnt. Nach dem grandiosen Dragon Age: Origins setzt sich die Serie von Spielen fort, die eine packende Geschichte mit vielseitigen und tiefgründigen Charakteren erzählen und das mit hübscher Grafik verbinden. **Mass Effect 2** ist ein wirklich großartiges Spiel, dass es aber leider nicht zur Perfektion geschafft hat. In den punkten Grafik und Bedienung wurde der erste Teil zwar konsequent weiterentwickelt, es fehlen aber hier und da Innovationen und der Drang nach mehr Modifikationen, die einem Rollenspiel gerecht werden würden. Auch die KI ist alles andere als perfekt, geschweige denn das Leveldesign, was durch seine starke Linearität einer der größten Kritikpunkte ist. Verzweifelt versucht man weitere Kritikpunkte an diesem doch sehr gutem Spiel zu finden &#8211; selbst die größsten Kritiker werden aber nur selten fündig werden. [BioWare][1] hat mit dem zweiten Teil der Mass Effect Trilogie trotz alle dem sein Ziel erreicht: Ein hochwertiges Produkt und damit ein gutes Spiel zu entwickeln, was dieses Jahr nur selten übertroffen werden dürfte.  
****  
<span style="text-decoration: underline;"><strong>Wertung: 89%</strong></span>