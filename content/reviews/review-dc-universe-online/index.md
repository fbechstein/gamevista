---
title: 'DC Universe Online'
author: gamevista
type: post
date: -001-11-30T00:00:00+00:00
excerpt: '<img class="caption alignright size-full wp-image-1932" src="http://www.gamevista.de/wp-content/uploads/2010/06/dcuo.jpg" border="0" alt="DC Universe Online" title="DC Universe Online" align="right" width="140" height="100" />DC Universe Online, oder auch "DCUO" für die schreibfaulen Gamer unter euch, ist ein weiterer Ausflug ins Genre der MMORPGs, wie sie in letzter Zeit mit fast schon beängstigender Dichte auf den Markt kommen. Die Grundzeichen stehen gut: Die DC Universe Lizenz verspricht viel Atmosphäre und potentiell spannende Quests und Handlungsbögen. <br />Ob es am Ende für ein rundum gelungenes MMO reicht, zeigt der Test.</p> <p> </p> '
draft: true
featured_image: /wp-content/uploads/2010/06/dcuo.jpg

---
**<span style="font-size: medium;">Superman, Batman und Wonder Woman</span>**

Das DC Universum ist auch hierzulande bekannt, wenn auch vielleicht nicht unter diesem Namen. Gemeint ist mit diesem Begriff das Universum derjenigen Superhelden, die in den USA von DC Comics &#8222;betreut&#8220; werden. Darunter fallen namhafte Gestalten wie Superman, Batman, Wonder Woman, The Flash und viele weitere Helden unserer Zeit. Im Grunde genommen kann mit so einer Lizenz nicht viel falsch gemacht werden, zumindest was die Geschichte von DC Universe Online betrifft. Doch mit ein wenig Geschick können selbst die besten Franchises in Grund und Boden programmiert werden. Bei DC Universe Online ist dies glücklicherweise nicht der Fall.


<img class="caption size-full wp-image-2565" src="http://www.gamevista.de/wp-content/uploads/2011/03/dcuniverseon_600.jpg" border="0" alt="DC Universe Online" title="DC Universe Online" width="600" height="337" srcset="http://www.gamevista.de/wp-content/uploads/2011/03/dcuniverseon_600.jpg 600w, http://www.gamevista.de/wp-content/uploads/2011/03/dcuniverseon_600-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" /> 

Wie bei den zahlreichen Genrevertretern beginnt auch bei DC Universe Online alles mit der Gestaltung des eigenen Charakters &#8211; der in diesem Fall ein potentieller Superheld ist. Hauttyp, Frisur und die Startausrüstung könnt ihr neben einigen weiteren Modifikationen komplett selbst erstellen. Der Editor ist somit zwar nicht ganz so extrem umfangreich wie beispielsweise bei EverQuest 2 ausgefallen, aber das muss auch gar nicht sein. Die verschiedenen Klassen teilen sich bei DC Universe Online in superheldenkonforme Varianten ein. Ein Batman-ähnlicher Held verfügt über vorbildliche Kampfkunst und einige Items in seiner Ausrüstung, die den Gegnern schwer zusetzen können; wollt ihr näher an Superman sein, dürft ihr stattdessen fliegen und mit den Fäusten Prügel austeilen.  
Zusätzlich muss ein sogenannter Mentor gewählt werden, welcher bestimmt, wo das Spiel für den Spieler losgeht. Helden wählen Superman oder Batman als Mentor, Schurken entscheiden sich eher für den Joker oder Lex Luthor. Dies hat Auswirkungen auf die Stadt, in welcher ihr startet, und die anschließenden Quests.

**<span style="font-size: medium;">Kampfsystem</span>**

Eine besondere Erwähnung muss dem Kampfsystem zugesprochen werden. Dies ähnelt ein wenig demjenigen System, das bereits in Age of Conan Anwendung findet. Statt stets dieselben Fähigkeiten auf ein Ziel loszulassen und mit wenigen Nummerntasten alles bedienen zu können, steuern sich die Kämpfe in DC Universe Online eher wie in einem Beat &#8218;em Up wie Street Fighter 4. Das angelegte Equipment spielt zwar eine maßgebliche Rolle, wenn es um die Berechnung von ausgeteiltem und eingestecktem Schaden geht, doch gleichzeitig wollen bestimmte Angriffskombos gelernt, eingesetzt und auch abgewehrt werden. Daraus ergibt sich, dass sich &#8211; ein Novum für MMOs &#8211; DC Universe Online am besten mit einem Gamepad steuern lässt. Das Anvisieren der Gegner gelingt durch die Tab-Taste tadellos, in Kämpfen ist das Gamepad der Maus & Tastatur Kombination dann tatsächlich überlegen. Zu Beginn mag dies äußerst gewöhnungsbedürftig klingen, in der Praxis funktioniert es nach einer gewissen Eingewöhnungszeit tadellos


<img class="caption size-full wp-image-2566" src="http://www.gamevista.de/wp-content/uploads/2011/03/superman_600.jpg" border="0" alt="Superman" title="Superman" width="600" height="337" srcset="http://www.gamevista.de/wp-content/uploads/2011/03/superman_600.jpg 600w, http://www.gamevista.de/wp-content/uploads/2011/03/superman_600-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" /> 

Die Quests, eines der Herzstücke eines jeden MMOs, sind komplett vertont, inklusive der deutschen Sprecher aus den bekannten Hollywoodfilmen wie The Dark Knight. Bei der Mechanik dieser Quests gewinnt DC Universe Online zwar keine Innovationspreise &#8211; töte zehn Gegner, sammle eine bestimmte Anzahl Gegenstände, beseitige einen bestimmten NPC -, doch durch die gewaltige Atmosphäre und die vorbildliche Präsentation macht es dennoch sehr viel mehr Spaß als bei anderen, namenlosen MMORPGs.



Instanzen gibt es wie bei jedem aktuellen MMO auch bei DC Universe Online. Hier können diese Bereiche von zwei, vier oder acht Spielern betreten werden. Ein kleines Problem gibt es bei der Schwierigkeit dieser Instanzen, denn bis etwa Level 17 ist kein wirklich taktisches Vorgehen nötig &#8211; bei insgesamt 30 Leveln, die ihr als Spieler erreichen könnt. Das ist ein wenig schade, denn mehr als stumpfes Drauflosprügeln wird hier kaum geboten. Erst gegen Ende zieht der Schwierigkeitsgrad deutlich an. Dann ist Planung und Taktik gefragt, wenn ihr nicht schon nach wenigen Minuten scheitern wollt. Ab Level 30 gibt es dann die gewohnten wirklich schweren Bosse und die damit verbundene beste Ausrüstung im Spiel zu ergattern.

 

 