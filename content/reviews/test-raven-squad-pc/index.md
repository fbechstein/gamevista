---
title: 'Test: Raven Squad (PC)'
author: gamevista
type: post
date: 2009-09-04T22:47:55+00:00
excerpt: '<img class="caption alignright size-full wp-image-608" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallrs2.jpg" border="0" alt="Die Level sind immer linear aufgebaut, was keinen Platz für taktische Spielereien lässt." title="Die Level sind immer linear aufgebaut, was keinen Platz für taktische Spielereien lässt." align="right" width="140" height="100" />Euch hat es schon immer geärgert, dass sich eure virtuellen Kollegen in einem Shooter dumm verhalten? Dann könnte „Raven Squad“ genau das Spiel für euch sein – eigentlich zumindest.</p> <p>GamingExcellence nominierte <strong>Raven Squad: Operation Hidden Danger</strong> neben <strong>R.U.S.E.</strong> und einigen anderen als „Best Strategy Game“ in diesem Sommer auf der E3. Die Kollegen von 411mania gingen noch einen Schritt weiter und erklärten es zum besten Strategiespiel der E3. Entwickelt wurde die Mischung als first-person-shooter und real-time-strategy von Atomic Motion, vertrieben und vermarktet wird es von <a href="http://www.southpeakgames.eu/" target="_top">SouthPeak Games</a> und <a href="http://www.evolvedgames.com/" target="_top">Evolved Games</a>.</p> '
image: http://www.gamevista.de//wp-content/uploads/2009/09/smallrs2.jpg

---
Im ersten Moment klingt es nach einer sehr interessanten Mischung aus den beiden beliebtesten Genre, aber leider hält dieses Interesse beim Spielen nicht all zu lang an. Raven Squad hatte viele Schattenseiten trotz tropischem Setting.

Die Story ist an den Haaren herbeigezogen. Irgendwie musste man es schaffen, dass zwei Teams von jeweils drei Mann irgendwo sind und keine Verstärkung haben. Man fliegt also im Jahre 2011 mit einem eher altertümlichen Flugzeug in den Urwald, wird abgeschossen und landet irgendwo dort. Der Urwald wird natürlich von irgendwelchen aufständischen Rebellen kontrolliert. Die erste Mission ist es dann, die beiden getrennten Teams wieder zu vereinen, um dann gemeinsam dem Feind entgegen zu treten.

<a href="http://www.gamevista.de/wp-content/uploads/2009/09/rs1.jpg" rel="lightbox[rs2]"><img class="caption alignright size-full wp-image-611" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallrs1.jpg" border="0" alt="Jedes Squad hat seine eigenen Muntionskisten." title="Jedes Squad hat seine eigenen Muntionskisten." align="right" width="140" height="100" /></a> Jeder der Männer ist ein Soldat, hat ein Gewehr und eine spezielle Fähigkeit / zweite Waffe. Diese Waffen kann man im Laufe des Spiels nicht wechseln, auch gibt es keine Upgrademöglichkeit – man kann aber jederzeit zwischen den sechs Charakteren wechseln (ausgenommen ist hier die erste Mission).

Das erste Team („Assault Squad“) besteht aus Paladin, welcher ein Maschinengewehr trägt und zusätzlich dazu noch ein schweres MG hat um Sperrfeuer zu geben, Oso, welcher ein Maschinengewehr mit einem Laserzielvisier hat und Splittergranaten werfen kann und Thor, welcher mit einer Schrotflinte um sich ballert und einen Raketenwerfer mitbringt.



Das zweite Team („Infiltrator Squad“) besteht aus drei weiteren Charakteren, die jeweils ein Maschinengewehr bei sich haben und ein Scharfschützengewehr, Rauch- und Blendgranaten bei sich haben. Die Fähigkeiten sind also nicht wirklich speziell oder neuartig und es wirkt so, als ob man die Fähigkeiten von normalerweise einem Soldat auf gleich sechs verteilt hätte.

<a href="http://www.gamevista.de/wp-content/uploads/2009/09/rs4.jpg" rel="lightbox[rs3]"><img class="caption alignright size-full wp-image-613" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallrs4.jpg" border="0" alt="Bewusstlose Teamkameraden kann man einfach wiederbeleben." title="Bewusstlose Teamkameraden kann man einfach wiederbeleben." align="right" width="140" height="100" /></a> In der <a href="hot-spots/preview-r-u-s-e" target="_top">Preview von R.U.S.E.</a> habe ich die herausragenden Zoommöglichkeiten gelobt – so etwas wird dem Spieler in Raven Squad leider nicht geboten. Man kann zwischen der First-Person-Ansicht und der Vogelperspektive wechseln. Dies geschieht durch das Drücken der Leertaste, einen stufenlosen Zoom gibt es aber nicht. Grafisch wirkt das Spiel als ob es auf der E3 2005 vorgestellt wurde. Vor allem der Ego-Shooter-Teil kann hierbei überhaupt nicht punkten. So gibt es keine Einschusslöcher, die Charaktermodelle sind schlecht, die Animationen sind nicht flüssig, die Bäume sehen schrecklich aus, die Licht-Schatten-Effekt sind das einzige, was aus dem Jahre 2007 stammen könnte. In der Vogelperspektive wirkt die Grafik etwas besser, ist aber auch nicht zu vergleichen mit der Grafikpracht von R.U.S.E.

Humor und eine packende Story können jedes Spiel aufwerten. Leider bedient sich Raven Squad keiner der beiden Möglichkeiten. Die Dialoge wirken teils unfreiwillig komisch, was der Atmosphäre wirklich nicht weiter hilft. Dazu kommen die schlechten Sprecher sowohl für die Charaktere als auch für den Erzähler, der uns für jede Mission eine Art Einweisung gibt.



<a href="http://www.gamevista.de/wp-content/uploads/2009/09/rs3.jpg" rel="lightbox[rs4]"><img class="caption alignright size-full wp-image-615" src="http://www.gamevista.de/wp-content/uploads/2009/09/smallrs3.jpg" border="0" alt="Schade, dass man die Teams nicht selbst ausrüsten kann." title="Schade, dass man die Teams nicht selbst ausrüsten kann." align="right" width="140" height="100" /></a> 

Portierungen von der Xbox360 auf den PC sind immer schwierig und oftmals gelingt es nicht, die Steuerung wirklich gut umzusetzen. Dieses Problem hat der Entwickler einigermaßen gelöst, sodass die Steuerung annehmbar oder nicht besonders gut ist.

Wir haben also eine interessante Idee, mit einer öden Story, einer veraltenden Grafik und wollen doch im Grunde nur ein wenig spielen. Leider wird man dabei immer und immer wieder durch das automatische Speichern für ein paar Sekunden unterbrochen – dies ist auf Dauer nicht nur nervig, sondern in die Häufigkeit auch völlig sinnlos.

Als diese Mischung aus Shooter und Strategie angekündigt wurde, sah alles nach einer interessanten Idee aus und das ist es wirklich, doch die Umsetzung ist leider schlecht und so kann man nur hoffen, dass das Spiel in Frieden ruht.

 