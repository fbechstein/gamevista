---
title: ' Left 4 Dead 2 (PC)'
author: gamevista
type: post
date: 2009-11-29T17:38:29+00:00
excerpt: '<img class="caption alignright size-full wp-image-116" src="http://www.gamevista.de/wp-content/uploads/2009/07/left4dead2_small.jpg" border="0" alt="Left 4 Dead 2" title="Left 4 Dead 2" align="right" width="140" height="100" /></strong>Der erste Teil von <strong>Left 4 Dead</strong> war eine willkommene Abwechselung im Ego-Shooter Genre. Der Entwickler <a href="http://www.l4d.com" target="_blank">Valve</a> präsentierte im November letzten Jahres einen Team-Shooter der etwas anderen Art und erzeugte damit frischen Wind im doch etwas angestaubten und innovationslosen Shooter-Bereich. Nun nach so ziemlich genau einem Jahr hat <a href="http://www.l4d.com" target="_blank">Valve</a> den zweiten Teil von <strong>Left 4 Dead</strong> veröffentlicht. Viele Fans waren enttäuscht das es ein Vollpreistitel geworden ist, weil <strong>Left 4 Dead 2</strong> doch eher nach einem besseren Addon aussieht. Ob das stimmt und ob <strong>Left 4 Dead 2</strong> sein Geld wert ist prüfen wir heute und möchten euch darüber berichten wie sich der neuste <a href="http://www.l4d.com" target="_blank">Valve</a> Titel spielt.</p> '
featured_image: /wp-content/uploads/2009/07/left4dead2_small.jpg

---
**Die Überlebenden**

Die auffälligste Änderung sind wohl die neuen Helden von **Left 4 Dead 2**. Da die alten vier Helden aus dem ersten Teil anscheinend überlebt haben, dürfen sich nun der High School Lehrer Coach, der Mechaniker Ellis, der Glücksspieler Nick und die Nachrichtenproduzentin Rochelle den Weg durch den Süden der USA nach New Orleans bahnen. Hier soll einer der wenigen Orte auf der Landkarte sein, wo noch nicht jeder jeden annagen möchte.

Wie immer legt <a href="http://www.l4d.com" target="_blank">Valve</a> nicht allzu viel Wert auf die Story der einzelnen Protagonisten sondern mehr auf die Action des Überlebenskampfes. Es gibt aber wieder jede Menge Details zur Story an Wänden der Levels zu finden. Dies hat mir schon im ersten Teil sehr gefallen und führte nicht selten dazu, dass man den Anschluss an die Gruppe verloren hat um sich die Nachrichten anderer Überlebender durchzulesen. 

Erwartungsgemäß geht man davon aus mit **Left 4 Dead 2** einen Ego-Shooter zu erhalten der den Fokus auf das abballern von Zombiehorden und mit dem verbundenen Teamwork legt.   
Dies hat <a href="http://www.l4d.com" target="_blank">Valve</a> wieder gut hinbekommen und einige Verbesserungen hinzugefügt. So hat der KI-Regisseur ein Update erhalten und neben den altbekannten Bosszombies Boomer, Hunter, Smoker und Tank wurden drei neue Fieslinge ins Spiel integriert. Zum einen wäre da der Jockey, welcher auf den Rücken des Opfers springt und auf ihm durch die Gegend reitet. Dabei kann er den Überlebenden steuern da dieser Handlungsunfähig ist.

Der Charger ist die Miniversion des Tanks, dieser stürmt auf seine Gegner zu und rammt sie in die nächste Wand die hinter dem Opfer steht. Zu guter Letzt wäre da noch der Spitter. Die stärke des Zombies liegt hier im Fernkampf indem er die Überlebenden mit ätzendem Schleim bespuckt. Außerdem gibt es auch leicht modifizierte Versionen der normalen Zombies. Diese tragen kugelsichere Westen oder feuerfeste Anzüge und müssen so mit unkonventionellen Mitteln zu Boden gebracht werden.

<p style="text-align: center;">
  <img class=" size-full wp-image-1120" src="http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**Der KI-Regisseur**

<a href="http://www.l4d.com" target="_blank">Valves</a> wohlmöglich größtes Argument im neuen **Left 4 Dead** ist die verbesserte KI des Directors. Dieser soll sowohl das Wetter bzw. die Level selbst verändern, was jedoch nur auf wenigen Karten und auch nur minimal bemerkbar ist. Beispielsweise in der Kampagne Sturmflut wirft die KI den Überlebenden einen Sturm entgegen der die Sicht extrem einschränkt. Zusammen mit einer Zombiehorde ein lustiges Erlebnis. Die Storystränge sind diesmal zusammenhängend, im Gegensatz zum ersten Teil. Die Überlebenden wandern durch fünf Kampagnen, wobei der obligatorische Abstecher in den Vergnügungspark oder in die Sumpfländer viel Abwechselung bietet.

Auch wurden die getriggerten Events, wo größere Horde von Zombies erscheinen und für kurze Zeit das Leben der Helden zur Hölle machen, immer mehr mit Aufgaben um diesen Zustand zu beenden gespickt. Im ersten Teil war es meist nur nötig sich in eine Ecke zu verkriechen und die ca. 100 Zombies abzuknallen. Nun muss das Team in **Left 4 Dead 2** oft selbst aktiv werden und einen Alarm ausschalten oder einem verbarrikadierten Überlebenden eine Cola, zu finden im Supermarkt, überreichen damit dieser den Weg freigibt.

**Die neuen Waffen**

Natürlich möchten die Fans auch neue Schießprügel ausprobieren und so hat <a href="http://www.l4d.com" target="_blank">Valve</a> dem Spiel ein größeres Waffenarsenal spendiert. Zum einen gibt es neue Nahkampfwaffen die man gegen die Pistolen eintauschen kann. So kann man sich z.B. mit Bratpfanne, Gitarre oder Axt zu Wehr setzen. Dem Fernkampfgerät wurden diverse Variationen von MP, MG, Scharfschützengewehr oder Schrotflinte hinzugefügt. Auch für die Pistolen gibt es nun eine Magnum, die zwar ein kleineres Magazin hat aber dafür über mehr Wumms verfügt.

<p style="text-align: center;">
  <img class=" size-full wp-image-1121" src="http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r6.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r6.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r6-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Das Zusatzequipment wurde nicht vergessen. Die Überlebenden können nun neben den bekannten Molotowcocktails und Rohrbomben auch auf einen Defibrillator oder Explosions- bzw. Brandmunition setzen. Adrenalinspritzen für einen schnellen Sprint in den sicheren Saveroom sind auch neu dabei.

 ****

**Single- oder Multiplayer**

Ob im Single- oder Multiplayer Modus, die neuen Waffen und Boss Infizierten richten mächtigen Schaden an. So ist es eine echte Herausforderung **L4D2** im Singleplayer durchzuspielen. Denn die KI verhält sich hier manchmal nicht ganz so schlau wie gewollt und steht so einfach nur dumm rum während man selbst von einem Smoker erwürgt wird. Aber **L4D2** ist und bleibt ein Koop. &#8211; bzw. Multiplayer-Titel. Somit eignet sich der Singleplayer eher zum Kennenlernen der neuen Karten und Waffen. **L4D2** entfaltet sich erst mit drei Freunden am besten in einem abgedunkelten Raum mit ordentlicher Verpflegung für die nächsten Stunden. Denn hier kommt die Stimmung und die düstere Atmosphäre voll zur Geltung. Wem der Expert-Modus im ersten Teil bisher zu einfach war kann nun noch einen Gang höher Schalten und den Realismus-Modus ausprobieren. Hier wird z.B. die Umrandung die man bei Verbündeten sieht, die hinter Wänden stehen, ausgeblendet. Zusätzlich bekommen Zombies mehr Lebensenergie und halten so noch mehr Treffer aus. Vervollständigt wird der Koop-Spaß durch den bekannten Survival-Modus, in dem man Welle für Welle unendlicher Zombiehorden bestehen muss.



**Player vs. Player**

Für Fans von **Left 4 Dead 2** die lieber gegen menschliche Spieler antreten wollen, gibt es erneut den Versus-Modus. Hier versuchen zwei Teams im 4vs4 die normale Kampagne schnellstmöglich zu durchlaufen. Während das andere Team als spielbare Zombies dies zu verhindern versucht. Umso mehr bietet **Left 4 Dead 2** hier den meisten Spaß, denn durch die neuen Bosszombies mit weiteren Fähigkeiten sind ganz neue Taktiken gefragt. Neben den zahlreichen Änderungen wird auch das Punktesystem geändert. So war es im ersten Teil möglich, die Partie schon sehr früh für sich zu behaupten, weil man einen sehr großen Bonus bekommen hat wenn man den Schutzraum erreicht hatte. Dieser Bonus fällt nun wesentlich geringer aus. Mehr Punkte gibt es jetzt wenn man ein Viertel des Levels durchlaufen hat. Im neuen Spielmodus mit dem Namen _Scavange_, treten auch zwei Teams in einem recht kleinen Levelabschnitt gegeneinander an. Ziel ist es hier einen Generator mit möglichst viel Benzin zu füllen, während die Infizierten einen davon abhalten wollen. Sehr spaßig und äußerst hektisch das Ganze.

**Grafik und Spieltiefe**

Die Grafik von **Left 4 Dead 2** beruht weiterhin auf der schon etwas betagten Source Engine, diese wurde zwar stark modifiziert. Dennoch fragt man sich manchmal wie solch ein Spiel mit der CryEngine 3 aussehen würde. <a href="http://www.l4d.com" target="_blank">Valve</a> schafft trotzdem ein weiteres Mal den Zombieshooter gut aussehen zu lassen. Zwar sind die Levels insgesamt etwas zu hell gestaltet, aber Ton und der Spaß mit drei Freunden zusammen spielen zu können macht diesen Umstand wieder wett.   
Dementsprechend sind die Systemanforderungen relativ niedrig Eingestuft und so spielt sich **Left 4 Dead 2** auch mit etwas älteren Rechnern flüssig. Die Atmosphäre ist ähnlich tiefgehend und spannend wie in **L4D1**. Zwar trüben die Tagmissionen den Gruselfaktor ein wenig, dafür haben es die Missionen in dem der KI-Director einen Sturm entfacht in sich. Wir haben die die deutsche Cut Version getestet. Ärgerlich ist hier der Ansatz der Unterhaltungssoftware Selbskontrolle, kurz USK, mit der Schere. Wenn sich ein getroffener Zombie schon vor dem Ableben aus der Spielwelt auflöst, macht dies wirklich wenig Spaß und zerstört die Atmosphäre. Dem erwachsenen Spieler sei hier die Importversion ans Herz gelegt.

<p style="text-align: center;">
  <img class=" size-full wp-image-1122" src="http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r2.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r2.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/11/l4d2r2-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

**Fazit</p> 

</strong>

Wer den ersten Teil von **Left 4 Dead** noch nicht sein Eigen nennt und sich anhand der Werbung des zweiten Teils fragt ob man sich nun **L4D1** oder **L4D2** zuerst kaufen soll. Dem sei gesagt, dass sich bis auf ein paar fehlende Updates der erste Teil super als Anfang lohnt. So gibt es den ersten Teil bei Amazon in der **Game of the Year Edition** bereits für 19,99 Euro für PC oder 29,99 Euro für die Xbox 360. Da wären wir auch gleich beim Hauptproblem von **Left 4 Dead 2**. Man kommt von dem Gedanken nicht los ein Addon zu spielen. Eines der besten Addons welche man je gespielt hat. Leider will <a href="http://www.l4d.com" target="_blank">Valve</a> aber den Wert eines Vollpreistitels vom Kunden haben. Fraglich ist wie <a href="http://www.l4d.com" target="_blank">Valve</a> mit einem Nachfolger umgehen wird. Denn wenn der dritte Teil wieder nur ein Aufguss vom zweiten werden sollte, wird die doch sehr angesehene Spieleschmiede langsam in den Geldmaschinenabgrund stürzen. So sei <a href="http://www.l4d.com" target="_blank">Valve </a>diesmal verziehen und das Produkt **Left 4 Dead 2** mit **85%** bewertet. Sicher könnte man ein paar Prozente wegen des Preises abziehen, aber hier sollte jeder Kunde selber wissen wie er damit umgehen möchte.

**<span style="text-decoration: underline;">Wertung: 85%</span>**





