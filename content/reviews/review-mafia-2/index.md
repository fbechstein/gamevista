---
title: ' Mafia 2'
author: gamevista
type: post
date: 2010-08-29T19:26:49+00:00
excerpt: '<img class="caption alignright size-full wp-image-429" src="http://www.gamevista.de/wp-content/uploads/2009/08/mafia2_small.png" border="0" alt="Mafia 2" title="Mafia 2" align="right" width="140" height="100" /> Der zweite Teil von <strong>Mafia </strong>verspricht mit neuer schicker Grafikengine und zahlreichen cineastischen Zwischensequenzen die Story von Vito Scaletta um ein vielfaches spannender zu gestaltet als im ersten Teil. Ob das in den 40er Jahre spielende Actionepos für einen Kauf lohnt, erfahrt ihr in unserer Review.</p> '
featured_image: /wp-content/uploads/2009/08/mafia2_small.png

---
Nach fast acht Jahren Entwicklungszeit präsentiert **2K Games** den zweiten Teil der Mafiasaga. Als Protagonist Vito Scaletta führt euch euer Weg vom 2. Weltkrieg in die Straßen der fiktiven US-Amerikanischen Stadt Empire City. Wie schon im ersten Teil muten die Level fast an ein Open-World-Spiel wie z.B. **GTA 4** an. Zwar kann man sich nicht ganz frei bewegen, die Karten nehmen dennoch riesige Ausmaße an.

Unser Held kehrt aufgrund einer Verletzung, die ihm im Rahmen des 2. Weltkriegs widerfahren ist, nach Amerika zurück. Dort angekommen stellt er schnell fest, dass seine Familie Probleme mit Schulden und der damit verbundenen Tilgung hat. Er sucht seinen Freund Joe auf und bekommt schnell erste Kontakte zum Untergrund und Mafia von Empire City.

Natürlich benötigt so ein richtiger Mafioso durschlagende Argumente. Diese durfte unser Protagonist im 2. Weltkrieg erlernen und einsetzen. Schon in der ersten Mission darf der Spieler, auf der italienischen Insel Sizilien, Truppen des Diktators Mussolini unter Beschuss nehmen und aus einer Kleinstadt jagen.

<p style="text-align: center;">
  <img class=" size-full wp-image-2170" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia2.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/mafia2.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/mafia2-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



<img class=" alignright size-full wp-image-2171" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia2new2.jpg" border="0" align="right" width="140" height="100" />In den ersten Missionen wird der Spieler im Stile eines Tutorials an die Steuerung des Spiels herangeführt. Sehr gefällt hier das Deckungssystem, was ähnlich wie bei **Splinter Cell: Conviction** oder **Rainbow Six: Vegas** funktioniert. Wir verstecken uns also hinter Mauern, Tischen und Schränken um Truppenteile unter Beschuss zu nehmen. Nach einer kurzen Schießerei wird auch schon die Überleitung zur Hauptstory in den USA gestartet.

Ein Gangsterboss mit dem Namen Don Calo kommt auf einem Sherman-Panzer geritten und fordert Mussolinis Truppen auf sich zu ergeben. Beeindruckt von dieser Machtdemonstration eines einzelnen Mannes kehrt Vito zurück nach Amerika.   
Nachdem er mit seinem Kumpel Joe schnell die benötigten Gelder für seine Mutter besorgt hat, macht Vito nun nicht halt und schlägt sich seinen Weg zu einem waschechten Mafioso.

<a href="http://www.mafia2game.com" target="_blank" rel="nofollow">Mafia 2</a> spielt sich ähnlich wie sein Genrekollege Grant Theft Auto 4. Die Spielwelt, die langen und perfekten Zwischensequenzen und Missionen kopieren dabei aber nicht ab. Sie haben ihren ganz eigenen Charme. Man klaut Autos, kann Klamotten kaufen, schlafen oder knackt Türschlösser. Im Zuge der Missionen darf da ein umfangreiches Waffenarsenal natürlich nicht fehlen. So bietet <a href="http://www.mafia2game.com" target="_blank" rel="nofollow">Mafia 2</a> viele Facetten von **GTA 4** oder **The Saboteur**, mehr Details als in **The Saboteur**, allerdings eine kleinere Spielwelt als beim vierten Teil von Grand Theft Auto.

<p style="text-align: center;">
  <img class=" size-full wp-image-2172" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia24.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/mafia24.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/mafia24-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

Wie bei den beiden Genrekollegen passen die Ordnungshüter darauf auf, dass ihr nicht ohne Verfolgungsjagd mit gestohlenen Wagen davon kommt. Größere Probleme machen die Cops aber nicht, ab in die nächste Werkstatt und schon hat sich der Strafzettel erledigt. Hier kann man auch gleich seinen Wagen reparieren, umlackieren oder sogar tunen lassen.

Überhaupt stellt die Ordnungsmacht den Spieler vor weit weniger Probleme als in **GTA 4**. Die Polizei muss schon in der Nähe sein um eure Missetaten zu bemerken. Wer also auf der Flucht ist wird nur von Polizisten verfolgt die euch sehen können.

Missionen erhaltet ihr auf verschiedenste Arten, ob per Telefon oder direktem Besuch in den eigenen vier Wänden. Vito wird durch zahlreiche Schießereien und Verfolgungsjagden geschickt. Die mit mehr als zwei Stunden Spielzeit bemessenen Zwischensequenzen tun da ihr übriges. Ganz im Stil von Mafia-Filmklassikern wie Good Fellas oder Der Pate wird hier die Story voran getrieben.    
Nach den etwas langen Sequenzen geht es dann meistens auch sofort los mit der eigentlichen Mission.

<p style="text-align: center;">
  <img class=" size-full wp-image-2174" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia23.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/mafia23.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/mafia23-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



<img class=" alignright size-full wp-image-2175" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia2new4.jpg" border="0" align="right" width="140" height="100" />Die Grafik von <a href="http://www.mafia2game.com" target="_blank">Mafia 2</a> setzt zwar keine neuen Akzente, aber in den überragenden Zwischensequenzen wird klar, dass die Engine nicht allein alles ist. Mit flüssigen Animationen und guter „Kameraführung“ wird die Geschichte um Vito Scaletta immer weiter gesponnen.    
Die Welt um Vito lebt, dass zeigt gleich die erste Mission in Italien sowie der Sprung nach Amerika, als Vito im Winter zu seiner Mutter heimkehrt. Die meisten Straßenszenen sind zwar geskriptet, trotzdem erwärmt es einem das Herz, wenn man durch Little Italy läuft und den Passanten bei ihren täglichen Treiben beobachtet.

Die Details der Fahrzeuge sind ähnlich denen in **GTA 4** sehr umfangreich. Baut man einen Unfall fliegen euch mal die Motorhaube oder die Tür weg. Der Sound von <a href="http://www.mafia2game.com" target="_blank" rel="nofollow">Mafia 2</a> bietet mit 40er- bzw.  50er Jahre-Musik eine sehr passende Klangkulisse. Die Titel fügen sich wunderbar in die Szenerie der Metropole Empire City ein und dürfen durch mehrere Radiosender gewechselt werden.

Die Steuerung von <a href="http://www.mafia2game.com" target="_blank" rel="nofollow">Mafia 2</a> fällt von Anfang an sehr leicht, auch das Tutorial am Anfang der Geschichte dürfte Genre-Neulingen leicht den Weg weisen. Wer schon **Grand Theft Auto 4** gespielt hat sollte sich sofort zurecht finden. Die Fahrzeuge steuern sich exakt wie bei den Genre-Kollegen und wer sich in den Nahkampf traut darf sogar auf ein paar Kombos hoffen.

<p style="text-align: center;">
  <img class=" size-full wp-image-2176" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia22.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/mafia22.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/mafia22-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**Fazit**

2K Games macht mit dem zweiten Teil von Mafia mal wieder alles richtig. Zwar bietet er keine wirklichen Innovationen, doch das alt bewährte Spielprinzip haben die Entwickler gut umgesetzt und weiterentwickelt. Wer sich schon immer in **GTA 4** wohlgefühlt hat und mal eine ganz andere Szenerie begutachten wollte, die mit einer wirklich guten Story gespickt ist, dem sei <a href="http://www.mafia2game.com" target="_blank" rel="nofollow">Mafia 2</a> sehr zu empfehlen. Mit schicker Grafik, viel Action in Gangster-Manier und tiefgründigen Charakteren versucht der zweite Teil zu überzeugen, reicht dabei aber nicht annähernd Genre-Primus GTA 4 heran. Hingegen kann <a href="http://www.mafia2game.com" target="_blank" rel="nofollow">Mafia 2</a> mit seiner überzeugenden Story punkten.

**Wertung: 85%**

<p style="text-align: center;">
  <img class=" size-full wp-image-2177" src="http://www.gamevista.de/wp-content/uploads/2010/08/mafia21.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2010/08/mafia21.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/08/mafia21-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

   

* * *

   

