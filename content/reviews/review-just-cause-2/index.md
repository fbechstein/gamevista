---
title: ' Just Cause 2'
author: gamevista
type: post
date: 2010-03-31T15:16:51+00:00
excerpt: '<img class="caption alignright size-full wp-image-1643" src="http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_small.png" border="0" alt="Just Cause 2" title="Just Cause 2" align="right" width="140" height="100" />Mit einem Open-World-Szenario basteln die Entwickler von <strong>Avalanche Software</strong> eine Welt die seinesgleichen sucht. Klar es gibt GTA, aber wem das Stadt-Setting nicht so liegt oder mehr Abwechselung sucht, der ist mit <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> gut beraten. Wie gut der zweite Teil ist und ob Rico Rodriguez wieder die Fetzen fliegen lässt, erfahrt ihr in unserer Review.</p> '
featured_image: /wp-content/uploads/2010/03/justcause2_small.png

---
Mit einem riesigen Repertoire an Fahrzeugen und einer über 1000 Quadratkilometer großen Insel mit pulsierenden Städten, traumhaften Stränden und gewaltigen Gebirgsmassiven, bietet <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> jede Menge Neuerungen hinsichtlich zum ersten Teil. Mit einem Enterhaken bewegt man sich wie Spiderman von Gebäude zu Gebäude, springt auf einem vorbeifliegenden Helikopter auf um ihn zu kapern oder wirft damit Gegner in den Abgrund. 

Der Held Rico hat seinen Job als Spion auf die Insel Panau verlegt. Dort herrscht eine Putschregierung mit der sich sein Arbeitgeber nicht gerade anfreunden kann. So stürzt sich Rico bereits im Intro aus einem Hubschrauber um kurz darauf mit dem Fallschirm mitten im Actionspektal zu landen. Den kann er übrigens jederzeit nutzen genauso wie den Enterhaken.   
Kaum gelandet, finden wir uns in einer schneebedeckten Gebirgslandschaft wieder. Kurz darauf gibt uns unsere Kollegin über Funk die erste Mission durch.

So müssen wir Speicherkarten sammeln, die gespickt sind mit allerlei sensibler Daten. Dabei laufen uns die ersten Gegner über den Weg die wir kurzerhand mit Pistole und später auch mit schweren Waffen ausschalten. Neben dem Fallschirm und dem üblichen Waffen, nutzen wir sehr oft den Enterhaken um schneller vorwärts zu kommen oder Gegner von Gebäude zu ziehen. Wie Spiderman kann man dieses Seil mit Haken an jedes beliebige Objekt heften und entwickelt dabei äußerst kreative Lösungen um die Missionen zu beenden.

<p style="text-align: center;">
  <img class="caption size-full wp-image-1644" src="http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_1.jpg" border="0" alt="Wir  bewegen uns nicht nur auf dem Land" title="Wir bewegen uns nicht nur auf dem Land" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_1.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_1-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Das Open-World Spiel <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> bietet eine riesige Spielwelt mit unterschiedlichsten Gebieten. Ob Strände, Wüsten, verschneite Berge, Dschungel bis hin zu Dörfern oder ganzen Städten. <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> bietet dabei mindestens genauso viel Abwechselung wie manch Onlinerollenspiel oder dem Genre-Kollegen Grand Theft Auto. Damit Rico die Reise nicht zu Fuß antreten muss, hat der Entwickler **Avalance Software** unserem Protagonisten einen umfangreichen Fuhrpark zur Verfügung gestellt. Dabei gibt es diverse Vehikel auf Rädern, mit Flügeln oder auf dem Wasser zu steuern.

Im GTA-Stil darf man sich hier frei bedienen und jegliches Gefährt klauen. Dennoch erscheint einem die Spielwelt von <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> relativ leer und lustlos. Die Genre-Kollegen **Grand Theft Auto 4** und **Saboteur** schicken zahlreiche Passanten auf die Straßen der Städte die ihrem künstlichen Leben nachgehen, selbst ein paar mehr Tiere hätten Panau nicht geschadet.   
Das Ambiente der Insel reißt hier aber einiges raus und man wundert sich das solch eine große, fast grenzenlose Welt so flüssig läuft und dabei so gut aussieht.  
Um in der Story voranzukommen muss man neben dem erfüllen des aktuellen Missionsziels auch Punkte sammeln.

Diese erhält man in dem man in Panau ordentlich die Fetzen fliegen lässt. So sprengt man sich fröhlich durch Fahrzeuge und Benzinfässer oder zündet ganze Ölsilos an um das nächste Kapitel der Geschichte freizuschalten. Denn neben Geld für Waffen und Fahrzeuge, benötigt man diese Punkte um die Geschichte voran zu treiben. Das Chaos was ihr anrichtet ist quasi eine Art Währung durch die ihr größeren Einfluss auf Insel erzielt.

So kommt mit Ricos neuem Abenteuer keine Langeweile auf. Mal legen wir mal eine Militärbasis in Schutt und Asche oder fliegen hoch über den Wolken am Steuerhebel eines Jets. Am meisten Spaß macht uns dabei die Stunteinlagen mit Gleitschirm und Enterhaken. Den klinken wir kurzerhand am Heck eines Wagens ein und lassen uns so zum Missionsort ziehen. Dies dürfte für einige interessante Youtube-Stunt Videos sorgen. Somit spielen wir zahlreiche Stunden abseits der Hauptstory. Bei Grand Theft Auto rufen solche Ballerorgien die Polizei auf den Plan. Auf Panau ist es die Regierung die mit Militäreinsätzen antwortet.

<p style="text-align: center;">
  <img class="caption size-full wp-image-1645" src="http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_3.jpg" border="0" alt="So hijacked man Fahrzeuge heute" title="So hijacked man Fahrzeuge heute" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_3.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/03/justcause2_3-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Die Welt von <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> könnte größer kaum sein. Es reihen sich Dschungel und Berge an Meere und Klippen. Städte, Dörfer und Flughäfen sind genauso vorhanden wie Ölbohrinseln und Wüsten. Wie schon erwähnt könnte die Performance nicht besser sein. Klar könnten hier und da mal ein paar mehr Details nicht schaden. Dennoch ist man einfach überwältigt von der gigantischen freibegehbaren Welt. Die Avalanche Engine verzaubert dabei gerade mit einer grandiosen Weitsicht über mehrere Kilometer. Da darf ein Echtzeit-Wettersystem und schön anzusehende Explosionen nicht fehlen. 

**Fazit**

Alles in allem ist <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> eine wirklich gute Fortsetzung des ersten Teils. Rico Rodriguez zerlegt mit seinem Gleitschirm und Enterhaken manchmal elegant, manchmal trickreich oder einfach rabiat seine Gegner und Umwelt. Dass dabei höllischer Spaß aufkommt ist wohl nicht weiter erwähnenswert.  Die Grafik ist ein Traum und dürfte auch auf schwächeren Rechnern ein gutes Bild abgeben. Wem Grand Theft Auto zu klein ist oder nicht genug Fahrzeuge bietet, dem sei <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> ans Herz gelegt. Allerdings dürfte <a href="http://www.justcause.com" target="_blank" rel="nofollow">Just Cause 2</a> mit seiner eigenen „kleinen“ Open-World für manche Vollblutzocker zu Arcade-lastig sein. Denn das Spiel ist voll auf Action getrimmt und pfeift dabei auf Physik und z.B. auf das genaue Zielen von Gegner. Ich hätte mir einen guten Mehrspielermodus gewünscht, ansonsten war ich sehr überrascht vom neuen Avalanche Software-Projekt und kann es definitiv weiterempfehlen.

**<span style="text-decoration: underline;">Wertung: 83%</span>**