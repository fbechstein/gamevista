---
title: ' The Saboteur'
author: gamevista
type: post
date: 2009-12-06T18:15:41+00:00
excerpt: '<img class="caption alignright size-full wp-image-1133" src="http://www.gamevista.de/wp-content/uploads/2009/12/the-saboteur.jpg" border="0" alt="The Saboteur" title="The Saboteur" align="right" width="140" height="100" />Es gab schon viele Versuche das Grand Theft Auto-Konzept in andere Spielwelten zu kopieren, mehr schlecht als recht. <strong>The Saboteur</strong> vom Entwicklerstudio <a href="http://www.pandemicstudios.com/thesaboteur/" target="_blank">Pandemic</a> ist aber alles andere als das. So wurde eine spannende Story rund um den Protagonisten Sean Devlin gewebt und versetzt den Spieler in das von Nazis besetzte Paris des 2. Weltkriegs. Wie sich das Ganze spielt und ob <strong>The Saboteur</strong> es den Vorlagen recht macht, erfahrt ihr in unserer Review.</p> '
featured_image: /wp-content/uploads/2009/12/the-saboteur.jpg

---
In einem Pariser Varieté voller halbnackter Tänzerinnen bezieht unser Held seinen Unterschlupf, bereit Anschläge auf die deutschen Besatzer zu verüben. Der Grund hierfür wird in einen Rückblick der Story, kurz vor dem Kriegsbeginn, erzählt. Denn Sean Devlin ist ein irischer Rennfahrer der zusammen mit seinem besten Kumpel an einem Rennen im deutschen Saarbrücken teilnimmt. Natürlich ist auch ein deutscher SS-Offizier mit von der Partie. 

Dieser gewinnt das Rennen nur knapp durch einen gewieften Betrug. Darauf folgend beschließen die beiden Freunde ihren wohl ersten Anschlag auf den unbeliebten deutschen Rennfahrer. Leider geht bei der Durchführung etwas schief und Seans französischer Freund Luc bleibt auf der Strecke. Grund genug für unseren Protagonisten mächtig sauer auf die deutschen Besatzer zu sein.

So beginnt gleich unser erster Auftrag damit ein Benzindepot der Nazis in Paris zu sprengen. Also rennen wir zum nächsten Wagen und springen in einen der schönen Oldtimer aus dem Jahre 1940. Auf dem Weg zum Ziel wird uns der Auftrag noch einmal unmissverständlich erklärt. Bisher finden wir uns sehr gut zurecht, in guter alter GTA-Manier haben wir eine Minimap auf der die Fahrt zum Ziel eingezeichnet ist. Fußgänger springen wild gestikulierend über den Fußweg als wir dicht an ihnen vorbei Fahren.

Am Ziel angekommen sollen wir das Dach eines Hauses erklimmen um dann elegant an einem Stromkabel ins Questgebiet zu gleiten. In einer frei begehbaren Umgebung erfüllen wir also allerlei Aufträge für die Resistance indem wir Autos klauen, NPCs durch die Gegend kutschieren oder Dinge hochjagen. Der Kletterpart ähnelt dabei Assassin\`s Creed, aber wirklich nur Ansatzweise, denn uns fehlt die Leichtigkeit des Ubisoft Spiels.

<p style="text-align: center;">
  <img class=" size-full wp-image-1148" src="http://www.gamevista.de/wp-content/uploads/2009/12/saboteur1.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/saboteur1.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/saboteur1-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



Da liegt auch das Problem von **The Saboteur**. Irgendwie wirkt die Story hier und da sehr platt und unausgereift. Auch sind auch die Spielinhalte, das Klettern und die Stadtrundfahrten, eher nur im Ansatz so durchdacht wie in GTA bzw. Assassin\`s Creed.  Aber **The Saboteur** hat auch seine Glanzpunkte, denn gerade manche Missionen sind durch wunderschöne Zwischensequenzen spannend erzählt. Dem Hauptcharakter kann man zwar etwas abgewinnen, wiederum ist die Idee ein GTA-Klon der im 2. Weltkrieg spielt zu Entwickeln, relativ unspektakulär.

Natürlich dürfen verschieden Alarmstufen, die z.B. durch das umkarren von Nazis ausgelöst werden, nicht fehlen. Allerdings versucht die KI recht simpel den Helden von der Bildfläche durch Waffengewalt verschwinden zu lassen. Klettert man mal auf ein Dach sind der KI da schon Grenzen gesetzt. So kann man in aller Ruhe Granaten oder ein Scharfschützengewehr einsetzen und hat von den deutschen Besatzern wenig zu befürchten. Spannende Verfolgungsjagden alla Assassin\`s Creed bleiben so auf der Strecke.

Wenn dann aber alles glatt läuft und der Einsatz so funktioniert wie geplant, kommt in **The Saboteur** echtes Resistance Feeling rüber. Auch ist die Idee der Spielewelt am Anfang einen grauen Schleier zu verpassen recht gut gelungen. Dieser Lichtet sich nachdem wir immer mehr Aufträge für den Untergrund erfüllt haben. So wird Paris nach ein paar Missionen farbprächtig in Szene gesetzt.

Die Grafik-Engine ist zwar nicht mehr auf dem neusten Stand. Sie ist aber, wenn man sie nicht mit einem Toptitel vergleicht, ausreichend. Leider sind Texturen der Gebäude und Landschaft mit zu wenig Details versehen. Ein Trost sind da die zahlreichen Dialoge der „Schauspieler“, denn diese sind oft sehr schön anzuschauen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1149" src="http://www.gamevista.de/wp-content/uploads/2009/12/saboteur2.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2009/12/saboteur2.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/12/saboteur2-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>



**Fazit**

Alles in allem ist **The Saboteur** ein gutes Spiel mit leichten Schwächen. Wo Grand Theft Auto 4 mit Straßenverfolgungszenen oder Assassins\`s Creed mit dynamischer Gebäudespringerei glänzen, hat das neuste Spiel von den Entwicklern <a href="http://www.pandemicstudios.com/thesaboteur/" target="_blank">Pandemic Studios</a> nur mehr schlecht als recht kopiert. Sicher haben die genannten Titel das Rad nicht neu erfunden, aber sie haben es sehr wohl perfektioniert. Gefallen hat mir der Farbwechsel der Spielwelt, von Grau in Grau zu hoffnungsvollen Farbgebungen. Auch sind die Missionen oft sehr spannenden Inszeniert und mit schönen Zwischensequenzen erzählt. Wer also ein GTA-Klon gepaart mit Elementen aus Assassins\`s Creed in einem 2. Weltkriegs-Setting interessant findet, dem sei **The Saboteur** durchaus empfohlen.

**<span style="text-decoration: underline;">Wertung 82%</p> 