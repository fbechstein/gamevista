---
title: 'Test: Logitech G9x Laser Mouse'
author: gamevista
type: post
date: 2009-12-25T13:55:31+00:00
excerpt: '<img class="caption alignright size-full wp-image-1201" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallg9x.jpg" border="0" alt="Die G9x Laser Mouse von Logitech" title="Die G9x Laser Mouse von Logitech" align="right" width="140" height="100" /></a>Die G9x Laser Mouse von Logitech[/caption]Wir haben für euch eine Gaming-Mouse von <a href="http://www.logitech.com/index.cfm/home/&cl=de,de" target="_blank">Logitech</a> getestet. Ob es sich lohnt 99 € in eine Maus zu investieren, haben für euch herausgefunden. Dabei haben wir die MX518, auch eine Logitech-Maus, auch aus der Gaming-Serie, für rund 50€ zum Vergleich herangezogen.</p> '
featured_image: /wp-content/uploads/2009/12/smallg9x.jpg

---
Bis jetzt habe ich immer mit einer Logitech MX518 Spiele getestet und ich wollte niemals eine andere Maus. Dann habe ich eine Logitech G9x zum Testen erhalten und muss ehrlich sagen: Ich würde sie gerne behalten!

Die MX518 gehört zur Gaming-Serie von <a href="http://www.logitech.com/index.cfm/home/&#038;cl=de,de" target="_blank">Logitech</a>, ebenso wie die G9x, aber diese ist einfach noch mal ein Level höher. Die Liste mit Features ist lange und natürlich gehen wir in diesem Artikel auf jedes Einzelne näher ein, aber eins muss man vorweg sagen: Ich hatte noch nie eine Maus, die so gut in der Hand lag. Anfangs liegt die Hand etwas ungewohnt, doch nach etwa einer halben Stunde, merkt man nur noch, dass es einfach super funktioniert und passt.

<a href="http://www.gamevista.de/wp-content/uploads/2009/12/mx518.png" rel="lightbox[logitech2]"><img class="caption alignright size-full wp-image-1204" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallmx518.jpg" border="0" alt="Die MX518 – meine Maus" title="Die MX518 – meine Maus" align="right" width="140" height="100" /></a>Die Maus ist durchaus sehr präzise, doch nicht fühlbar präziser als die MX518. Beide Mäuse haben eine einstellbare Abtastgeschwindigkeit. Dabei geht der Laser der MX518 bis 1800 dpi, während der der G9x bis satte 5700 dpi geht. Das erlaubt natürlich bedeutend mehr Präzision und Geschwindigkeit, doch kann man das kaum nutzen. Beim Arbeiten im Office-Bereich stellte ich keinen Unterschied fest, denke aber es gab einen Unterschied beim Spielen. Das kann ehrlich gesagt auch nur Einbildung gewesen sein.



<a href="http://www.gamevista.de/wp-content/uploads/2009/12/g9x_griffschale.jpg" rel="lightbox[logtiech3]"><img class="caption alignright size-full wp-image-1206" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallg9x_griffschale.jpg" border="0" alt="Ein Beispiel für die Vielzahl an Griffschalen" title="Ein Beispiel für die Vielzahl an Griffschalen" align="right" width="140" height="100" /></a> Die Griffschale der G9x ist natürlich auswechselbar. Mitgeliefert werden zwei Schalen und beide liegen sehr gut in der Hand. Eine ist grau und glatt, die andere schwarz und rauer. Man kann sich aber auch noch weitere Griffschalen bestellen und dabei sogar selbst kreativ tätig werden (sogenannte ID-Griffschale). Die DryGrip-Technologie soll die Griffschalen besonders griffig machen. Das ist für mich eher eine Bezeichnung der Marketingabteilung für eine gelungene Griffschale als ernsthafte Technologie. Wie auch immer: Die G9x liegt gut in der Hand.

Bei beiden Mäuse befindet sich oberhalb des Daumens eine Vor- und eine Rückwärtstaste. Das ist sehr praktisch und erleichtert das Surfen deutlich. Die MX518 verfügt darüber hinaus nur noch über die linke und rechte Maustaste, sowie ein Mausrad. Während die G9x hingegen noch ein LED-Licht hat, was in 3 Farben leuchten kann. Dementsprechend gibt es auch drei einzeln einstellbare Profile, die man durchwechseln kann. Interessant ist dieses Feature durchaus, aber man braucht es eigentlich nicht.

<a href="http://www.gamevista.de/wp-content/uploads/2009/12/g9x_gewichte.jpg" rel="lightbox[logtiech4]"><img class="caption alignright size-full wp-image-1208" src="http://www.gamevista.de/wp-content/uploads/2009/12/smallg9x_gewichte.jpg" border="0" alt="Die Metallbox und die Schublade für die Gewichte" title="Die Metallbox und die Schublade für die Gewichte" align="right" width="140" height="100" /></a> Die gesamte Maus ist robust und dennoch einfach auseinander zu bauen – das muss man bei einer normalen Maus nicht erwähnen, weil man es nicht braucht – bei der G9x ist schon. Wie schwer ist eigentlich eure Maus? Bei der G9x ist das von Spieler zu Spieler unterschiedlich. Die Maus wird mit einer kleinen Metallbox ausgeliefert, in der sich vier 7 Gramm und vier 4 Gramm Gewichte befinden. Davon kann man maximal vier in der Maus verstauen. So kann jeder für sich selbst heraus finden, was das angenehmste Gewicht ist.



Die Mäuse sind beide sehr gut und sehen auch beide gut aus, wobei da jeder für sich selbst entscheiden muss, welche nun besser aussieht. Ich persönlich finde die G9x hübscher, weil sie etwas kleiner und schlichter ist. Das Mausrad der G9x ist bereits das neue Logitech-Mausrad, während die MX518 noch das alte Mausrad hat. Ich persönlich finde das etwas angenehmer. Es ist leiser, präziser und leichter zu rollen.

**Fazit:** Die G9x ist eine Top-Maus und legt die Messlatte ein gutes Stück höher. Wer gerne spielt, sollte in eine gute Maus investieren. Wer dabei etwas mehr Geld investieren möchte, sollte sich die G9x kaufen – es macht Spaß mit ihr zu spielen.
