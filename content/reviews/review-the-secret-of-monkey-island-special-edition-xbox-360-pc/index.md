---
title: ' The Secret of Monkey Island Special Edition (Xbox 360, PC)'
author: gamevista
type: post
date: 2009-07-16T20:09:40+00:00
excerpt: '<img class="caption alignright size-full wp-image-57" src="http://www.gamevista.de/wp-content/uploads/2009/07/monkeyse_small.png" border="0" alt="The Secret of Monkey Island SE - Guybrush kehrt zurück" title="The Secret of Monkey Island SE - Guybrush kehrt zurück" align="right" width="140" height="100" />Nach knapp 20 Jahren erlebt der Piratenanwärter Guybrush Threepwood seine Wiedergeburt in Form von <strong>The Secret of Monkey Island Special Edition</strong>. Ob das Remake gelungen ist - oh ja, und wie! </p>'
featured_image: http://www.gamevista.de/wp-content/wp-content/uploads/2009/07/monkeyse_small.png

---
Wer hätte damals gedacht, dass <a href="http://www.lucasarts.com/" target="_blank">LucasArts</a> mit seinen Adventures Millionen von Fans gewinnen wird. Ob Zak McKracken, Maniac Mansion, Indiana Jones oder eben auch Monkey Island. Alle Adventures landeten sofort in den Top Ten und wurden zum Kult. Jeder kennt sie, jeder hat sie gespielt.   
Nach knapp 20 Jahren und eine Woche nach dem Release der fünfteiligen Fortsetzung von Monkey Island **Tales of Monkey Island** bringt LucasArts eine aufpolierte Perle unter das Volk. Nein, es ist nicht die Black Pearl eines ebenso wie Guybrush Threepwood tollpatschigen Piraten Jack Sparrow, sondern das Adventure The Secret of Monkey Island Special Edition.

**Was ist neu?** 

 <img class="caption alignright size-full wp-image-58" src="http://www.gamevista.de/wp-content/uploads/2009/07/scummbar_small.png" border="0" alt="The Secret of Monkey Island SE - Wiedersehen in der Scumm Bar" title="The Secret of Monkey Island SE - Wiedersehen in der Scumm Bar" align="right" width="140" height="100" />Beim Remake haben sich die Entwickler 1:1 an der Vorlage des Originals von 1990 gehalten. D.h. inhaltlich ist alles gleich geblieben, nur äusserlich hat sich einiges getan. Die Grafiken wurden komplett runderneuert und auch das Interface ist einer Schönheits OP unterzogen worden. Fans werden jetzt aufschreien &#8211; das ist doch nicht mehr Monkey Island, das ist doch nicht mehr unser Pirat! Und auch daran haben die Entwickler gedacht: für Nostalgiker gibt´s das nette Feature, ich nenne es so: Remake- to-Classic-Feature. Per Knopfdruck kann man jederzeit in das Originalinterface von 1990 wechseln. Dann sieht man die Originalgrafik und Interface &#8211; einen besseren Vergleich zwischen heute und damals kann man nicht bekommen. 



  
<figure id="attachment_59" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-59" src="http://www.gamevista.de/wp-content/uploads/2009/07/originalgame_small.png" border="0" alt="Die Grafiken wurden runderneuert - hier das Original von 1990..." title="Die Grafiken wurden runderneuert - hier das Original von 1990..." width="140" height="100" align="right" /><figcaption class="wp-caption-text">Die Grafiken wurden runderneuert &#8211; hier das Original von 1990&#8230;</figcaption></figure>Die Steuerung und das neue Interface wurden sehr gut gelöst. Nach einigen Eingewöhnungsminuten kann es sofort mit dem Abenteuer losgehen. Guybrush Threepwood will Pirat werden, bei seinen Prüfungen lernt er die wunderschöne Elaine kennen. Der Bösewicht in der Geschichte ist der untote Pirat LeChuck, der ebenso die wunderschöne Elaine für sich haben will wie Guybrush. Wer die Geschichte nicht kennt, der sollte sich sofort das Spiel kaufen, wer die Geschichte kennt, ebenso.  
Für das Remake wurden alle Musikstücke neu aufgenommen und auch die Sprachaufnahmen sind sehr gut geworden. Leider sind diese nur in englischer Sprache. Wer des Englischen nicht so mächtig ist, der kann die deutschen Untertitel dazuschalten und so das Abenteuer auf sich nehmen. Die Classic Version im Spiel ist komplett in englisch und kann nicht ins deutsche umgeschaltet werden.   
<figure id="attachment_60" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-60" src="http://www.gamevista.de/wp-content/uploads/2009/07/remakegame_small.png" border="0" alt="...und das Remake von 2009" title="...und das Remake von 2009" width="140" height="100" align="right" /><figcaption class="wp-caption-text">&#8230;und das Remake von 2009</figcaption></figure>Während des Spiels gibt es eine Auto-Save-Funktion in Form eines 5 1/4 Disk-Logos &#8211; Herzen der Nostalgiefans werden hier sofort höher schlagen, erinnert doch die gewisse Form an die guten alten C64 Zeiten.  
Für Spieler, die ein Problem bei der Lösung eines Rätsels haben, können auf eine 3-Stufige interne Hilfe zurückgreifen. Beim ersten Ersuchen der Hilfe erhält man nur einen Tipp, beim zweiten wird der Tipp deutlicher und beim dritten Mal offenbart sich die komplette Lösung des Rätsels.

 

  
**Fazit** 

<figure id="attachment_61" style="width: 140px" class="wp-caption alignright"><img class="caption alignright size-full wp-image-61" src="http://www.gamevista.de/wp-content/uploads/2009/07/piraten_small.png" border="0" alt="The Secret of Monkey Island SE - bei den Piraten fängt das Abenteuer an" title="The Secret of Monkey Island SE - bei den Piraten fängt das Abenteuer an" width="140" height="100" align="right" /><figcaption class="wp-caption-text">The Secret of Monkey Island SE &#8211; bei den Piraten fängt das Abenteuer an</figcaption></figure>Herzlichen Glückwunsch LucasArts! Mit dem Remake habt Ihr genau ins schwarze getroffen! Es macht unglaublich Spaß knapp 20 Jahre später den jungen Piraten durch die Gegend zu steuern. Die neuen Grafiken sind nicht zuviel, aber auch nicht zuwenig des Guten. Gut, dass hier die 2D Grafik nicht einer 3D-Engine weichen musste &#8211; dann nämlich wäre das Nostalgiegefühl höchstwahrscheinlich ruininiert und so kann ich auf meinem HDTV in 1080p und meinem Xbox 360-Gamepad die alten Zeiten wiederaufleben lassen. Was ich mir jetzt noch wünschen würde? Zak McKracken, Maniac Mansion und das geniale Indiana Jones and the Fate of Atlantis im genau selben Stil nochmal erleben zu dürfen. Aber bitte nicht in 20 Jahren&#8230;

[> Screenshots zu The Secret of Monkey Island Special Edition][1]  
[> Video zu The Secret of Monkey Island Special Edition][2] 

**</p> 

The Secret of Monkey Island Special Edition

<u>Allgemeines</u>

</strong>

**Hersteller:** LucasArts**  
System:** PC (über Steam), Xbox 360 (über XBLA)  
**Sprache:** englische Sprachausgabe, deutsche Untertitel  
**Preis:** ca. 10,- EUR  
**USK:** nicht geprüft

**<u>Bewertung</u></p> 

Grafik:</strong> 8/10  
**Sound:** 8/10  
**Steuerung:** 8/10  
**Spielspaß:** 10/10  
**   
** **Gesamturteil: Sehr Gut**   







 [1]: screenshots/item/root/the-secret-of-monkey-island-se
 [2]: videos/item/root/the-secret-of-monkey-island-special-edition