---
title: ' Aion Beta (PC)'
author: gamevista
type: post
date: 2009-08-02T20:27:58+00:00
excerpt: '<p>[caption id="attachment_246" align="alignright" width=""]<img class="caption alignright size-full wp-image-246" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion_small.png" border="0" alt="Aion Beta" title="Aion Beta" align="right" width="140" height="100" />Aion Beta[/caption]Am Freitag den 31. Juli 2009 war es endlich so weit. Einer unserer Redakteure durfte mit 25 anderen Gewinnern in die Closed Beta des Rollenspiels Aion eintauchen. Das Online Rollenspiel Aion, das im fernen Osten schon erhältlich ist, wird nun für den westlichen Markt portiert. Um 21.00 Uhr hieß es dann für alle einloggen und staunen. Was er alles erlebt hat und ob Aion zum erhofften Hit wird lesen sie auf den folgenden Seiten.</p> '
featured_image: /wp-content/uploads/2009/08/aion_small.png

---
**Inhaltsverzeichnis:**

  * Allgemeines zu Aion
  * Die Charaktererstellung
  * Das Userinterface
  * Der Kampf
  * Die Itemmodifizierung
  * Die Charakterentwicklung
  * Das Questen
  * Die Welt von Aion
  * Mein Fazit  
    **  
** 

**Allgemeines zu Aion**

Nachdem ich schon mehrere Online Rollenspiele angetestet hatte, bekam ich nun endlich die Möglichkeit in die nunmehr fünfte Closed Beta, des aus Asien stammenden Online Rollenspiels, Aion reinschauen zu dürfen. Die Betaspielzeit betrug zwei Tage und ich möchte euch nun ein paar Impressionen meiner ersten Gehversuche in Aion vermitteln. Ziel der Beta vom Entwickler NCSoft war es die zwei Fraktionen, Elyos auf der guten Seite und Asmodier auf der bösen, dem Spieler näher zu bringen. Die Maximalstufe die man erreichen konnte war auf 30 Level begrenzt. Da zwei Tage für ein Online Rollenspiel nicht allzu viel Zeit sind, schafften dies wohl nur die Hardcore-Zocker Freunde. Meine Gladiatorin ist auf Level 11 hängen geblieben, da ich mich entschied die Welt lieber ein wenig zu Erkunden als den üblichen Quests nachzugehen. Dabei kamen ein paar sehr interessante Information und Screenshots ans Tageslicht.



<div>
  <p>
    <img class=" size-full wp-image-247" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion100_resize.jpg" border="0" width="600" height="262" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion100_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion100_resize-300x131.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>

Meine Erwartungshaltung zu Aion bzw. dem nächsten WoW-Killer, wie manche es nennen, waren relativ hoch angesiedelt. Immerhin ist das Spiel im asiatischen Markt schon erhältlich und sollte daher möglichst wenig Bugs und sehr viel Content bieten. Natürlich war ich gespannt auf die Grafik. Aion nutzt die Engine des Frankfurter Entwickler Teams Crytek. Die Crysis Engine ist zwar schon etwas angestaubt, da das MMO „nur“ die erste Version nutzt. Diese kam auch bei Farcry 1 zum Einsatz, aber wie man weiß sollte man bei Online Rollenspielen nicht allzu sehr auf den Grafikfaktor setzen, sondern eher darauf achten das man als Entwickler viel Liebe ins Detail steckt.

<p class="MsoNormal">
  Legen wir los. Die Installation verlief Problemlos und nachdem wir den neuen Lokfalisationspatch, der neue Sprachdateien bringen soll, installiert haben starten wir das Spiel. Schon der Loginscreen lässt viel Fantasie und eine tolle Welt erhoffen.
</p>

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-248" src="http://www.gamevista.de/wp-content/uploads/2009/08/loginscreen_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/loginscreen_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/loginscreen_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



**Die Charaktererstellung**

Bevor wir überhaupt irgendetwas machen können erstellen wir uns natürlich einen Charakter.

Dazu suchen wir uns eine der beiden spielbaren Rassen aus. Das Spiel dreht sich um drei Fraktionen. Den Elyos, den Engeln und den Asmodiern, den Dämonen. Die dritte Fraktion die nur von der künstlichen Intelligenz gesteuert wird heißt Balaur, dazu aber später mehr.

Da ich bisher in jedem Rollenspiel die bösen Rassen interessanter fand, wählte ich die dämonischen Asmodier. Meiner Meinung haben Entwickler wesentlich mehr Spass die drakonische Fraktion zu designen. Schade finde ich, das es nur eine Rasse pro Seiten gibt. Durch viele andere Rollenspiele ist man ja mittlerweile gewohnt aus mehreren Rassen zu wählen. Dies trägt auch meiner Meinung nach positiv zur Spieltiefe bei. Hoffen wir das später neue Rassen hinzugefügt werden.  
Das heißt aber nicht das jeder Spieler gleich aussieht. Im Gegenteil. Auch wenn es nur eine Rasse pro Fraktion gibt und ihr in der Hauptstadt der Asmodier „Pandemonium“ euren Handel treibt, wird euch auffallen das kein Charakter dem anderen ähnelt. <span></span>

Nachdem wir uns also für die böse Fraktion der Asmodier entschieden haben geht es an die Charaktererstellung. Dabei suchen wir uns eine von vier primär Klassen aus. Späher, Magier, Priester oder Krieger stehen zur Verfügung. Da wir uns aber erst mit Level 10 wirklich entscheiden mussten ob wir einen Tank (Templer) oder Damage Dealer (Gladiator) spielen möchten, wählen wir den Krieger relativ zielstrebig. Diese Art den Spieler erstmal grob an seine Klasse heranzuführen um dann später sich leichter für eine Spezialisierung entscheiden zu können, fand ich persönlich sehr angenehm.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-249" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0227_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0227_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0227_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>

Nun geht es weiter mit dem anpassen des Aussehens eures Protagonisten. Auf dem Bildschirm sieht man nun sehr gut warum die eine Rasse pro Seite nicht unbedingt wenig Abwechselung im Aussehen der Charaktere sein muss.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-250" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0237_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0237_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0237_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>

Im Character Creator findet ihr zig Möglichkeiten euren Helden im Aussehen zu verändern. Angefangen bei der Größe, könnt ihr als Riese oder als Zwerg euer Unwesen treiben. Aion bietet mit Abstand die beste Charakteranpassung die ich bisher gesehen habe. Dies zieht sich übrigens durch das gesamte Spiel. Aber dazu später mehr. Ob Haarfarbe, Hautfarbe, Gesichtszüge oder Körperfülle, kein Wunsch bleibt unerhört wie ihr euren Alter Ego seht. Insgesamt gibt es wohl mehr als 30 Schieberegler und Optionen für jegliche Körperpartie. Außerdem gibt es auch mehrere Stimmen zur Auswahl.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-251" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0229_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0229_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0229_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



**Das** **Userinterface**

Beim Userinterface muss man sich wohl oder übel bei der Konkurrenz zumindest Ideen holen. Möglichst diese Ideen umsetzen und mit Innovation mischen. Wie ich finde hat dies Aion sehr gut umgesetzt. Da ich viele Jahre mit World of Warcraft, Age of Conan, Herr der Ringe Online oder Warhammer Online verbringen durfte fühle ich mich auch gleich in dem Interface von Aion Zuhause. Wer hier aber ein Standard 0815-Interface vermutet liegt falsch. Aion Entwickler NCSoft zeigt sehr schön wie man bewährtes mit Neuem verbindet.

Über halbtransparente Karten mit dem es sich wunderbar längere Wegstrecken läuft bis hin zu einem Radar auf dem Monster, die angriffslustig oder friedlich sind, angezeigt werden.

Außerdem sind Questgeber und Questmonster zu sehen. Über verlinkte Inhalte im Questtext wird mir sehr schön auf der Karte der gesuchte Ort oder das gesuchte Monster angezeigt.

Aions Interface macht soweit alles richtig. Leider wird eine UI-Modifikationsunterstützung bzw. das nutzen von Plugins wohl oder übel verboten sein. Finde ich sehr schade, denn das Aussehen des Interface ist immer noch Geschmackssache bzw. obliegt unterschiedlichen Anforderungen von Klasse zu Klasse. Dafür können wir das vorhandene UI sehr frei Anpassen. Ob für Raids reicht wird sich zeigen.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-252" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0086_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0086_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0086_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



**  
Der Kampf**

Aions Kampfsystem ist relativ ähnlich im Hinblick zu seinen Artverwandten.

Das Spiel bietet die üblichen Features wie das Anwählen der Gegner mit der Tabulator Taste und das Aktivieren verschiedener Spezialfähigkeiten. Natürlich dürfen Buffs und Cooldowns nicht fehlen. Die Kampfgeräusche und Animationen sind NCSoft sehr gut gelungen. Alles ist sehr gut Vertont und die Animationen sehen sehr flüssig aus. Ähnlich wie in World of Warcraft oder Warhammer Online gibt es verkettete Angriffe und Aktionen. Als Krieger fangen sie mit einem simplen Schwertangriff an um dann angezeigt zu bekommen ob sie entweder eine noch stärkere Attacke ausführen wollen oder eher etwas für ihre Defensive tun möchten.

Diese verketteten Specialmoves gehen dann soweit das sie später sehr starke Angriffe nutzen können. Im PVP ist es wichtig diese Ketten möglichst zu unterbrechen.

Neu ist aber das sie dies auf dem Bildschirm angezeigt bekommen welche Möglichkeiten sie haben. Ähnlich ist das in Age of Conan. Das sollte Neueinsteiger und Anfänger leichter an die Materie der „Wahl der Mittel“ heranführen. Neben Lebens und Manabalken konnte ich noch einen Divine Balken entdecken. Dieser füllt sich langsam pro getöteten Gegner. Ist er voll habt ihr die Wahl denn es gibt 3 Stufen dieses Balkens. Entweder ihr setzt ihn auf Stufe 1 ein und haut eurem Gegner einen mächtigen Zauberspruch oder Attacke um die Ohren. Oder ihr fühlt ihn soweit auf das er auf Stufe 3 ist und könnt somit dreimal hintereinander sehr sftarke Attacken nutzen. Auch sieht man euch an ob ihr mit göttlicher Macht erfüllt seid. Auf Stufe 1 glüht euer Charakter ein wenig, wohingegen er auf Stufe 3 schon gut leuchtet und eure PVP Gegner lieber auf Abstand gehen lässt. Wie ich finde einer sehr nette Idee denn dies hat mir öfter mal das Leben gerettet.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-253" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0168_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0168_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0168_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



**  
Itemization**

Nach ein paar Monstern viel mir auch schon der erste Sockelsteine entgegen. In Aion Manasteine genannt. Nach einer kleinen Animation waren diese schnell in die Startrüstung eingesetzt und gaben mir zum Beispiel erhöhte Werte in Verteidigung oder eine erhöhte Chance zum Parieren. Es gibt auch noch andere Steine diese nannten sich „Power Shards“ und konnten für die Waffen an einen bestimmten Slot im Inventar platziert werden. Mit einer Taste konnte diese aktiviert werden um der Waffe mehr Schaden zu verleihen. Pro Schlag werden dann diese Steine aufgebraucht. Man findet aber genug neue „Power Shards“ während man irgendwelche Monster klopft. Damit begann für mich die große Welt der „Customization“ in Aion. Ich wollte mehr erfahren was man denn nun alles an seiner Ausrüstung ändern kann und wurde überschüttet mit Inhalten.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-254" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0239_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0239_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0239_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>

 

Erstmal wären da wie schon erwähnt die ganzen Sockelmöglichkeiten mit so genannten Manasteinen oder „Power Shards“. In der Hauptstadt Pandemonium fand ich dann einen Händler der den Service anbot „Enchantment Stones“ zu sockeln. Diese erhöhen Waffenwerte bzw. Rüstungswerte. „God-Stones“ sind eine weitere Variante eure Ausrüstung zu modifizieren. Diese können nur in Waffen gesockelt werden und geben den Waffen verschiedene Fähigkeiten. Diese haben eine bestimmte prozentuale Chance in Kraft zu treten und euren Gegnern zum Beispiel eine gewisse Anzahl an Schadenspunkten zusätzlich zuzufügen.

Achja hatte ich schon erwähnt das ihr eure Ausrüstung einfärben könnt. Nein? Das ist aber auch immer noch nicht alles. Ob Brillen, Hüte oder ganze Neujahrstrachten. Mir grauts vor den Feiertagen.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-255" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0127_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0127_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0127_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



 

**  
Charakterentwicklung**

Ab Level 10 bekam ich endlich meine Flügel. Diese Form der Fortbewegung ist wesentlich spannender als all das was ich bisher kannte. Denn zwischen Level 1 und Level 10 ist „Subi“, mein Charakter, nur ein gewöhnlicher Mensch. Diese wird in den Kreis der Deava aufgenommen und bekommt neben ein paar netten Items auch ihre Flügel. Flügel? „Mh ok“ dachte ich mir,  <span></span>da ich in World of Warcraft einen Druiden gespielt hatte war ich ja gut trainiert um Umgang mit der Flugform. Aber weit gefehlt. Mein erster Flugversuch endete mit einem Absturz aus 10 Metern der zum Glück nicht tödlich war. In Aion ist die Flugzeit begrenzt. Das heißt ihr könnt am Anfang nur für mehrere Sekunden fliegen und müsst aufpassen das wenn die Zeit rum ist ihr wieder auf dem Boden seid. Das Fliegen wird im PvP essentiell wichtig werden, da ihr durch die begrenze Flugzeit und die Option im Flug kämpfen zu können, ganz neue Taktiken entwickeln müsst. Wie alles in Aion kann auch dies mit einem Upgrade versehen werden. Dadurch werdet ihr unter anderem eine längere Flugzeit erwerben können indem ihr euch neue Federn für eure Flügel kauft.

Mit Stigmasteinen kann man seinen Charakter dahingehend modifizieren das er neue Fähigkeiten bekommt oder alte Ausbaut. Diese Steine bekommt ihr von getöteten Gegnern und gewähren euch zum Beispiel mehr Gesundheitspunkte für kurze Zeit oder die Möglichkeit 2 Waffen gleichzeitig zu tragen.

Auf der maximalen Stufe die ihr mit Level 50 erreichen könnt sind 5 solcher Stigmasockel verfügbar. Diese weitere Möglichkeit der Individualisierung des Charakters ist sicher für das wählen mehrerer PvE- oder PvP-Skillungen gedacht.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-256" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0064_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0064_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0064_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



**  
Die Quests**

Um es kurz zu machen. das Questen in Aion unterscheidet sich kaum von seinen Rollenspielkollegen. Wie immer sollen wir dies sammeln oder jenes töten. Dies Erkenntnis werdet ihr sofort haben wenn ihr den ersten Questgeber ansprecht. Die üblichen Icons über den Köpfen der NPC zeigen euch das dieser etwas von euch möchte.

Die Qualität der Quests geht in Ordnung, wieder mal darf ein Held Blumen sammeln und schweinsähnliche Kreaturen abmurksen. Sicher könnte man das noch etwas spannender machen, vielleicht wird es das ja wenn man in den PvP Gebieten Questen darf. So ziehe ich aber meine Bahnen und sammle und meuchle was das Zeug hält um auf das nächste Level zu kommen. Beeindruckt, durch die Erkenntnis, bin ich trotzdem, denn Aion setzt in der asiatischen Version weniger auf Quests.  <span></span>Das so genannte „Grinden“ (töten von Monstern für Erfahrungspunkte) steht dort an der Tagesordnung und ist anscheinend auch sehr beliebt. Was ich sehr schön fand sind die aus Lord of The Rings Online bekannten Kampagnen Quests die die Story vorantreiben sollen. Diese werden auch schick mit kleinen Ingame-Videos unterlegt.

<div style="text-align: center">
  <p>
    <img class=" size-full wp-image-257" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0083_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0083_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0083_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
  </p></p>
</div>



**  
Die Welt von Aion**

Nun möchte ich euch etwas über die Welt und die enthaltenen Features erzählen. Natürlich muss man hier erstmal auf die Grafik eingehen. Dieses Spiel hat möglicherweise eine der schönsten Grafiksettings die es im MMO Markt gibt. Klar sagen hier einige, es ist ja auch die Crysis Engine. Richtig, aber es ist nur die Cryengine 1 die schon beim etwas betagten Far Cry zum Einsatz kam. Trotzdem versucht hier NCSoft mit sehr schönen Lichteffekten, sauberen Texturen und sehr guter Performance der etwas angestaubten Grafikengine wieder ein bisschen Leben einzuhauchen. Neben Age of Conan und Lord of The Rings: Online hat der Hersteller hier, wie ich finde, das beste Maß einer guten Grafik und flüssigem Gameplay gefunden.

<p class="MsoNormal">
  Wie ich weiter oben schon erwähnte setzt Aion auf ein recht großes Repertoire aus bekannten Online Rollenspiel Features. Ob Flugmeister wie in World of Warcraft, ein Mailsystem oder das obligatorische Auktionshaus. Aion bietet viele bekannte Features auf einem funktionierenden Level. Was ich sehr schön fand war die private Handelsfunktion obwohl es Auktionshäuser gibt. Damit könnt ihr euren Charakter mal ebend als Händler auf den Platz stellen. Dieser schreit dann euren eingegebenen Text in die Welt hinaus und Interessenten können sich eigenständig an euren Waren bedienen. Wenn euer Charakter in einem Teich steht interagiert er mit dem Wasser, wirbelt es ein bisschen auf. Ist es regnerisch wird ein Regenschirm in Form eines großen Blattes herausgeholt. Aion lässt die Charaktere auch sehr schön mit der Umwelt interagieren, auch die Emotes waren mal was Neues. Da wird nicht nur mit Händen und Füssen gestikuliert, sondern auch mal beim hinsetzen ein fliegender Teppich rausgeholt. Wenn es Zeit ist schlafen zu gehen bettet sich euer Held in einer leuchtenden Wolke. Das dies eine Beta war fiel mir kaum auf. Was ich von anderen Betas nie behaupten konnte.
</p>

<div style="text-align: center">
  <img class=" size-full wp-image-258" src="http://www.gamevista.de/wp-content/uploads/2009/08/aion0040_resize.jpg" border="0" width="600" height="375" srcset="http://www.gamevista.de/wp-content/uploads/2009/08/aion0040_resize.jpg 600w, http://www.gamevista.de/wp-content/uploads/2009/08/aion0040_resize-300x188.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</div>

Die Welt an sich wird grob in 3 Teile untergliedert. Auf der einen Seite der Kontinent für die Elyos auf der anderen das Gebiet der Asmodians. In der Mitte befindet sich die PvP Zone mit dem Namen Abyss, die man mit Level 25 betreten kann. Da es nur 2 Rassen gibt ist das Questen relativ linear. Wen man einen zweiten Charakter hochleveln möchte, darf man sich nicht viel Abwechselung versprechen. Diesen Fakt fand ich bei World of Warcraft, mit den unterschiedlichen Startzonen und der Möglichkeit mal das Startgebiet wechseln zu können, sehr viel besser. Auch stelle ich mir das Questen im Abyss sehr schwer vor, da man sicher nicht ohne Gruppe klarkommen wird. Dieses Problem ist aber auch bei vielen anderen PvP-lastigen Spielen vorhanden. Ich hoffe, dass die Entwickler hier eine gute Lösung gefunden haben. In der nächsten Closed Beta werde ich versuchen im Abyss mein Unwesen zu treiben um diesen Umstand zu testen. Was ich befürchte ist das Aion uns in eine sehr lineare Questgeschichte schickt und wenig Alternativen lässt. 



**  
Mein Fazit**

Aion war für mich keine Beta. Dieses Spiel hätte so auch gut im Regal eines Ladens stehen können. Eine Beta sollte Bugs enthalten an denen noch gearbeitet werden muss. Diese konnte ich nicht ausmachen. Das Online Rollenspiel bietet eine hammermäßige Grafik die auch auf schlechteren PCs flüssig laufen sollte. Der Umstand das Aion in Übersee schon erhältlich ist macht es den europäischen Spielern relativ einfach, denn das Spiel hat kaum Kinderkrankheiten, Features die erhofft wurden sind auch enthalten und werden nicht monatelang versprochen. Genau das ist einer der Hauptgründe warum man sich Aion kaufen sollte, hier bekommt man ein fertiges Spiel was in der letzten Zeit im MMO-Markt eher die Seltenheit ist.

Das Spiel fokussiert sehr den PvP-Teil eines MMOs. Ich hoffe das hier der Entwickler funktionierende Mechanismen eingebaut hat um typische „Rush and Zerg“ Taktiken zu vermeiden. Eine Variante gibt es bereits. Wie in jedem MMO verdient ihr Punkte in dem ihr andere Spieler im „Abyss“ tötet aber ihr verliert auch Punkte solltet ihr getötet werden.Aion ist kein Spiel was mit Innovationen um sich wirft. Sicher wird hier das ein oder andere neu in die Online Rollenspiel Szene eingeführt. Es wird aber auch kräftig von Genre Kollegen kopiert, was ich nicht schlimm finde wenn es gut umgesetzt ist. Aion lässt in der Hinsicht nichts vermissen. Obwohl Aion für den asiatischen Markt entwickelt wurde, werden gerade World of Warcraft oder Warhammer Online Spieler die Zielgruppe hier in Europa oder Amerika sein. Diese werden sich sofort heimisch fühlen.

Raiden könnt ihr natürlich auch. Aion bietet hierbei ein bekanntes System, so genannte Worldbosse bieten sehr gute Items als Drop. Natürlich schaut sich das die gegnerische Fraktion nicht friedlich mit an und wird munter mitmischen. Bleibt abzuwarten ob es auch instanzierte Raidbosse geben wird. Die typischen Instanzen habe ich zwar noch nicht gesehen aber sie sollen enthalten sein. Ob diese kontinuierlich weiterentwickelt werden bleibt abzuwarten.

[> Aion Beta Event Screenshots][1]  
[> Aion Videos][2]

<div class="textblock">
  
</div>

<div class="textblock">
  
</div>

<div class="textblock">
  
</div>

 [1]: screenshots/item/root/aion-beta-screenshots
 [2]: videos/alphaindex/a