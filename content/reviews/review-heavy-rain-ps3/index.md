---
title: ' Heavy Rain (PS3)'
author: gamevista
type: post
date: 2010-02-18T09:57:58+00:00
excerpt: '<img class="caption alignright size-full wp-image-1338" src="http://www.gamevista.de/wp-content/uploads/2010/01/heavyrain.jpg" border="0" alt="Heavy Rain" title="Heavy Rain" align="right" width="140" height="100" />Der Entwickler <a href="http://www.quanticdream.com/" target="_blank" rel="nofollow">Quantic Dream</a>, besser bekannt durch Spiele wie Fahrenheit oder The Nomad Soul, kündigte bereits 2008 ein neues, innovatives Spiel an. Bisher wurden nur sehr wenige Informationen im Bezug auf Story, Gameplay und Technik bekanntgegeben. Dies soll sich nun ändern, denn Heavy Rain ist da.<br />Um es gleich im vorweg zu sagen - Heavy Rain ist kein Spiel, wie wir es kennen. Es ist eher ein interaktiver Film mit Hauch von Action Adventure. Aber der Reihe nach.<br /><br /></p> '
featured_image: /wp-content/uploads/2010/01/heavyrain.jpg

---
**The Beginning**

Das Spiel beginnt in einem Haus, wie man es aus der besten Familiensiedlung her kennt. Der Hauptcharakter Ethan Mars ist vom Beruf Architekt, er hat zwei Kinder und eine wunderschöne Frau. Die Welt scheint (noch) heil zu sein. Dies jedoch ändert sich schnell, nach dem einer der Söhne bei einem Verkehrsunfall stirbt. 2 Jahre sind vergangen und Ethan ist ein seelisch zerbrochener und geschiedener Mann. Ab hier beginnt der Thriller des Jahres, denn ein Killer, der Origami Killer, entführt Kinder und lässt diese durch das Regenwasser ertrinken. Auch der zweite Sohn von Ethan wird Opfer der Entführung und der Wettlauf gegen die Zeit beginnt. Wer ist der Origami Killer?

**Die Charaktere**

Neben Ethan Mars schlüpft Ihr in die Rolle drei weiterer Charaktere. Der Detektiv Scott Shelby, ein pensionierter Polizist und der FBI Agent Norman Jayden, der ein Drogenproblem hat, ermitteln wegen den Morden des Origami Killers. Last but not least ist da auch die schöne Reporterin Madison Paige, die unter Schlaflosigkeit leidet und in ein Motel zieht, in welches ebenfalls der angeschlagene Ethan hingezogen ist. Beide treffen dort aufeinander und entwickeln eine emotionale Bindung zueinander.<figure id="attachment_1460" style="width: 600px" class="wp-caption alignnone">

<img class="caption size-full wp-image-1460" src="http://www.gamevista.de/wp-content/uploads/2010/02/ethan_mall.png" border="0" alt="Ist Ethan Mars der Origami Killer?" title="Ist Ethan Mars der Origami Killer?" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/ethan_mall.png 600w, http://www.gamevista.de/wp-content/uploads/2010/02/ethan_mall-300x169.png 300w" sizes="(max-width: 600px) 100vw, 600px" /><figcaption class="wp-caption-text">Ist Ethan Mars der Origami Killer?</figcaption></figure> 



 

**Das Gameplay**

Während des Spiels muss Ethan eine Menge Aufgaben gegen die Zeit lösen, denn jede Stunde die verstreicht, gefährdet das Leben seines Sohnes. &#8222;Wie weit würdest Du gehen&#8230;&#8220; ist der Slogan des gesamten Spiels. Dabei spielt das Spiel klar mit den Emotionen des Zuschauers bzw. des Spielers. Viele Wendungen in der Geschichte und ständig wechselnde Aufgaben verleihen dem Spiel den besonderen Thrill.

Die Steuerung des Spiels ist innovativ. Mit den rechten Tasten (Kreis, Rechteck usw.) führt Ihr diverse Action Elemente aus. Dabei kann es in manchen Szenen zum hektischem rumgeklicke und Fingerakrobatik kommen, denn dann ist Schnelligkeit gefragt. Auch werden die Action &#8211; Tasten bei Unterhaltung mit anderem NPCs  benutzt. Als Highlight wurde die Sixaxis Unterstützung ins Spiel eingabaut. In manchen Quicktime Szenen müsst Ihr neben den angesprochenen Actiontasten auch den Controller nach links bzw. rechts schwingen oder einfach nur rütteln. Auch die Geschwindigkeit spielt eine Rolle &#8211; bewegt Ihr den Controller manchmal zu schnell, so führt dies zum negativen Ergebnis. Beispielsweise müsste Ihr Ethan mit einem Verband verbinden. Tut Ihr das zu schnell, so schreit er autsch. Tut Ihr dies langsam und mit Gefühl, so wird er sich bei Euch bedanken.   
Mit R2 lasst Ihr Euren Charakter duch das Szenario laufen, was manchmal sehr steif wirkt und einige Male nervt, wenn die Kamera, die Ihr im übrigen mit L1 in eine andere Perspektive umschalten könnt, nicht von alleine wechselt. Mit L2 könnt Ihr sehen was Euer Charakter gerade denkt und könnt so an einige Tipps für den weiteren Spielverlauf kommen.<figure id="attachment_1461" style="width: 600px" class="wp-caption alignnone">

<img class="caption size-full wp-image-1461" src="http://www.gamevista.de/wp-content/uploads/2010/02/ethan_madison.png" border="0" alt="Madison hilft Ethan im Laufe des Abenteuers" title="Madison hilft Ethan im Laufe des Abenteuers" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/ethan_madison.png 600w, http://www.gamevista.de/wp-content/uploads/2010/02/ethan_madison-300x169.png 300w" sizes="(max-width: 600px) 100vw, 600px" /><figcaption class="wp-caption-text">Madison hilft Ethan im Laufe des Abenteuers</figcaption></figure> 



 

Die Aufgaben und Rätsel sind selbsterklärend und leider nicht besonders schwer. Ist man z.B. in einem Raum und kommt nicht ans Fenster, so läuft man durch den Raum und siehe da, man kann nur eine Kiste verschieben. Was nimmt man dann? Die Kiste natürlich. Man hätte sich beim Rätsel Design etwas mehr Mühe machen sollen um bestimmte Aufgaben zu lösen &#8211; z.B. durch Kombination von verschiedenen Gegenständen miteinander.   
Der FBI Agent Norman Jayden besitzt noch eine Hightech Ausrüstung, mit der er verschiedene Spuren scannen und analysieren kann. Dies ist zwar nett, jedoch erhöht in keinster Weise das Niveau der Rätsel. So bleibt der Rätselspaß leider auf der Strecke bzw. auf dem Niveau eines Säuglings.

Die Dialoge dafür sind umso besser geworden. Man kann durch die Dialoge die Geschichte ziemlich beeinflussen, denn spielt man z.B. den bösen Cop, so zieht sich diese Entscheidung durch das gesamte Spiel. Man muss daher schon überlegen, was man sagt und vorallem wie man es sagt. Dabei kann man zwischen einem aggresivem oder höflichen Auftreten wählen.   
Die deutsche Synchronisation ist ziemlich gut geworden, bis auf ein paar Aussetzer. In machen Szenen passt die Sprache und die Betonung einfach nicht zur Situation.

Das gesamte Spiel lebt von Entscheidungen. Es gibt Szenen, die den Ausgang des Spiels durch Euere Entscheidung positiv oder negativ beeinflussen. Man kann im Spiel nicht wirklich sterben (Game Over Screen), jedoch wirkt sich &#8222;ein Sterben&#8220; auf die weitere Geschichte aus. Hier wollen wir nicht allzuviel verraten um nicht zu spoilern.

Die Grafik in Heavy Rain ist im grossen und ganzen sehr gut geworden. Ähnlich wie in der Synchronisation gibt es aber auch hier einige Aussetzer, wie z.B. klobige Texturen an manchen Stellen, sie sich aber im grossen und ganzen in Grenzen halten.<figure id="attachment_1462" style="width: 600px" class="wp-caption alignnone">

<img class="caption size-full wp-image-1462" src="http://www.gamevista.de/wp-content/uploads/2010/02/madison.png" border="0" alt="Heavy Rain setzt auf Emotionen der Charaktere" title="Heavy Rain setzt auf Emotionen der Charaktere" width="600" height="346" srcset="http://www.gamevista.de/wp-content/uploads/2010/02/madison.png 600w, http://www.gamevista.de/wp-content/uploads/2010/02/madison-300x173.png 300w" sizes="(max-width: 600px) 100vw, 600px" /><figcaption class="wp-caption-text">Heavy Rain setzt auf Emotionen der Charaktere</figcaption></figure> 



 

**Fazit**

Heavy Rain ist ein grossartiges Spiel mit einigen Mängeln geworden. Mich stört, dass man im Bezug auf das Rätseldesign nicht mehr gemacht hat. Insgesamt jedoch ist das Spiel sehr stimmig und zieht den Spieler bzw. Zuschauer in seinen Bann. Man will unbedingt wissen, wer der Origami Killer ist. Leider ist der Ablauf, trotz der möglichen Entscheidungen die der Spieler treffen kann, sehr linear aufgebaut. Man muss sich aber eines klarmachen &#8211; es ist kein klassisches Spiel, sondern ein interaktiver Film. Wer Psychothriller mag, wird Heavy Rain lieben!

<span style="text-decoration: underline;"><strong>Bewertung 92%</strong></span>