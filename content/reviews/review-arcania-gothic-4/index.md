---
title: ' Arcania: Gothic 4'
author: gamevista
type: post
date: 2010-10-25T17:02:35+00:00
excerpt: '<img class="caption alignright size-full wp-image-1648" src="http://www.gamevista.de/wp-content/uploads/2010/03/berg_small.png" border="0" alt="Arcania: Gothic 4" title="Arcania: Gothic 4" align="right" width="140" height="100" />Spellbound möchte mit seinem neusten Teil der Gothic-Serie <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> abermals zeigen das Gothic zu den Highlights des Rollenspiel-Genres zählt. Ob den Entwicklern ein neuer Meilenstein gelungen ist und was das neue Gothic ausmacht lest ihr in unserer Review.</p> '
featured_image: /wp-content/uploads/2010/03/berg_small.png

---
Als namenloser Held spielt ihr einen Schäferjungen der miterleben muss wie sein Dorf in Flammen aufgeht. Angegriffen von Söldnern unter dem Zeichen des Adlers. Nun verlasst ihr die Insel Feshyr auf der Suche nach Wahrheit und Rache. 10 Jahre sind mittlerweile seit dem letzten Teil der Gothic-Reihe vergangen. Im neusten Teil stehen euch wieder zahlreiche Begleiter und neue Charaktere zur Seite. In <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> trefft ihr aber auch alte Bekannte wie Milten, Diego, Gorn oder Xardas.

Als Einstieg führt uns Spellbound durch ein Tutorial auf der Insel Feshyr. Vergleichsweise unkompliziert kommt der neue Teil daher und bietet Action mit nicht allzu tiefgründigen Rollenspielelementen. Wem Fallout 3 oder Dragon Age zu kompliziert war ist hier gut beraten. Anders als in den vorangegangenen Teilen bietet Gothic 4 keine offene Spielwelt und lässt euch selten Entscheidungen fallen. Gelegentlich dürft ihr zwischen einem diplomatischen Weg wählen, anstatt einfach drauflos zu holzen. Die Welt teilt sich in mehrere Zonen auf, dabei treffen wir oft auf künstliche Kanten an denen wir gelegentlich hängen bleiben. Ein typischer Fehler der aber nicht weiter ins Gewicht fällt.

<p style="text-align: center;">
  <img class=" size-full wp-image-2375" src="http://www.gamevista.de/wp-content/uploads/2010/10/004-entering-the-cleaved-maiden-inn.jpg" border="0" width="600" height="356" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/004-entering-the-cleaved-maiden-inn.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/004-entering-the-cleaved-maiden-inn-300x178.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *

  





Nachdem wir die ersten Quests auf der Startinsel abgeschlossen haben, reisen wir zur großen Hauptinsel Argaan. Dort beginnen wir mit der Hauptquestlinie und hangeln uns von einem Gebiet zu nächsten. Dabei dürfen wir erst das nächste Gebiet betreten, nachdem wir alle Missionen erfüllt haben. Dieser Aspekt stört schon sehr, schließlich möchte man die Welt erkunden und natürliche Grenzen, Monster über unserem Level, hätten ihr übriges getan.    
Auch der Tag- Nachtwechsel wurde inkonsequent in die Welt von Arcania implementiert. So recht können wir es dem Spiel nicht abkaufen das es Nacht wird, wenn selbst die Bewohner weiter ihren Geschäften nachgehen. Denn die Bewohner interessiert es herzlich wenig wenn die Sonne ihre Lichter ausknipst. Ein Umstand den das Rollenspiel wie Fallout 3 oder das neue Fallout: New Vegas glänzend umgesetzt haben. Die Bewohner gehen dort schlafen wenn es dunkel wird. Auch unser Held kann sich lediglich in Rollenspielmanier ins Bett legen. Einen Nutzen zieht er daraus dabei aber nicht. Zum Rahmen der Rollenspielelemente zählt noch das Bücherlesen. Wieder ein Element was keinerlei Funktion besitzt. Vermutlich wollte Spellbound hier Inhalte einfügen, hat sie aber aus Zeitdruck entfernt. Dennoch wirkt die Spielwelt dank der neuen Grafikengine überaus anschaulich und mystisch.

Beim Kampfsystem haben sich die Entwickler keine Schnitzer erlaubt. Mit Kombo-Attacken greifen wir mit verschiedensten Waffen, ob im Nah- oder Fernkampf, rollenspieltypische Monster an. Dabei weichen wir den Angriffen der Gegner gekonnt mit Hechtrollen oder Seitwärtsschritten aus. Das artet zwar in so manche Klickgelage aus, dennoch wirken die Kämpfe immer taktisch und spannend indem wir angreifen und ausweichen müssen.

<p style="text-align: center;">
  <img class=" size-full wp-image-2376" src="http://www.gamevista.de/wp-content/uploads/2010/10/argaan_deer-hunting-in-the-woods.jpg" border="0" width="600" height="356" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/argaan_deer-hunting-in-the-woods.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/argaan_deer-hunting-in-the-woods-300x178.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *

  





Ein grundsolides System was dennoch an Innovationen vermissen lässt. Natürlich darf bei einem richtigen Rollenspiel die Zauberei nicht fehlen, leider fallen diese sehr spärlich aus. Mit nur drei Zaubern, Feuer, Eis und Blitz ist Argaan definitiv keine Welt voller Magie. Dennoch dürft ihr per Stufenaufstieg eure Zauber verstärken und so immer mächtigere Feuerbälle verschießen. Auch wenn es Spezialisierungen gibt, bei denen ihr mehr Schaden im Fernkampf verursacht, wird euer Held nicht ohne Nahkampf auskommen. Dazu wird das komplette Repertoire an Äxten, Keulen und Schwertern aufgefahren. Als sehr positiv sind die kaum vorhandenen Bugs zu erwähnen. Wer ältere Gothic-Teile erleben durfte wird über diesen Umstand sehr erfreut sein.

Das Charaktersystem von <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> gestaltet sich angenehm einfach. So stehen euch fünf Charakterwerte und die schon genannten drei Zauber zur Verfügung. Die Werte lassen sich per Levelaufstieg so steigern, dass ihr z.B. bei Nahkampfangriffen härtere Schläge austeilt. Selbst das genretypische Craftingsystem ist wenig anspruchsvoll. Anstatt eure Fähigkeiten steigern zu müssen, reicht lediglich das passende Rezept und die richtigen Zutaten die ihr verstreut auf der Welt finden könnt.

So verbringt ihr zahlreiche Stunden beim Erkunden der Strände, Wälder und Hügel der Insel Argaan. Wer ordentliche Hardware in seinem Rechner verbaut hat, darf sich auf einen imposanten Tag- Nachtwechsel, auf hübsche Täler und stimmungsvolle Bergseen freuen. Dabei solltet ihr für einen optimalen Spielegenuss mindestens 4 GByte RAM sowie einen Intel Core I7 mit 3 GHz oder das AMD Pendant Phenom 2 x 4 mit 3 GHz betreiben. Als Grafikkarte empfiehlt sich eine NVIDIA Geforce GTX 295 oder eine ATI 5870. Als Betriebssystem werden Windows XP sowie Vista und 7 unterstützt.

<a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> benutzt den Securom Kopierschutz und muss daher einmalig im Internet aktiviert werden. Ihr könnt eure Kopie unbegrenzt oft installieren und auf bis zu drei Windows-PCs gleichzeitig betreiben. Erfreulich ist das die DVD zum Spielstart nicht im Laufwerk liegen muss. Auch benötigt ihr keine dauerhafte Internetverbindung.

<p style="text-align: center;">
  <img class=" size-full wp-image-2377" src="http://www.gamevista.de/wp-content/uploads/2010/10/009-encounter-with-bloodflies.jpg" border="0" width="600" height="356" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/009-encounter-with-bloodflies.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/009-encounter-with-bloodflies-300x178.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *

  





**Fazit**

Viel zu oft werden Fortsetzungen in der Spielebranche ohne Konzept und  wirkliche Neuerungen präsentiert. Ein zuvor sehr erfolgreiches Produkt wird in den nachfolgenden Teilen einfach nur noch abgewirtschaftet. Dabei ist <a href="http://www.arcania-game.com/" target="_blank">Arcania: Gothic 4</a> ein grundsolides Rollenspielabenteuer geworden, leider nicht mehr und nicht weniger. Zu wenig Inhalt für echte Rollenspielkenner, gerade so passend für Anfänger. Statt Spieltiefe liefert der Titel enttäuschend monotone Quests mit linearer Handlung. Dabei haben die Vorgänger der Gothic-Reihe gerade das ausgemacht. Fans der ersten Teile werden mit diesem Produkt nicht auf ihre Kosten kommen, dennoch bietet das Spiel, gerade für Einsteiger, unkomplizierte Actioneinlagen. Auch stellt man im fortschreiten des Spiels immer wieder fest, dass es in Arcania an allen Ecken und Enden an Inhalten fehlt. Echte Rollenspielelemente die Atmosphäre schaffen, gepaart mit einem guten Story Plot lässt auf den nächsten Teil hoffen.

**Wertung: 75%**

<p style="text-align: center;">
  <img class=" size-full wp-image-2378" src="http://www.gamevista.de/wp-content/uploads/2010/10/feshyr_fight-at-diegos-boat.jpg" border="0" width="600" height="356" srcset="http://www.gamevista.de/wp-content/uploads/2010/10/feshyr_fight-at-diegos-boat.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/10/feshyr_fight-at-diegos-boat-300x178.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

* * *

  

