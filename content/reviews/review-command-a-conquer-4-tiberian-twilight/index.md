---
title: ' Command & Conquer 4 Tiberian Twilight'
author: gamevista
type: post
date: 2010-03-24T19:18:50+00:00
excerpt: '<img class="caption alignright size-full wp-image-1604" src="http://www.gamevista.de/wp-content/uploads/2010/03/cc4_small.png" border="0" alt="Command & Conquer 4 Tiberian Twilight" title="Command & Conquer 4 Tiberian Twilight" align="right" width="140" height="100" />Der Entwickler <strong>Electronic Arts</strong> präsentiert den nunmehr vierten Teil der Command & Conquer-Serie. <strong>EA </strong>will mit <strong>Tiberian Twilight</strong> die legendäre Echtzeitstrategiereihe zum Abschluss bringen und mit zahlreichen Neuerungen beim Spieler punkten. Ob der neuste Spross auch Innovationen mit sich bringt und nicht wieder allzu bunt vom Setting ausfällt, erfahren Sie in unserer Review für <a href="http://www.commandandconquer.com" target="_blank">Command & Conquer 4: Tiberian Twilight</a>.</p> '
featured_image: /wp-content/uploads/2010/03/cc4_small.png

---
In Zeiten von sehr guten Strategietiteln wie Company of Heroes, Starcraft 2, Dawn of War 2 oder dem nahenden R.U.S.E., tat sich EA in der letzten Zeit sehr schwer mit wirklichen Innovationen mit ihrem Strategieflagschiff Command & Conquer. Schon die letzten Titel waren eher Mittelmaß und an das jüngere Publikum gerichtet. Doch nun geht **Tiberian Twilight** neue Wege. Weg vom üblichen Basisbau und Rohstoffe sammeln, hin zum heutzutage typischen schnellen Gameplay. Mit neuem Konzept, aufgemotzter Grafik, der Auflösung wer Kane wirklich ist und was seine Bruderschaft von NOD plant, versucht der Entwickler neue Akzente zu setzen.

Wie schon im Vorfeld von EA angekündigt, werden auch im vierten Teil reale Filmsequenzen die Story vorantreiben. So wird mit den doch sehr sympathisch wirkenden Szenen an die alten guten Tage von C&C erinnert. Obwohl wir die GDI einen Bund mit Kane eingeht kommt es zum Eklat zwischen NOD und GDI, weil Teile der Bruderschaft gegen Kane rebellieren. Somit stehen wieder zwei Kampagnen zur Auswahl und beide Parteien im Clinch.

Wie schon erwähnt wurde gibt es im vierten Teil keinen Basisbau im eigentlichen Sinne. Auch ist das Sammeln von Rohstoffen mit Sammlern, deren KI immer ein schmerzliches Thema war, der Crawler Unit zum Opfer gefallen. Diese Basiseinheit verbindet nun alles was das Produzieren von Einheiten angeht.

<p style="text-align: center;">
  <img class=" size-full wp-image-1605" src="http://www.gamevista.de/wp-content/uploads/2010/03/cc4_0023_wm.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/03/cc4_0023_wm.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/03/cc4_0023_wm-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>

So wählt man am Anfang der Mission einen von drei Crawlertypen, den Supportcrawler der Luftschläge zulässt, den Defensivcrawler mit dem man Bunker und Türme bauen kann oder den offensiven Crawler der starke Panzer produziert. Natürlich darf man auch den Typ des Stahlriesen in laufenden Missionen wechseln, z.B. bei neuen Missionszielen.

Taktisch möchte EA somit neue Wege gehen, was doch sehr gut funktioniert.   
Neu ist auch das Einheitenlimit, somit gibt es eine maximale Obergrenze an Einheiten die man auf das Schlachtfeld schicken darf. Mit Einheitenpunkten kann man somit zum einen massig Infanterie bauen, oder ein paar Panzer. Natürlich wird der Gegner darauf reagieren und nach dem Stein-Schere-Papier Prinzip, den passenden Konter parat halten. Somit ist es in Tiberian Twilight Programm den passenden Einheitenmix zu finden.   
Dies dürfte alte C&C-Fans doch sehr vor den Kopf stoßen, da viele Einheiten gepaart mit der Schlacht um Ressourcen immer auf dem Plan stand.

Das die Einheitenproduktion ohne Ressourcen auch ihre Nachteile hat, zeigt sich dann auch in den Missionen bei denen es um das rein militärische Bezwingen des Gegners geht. Denn man kann nun unbegrenzt Infanterie und Panzer aus seinem Crawler rollen lassen. Deswegen hat sich der Entwickler in den meisten Einsätzen mehr als nur das Plumpe zerstören des Gegners einfallen lassen. So gibt es diverse Begleit- oder Schutzmissionen bei denen es eher um die Taktik und den Einheitenmix geht. Natürlich darf da die Einstellung von drei verschiedenen Schwierigkeitsgraden nicht fehlen.

<p style="text-align: center;">
  <img class=" size-full wp-image-1606" src="http://www.gamevista.de/wp-content/uploads/2010/03/cc4_0069_wm.jpg" border="0" width="600" height="338" srcset="http://www.gamevista.de/wp-content/uploads/2010/03/cc4_0069_wm.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/03/cc4_0069_wm-300x169.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>


Die Kampagne von <a href="http://www.commandandconquer.com" target="_blank">Command & Conquer 4: Tiberian Twilight</a> ist in zwei spielbare Feldzüge aufgeteilt. Die GDI und NOD Kampagne bieten zusammen damit ca. 15 Stunden Spielzeit. Was natürlich etwas wenig ist. Dafür sind die Missionen knackig, allerdings manchmal doch sehr kurz gehalten. Die außerirdischen Scrin sind leider nicht mehr enthalten. Durch das erfolgreiche absolvieren von Einsätzen erhält der Spieler Erfahrungspunkte, die Sie wiederum in Upgrades und neue Einheiten investieren dürfen. Ob eine gesteigerte Schussrate, höhere Reichweite oder schnellere Einheiten, hier hat der Spieler die freie Wahl. Somit darf der Spieler sich bis zum Level 20-General hocharbeiten und nach belieben seine Truppen nach belieben aufwerten.

Die Computer-Intelligenz darf sich diesmal auch so nennen. Denn auf dem höchsten Schwierigkeitsgrad kontert der Computergegner unserer Truppen mit den richtigen Einheiten. So werden z.B. bevorzugt unsere Heiler aufs Korn genommen und den nervigen Sammler, aus früheren Tagen, gibt es zum Glück ja nicht mehr.   
Die Grafik basiert auf einer aufgebohrten, etwas angestaubten, Sage-Engine. Leider ist dieser Umstand auch wieder einer der großen Schwächen vom neusten C&C-Titel. Denn leider war in der Vergangenheit die Grafik selten auf dem Stand der Zeit. So sehen Explosionen weniger bombastisch aus als wir erhofft hatten. Auch sind Einheiten, Gebäude und Landschaft eher detailarm. Der Sound der Einheiten ist dagegen ausgesprochen gelungen und auch musikalisch wird einiges geboten.

Natürlich darf bei einem Strategiespiel der Mehrspielermodus nicht fehlen. Neben Skirmish-Scharmützeln gegen den Computer, sind Gefechte mit bis zu neun weiteren Teilnehmern möglich. Auch kann man mit einem Freund die Kampagne im Koop-Modus durchleben.   
Nach der Installation muss <a href="http://www.commandandconquer.com" target="_blank">Command & Conquer 4 Tiberian Twilight</a> mit einem EA-Account verknüpft werden. Damit Sie das Spiel starten können ist zwingend eine Internetverbindung, ähnlich Ubisofts System, erforderlich. Wer in einer Partie die Verbindung zum Server verliert erhält keine Erfahrungspunkte. Einen LAN-Modus gibt es leider nicht. **Command & Conquer 4** ist ab sofort im Handel erhältlich und kostet rund 40 Euro.

<p style="text-align: center;">
  <img class=" size-full wp-image-1607" src="http://www.gamevista.de/wp-content/uploads/2010/03/cnc4_sp_polynesiascreen9.jpg" border="0" width="600" height="334" srcset="http://www.gamevista.de/wp-content/uploads/2010/03/cnc4_sp_polynesiascreen9.jpg 600w, http://www.gamevista.de/wp-content/uploads/2010/03/cnc4_sp_polynesiascreen9-300x167.jpg 300w" sizes="(max-width: 600px) 100vw, 600px" />
</p>


**Fazit**

**Electronic Art**s hat mit <a href="http://www.commandandconquer.com" target="_blank">Command & Conquer 4: Tiberian Twilight</a> einen Neuanfang versucht und auf altgediente Elemente wie Basisbau und der Kampf um Ressourcen verzichtet.    
Ob das nun gut oder schlecht ist, sei dahingestellt. Die neue Crawler-Unit spielt sich allerdings frisch und ganz anders als alle bisherigen C&C-Teile. Somit ist das neue Spielkonzept für echte Fans, der angeblich letzte Teil der Serie, doch eher gewöhnungsbedürftig. Für Spieler die offen für Neues sind, dürfte dies eine willkommene Abwechselung sein. Ein bisschen Basisbau bzw. Ressourcenmanagement hätte dem Spiel dann aber doch ganz gut getan.    
Auf Dauer fehlt dem Spiel leider die Abwechslung. So sind die Missionen recht kurz geraten. Auch waren die Filmsequenzen in anderen Teilen überzeugender. Man wird den Eindruck nicht los, und damit ist C&C 4 nicht der erste Titel, dass die Entwickler einen Endspurt eingelegt haben und das Spiel an sich viel zu kurz geraten ist. Trotzdem sorgt es mit den überlegten Rollenspielelementen und taktischen Kämpfen für kurzweiligen Zeitvertreib.

**<span style="text-decoration: underline;">Wertung (PC Version) 79%</span>**

   

