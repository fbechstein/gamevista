---
title: Impressum
type: page

---
**Betreiber:**

Frank Bechstein   
-GameVista   
Bahnhofstr. 11   
22880 Wedel   
Germany

Telefon: +49 4103 7018423

eMail: FrankBechstein@gamevista.de

Kleinunternehmer  
&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8212;&#8211;  
**Finanzamt: FA Pinneberg**   
**Steuernummer: 31/004/03170**

Web: [http://www.gamevista.de][1]

**Chefredaktion, verantwortlich für den Inhalt:** 

**Redaktion:**

**Anzeigenleitung & Marketing**

<p class="testtext">
  <strong>Urheberrecht<br /></strong>Die Inhalte der gamevista.de-Seiten sind urheberrechtlich geschützt und ihre Inhalte dürfen weder ganz noch in Teilen ohne vorherige schriftliche Genehmigung des Urhebers vervielfältigt und/oder veröffentlicht oder in einem Informationssystem gespeichert werden.<br />Der Betreiber übernehmt keinerlei Gewähr für die Aktualität, Korrektheit, Vollständigkeit oder Qualität der bereitgestellten Informationen. Jegliche Haftungsansprüche für Schäden, die aus der Nutzung der hier angebotenen Informationen bzw. durch die Nutzung fehlerhafter oder unvollständiger Informationen verursacht wurden, sind grundsätzlich ausgeschlossen.<br />Der Betreiber behält sich vor Änderungen oder Korrekturen an den angebotenen Informationen vorzunehmen.
</p>

<p class="testtext">
  <strong>Verweise und Links</strong><br />Bei direkten oder indirekten Verweisen auf fremde Webseiten (&#8222;Hyperlinks&#8220;), die außerhalb des Verantwortungsbereiches des Betreibers liegen, würde eine Haftungsverpflichtung ausschließlich in dem Fall in Kraft treten, in dem der Betreiber von den Inhalten Kenntnis hat und es ihm technisch möglich und zumutbar wäre, die Nutzung im Falle rechtswidriger Inhalte zu verhindern.<br />Der Betreiber erklärt hiermit ausdrücklich, dass zum Zeitpunkt der Linksetzung keine illegalen Inhalte auf den zu verlinkenden Seiten erkennbar waren. Der Betreiber distanziert sich hiermit ausdrücklich von allen Inhalten aller gelinkten /verknüpften Seiten, die nach der Linksetzung verändert wurden. Diese Feststellung gilt für alle innerhalb des eigenen Internetangebotes gesetzten Links und Verweise sowie für Fremdeinträge in von dem Betreiber eingerichteten Gästebüchern, Diskussionsforen und Mailinglisten. Für illegale, fehlerhafte oder unvollständige Inhalte und insbesondere für Schäden, die aus der Nutzung oder Nichtnutzung solcherart dargebotener Informationen entstehen, haftet allein der Anbieter der Seite, auf welche verwiesen wurde, nicht derjenige, der über Links auf die jeweilige Veröffentlichung lediglich verweist.
</p>

<span class="testtext"><strong>Urheber- und Kennzeichenrecht<br /></strong>Alle innerhalb dieses Webangebots genannten bzw. benutzen Firmen- und Markennamen sowie Logos sind eingetragene Warenzeichen der jeweiligen Firmen, auch wenn sie nicht explizit als solche gekennzeichnet sind. Selbiges gilt auch für Namen von Produkten deren Namensrechte bei den Herstellern liegen.</span>

 [1]: /